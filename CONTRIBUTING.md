# Contribution Guide

## 1. Code Style Guide

The official style guide is the [WebKit Style Guide for C++](https://webkit.org/code-style-guidelines/). Take your time to read the style guide thoroughly. Be aware that the standards do not only include formatting, but also rules
e.g. on the naming of variables. Python code should follow the PEP8 style guide.

The WebKit style guide is extended as described in the following sections.

### 1.1 File Names

The file names follow strict naming rules. C++ files must use the .hpp / .cpp file endings. Pure C-code must use .h / .c.

| Type | Naming | Other Rules |
|---|---|---|
| **Classes** | **CamelCase**, e.g. SuperClass.hpp | In general there must not be more than one class per file. Only in very few exceptions (small helper classes) it is viable to put these classes together |
| **Methods-only files** | **snake_case**, e.g. math.cpp or pure_c.c | |
| **Applications / Tests** | **snake_case**, e.g. test_app.cpp | |

### 1.2 File Header

The following file header must be added to the beginning of all project files:

```C++
/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mec.ed.tum.de/am
 */
```

### 1.3 Comment Style

All declarations must be documented in Qt-Style manner (see the example below). For more information see the Doxygen [documentation](http://www.doxygen.nl/manual/docblocks.html).

```C++
//!  A test class.
/*!
  A more elaborate class description.
*/
class QTstyle_Test
{
  public:
    //! An enum.
    /*! More detailed enum description. */
    enum TEnum {
                 TVal1, /*!< Enum value TVal1. */
                 TVal2, /*!< Enum value TVal2. */
                 TVal3  /*!< Enum value TVal3. */
               }
         //! Enum pointer.
         /*! Details. */
         *enumPtr,
         //! Enum variable.
         /*! Details. */
         enumVar;

    //! A constructor.
    /*!
      A more elaborate description of the constructor.
    */
    QTstyle_Test();
    //! A destructor.
    /*!
      A more elaborate description of the destructor.
    */
   ~QTstyle_Test();

    //! A normal member taking two arguments and returning an integer value.
    /*!
      \param a an integer argument.
      \param s a constant character pointer.
      \return The test results
      \sa QTstyle_Test(), ~QTstyle_Test(), testMeToo() and publicVar()
    */
    int testMe(int a,const char *s);

    //! A pure virtual member.
    /*!
      \sa testMe()
      \param c1 the first argument.
      \param c2 the second argument.
    */
    virtual void testMeToo(char c1,char c2) = 0;

    //! A public variable.
    /*!
      Details.
    */
    int publicVar;

    //! A function variable.
    /*!
      Details.
    */
    int (*handler)(int a,int b);
};
```

#### Documenting Virtual Methods

Virtual methods must always be documented in the superclasses / interfaces that declare them first. In the subclasses, there are several options:

1. Add no documentation at all. Doxygen will automatically pull in the documentation of the superclass's method
2. Modify the superclass documentation with copydoc:
  ```
    /*!
     * @copydoc Superclass::method()
     * Here you may add additional text, which is added to the superclass' documentation
     */
  ```

## 2. Automatic Code-Formatting with clang-format

You may format parts of files / whole files automatically using the command line tool 'clang-format'. The style of the project is described by the clang-format config file '.clang-format' in the project's root directory. Please
make sure to run clang-format in the base directory.

### 2.1 Using the command line

The command ```clang-format -style=file [filename]``` prints a formatted version of the given file to stdout. The "file" option for the style tells clang-format to look for the '.clang-format' file in the project directory (parent directories or current directory).

### 2.2 Within QtCreator

clang-format may be called from within qtCreator by installing the "Beautifier" plugin. After installation, the path to the clang-format executable as well as the style "File" must be specified in the qtCreator settings.
Formatting of code may be trigged via the menu "Extras->Beautifier->ClangFormat->...". You can also set a keyboard shortcut in the qtCreator settings.

## 3. Working with CLion

All CLion settings (code style, templates for headers, ...) are stored in the git repository itself. To get started
with the configuration
* duplicate the template workspace setup `./idea/workspace_template.xml` to `./idea/workspace.xml`.
* Launch CLion and open project from CMakeLists file
* You are all set.

**Do not commit changed files in .idea/ unless you know what you're doing.**

## 4. Git Guidelines

Please have a look at https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow if you are new to the Gitflow Workflow.

### 4.1 Make rebase default on pull

Set up git to use rebasing on pull by default. This ensures linear history for "standard" commits and doesn't create unnecessary merge commits.
Merge commits should always imply the merging of different branches and not "everyday merges" caused by pulling-in remote changes.

```git config --global branch.autosetuprebase always```

**NOTE:** This only applies to newly created or checked out branches of all projects.
You can either clone the project again or use the following command to activate rebase on pull for a specific branch "branchname" of the current project:

```git config branch.[branchname].rebase true```

### 4.2 How to fix an issue:

1. Create Issue(s), feature branch and Merge Request in gitlab
2. Fix the damn thing
3. Resolve the Work in Progress state of the Merge Request and assign it to one of the Maintainers.
4. The MR is reviewed and accepted if good.

### 4.3 Branches

There is no 'master' branch. Instead, there is a release branch for every major version of broccoli. Branches of old releases may be converted to a git tag if the version is no longer maintained.

| Version | Branch name |
| -------- | -------- |
| 0.x.x | atlantic |
| 1.x.x | primo |
| 2.x.x | belstar |
| 3.x.x | sparco |
| 4.x.x | corvet |

Major releases of broccoli may break compatibility. Minor + Patch releases must keep compatibility with old versions.

### 4.4 Releases (Maintainers only)

Releasing a version of broccoli:

1. Update the version information in CMakeLists.txt in develop (if not already done)
2. Create a Merge Request from develop to the corresponding release branch (see 3.3). You may have to create the branch if this is a new major version of broccoli. Write down all changes, which are part of this version.
3. Create a tag with the version name - e.g. "v0.0.1" - from the release branch. Add the release merge request description in release notes.
