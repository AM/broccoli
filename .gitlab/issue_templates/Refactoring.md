### Problem / Bad Code Smell

Why do we have to change something?

## Proposed Solution

What is your suggestion?

## Possible Side-Effects

What are possible (positive and negative) side-effects of the refactoring?

/label ~refactoring