Please describe your changes to the behavior of the affected code modules here. Add changes
below the respective category entry (please delete categories you don't need)

**New Features**
* [module name]:
  * What is new...

**Bug Fixes**
* [module name]:
  * What you fixed
* [module name]: Inline if only one thing changed

**Non-Breaking Changes**
* [module name]:
  * What has changed

**Breaking Changes**
* [module name]:
  * What has changed
