

#
# broccoli module
#

# Generate version-header
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/version.h.in ${CMAKE_CURRENT_BINARY_DIR}/broccoli/broccoli-version.h)

# add imported target support
add_library(broccoli INTERFACE)

# Create namespaced alias
add_library(eat::broccoli ALIAS broccoli)

target_include_directories (broccoli INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
    $<INSTALL_INTERFACE:include>
)

target_compile_features(broccoli INTERFACE cxx_std_11)

#
# Project options
#

# Export library for downstream projects
export(TARGETS broccoli NAMESPACE eat:: FILE ${PROJECT_BINARY_DIR}/cmake/broccoli-export.cmake)

# Export broccoli package to CMake registry such that it can be easily found by
# CMake even if it has not been installed to a standard directory.
export (PACKAGE broccoli)


#
# Deployment
#

# Library
install(TARGETS broccoli
    EXPORT  broccoli-export               COMPONENT dev
)

# CMake Config
install(EXPORT broccoli-export
    NAMESPACE   eat::
    DESTINATION ${INSTALL_CMAKE}
)

# Cmake components
install(FILES
        ${CMAKE_SOURCE_DIR}/cmake/broccoli-eigen.cmake
        ${CMAKE_SOURCE_DIR}/cmake/broccoli-sdl.cmake
        ${CMAKE_SOURCE_DIR}/cmake/broccoli-zlib.cmake
        DESTINATION ${INSTALL_CMAKE})

# Header files
install(DIRECTORY
    ${CMAKE_CURRENT_SOURCE_DIR}/broccoli DESTINATION ${INSTALL_INCLUDE}
    COMPONENT dev
    FILES_MATCHING PATTERN "*.hpp"
)

# version header
install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/broccoli/broccoli-version.h
    DESTINATION ${INSTALL_INCLUDE}
    COMPONENT dev
)
