/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once

#include "../ForwardDeclarations.hpp"
#include "FakeBusDriver.hpp"
#include "FakeBusVariableRegistry.hpp"
#include "gmock/gmock.h"
#include <memory>

namespace broccoli {
namespace hwl {
    namespace testing {

        /*!
         * \brief Default factory for a device under test of type T
         * \tparam T
         */
        template <typename T>
        struct DUTFactory {
            template <typename BusType>
            static std::shared_ptr<T> make(BusDriverControl<BusType>& bus)
            {
                return std::make_shared<T>(bus);
            }
        };

        /*!
         * \brief Test Suite for BusDevice instances
         * \tparam DUTType Device under Test Type
         * \tparam Factory DUT factory
         * \tparam BusType Explicitly specified BusType
         * \tparam BusDriverTemplate Bus Variable Registry template
         */
        template <typename DUTType, typename Factory = DUTFactory<DUTType>, typename BusType = typename DUTType::BusType, template <typename> class BusDriverTemplate = FakeBusDriver>
        class BusDeviceTest : public ::testing::Test {
        public:
            using DeviceType = DUTType;

        protected:
            void SetUp() override
            {
                m_device = Factory::make(m_busDriver);
                m_busDriver.addDevice(*m_device);
                m_busDriver.registry().testIfValid();

                ON_CALL(this->m_busDriver, cycleTimeInUs()).WillByDefault(::testing::Return(1000));
            }

            //! Returns a reference to the device under test
            DeviceType& dut() { return *m_device; }

            //! Test the device's process() method. Passes m_busControl as parameter
            void process()
            {
                m_busDriver.process();
            }

            std::shared_ptr<DeviceType> m_device;
            ::testing::NiceMock<BusDriverTemplate<BusType>> m_busDriver;
        };

    } // namespace testing
} // namespace hwl
} // namespace broccoli
