/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once

#include "../BusDriver.hpp"
#include "FakeBusVariableRegistry.hpp"
#include "gmock/gmock.h"

namespace broccoli {
namespace hwl {
    namespace testing {

        /*!
         * \brief Fakes a BusDriver
         * \tparam BusType
         */
        template <typename BusType>
        class FakeBusDriver : public BusDriver<BusType> {
        public:
            void requestState(const typename BusType::StateType& state) override { m_state = state; }
            typename BusType::StateType state() const noexcept override { return m_state; }

            MOCK_METHOD(std::size_t, cycleTimeInUs, (), (const, noexcept, override));
            MOCK_METHOD(bool, delegateTransfer, (std::size_t), (override));
            void processBus() override {}

            const typename BusDriver<BusType>::DeviceContainerType& devices() const override { return m_devices; }

            template <typename DeviceType>
            void addDevice(DeviceType& device)
            {
                device.linkVariables(m_registry);
                m_devices.push_back(device);
            }

            FakeBusVariableRegistry<BusType>& registry() { return m_registry; }
            const FakeBusVariableRegistry<BusType>& registry() const noexcept { return m_registry; }

        protected:
            typename BusDriver<BusType>::DeviceContainerType& devices() override { return m_devices; }
            typename BusDriver<BusType>::DeviceContainerType m_devices;
            typename BusType::StateType m_state;
            FakeBusVariableRegistry<BusType> m_registry;
        };

    } // namespace testing
} // namespace hwl
} // namespace broccoli
