/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once

#include "../BusDriverControl.hpp"
#include "../type_traits.hpp"
#include "../variables.hpp"

namespace broccoli {
namespace hwl {

    template <typename BusVariableType>
    class AutoTransfer : public BusVariableType {
    public:
        using BusVariableType::BusVariableType;
        using BusVariableType::operator=;

        /*!
         * \brief Set the maximum number of automatic retries in case a transaction fails
         * \param maxRetries Number of retries (0 = no retry)
         */
        void setMaxRetries(std::size_t maxRetries) noexcept { m_maxRetries = maxRetries; }

        /*!
         * \brief
         */
        void renew() noexcept
        {
            m_failedTransfers = 0;
            auto guard = this->lockWithGuard();
            guard->markUncertain();
        }

        bool isLatched() const noexcept
        {
            auto guard = this->lockWithGuard();
            return guard->isLatched();
        }

        bool retryError() const noexcept
        {
            auto guard = this->lockWithGuard();
            return guard->hasFailed() && !canRetry();
        }

        void resetRetryCounter() noexcept { m_failedTransfers = 0; }

        template <typename BusType>
        bool dispatch(BusDriverControl<BusType>& bus) noexcept
        {
            bool doTransfer;
            bool newLatch = false;
            {
                auto guard = this->lockConstWithGuard();
                doTransfer = guard->isUncertain() && !guard->isPending();

                if (guard->hasFailed()) {
                    m_failedTransfers++;
                    doTransfer = canRetry();
                } else if (guard->hasFinished()) {
                    m_failedTransfers = 0;
                }
                newLatch = guard->isLatched() && !m_wasLatched;
                m_wasLatched = guard->isLatched();
            }
            if (doTransfer) {
                if (!bus.template transfer(*this))
                    m_failedTransfers++;
            }
            return newLatch;
        }

    protected:
        bool canRetry() const noexcept { return m_failedTransfers < m_maxRetries; }

        //! Was latched in last dispatch cycle?
        bool m_wasLatched = false;

        //! Number of failed transfers in a row
        std::size_t m_failedTransfers = 0;

        //! Maximum number of retries before a state update fails
        std::size_t m_maxRetries = 3;
    };

} // namespace hwl
} // namespace broccoli
