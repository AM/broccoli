/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once

#include "../BusDriverControl.hpp"
#include "../type_traits.hpp"
#include "../variables.hpp"
#include "AutoTransfer.hpp"

namespace broccoli {
namespace hwl {

    template <typename InputBusVariableType, typename OutputBusVariableType>
    class AutoState;

    template <typename T>
    class AutoState<InputBusVariable<T>, AsyncOutputBusVariable<T>> {
    public:
        using InputVariableType = InputBusVariable<T>;
        using OutputVariableType = AsyncOutputBusVariable<T>;

        AutoState() = default;

        explicit AutoState(const T& value)
            : m_outputTransfer(value)
        {
        }

        explicit AutoState(T&& value)
            : m_outputTransfer(std::move(value))
        {
        }

        void renew() noexcept { m_outputTransfer.renew(); }

        bool isLatched() const noexcept { return m_outputTransfer.isLatched() && m_stateMonitor == m_outputTransfer; }

        bool retryError() const noexcept { return m_outputTransfer.retryError(); }

        void resetRetryCounter() noexcept { m_outputTransfer.resetRetryCounter(); }

        template <typename BusType>
        bool dispatch(BusDriverControl<BusType>& bus) noexcept
        {
            bool latched = isLatched();
            bool newLatch = latched && !m_wasLatched;
            m_wasLatched = latched;

            m_outputTransfer.template dispatch(bus);
            return newLatch;
        }

        InputVariableType& monitor() noexcept { return m_stateMonitor; }
        const InputVariableType& monitor() const noexcept { return m_stateMonitor; }

        OutputVariableType& desired() noexcept { return m_outputTransfer; }
        const OutputVariableType& desired() const noexcept { return m_outputTransfer; }

    protected:
        //! Input state monitor
        InputVariableType m_stateMonitor;

        //! Output state
        AutoTransfer<OutputVariableType> m_outputTransfer;

        //! Was latched in last dispatch cycle?
        bool m_wasLatched = false;
    };

    template <typename T>
    class AutoState<AsyncInputBusVariable<T>, AsyncOutputBusVariable<T>> {
    public:
        using InputVariableType = AsyncInputBusVariable<T>;
        using OutputVariableType = AsyncOutputBusVariable<T>;

        AutoState() = default;

        explicit AutoState(const T& value)
            : m_outputTransfer(value)
        {
        }

        explicit AutoState(T&& value)
            : m_outputTransfer(std::move(value))
        {
        }

        void renew() noexcept
        {
            m_outputTransfer.renew();
        }

        bool isLatched() const noexcept { return m_stateMonitor.isLatched() && m_outputTransfer.isLatched() && m_stateMonitor == m_outputTransfer; }

        bool retryError() const noexcept { return m_outputTransfer.retryError() || m_stateMonitor.retryError(); }

        void resetRetryCounter() noexcept
        {
            m_outputTransfer.resetRetryCounter();
            m_stateMonitor.resetRetryCounter();
        }

        template <typename BusType>
        bool dispatch(BusDriverControl<BusType>& bus) noexcept
        {
            bool latched = isLatched();
            bool newLatch = latched && !m_wasLatched;
            m_wasLatched = latched;

            if (m_outputTransfer.template dispatch(bus)) {
                m_stateMonitor.renew();
            }
            m_stateMonitor.template dispatch(bus);

            return newLatch;
        }

        InputVariableType& monitor() noexcept { return m_stateMonitor; }
        const InputVariableType& monitor() const noexcept { return m_stateMonitor; }

        OutputVariableType& desired() noexcept { return m_outputTransfer; }
        const OutputVariableType& desired() const noexcept { return m_outputTransfer; }

    protected:
        //! Input state monitor
        AutoTransfer<InputVariableType> m_stateMonitor;

        //! Output state
        AutoTransfer<OutputVariableType> m_outputTransfer;

        //! Was latched in last dispatch cycle?
        bool m_wasLatched = false;
    };

    //! AutoState alias using Async bus variable both for output and monitoring
    template <typename T>
    using AsyncAutoState = AutoState<AsyncInputBusVariable<T>, AsyncOutputBusVariable<T>>;

    /*!
     * \brief Helper template to register both desired and monitor variable of an AsyncAutoState to the same identifier
     * \tparam T Type of the variable
     * \tparam Derived Derived registry type
     * \tparam Identifier Object identifier
     * \param registry The registry to register to
     * \param state The AsyncAutoState instance
     * \param id An identifier the state should be registered to (reading/writing)
     */
    template <typename T, typename Derived, typename Identifier>
    void registerAutoStatePair(BusVariableRegistryBase<Derived>& registry, AsyncAutoState<T>& state, const Identifier& id)
    {
        registry.registerVariable(state.desired(), id);
        registry.registerVariable(state.monitor(), id);
    }

} // namespace hwl
} // namespace broccoli
