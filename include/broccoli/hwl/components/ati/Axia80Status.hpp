/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#pragma once

namespace broccoli {
namespace hwl {
    /*!
     * \brief ATI/Schunk Axia80 Status Representation
     * \ingroup broccoli_hwl_components
     */
    class Axia80Status {
    public:
        void update(const uint32_t& statusCode)
        {
            m_statusCode = statusCode;
        }

        //! Returns true if the status indicates an error
        bool isError() const
        {
            return m_statusCode & 0x38000027;
        }

        //! Returns true if the sensor is operational
        bool isOperational() const
        {
            return (m_statusCode & 0x3800002f) == 0;
        }

        //! String representation of the status
        std::string toString() const
        {
            if (isOperational()) {
                return "Ok";
            }

            if (m_statusCode & (1 << 0)) {
                return "Strain gage temperature out of range";
            }

            if (m_statusCode & (1 << 1)) {
                return "Power supply out of range";
            }

            if (m_statusCode & (1 << 2)) {
                return "Strain gage defective";
            }

            if (m_statusCode & (1 << 3)) {
                return "Sensor is busy changing calibration or other settings";
            }

            if (m_statusCode & (1 << 5)) {
                return "One or more internal errors occurred (see manual)";
            }

            if (m_statusCode & (1 << 27)) {
                return "Strain gage load out of range";
            }

            if (m_statusCode & (1 << 28)) {
                return "Simulated error bit set";
            }

            if (m_statusCode & (1 << 29)) {
                return "Calibration checksum error";
            }

            if (m_statusCode & (1 << 30)) {
                return "Sensing range exceeded (output is saturated)";
            }

            return "Unknown state";
        }

    private:
        //! Internal status code storage - start with all bits set (error)
        uint32_t m_statusCode = 0xffffffff;
    };
} // namespace hwl
} // namespace broccoli
