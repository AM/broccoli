/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#pragma once

#include "../../BusDevice.hpp"
#include "../../BusDriverControl.hpp"
#include "../../bus_types/EtherCAT.hpp"
#include "../../variables.hpp"
#include "../../variables/AutoTransfer.hpp"
#include "../general/Sensor.hpp"
#include "Axia80Control.hpp"
#include "Axia80Status.hpp"

#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#endif

namespace broccoli {
namespace hwl {
    template <typename BusType>
    class Axia80;

    /*!
     * \brief Implements an EtherCAT BusDevice for the ATI/Schunk Axia80 force/torque sensor
     * \ingroup broccoli_hwl_components
     */
    template <>
    class Axia80<EtherCAT> : public BusDevice<EtherCAT>, public Sensor {
    public:
        using BusType = EtherCAT;
        using BusDevice<EtherCAT>::BusDevice;

        //! Returns a status object for the sensor
        const Axia80Status& status() const noexcept { return m_status; }

        //! Returns a control object for the sensor
        Axia80Control& control() noexcept { return m_control; }

#ifdef HAVE_EIGEN3
        //! Returns the contact forces [N] in the sensor FoR
        const Eigen::Vector3d& forces() const noexcept { return m_forces; }

        //! Returns the contact torques [Nm] in the sensor FoR
        const Eigen::Vector3d& torques() const noexcept { return m_torques; }
#endif

        //! Raw counts for sensor force in x-direction
        int32_t forceInCountsX() const noexcept { return m_Fx; };

        //! Raw counts for sensor force in y-direction
        int32_t forceInCountsY() const noexcept { return m_Fy; };

        //! Raw counts for sensor force in z-direction
        int32_t forceInCountsZ() const noexcept { return m_Fz; };

        //! Raw counts for sensor torque around x-axis
        int32_t torqueInCountsX() const noexcept { return m_Tx; };

        //! Raw counts for sensor torque around y-axis
        int32_t torqueInCountsY() const noexcept { return m_Ty; };

        //! Raw counts for sensor torque around z-axis
        int32_t torqueInCountsZ() const noexcept { return m_Tz; };

        /*!
         * \brief Returns the counts per N for forces
         * \warning This is only valid when ready() is true
         */
        int32_t countsPerNewton() const noexcept { return m_countsPerNewton; }

        /*!
         * \brief Returns the counts per Nm for torques
         * \warning This is only valid when ready() is true
         */
        int32_t countsPerNewtonMeter() const noexcept { return m_countsPerNewtonMeter; }

        bool zeroState() noexcept override
        {
            control().setZero();
            return true;
        }

        bool ready() const noexcept override
        {
            return m_status.isOperational() && m_countsPerNewton.isLatched() && m_countsPerNewtonMeter.isLatched();
        }

        //! Registers bus variables to given registry
        template <typename Derived>
        void linkVariables(BusVariableRegistryBase<Derived>& registry)
        {
            registry.registerVariable(m_Fx, EtherCAT::ObjectIdentifierType{ "Reading Data.Fx/Gage0" });
            registry.registerVariable(m_Fy, EtherCAT::ObjectIdentifierType{ "Reading Data.Fy/Gage1" });
            registry.registerVariable(m_Fz, EtherCAT::ObjectIdentifierType{ "Reading Data.Fz/Gage2" });
            registry.registerVariable(m_Tx, EtherCAT::ObjectIdentifierType{ "Reading Data.Tx/Gage3" });
            registry.registerVariable(m_Ty, EtherCAT::ObjectIdentifierType{ "Reading Data.Ty/Gage4" });
            registry.registerVariable(m_Tz, EtherCAT::ObjectIdentifierType{ "Reading Data.Tz/Gage5" });

            registry.registerVariable(m_controlCode, EtherCAT::ObjectIdentifierType{ "Control Codes.Control 1" });
            registry.registerVariable(m_statusCode, EtherCAT::ObjectIdentifierType{ "Reading Data.Status Code" });

            registry.registerVariable(m_countsPerNewton, EtherCAT::AsyncObjectIdentifierType{ 0x2021, 0x37 });
            registry.registerVariable(m_countsPerNewtonMeter, EtherCAT::AsyncObjectIdentifierType{ 0x2021, 0x38 });
        }

    protected:
        void processDevice() override
        {
            if (bus().state() != BusType::StateType::op())
                return;

            m_controlCode = m_control.controlCodeOne();
            m_status.update(m_statusCode);
            m_countsPerNewton.dispatch(bus());
            m_countsPerNewtonMeter.dispatch(bus());

#ifdef HAVE_EIGEN3
            if (m_countsPerNewton != 0) {
                m_forces(0) = static_cast<double>(m_Fx.value()) / m_countsPerNewton;
                m_forces(1) = static_cast<double>(m_Fy.value()) / m_countsPerNewton;
                m_forces(2) = static_cast<double>(m_Fz.value()) / m_countsPerNewton;
            }
            if (m_countsPerNewtonMeter != 0) {
                m_torques(0) = static_cast<double>(m_Tx.value()) / m_countsPerNewtonMeter;
                m_torques(1) = static_cast<double>(m_Ty.value()) / m_countsPerNewtonMeter;
                m_torques(2) = static_cast<double>(m_Tz.value()) / m_countsPerNewtonMeter;
            }
#endif

            if (m_isInitialized) {
                if (!ready())
                    m_isInitialized = false;
            } else {
                if (ready())
                    m_isInitialized = true;
            }
        }

        void onStateChange() override
        {
            if (bus().state() == EtherCAT::StateType::safeOp()) {
                m_countsPerNewton.renew();
                m_countsPerNewtonMeter.renew();
            }
        }

    private:
        //! Is initialized?
        bool m_isInitialized = false;

        // Forces
        //! Fx force in counts
        InputBusVariable<int32_t> m_Fx;

        //! Fy force in counts
        InputBusVariable<int32_t> m_Fy;

        //! Fz force in counts
        InputBusVariable<int32_t> m_Fz;

        // Torques

        //! Tx torque in counts
        InputBusVariable<int32_t> m_Tx;

        //! Ty torque in counts
        InputBusVariable<int32_t> m_Ty;

        //! Tz torque in counts
        InputBusVariable<int32_t> m_Tz;

        //! Control Code 1
        OutputBusVariable<uint32_t> m_controlCode;

        //! Status
        InputBusVariable<uint32_t> m_statusCode;

        //! Calibration counts per N
        AutoTransfer<AsyncInputBusVariable<int32_t>> m_countsPerNewton;

        //! Calibration counts per Nm
        AutoTransfer<AsyncInputBusVariable<int32_t>> m_countsPerNewtonMeter;

        //! Representation of the status code
        Axia80Status m_status;

        //! Control representation
        Axia80Control m_control;

#ifdef HAVE_EIGEN3
        //! The measured forces
        Eigen::Vector3d m_forces = Eigen::Vector3d::Zero();

        //! The measured torques
        Eigen::Vector3d m_torques = Eigen::Vector3d::Zero();

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif
    };
} // namespace hwl
} // namespace broccoli
