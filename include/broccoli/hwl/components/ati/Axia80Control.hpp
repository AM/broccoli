/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#pragma once

namespace broccoli {
namespace hwl {
    /*!
     * \brief ATI/Schunk Axia80 Control Interface
     * \ingroup broccoli_hwl_components
     */
    class Axia80Control {
    public:
        //! Axia calibrations
        enum class Calibration : uint {
            AXIA_CALIB1_200_360N_8NM = 0,
            AXIA_CALIB2_500_900N_20NM = 1
        };

        //! Axia sample rates
        enum class SampleRate : uint {
            AXIA_500HZ = 0,
            AXIA_1KHZ = 1,
            AXIA_2KHZ = 2,
            AXIA_4KHZ = 3
        };

        //! Set lowpass filter (see manual), 0 = disabled.
        void setLowPassFilter(unsigned int filter)
        {
            if (filter > 8) {
                filter = 0;
            }

            m_controlCodeOne = (m_controlCodeOne & 0xff0f) | ((filter & 0xf) << 4);
        }

        //! Choose calibration range
        void setCalibration(Calibration calibration)
        {
            m_controlCodeOne = (m_controlCodeOne & 0xfeff) | (((int)calibration & 1) << 8);
        }

        //! Choose sample rate
        void setSampleRate(SampleRate sampleRate)
        {
            m_controlCodeOne = (m_controlCodeOne & 0xfff) | (((int)sampleRate & 0xf) << 12);
        }

        //! Zero force coutput
        void setZero()
        {
            m_singleCycleControlCodeOne |= 0x1;
        }

        //! Returns the encoded "Control Code 1" to be sent to the device
        uint32_t controlCodeOne()
        {
            uint32_t totalCommand = m_controlCodeOne | m_singleCycleControlCodeOne;
            m_singleCycleControlCodeOne = 0;
            return totalCommand;
        }

    private:
        //! Control code 1
        uint32_t m_controlCodeOne = 0x0;

        //! Single-shot command mask
        mutable uint32_t m_singleCycleControlCodeOne = 0x0;
    };
} // namespace hwl
} // namespace broccoli
