/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

#include <memory>
#include <unordered_set>
#include <vector>

namespace broccoli {
namespace memory {
    //! Generic pool allocator for user-defined objects
    /*!
     * \ingroup broccoli_memory
     *
     * Allocates user-defined objects in the form of blocks ("pools"), where each block contains a fixed count of objects. This can significantly increase performance when
     * it is necessary to frequently allocate objects of the same type at runtime. Note that performance may be tuned by the parameter \ref objectsPerBlock() (optimum
     * depends on object type, implementation and platform and has to be evaluated experimentally).
     *
     * \note This class supports **optional** construction and destruction of the managed objects. This can be toggled with the parameters \ref constructObjects() and
     * \ref destructObjects(). Note that destruction of the pool allocator (or calling \ref reset()) is slow, if \ref destructObjects() is set to `true`, since the pool
     * allocator has to figure out which objects have to be destructed and which not (empty slots). If this is a problem, one might set \ref destructObjects() to `false`
     * such that destruction is up to the user. For plain-old-data structures, \ref constructObjects() and \ref destructObjects() should be set to `false` (no construction
     * or destruction necessary).
     *
     * \attention All allocated memory is automatically freed upon destruction (or on calling \ref reset()). Thus, the user **must not** free object memory manually!
     *
     * \tparam T Datatype of the objects to allocate
     * \tparam Allocator The allocator used to allocate blocks of objects (set this to `Eigen::aligned_allocator<T>` if `T` is an `Eigen` object, otherwise `std::allocator<T>` can be used)
     */
    template <typename T, typename Allocator = std::allocator<T>>
    class PoolAllocator {
        // Type definition
        // ---------------
    protected:
        //! Placeholder for "free" object slots in a block
        /*!
         * Free slots in a block are filled with an object placeholder. The placeholder contains a simple pointer
         * to the next free slot in the same OR another block. If the corresponding slot is to be filled, the
         * placeholder is discarded and the memory is instead filled with the data of the object.
         */
        struct FreeObjectPlaceHolder {
            FreeObjectPlaceHolder* m_nextFreeObjectPlaceholder; //!< Pointer to next free object placeholder (may be in the same OR another block) (a `nullptr` indicates, that there is no further free object placeholder available)
        };

        //! Abstraction of a block
        struct Block {
            Block* m_previousBlock; //!< Pointer to "previous" block (a `nullptr` indicates, that there is no previous block)
            T* m_firstObject; //!< Pointer to first object managed by this block (indicates start of memory occupied by this block)
        };

        // Static checks
        // -------------
    protected:
        static_assert(sizeof(T) >= sizeof(FreeObjectPlaceHolder), "The size of the object has to be greater or equal to the size of the free object placeholder!");

        // Construction
        // ------------
    public:
        //! Default constructor
        PoolAllocator() = default;

        //! Specialized constructor
        /*!
         * \param [in] objectsPerBlock Initializes \ref objectsPerBlock() - \copybrief objectsPerBlock()
         * \param [in] objectCount Count of objects for which memory should be reserved initially
         */
        PoolAllocator(const size_t& objectsPerBlock, const size_t& objectCount = 0)
            : m_objectsPerBlock((objectsPerBlock > 0) ? objectsPerBlock : 1)
        {
            if (objectCount > 0)
                reserve(objectCount);
        }

        //! Specialized constructor
        /*!
         * \param [in] objectsPerBlock Initializes \ref objectsPerBlock() - \copybrief objectsPerBlock()
         * \param [in] constructObjects Initializes \ref constructObjects() - \copybrief constructObjects()
         * \param [in] destructObjects Initializes \ref destructObjects() - \copybrief destructObjects()
         * \param [in] objectCount Count of objects for which memory should be reserved initially
         */
        PoolAllocator(const size_t& objectsPerBlock, const bool& constructObjects, const bool& destructObjects, const size_t& objectCount = 0)
            : m_objectsPerBlock((objectsPerBlock > 0) ? objectsPerBlock : 1)
            , m_constructObjects(constructObjects)
            , m_destructObjects(destructObjects)
        {
            if (objectCount > 0)
                reserve(objectCount);
        }

        //! Destructor
        /*! Triggers \ref reset() to free all memory */
        ~PoolAllocator() { reset(); }

        //! Copy constructor - **deleted**
        /*!
         * \note **Deleted** since copying a pool allocator makes no sense: since a pool-allocator manages it's
         * own memory (especially it frees the memory upon destruction) two pool-allocators must not share the
         * same memory blocks. Letting two pool-allocators manage the same memory blocks also obviously leads
         * to data corruption whenever a new object is allocated or deallocated. On the other hand, if a copy
         * operation would create a second pool with the same contained data (duplicate memory), there would still
         * be no possibility for the "user" to access the stored data of the new copy since the user does not know
         * the pointers to the objects stored in the new copy.
         */
        PoolAllocator(const PoolAllocator& other) = delete;

        //! Copy assignment operator - **deleted**
        /*! \note **Deleted** for the same reason as for the copy constructor (see above). */
        PoolAllocator& operator=(const PoolAllocator& other) = delete;

        //! Move constructor
        PoolAllocator(PoolAllocator&& other)
            : m_objectsPerBlock(other.m_objectsPerBlock)
            , m_constructObjects(other.m_constructObjects)
            , m_destructObjects(other.m_destructObjects)
            , m_blockAllocator(std::move(other.m_blockAllocator))
            , m_blockCount(other.m_blockCount)
            , m_lastAllocatedBlock(other.m_lastAllocatedBlock)
            , m_firstFreeObjectPlaceholder(other.m_firstFreeObjectPlaceholder)
        {
            // Manual reset of source (so it can be re-used or safely destructed)
            other.m_blockAllocator = std::make_unique<Allocator>();
            other.m_blockCount = 0;
            other.m_lastAllocatedBlock = nullptr;
            other.m_firstFreeObjectPlaceholder = nullptr;
        }

        //! Move assignment operator
        PoolAllocator& operator=(PoolAllocator&& other)
        {
            // Clean-up own storage before assignment
            reset();

            // Assign through move from source
            m_objectsPerBlock = other.m_objectsPerBlock;
            m_constructObjects = other.m_constructObjects;
            m_destructObjects = other.m_destructObjects;
            m_blockAllocator = std::move(other.m_blockAllocator);
            m_blockCount = other.m_blockCount;
            m_lastAllocatedBlock = other.m_lastAllocatedBlock;
            m_firstFreeObjectPlaceholder = other.m_firstFreeObjectPlaceholder;

            // Manual reset of source (so it can be re-used or safely destructed)
            other.m_blockAllocator = std::make_unique<Allocator>();
            other.m_blockCount = 0;
            other.m_lastAllocatedBlock = nullptr;
            other.m_firstFreeObjectPlaceholder = nullptr;

            return *this;
        }

        // Members
        // -------
    protected:
        // Configuration
        size_t m_objectsPerBlock = 1024; //!< \copybrief objectsPerBlock()
        bool m_constructObjects = false; //!< \copybrief constructObjects()
        bool m_destructObjects = false; //!< \copybrief destructObjects()

        // Internals
        std::unique_ptr<Allocator> m_blockAllocator = std::make_unique<Allocator>(); //!< Allocator used to allocate and deallocate blocks
        size_t m_blockCount = 0; //! \copybrief blockCount()
        Block* m_lastAllocatedBlock = nullptr; //!< Pointer to last allocated block (a `nullptr` indicates, that no blocks have been allocated yet)
        FreeObjectPlaceHolder* m_firstFreeObjectPlaceholder = nullptr; //!< Pointer to first free object placeholder (a `nullptr` indicates, that there is no free object placeholder available)

        // Getters
        // -------
    public:
        //! Count of objects per block (pool-size) - guaranteed to be greater than zero
        inline const size_t& objectsPerBlock() const { return m_objectsPerBlock; }

        //! If `true`, the pool allocator automatically constructs object instances when \ref allocate() is called
        inline const bool& constructObjects() const { return m_constructObjects; }

        //! If `true`, the pool allocator automatically destructs object instances when \ref deallocate() or \ref reset() or the destructor of the pool allocator is called
        inline const bool& destructObjects() const { return m_destructObjects; }

        //! Total count of allocated blocks
        inline const size_t& blockCount() const { return m_blockCount; }

        //! Maximum count of objects currently managed by the pool-allocator (includes occupied **and** empty slots)
        inline size_t capacity() const { return m_blockCount * m_objectsPerBlock; }

        // Setters
        // -------
    public:
        //! Resets the complete allocator and all managed objects
        /*! Destructs all contained objects (occupied slots) if \ref destructObjects() is set to `true`. Frees all memory. */
        inline void reset()
        {
            // Skip, if there are no blocks (no managed data at all -> nothing to reset)
            if (m_blockCount == 0)
                return;

            // Get list of all allocated blocks
            std::vector<Block*> blocks;
            blocks.reserve(m_blockCount);
            Block* currentBlock = m_lastAllocatedBlock;
            while (currentBlock != nullptr) {
                blocks.push_back(currentBlock);
                currentBlock = currentBlock->m_previousBlock;
            }

            // Destruct all allocated objects (only non-empty object slots)
            if (m_destructObjects == true) {
                // Create set of all free object slots
                std::unordered_set<FreeObjectPlaceHolder*> freeObjectPlaceHolders;
                freeObjectPlaceHolders.reserve(capacity()); // Worst case: all object slots are empty
                FreeObjectPlaceHolder* currentSlot = m_firstFreeObjectPlaceholder;
                while (currentSlot != nullptr) {
                    freeObjectPlaceHolders.insert(currentSlot);
                    currentSlot = currentSlot->m_nextFreeObjectPlaceholder;
                }

                // Iterate over blocks
                for (size_t i = 0; i < blocks.size(); i++) {
                    // Iterate over object slots in this block
                    T* currentObject = blocks[i]->m_firstObject;
                    for (size_t j = 0; j < m_objectsPerBlock; j++) {
                        currentSlot = reinterpret_cast<FreeObjectPlaceHolder*>(currentObject);
                        auto searchResult = freeObjectPlaceHolders.find(currentSlot);
                        if (searchResult != freeObjectPlaceHolders.end()) {
                            // Found in set of free objects -> slot is free -> remove from set to speedup remaining iterations (each object slot occurs only once)
                            freeObjectPlaceHolders.erase(searchResult);
                        } else {
                            // Not found in set of free objects -> occupied slot -> call destructor of object
                            currentObject->~T();
                        }
                        currentObject++;
                    }
                }
            }

            // Deallocate memory of blocks
            for (size_t i = 0; i < blocks.size(); i++) {
                m_blockAllocator->deallocate(blocks[i]->m_firstObject, m_objectsPerBlock); // Deallocate memory for managed objects
                delete blocks[i]; // Delete block itself
            }

            // Reset internals
            m_blockCount = 0;
            m_lastAllocatedBlock = nullptr;
            m_firstFreeObjectPlaceholder = nullptr;
        }

        //! Setter for \ref objectsPerBlock()
        /*! \warning Triggers \ref reset(), to guarantee that the pool allocator is never in an inconsistent state! */
        inline void setObjectsPerBlock(const size_t& value)
        {
            const size_t projectedValue = (value > 0) ? value : 1;
            if (projectedValue != m_objectsPerBlock) {
                reset();
                m_objectsPerBlock = projectedValue;
            }
        }

        //! Setter for \ref constructObjects()
        /*! \warning Triggers \ref reset(), to guarantee that the pool allocator is never in an inconsistent state! */
        inline void setConstructObjects(const bool& value)
        {
            if (value != m_constructObjects) {
                reset();
                m_constructObjects = value;
            }
        }

        //! Setter for \ref destructObjects()
        /*! \warning Triggers \ref reset(), to guarantee that the pool allocator is never in an inconsistent state! */
        inline void setDestructObjects(const bool& value)
        {
            if (value != m_destructObjects) {
                reset();
                m_destructObjects = value;
            }
        }

        //! Reserves memory for the given count of objects
        /*! \param [in] objectCount Count of objects for which memory should be reserved */
        inline void reserve(const size_t& objectCount)
        {
            while (capacity() < objectCount)
                addBlock();
        }

        //! Allocates an object **with** calling its constructor (may trigger allocation of a new block)
        /*! \param [in] args Arguments to forward to the constructor of the object */
        template <typename... Args>
        inline T* allocateWithConstruction(Args&&... args)
        {
            // Check, if there is no free slot available and create new block in this case
            if (m_firstFreeObjectPlaceholder == nullptr)
                addBlock();

            // Grab free object from pool
            T* newObject = reinterpret_cast<T*>(m_firstFreeObjectPlaceholder);

            // Move pointer to next free object
            m_firstFreeObjectPlaceholder = m_firstFreeObjectPlaceholder->m_nextFreeObjectPlaceholder;

            // Call constructor
            newObject = new (newObject) T(std::forward<Args>(args)...);

            // Pass back grabbed object
            return newObject;
        }

        //! Allocates an object **without** calling its constructor (may trigger allocation of a new block)
        inline T* allocateWithoutConstruction()
        {
            // Check, if there is no free slot available and create new block in this case
            if (m_firstFreeObjectPlaceholder == nullptr)
                addBlock();

            // Grab free object from pool
            T* newObject = reinterpret_cast<T*>(m_firstFreeObjectPlaceholder);

            // Move pointer to next free object
            m_firstFreeObjectPlaceholder = m_firstFreeObjectPlaceholder->m_nextFreeObjectPlaceholder;

            // Pass back grabbed object
            return newObject;
        }

        //! Allocates an object (may trigger allocation of a new block)
        /*! \param [in] args Arguments to forward to the constructor of the object (ignored, if \ref constructObjects() is set to `false`) */
        template <typename... Args>
        inline T* allocate(Args&&... args)
        {
            if (m_constructObjects == true)
                return allocateWithConstruction(std::forward<Args>(args)...);
            else
                return allocateWithoutConstruction();
        }

        //! De-allocates an object **with** calling its destructor
        inline void deallocateWithDestruction(T* object)
        {
            // Call destructor
            object->~T();

            // Mark corresponding memory as free
            FreeObjectPlaceHolder* slot = reinterpret_cast<FreeObjectPlaceHolder*>(object);
            slot->m_nextFreeObjectPlaceholder = m_firstFreeObjectPlaceholder;

            // Update pointer to next free object
            m_firstFreeObjectPlaceholder = slot;
        }

        //! De-allocates an object **without** calling its destructor
        inline void deallocateWithoutDestruction(T* object)
        {
            // Mark corresponding memory as free
            FreeObjectPlaceHolder* slot = reinterpret_cast<FreeObjectPlaceHolder*>(object);
            slot->m_nextFreeObjectPlaceholder = m_firstFreeObjectPlaceholder;

            // Update pointer to next free object
            m_firstFreeObjectPlaceholder = slot;
        }

        //! De-allocates an object (destructs object if \ref destructObjects() is set to `true`, but does **not** free the memory!)
        inline void deallocate(T* object)
        {
            if (m_destructObjects == true)
                deallocateWithDestruction(object);
            else
                deallocateWithoutDestruction(object);
        }

        // Helpers
        // -------
    protected:
        //! Adds a new block to the pool allocator (does **NOT** call object constructors)
        inline void addBlock()
        {
            // Create block and allocate memory for objects (does NOT call constructor)
            Block* newBlock = new Block;
            newBlock->m_previousBlock = m_lastAllocatedBlock;
            newBlock->m_firstObject = m_blockAllocator->allocate(m_objectsPerBlock);
            FreeObjectPlaceHolder* firstFreeObjectPlaceHolderInBlock = reinterpret_cast<FreeObjectPlaceHolder*>(newBlock->m_firstObject);

            // Setup object placeholders with a pointer to the "next free" object placeholder
            FreeObjectPlaceHolder* currentFreeObjectPlaceHolder = firstFreeObjectPlaceHolderInBlock;
            for (size_t i = 0; i + 1 < m_objectsPerBlock; i++) {
                currentFreeObjectPlaceHolder->m_nextFreeObjectPlaceholder = reinterpret_cast<FreeObjectPlaceHolder*>(reinterpret_cast<char*>(currentFreeObjectPlaceHolder) + sizeof(T));
                currentFreeObjectPlaceHolder = currentFreeObjectPlaceHolder->m_nextFreeObjectPlaceholder;
            }
            currentFreeObjectPlaceHolder->m_nextFreeObjectPlaceholder = m_firstFreeObjectPlaceholder;
            m_firstFreeObjectPlaceholder = firstFreeObjectPlaceHolderInBlock;

            // Update list of blocks
            m_lastAllocatedBlock = newBlock;

            // Update block counter
            m_blockCount++;
        }
    };
} // namespace memory
} // namespace broccoli
