/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

#include "PoolAllocator.hpp"
#include <assert.h>
#include <unordered_map>
#include <vector>

namespace broccoli {
namespace memory {
    //! Representation of a single node in an implicit D-ary heap (see \ref ImplicitDaryHeap)
    /*!
     * \ingroup broccoli_memory
     *
     * \tparam Key Datatype of keys used for sorting
     * \tparam T Datatype for actual user-data to be stored in the heap
     */
    template <typename Key, typename T>
    class ImplicitDaryHeapNode : public T {
        // Static checks
        // -------------
    public:
        static_assert(std::is_copy_constructible<T>::value, "The user-data 'T' has to be copy-constructible!");
        static_assert(std::is_destructible<T>::value, "The user-data 'T' has to be destructible!");

        // Construction
        // ------------
    public:
        //! Base constructor (from user-data)
        ImplicitDaryHeapNode(const T& data)
            : T(data)
        {
        }

        //! Copy-constructor
        ImplicitDaryHeapNode(const ImplicitDaryHeapNode& original) = default;

        // Members
        // -------
    public:
        Key m_nodeKey; //!< Key of this node used for sorting
        size_t m_nodeIndex; //!< Index of this node in the tree array
        size_t m_dataHash; //!< Hash of user-data
    };

    //! Implicit D-ary heap
    /*!
     * \ingroup broccoli_memory
     *
     * This class is a generalization of a binary heap (\f$ D = 2 \f$), where each node has \f$ D > 1 \f$ child nodes. The data is stored as an
     * **implicit** heap, which means that it is encoded as a level-order traversal in an array (in contrast to an "explicit" heap which stores
     * heap-allocated nodes and pointers). This typically has better caching behaviour as all nodes are stored in a continuous container (here:
     * `std::vector` for node pointers in the "tree" and \ref PoolAllocator for actual node data). Moreover, it allows simple iteration over all
     * nodes in the tree. The class is augmented by an internal `std::unordered_map` which allows efficient element access (e.g. for modifying
     * keys) and containment checks by the hash of the user-data.
     *
     * This class extends a `classical` priority queue which supports
     *   * checking if the heap is empty (see \ref empty()),
     *   * inserting a new node with corresponding key (see \ref insert()), and
     *   * extraction of the highest priority node (see \ref extractRoot()),
     *
     * by allowing the user also to
     *   * get the total count of nodes stored in the heap (see \ref size()),
     *   * get the highest priority node and its key without removing it from the heap (see \ref rootKey() and \ref rootData()),
     *   * check containment of an arbitrary node in the heap (selected by the user-data hash) (see \ref contains()),
     *   * clear the heap and optionally free all allocated memory (see \ref clear()),
     *   * pre-allocate memory for a certain amount of nodes as acceleration (see \ref reserve()),
     *   * remove the highest priority node from the heap (see \ref removeRoot()),
     *   * remove an arbitrary node (selected by the user-data hash) (see \ref remove()), and
     *   * increase/decrease the priority of an arbitrary node (selected by the user-data hash) (see \ref updateKey()).
     *
     * \tparam Key Datatype of keys used for sorting
     * \tparam KeyCompare Sorting function for keys (use `std::less<Key>` for a min-heap (minimum key is root node) and 'std::greater<Key>' for a max-heap (maximum key is root node))
     * \tparam T Datatype for actual user-data to be stored in the heap
     * \tparam Hash Custom hasher for `T` which allows to efficiently search for nodes in the heap (by the user-data hash)
     * \tparam Allocator The allocator used to allocate **nodes** (set this to `Eigen::aligned_allocator<ImplicitDaryHeapNode<Key, T>>` if `T` is an `Eigen` object, otherwise `std::allocator<ImplicitDaryHeapNode<Key, T>>` can be used)
     *
     * Reference
     * ---------
     * Implementation adopted and extended from D. H. Larkin, S. Sen, and R. E. Tarjan, "A Back-to-Basics Empirical Study of Priority Queues," in 2014 Proceedings of the Meeting on Algorithm Engineering and Experiments (ALENEX), pp. 61–72. doi: 10.1137/1.9781611973198.7.
     * Therein denoted as "implicit d-ary heap" (**not** the "simple" variant to allow an efficient \ref updateKey() operation).
     */
    template <typename Key, typename KeyCompare, typename T, typename Hash = std::hash<T>, typename Allocator = std::allocator<ImplicitDaryHeapNode<Key, T>>>
    class ImplicitDaryHeap {
        // Static checks
        // -------------
    public:
        static_assert(std::is_copy_constructible<T>::value, "The user-data 'T' has to be copy-constructible!");
        static_assert(std::is_destructible<T>::value, "The user-data 'T' has to be destructible!");

        // Type definitions
        // ----------------
    protected:
        //! Identity hash function (may accelerate default hasher on some platforms)
        struct IdentityHash {
            inline const size_t& operator()(const size_t& value) const { return value; }
        };

        //! Datatype for nodes
        using Node = ImplicitDaryHeapNode<Key, T>;

        //! Datatype for allocator used to allocate and deallocate nodes
        using NodeAllocator = PoolAllocator<Node, Allocator>;

        //! Datatype for node tree encoded as a level-order traversal in an array
        using NodeTree = std::vector<Node*>;

        //! Datatype for mapping of user-data hashes to the corresponding nodes
        using NodeMap = std::unordered_map<size_t, Node*, IdentityHash>;

        // Construction
        // ------------
    public:
        //! Constructor
        /*!
         * \param [in] branchingFactor Initializes \ref branchingFactor() - \copybrief branchingFactor()
         * \param [in] maximumNodeCount The (estimated) maximum count of nodes stored at the same time (exceeding this limit is possible, however, it requires internal re-allocation which can be a performance bottleneck)
         * \param [in] reserve If `true`, the heap not only configures itself for the given maximum node count but also reserves the corresponding amount of memory (equivalent to calling \ref reserve() after the constructor)
         */
        ImplicitDaryHeap(const size_t& branchingFactor, const size_t& maximumNodeCount, const bool& reserve)
            : m_branchingFactor((branchingFactor > 2) ? branchingFactor : 2)
            , m_nodeAllocator(maximumNodeCount, false, false, (reserve == true) ? maximumNodeCount : 0)
        {
            if (reserve == true) {
                m_nodeTree.reserve(maximumNodeCount);
                m_nodeMap.reserve(maximumNodeCount);
            }
        }

        //! Destructor
        ~ImplicitDaryHeap() { clear(true); }

        //! Copy constructor
        ImplicitDaryHeap(const ImplicitDaryHeap& other)
            : m_branchingFactor(other.m_branchingFactor)
            , m_nodeAllocator(other.m_nodeAllocator.objectsPerBlock(), other.m_nodeAllocator.constructObjects(), other.m_nodeAllocator.destructObjects(), other.m_nodeAllocator.capacity())
        {
            m_nodeTree.reserve(other.m_nodeTree.capacity());
            m_nodeMap.reserve(other.m_nodeTree.capacity());

            // Copy nodes
            for (size_t i = 0; i < other.m_nodeTree.size(); i++) {
                Node* newNode = m_nodeAllocator.allocateWithoutConstruction();
                newNode = new (newNode) Node(*other.m_nodeTree[i]); // Construction
                m_nodeTree.push_back(newNode);
                m_nodeMap.insert({ newNode->m_dataHash, newNode });
            }
        }

        //! Copy assignment operator
        ImplicitDaryHeap& operator=(const ImplicitDaryHeap& other)
        {
            // Clean-up and free all memory before assignment
            clear(true);

            // Set branching factor
            m_branchingFactor = other.m_branchingFactor;

            // Pre-allocate memory
            m_nodeAllocator = NodeAllocator(other.m_nodeAllocator.objectsPerBlock(), other.m_nodeAllocator.constructObjects(), other.m_nodeAllocator.destructObjects(), other.m_nodeAllocator.capacity());
            m_nodeTree.clear();
            m_nodeTree.reserve(other.m_nodeTree.capacity());
            m_nodeMap.clear();
            m_nodeMap.reserve(other.m_nodeTree.capacity());

            // Copy nodes
            for (size_t i = 0; i < other.m_nodeTree.size(); i++) {
                Node* newNode = m_nodeAllocator.allocateWithoutConstruction();
                newNode = new (newNode) Node(*other.m_nodeTree[i]); // Construction
                m_nodeTree.push_back(newNode);
                m_nodeMap.insert({ newNode->m_dataHash, newNode });
            }

            return *this;
        }

        //! Move constructor
        ImplicitDaryHeap(ImplicitDaryHeap&& other)
            : m_branchingFactor(other.m_branchingFactor)
            , m_nodeAllocator(std::move(other.m_nodeAllocator))
            , m_nodeTree(std::move(other.m_nodeTree))
            , m_nodeMap(std::move(other.m_nodeMap))
        {
            // Manual reset of source (so it can be re-used or safely destructed)
            other.m_nodeTree = NodeTree();
            other.m_nodeMap = NodeMap();
        }

        //! Move assignment operator
        ImplicitDaryHeap& operator=(ImplicitDaryHeap&& other)
        {
            // Clean-up and free all memory before assignment
            clear(true);

            // Set branching factor
            m_branchingFactor = other.m_branchingFactor;

            // Assign through move from source
            m_nodeAllocator = std::move(other.m_nodeAllocator);
            m_nodeTree = std::move(other.m_nodeTree);
            m_nodeMap = std::move(other.m_nodeMap);

            // Manual reset of source (so it can be re-used or safely destructed)
            other.m_nodeTree = NodeTree();
            other.m_nodeMap = NodeMap();

            return *this;
        }

        // Members
        // -------
    protected:
        size_t m_branchingFactor; //!< \copybrief branchingFactor()
        NodeAllocator m_nodeAllocator; //!< Allocator used to allocate and deallocate nodes
        NodeTree m_nodeTree; //!< Node tree encoded as a level-order traversal in an array
        NodeMap m_nodeMap; //!< Mapping of user-data hashes to the corresponding nodes

        // Getters
        // -------
    public:
        //! Branching factor \f$ D>1 \f$ - (maximum) count of children for nodes ("maximum": because the last level of the tree might not be complete) (use \f$ D=2 \f$ for a binary heap)
        inline const size_t& branchingFactor() const { return m_branchingFactor; }

        //! Returns `true` if the heap is empty, `false` otherwise
        inline bool empty() const { return (m_nodeTree.size() == 0); }

        //! Returns size of the heap (count of nodes)
        inline size_t size() const { return m_nodeTree.size(); }

        //! Tries to find a certain node in the heap (identified by its user-data hash)
        /*!
         * \param [in] dataHash Hash of user-data which is used to find the corresponding node in the node tree
         * \return Index of the corresponding node in the node-tree or \ref size(), if there is no corresponding node in the heap (user-data hash not found)
         *
         * \warning The index of a node in the node-tree changes whenever the tree is updated, i.e., whenever an element is added/removed/updated!
         */
        inline size_t findIndex(const size_t& dataHash) const
        {
            const auto iterator = m_nodeMap.find(dataHash);
            if (iterator != m_nodeMap.end())
                return iterator->second->m_nodeIndex;
            else
                return m_nodeTree.size();
        }

        //! Tries to find a certain node in the heap (identified by its user-data hash)
        /*!
         * \param [in] data User-data which is hashed to find the corresponding node in the node tree
         * \return Index of the corresponding node in the node-tree or \ref size(), if there is no corresponding node in the heap (user-data hash not found)
         *
         * \warning The index of a node in the node-tree changes whenever the tree is updated, i.e., whenever an element is added/removed/updated!
         */
        inline size_t findIndex(const T& data) const { return findIndex(Hash{}(data)); }

        //! Checks, if a certain node is contained in the heap (identified by its user-data hash)
        /*!
         * \param [in] dataHash Hash of user-data which is used to find the corresponding node in the node tree
         * \return `true` if a corresponding node is in the heap, `false` otherwise
         */
        inline bool contains(const size_t& dataHash) const { return findIndex(dataHash) != m_nodeTree.size(); }

        //! Checks, if a certain node is contained in the heap (identified by its user-data hash)
        /*!
         * \param [in] data User-data which is hashed to find the corresponding node in the node tree
         * \return `true` if a corresponding node is in the heap, `false` otherwise
         */
        inline bool contains(const T& data) const { return contains(Hash{}(data)); }

        //! Returns the key of a certain node **without** removing it from the heap (identified by its index in the node-tree)
        /*! \param [in] nodeIndex Index of the node in the node-tree (has to be smaller than \ref size()) */
        inline const Key& keyByIndex(const size_t& nodeIndex) const
        {
            assert(nodeIndex < m_nodeTree.size());
            return m_nodeTree[nodeIndex]->m_nodeKey;
        }

        //! Returns the user-data of a certain node **without** removing it from the heap (identified by its index in the node-tree)
        /*! \param [in] nodeIndex Index of the node in the node-tree (has to be smaller than \ref size()) */
        inline const T& dataByIndex(const size_t& nodeIndex) const
        {
            assert(nodeIndex < m_nodeTree.size());
            return *m_nodeTree[nodeIndex];
        }

        //! Returns the key of the root node (highest priority node) **without** removing it from the heap
        /*! \warning The tree has to contain at least one node (may be checked with \ref empty() before)! */
        inline const Key& rootKey() const { return keyByIndex(0); }

        //! Returns the user-data of the root node (highest priority node) **without** removing it from the heap
        /*! \warning The tree has to contain at least one node (may be checked with \ref empty() before)! */
        inline const T& rootData() const { return dataByIndex(0); }

        // Setters
        // -------
    public:
        //! Setter for \ref branchingFactor()
        /*! \warning Triggers \ref clear(), to guarantee that the heap is never in an inconsistent state! */
        inline void setBranchingFactor(const size_t& value)
        {
            const size_t projectedValue = (value > 2) ? value : 2;
            if (projectedValue != m_branchingFactor) {
                clear(false);
                m_branchingFactor = projectedValue;
            }
        }

        // Modifiers
        // ---------
    public:
        //! Clears the complete heap
        /*! \param freeMemory If `true`, all allocated memory (reserved elements) is freed. */
        inline void clear(const bool& freeMemory = false)
        {
            // Destruct nodes
            for (size_t i = 0; i < m_nodeTree.size(); i++)
                m_nodeTree[i]->~Node();

            // Free memory
            if (freeMemory == false) {
                for (size_t i = 0; i < m_nodeTree.size(); i++)
                    m_nodeAllocator.deallocateWithoutDestruction(m_nodeTree[i]); // Mark slot in the memory pool as free
                m_nodeTree.clear();
                m_nodeMap.clear();
            } else {
                m_nodeAllocator.reset();
                NodeTree().swap(m_nodeTree);
                NodeMap().swap(m_nodeMap);
            }
        }

        //! Pre-allocates memory for the given count of nodes
        /*! \param [in] count Count of nodes to allocate memory for */
        inline void reserve(const size_t& count)
        {
            m_nodeAllocator.reserve(count);
            m_nodeTree.reserve(count);
            m_nodeMap.reserve(count);
        }

        //! Inserts the given key-data pair and sorts the heap accordingly (copy-constructs data to heap)
        /*!
         * \param [in] key The key of the node to insert
         * \param [in] data The user-data linked to this node
         * \param [in] dataHash The hash of the user-data
         * \return `true` on success, `false` otherwise (if an element with the same user-data hash already exists)
         */
        inline bool insert(const Key& key, const T& data, const size_t& dataHash)
        {
            // Grow tree and map if necessary
            const size_t previousTreeSize = m_nodeTree.size();
            if (previousTreeSize == m_nodeTree.capacity()) {
                const size_t newCapacity = previousTreeSize * 2;
                m_nodeTree.reserve(newCapacity);
                m_nodeMap.reserve(newCapacity);
            }

            // Create new node and attach it to the very end of the heap
            Node* newNode = m_nodeAllocator.allocateWithoutConstruction();
            newNode = new (newNode) Node(data); // Construction (copy-construct user-data)
            newNode->m_nodeKey = key;
            newNode->m_nodeIndex = previousTreeSize;
            newNode->m_dataHash = dataHash;
            m_nodeTree.push_back(newNode);
            if (m_nodeMap.insert({ dataHash, newNode }).second == false) {
                // The same user-data hash already exists in the heap -> revert changes
                m_nodeTree.pop_back();
                newNode->~Node(); // Destruction
                m_nodeAllocator.deallocateWithoutDestruction(newNode);
                return false;
            }

            // Sort heap (move new node up)
            heapifyUp(newNode);

            // Success
            return true;
        }

        //! Inserts the given key-data pair and sorts the heap accordingly (copy-constructs data to heap)
        /*!
         * \param [in] key The key of the node to insert
         * \param [in] data The user-data linked to this node
         * \return `true` on success, `false` otherwise (if an element with the same user-data hash already exists)
         */
        inline bool insert(const Key& key, const T& data) { return insert(key, data, Hash{}(data)); }

        //! Removes a certain node from the heap (identified by its index in the node-tree)
        /*! \param [in] nodeIndex Index of the node in the node-tree (has to be smaller than \ref size()) */
        inline void removeByIndex(const size_t& nodeIndex)
        {
            assert(nodeIndex < m_nodeTree.size());
            remove(m_nodeTree[nodeIndex]);
        }

        //! Remove root node (highest priority node) from the heap
        /*! \warning The tree has to contain at least one node (may be checked with \ref empty() before)! */
        inline void removeRoot() { removeByIndex(0); }

        //! Returns the user-data of the root node (highest priority node, copy-constructs data from heap) **and** removes it from the heap
        /*! \warning The tree has to contain at least one node (may be checked with \ref empty() before)! */
        inline T extractRoot()
        {
            assert(!empty());
            Node* rootNode = m_nodeTree.front();
            T returnedObject(*rootNode); // Copy-construct
            remove(rootNode);
            return returnedObject;
        }

        //! Removes a certain node from the heap (identified by its user-data hash)
        /*!
         * \param [in] dataHash Hash of user-data which is used to find the corresponding node in the node tree
         * \return `true` on success, `false` otherwise (if there is no node with the given user-data hash)
         */
        inline bool remove(const size_t& dataHash)
        {
            const auto searchResult = m_nodeMap.find(dataHash);
            if (searchResult == m_nodeMap.end())
                return false; // Not found
            remove(searchResult->second, false);
            m_nodeMap.erase(searchResult);
            return true;
        }

        //! Removes a certain node from the heap (identified by its user-data hash)
        /*!
         * \param [in] data User-data which is hashed to find the corresponding node in the node tree
         * \return `true` on success, `false` otherwise (if there is no node with the given user-data hash)
         */
        inline bool remove(const T& data) { return remove(Hash{}(data)); }

        //! Updates the key of a certain node in the heap (identified by its index in the node-tree)
        /*!
         * \param [in] newKey The new key of the node
         * \param [in] nodeIndex Index of the node in the node-tree (has to be smaller than \ref size())
         */
        inline bool updateKeyByIndex(const Key& newKey, const size_t& nodeIndex)
        {
            assert(nodeIndex < m_nodeTree.size());
            Node* node = m_nodeTree[nodeIndex];

            // Check, if priority is increased
            if (KeyCompare{}(newKey, node->m_nodeKey)) {
                node->m_nodeKey = newKey;
                heapifyUp(node);
            }
            // Check, if priority is decreased
            else if (KeyCompare{}(node->m_nodeKey, newKey)) {
                node->m_nodeKey = newKey;
                heapifyDown(node);
            }
            // else: same priority -> same key -> nothing to do

            // Success
            return true;
        }

        //! Updates the key of a certain node in the heap (identified by its user-data hash)
        /*!
         * \param [in] newKey The new key of the node
         * \param [in] dataHash Hash of user-data which is used to find the corresponding node in the node tree
         * \return `true` on success, `false` otherwise (if there is no node with the given user-data hash)
         */
        inline bool updateKey(const Key& newKey, const size_t& dataHash)
        {
            const size_t nodeIndex = findIndex(dataHash);
            if (nodeIndex == m_nodeTree.size())
                return false; // Not found
            else
                return updateKeyByIndex(newKey, nodeIndex);
        }

        //! Updates the key of a certain node in the heap (identified by its user-data hash)
        /*!
         * \param [in] newKey The new key of the node
         * \param [in] data User-data which is hashed to find the corresponding node in the node tree
         * \return `true` on success, `false` otherwise (if there is no node with the given user-data hash)
         */
        inline bool updateKey(const Key& newKey, const T& data) { return updateKey(newKey, Hash{}(data)); }

        // Helpers
        // -------
    protected:
        //! Computes the tree index of the parent node linked to the given child node
        inline size_t parentNodeIndex(const size_t& childNodeIndex) { return (childNodeIndex - 1) / m_branchingFactor; }

        //! Computes the tree index of the first child node linked to the given parent node
        inline size_t firstChildNodeIndex(const size_t& parentNodeIndex) { return parentNodeIndex * m_branchingFactor + 1; }

        //! Swaps the location of two nodes in the tree
        inline void swapNodes(Node* firstNode, Node* secondNode)
        {
            std::swap(firstNode->m_nodeIndex, secondNode->m_nodeIndex);
            m_nodeTree[firstNode->m_nodeIndex] = firstNode;
            m_nodeTree[secondNode->m_nodeIndex] = secondNode;
        }

        //! Moves a node, which is potentially at a lower position in the tree than it should be, to the correct location
        /*! \param [in] node The node to be moved */
        inline void heapifyUp(Node* node)
        {
            while (node->m_nodeIndex > 0) {
                Node* parentNode = m_nodeTree[parentNodeIndex(node->m_nodeIndex)];
                if (KeyCompare{}(node->m_nodeKey, parentNode->m_nodeKey))
                    swapNodes(node, parentNode);
                else
                    return;
            }
        }

        //! Moves a node, which is potentially at a higher position in the tree than it should be, to the correct location
        /*! \param [in] node The node to be moved */
        inline void heapifyDown(Node* node)
        {
            const size_t DminusOne = m_branchingFactor - 1;
            const size_t lastNodeIndex = m_nodeTree.size() - 1;
            while (true) {
                // Get corresponding first child node
                const size_t currentFirstChildNodeIndex = firstChildNodeIndex(node->m_nodeIndex);
                if (currentFirstChildNodeIndex > lastNodeIndex)
                    return; // No child nodes -> stop

                // Get corresponding last child node
                const size_t currentLastChildNodeIndex = std::min(currentFirstChildNodeIndex + DminusOne, lastNodeIndex);

                // Get child node with highest priority
                size_t currentHighestPriorityChildNodeIndex = currentFirstChildNodeIndex;
                for (size_t i = currentFirstChildNodeIndex + 1; i <= currentLastChildNodeIndex; i++) {
                    if (KeyCompare{}(m_nodeTree[i]->m_nodeKey, m_nodeTree[currentHighestPriorityChildNodeIndex]->m_nodeKey))
                        currentHighestPriorityChildNodeIndex = i;
                }

                // Swap, if child node has higher priority than source node
                Node* currentHighestPriorityChildNode = m_nodeTree[currentHighestPriorityChildNodeIndex];
                if (KeyCompare{}(currentHighestPriorityChildNode->m_nodeKey, node->m_nodeKey))
                    swapNodes(currentHighestPriorityChildNode, node);
                else
                    return;
            }
        }

        //! Removes the given node from the heap (also destructs and deallocates linked user-data)
        /*!
         * \param [in] node The node to be removed
         * \param [in] removeFromNodeMap If `true` the node is also removed from the nodemap, otherwise not (e.g. if the it is removed from the node map manually)
         */
        inline void remove(Node* node, const bool& removeFromNodeMap = true)
        {
            // Check, if this is the last node in the tree
            Node* lastNode = m_nodeTree.back();
            if (node == lastNode) {
                // ...yes, last node -> just resize tree
                m_nodeTree.pop_back();
            } else {
                // ...no, not last node -> swap with last node, then resize tree and move (previously) last node down
                swapNodes(node, lastNode);
                m_nodeTree.pop_back();
                heapifyDown(lastNode);
            }

            // Remove node from map
            if (removeFromNodeMap == true)
                m_nodeMap.erase(node->m_dataHash);

            // Destruct and deallocate
            node->~Node(); // Destruction
            m_nodeAllocator.deallocateWithoutDestruction(node);
        }
    };

    //! Implicit d-ary min-heap (minimum key is root node)
    /*! See \ref ImplicitDaryHeap for an explanation of the template parameters. */
    template <typename Key, typename T, typename Hash = std::hash<T>, typename Allocator = std::allocator<ImplicitDaryHeapNode<Key, T>>>
    using ImplicitDaryMinHeap = ImplicitDaryHeap<Key, std::less<Key>, T, Hash, Allocator>;

    //! Implicit d-ary max-heap (maximum key is root node)
    /*! See \ref ImplicitDaryHeap for an explanation of the template parameters. */
    template <typename Key, typename T, typename Hash = std::hash<T>, typename Allocator = std::allocator<ImplicitDaryHeapNode<Key, T>>>
    using ImplicitDaryMaxHeap = ImplicitDaryHeap<Key, std::greater<Key>, T, Hash, Allocator>;
} // namespace memory
} // namespace broccoli
