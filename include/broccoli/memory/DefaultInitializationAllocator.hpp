/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

#include <memory>

namespace broccoli {
namespace memory {
    //! Adaptor for allocators which changes value initialization to default initialization
    /*!
     * \ingroup broccoli_memory
     *
     * This adaptor modifies the given `BaseAllocator` such that it uses default initialization instead of value initialization.
     *
     * As an example, consider calling `resize(n)` on a `std::vector<int>` which creates a vector of `n` integers which are "value
     * initialized". For fundamental types such as `int` this means that all elements are zero-initialized. For large vectors
     * this is a significant performance bottleneck if it is not necessary to set all elements to zero (e.g. because the elements
     * get overwritten anyways).
     *
     * In contrast, calling `resize(n)` on a `std::vector<int, DefaultInitializationAllocator<int>>` will create a vector of the
     * same size, however, where all elements are "default initialized". For fundamental types such as `int` this means that the
     * values remain uninitialized.
     *
     * \tparam T Type of elements to allocate
     * \tparam BaseAllocator Base allocator on which this adaptor is applied (also supports `Eigen::aligned_allocator`)
     *
     * References
     * ----------
     * See https://en.cppreference.com/w/cpp/named_req/DefaultInsertable
     * "If value-initialization is undesirable, for example, if the object is of non-class type and zeroing out is not needed, it
     * can be avoided by providing a custom Allocator::construct." (attached link)
     */
    template <typename T, typename BaseAllocator = std::allocator<T>>
    class DefaultInitializationAllocator : public BaseAllocator {
    public:
        //! Rebind struct to get an equivalent allocator for a different element type
        template <typename U>
        struct rebind {
            using other = DefaultInitializationAllocator<U, typename std::allocator_traits<BaseAllocator>::template rebind_alloc<U>>;
        };

        //! Constructs an object of type `T` in allocated uninitialized storage pointed to by `p` (default initialized)
        template <typename U>
        void construct(U* p) noexcept(std::is_nothrow_default_constructible<U>::value) { ::new (static_cast<void*>(p)) U; }

        //! Constructs an object of type `T` in allocated uninitialized storage pointed to by `p` (passes arguments to base allocator)
        template <typename U, typename... Args>
        void construct(U* p, Args&&... args) { std::allocator_traits<BaseAllocator>::construct(static_cast<BaseAllocator&>(*this), p, std::forward<Args>(args)...); }
    };
} // namespace memory
} // namespace broccoli
