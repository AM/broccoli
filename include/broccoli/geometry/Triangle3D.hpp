/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "TriangleND.hpp"

namespace broccoli {
namespace geometry {
    //! Representation of a triangle in three-dimensional space
    /*!
     * \ingroup broccoli_geometry
     *
     * Provides various auxillary parameters used to speedup intersection and distance tests.
     * For details on vertex/edge indexing and general notation, please see the base class \ref broccoli::geometry::TriangleND().
     */
    class Triangle3D : public TriangleND<3> {
        // Type definition
        // ---------------
    public:
        //! Container for result of a ray-triangle intersection test
        struct RayIntersectionResult {
            bool m_intersection; //!< Flag indicating, if the ray intersects the triangle
            BarycentricCoordinates m_intersectionPointCoordinates; //!< Barycentric coordinates of intersection point (**Warning:** Indeterminate if there is no intersection)
            double m_rayPosition; //!< Position of intersection point "on ray" (IntersectionPoint = RayOrigin + RayPosition * RayDirection) (**Warning:** Indeterminate if there is no intersection)
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
        };

        //! Container for result of a point-triangle distance test
        struct PointDistanceResult {
            BarycentricCoordinates m_projectedPointCoordinates; //!< Barycentric coordinates of point projected to triangle plane (along triangle normal)
            double m_distanceToTrianglePlane; //!< Signed distance from point to triangle plane (positive along triangle normal)
            BarycentricCoordinates m_closestPointCoordinates; //!< Barycentric coordinates of closest point on triangle
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
        };

        // Construction
        // ------------
    public:
        //! Specialized constructor (full specification of triangle)
        /*!
         * \param [in] v0 Initializes \ref m_v0 - \copybrief m_v0
         * \param [in] v1 Initializes \ref m_v1 - \copybrief m_v1
         * \param [in] v2 Initializes \ref m_v2 - \copybrief m_v2
         */
        Triangle3D(const Eigen::Vector3d& v0, const Eigen::Vector3d& v1, const Eigen::Vector3d& v2)
            : TriangleND(v0, v1, v2)
        {
            // Abort, if triangle is degenerated
            if (m_degenerated == true)
                return;

            // Compute coordinate transformation matrix D = [e0, e1, n]
            Eigen::Matrix3d D;
            D << m_e0(0), m_e1(0), m_n(0), //
                m_e0(1), m_e1(1), m_n(1), //
                m_e0(2), m_e1(2), m_n(2);
            m_invD = D.inverse();
        }

        // Operators
        // ---------
    public:
        //! Comparison operator: **equality**
        inline bool operator==(const Triangle3D& reference) const { return TriangleND::operator==(reference); }

        //! Comparison operator: **inequality**
        inline bool operator!=(const Triangle3D& reference) const { return !(*this == reference); }

        // Members
        // -------
    protected:
        // Auxillary parameters
        Eigen::Matrix3d m_invD; //!< \copybrief invD()

        // Getters
        // -------
    public:
        // Auxillary parameters
        //! Inverse of matrix \f$ D = [e_0,\, e_1,\, n] \f$ which allows for efficient point-triangle distance evaluation (**Warning:** unset if triangle is degenerated!)
        inline const Eigen::Matrix3d& invD() const
        {
            assert(!m_degenerated);
            return m_invD;
        }

        // Setters
        // -------
    public:
        //! Rotates the triangle (re-computes only parameters which need an update)
        inline void rotate(const Eigen::Matrix3d& rotation)
        {
            // Update base parameters
            TriangleND::rotate(rotation);

            // Update D-matrix
            m_invD = (m_invD * rotation.transpose()).eval();
        }

        // Intersection and distance tests
        // -------------------------------
    public:
        //! Performs a ray-triangle intersection test
        /*!
         * \warning The triangle must not be degenerated! This should be checked with \ref degenerated() in advance!
         *
         * The implementation uses the method from Moeller and Trumbore 1997 (non-culling branch).
         *
         * \param [in] rayOrigin Origin of ray in 3d space
         * \param [in] rayDirection Direction of ray in 3d space (not necessarily normalized but must not be zero-vector)
         * \return Data container for results of the test
         *
         * References
         * ----------
         * T. Moeller and B. Trumbore, "Fast, Minimum Storage Ray-Triangle Intersection", Journal of Graphics Tools, vol. 2, no. 1, pp. 21–28, 1997. doi: 10.1080/10867651.1997.10487468.
         */
        inline RayIntersectionResult evaluateRayIntersection(const Eigen::Vector3d& rayOrigin, const Eigen::Vector3d& rayDirection) const
        {
            assert(!m_degenerated);

            // Initialize result
            RayIntersectionResult result;
            double& u = result.m_intersectionPointCoordinates.u();
            double& v = result.m_intersectionPointCoordinates.v();

            // Compute intersection
            const Eigen::Vector3d pvec = rayDirection.cross(m_e1);
            const double det = m_e0.dot(pvec);
            if (std::fabs(det) <= m_epsilon) {
                // Ray is parallel to triangle -> no intersection
                result.m_intersection = false;
                return result;
            }
            const double inv_det = 1.0 / det;
            const Eigen::Vector3d tvec = rayOrigin - m_v0;
            u = tvec.dot(pvec) * inv_det;
            if (u < 0.0 || u > 1.0) {
                // Intersection point is outside of triangle -> no intersection
                result.m_intersection = false;
                return result;
            }
            const Eigen::Vector3d qvec = tvec.cross(m_e0);
            v = rayDirection.dot(qvec) * inv_det;
            if (v < 0.0 || u + v > 1.0) {
                // Intersection point is outside of triangle -> no intersection
                result.m_intersection = false;
                return result;
            }
            result.m_intersectionPointCoordinates.updateW();
            result.m_intersection = true;
            result.m_rayPosition = m_e1.dot(qvec) * inv_det;

            // Pass back result
            return result;
        }

        //! Performs a point-triangle distance test
        /*!
         * \warning The triangle must not be degenerated! This should be checked with \ref degenerated() in advance!
         *
         * Point-Triangle Distance
         * -----------------------
         * \verbatim
         * x ...custom point in 3D space
         * p ...point x projected to triangle plane (may be outside of triangle!)
         * d ...(signed) distance from x to triangle plane (positive along triangle normal)
         *
         * p = x - d*n = v0 + u*e0 + v*e1
         * thus
         * u*e0 + v*e1 + d*n = x - v0
         * with
         * D = [e0, e1, n]
         * we can compute u, v and d through solving
         * D * [u, v, d]^T = x - v0
         * \endverbatim
         *
         * \param [in] point The position of the point in 3d space
         * \return Data container for results of the test
         */
        inline PointDistanceResult evaluatePointDistance(const Eigen::Vector3d& point) const
        {
            assert(!m_degenerated);

            // Initialize result
            PointDistanceResult result;

            // Compute u, v (barycentric coordinates) of projected point p and d (distance of point to triangle plane)
            result.m_projectedPointCoordinates.uvw() = m_invD * (point - m_v0); // d is temporarily stored in w
            result.m_distanceToTrianglePlane = result.m_projectedPointCoordinates.w(); // Copy d from w to correct variable
            result.m_projectedPointCoordinates.updateW(); // Re-compute w to get valid barycentric coordinates

            // Compute coordinates of closest point
            result.m_closestPointCoordinates = projectToTriangle(point, result.m_projectedPointCoordinates);

            // Pass back result
            return result;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace geometry
} // namespace broccoli

#endif // HAVE_EIGEN3
