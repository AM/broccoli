/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "../core/floats.hpp"
#include "../core/math.hpp"
#include <Eigen/Dense>
#include <assert.h>

namespace broccoli {
namespace geometry {
    //! Representation of a triangle in n-dimensional space
    /*!
     * \ingroup broccoli_geometry
     *
     * Provides various auxillary parameters used to speedup intersection and distance tests.
     *
     * Barycentric Coordinates
     * -----------------------
     * \verbatim
     * u + v + w = 1
     * thus
     * u = 1 - v - w
     * v = 1 - u - w
     * w = 1 - u - v
     *
     *                  \    u<0   /
     *                   \   v>0  /
     *                    \  w<0 /
     *                     \    /
     *                      \  /
     *                       \/
     *                       v2
     *                      /|\
     *                     / | \
     *                    /  |  \
     *       u<0         /   |   \     u>0
     *       v>0        /    |    \    v>0
     *       w>0       /     |     \   w<0
     *                /      |      \
     *              e1  u    |    w  e2
     *              /        p        \
     *             /       xx xx       \
     *            /     xxx     xxx     \
     *           /    xx           xx    \
     *          /  xxx       v       xxx  \
     *         / xx                     xx \
     * -------v0------------e0-------------v1-------
     * u<0   /              u>0             \   u>0
     * v<0  /               v<0              \  v<0
     * w>0 /                w>0               \ w<0
     *
     * Special cases:
     *  * u>0, v>0, w>0 --> inside triangle
     *  * u<0, v<0, w<0 --> not possible since u + v + w = 1!
     *
     * p ...custom point on triangle plane (does NOT have to lie within triangle)
     * t = p - v0 ...vector from v0 to p
     * e0 = v1 - v0
     * e1 = v2 - v0
     * e2 = v2 - v1
     *
     * A = area(v0, v1, v2) = 1/2 * |(e0 x e1)|
     * u = area(v0, p, v2) / A
     *   = (1/2 * |(t x e1)|) / A
     *   = |(t x e1)| / |(e0 x e1)|
     * v = area(v0, v1, p) / A
     *   = (1/2 * |(e0 x t|) / A
     *   = |(e0 x t)| / |(e0 x e1)|
     * w = area(v1, v2, p) / A
     *   = 1 - u - v
     *
     * p = w*v0 + u*v1 + v*v2 = v0 + u*e0 + v*e1
     * \endverbatim
     */
    template <unsigned int N>
    class TriangleND {
        static_assert(N == 2 || N == 3, "TriangleND only supports two- and three-dimensional spaces!");

        // Type definitions
        // ----------------
    public:
        //! Type of N-dimensional vectors (vertices, edges)
        using VectorND = Eigen::Matrix<double, N, 1>;

        //! Barycentric coordinates
        class BarycentricCoordinates {
            // Construction
            // ------------
        public:
            //! Default constructor
            BarycentricCoordinates() = default;

            //! Specialized constructor
            /*!
             * \param [in] u Initializes \ref u() - \copybrief u()
             * \param [in] v Initializes \ref v() - \copybrief v()
             * \param [in] w Initializes \ref w() - \copybrief w()
             */
            BarycentricCoordinates(const double& u, const double& v, const double& w)
                : m_uvw(u, v, w)
            {
            }

            // Operators
            // ---------
        public:
            //! Comparison operator: **equality**
            inline bool operator==(const BarycentricCoordinates& reference) const
            {
                return m_uvw.x() == reference.m_uvw.x() && //
                    m_uvw.y() == reference.m_uvw.y() && //
                    m_uvw.z() == reference.m_uvw.z();
            }

            //! Comparison operator: **inequality**
            inline bool operator!=(const BarycentricCoordinates& reference) const { return !(*this == reference); }

            //! Checks, if the set of coordinates is valid
            inline bool isValid(const double& tolerance = 1e-9) const { return fabs(m_uvw.x() + m_uvw.y() + m_uvw.z() - 1.0) <= tolerance; }

            // Members
            // -------
        protected:
            Eigen::Vector3d m_uvw; //!< Barycentric coordinates \f$ [u,\,v,\,w]^T \f$

            // Getters
            // -------
        public:
            //! Direct access to vector of components
            inline const Eigen::Vector3d& uvw() const { return m_uvw; }
            //! \copydoc uvw() const
            inline Eigen::Vector3d& uvw() { return m_uvw; }
            //! Barycentric coordinate \f$ u = area(v0, p, v2) / A \f$
            inline const double& u() const { return m_uvw.x(); }
            //! \copydoc u() const
            inline double& u() { return m_uvw.x(); }
            //! Barycentric coordinate \f$ v = area(v0, v1, p) / A \f$
            inline const double& v() const { return m_uvw.y(); }
            //! \copydoc v() const
            inline double& v() { return m_uvw.y(); }
            //! Barycentric coordinate \f$ w = area(v1, v2, p) / A \f$
            inline const double& w() const { return m_uvw.z(); }
            //! \copydoc w() const
            inline double& w() { return m_uvw.z(); }

            // Setters
            // -------
        public:
            //! Updates \f$ u \f$ from \f$ v \f$ and \f$ w \f$
            inline void updateU() { m_uvw.x() = 1.0 - m_uvw.y() - m_uvw.z(); }
            //! Updates \f$ v \f$ from \f$ u \f$ and \f$ w \f$
            inline void updateV() { m_uvw.y() = 1.0 - m_uvw.x() - m_uvw.z(); }
            //! Updates \f$ w \f$ from \f$ u \f$ and \f$ v \f$
            inline void updateW() { m_uvw.z() = 1.0 - m_uvw.x() - m_uvw.y(); }
            //! Setter for all three coordinates
            inline void setUVW(const double& u, const double& v, const double& w)
            {
                m_uvw.x() = u;
                m_uvw.y() = v;
                m_uvw.z() = w;
            }
            //! Setter for coordinates \f$ u \f$ and \f$ v \f$ (computes \f$ w \f$)
            inline void setUV(const double& u, const double& v)
            {
                m_uvw.x() = u;
                m_uvw.y() = v;
                updateW();
            }
            //! Setter for coordinates \f$ u \f$ and \f$ w \f$ (computes \f$ v \f$)
            inline void setUW(const double& u, const double& w)
            {
                m_uvw.x() = u;
                m_uvw.z() = w;
                updateV();
            }
            //! Setter for coordinates \f$ v \f$ and \f$ w \f$ (computes \f$ u \f$)
            inline void setVW(const double& v, const double& w)
            {
                m_uvw.y() = v;
                m_uvw.z() = w;
                updateU();
            }

        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
        };

        // Construction
        // ------------
    public:
        //! Specialized constructor (full specification of triangle)
        /*!
         * \param [in] v0 Initializes \ref m_v0 - \copybrief m_v0
         * \param [in] v1 Initializes \ref m_v1 - \copybrief m_v1
         * \param [in] v2 Initializes \ref m_v2 - \copybrief m_v2
         */
        TriangleND(const VectorND& v0, const VectorND& v1, const VectorND& v2)
            : m_v0(v0)
            , m_v1(v1)
            , m_v2(v2)
        {
            // Computes edges
            computeEdges();

            // Compute dot-product of edges
            computeEdgeDotProducts();

            // Compute area, detect degeneration and compute normal
            processAreaDegeneratedNormal(m_e0, m_e1);

            // Compute bounding box
            computeBoundingBox();
        }

        // Operators
        // ---------
    public:
        //! Comparison operator: **equality**
        inline bool operator==(const TriangleND& reference) const
        {
            // Only compare vertices (all other members are auto-generated)
            if (!core::isEqual(m_v0, reference.m_v0) || !core::isEqual(m_v1, reference.m_v1) || !core::isEqual(m_v2, reference.m_v2))
                return false;
            return true;
        }

        //! Comparison operator: **inequality**
        inline bool operator!=(const TriangleND& reference) const { return !(*this == reference); }

        // Members
        // -------
    protected:
        // General
        static constexpr double m_epsilon = 1e-9; //!< Floating-point threshold for close-to-zero detection

        // Input parameters
        VectorND m_v0; //!< \copybrief v0()
        VectorND m_v1; //!< \copybrief v1()
        VectorND m_v2; //!< \copybrief v2()

        // Auxillary parameters
        VectorND m_e0; //!< \copybrief e0()
        VectorND m_e1; //!< \copybrief e1()
        VectorND m_e2; //!< \copybrief e2()
        double m_e0_dot_e0; //!< \copybrief e0_dot_e0()
        double m_e1_dot_e1; //!< \copybrief e1_dot_e1()
        double m_e2_dot_e2; //!< \copybrief e2_dot_e2()
        double m_e0_dot_e1; //!< \copybrief e0_dot_e1()
        double m_e0_dot_e2; //!< \copybrief e0_dot_e2()
        double m_e1_dot_e2; //!< \copybrief e1_dot_e2()
        double m_area; //!< \copybrief area()
        bool m_degenerated; //!< \copybrief degenerated()
        Eigen::Vector3d m_n; //!< \copybrief n()

        // Bounding box
        VectorND m_boundingBoxMinimum; //!< \copybrief boundingBoxMinimum()
        VectorND m_boundingBoxMaximum; //!< \copybrief boundingBoxMaximum()

        // Getters
        // -------
    public:
        // Input parameters
        //! First vertex of triangle \f$ v_0 \f$
        inline const VectorND& v0() const { return m_v0; }
        //! Second vertex of triangle \f$ v_1 \f$
        inline const VectorND& v1() const { return m_v1; }
        //! Third vertex of triangle \f$ v_2 \f$
        inline const VectorND& v2() const { return m_v2; }

        // Auxillary parameters
        //! First edge of triangle \f$ e_0 = v_1 - v_0 \f$
        inline const VectorND& e0() const { return m_e0; }
        //! Second edge of triangle \f$ e_1 = v_2 - v_0 \f$
        inline const VectorND& e1() const { return m_e1; }
        //! Third edge of triangle \f$ e_2 = v_2 - v_1 \f$
        inline const VectorND& e2() const { return m_e2; }
        //! Dot-product \f$ e_0 * e_0 \f$
        inline const double& e0_dot_e0() const { return m_e0_dot_e0; }
        //! Dot-product \f$ e_1 * e_1 \f$
        inline const double& e1_dot_e1() const { return m_e1_dot_e1; }
        //! Dot-product \f$ e_2 * e_2 \f$
        inline const double& e2_dot_e2() const { return m_e2_dot_e2; }
        //! Dot-product \f$ e_0 * e_1 \f$
        inline const double& e0_dot_e1() const { return m_e0_dot_e1; }
        //! Dot-product \f$ e_0 * e_2 \f$
        inline const double& e0_dot_e2() const { return m_e0_dot_e2; }
        //! Dot-product \f$ e_1 * e_2 \f$
        inline const double& e1_dot_e2() const { return m_e1_dot_e2; }
        //! Area covered by this triangle (signed for N=2)
        inline const double& area() const { return m_area; }
        //! Flag indicating, if this triangle is degenerated (degraded to line or point)
        inline const bool& degenerated() const { return m_degenerated; }
        //! Triangle normal \f$ n \f$ (computed by right-hand rule) (**Warning:** unset if triangle is degenerated!)
        inline const Eigen::Vector3d& n() const
        {
            assert(!m_degenerated);
            return m_n;
        }

        // Bounding box
        //! Minimum of (axis-aligned) bounding box
        inline const VectorND& boundingBoxMinimum() const { return m_boundingBoxMinimum; }
        //! Maximum of (axis-aligned) bounding box
        inline const VectorND& boundingBoxMaximum() const { return m_boundingBoxMaximum; }

        // Setters
        // -------
    public:
        //! Translates the triangle (re-computes only parameters which need an update)
        inline void translate(const VectorND& translation)
        {
            // Update input parameters
            m_v0 += translation;
            m_v1 += translation;
            m_v2 += translation;

            // Update bounding box
            m_boundingBoxMinimum += translation;
            m_boundingBoxMaximum += translation;
        }

        //! Rotates the triangle (re-computes only parameters which need an update)
        inline void rotate(const Eigen::Matrix<double, N, N>& rotation)
        {
            // Update input parameters
            m_v0 = (rotation * m_v0).eval();
            m_v1 = (rotation * m_v1).eval();
            m_v2 = (rotation * m_v2).eval();

            // Update edges
            computeEdges();

            // Update normal
            rotateNormal(rotation);

            // Update bounding box
            computeBoundingBox();
        }

        // Projection
        // ----------
    public:
        //! Projects the given point to the triangle (shortest euclidean distance)
        /*!
         * \param [in] point Arbitrary point (may lie outside of triangle plane)
         * \param [in] coordinates Barycentric coordinates of the projection of this point onto the triangle plane (along triangle normal)
         * \return Barycentric coordinates of the projected point (lies on triangle border or inside of triangle)
         */
        inline BarycentricCoordinates projectToTriangle(const VectorND& point, const BarycentricCoordinates& coordinates) const
        {
            // Initialize result
            BarycentricCoordinates result;

            // Check for all possible cases (see drawing in description of this class)
            if (coordinates.u() >= 0.0) {
                // u>=0
                if (coordinates.v() >= 0.0) {
                    // u>=0, v>=0
                    if (coordinates.w() >= 0.0) {
                        // u>=0, v>=0, w>=0 --> inside triangle
                        result = coordinates;
                    } else {
                        // u>=0, v>=0, w<0 --> on edge 2
                        result.setVW(core::math::clamp(m_e2.dot(point - m_v1) / m_e2_dot_e2, 0.0, 1.0), 0.0);
                    }
                } else {
                    // u>=0, v<0
                    if (coordinates.w() >= 0.0) {
                        // u>=0, v<0, w>=0 --> on edge 0
                        result.setUV(core::math::clamp(m_e0.dot(point - m_v0) / m_e0_dot_e0, 0.0, 1.0), 0.0);
                    } else {
                        // u>=0, v<0, w<0 --> outside triangle, close to v1 --> check if triangle is obtuse (corner angle > 90deg)
                        if (m_e0_dot_e2 > 0.0) {
                            // ...yes -> angle > 90deg -> distinct between further cases
                            const VectorND p_minus_v1 = point - m_v1;
                            const double p_minus_v1_dot_e0 = p_minus_v1.dot(m_e0);
                            if (p_minus_v1_dot_e0 < 0.0) {
                                // On edge 0
                                result.setVW(0.0, core::math::clamp(-p_minus_v1_dot_e0 / m_e0_dot_e0, 0.0, 1.0));
                            } else {
                                const double p_minus_v1_dot_e2 = p_minus_v1.dot(m_e2);
                                if (p_minus_v1_dot_e2 > 0.0) {
                                    // On edge 2
                                    result.setVW(core::math::clamp(p_minus_v1_dot_e2 / m_e2_dot_e2, 0.0, 1.0), 0.0);
                                } else {
                                    // On vertex 1
                                    result.setUVW(1.0, 0.0, 0.0);
                                }
                            }
                        } else {
                            // On vertex 1
                            result.setUVW(1.0, 0.0, 0.0);
                        }
                    }
                }
            } else {
                // u<0
                if (coordinates.v() >= 0.0) {
                    // u<0, v>=0
                    if (coordinates.w() >= 0.0) {
                        // u<0, v>=0, w>=0 --> on edge 1
                        result.setUV(0.0, core::math::clamp(m_e1.dot(point - m_v0) / m_e1_dot_e1, 0.0, 1.0));
                    } else {
                        // u<0, v>=0, w<0 -> outside triangle, close to v2 --> check if triangle is obtuse (corner angle > 90deg)
                        if (m_e1_dot_e2 < 0.0) {
                            // ...yes -> angle > 90deg -> distinct between further cases
                            const VectorND p_minus_v2 = point - m_v2;
                            const double p_minus_v2_dot_e1 = p_minus_v2.dot(m_e1);
                            if (p_minus_v2_dot_e1 < 0.0) {
                                // On edge 1
                                result.setUW(0.0, core::math::clamp(-p_minus_v2_dot_e1 / m_e1_dot_e1, 0.0, 1.0));
                            } else {
                                const double p_minus_v2_dot_e2 = p_minus_v2.dot(m_e2);
                                if (p_minus_v2_dot_e2 < 0.0) {
                                    // On edge 2
                                    result.setUW(core::math::clamp(-p_minus_v2_dot_e2 / m_e2_dot_e2, 0.0, 1.0), 0.0);
                                } else {
                                    // On vertex 2
                                    result.setUVW(0.0, 1.0, 0.0);
                                }
                            }
                        } else {
                            // On vertex 2
                            result.setUVW(0.0, 1.0, 0.0);
                        }
                    }
                } else {
                    // u<0, v<0
                    if (coordinates.w() >= 0.0) {
                        // u<0, v<0, w>=0 --> outside triangle, close to v0 --> check if triangle is obtuse (corner angle > 90deg)
                        if (m_e0_dot_e1 < 0.0) {
                            // ...yes -> angle > 90deg -> distinct between further cases
                            const VectorND p_minus_v0 = point - m_v0;
                            const double p_minus_v0_dot_e0 = p_minus_v0.dot(m_e0);
                            if (p_minus_v0_dot_e0 > 0.0) {
                                // On edge 0
                                result.setUV(core::math::clamp(p_minus_v0_dot_e0 / m_e0_dot_e0, 0.0, 1.0), 0.0);
                            } else {
                                const double p_minus_v0_dot_e1 = p_minus_v0.dot(m_e1);
                                if (p_minus_v0_dot_e1 > 0.0) {
                                    // On edge 1
                                    result.setUV(0.0, core::math::clamp(p_minus_v0_dot_e1 / m_e1_dot_e1, 0.0, 1.0));
                                } else {
                                    // On vertex 0
                                    result.setUVW(0.0, 0.0, 1.0);
                                }
                            }
                        } else {
                            // On vertex 0
                            result.setUVW(0.0, 0.0, 1.0);
                        }
                    } else {
                        // u<0, v<0, w<0 -> not possible since u + v + w = 1! --> just pass back input (no projection)
                        assert(false);
                        result = coordinates;
                    }
                }
            }

            // Pass back result
            return result;
        }

        // Interpolation / extrapolation
        // -----------------------------
    public:
        //! Interpolates the given vertex attributes for a certain point
        /*!
         * \param [in] coordinates Barycentric coordinates of the point to interpolate (extrapolation if outside of triangle)
         * \param [in] x0 Vertex attribute of vertex \f$ v_0 \f$
         * \param [in] x1 Vertex attribute of vertex \f$ v_1 \f$
         * \param [in] x2 Vertex attribute of vertex \f$ v_2 \f$
         * \return Interpolated or extrapolated attribute
         */
        template <typename T>
        static inline T interpolate(const BarycentricCoordinates& coordinates, const T& x0, const T& x1, const T& x2) { return coordinates.w() * x0 + coordinates.u() * x1 + coordinates.v() * x2; }

        //! Interpolates the given vertex attributes for a certain point (with deltas from first vertex)
        /*!
         * \param [in] coordinates Barycentric coordinates of the point to interpolate (extrapolation if outside of triangle)
         * \param [in] x0 Vertex attribute of vertex \f$ v_0 \f$
         * \param [in] x1_minus_x0 Vertex attribute of vertex \f$ v_1 \f$ minus vertex attribute of vertex \f$ v_0 \f$
         * \param [in] x2_minus_x0 Vertex attribute of vertex \f$ v_2 \f$ minus vertex attribute of vertex \f$ v_0 \f$
         * \return Interpolated or extrapolated attribute
         */
        template <typename T>
        static inline T interpolateFromDelta(const BarycentricCoordinates& coordinates, const T& x0, const T& x1_minus_x0, const T& x2_minus_x0) { return x0 + coordinates.u() * x1_minus_x0 + coordinates.v() * x2_minus_x0; }

        //! Computes the position of a point from its Barycentric coordinates
        /*!
         * \param [in] coordinates Barycentric coordinates of the point to interpolate (extrapolation if outside of triangle)
         * \return The corresponding n-d position of the point
         */
        inline VectorND interpolatePoint(const BarycentricCoordinates& coordinates) const { return interpolateFromDelta(coordinates, m_v0, m_e0, m_e1); }

        // Helpers
        // -------
    protected:
        //! Re-computes the edges
        inline void computeEdges()
        {
            m_e0 = m_v1 - m_v0;
            m_e1 = m_v2 - m_v0;
            m_e2 = m_v2 - m_v1;
        }

        //! Re-computes dot-products of edges
        inline void computeEdgeDotProducts()
        {
            m_e0_dot_e0 = m_e0.dot(m_e0);
            m_e1_dot_e1 = m_e1.dot(m_e1);
            m_e2_dot_e2 = m_e2.dot(m_e2);
            m_e0_dot_e1 = m_e0.dot(m_e1);
            m_e0_dot_e2 = m_e0.dot(m_e2);
            m_e1_dot_e2 = m_e1.dot(m_e2);
        }

        //! Processes \ref area(), \ref degenerated(), and \ref n() for 2-dimensional triangles
        inline void processAreaDegeneratedNormal(const Eigen::Vector2d& e0, const Eigen::Vector2d& e1)
        {
            static const Eigen::Vector3d normalUp = Eigen::Vector3d::UnitZ();
            static const Eigen::Vector3d normalDown = -Eigen::Vector3d::UnitZ();
            const double e0_cross_e1 = e0.x() * e1.y() - e0.y() * e1.x();
            const double norm_e0_cross_e1 = fabs(e0_cross_e1);
            m_area = e0_cross_e1 / 2.0;
            m_degenerated = norm_e0_cross_e1 < m_epsilon;
            if (m_degenerated == false)
                m_n = (e0_cross_e1 > 0) ? normalUp : normalDown;
        }

        //! Processes \ref area(), \ref degenerated(), and \ref n() for 3-dimensional triangles
        inline void processAreaDegeneratedNormal(const Eigen::Vector3d& e0, const Eigen::Vector3d& e1)
        {
            const Eigen::Vector3d e0_cross_e1 = e0.cross(e1);
            const double norm_e0_cross_e1 = e0_cross_e1.norm();
            m_area = norm_e0_cross_e1 / 2.0;
            m_degenerated = norm_e0_cross_e1 < m_epsilon;
            if (m_degenerated == false)
                m_n = e0_cross_e1 / norm_e0_cross_e1;
        }

        //! Rotates the normal for 2-dimensional triangles (no-op)
        inline void rotateNormal(const Eigen::Matrix<double, 2, 2>&) {}

        //! Rotates the normal for 3-dimensional triangles
        inline void rotateNormal(const Eigen::Matrix<double, 3, 3>& rotation) { m_n = (rotation * m_n).eval(); }

        //! Re-computes the (axis-aligned) bounding box
        inline void computeBoundingBox()
        {
            m_boundingBoxMinimum = m_v0;
            m_boundingBoxMaximum = m_v0;
            for (Eigen::Index i = 0; i < N; i++) {
                if (m_v1[i] < m_boundingBoxMinimum[i])
                    m_boundingBoxMinimum[i] = m_v1[i];
                if (m_v1[i] > m_boundingBoxMaximum[i])
                    m_boundingBoxMaximum[i] = m_v1[i];
                if (m_v2[i] < m_boundingBoxMinimum[i])
                    m_boundingBoxMinimum[i] = m_v2[i];
                if (m_v2[i] > m_boundingBoxMaximum[i])
                    m_boundingBoxMaximum[i] = m_v2[i];
            }
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace geometry
} // namespace broccoli

#endif // HAVE_EIGEN3
