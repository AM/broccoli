/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "TriangleND.hpp"

namespace broccoli {
namespace geometry {
    //! Representation of a triangle in two-dimensional space
    /*!
     * \ingroup broccoli_geometry
     *
     * Provides various auxillary parameters used to speedup intersection and distance tests.
     * For details on vertex/edge indexing and general notation, please see the base class \ref broccoli::geometry::TriangleND().
     */
    class Triangle2D : public TriangleND<2> {
        // Type definition
        // ---------------
    public:
        //! Container for result of a point-triangle intersection test
        struct PointIntersectionResult {
            bool m_intersection; //!< Flag indicating, if the point intersects the triangle
            BarycentricCoordinates m_intersectionPointCoordinates; //!< Barycentric coordinates of intersection point (available, even if there is no intersection)
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
        };

        //! Container for result of a point-triangle distance test
        struct PointDistanceResult {
            BarycentricCoordinates m_pointCoordinates; //!< Barycentric coordinates of input point
            BarycentricCoordinates m_closestPointCoordinates; //!< Barycentric coordinates of closest point on triangle
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
        };

        // Construction
        // ------------
    public:
        //! Specialized constructor (full specification of triangle)
        /*!
         * \param [in] v0 Initializes \ref m_v0 - \copybrief m_v0
         * \param [in] v1 Initializes \ref m_v1 - \copybrief m_v1
         * \param [in] v2 Initializes \ref m_v2 - \copybrief m_v2
         */
        Triangle2D(const Eigen::Vector2d& v0, const Eigen::Vector2d& v1, const Eigen::Vector2d& v2)
            : TriangleND(v0, v1, v2)
        {
            // Abort, if triangle is degenerated
            if (m_degenerated == true)
                return;

            // Compute coordinate transformation matrix D = [e0, e1]
            Eigen::Matrix2d D;
            D << m_e0(0), m_e1(0), //
                m_e0(1), m_e1(1);
            m_invD = D.inverse();
        }

        // Operators
        // ---------
    public:
        //! Comparison operator: **equality**
        inline bool operator==(const Triangle2D& reference) const { return TriangleND::operator==(reference); }

        //! Comparison operator: **inequality**
        inline bool operator!=(const Triangle2D& reference) const { return !(*this == reference); }

        // Members
        // -------
    protected:
        // Auxillary parameters
        Eigen::Matrix2d m_invD; //!< \copybrief invD()

        // Getters
        // -------
    public:
        // Auxillary parameters
        //! Flag indicating, if the vertices are ordered in a clockwise order
        inline bool clockwise() const { return m_area < 0.0; }
        //! Flag indicating, if the vertices are ordered in a counter-clockwise order
        inline bool counterClockwise() const { return m_area >= 0.0; }
        //! Inverse of matrix \f$ D = [e_0,\, e_1] \f$ which allows for efficient point-triangle intersection/distance evaluation (**Warning:** unset if triangle is degenerated!)
        inline const Eigen::Matrix2d& invD() const
        {
            assert(!m_degenerated);
            return m_invD;
        }

        // Setters
        // -------
    public:
        //! Rotates the triangle (re-computes only parameters which need an update)
        inline void rotate(const Eigen::Matrix2d& rotation)
        {
            // Update base parameters
            TriangleND::rotate(rotation);

            // Update D-matrix
            m_invD = (m_invD * rotation.transpose()).eval();
        }

        // Intersection and distance tests
        // -------------------------------
    public:
        //! Performs a point-triangle intersection test
        /*!
         * \warning The triangle must not be degenerated! This should be checked with \ref degenerated() in advance!
         *
         * Point-Triangle Intersection
         * ---------------------------
         * \verbatim
         * p ...point to test intersection for
         *
         * p = v0 + u*e0 + v*e1
         * thus
         *  u*e0 + v*e1 = p - v0
         * with
         * D = [e0, e1]
         * we can compute u and v through solving
         * D * [u, v]^T = p - v0
         * \endverbatim
         *
         * \param [in] point The position of the point in 2d space
         * \return Data container for results of the test
         */
        inline PointIntersectionResult evaluatePointIntersection(const Eigen::Vector2d& point) const
        {
            assert(!m_degenerated);

            // Initialize result
            PointIntersectionResult result;

            // Compute u, v (barycentric coordinates) of point
            result.m_intersectionPointCoordinates.uvw().head<2>() = m_invD * (point - m_v0);
            result.m_intersectionPointCoordinates.updateW();

            // Check, if point lies inside or outside of triangle
            if (result.m_intersectionPointCoordinates.u() >= 0.0 && result.m_intersectionPointCoordinates.v() >= 0.0 && result.m_intersectionPointCoordinates.w() >= 0.0)
                result.m_intersection = true;
            else
                result.m_intersection = false;

            // Pass back result
            return result;
        }

        //! Performs a point-triangle distance test
        /*!
         * \warning The triangle must not be degenerated! This should be checked with \ref degenerated() in advance!
         *
         * Point-Triangle Distance
         * -----------------------
         * \verbatim
         * p ...point for which a closest point on the triangle should be computed
         *
         * p = v0 + u*e0 + v*e1
         * thus
         *  u*e0 + v*e1 = p - v0
         * with
         * D = [e0, e1]
         * we can compute u and v through solving
         * D * [u, v]^T = p - v0
         * \endverbatim
         *
         * \param [in] point The position of the point in 2d space
         * \return Data container for results of the test
         */
        inline PointDistanceResult evaluatePointDistance(const Eigen::Vector2d& point) const
        {
            assert(!m_degenerated);

            // Initialize result
            PointDistanceResult result;

            // Compute u, v (barycentric coordinates) of point
            result.m_pointCoordinates.uvw().head<2>() = m_invD * (point - m_v0);
            result.m_pointCoordinates.updateW();

            // Compute coordinates of closest point
            result.m_closestPointCoordinates = projectToTriangle(point, result.m_pointCoordinates);

            // Pass back result
            return result;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace geometry
} // namespace broccoli

#endif // HAVE_EIGEN3
