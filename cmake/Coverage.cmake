#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

#
# Enables coverage generation
#

set(OPTION_COVERAGE_ENABLED OFF)

# configure a target for coverage data generation
function(generate_coverage_data target)

    if (NOT ${OPTION_COVERAGE_ENABLED})
        return()
    endif()

    if (NOT TARGET coverage)
        add_custom_target(coverage)

        set_target_properties(coverage
            PROPERTIES
            FOLDER "Maintenance"
            EXCLUDE_FROM_DEFAULT_BUILD 1
        )
    endif()

    # create target for running the executable with LLVM_PROFILE_FILE var
    add_custom_target(run-${target}
        COMMAND LLVM_PROFILE_FILE=${target}.profraw $<TARGET_FILE:${target}>
    )

    add_dependencies(coverage run-${target})
endfunction()

# generate a named report with llvm-cov for the given list of target arguments
function(generate_coverage_report name)

    if (NOT ${OPTION_COVERAGE_ENABLED})
        return()
    endif()

    # build argument lists with all profdata files and targets
    set(PROFDATA_ARGS "")
    set(OBJECT_ARGS "")
    foreach (target ${ARGN})

        set(PROFDATA_ARGS ${PROFDATA_ARGS} ${target}.profraw)

        if (NOT OBJECT_ARGS)
            set(OBJECT_ARGS
                "$<TARGET_FILE:${target}>"
            )
        else()
            set(OBJECT_ARGS
                ${OBJECT_ARGS} -object=$<TARGET_FILE:${target}>
            )
        endif()


    endforeach()

    # merge the files
    add_custom_command(TARGET coverage POST_BUILD
        COMMAND ${llvm-profdata_EXECUTABLE} merge -sparse -o ${name}-coverage.profdata ${PROFDATA_ARGS}
    )

    # create line-oriented coverage report
    add_custom_command(TARGET coverage POST_BUILD
        COMMAND ${llvm-cov_EXECUTABLE} report ${OBJECT_ARGS} -instr-profile ${name}-coverage.profdata ${CMAKE_SOURCE_DIR}/include
    )

    # create line-oriented coverage report (html)
    add_custom_command(TARGET coverage POST_BUILD
        COMMAND ${llvm-cov_EXECUTABLE} show ${OBJECT_ARGS} -instr-profile ${name}-coverage.profdata ${CMAKE_SOURCE_DIR}/include -format=html -output-dir=${CMAKE_BINARY_DIR}/coverage -project-title=${META_PROJECT_NAME}
    )

endfunction()

# Enable or disable coverage
function(enable_coverage status)
    if(NOT ${status})
        set(OPTION_COVERAGE_ENABLED ${status} PARENT_SCOPE)
        message(STATUS "Coverage skipped: Manually disabled")

        return()
    endif()

    find_package(llvm-cov)
    find_package(llvm-profdata)

    if(NOT llvm-cov_FOUND)
        set(OPTION_COVERAGE_ENABLED OFF PARENT_SCOPE)
        message(STATUS "Coverage skipped: llvm-cov not found")

        return()
    endif()

    if(NOT llvm-profdata_FOUND)
        set(OPTION_COVERAGE_ENABLED OFF PARENT_SCOPE)
        message(STATUS "Coverage skipped: llvm-profdata not found")

        return()
    endif()

    set(OPTION_COVERAGE_ENABLED ${status} PARENT_SCOPE)
    message(STATUS "Coverage report enabled")
endfunction()
