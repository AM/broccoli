# Toolchain file for QNX 7.0 x86-64

# cmake prior to 3.14 does not detect
# cxx features correctly
cmake_minimum_required(VERSION 3.14 FATAL_ERROR)

set(CMAKE_SYSTEM_NAME QNX)

set(arch gcc_ntox86_64)

set(CMAKE_C_COMPILER qcc)
set(CMAKE_C_COMPILER_TARGET ${arch})
set(CMAKE_CXX_COMPILER q++)
set(CMAKE_CXX_COMPILER_TARGET ${arch})

# activate avx for the target
add_compile_options(-Y _cxx -mavx -mavx2)

# set find_package root to toolchain root dir
set(CMAKE_FIND_ROOT_PATH /opt/qnx700/target/qnx7/x86_64 /opt/qnx700/target/qnx7/)

# Debug and Release flags
# Uses Level-3 Optimization for Release-type QNX toolchains (default)
SET(CMAKE_C_FLAGS_DEBUG "-g")
SET(CMAKE_C_FLAGS_MINSIZEREL "-Os -DNDEBUG")
SET(CMAKE_C_FLAGS_RELEASE "-O3 -DNDEBUG")
SET(CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 -g")

SET(CMAKE_CXX_FLAGS_DEBUG "-g")
SET(CMAKE_CXX_FLAGS_MINSIZEREL "-Os -DNDEBUG")
SET(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")
SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g")
