#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

# sdl component of broccoli

if (broccoli_FIND_REQUIRED AND broccoli_FIND_REQUIRED_sdl)
    find_package(SDL2 REQUIRED)
else()
    find_package(SDL2 QUIET)
endif()

if (SDL2_FOUND)
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_COMPILE_DEFINITIONS HAVE_SDL2)
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES
            $<BUILD_INTERFACE:${SDL2_INCLUDE_DIRS}>
            )
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_LINK_LIBRARIES ${SDL2_LIBRARIES})
else()
    if (broccoli_FIND_REQUIRED_sdl)
        # if sdl is required set broccoli found to false if it is not found
        set(broccoli_FOUND False)
    endif()
endif()
