#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

# Findllvm-cov results:
# llvm-cov_FOUND
# llvm-cov_EXECUTABLE

include(FindPackageHandleStandardArgs)

# work around CMP0053, see http://public.kitware.com/pipermail/cmake/2014-November/059117.html
set(PROGRAMFILES_x86_ENV "PROGRAMFILES(x86)")

find_program(llvm-cov_EXECUTABLE
    NAMES
        llvm-cov
    PATHS
        "${LLVM-COV_DIR}"
        "$ENV{LLVM-COV_DIR}"
        "$ENV{PROGRAMFILES}/llvm-cov"
        "$ENV{${PROGRAMFILES_x86_ENV}}/llvm-cov"
)

find_package_handle_standard_args(llvm-cov
	FOUND_VAR
        llvm-cov_FOUND
    REQUIRED_VARS
        llvm-cov_EXECUTABLE
)

mark_as_advanced(llvm-cov_EXECUTABLE)
