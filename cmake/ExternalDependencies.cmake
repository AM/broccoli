#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

include(ConfigureTimeDependency)

# We download our own copy of Eigen and do not rely on system's eigen
add_configure_time_dependency(eigen)

# Make sure broccoli can be found
set(broccoli_DIR "${CMAKE_SOURCE_DIR}")
