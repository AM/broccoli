#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

# Findllvm-profdata results:
# llvm-profdata_FOUND
# llvm-profdata_EXECUTABLE

include(FindPackageHandleStandardArgs)

# work around CMP0053, see http://public.kitware.com/pipermail/cmake/2014-November/059117.html
set(PROGRAMFILES_x86_ENV "PROGRAMFILES(x86)")

find_program(llvm-profdata_EXECUTABLE
    NAMES
        llvm-profdata
    PATHS
        "${LLVM-profdata_DIR}"
        "$ENV{LLVM-profdata_DIR}"
        "$ENV{PROGRAMFILES}/llvm-profdata"
        "$ENV{${PROGRAMFILES_x86_ENV}}/llvm-profdata"
)

find_package_handle_standard_args(llvm-profdata
	FOUND_VAR
        llvm-profdata_FOUND
    REQUIRED_VARS
        llvm-profdata_EXECUTABLE
)

mark_as_advanced(llvm-profdata_EXECUTABLE)
