#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

# zlib component of broccoli

if (broccoli_FIND_REQUIRED AND broccoli_FIND_REQUIRED_zlib)
    find_package(ZLIB REQUIRED)
else()
    find_package(ZLIB QUIET)
endif()

if (ZLIB_FOUND)
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_COMPILE_DEFINITIONS HAVE_ZLIB)
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES
            $<BUILD_INTERFACE:${ZLIB_INCLUDE_DIRS}>
            )
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_LINK_LIBRARIES ${ZLIB_LIBRARIES})
else()
    if (broccoli_FIND_REQUIRED_zlib)
        # if zlib is required set broccoli found to false if it is not found
        set(broccoli_FOUND False)
    endif()
endif()
