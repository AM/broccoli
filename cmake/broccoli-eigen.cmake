#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

# eigen component of broccoli

if (broccoli_FIND_REQUIRED AND broccoli_FIND_REQUIRED_eigen)
    find_package(Eigen3 REQUIRED)
else()
    find_package(Eigen3 QUIET)
endif()

if (Eigen3_FOUND)
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_COMPILE_DEFINITIONS HAVE_EIGEN3)
    set_property(TARGET ${_broccoli_imported_target_name} APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES
            $<BUILD_INTERFACE:${EIGEN3_INCLUDE_DIRS}>
            )
else()
    if (broccoli_FIND_REQUIRED_eigen)
        # if eigen is required set broccoli found to false if it is not found
        set(broccoli_FOUND False)
    endif()
endif()
