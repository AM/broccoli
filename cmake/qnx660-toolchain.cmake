# Toolchain file for QNX 6.6 x86

set(CMAKE_SYSTEM_NAME QNX)

set(arch gcc_ntox86)

set(CMAKE_C_COMPILER qcc)
set(CMAKE_C_COMPILER_TARGET ${arch})
set(CMAKE_CXX_COMPILER QCC)
set(CMAKE_CXX_COMPILER_TARGET ${arch})

# We know that QNX 6.6 supports C++11
set(CMAKE_CXX_COMPILE_FEATURES "cxx_std_11")

# we make an educated guess for the instruction sets of the target
# and we use the GNU standard library

set(CMAKE_CXX_FLAGS_INIT "-std=gnu++11 -Y _gpp -mavx -mfma")
set(CMAKE_C_FLAGS_INIT "-Y _gpp -mavx -mfma")

# Debug and Release flags
# Uses Level-3 Optimization for Release-type QNX toolchains (default)
SET(CMAKE_C_FLAGS_DEBUG "-g")
SET(CMAKE_C_FLAGS_MINSIZEREL "-Os -DNDEBUG")
SET(CMAKE_C_FLAGS_RELEASE "-O3 -DNDEBUG")
SET(CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 -g")

SET(CMAKE_CXX_FLAGS_DEBUG "-g")
SET(CMAKE_CXX_FLAGS_MINSIZEREL "-Os -DNDEBUG")
SET(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")
SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g")
