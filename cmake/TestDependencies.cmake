#
# This file is part of broccoli.
# Copyright (c) 2019 Chair of Applied Mechanics, Technical University of Munich
# https://www.amm.mw.tum.de/
#

include(ConfigureTimeDependency)

# Include all dependencies required for testing

###############################################
# Google Test target
###############################################

# Get google test
add_configure_time_dependency(googletest)

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_BINARY_DIR}/googletest-src
                 ${CMAKE_BINARY_DIR}/googletest-build
                 EXCLUDE_FROM_ALL)

# The gtest/gtest_main targets carry header search path
# dependencies automatically when using CMake 2.8.11 or
# later. Otherwise we have to add them here ourselves.
if (CMAKE_VERSION VERSION_LESS 2.8.11)
  include_directories("${gtest_SOURCE_DIR}/include")
endif()