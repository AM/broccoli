# Adds function to check target with clang-tidy

if(CMAKE_VERSION VERSION_GREATER 3.6)
    # Add clang-tidy if available
    option(OPTION_CLANG_TIDY_FIX "Perform fixes for Clang-Tidy" OFF)
    find_program(
            CLANG_TIDY_EXE
            NAMES "clang-tidy"
            DOC "Path to clang-tidy executable"
    )

    if(CLANG_TIDY_EXE)
        if(OPTION_CLANG_TIDY_FIX)
            set(DO_CLANG_TIDY "${CLANG_TIDY_EXE}" "-fix")
        else()
            set(DO_CLANG_TIDY "${CLANG_TIDY_EXE}")
        endif()
    endif()
endif()

function(clang_tidy_target target)

    if (DO_CLANG_TIDY)
        set_property(TARGET ${target} PROPERTY CXX_CLANG_TIDY ${DO_CLANG_TIDY})
    endif()

endfunction()