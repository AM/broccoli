/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include <broccoli/io/network/NetworkSocket.hpp>
#include <broccoli/io/serialization/SerializableDataString.hpp>
#include <iostream>

using namespace broccoli;
using namespace io;

void NetworkSocketCommunication(const SerializableDataString m_sendString)
{
    // Set up sockets
    NetworkSocket<SerializableDataString, SerializableDataString> sendSocket(TCPServerOptions("sendSocket", 5005, true, 0, 0.001, false, 10, 10));
    NetworkSocket<SerializableDataString, SerializableDataString> receiveSocket(TCPClientOptions("receiveSocket", "localhost", 5005, true, 0, 0.001, false, 10, 10));

    // Initialize sockets
    sendSocket.initialize();
    receiveSocket.initialize();

    // Wait to allow establishing connection
    core::Time::sleep(0.03);

    // Send data
    sendSocket.m_objectsToSend.push(m_sendString);

    // Wait to give sending/receiving some time
    core::Time::sleep(0.02);

    // Receive data
    SerializableDataString m_receivedString;
    receiveSocket.m_receivedObjects.pop(m_receivedString);

    // Output user information
    std::cout << "\nReceived message: " << m_receivedString.m_string;

    // Deinitialize sockets
    sendSocket.deInitialize();
    receiveSocket.deInitialize();
}

int main()
{
    // Message to send
    SerializableDataString m_sendString;
    m_sendString.m_string = "Hello World!";

    // Output user information
    std::cout << "Tutorial Network Socket (TCP)" << std::endl;
    std::cout << "=============================" << std::endl;
    std::cout << "Message to send: " << m_sendString.m_string << std::endl;

    // Run communication
    NetworkSocketCommunication(m_sendString);

    // Exit program
    return 0;
}
