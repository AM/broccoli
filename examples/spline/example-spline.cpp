/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include <fstream>
#include <iostream>
#include <string>

// Include headers from broccoli libary
#include <broccoli/curve/splines/PolynomialSpline.hpp>

using namespace broccoli;
using namespace curve;

//! Generic function to plot a spline
/*!
 * \tparam EvaluatableObjectType spline type to plot
 * \param [in] evaluatableObject spline to plot
 * \param [in] xmin Minimum x-value
 * \param [in] xmax Maximum x-value
 * \param [in] stepSize Step size for sampling the x-range
 * \param [in] maximumDerivation Maximum derivation to be plotted
 * \param [in] fileName Filename of output file(s)
 */
template <class EvaluatableObjectType>
inline void plotGeneric(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "plot '" << fileName << ".dat' using 1:2 w l title \"Base function\"\n";
    for (unsigned int d = 1; d <= maximumDerivation; d++)
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (2 + d) << " w l title \"Derivative of order " << d << "\"\n";
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    const size_t steps = std::floor((xmax - xmin) / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = xmin + i * stepSize;
        dataFile << x << " ";

        // Evaluate base function
        dataFile << evaluatableObject.evaluate(x, 0);

        // Evaluate derivative
        for (unsigned int d = 1; d <= maximumDerivation; d++)
            dataFile << " " << evaluatableObject.evaluate(x, d);

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Tutorial for polynomial splines
/*! \param [in] quintic If `true`, quintic spline interpolation is used, otherwise cubic spline interpolation */
void polynomialSplineTutorial(const bool& quintic)
{
    const unsigned int degree = 5; // Degree of the polynomial spline
    PolynomialSpline<degree> spline1; // Create polynomial spline (degree of the polynomial as template parameter)

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the spline is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    const unsigned int derivationOrderEvaluateD0toDN = 6; // Derivation order for the function evaluateD0toDN()
    unsigned int derivationOrderPlot = 1; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    double xminPlot = 0; // Starting position of the plot
    double xmaxPlot = 1; // End position of the plot
    std::string fileName = "PolynomialSpline"; // File in which the plot is written

    // Declaration and assigning parameters to the polynomial spline
    // -------------------------------------------------------------
    std::vector<double> segmentProportions = { 0.2, 0.3, 0.5 }; // Declare the length of the spline segments, the sum of all segments has to be 1
    SplineResult result; // Stores detailed error description
    if (quintic == true) {
        // Quintic spline interpolation
        std::vector<double> interpolationParameters = { 1, 2, -0.5, 0.5, 0, 2, 0.5, -1 }; // Interpolation parameters for POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES interpolation (s0(x=0), s0(x=1)=s1(x=0), s1(x=1)=s2(x=0), s2(x=1), s0'(x=0), s2'(x=1), s0''(x=0), s2''(x=1))
        spline1.interpolate(interpolationParameters, &segmentProportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result); // Calculating the spline by interpolation method "POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES" with the before declared parameters, other interpolation methods can be found in the documentation
        std::cout << "Polynomial spline with " << spline1.m_segments.size() << " segments created by POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES interpolation with the parameters:" << std::endl;
        std::cout << "s0(x=0) = " << interpolationParameters[0] << std::endl;
        std::cout << "s0(x=1) = s1(x=0) = " << interpolationParameters[1] << std::endl;
        std::cout << "s1(x=1) = s2(x=0) = " << interpolationParameters[2] << std::endl;
        std::cout << "s2(x=1) = " << interpolationParameters[3] << std::endl;
        std::cout << "s0'(x=0) = " << interpolationParameters[4] << std::endl;
        std::cout << "s2'(x=1) = " << interpolationParameters[5] << std::endl;
        std::cout << "s0''(x=0) = " << interpolationParameters[6] << std::endl;
        std::cout << "s2''(x=1) = " << interpolationParameters[7] << std::endl
                  << std::endl;
    } else {
        // Cubic spline interpolation
        std::vector<double> interpolationParameters = { 0, 1, 2, 3, 2, 0 }; // Interpolation parameters for POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES interpolation (s0(x=0), s0(x=1)=s1(x=0), s1(x=1)=s2(x=0), s2(x=1), s0''(x=0), s2''(x=1))
        spline1.interpolate(interpolationParameters, &segmentProportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &result); // Calculating the spline by interpolation method "POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES" with the before declared parameters, other interpolation methods can be found in the documentation
        std::cout << "Polynomial spline with " << spline1.m_segments.size() << " segments created by POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES interpolation with the parameters:" << std::endl;
        std::cout << "s0(x=0) = " << interpolationParameters[0] << std::endl;
        std::cout << "s0(x=1) = s1(x=0) = " << interpolationParameters[1] << std::endl;
        std::cout << "s1(x=1) = s2(x=0) = " << interpolationParameters[2] << std::endl;
        std::cout << "s2(x=1) = " << interpolationParameters[3] << std::endl;
        std::cout << "s0''(x=0) = " << interpolationParameters[4] << std::endl;
        std::cout << "s2''(x=1) = " << interpolationParameters[5] << std::endl
                  << std::endl;
    }
    std::cout << "underlying polynomial curves:" << std::endl;
    for (unsigned int i = 0; i < spline1.m_segments.size(); i++) {
        std::cout << "s" << i << "(x) = " << spline1.m_segments[i].m_coefficients[0];
        for (unsigned int j = 1; j <= spline1.m_segments[i].degree(); j++) {
            std::cout << " + " << spline1.m_segments[i].m_coefficients[j] << "*x^" << j; // Output of underlying polynomial curve
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    // Output of spline parameters
    // ---------------------------
    std::cout << "Verification if the spline was implemented correctly (1 if true): " << spline1.isValid() << std::endl; // Shows if the spline was implemented correctly
    std::cout << "degree = " << spline1.degree() << std::endl; // Shows the maximum degree of the underlying polynomials
    std::cout << "order = " << spline1.order() << std::endl
              << std::endl; // Shows the order of the underlying polynomials

    // Operations regarding spline segments
    // ------------------------------------
    auto segmentBoundaries = spline1.getSegmentBoundaries(); // Stores the spline boundaries in a vector
    std::cout << "Number of spline segments: " << spline1.m_segments.size() << std::endl; // Shows number of spline segments
    for (unsigned int i = 0; i < segmentBoundaries.size() - 1; i++)
        std::cout << "Boundaries of spline segment " << i << " : " << segmentBoundaries[i] << " to " << segmentBoundaries[i + 1] << std::endl; // Shows the boundaries of the spline segments
    std::cout << "Segment index at x = " << position << ": " << spline1.getSegmentIndex(position) << std::endl; // Shows the index of the spline segment at the position x
    std::cout << "Empty spline segments: " << spline1.removeEmptySegments() << std::endl
              << std::endl; // Shows the number of empty spline segments and deletes them

    // Evaluation methods of the spline
    // --------------------------------
    double resultEvaluate = spline1.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the spline at the position x
    std::cout << "Evaluation of the spline" << std::endl;
    std::cout << "y(" << position << ") = " << resultEvaluate << std::endl
              << std::endl;
    std::cout << "Evaluation of derivatives D0 to D" << derivationOrderEvaluateD0toDN << std::endl;
    auto resultEvaluateD0toDN = spline1.evaluateD0ToDN<derivationOrderEvaluateD0toDN>(position); // Evaluates the derivatives 0 to N of the spline at the position x
    for (unsigned int i = 0; i <= derivationOrderEvaluateD0toDN; i++)
        std::cout << "d^" << i << "y/dx^" << i << " = " << resultEvaluateD0toDN[i] << std::endl; // Shows the values of the before evaluated derivatives
    std::cout << std::endl;

    // Plot the spline
    // --------------
    plotGeneric(spline1, xminPlot, xmaxPlot, stepSizePlot, derivationOrderPlot, fileName); //Writes the plot files
    std::cout << "The spline was succesfully plotted. To view the plot start gnuplot in your build repository via the terminal and execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

int main()
{
    const bool quintic = true; // `false` for POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, `true` for POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES

    // Tutorial for polynomial spline
    polynomialSplineTutorial(quintic);

    // Quit with success
    return 0;
}
