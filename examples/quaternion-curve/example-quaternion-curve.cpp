/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include <fstream>
#include <iostream>
#include <string>

// Include headers from broccoli libary
#include <broccoli/curve/curves/QuaternionBezierCurve.hpp>
#include <broccoli/curve/curves/QuaternionLERPCurve.hpp>
#include <broccoli/curve/curves/QuaternionNLERPCurve.hpp>
#include <broccoli/curve/curves/QuaternionSLERPCurve.hpp>
#include <broccoli/curve/curves/QuaternionSQUADCurve.hpp>
#include <broccoli/curve/splines/QuaternionBSplineSpline.hpp>

using namespace broccoli;
using namespace curve;

//! Function for plotting quaternion objects with corresponding evaluate function in 2D (using gnuplot)
/*!
 * \tparam EvaluatableObjectType quaternion curve type to plot
 * \param [in] evaluatableObject quaternion curve to plot
 * \param [in] xmin Minimum x-value
 * \param [in] xmax Maximum x-value
 * \param [in] stepSize Step size for sampling the x-range
 * \param [in] maximumDerivation Maximum derivation to be plotted
 * \param [in] fileName Filename of output file(s)
 */
template <class EvaluatableObjectType>
inline void plotGenericQuaternion2D(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "plot '" << fileName << ".dat' using 1:" << (1 + 1) << " w l title \" q_w\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 2) << " w l title \" q_x\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 3) << " w l title \" q_y\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4) << " w l title \" q_z\"\n";
    for (unsigned int d = 1; d <= maximumDerivation; d++) {
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 1) << " w l title \"D^" << d << " q_w\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 2) << " w l title \"D^" << d << " q_x\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 3) << " w l title \"D^" << d << " q_y\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 4) << " w l title \"D^" << d << " q_z\"\n";
    }
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    const size_t steps = std::floor((xmax - xmin) / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = xmin + i * stepSize;
        dataFile << x << " ";

        // Evaluate value and derivatives
        std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> Di;
        size_t maximumDerivationToEvaluate = maximumDerivation;
        Di.resize(1 + maximumDerivationToEvaluate);
        for (unsigned int d = 0; d <= maximumDerivationToEvaluate; d++)
            Di[d] = evaluatableObject.evaluate(x, d);

        // Write value and derivatives
        for (unsigned int d = 0; d <= maximumDerivation; d++) {
            dataFile << " " << Di[d].w();
            dataFile << " " << Di[d].x();
            dataFile << " " << Di[d].y();
            dataFile << " " << Di[d].z();
        }

        dataFile << "\n";
    }
    dataFile.close();
}

//! Function for plotting quaternion curves in 2D (using gnuplot)
template <class CurveType>
inline void plotQuaternionCurve2D(const CurveType& curve, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    plotGenericQuaternion2D<CurveType>(curve, 0, 1, stepSize, maximumDerivation, fileName);
}

//! Function for plotting quaternion splines in 2D (using gnuplot)
template <class SplineType>
inline void plotQuaternionSpline2D(const SplineType& spline, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    plotGenericQuaternion2D<SplineType>(spline, 0, 1, stepSize, maximumDerivation, fileName);
}

//! Function for plotting quaternion objects with corresponding evaluate function in 3D (using gnuplot)
/*!
 * \tparam EvaluatableObjectType quaternion curve type to plot
 * \param [in] evaluatableObject quaternion curve to plot
 * \param [in] xmin Minimum x-value
 * \param [in] xmax Maximum x-value
 * \param [in] stepSize Step size for sampling the x-range
 * \param [in] maximumDerivation Maximum derivation to be plotted
 * \param [in] fileName Filename of output file(s)
 */
template <class EvaluatableObjectType>
inline void plotGenericQuaternion3D(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const double& stepSize, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "set style line 1 linecolor rgb '#0065BD' linetype 1 linewidth 1\n";
    gnuplotFile << "set style line 2 linecolor rgb '#E37222' linetype 1 linewidth 1\n";
    gnuplotFile << "set style line 3 linecolor rgb '#A2AD00' linetype 1 linewidth 1\n";
    gnuplotFile << "\n";
    gnuplotFile << "splot '" << fileName << ".dat' using 2:3:4 with lines linestyle 1 title \"x-axis\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 5:6:7 with lines linestyle 2 title \"y-axis\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 8:9:10 with lines linestyle 3 title \"z-axis\"\n";
    gnuplotFile << "set view equal xyz\n";
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    const size_t steps = std::floor((xmax - xmin) / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = xmin + i * stepSize;
        dataFile << x << " ";

        // Evaluate value
        Eigen::Quaterniond point = evaluatableObject.evaluate(x, 0);

        // Compute x-axis
        Eigen::Quaterniond xAxis = point * Eigen::Quaterniond(0, 1, 0, 0) * point.conjugate();
        dataFile << " " << xAxis.x() << " " << xAxis.y() << " " << xAxis.z();

        // Compute y-axis
        Eigen::Quaterniond yAxis = point * Eigen::Quaterniond(0, 0, 1, 0) * point.conjugate();
        dataFile << " " << yAxis.x() << " " << yAxis.y() << " " << yAxis.z();

        // Compute z-axis
        Eigen::Quaterniond zAxis = point * Eigen::Quaterniond(0, 0, 0, 1) * point.conjugate();
        dataFile << " " << zAxis.x() << " " << zAxis.y() << " " << zAxis.z();

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Function for plotting quaternion curves in 3D (using gnuplot)
template <class CurveType>
inline void plotQuaternionCurve3D(const CurveType& curve, const double& stepSize, const std::string& fileName)
{
    plotGenericQuaternion3D<CurveType>(curve, 0, 1, stepSize, fileName);
}

//! Function for plotting quaternion splines in 3D (using gnuplot)
template <class SplineType>
inline void plotQuaternionSpline3D(const SplineType& spline, const double& stepSize, const std::string& fileName)
{
    plotGenericQuaternion3D<SplineType>(spline, 0, 1, stepSize, fileName);
}

//! Tutorial for quaternion LERP curves
void quaternionLERPCurveTutorial()
{
    // Creation of quaternion by Linear intERPolation between start-quaternion and end-quaternion
    // ------------------------------------------------------------------------------------------
    QuaternionLERPCurve quaternionLERPCurve1; // Create quaternion LERP Curve with default constructor. The control points are not yet assigned
    QuaternionLERPCurve quaternionLERPCurve2(Eigen::Quaterniond(0, 1, 0, 0), Eigen::Quaterniond(0, 0, 1, 0)); // Create quaternion LERP Curve with specialized constructor. The control points are assigned directly

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the quaternion curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    unsigned int derivationOrderPlot = 0; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    std::string fileName = "QuaternionLERPCurve2d"; // File in which the 2d plot is written
    std::string fileName3d = "QuaternionLERPCurve3d"; // File in which the 3d plot is written

    // Assigning control points to the quaternion LERP Curve
    // -----------------------------------------------------
    quaternionLERPCurve1.m_controlPoints[0] = Eigen::Quaterniond(0, 1, 0, 0); // Assign quaternion q0 at the beginning of the curve
    quaternionLERPCurve1.m_controlPoints[1] = Eigen::Quaterniond(0, 0, 1, 0); // Assign quaternion q1 at the end of the curve
    std::cout << "Quaternion curve linear interpolated with the quaternions q0 (start) = [w=" << quaternionLERPCurve1.m_controlPoints[0].w() << ", x=" << quaternionLERPCurve1.m_controlPoints[0].x() << ", y=" << quaternionLERPCurve1.m_controlPoints[0].y() << ", z=" << quaternionLERPCurve1.m_controlPoints[0].z() << "] and q1 (end) = [w=" << quaternionLERPCurve1.m_controlPoints[1].w() << ", x=" << quaternionLERPCurve1.m_controlPoints[1].x() << ", y=" << quaternionLERPCurve1.m_controlPoints[1].y() << ", z=" << quaternionLERPCurve1.m_controlPoints[1].z() << "]." << std::endl
              << std::endl;

    // Converting a quaternion to a rotation Matrix
    //----------------------------------------------
    auto rotationMatrix = quaternionLERPCurve1.m_controlPoints[0].toRotationMatrix();
    std::cout << "The quaternion q = [w=" << quaternionLERPCurve1.m_controlPoints[0].w() << ", x=" << quaternionLERPCurve1.m_controlPoints[0].x() << ", y=" << quaternionLERPCurve1.m_controlPoints[0].y() << ", z=" << quaternionLERPCurve1.m_controlPoints[0].z() << "] converted to a rotation matrix is:" << std::endl
              << rotationMatrix << std::endl
              << std::endl;

    // Evaluation of the quaternion LERP Curve
    // ---------------------------------------
    auto resultEvaluate = quaternionLERPCurve1.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the quaternion curve at the position x. The evaluation methods "evaluateD0ToDN" and "evaluateNumericDerivative" are available too.
    std::cout << "Evaluation of the quaternion curve at the position " << position << ":" << std::endl;
    std::cout << "q = [w=" << resultEvaluate.w() << ", x=" << resultEvaluate.x() << ", y=" << resultEvaluate.y() << ", z=" << resultEvaluate.z() << "]" << std::endl
              << std::endl;

    // Plot the quaternion curve
    // -------------------------
    plotQuaternionCurve2D(quaternionLERPCurve1, stepSizePlot, derivationOrderPlot, fileName); // Writes the files for plotting the underlying quaternion components w, x, y and z
    plotQuaternionCurve3D(quaternionLERPCurve1, stepSizePlot, fileName3d); // Writes the files for plotting the 3d quaternion curve for the end of the coordinate axis x, y and z
    std::cout << "The quaternion curve was succesfully plotted. To view the plots start gnuplot in your build repository via the terminal." << std::endl;
    std::cout << "To view the plot of the underlying quaternion components w, x, y and z execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "To view the 3d plot of the quaternion curve for the end of the coordinate axis x, y and z execute the command: load \"" << fileName3d << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Tutorial for quaternion NLERP curves
void quaternionNLERPCurveTutorial()
{
    // Creation of quaternion by Normalized Linear intERPolation between start-quaternion and end-quaternion
    // -----------------------------------------------------------------------------------------------------
    QuaternionNLERPCurve quaternionNLERPCurve1; // Create quaternion NLERP Curve with default constructor. The control points are not yet assigned
    QuaternionNLERPCurve quaternionNLERPCurve2(Eigen::Quaterniond(0, 1, 0, 0), Eigen::Quaterniond(0, 0, 1, 0)); // Create quaternion NLERP Curve with specialized constructor. The control points are assigned directly

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the quaternion curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    unsigned int derivationOrderPlot = 0; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    std::string fileName = "QuaternionNLERPCurve2d"; // File in which the 2d plot is written
    std::string fileName3d = "QuaternionNLERPCurve3d"; // File in which the 3d plot is written

    // Assigning control points to the quaternion NLERP Curve
    // ------------------------------------------------------
    quaternionNLERPCurve1.m_controlPoints[0] = Eigen::Quaterniond(0, 1, 0, 0); // Assign quaternion q0 at the beginning of the curve
    quaternionNLERPCurve1.m_controlPoints[1] = Eigen::Quaterniond(0, 0, 1, 0); // Assign quaternion q1 at the end of the curve
    std::cout << "Quaternion curve linear normalized interpolated with the quaternions q0 (start) = [w=" << quaternionNLERPCurve1.m_controlPoints[0].w() << ", x=" << quaternionNLERPCurve1.m_controlPoints[0].x() << ", y=" << quaternionNLERPCurve1.m_controlPoints[0].y() << ", z=" << quaternionNLERPCurve1.m_controlPoints[0].z() << "] and q1 (end) = [w=" << quaternionNLERPCurve1.m_controlPoints[1].w() << ", x=" << quaternionNLERPCurve1.m_controlPoints[1].x() << ", y=" << quaternionNLERPCurve1.m_controlPoints[1].y() << ", z=" << quaternionNLERPCurve1.m_controlPoints[1].z() << "]." << std::endl
              << std::endl;

    // Converting a quaternion to a rotation Matrix
    // --------------------------------------------
    auto rotationMatrix = quaternionNLERPCurve1.m_controlPoints[0].toRotationMatrix();
    std::cout << "The quaternion q = [w=" << quaternionNLERPCurve1.m_controlPoints[0].w() << ", x=" << quaternionNLERPCurve1.m_controlPoints[0].x() << ", y=" << quaternionNLERPCurve1.m_controlPoints[0].y() << ", z=" << quaternionNLERPCurve1.m_controlPoints[0].z() << "] converted to a rotation matrix is:" << std::endl
              << rotationMatrix << std::endl
              << std::endl;

    // Evaluation of the quaternion NLERP Curve
    // ----------------------------------------
    auto resultEvaluate = quaternionNLERPCurve1.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the quaternion curve at the position x. The evaluation methods "evaluateD0ToDN" and "evaluateNumericDerivative" are available too.
    std::cout << "Evaluation of the quaternion curve at the position " << position << ":" << std::endl;
    std::cout << "q = [w=" << resultEvaluate.w() << ", x=" << resultEvaluate.x() << ", y=" << resultEvaluate.y() << ", z=" << resultEvaluate.z() << "]" << std::endl
              << std::endl;

    // Plot the quaternion curve
    // -------------------------
    plotQuaternionCurve2D(quaternionNLERPCurve1, stepSizePlot, derivationOrderPlot, fileName); // Writes the files for plotting the underlying quaternion components w, x, y and z
    plotQuaternionCurve3D(quaternionNLERPCurve1, stepSizePlot, fileName3d); // Writes the files for plotting the 3d quaternion curve for the end of the coordinate axis x, y and z
    std::cout << "The quaternion curve was succesfully plotted. To view the plots start gnuplot in your build repository via the terminal." << std::endl;
    std::cout << "To view the plot of the underlying quaternion components w, x, y and z execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "To view the 3d plot of the quaternion curve for the end of the coordinate axis x, y and z execute the command: load \"" << fileName3d << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Tutorial for quaternion SLERP curves
void quaternionSLERPCurveTutorial()
{
    // Creation of quaternion by Spherical Linear intERPolation between start-quaternion and end-quaternion
    // ----------------------------------------------------------------------------------------------------
    QuaternionSLERPCurve quaternionSLERPCurve1; // Create quaternion SLERP Curve with default constructor. The control points are not yet assigned
    QuaternionSLERPCurve quaternionSLERPCurve2(Eigen::Quaterniond(0, 1, 0, 0), Eigen::Quaterniond(0, 0, 1, 0)); // Create quaternion SLERP Curve with specialized constructor. The control points are assigned directly

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the quaternion curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    unsigned int derivationOrderPlot = 0; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    std::string fileName = "QuaternionSLERPCurve2d"; // File in which the 2d plot is written
    std::string fileName3d = "QuaternionSLERPCurve3d"; // File in which the 3d plot is written

    // Assigning control points to the quaternion SLERP Curve
    // ------------------------------------------------------
    quaternionSLERPCurve1.m_controlPoints[0] = Eigen::Quaterniond(0, 1, 0, 0); // Assign quaternion q0 at the beginning of the curve
    quaternionSLERPCurve1.m_controlPoints[1] = Eigen::Quaterniond(0, 0, 1, 0); // Assign quaternion q1 at the end of the curve
    std::cout << "Quaternion curve spherical linear interpolated with the quaternions q0 (start) = [w=" << quaternionSLERPCurve1.m_controlPoints[0].w() << ", x=" << quaternionSLERPCurve1.m_controlPoints[0].x() << ", y=" << quaternionSLERPCurve1.m_controlPoints[0].y() << ", z=" << quaternionSLERPCurve1.m_controlPoints[0].z() << "] and q1 (end) = [w=" << quaternionSLERPCurve1.m_controlPoints[1].w() << ", x=" << quaternionSLERPCurve1.m_controlPoints[1].x() << ", y=" << quaternionSLERPCurve1.m_controlPoints[1].y() << ", z=" << quaternionSLERPCurve1.m_controlPoints[1].z() << "]." << std::endl
              << std::endl;

    // Converting a quaternion to a rotation Matrix
    //----------------------------------------------
    auto rotationMatrix = quaternionSLERPCurve1.m_controlPoints[0].toRotationMatrix();
    std::cout << "The quaternion q = [w=" << quaternionSLERPCurve1.m_controlPoints[0].w() << ", x=" << quaternionSLERPCurve1.m_controlPoints[0].x() << ", y=" << quaternionSLERPCurve1.m_controlPoints[0].y() << ", z=" << quaternionSLERPCurve1.m_controlPoints[0].z() << "] converted to a rotation matrix is:" << std::endl
              << rotationMatrix << std::endl
              << std::endl;

    // Evaluation of the quaternion SLERP Curve
    // ----------------------------------------
    auto resultEvaluate = quaternionSLERPCurve1.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the quaternion curve at the position x. The evaluation methods "evaluateD0ToDN" and "evaluateNumericDerivative" are available too.
    std::cout << "Evaluation of the quaternion curve at the position " << position << ":" << std::endl;
    std::cout << "q = [w=" << resultEvaluate.w() << ", x=" << resultEvaluate.x() << ", y=" << resultEvaluate.y() << ", z=" << resultEvaluate.z() << "]" << std::endl
              << std::endl;

    // Plot the quaternion curve
    // -------------------------
    plotQuaternionCurve2D(quaternionSLERPCurve1, stepSizePlot, derivationOrderPlot, fileName); // Writes the files for plotting the underlying quaternion components w, x, y and z
    plotQuaternionCurve3D(quaternionSLERPCurve1, stepSizePlot, fileName3d); // Writes the files for plotting the 3d quaternion curve for the end of the coordinate axis x, y and z

    std::cout << "The quaternion curve was succesfully plotted. To view the plots start gnuplot in your build repository via the terminal." << std::endl;
    std::cout << "To view the plot of the underlying quaternion components w, x, y and z execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "To view the 3d plot of the quaternion curve for the end of the coordinate axis x, y and z execute the command: load \"" << fileName3d << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Tutorial for quaternion SQUAD curves
void quaternionSQUADCurveTutorial()
{
    // Creation of quaternion by Spherical QUADrangle curve interpolation between start-quaternion and end-quaternion with two control quaternions in between
    // ------------------------------------------------------------------------------------------------------------------------------------------------------
    QuaternionSQUADCurve quaternionSQUADCurve1; // Create quaternion SQUAD Curve with default constructor. The control points are not yet assigned
    QuaternionSQUADCurve quaternionSQUADCurve2(Eigen::Quaterniond(0, 1, 0, 0), Eigen::Quaterniond(0, 0, 1, 0), Eigen::Quaterniond(1, 0, 0, 0), Eigen::Quaterniond(0, 0, 0, 1)); //Create quaternion SQUAD Curve with specialized constructor. The control points are assigned directly

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the quaternion curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    unsigned int derivationOrderPlot = 0; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    std::string fileName = "QuaternionSQUADCurve2d"; // File in which the 2d plot is written
    std::string fileName3d = "QuaternionSQUADCurve3d"; // File in which the 3d plot is written

    // Assigning control points to the quaternion SQUAD Curve
    // ------------------------------------------------------
    quaternionSQUADCurve1.m_controlPoints[0] = Eigen::Quaterniond(0, 1, 0, 0); // Assign quaternion q0 at the beginning of the curve
    quaternionSQUADCurve1.m_controlPoints[1] = Eigen::Quaterniond(0, 0, 1, 0); // Assign first control-quaternion q1 to the curve
    quaternionSQUADCurve1.m_controlPoints[2] = Eigen::Quaterniond(1, 0, 0, 0); // Assign second control-quaternion q2 to the curve
    quaternionSQUADCurve1.m_controlPoints[3] = Eigen::Quaterniond(0, 0, 0, 1); // Assign quaternion q3 at the end of the curve
    std::cout << "Quaternion curve spherical quadrangle interpolated with the quaternions q0 (start) = [w=" << quaternionSQUADCurve1.m_controlPoints[0].w() << ", x=" << quaternionSQUADCurve1.m_controlPoints[0].x() << ", y=" << quaternionSQUADCurve1.m_controlPoints[0].y() << ", z=" << quaternionSQUADCurve1.m_controlPoints[0].z() << "], q3 (end) = [w=" << quaternionSQUADCurve1.m_controlPoints[3].w() << ", x=" << quaternionSQUADCurve1.m_controlPoints[3].x() << ", y=" << quaternionSQUADCurve1.m_controlPoints[3].y() << ", z=" << quaternionSQUADCurve1.m_controlPoints[3].z() << "] and control quaternions q1 = [w=" << quaternionSQUADCurve1.m_controlPoints[1].w() << ", x=" << quaternionSQUADCurve1.m_controlPoints[1].x() << ", y=" << quaternionSQUADCurve1.m_controlPoints[1].y() << ", z=" << quaternionSQUADCurve1.m_controlPoints[1].z() << "] and q2 = [w=" << quaternionSQUADCurve1.m_controlPoints[2].w() << ", x=" << quaternionSQUADCurve1.m_controlPoints[2].x() << ", y=" << quaternionSQUADCurve1.m_controlPoints[2].y() << ", z=" << quaternionSQUADCurve1.m_controlPoints[2].z() << "]." << std::endl
              << std::endl;

    // Converting a quaternion to a rotation Matrix
    //----------------------------------------------
    auto rotationMatrix = quaternionSQUADCurve1.m_controlPoints[0].toRotationMatrix();
    std::cout << "The quaternion q = [w=" << quaternionSQUADCurve1.m_controlPoints[0].w() << ", x=" << quaternionSQUADCurve1.m_controlPoints[0].x() << ", y=" << quaternionSQUADCurve1.m_controlPoints[0].y() << ", z=" << quaternionSQUADCurve1.m_controlPoints[0].z() << "] converted to a rotation matrix is:" << std::endl
              << rotationMatrix << std::endl
              << std::endl;

    // Evaluation of the quaternion SQUAD Curve
    // ----------------------------------------
    auto resultEvaluate = quaternionSQUADCurve1.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the quaternion curve at the position x. The evaluation methods "evaluateD0ToDN" and "evaluateNumericDerivative" are available too.
    std::cout << "Evaluation of the quaternion curve at the position " << position << ":" << std::endl;
    std::cout << "q = [w=" << resultEvaluate.w() << ", x=" << resultEvaluate.x() << ", y=" << resultEvaluate.y() << ", z=" << resultEvaluate.z() << "]" << std::endl
              << std::endl;

    // Plot the quaternion curve
    // -------------------------
    plotQuaternionCurve2D(quaternionSQUADCurve1, stepSizePlot, derivationOrderPlot, fileName); // Writes the files for plotting the underlying quaternion components w, x, y and z
    plotQuaternionCurve3D(quaternionSQUADCurve1, stepSizePlot, fileName3d); // Writes the files for plotting the 3d quaternion curve for the end of the coordinate axis x, y and z

    std::cout << "The quaternion curve was succesfully plotted. To view the plots start gnuplot in your build repository via the terminal." << std::endl;
    std::cout << "To view the plot of the underlying quaternion components w, x, y and z execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "To view the 3d plot of the quaternion curve for the end of the coordinate axis x, y and z execute the command: load \"" << fileName3d << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Tutorial for quaternion Bezier curves
void quaternionBezierCurveTutorial()
{
    // Creation of quaternion by interpolation using cubic Bezier between start-quaternion and end-quaternion with two control quaternions in between
    // ----------------------------------------------------------------------------------------------------------------------------------------------
    QuaternionBezierCurve quaternionBezierCurve1; // Create quaternion Bezier Curve with default constructor. The control points are not yet assigned
    QuaternionBezierCurve quaternionBezierCurve2(Eigen::Quaterniond(0, 1, 0, 0), Eigen::Quaterniond(0, 0, 1, 0), Eigen::Quaterniond(1, 0, 0, 0), Eigen::Quaterniond(0, 0, 0, 1)); //Create quaternion Bezier Curve with specialized constructor. The control points are assigned directly

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the quaternion curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    unsigned int derivationOrderPlot = 0; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    std::string fileName = "QuaternionBezierCurve2d"; // File in which the 2d plot is written
    std::string fileName3d = "QuaternionBezierCurve3d"; // File in which the 3d plot is written

    // Assigning control points to the quaternion Bezier Curve
    // -------------------------------------------------------
    quaternionBezierCurve1.m_controlPoints[0] = Eigen::Quaterniond(0, 1, 0, 0); // Assign quaternion q0 at the beginning of the curve
    quaternionBezierCurve1.m_controlPoints[1] = Eigen::Quaterniond(0, 0, 1, 0); // Assign first control-quaternion q1 to the curve
    quaternionBezierCurve1.m_controlPoints[2] = Eigen::Quaterniond(1, 0, 0, 0); // Assign second control-quaternion q2 to the curve
    quaternionBezierCurve1.m_controlPoints[3] = Eigen::Quaterniond(0, 0, 0, 1); // Assign quaternion q3 at the end of the curve
    std::cout << "Quaternion curve interpolated using cubic Bezier curves with the quaternions q0 (start) = [w=" << quaternionBezierCurve1.m_controlPoints[0].w() << ", x=" << quaternionBezierCurve1.m_controlPoints[0].x() << ", y=" << quaternionBezierCurve1.m_controlPoints[0].y() << ", z=" << quaternionBezierCurve1.m_controlPoints[0].z() << "], q3 (end) = [w=" << quaternionBezierCurve1.m_controlPoints[3].w() << ", x=" << quaternionBezierCurve1.m_controlPoints[3].x() << ", y=" << quaternionBezierCurve1.m_controlPoints[3].y() << ", z=" << quaternionBezierCurve1.m_controlPoints[3].z() << "] and control quaternions q1 = [w=" << quaternionBezierCurve1.m_controlPoints[1].w() << ", x=" << quaternionBezierCurve1.m_controlPoints[1].x() << ", y=" << quaternionBezierCurve1.m_controlPoints[1].y() << ", z=" << quaternionBezierCurve1.m_controlPoints[1].z() << "] and q2 = [w=" << quaternionBezierCurve1.m_controlPoints[2].w() << ", x=" << quaternionBezierCurve1.m_controlPoints[2].x() << ", y=" << quaternionBezierCurve1.m_controlPoints[2].y() << ", z=" << quaternionBezierCurve1.m_controlPoints[2].z() << "]." << std::endl
              << std::endl;

    // Converting a quaternion to a rotation Matrix
    //----------------------------------------------
    auto rotationMatrix = quaternionBezierCurve1.m_controlPoints[0].toRotationMatrix();
    std::cout << "The quaternion q = [w=" << quaternionBezierCurve1.m_controlPoints[0].w() << ", x=" << quaternionBezierCurve1.m_controlPoints[0].x() << ", y=" << quaternionBezierCurve1.m_controlPoints[0].y() << ", z=" << quaternionBezierCurve1.m_controlPoints[0].z() << "] converted to a rotation matrix is:" << std::endl
              << rotationMatrix << std::endl
              << std::endl;

    // Evaluation of the quaternion Bezier Curve
    // ---------------------------------------
    auto resultEvaluate = quaternionBezierCurve1.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the quaternion curve at the position x. The evaluation methods "evaluateD0ToDN" and "evaluateNumericDerivative" are available too.
    std::cout << "Evaluation of the quaternion curve at the position " << position << ":" << std::endl;
    std::cout << "q = [w=" << resultEvaluate.w() << ", x=" << resultEvaluate.x() << ", y=" << resultEvaluate.y() << ", z=" << resultEvaluate.z() << "]" << std::endl
              << std::endl;

    // Plot the quaternion curve
    // -------------------------
    plotQuaternionCurve2D(quaternionBezierCurve1, stepSizePlot, derivationOrderPlot, fileName); // Writes the files for plotting the underlying quaternion components w, x, y and z
    plotQuaternionCurve3D(quaternionBezierCurve1, stepSizePlot, fileName3d); // Writes the files for plotting the 3d quaternion curve for the end of the coordinate axis x, y and z

    std::cout << "The quaternion curve was succesfully plotted. To view the plots start gnuplot in your build repository via the terminal." << std::endl;
    std::cout << "To view the plot of the underlying quaternion components w, x, y and z execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "To view the 3d plot of the quaternion curve for the end of the coordinate axis x, y and z execute the command: load \"" << fileName3d << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Tutorial for quaternion B-spline curves
void quaternionBSplineSplineTutorial()
{
    // Creation of quaternion by interpolation using Bsplines between start-quaternion and end-quaternion with two control quaternions in between
    // ------------------------------------------------------------------------------------------------------------------------------------------
    const unsigned int degree = 3; // Degree of underlying BSpline basis
    QuaternionBSplineSpline<degree> quaternionBSplineSpline1; // Create quaternion BSpline spline with default constructor. The control points are not yet assigned

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the quaternion spline is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    unsigned int derivationOrderPlot = 0; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    std::string fileName = "QuaternionBSplineCurve2d"; // File in which the 2d plot is written
    std::string fileName3d = "QuaternionBSplineCurve3d"; // File in which the 3d plot is written

    // Create quaternion BSpline by interpolation
    // ------------------------------------------
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> controlQuaternions; // Create vector with control quaternions
    // Assigning control points to the quaternion BSpline spline. Number of control points has to be >= degree+1
    controlQuaternions.push_back(Eigen::Quaterniond(1, 0, 0, 0)); // Define quaternion q0 at the beginning of the spline
    controlQuaternions.push_back(Eigen::Quaterniond(1, 1, 0, 0).normalized()); // Define first control-quaternion q1 to the spline
    controlQuaternions.push_back(Eigen::Quaterniond(0, 1, 1, 0).normalized()); // Define second control-quaternion q2 to the spline
    controlQuaternions.push_back(Eigen::Quaterniond(0, 0, 1, 1).normalized()); // Define quaternion q3 at the end of the spline
    QuaternionSplineResult result; // Stores detailed error description
    quaternionBSplineSpline1.interpolate(controlQuaternions, nullptr, QuaternionSplineInterpolationMethod::BSPLINE_SIMPLE, &result); // Calculating the quaternion BSpline spline by interpolation method "BSPLINE_SIMPLE" with the before declared parameters, if the splines should have more than one segment, nullptr has to be replaced by a pointer to the segment proportions.
    std::cout << "Quaternion spline interpolated using BSplines with the quaternions q0 (start) = [w=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].w() << ", x=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].x() << ", y=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].y() << ", z=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].z() << "], q3 (end) = [w=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[3].w() << ", x=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[3].x() << ", y=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[3].y() << ", z=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[3].z() << "] and control quaternions q1 = [w=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[1].w() << ", x=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[1].x() << ", y=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[1].y() << ", z=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[1].z() << "] and q2 = [w=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[2].w() << ", x=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[2].x() << ", y=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[2].y() << ", z=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[2].z() << "]." << std::endl
              << std::endl;

    // Converting a quaternion to a rotation Matrix
    // ---------------------------------------------
    auto rotationMatrix = quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].toRotationMatrix();
    std::cout << "The quaternion q = [w=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].w() << ", x=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].x() << ", y=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].y() << ", z=" << quaternionBSplineSpline1.m_segments[0].m_controlPoints[0].z() << "] converted to a rotation matrix is:" << std::endl
              << rotationMatrix << std::endl
              << std::endl;

    // Evaluation of the quaternion BSpline spline
    // -------------------------------------------
    auto resultEvaluate = quaternionBSplineSpline1.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the quaternion spline at the position x. The evaluation methods "evaluateD0ToDN" and "evaluateNumericDerivative" are available too.
    std::cout << "Evaluation of the quaternion curve at the position " << position << ":" << std::endl;
    std::cout << "q = [w=" << resultEvaluate.w() << ", x=" << resultEvaluate.x() << ", y=" << resultEvaluate.y() << ", z=" << resultEvaluate.z() << "]" << std::endl
              << std::endl;

    // Plot the quaternion spline
    // --------------------------
    plotQuaternionSpline2D(quaternionBSplineSpline1, stepSizePlot, derivationOrderPlot, fileName); // Writes the files for plotting the underlying quaternion components w, x, y and z
    plotQuaternionSpline3D(quaternionBSplineSpline1, stepSizePlot, fileName3d); // Writes the files for plotting the 3d quaternion curve for the end of the coordinate axis x, y and z
    std::cout << "The quaternion spline was succesfully plotted. To view the plots start gnuplot in your build repository via the terminal." << std::endl;
    std::cout << "To view the plot of the underlying quaternion components w, x, y and z execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "To view the 3d plot of the quaternion curve for the end of the coordinate axis x, y and z execute the command: load \"" << fileName3d << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Main program function
int main()
{
    // Tutorial for quaternion LERP curves
    quaternionLERPCurveTutorial();

    // Tutorial for quaternion NLERP curves
    quaternionNLERPCurveTutorial();

    // Tutorial for quaternion SLERP curves
    quaternionSLERPCurveTutorial();

    // Tutorial for quaternion SQUAD curves
    quaternionSQUADCurveTutorial();

    // Tutorial for quaternion Bezier curves
    quaternionBezierCurveTutorial();

    // Tutorial for quaternion B-spline splines
    quaternionBSplineSplineTutorial();

    // Quit with success
    return 0;
}
