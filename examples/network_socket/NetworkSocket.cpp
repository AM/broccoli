/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include <broccoli/io/network/NetworkSocket.hpp>
#include <broccoli/io/serialization/SerializableDataString.hpp>
#include <iomanip>
#include <iostream>

using namespace broccoli;
using namespace io;
using namespace serialization;

//! Test of networksocket communication for different socket types
bool testSocketCommunication(const NetworkSocketType& socketTypeA, const NetworkSocketType& socketTypeB, const std::string& TestCase)
{
    //Initiaize Helpers
    std::array<bool, 17> ASSERT_Array;
    bool ASSERT = true;

    // Create sockets
    NetworkSocket<SerializableDataString, SerializableDataString> socketA(NetworkSocketOptions(socketTypeA, "socketA", "localhost", 5006, 5005, true, 0, 0.001, false, 10, 10));
    NetworkSocket<SerializableDataString, SerializableDataString> socketB(NetworkSocketOptions(socketTypeB, "socketB", "localhost", 5005, 5006, true, 0, 0.001, false, 10, 10));

    // Copy constructor
    NetworkSocket<SerializableDataString, SerializableDataString> socketCopy(socketA);
    ASSERT_Array[0] = (socketCopy.name() == socketA.name());

    // Assignment operator
    socketCopy = socketB;
    ASSERT_Array[1] = (socketCopy.name() == socketB.name());

    // Redefine heartbeat timeouts
    socketA.setHeartBeatSendInterval(core::Time(0.001));
    socketB.setHeartBeatSendInterval(core::Time(0.001));
    socketA.setHeartBeatTimeout(core::Time(0.01));
    socketB.setHeartBeatTimeout(core::Time(0.01));

    // Setup
    // -----
    // Initialize sockets
    ASSERT_Array[2] = (socketA.initialize() == true);
    ASSERT_Array[3] = (socketB.initialize() == true);

    // Wait to allow establishing connection
    core::Time::sleep(0.03);

    // Check connection status
    ASSERT_Array[4] = (socketA.isConnected() == true);
    ASSERT_Array[5] = (socketB.isConnected() == true);

    // Heartbeat detection
    // -------------------
    // Wait to check for heartbeat timeout
    core::Time::sleep(0.03);
    ASSERT_Array[6] = (socketA.isConnected() == true);
    ASSERT_Array[7] = (socketB.isConnected() == true);

    // Check connection status
    ASSERT_Array[8] = (socketA.isConnected() == true);
    ASSERT_Array[9] = (socketB.isConnected() == true);

    // Sending and receiving
    // ---------------------
    // Add message to send buffer
    SerializableDataString testMessageToSend("test");
    ASSERT_Array[10] = (socketA.m_objectsToSend.push(testMessageToSend) == 0);

    // Wait to give sending/receiving some time...
    core::Time::sleep(0.02);

    // Try to get message
    SerializableDataString testMessageToReceive;
    ASSERT_Array[11] = (socketB.m_receivedObjects.pop(testMessageToReceive) == 1);

    // Compare messages
    ASSERT_Array[12] = (testMessageToReceive == testMessageToSend);

    // Output signals to console
    NetworkSocketSignal signal;
    while (socketA.m_signalBuffer.pop(signal) != 0)
        std::cout << "Signal from socket A: type=" << NetworkSocketSignal::typeString(signal.m_type) << " message=" << NetworkSocketSignal::messageString(signal.m_message) << " text=" << signal.m_text << std::endl;
    while (socketB.m_signalBuffer.pop(signal) != 0)
        std::cout << "Signal from socket B: type=" << NetworkSocketSignal::typeString(signal.m_type) << " message=" << NetworkSocketSignal::messageString(signal.m_message) << " text=" << signal.m_text << std::endl;

    // Shutdown
    // --------
    // De-initialize one socket to simulate disconnection
    ASSERT_Array[13] = (socketA.deInitialize() == true);

    // Wait to check for heartbeat timeout
    core::Time::sleep(0.03);
    ASSERT_Array[14] = (socketA.isConnected() == false);
    ASSERT_Array[15] = (socketB.isConnected() == false);

    // De-initialize other socket and check for proper shutdown
    ASSERT_Array[16] = (socketB.deInitialize() == true);

    // Test output
    std::cout << "[RUN       ] " << TestCase << std::endl;
    for (size_t i = 0; i < ASSERT_Array.size(); i++) {
        ASSERT = ASSERT && ASSERT_Array[i];

        if (ASSERT_Array[i] == false) {
            std::cout << "  Expected equality in:" << std::endl;
            std::cout << "    ASSERT_Array[" << i << "]" << std::endl;
            std::cout << "    Which is: false" << std::endl;
        }
    }
    if (ASSERT == false)
        std::cout << "[    FAILED] " << TestCase << std::endl;
    else
        std::cout << "[    PASSED] " << TestCase << std::endl;

    return ASSERT;
}

//! Test of setters and getters
bool testSettersAndGetters(const std::string& TestCase)
{
    //Initialize Helpers
    std::array<bool, 13> ASSERT_Array;
    bool ASSERT = true;

    // Create instance
    NetworkSocket<SerializableDataString, SerializableDataString> socket(NetworkSocketOptions(NetworkSocketType::UDP, "socket", "localhost", 5006, 5005, true, 0, 0, true, 10, 10));

    // Setters and getters
    core::Time connectionAttemptInterval = core::Time(1);
    socket.setConnectionAttemptInterval(connectionAttemptInterval);
    ASSERT_Array[0] = (connectionAttemptInterval == socket.connectionAttemptInterval());
    bool sendHeartBeats = true;
    socket.setSendHeartBeats(sendHeartBeats);
    ASSERT_Array[1] = (sendHeartBeats == socket.sendHeartBeats());
    NetworkSocket<SerializableDataString, SerializableDataString>::DataStream heartBeatSendCode = { 0x1A };
    socket.setHeartBeatSendCode(heartBeatSendCode);
    ASSERT_Array[2] = (heartBeatSendCode == socket.heartBeatSendCode());
    core::Time heartBeatSendInterval = core::Time(0.1);
    socket.setHeartBeatSendInterval(heartBeatSendInterval);
    ASSERT_Array[3] = (heartBeatSendInterval == socket.heartBeatSendInterval());
    bool receiveHeartBeats = true;
    socket.setReceiveHeartBeats(receiveHeartBeats);
    ASSERT_Array[4] = (receiveHeartBeats == socket.receiveHeartBeats());
    NetworkSocket<SerializableDataString, SerializableDataString>::DataStream heartBeatReceiveCode = { 0x1B };
    socket.setHeartBeatReceiveCode(heartBeatReceiveCode);
    ASSERT_Array[5] = (heartBeatReceiveCode == socket.heartBeatReceiveCode());
    core::Time heartBeatTimeout = core::Time(0.123);
    socket.setHeartBeatTimeout(heartBeatTimeout);
    ASSERT_Array[6] = (heartBeatTimeout == socket.heartBeatTimeout());

    // Getters only
    NetworkSocketType type = socket.options().m_type;
    ASSERT_Array[7] = (type == NetworkSocketType::UDP);
    std::string name = socket.options().m_name;
    ASSERT_Array[8] = (name == "socket");
    std::string remoteIP = socket.options().m_remoteIP;
    ASSERT_Array[9] = (remoteIP == "localhost");
    int remotePort = socket.options().m_remotePort;
    ASSERT_Array[10] = (remotePort == 5006);
    int ownPort = socket.options().m_ownPort;
    ASSERT_Array[11] = (ownPort == 5005);
    bool isConnected = socket.isConnected();
    ASSERT_Array[12] = (isConnected == false);
    core::Time lastConnectionAttempt = socket.lastConnectionAttempt();
    (void)lastConnectionAttempt;
    core::Time lastHeartBeatSent = socket.lastHeartBeatSent();
    (void)lastHeartBeatSent;
    core::Time lastHeartBeatReceived = socket.lastHeartBeatReceived();
    (void)lastHeartBeatReceived;

    //Test output
    std::cout << "[RUN       ] " << TestCase << std::endl;
    for (size_t i = 0; i < ASSERT_Array.size(); i++) {
        ASSERT = ASSERT && ASSERT_Array[i];

        if (ASSERT_Array[i] == false) {
            std::cout << "  Expected equality in:" << std::endl;
            std::cout << "    ASSERT_Array[" << i << "]" << std::endl;
            std::cout << "    Which is: false" << std::endl;
        }
    }
    if (ASSERT == false)
        std::cout << "[    FAILED] " << TestCase << std::endl;
    else
        std::cout << "[    PASSED] " << TestCase << std::endl;
    return ASSERT;
}

//! Mock object to simulate a large data object
class MockLargeData : public SerializableData {
public:
    // Members
    std::vector<double> m_data;

    // Serialization of payload (see base class for details)
    virtual BinaryStreamSize serializePayload(BinaryStream& stream, const Endianness& endianness) const
    {
        // Pre-allocate memory for best performance
        stream.reserve(sizeof(double) * m_data.size() + sizeof(BinaryStreamSize) * 1);

        // Serialize data object
        return serialization::serialize(stream, endianness, m_data);
    }

    // Deserialization of payload (see base class for details)
    virtual BinaryStreamSize deSerializePayload(const BinaryStream& stream, const BinaryStreamSize& index, const BinaryStreamSize& payloadSize, const Endianness& endianness)
    {
        // Deserialize and check, if the payload size is correct
        if (serialization::deSerialize(stream, index, endianness, m_data) != payloadSize)
            return 0;
        else
            return payloadSize;
    }
};

bool testLargeObjectTransfer(const std::string& TestCase)
{
    //Initialize Helpers
    std::array<bool, 3> ASSERT_Array;
    bool ASSERT = true;

    // Remember start time
    const core::Time totalStartTime = broccoli::core::Time::currentTime();

    // Setup network sockets
    NetworkSocket<MockLargeData, MockLargeData> socketA(TCPServerOptions("socketA", 5005, true, 0, 0.001, false, 10, 10));
    NetworkSocket<MockLargeData, MockLargeData> socketB(TCPClientOptions("socketB", "localhost", 5005, true, 0, 0.001, false, 10, 10));
    socketA.initialize();
    socketB.initialize();
    while ((!socketA.isConnected() || !socketB.isConnected()) && (core::Time::currentTime() - totalStartTime).toDouble() < 3.0) {
    }
    ASSERT_Array[0] = socketA.isConnected();
    ASSERT_Array[1] = socketB.isConnected();

    // Send data from one socket to the other
    MockLargeData sendData;
    sendData.m_data.resize(1000000ULL);
    for (size_t i = 0; i < sendData.m_data.size(); i++)
        sendData.m_data[i] = i % 256;
    const broccoli::core::Time transmissionStartTime = broccoli::core::Time::currentTime();
    socketA.m_objectsToSend.push(sendData);
    while (socketB.m_receivedObjects.elementCount() == 0 && (broccoli::core::Time::currentTime() - transmissionStartTime).toDouble() < 3.0) {
    }
    const broccoli::core::Time transmissionEndTime = broccoli::core::Time::currentTime();
    MockLargeData receiveData;
    socketB.m_receivedObjects.pop(receiveData);
    ASSERT_Array[2] = (sendData.m_data == receiveData.m_data);

    // Output signals to console
    NetworkSocketSignal signal;
    while (socketA.m_signalBuffer.pop(signal) != 0)
        std::cout << "Signal from socket A: type=" << NetworkSocketSignal::typeString(signal.m_type) << " message=" << NetworkSocketSignal::messageString(signal.m_message) << " text=" << signal.m_text << std::endl;
    while (socketB.m_signalBuffer.pop(signal) != 0)
        std::cout << "Signal from socket B: type=" << NetworkSocketSignal::typeString(signal.m_type) << " message=" << NetworkSocketSignal::messageString(signal.m_message) << " text=" << signal.m_text << std::endl;

    // Deinitialize sockets
    socketA.deInitialize();
    socketB.deInitialize();

    // Remember end time
    const broccoli::core::Time totalEndTime = broccoli::core::Time::currentTime();

    //Test output
    std::cout << "[RUN       ] " << TestCase << std::endl;
    for (size_t i = 0; i < ASSERT_Array.size(); i++) {
        ASSERT = ASSERT && ASSERT_Array[i];

        if (ASSERT_Array[i] == false) {
            std::cout << "  Expected equality in:" << std::endl;
            std::cout << "    ASSERT_Array[" << i << "]" << std::endl;
            std::cout << "    Which is: false" << std::endl;
        }
    }
    if (ASSERT == false)
        std::cout << "[    FAILED] " << TestCase << std::endl;
    else
        std::cout << "[    PASSED] " << TestCase << std::endl;
    serialization::BinaryStream stream;
    sendData.serialize(stream, Endianness::LITTLE);
    std::cout << "transmitted bytes: " << sizeof(serialization::BinaryStreamSize) + stream.size() << "\n";
    std::cout << "transmission duration: " + (transmissionEndTime - transmissionStartTime).encodeToDurationString() << "\n";
    std::cout << "total duration: " + (totalEndTime - totalStartTime).encodeToDurationString() << "\n";
    return ASSERT;
}

int main()
{
    //Initialise Helpers
    std::array<bool, 4> TestReturn;
    int PassedTestCounter = 0;

    // Output user information
    std::cout << "Network Socket Example Application" << std::endl;
    std::cout << "==================================" << std::endl;

    std::cout << "\n[----------] " << TestReturn.size() << " tests from network-socket" << std::endl;

    //! Check implementation of UDP communication
    TestReturn[0] = testSocketCommunication(NetworkSocketType::UDP, NetworkSocketType::UDP, std::string("network_socket.UDPCommunication"));

    //! Check implementation of TCP communication
    TestReturn[1] = testSocketCommunication(NetworkSocketType::TCP_SERVER, NetworkSocketType::TCP_CLIENT, std::string("network_socket.TCPCommunication"));

    //! Test setters and getters
    TestReturn[2] = testSettersAndGetters(std::string("network-socket.SettersAndGetters"));

    //! Check transmission of large data objects
    TestReturn[3] = testLargeObjectTransfer(std::string("network-socket.LargeObjectTransfer"));

    // Output final test results
    for (size_t i = 0; i < TestReturn.size(); i++) {
        if (TestReturn[i] == true)
            PassedTestCounter++;
    }
    std::cout << "\n[==========] " << TestReturn.size() << " tests from network-socket ran" << std::endl;
    if (PassedTestCounter == 1)
        std::cout << "[  PASSED  ] " << PassedTestCounter << " test" << std::endl;
    else
        std::cout << "[  PASSED  ] " << PassedTestCounter << " tests" << std::endl;
    if ((PassedTestCounter - 1) == 1)
        std::cout << "[  FAILED  ] " << TestReturn.size() - PassedTestCounter << " test" << std::endl;
    else
        std::cout << "[  FAILED  ] " << TestReturn.size() - PassedTestCounter << " tests" << std::endl;

    // Exit program
    return 0;
}
