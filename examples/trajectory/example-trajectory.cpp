/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include <fstream>
#include <iostream>
#include <string>

// Include headers from broccoli libary
#include <broccoli/curve/trajectories/PolynomialTrajectory.hpp>

using namespace broccoli;
using namespace curve;

//! Generic function to plot a trajectory
/*!
 * \tparam TrajectoryType trajectory type to plot
 * \tparam dimension Dimension of the trajectory to plot
 * \param [in] trajectory trajectory to plot
 * \param [in] stepSize Step size for sampling the time axis
 * \param [in] maximumDerivation Maximum derivation to be plotted
 * \param [in] fileName Filename of output file(s)
 */
template <class TrajectoryType, unsigned int dimension>
inline void plotTrajectory(const TrajectoryType& trajectory, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    char axis = '?';

    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");

    for (unsigned int d = 0; d <= maximumDerivation; d++) {
        for (unsigned int j = 0; j < dimension; j++) {
            if (j > 0 || d > 0)
                gnuplotFile << "re";
            if (j == 0)
                axis = 'x';
            else if (j == 1)
                axis = 'y';
            else if (j == 2)
                axis = 'z';
            gnuplotFile << "plot '" << fileName << ".dat' using 1:" << (2 + j + d * dimension) << " w l title \"d^" << d << axis << " / dt^" << d << "\"\n";
        }
    }
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    size_t steps = std::floor(trajectory.m_duration / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = i * stepSize;
        dataFile << x << " ";

        // Evaluate value and derivatives
        for (unsigned int d = 0; d <= maximumDerivation; d++) {
            std::array<double, dimension> Di = trajectory.evaluate(x, d);
            for (unsigned int j = 0; j < dimension; j++)
                dataFile << " " << Di[j];
        }

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Generic function to plot a trajectory in 3d
/*!
 * \tparam TrajectoryType trajectory type to plot
 * \tparam dimension Dimension of the trajectory to plot
 * \param [in] trajectory trajectory to plot
 * \param [in] stepSize Step size for sampling the time axis
 * \param [in] maximumDerivation Maximum derivation to be plotted
 * \param [in] fileName Filename of output file(s)
 */
template <class TrajectoryType, unsigned int dimension>
inline void plotTrajectory3d(const TrajectoryType& trajectory, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "s";
    for (unsigned int d = 0; d <= maximumDerivation; d++) {
        if (d > 0)
            gnuplotFile << "re";
        gnuplotFile << "plot '" << fileName << ".dat' using " << 2 + d * 3 << ":" << 3 + d * 3 << ":" << 4 + d * 3 << " w l title \"d^" << d << " f / dt^" << d << "\n";
    }
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    size_t steps = std::floor(trajectory.m_duration / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = i * stepSize;
        dataFile << x << " ";

        // Evaluate value and derivatives
        for (unsigned int d = 0; d <= maximumDerivation; d++) {
            std::array<double, dimension> Di = trajectory.evaluate(x, d);
            for (unsigned int j = 0; j < dimension; j++)
                dataFile << " " << Di[j];
        }

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Tutorial for a polynomial trajectory
void polynomialTrajectoryTutorial()
{
    const unsigned int degree = 3; // Degree of the polynomial trajectory
    const unsigned int dimensions = 3; // Dimensions of the polynomial trajectory
    PolynomialTrajectory<degree, dimensions> trajectory; // Create polynomial trajectory (degree and dimension of the trajectory as template parameters)

    // Declaration of variables
    // ------------------------
    double time = 3; // Time when the trajectory is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    const unsigned int derivationOrderEvaluateD0toDN = 4; // Derivation order for the function evaluateD0toDN()
    unsigned int derivationOrderPlot = 0; // Derivation order for the plot
    double stepSizePlot = 1e-2; // Step size for the plot
    std::string fileName = "PolynomialTrajectory"; // File in which the plot is written
    std::string fileName3d = "PolynomialTrajectory3d"; // File in which the 3d plot is written

    // Declaration and assigning parameters to the polynomial trajectory
    // -----------------------------------------------------------------
    trajectory.m_duration = 8; // Duration of the trajectory

    // Declaration of the length of the spline segments, the sum of all segments has to be 1
    std::vector<double> segmentProportionsX = { 1 };
    std::vector<double> segmentProportionsY = { 1 };
    std::vector<double> segmentProportionsZ = { 0.5, 0.5 };
    // Variables which store detailed error description
    SplineResult resultX;
    SplineResult resultY;
    SplineResult resultZ;
    // Interpolation parameters of the splines
    std::vector<double> interpolationParametersX = { 0, 1, 0, 6 };
    std::vector<double> interpolationParametersY = { 0, -3, 2, 8 };
    std::vector<double> interpolationParametersZ = { 0, 2, 0 };
    // Interpolation of the splines
    trajectory.m_splines[0].interpolate(interpolationParametersX, &segmentProportionsX, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &resultX);
    trajectory.m_splines[1].interpolate(interpolationParametersY, &segmentProportionsY, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &resultY);
    trajectory.m_splines[2].interpolate(interpolationParametersZ, &segmentProportionsZ, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_LINEAR, &resultZ);

    // Store the trajectories boundaries
    auto segmentBoundaries = trajectory.getSegmentBoundaries();

    // Output of the trajectory
    std::cout << degree << "-dimensional polynomial trajectory with underlying splines:" << std::endl;
    for (unsigned int i = 0; i < trajectory.dimension(); i++) {
        char axis = '?';
        if (i == 0)
            axis = 'x';
        else if (i == 1)
            axis = 'y';
        else if (i == 2)
            axis = 'z';
        std::cout << axis << "(t):" << std::endl;
        for (unsigned int j = 0; j < segmentBoundaries[i].size() - 1; j++) {
            std::cout << axis << j << "(t) = " << trajectory.m_splines[i].m_segments[j].m_coefficients[0];
            for (unsigned int k = 1; k <= trajectory.degree(); k++) {
                std::cout << " + " << trajectory.m_splines[i].m_segments[j].m_coefficients[k] << "*t^" << k;
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    // Output of trajectory parameters
    // -------------------------------
    TrajectoryResult trajectoryResult; // Stores detailed error description
    SplineResult splineResult; // Stores detailed error description
    std::cout << "Verification if the spline was implemented correctly (1 if true): " << trajectory.isValid(&trajectoryResult, &splineResult) << std::endl; // Shows if the trajectory was implemented correctly
    std::cout << "degree = " << trajectory.degree() << std::endl; // Shows the maximum degree of the underlying polynomials
    std::cout << "order = " << trajectory.order() << std::endl; // Shows the order of the underlying polynomials
    std::cout << "dimension = " << trajectory.dimension() << std::endl // Shows the dimensions of the trajectory
              << std::endl;

    // Shows the boundaries of the spline segments
    for (unsigned int i = 0; i < segmentBoundaries.size(); i++) {
        std::cout << "Boundaries of spline " << i + 1 << ":" << std::endl;
        for (unsigned int j = 0; j < segmentBoundaries[i].size() - 1; j++) {
            std::cout << "Boundaries of spline segment " << j << ": " << segmentBoundaries[i][j] << " to " << segmentBoundaries[i][j + 1] << std::endl;
        }
        std::cout << std::endl;
    }

    // Evaluation methods of the trajectory
    // ------------------------------------
    auto resultEvaluate = trajectory.evaluate(time, derivationOrderEvaluate); // Evaluates the n-th derivation of the trajectory at the time t
    std::cout << "Evaluation of the trajectory:" << std::endl;
    std::cout << "x(" << time << ") = " << resultEvaluate[0] << std::endl;
    std::cout << "y(" << time << ") = " << resultEvaluate[1] << std::endl;
    std::cout << "z(" << time << ") = " << resultEvaluate[2] << std::endl
              << std::endl;

    auto resultEvaluateD0toDN = trajectory.evaluateD0ToDN<derivationOrderEvaluateD0toDN>(time); // Evaluates the derivatives 0 to N of the trajectory at the time t
    std::cout << "Evaluation of derivatives D0 to D" << derivationOrderEvaluateD0toDN << std::endl;
    for (unsigned int i = 0; i <= derivationOrderEvaluateD0toDN; i++) {
        std::cout << "d^" << i << "x/dt^" << i << " = " << resultEvaluateD0toDN[0][i] << std::endl;
        std::cout << "d^" << i << "y/dt^" << i << " = " << resultEvaluateD0toDN[1][i] << std::endl;
        std::cout << "d^" << i << "z/dt^" << i << " = " << resultEvaluateD0toDN[2][i] << std::endl
                  << std::endl;
    }

    // Plot the trajectory
    // -------------------
    plotTrajectory<PolynomialTrajectory<degree, dimensions>, dimensions>(trajectory, stepSizePlot, derivationOrderPlot, fileName); // Writes the files for plotting the underlying splines
    plotTrajectory3d<PolynomialTrajectory<degree, dimensions>, dimensions>(trajectory, stepSizePlot, derivationOrderPlot, fileName3d); // Writes the files for plotting the 3d trajectory

    std::cout << "The trajectory was succesfully plotted. To view the plots start gnuplot in your build repository via the Terminal." << std::endl;
    std::cout << "To view the plot of the underlying splines execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "To view the 3d plot of the trajectory execute the command: load \"" << fileName3d << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Main program function
int main()
{
    // Tutorial for polynomial trajectoris
    polynomialTrajectoryTutorial();

    // Quit with success
    return 0;
}
