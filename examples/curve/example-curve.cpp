/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include <fstream>
#include <iostream>
#include <string>

// Include headers from broccoli libary
#include <broccoli/curve/curves/ExponentialCurve.hpp>
#include <broccoli/curve/curves/PolynomialCurve.hpp>
#include <broccoli/curve/curves/TrigonometricCurve.hpp>

using namespace broccoli;
using namespace curve;

//! Generic function to plot a curve
/*!
 * \tparam EvaluatableObjectType curve type to plot
 * \param [in] evaluatableObject curve to plot
 * \param [in] xmin Minimum x-value
 * \param [in] xmax Maximum x-value
 * \param [in] stepSize Step size for sampling the x-range
 * \param [in] maximumDerivation Maximum derivation to be plotted
 * \param [in] fileName Filename of output file(s)
 */
template <class EvaluatableObjectType>
inline void plotGeneric(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "plot '" << fileName << ".dat' using 1:2 w l title \"Base function\"\n";
    for (unsigned int d = 1; d <= maximumDerivation; d++)
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (2 + d) << " w l title \"Derivative of order " << d << "\"\n";
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    const size_t steps = std::floor((xmax - xmin) / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = xmin + i * stepSize;
        dataFile << x << " ";

        // Evaluate base function
        dataFile << evaluatableObject.evaluate(x, 0);

        // Evaluate derivative
        for (unsigned int d = 1; d <= maximumDerivation; d++)
            dataFile << " " << evaluatableObject.evaluate(x, d);

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Tutorial for polynomial curves
/*! \param [in] interpolate If `true`, the curve is generated using interpolation, otherwise the coefficients are manually set */
void polynomialCurveTutorial(const bool& interpolate)
{
    // Creation of polynomial curve
    // ----------------------------
    const unsigned int degree = 3; // Degree of the polynomial curve
    PolynomialCurve<degree> polynomial; // Create polynomial curve (degree of the polynomial as template parameter)

    // Declaration of variables
    // ------------------------
    double position = 0.5; // Position where the curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    const unsigned int derivationOrderEvaluateD0toDN = 4; // Derivation order for the function evaluateD0toDN()
    unsigned int derivationOrderEvaluateNumericDerivative = 2; // Derivation order for the function evaluateNumericDerivative()
    unsigned int derivationOrderPlot = 1; // Derivation order for the plot
    double stepSize = 1e-6; // Step size for the function evaluateNumericDerivative()
    double stepSizePlot = 1e-2; // Step size for the plot
    double xminPlot = 0; // Starting position of the plot
    double xmaxPlot = 1; // End position of the plot
    std::string fileName = "PolynomialCurve"; // File in which the plot is written

    // Assigning parameters to the polynomial curve
    // --------------------------------------------
    if (interpolate == false) {
        // Directly set coefficients
        std::array<double, degree + 1> coefficients = { -1, 2, 3, -4 }; // Coefficients a0 to aN of the polynomial
        polynomial.m_coefficients = coefficients; // Assign coefficients a0 to aN (N = degree + 1)
        std::cout << "Polynomial Curve: " << coefficients[0];
        for (unsigned int i = 1; i <= degree; i++)
            std::cout << " + " << coefficients[i] << "*x^" << i;
        std::cout << std::endl
                  << std::endl;
    } else {
        // Automatically determine coefficients by interpolation
        std::vector<double> interpolationParameters = { 1, -1, 3, 0 }; // Interpolation parameters for POLYNOMIAL_CUBIC_FIRST_DERIVATIVES interpolation (y(x=0), y(x=1), y'(x=0), y'(x=1))
        CurveResult result; // Stores detailed error description
        polynomial.interpolate(interpolationParameters, CurveInterpolationMethod::POLYNOMIAL_CUBIC_FIRST_DERIVATIVES, &result); // Calculating the curve by interpolation method "POLYNOMIAL_CUBIC_FIRST_DERIVATIVES" with the before declared parameters, other interpolation methods can be found in the documentation
        std::cout << "Polynomial curve interpolated with parameters: y(x=0) = " << interpolationParameters[0] << ", y(x=1) = " << interpolationParameters[1] << ", y'(x=0) = " << interpolationParameters[2] << ", y'(x=1) = " << interpolationParameters[3] << ":" << std::endl;
        std::cout << polynomial.m_coefficients[0];
        for (unsigned int i = 1; i <= degree; i++)
            std::cout << " + " << polynomial.m_coefficients[i] << "*x^" << i;
        std::cout << std::endl
                  << std::endl;
    }

    // Output of curve attributes
    // --------------------------
    std::cout << "Function type: " << Curve::functionTypeString(polynomial.functionType()) << std::endl; // Shows function type
    std::cout << "Degree = " << polynomial.degree() << std::endl; // Shows degree of the polynomial
    std::cout << "Order = " << polynomial.order() << std::endl
              << std::endl; // Shows order of the polynomial

    // Evaluation methods of the curve
    // -------------------------------
    double resultEvaluate = polynomial.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the curve at the position x
    std::cout << "Evaluation of the function" << std::endl;
    std::cout << "y(" << position << ") = " << resultEvaluate << std::endl
              << std::endl;

    std::cout << "Evaluation of derivatives D0 to D" << derivationOrderEvaluateD0toDN << std::endl;
    auto resultEvaluateD0toDN = polynomial.evaluateD0ToDN<derivationOrderEvaluateD0toDN>(position); // Evaluates the derivatives 0 to N of the curve at the position x
    for (unsigned int i = 0; i <= derivationOrderEvaluateD0toDN; i++) {
        std::cout << "d^" << i << "y/dx^" << i << " = " << resultEvaluateD0toDN[i] << std::endl; // Shows the values of the before evaluated derivatives
    }
    std::cout << std::endl;

    double resultEvaluateNumericDerivative = polynomial.evaluateNumericDerivative(position, derivationOrderEvaluateNumericDerivative, stepSize); // Evaluates the n-th derivative at the position x using finite differrence method
    std::cout << "Numeric evaluation of D" << derivationOrderEvaluateNumericDerivative << std::endl;
    std::cout << "d^" << derivationOrderEvaluateNumericDerivative << "y/dx^" << derivationOrderEvaluateNumericDerivative << " = " << resultEvaluateNumericDerivative << std::endl
              << std::endl;

    // Plot the curve
    // --------------
    plotGeneric(polynomial, xminPlot, xmaxPlot, stepSizePlot, derivationOrderPlot, fileName); // Writes the plot files

    std::cout << "The curve was succesfully plotted. To view the plot start gnuplot in your build repository via the terminal and execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Tutorial for exponential curves
void exponentialCurveTutorial()
{
    ExponentialCurve exponential; // Create exponential curve

    // Declaration of variables
    // ------------------------
    std::array<double, 3> coefficients = { 1, 1, 1 }; // Coefficients a0 + a1 e^(a2*x) of the exponential curve
    double position = 1; // Position where the curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    const unsigned int derivationOrderEvaluateD0toDN = 2; // Derivation order for the function evaluateD0toDN()
    unsigned int derivationOrderEvaluateNumericDerivative = 1; // Derivation order for the function evaluateNumericDerivative()
    unsigned int derivationOrderPlot = 1; // Derivation order for the plot
    double stepSize = 1e-6; // Step size for the function evaluateNumericDerivative()
    double stepSizePlot = 1e-2; // Step size for the plot
    double xminPlot = 0; // Starting position of the plot
    double xmaxPlot = 1; // End position of the plot
    std::string fileName = "ExponentialCurve"; // File in which the plot is written

    // Assigning parameters to the exponential curve
    // ---------------------------------------------
    exponential.m_coefficients = coefficients; // Assign coefficients a0 + a1 * e^(a2*x)

    std::cout << "Exponential Curve: " << coefficients[0] << " + " << coefficients[1] << " * e^(" << coefficients[2] << "*x)" << std::endl
              << std::endl;

    // Output of curve attributes
    // --------------------------
    std::cout << "Function type: " << Curve::functionTypeString(exponential.functionType()) << std::endl
              << std::endl; //Shows function type

    // Evaluation methods of the curve
    // -------------------------------
    double resultEvaluate = exponential.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the curve at the position x
    std::cout << "Evaluation of the function" << std::endl;
    std::cout << "y(" << position << ") = " << resultEvaluate << std::endl
              << std::endl;

    std::cout << "Evaluation of derivatives D0 to D" << derivationOrderEvaluateD0toDN << std::endl;
    auto resultEvaluateD0toDN = exponential.evaluateD0ToDN<derivationOrderEvaluateD0toDN>(position); // Evaluates the derivatives 0 to N of the curve at the position x
    for (unsigned int i = 0; i <= derivationOrderEvaluateD0toDN; i++) {
        std::cout << "d^" << i << "y/dx^" << i << " = " << resultEvaluateD0toDN[i] << std::endl;
    }
    std::cout << std::endl;

    double resultEvaluateNumericDerivative = exponential.evaluateNumericDerivative(position, derivationOrderEvaluateNumericDerivative, stepSize); // Evaluates the n-th derivative at the position x using finite differrence method
    std::cout << "Numeric evaluation of D" << derivationOrderEvaluateNumericDerivative << std::endl;
    std::cout << "d^" << derivationOrderEvaluateNumericDerivative << "y/dx^" << derivationOrderEvaluateNumericDerivative << " = " << resultEvaluateNumericDerivative << std::endl
              << std::endl;

    // Plot the curve
    // --------------
    plotGeneric(exponential, xminPlot, xmaxPlot, stepSizePlot, derivationOrderPlot, fileName); // Writes the plot files

    std::cout << "The curve was succesfully plotted. To view the plot start gnuplot in your build repository via the terminal and execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Tutorial for trigonometric curves
void trigonometricCurveTutorial()
{
    TrigonometricCurve trigonometric; // Create trigonometric curve

    // Declaration of variables
    // ------------------------
    std::array<double, 4> coefficients = { 0, 1, 10, M_PI }; // Coefficients a0 + a1 * cos(a2 * x + a3) of the trigonometric curve
    double position = 0; // Position where the curve is evaluated
    unsigned int derivationOrderEvaluate = 0; // Derivation order for the function evaluate()
    const unsigned int derivationOrderEvaluateD0toDN = 2; // Derivation order for the function evaluateD0toDN()
    unsigned int derivationOrderEvaluateNumericDerivative = 1; // Derivation order for the function evaluateNumericDerivative()
    unsigned int derivationOrderPlot = 2; // Derivation order for the plot
    double stepSize = 1e-6; // Step size for the function evaluateNumericDerivative()
    double stepSizePlot = 1e-2; // Step size for the plot
    double xminPlot = 0; // Starting position of the plot
    double xmaxPlot = 1; // End position of the plot
    std::string fileName = "TrigonometricCurve"; // File in which the plot is written

    // Assigning parameters to the trigonometric curve
    // ---------------------------------------------
    trigonometric.m_coefficients = coefficients; // Assign coefficients a0 + a1 * cos(a2 * x + a3)
    trigonometric.m_type = TrigonometricCurve::Type::COSINE; // Assign type SINE, COSINE or TANGENT
    std::cout << "Trigonometric Curve: " << coefficients[0] << " + " << coefficients[1] << " * "
              << "cos(" << coefficients[2] << " * x + " << coefficients[3] << ")" << std::endl
              << std::endl;

    // Output of curve attributes
    // --------------------------
    std::cout << "Function type: " << Curve::functionTypeString(trigonometric.functionType()) << std::endl
              << std::endl; //Shows function type

    // Evaluation methods of the curve
    // -------------------------------
    double resultEvaluate = trigonometric.evaluate(position, derivationOrderEvaluate); // Evaluates the n-th derivation of the curve at the position x
    std::cout << "Evaluation of the function" << std::endl;
    std::cout << "y" << position << ") = " << resultEvaluate << std::endl
              << std::endl;

    std::cout << "Evaluation of derivatives D0 to D" << derivationOrderEvaluateD0toDN << std::endl;
    auto resultEvaluateD0toDN = trigonometric.evaluateD0ToDN<derivationOrderEvaluateD0toDN>(position); // Evaluates the derivatives 0 to N of the curve at the position x
    for (unsigned int i = 0; i <= derivationOrderEvaluateD0toDN; i++) {
        std::cout << "d^" << i << "y/dx^" << i << " = " << resultEvaluateD0toDN[i] << std::endl;
    }
    std::cout << std::endl;

    double resultEvaluateNumericDerivative = trigonometric.evaluateNumericDerivative(position, derivationOrderEvaluateNumericDerivative, stepSize); // Evaluates the n-th derivative at the position x using finite differrence method
    std::cout << "Numeric evaluation of D" << derivationOrderEvaluateNumericDerivative << std::endl;
    std::cout << "d^" << derivationOrderEvaluateNumericDerivative << "y/dx^" << derivationOrderEvaluateNumericDerivative << " = " << resultEvaluateNumericDerivative << std::endl
              << std::endl;

    // Plot the curve
    // --------------
    plotGeneric(trigonometric, xminPlot, xmaxPlot, stepSizePlot, derivationOrderPlot, fileName); // Writes the plot files
    std::cout << "The curve was succesfully plotted. To view the plot start gnuplot in your build repository via the terminal and execute the command: load \"" << fileName << ".gp\"" << std::endl;
    std::cout << "---------------------------------------------------------------------------------------------" << std::endl
              << std::endl;
}

//! Main program function
int main()
{
    const bool interpolate = true; // `false` for assigning coefficients, `true` for interpolating the curve

    // Tutorial for polynomial curves
    polynomialCurveTutorial(interpolate);

    // Tutorial for exponential curves
    exponentialCurveTutorial();

    // Tutorial for trigonometric curves
    trigonometricCurveTutorial();

    // Quit with success
    return 0;
}
