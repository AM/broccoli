/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/analysis/taskspace/TaskSpaceEvaluator.hpp"
#include "broccoli/geometry/DHParameters.hpp"

using namespace broccoli;
using namespace analysis;

//! Helper function to create a kinematic chain
/*!
 * As an example the kinematic structure of the Franka Emika Panda robot is implemented (source https://frankaemika.github.io/docs/control_parameters.html)
 *
 * \param [in] maximumTranslationStepSize Maximum allowed translational step size [m] of the TCP frame in the task space between two samples (should be smaller than the voxel size)
 * \return Kinematic chain to be used as input for the taskspace evaluator
 */
static inline KinematicChain createKinematicChain(const double& maximumTranslationStepSize)
{
    // Initialize return value
    KinematicChain chain;

    // Set TCP shift
    static const double tcpShift = 0.12; // Shift of TCP from x-axis of flange along z-axis of flange

    // Setup dimension
    static constexpr size_t D = 7; // DoF count: 7xjoint
    static constexpr size_t S = D + 1; // Segment count: DoF count + 1xflange/tcp

    // Setup DH parameters of Franka Emika Panda robot
    std::array<double, S> a{ { 0.0, 0.0, 0.0, 0.0825, -0.0825, 0.0, 0.088, 0.0 } }; // [m]
    std::array<double, S> alpha{ { 0.0, -M_PI_2, M_PI_2, M_PI_2, -M_PI_2, M_PI_2, M_PI_2, 0.0 } }; // [rad]
    std::array<double, S> d{ { 0.333, 0.0, 0.316, 0.0, 0.384, 0.0, 0.0, 0.107 + tcpShift } }; // [m]
    std::array<double, S> theta{ { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 } }; // [rad]

    // Setup joint limits of Franka Emika Panda robot
    std::array<double, D> minimumTheta{ { -2.8973, -1.7628, -2.8973, -3.0718, -2.8973, -0.0175, -2.8973 } }; // Minimum joint angle [rad]
    std::array<double, D> maximumTheta{ { 2.8973, 1.7628, 2.8973, -0.0698, 2.8973, 3.7525, 2.8973 } }; // Maximum joint angle [rad]

    // Setup segment list
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(S);
    std::string parentSegmentName = ""; // Name of the parent segment
    for (size_t i = 0; i < D; i++) {
        const std::string currentSegmentName = "joint" + std::to_string(i);
        KinematicChainSegment segment(currentSegmentName, parentSegmentName);
        segment.dofType() = KinematicChainSegment::DOFType::ROTATION;
        segment.initialTransform() = geometry::DHParameters(a[i], alpha[i], d[i], theta[i]).toTransform();
        segment.minimumPosition() = minimumTheta[i];
        segment.maximumPosition() = maximumTheta[i];
        segment.samples() = 2; // Will be overwritten by automatically computed sample count
        segmentList.push_back(segment);
        parentSegmentName = currentSegmentName;
    }

    // Create tcp
    KinematicChainSegment tcpSegment("tcp", parentSegmentName);
    tcpSegment.initialTransform() = geometry::DHParameters(a.back(), alpha.back(), d.back(), theta.back()).toTransform();
    segmentList.push_back(tcpSegment);

    // Set list
    chain.set(segmentList);

    // Use adaptive sampling
    chain.autoComputeSamples(maximumTranslationStepSize);

    // Pass back chain
    return chain;
}

int main()
{
    // Configuration
    const double minimumVoxelSize = 0.05; // Minimum size [m] of voxels in the taskspace grid

    // Setup input
    // -----------
    TaskSpaceEvaluatorInput<4> input;

    // Setup kinematic chain
    input.m_kinematicChain = createKinematicChain(minimumVoxelSize / 2.0);

    // Define task space (translation only)
    input.setupTaskSpaceSelectionMatrix(true, true, true, false, false, false);

    // Minimum voxel dimension
    input.m_minimumCellDimensions = Eigen::Vector3d(minimumVoxelSize, minimumVoxelSize, minimumVoxelSize);

    // Set output folder
    input.m_outputFolder = "taskspace-evaluator-output";

    // Output all possible meshes (all setting combinations)
    input.setupOutputMeshesAll();

    // Setup evaluator
    TaskSpaceEvaluator<4> evaluator(input);

    // Trigger pre-processing
    if (evaluator.preProcess() == false)
        return 1; // An error occured

    // Trigger processing
    if (evaluator.process() == false)
        return 1; // An error occured

    // Trigger post-processing
    if (evaluator.postProcess() == false)
        return 1; // An error occured

    // Exit program (success!)
    return 0;
}
