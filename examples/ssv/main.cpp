/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/geometry/ssv/SSVScene.hpp"
#include <csignal>
#include <iostream>

using namespace broccoli;
using namespace geometry;

// SIG handler
// (ensure clean shutdown in SIG)
volatile bool abortProgram = false;
void handle_sigint(int) { abortProgram = true; }

int main()
{
    // Init the signal handler
    std::signal(SIGINT, handle_sigint);

    /* Create some SSV elements
     * ------------------------
     * These are meant to be defined once and stay the same for the remainder. However, if
     * required, they implement methods to scale, rotate and translate. This may be used by
     * a file parser for example.
     */
    const SSVPointElement pointElement1(Eigen::Vector3d(-5, 0, 0), 1);
    const SSVPointElement pointElement2(Eigen::Vector3d(5, 0, 0), 1);
    const SSVLineElement lineElement(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(0, 0, 10), 1);
    const SSVTriangleElement triangleElement(Eigen::Vector3d(-5, 0, 0), Eigen::Vector3d(5, 0, 0), Eigen::Vector3d(0, 10, 0), 0.5);

    /* OPTIONAL: Check, if element definition is valid
     * -----------------------------------------------
     * In a file-parser one may use the isValid() methods to detect invalid element definitions (e.g. degenerated lines/triangles or invalid radii)
     */
    std::cout << "Validity of 'pointElement1' is: " << pointElement1.isValid() << std::endl;
    std::cout << "Validity of 'pointElement2' is: " << pointElement2.isValid() << std::endl;
    std::cout << "Validity of 'lineElement' is: " << lineElement.isValid() << std::endl;
    std::cout << "Validity of 'triangleElement' is: " << triangleElement.isValid() << std::endl;

    /* OPTIONAL: Compute distance between SSV elements
     * -----------------------------------------------
     * If one is only interested in the distance between two SSV elements, this computation can be triggered
     * directly without explicit definition of a SSV scene.
     */
    SSVElementDistance elementDistance;
    SSVDistanceEvaluator::evaluate(pointElement1, pointElement2, elementDistance);
    std::cout << "Distance from 'pointElement1' to 'pointElement2' = " << elementDistance.distance() << " (connection vector = [" << elementDistance.connection()(0) << ", " << elementDistance.connection()(1) << ", " << elementDistance.connection()(2) << "])" << std::endl;
    SSVDistanceEvaluator::evaluate(pointElement1, lineElement, elementDistance);
    std::cout << "Distance from 'pointElement1' to 'lineElement' = " << elementDistance.distance() << " (connection vector = [" << elementDistance.connection()(0) << ", " << elementDistance.connection()(1) << ", " << elementDistance.connection()(2) << "])" << std::endl;
    SSVDistanceEvaluator::evaluate(pointElement1, triangleElement, elementDistance);
    std::cout << "Distance from 'pointElement1' to 'triangleElement' = " << elementDistance.distance() << " (connection vector = [" << elementDistance.connection()(0) << ", " << elementDistance.connection()(1) << ", " << elementDistance.connection()(2) << "])" << std::endl;

    /* Create segments
     * ---------------
     * Segments are a collection of linked elements (each segment represents a "rigid body"). A segment
     * has a specific position and orientation in the scene and automatically computes the global
     * representation of its elements in the scene coordinate system.
     */
    // Create first segment (initialized at world origin)
    const SSVSegment::ID segment1ID = 123; // Remember its ID for easy access later on
    SSVSegment segment1(segment1ID, "my-segment-with-unique-id-" + std::to_string(segment1ID), 1, 1, 1);
    segment1.addElement(pointElement1);
    segment1.addElement(pointElement2);
    segment1.addElement(lineElement);
    segment1.addElement(triangleElement);

    // Create second segment (initialized at world origin)
    const SSVSegment::ID segment2ID = 456; // Remember its ID for easy access later on
    SSVSegment segment2(segment2ID, "my-segment-with-unique-id-" + std::to_string(segment2ID), 1, 1, 0);
    segment1.addElement(pointElement2);
    segment1.addElement(lineElement);

    // Create third segment (initialized at world origin)
    const SSVSegment::ID segment3ID = 789; // Remember its ID for easy access later on
    SSVSegment segment3(segment3ID, "my-segment-with-unique-id-" + std::to_string(segment3ID), 0, 1, 0);
    segment3.addElement(lineElement);

    /* OPTIONAL: Check, if segment definition is valid
     * -----------------------------------------------
     * In a file-parser one may use the isValid() methods to detect invalid segment definitions (e.g. degenerated lines/triangles or invalid radii)
     */
    std::cout << "Validity of 'segment1' is: " << segment1.isValid() << std::endl;
    std::cout << "Validity of 'segment2' is: " << segment2.isValid() << std::endl;
    std::cout << "Validity of 'segment3' is: " << segment3.isValid() << std::endl;

    /* OPTIONAL: Compute distance between SSV segments
     * -----------------------------------------------
     * If one is only interested in the distance between two SSV segments, this computation can be triggered
     * directly without explicit definition of a SSV scene.
     */
    SSVSegmentDistance segmentDistance;
    segment1.update(); // IMPORTANT: update the global transform of all of its elements (and its bounding boxes) before evaluating the distance
    segment2.update(); // IMPORTANT: update the global transform of all of its elements (and its bounding boxes) before evaluating the distance
    SSVDistanceEvaluator::evaluate(segment1, segment2, segmentDistance);
    std::cout << "Distance from 'segment1' to 'segment2' = " << segmentDistance.distance() << " (connection vector = [" << segmentDistance.connection()(0) << ", " << segmentDistance.connection()(1) << ", " << segmentDistance.connection()(2) << "])" << std::endl;
    segment2.setPosition(Eigen::Vector3d(1.0, 2.0, 3.0)); // Change the global position of the segment
    segment2.update(); // We have to update the segment, because we changed the position
    SSVDistanceEvaluator::evaluate(segment1, segment2, segmentDistance);
    std::cout << "Distance from 'segment1' to 'segment2' (shifted) = " << segmentDistance.distance() << " (connection vector = [" << segmentDistance.connection()(0) << ", " << segmentDistance.connection()(1) << ", " << segmentDistance.connection()(2) << "])" << std::endl;

    /* Create and setup SSV scene
     * --------------------------
     * The scene defines a collection of segments which can move (rotate and translate) through the space.
     * Moreover, the user specifies a collection of segment pairs which should be evaluated. Finally, one
     * can specify the execution policy.
     */
    // Create scene
    SSVScene scene("my-cool-ssv-scene");
    scene.setMaximumDistance(10); // OPTIONAL: set a maximum relevant distance (What is too far away?) - allows accelerated evaluation (uses bounding-boxes internally)

    // Add segments to scene
    scene.addSegment(segment1);
    scene.addSegment(segment3); // <- the order does not matter (once added, segments in a scene are identified by their unique ID)!
    scene.addSegment(segment2);

    // Define segment pairs to evaluate (only for these segments a distance will be computed) - order does not matter
    scene.addPair(segment1ID, segment2ID);
    scene.addPair(segment1ID, segment3ID);
    // (Note: if one is interested in all possible distances one might call setPairsAll() instead)

    /* OPTIONAL: Remove unused segments and invalid pairs
     * --------------------------------------------------
     * If the user adds lots of segments and pairs some mistakes might happen. For example the user might add a segment which is not used in any pair (e.g. because
     * the corresponding pair has been removed but the user forgot to remove the corresponding segments from the scene). Or the user removes a segment, but not all
     * pairs which refer to this segment. Although evaluation is still save (because such cases are just skipped), this unneccessarily consumes processing power. For
     * such cases the scene provides a clean-up routine which gets rid of unused segments and invalid pairs.
     */
    const std::pair<size_t, size_t> cleanUpResult = scene.cleanUp();
    std::cout << "Cleanup (removed unused segments: " << cleanUpResult.first << ", removed invalid pairs: " << cleanUpResult.second << ")" << std::endl;

    /* OPTIONAL: Set multi-threading parameters
     * ----------------------------------------
     * A SSV scene allows the user to automatically evaluate all specified segment pairs one after the other (single-threaded) or
     * in parallel (multi-threaded). For this the scene has setters to specify the thread-priority and thread-count for both, pre-processing
     * and processing. Moreover, the scene allows to auto-detect the optimal choice (for minimum runtime).
     */
    scene.setThreadPriorityPreProcessing(0); // Has an impact on LinuxRT or QNX Neutrino for example
    scene.setThreadPriorityProcessing(0); // Has an impact on LinuxRT or QNX Neutrino for example
    scene.setThreadCountPreProcessing(1); // Use only one thread (single-threaded) for pre-processing
    scene.setThreadCountProcessing(2); // Use two-threads (multi-threaded) for processing

    // Optional: uncomment to automatically determine the optimal choice of thread-counts (pre-processing and processing) - WARNING: this call might take some time!
    //scene.autoTune();

    /* Evaluation
     * ----------
     * Typically, distance evaluations with SSVs are run contiuously (online). Thus, we assume that there
     * is an execution loop, where in each iteration the position and orientation of (some) segments change and
     * the new distances have to be computed.
     */
    // Main Execution loop
    Eigen::Vector3d position = Eigen::Vector3d::Zero();
    while (!abortProgram) {
        // Step 1: update positions (and orientations) of all (moving) segments
        // --------------------------------------------------------------------
        position += Eigen::Vector3d(1e-1, 0.0, -1e-1); // Update "animated" position (simulate some motion)
        scene.setSegmentPosition(segment1ID, position); // Translate segment

        // Step 2: evaluate scene
        // ----------------------
        scene.evaluate();

        // Step 3: process results
        // -----------------------
        // Get count of evaluated distances (may be empty, if maximum relevant distance is exceeded by all segment pairs)
        const size_t distanceCount = scene.distances().size();
        std::cout << "Scene evaluation: distance-count=" << distanceCount;
        if (distanceCount > 0) {
            // If there is at least one evaluated distance we can get the minimum of all distances
            const auto minimumDistance = scene.minimumDistance();
            // (Note: one has access to all evaluated distances by using scene.distances())

            // Print additional information
            std::cout << ", minimum-distance=" << minimumDistance.distance();
            std::cout << " (first-segment-ID: " << minimumDistance.firstID() << ", second-segment-ID: " << minimumDistance.secondID() << ")";
        }
        std::cout << " (runtime=" << scene.runTime().encodeToDurationString() << ")";
        std::cout << std::endl;

        // Sleep a bit to slow down console output
        core::Time::sleep(0.1);
    }

    // Exit program (success!)
    return 0;
}
