/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include <broccoli/io/joystick/Joystick.hpp>
#include <iomanip>
#include <iostream>

using namespace broccoli;
using namespace io;

int main()
{
    // Initialize joystick
    Joystick joystick(0, "UnspecifiedDevice");
    joystick.initialize();

    // Wait until background worker of joystick was executed at least once
    while (joystick.executionCalls() <= 1) {
        // Wait...
    }

    // Output user information
    std::cout << "Joystick Example Application" << std::endl;
    std::cout << "============================" << std::endl;

    // Get list of available devices
    std::vector<JoystickInformation> availableJoysticks = joystick.availableJoysticks();
    std::cout << "Available devices:" << std::endl;
    std::cout << "------------------" << std::endl;
    for (size_t i = 0; i < availableJoysticks.size(); i++) {
        std::cout << "Index: " << i << std::endl;
        std::cout << ">>>Name: " << availableJoysticks[i].m_name << std::endl;
        std::cout << ">>>GUID: " << availableJoysticks[i].m_GUIDstring << std::endl;
        std::cout << ">>>Count of Axes: " << availableJoysticks[i].m_numberOfAxes << std::endl;
        std::cout << ">>>Count of balls: " << availableJoysticks[i].m_numberOfBalls << std::endl;
        std::cout << ">>>Count of buttons: " << availableJoysticks[i].m_numberOfButtons << std::endl;
        std::cout << ">>>Count of hats: " << availableJoysticks[i].m_numberOfHats << std::endl;
        std::cout << ">>>Is haptic: " << (uint64_t)availableJoysticks[i].m_isHaptic << std::endl;
        std::cout << ">>>Power level: " << availableJoysticks[i].m_powerLevel << std::endl;
    }
    if (availableJoysticks.size() == 0)
        std::cout << "No devices available!" << std::endl;

    // Get users choice
    std::cout << std::endl;
    std::cout << "Choose device by index:" << std::endl;
    int deviceIndex;
    std::cin >> deviceIndex;
    if (deviceIndex < 0)
        deviceIndex = 0;
    if (deviceIndex >= (int)availableJoysticks.size())
        deviceIndex = 0;
    std::cout << "Choosen device: " << deviceIndex << " (" << availableJoysticks[deviceIndex].m_name << ")" << std::endl;
    joystick.setDesiredDeviceName(availableJoysticks[deviceIndex].m_name);

    // Wait until joystick is connected
    std::cout << "Connecting to joystick...";
    while (joystick.isConnected() == false) {
    }
    std::cout << "...connected!" << std::endl;

    // Loop (until joystick gets disconnected)
    std::cout << std::setfill(' ');
    while (joystick.isConnected() == true) {
        // Get state of joystick
        JoystickState joystickState = joystick.currentState();

        // Output state of joystick
        // ------------------------
        // Output state of axes
        for (size_t i = 0; i < joystickState.m_axisValue.size(); i++)
            std::cout << std::setw(6) << std::showpos << joystickState.m_axisValue[i] << std::noshowpos << " ";

        // Output state of hats
        for (size_t i = 0; i < joystickState.m_hatValue.size(); i++)
            std::cout << (int)joystickState.m_hatValue[i] << " ";

        // Output state of buttons
        for (size_t i = 0; i < joystickState.m_buttonPressed.size(); i++)
            std::cout << (int)joystickState.m_buttonPressed[i] << " ";

        std::cout << std::endl;

        // Sleep a bit to avoid spamming the console
        core::Time::sleep(0.05);
    }

    // Deinitialize joystick
    joystick.deInitialize();

    // Exit program
    return 0;
}
