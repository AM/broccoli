#
# Broccoli package config file
#


# Find depencencies
include(CMakeFindDependencyMacro)

# There are optional components of broccoli.
# These are mapped to the dependencies of the broccoli headers:
set(_optional_broccoli_dependencies eigen sdl zlib)

# List of modules
set(MODULE_NAMES
    broccoli
)


# Macro to search for a specific module
macro(find_module FILENAME)
    if(EXISTS "${FILENAME}")
        set(broccoli_FOUND TRUE)
        include("${FILENAME}")
    endif()
endmacro()

# Macro to search for all modules
macro(find_modules PREFIX)
    foreach(module_name ${MODULE_NAMES})
        if(TARGET ${module_name})
            set(broccoli_FOUND TRUE)
        else()
            find_module("${CMAKE_CURRENT_LIST_DIR}/${PREFIX}/${module_name}-export.cmake")
        endif()
    endforeach(module_name)
endmacro()


# Try install location
set(broccoli_FOUND FALSE)
find_modules("cmake")

# Try common build locations
if(NOT broccoli_FOUND)
    if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
        find_modules("build-debug/cmake")
        find_modules("build/cmake")
    else()
        find_modules("build/cmake")
        find_modules("build-debug/cmake")
    endif()
endif()

# check the name of the imported broccoli target
set(_broccoli_imported_target_name eat::broccoli)

# broccoli itself is only available within its own project
if (TARGET broccoli)
    set(_broccoli_imported_target_name broccoli)
endif()

# load components
foreach(_comp ${broccoli_FIND_COMPONENTS})
    list(FIND _optional_broccoli_dependencies "${_comp}" _index)
    if (_index MATCHES -1)
        message("invalid: ${_comp}")
        set(broccoli_FOUND False)
        set(broccoli_NOTFOUND_MESSAGE "Specified unsupported component: ${_comp}")
    endif()
    include("${CMAKE_CURRENT_LIST_DIR}/cmake/broccoli-${_comp}.cmake")
endforeach()
