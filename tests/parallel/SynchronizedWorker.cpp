/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/parallel/SynchronizedWorker.hpp"
#include "gtest/gtest.h"
#include <string>

using namespace broccoli;
using namespace parallel;

//! Definition of a simple operation class
class SimpleOperation {
public:
    SimpleOperation(const int& a)
        : m_a(a)
    {
    }

    // Members
    int m_a = 0;
    int m_b = 0;

    // Processing function
    inline void process()
    {
        m_b = m_a * m_a;
    }
};

//! Check basic usage of a synchronized worker
TEST(SynchronizedWorker, BasicUsage)
{
    // Create list of operations
    const size_t operationsPerWorker = 5;
    const size_t operationCount = 2 * operationsPerWorker;
    std::vector<SimpleOperation> operationList;
    operationList.reserve(operationCount);
    for (size_t i = 0; i < operationCount; i++)
        operationList.push_back(SimpleOperation(i));

    // Create worker pool
    SynchronizedWorker<decltype(operationList)::iterator> workerA("WorkerA", 0);
    SynchronizedWorker<decltype(operationList)::iterator> workerB("WorkerB", 0);
    ASSERT_FALSE(workerA.ready());
    ASSERT_FALSE(workerB.ready());

    // Initialize workers
    ASSERT_TRUE(workerA.initialize());
    ASSERT_TRUE(workerB.initialize());

    // Wait until all workers are ready
    while (true) {
        if (workerA.ready() == true && workerB.ready() == true)
            break;
    }

    // Trigger processing of operation list
    workerA.trigger(operationList.begin(), operationsPerWorker);
    workerB.trigger(operationList.begin() + operationsPerWorker, operationsPerWorker);

    // Wait until all workers are finished
    while (true) {
        if (workerA.finished() == true && workerB.finished() == true)
            break;
    }

    // Check result
    for (size_t i = 0; i < operationCount; i++)
        ASSERT_EQ(operationList[i].m_b, operationList[i].m_a * operationList[i].m_a);

    // De-initialize workers
    ASSERT_TRUE(workerA.deInitialize());
    ASSERT_TRUE(workerB.deInitialize());
}
