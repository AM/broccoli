/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/parallel/ThreadSafeContainer.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace parallel;

//! Mockup class for testing
class ThreadSafeContainerMockup : public ThreadSafeContainer {
public:
    //! Basic test of locking mechanisms
    void testLocking()
    {
        // Test basic locking
        ASSERT_TRUE(lockForRead());
        ASSERT_TRUE(unlock());
        ASSERT_TRUE(lockForWrite());
        ASSERT_TRUE(unlock());

        // Test try locking
        ASSERT_TRUE(lockForWrite());
        ASSERT_FALSE(tryLockForRead());
        ASSERT_FALSE(tryLockForWrite());
        ASSERT_TRUE(unlock());
        ASSERT_TRUE(tryLockForRead());
        ASSERT_TRUE(unlock());
        ASSERT_TRUE(tryLockForWrite());
        ASSERT_TRUE(unlock());
    }

    //! Basic test of setters and getters
    void testSettersAndGetters()
    {
        // Initialize helpers
        double testValue = 0;
        const double expectedTestValue = 1.23;

        // Basic setting and getting
        setProtectedData(m_protectedValue, expectedTestValue);
        testValue = getProtectedData(m_protectedValue);
        ASSERT_EQ(testValue, expectedTestValue);

        // Addition template
        setProtectedData(m_protectedValue, 1.0);
        protectedDataAddition(m_protectedValue, 1.0);
        testValue = getProtectedData(m_protectedValue);
        ASSERT_EQ(testValue, 2.0);

        // Multiplication template
        setProtectedData(m_protectedValue, 2.0);
        protectedDataMultiplication(m_protectedValue, 10.0);
        testValue = getProtectedData(m_protectedValue);
        ASSERT_EQ(testValue, 20.0);
    }

    //! Pass-through for testing
    bool lock() { return ThreadSafeContainer::tryLockForWrite(); }

    //! Pass-through for testing
    bool unlock() { return ThreadSafeContainer::unlock(); }

private:
    double m_protectedValue = 0.0;
    std::string m_protectedString = "";
};

//! Check creation and handling of thread-safe data containers
TEST(ThreadSafeContainer, BasicLocking)
{
    ThreadSafeContainerMockup container;
    container.testLocking();
}

//! Check copy constructor
TEST(ThreadSafeContainer, CopyConstructor)
{
    ThreadSafeContainerMockup containerA;
    ThreadSafeContainerMockup containerB(containerA);
    ASSERT_TRUE(containerA.lock());
    ASSERT_TRUE(containerB.lock());
    ASSERT_TRUE(containerA.unlock());
    ASSERT_TRUE(containerB.unlock());
}

//! Check copy assignment operator
TEST(ThreadSafeContainer, CopyAssignment)
{
    ThreadSafeContainerMockup containerA;
    ThreadSafeContainerMockup containerB;
    containerB = containerA;
    ASSERT_TRUE(containerA.lock());
    ASSERT_TRUE(containerB.lock());
    ASSERT_TRUE(containerA.unlock());
    ASSERT_TRUE(containerB.unlock());
}

//! Check setters and getters
TEST(ThreadSafeContainer, SettersAndGetters)
{
    ThreadSafeContainerMockup container;
    container.testSettersAndGetters();
}
