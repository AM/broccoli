/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/parallel/BackgroundWorker.hpp"
#include "gtest/gtest.h"
#include <string>

using namespace broccoli;
using namespace parallel;

//! Derived background worker class for basic tests
class BasicWorker : public BackgroundWorker {
public:
    BasicWorker(const std::string& name, const bool& multiThreaded, const int& threadPriority, const core::Time& minimumCycleTime, const bool& blockingWaiting)
        : BackgroundWorker(name, multiThreaded, threadPriority, minimumCycleTime, blockingWaiting)
        , m_testCounter(0)
    {
    }

private:
    //! Copy constructor (internal) (**not** thread-safe -> should only be called by the thread-safe wrapper)
    /*! \param [in] original Reference to original object. */
    BasicWorker(const BasicWorker& original, const int& /* <- trick used for locking mutex */)
        : BackgroundWorker(original)
        , m_testCounter(original.m_testCounter)
    {
    }

public:
    //! Copy constructor (wrapper) (**thread-safe**)
    /*!
     * Locks the mutex of the original object for reading before calling the internal (protected) constructor. Unlocks original object afterwards.
     * \param [in] original Reference to original object.
     */
    BasicWorker(const BasicWorker& original)
        : BasicWorker(original, original.lockForRead() /* <- lock mutex of original object first (for reading since also using "const") */)
    {
        original.unlock(); // Unlock mutex of original object after internal (protected) constructor has been called
    }

    //! Copy assignment operator (**thread-safe**)
    /*!
     * Uses own mutex and mutex of the reference object to guarantee thread-safe copying of members.
     * \param [in] reference Reference to reference object.
     * \return Pointer to this instance.
     */
    BasicWorker& operator=(const BasicWorker& reference)
    {
        // Avoid self-assignment
        if (this == &reference)
            return *this;

        // Try to lock ourselves and reference (while avoiding deadlocks)
        while (true) { // spinning
            lockForWrite(); // Lock ourselves for writing (blocking)
            if (reference.tryLockForRead() == true) // Try to lock reference for reading
                break; // ...success -> stop spinning
            else
                unlock(); // ...fail -> unlock ourselves to allow other threads to access us and thus resolve possible deadlocks
        }

        // Copy data
        BackgroundWorker::operator=(reference);
        m_testCounter = reference.m_testCounter;

        // Unlock reference object and ourselves
        reference.unlock();
        unlock();

        return *this;
    }

    virtual void execute()
    {
        // Increment counter by 1
        int currentCounter = testCounter();
        currentCounter++;
        setTestCounter(currentCounter);

        // Stop after a certain count of executions
        if (currentCounter >= 10)
            stop();
    }

    // Protected worker data
    int m_testCounter;

    // Getters
    int testCounter() const { return getProtectedData(m_testCounter); }

    // Setters
    void setTestCounter(const int& newValue) { setProtectedData(m_testCounter, newValue); }
};

//! Derived background worker class for test of abortion
class AbortWorker : public BackgroundWorker {
public:
    AbortWorker(const std::string& name, const bool& multiThreaded, const int& threadPriority, const core::Time& minimumCycleTime, const bool& blockingWaiting)
        : BackgroundWorker(name, multiThreaded, threadPriority, minimumCycleTime, blockingWaiting)
        , m_enableEmergencyExit(false)
    {
    }

private:
    //! Copy constructor (internal) (**not** thread-safe -> should only be called by the thread-safe wrapper)
    /*! \param [in] original Reference to original object. */
    AbortWorker(const AbortWorker& original, const int& /* <- trick used for locking mutex */)
        : BackgroundWorker(original)
        , m_enableEmergencyExit(original.m_enableEmergencyExit)
    {
    }

public:
    //! Copy constructor (wrapper) (**thread-safe**)
    /*!
     * Locks the mutex of the original object for reading before calling the internal (protected) constructor. Unlocks original object afterwards.
     * \param [in] original Reference to original object.
     */
    AbortWorker(const AbortWorker& original)
        : AbortWorker(original, original.lockForRead() /* <- lock mutex of original object first (for reading since also using "const") */)
    {
        original.unlock(); // Unlock mutex of original object after internal (protected) constructor has been called
    }

    //! Copy assignment operator (**thread-safe**)
    /*!
     * Uses own mutex and mutex of the reference object to guarantee thread-safe copying of members.
     * \param [in] reference Reference to reference object.
     * \return Pointer to this instance.
     */
    AbortWorker& operator=(const AbortWorker& reference)
    {
        // Avoid self-assignment
        if (this == &reference)
            return *this;

        // Try to lock ourselves and reference (while avoiding deadlocks)
        while (true) { // spinning
            lockForWrite(); // Lock ourselves for writing (blocking)
            if (reference.tryLockForRead() == true) // Try to lock reference for reading
                break; // ...success -> stop spinning
            else
                unlock(); // ...fail -> unlock ourselves to allow other threads to access us and thus resolve possible deadlocks
        }

        // Copy data
        BackgroundWorker::operator=(reference);
        m_enableEmergencyExit = reference.m_enableEmergencyExit;

        // Unlock reference object and ourselves
        reference.unlock();
        unlock();

        return *this;
    }

    virtual void execute()
    {
        // Simulate heavy load
        for (uint64_t i = 0; i < 10000; i++) {
            // Emergency exit (if enabled)
            if (enableEmergencyExit() == true && stopTriggered() == true)
                break;
            // else: do nothing
        }
    }

    // Protected worker data
    bool m_enableEmergencyExit;

    // Getters
    int enableEmergencyExit() const { return getProtectedData(m_enableEmergencyExit); }

    // Setters
    void setEnableEmergencyExit(const bool& newValue) { setProtectedData(m_enableEmergencyExit, newValue); }
};

//! Test setters and getters
TEST(BackgroundWorker, SettersAndGetters)
{
    // Create instance
    BasicWorker worker("Worker", false, 0, core::Time(0.1), false);

    // Setters and getters
    // ...no public setters

    // Getters only
    std::string name = worker.name();
    ASSERT_EQ(name, "Worker");
    bool multiThreaded = worker.multiThreaded();
    ASSERT_EQ(multiThreaded, false);
    int threadPriority = worker.threadPriority();
    ASSERT_EQ(threadPriority, 0);
    core::Time minimumCycleTime = worker.minimumCycleTime();
    ASSERT_EQ(minimumCycleTime, core::Time(0.1));
    bool blockingWaiting = worker.blockingWaiting();
    ASSERT_EQ(blockingWaiting, false);
    bool threadRunning = worker.threadRunning();
    ASSERT_EQ(threadRunning, false);
    bool startTriggered = worker.startTriggered();
    ASSERT_EQ(startTriggered, false);
    bool stopTriggered = worker.stopTriggered();
    ASSERT_EQ(stopTriggered, false);
    bool joinTriggered = worker.joinTriggered();
    ASSERT_EQ(joinTriggered, false);
    bool pauseActive = worker.pauseActive();
    ASSERT_EQ(pauseActive, false);
    core::Time lastCycleTime = worker.lastCycleTime();
    (void)lastCycleTime;
    core::Time lastExecutionCallTime = worker.lastExecutionCallTime();
    (void)lastExecutionCallTime;
    core::Time totalExecutionRunTime = worker.totalExecutionRunTime();
    (void)totalExecutionRunTime;
    unsigned long long executionCalls = worker.executionCalls();
    ASSERT_EQ(executionCalls, 0);
}

//! Check creation, execution and stopping of background workers in single-threaded mode
TEST(BackgroundWorker, SingleThreaded)
{
    // Create workers
    BasicWorker workerA("WorkerASingleThreaded", false, 0, core::Time(0.1), false);
    BasicWorker workerB("WorkerBSingleThreaded", false, 0, core::Time(0.1), false);

    // Copy constructor
    BasicWorker workerCopy(workerA);
    ASSERT_TRUE(workerCopy.name() == workerA.name());

    // Assignment operator
    workerCopy = workerB;
    ASSERT_TRUE(workerCopy.name() == workerB.name());

    // Check execution counter
    ASSERT_EQ(workerA.executionCalls(), 0);
    ASSERT_EQ(workerB.executionCalls(), 0);

    // Check member access
    ASSERT_EQ(workerA.testCounter(), 0);
    ASSERT_EQ(workerB.testCounter(), 0);

    // Start workers
    ASSERT_EQ(workerA.start(), true);
    ASSERT_EQ(workerB.start(), true);

    // Check execution counter
    ASSERT_EQ(workerA.executionCalls(), 0);
    ASSERT_EQ(workerB.executionCalls(), 0);

    // Trigger initial execution
    workerA.triggerManualExecution();
    workerB.triggerManualExecution();
    core::Time runTimeA0 = workerA.lastExecutionRunTime();
    core::Time runTimeB0 = workerB.lastExecutionRunTime();

    // Check execution counter
    ASSERT_EQ(workerA.executionCalls(), 1);
    ASSERT_EQ(workerB.executionCalls(), 1);

    // Check member access
    ASSERT_EQ(workerA.testCounter(), 1);
    ASSERT_EQ(workerB.testCounter(), 1);

    // Trigger execution and get runtimes
    workerA.triggerManualExecution();
    workerB.triggerManualExecution();
    core::Time runTimeA1 = workerA.lastExecutionRunTime();
    core::Time runTimeB1 = workerB.lastExecutionRunTime();

    // Check pause
    workerA.pause();
    workerB.pause();
    workerA.triggerManualExecution();
    workerB.triggerManualExecution();
    ASSERT_EQ(workerA.executionCalls(), 2);
    ASSERT_EQ(workerB.executionCalls(), 2);

    // Check resume
    workerA.resume();
    workerB.resume();
    workerA.triggerManualExecution();
    workerB.triggerManualExecution();
    core::Time runTimeA2 = workerA.lastExecutionRunTime();
    core::Time runTimeB2 = workerB.lastExecutionRunTime();

    // Check execution counter
    ASSERT_EQ(workerA.executionCalls(), 3);
    ASSERT_EQ(workerB.executionCalls(), 3);

    // Check total runtime
    core::Time runTimeAT = workerA.totalExecutionRunTime();
    core::Time runTimeBT = workerB.totalExecutionRunTime();
    ASSERT_EQ(runTimeAT, runTimeA0 + runTimeA1 + runTimeA2);
    ASSERT_EQ(runTimeBT, runTimeB0 + runTimeB1 + runTimeB2);

    // Check member access
    ASSERT_EQ(workerA.testCounter(), 3);
    ASSERT_EQ(workerB.testCounter(), 3);

    // Check stopping
    ASSERT_EQ(workerA.stop(), true);
    ASSERT_EQ(workerB.stop(), true);

    // Trigger execution
    workerA.triggerManualExecution();
    workerB.triggerManualExecution();

    // Check execution counter
    ASSERT_EQ(workerA.executionCalls(), 3);
    ASSERT_EQ(workerB.executionCalls(), 3);

    // Check joining
    ASSERT_EQ(workerA.join(core::Time(0)), true);
    ASSERT_EQ(workerB.join(core::Time(0)), true);
}

//! Check creation, execution and stopping of background workers in multi-threaded mode
TEST(BackgroundWorker, MultiThreadedBasic)
{
    // Create workers
    BasicWorker workerA("WorkerAMultiThreaded", true, 0, core::Time(0.010), false);
    BasicWorker workerB("WorkerBMultiThreaded", true, 0, core::Time(0.025), false);

    // Check execution counter
    ASSERT_EQ(workerA.executionCalls(), 0);
    ASSERT_EQ(workerB.executionCalls(), 0);

    // Check member access
    ASSERT_EQ(workerA.testCounter(), 0);
    ASSERT_EQ(workerB.testCounter(), 0);
    workerB.setTestCounter(8); // Start already at 8

    // Start workers
    ASSERT_EQ(workerA.start(), true);
    core::Time::sleep(0.001); // Wait some time to simulate different starting times
    ASSERT_EQ(workerB.start(), true);

    // Wait until both threads have finished their work
    while (workerA.testCounter() < 10 || workerB.testCounter() < 10) {
    }

    // Try to join counters
    ASSERT_EQ(workerA.join(core::Time(-1)), true);
    ASSERT_EQ(workerB.join(core::Time(-1)), true);

    // Check execution counter
    ASSERT_EQ(workerA.executionCalls(), 10);
    ASSERT_EQ(workerB.executionCalls(), 2);

    // Check member access
    ASSERT_EQ(workerA.testCounter(), 10);
    ASSERT_EQ(workerB.testCounter(), 10);
}

//! Check aborting worker
TEST(BackgroundWorker, MultiThreadedAbort)
{
    // Create workers
    AbortWorker workerA("WorkerAWithEmergencyExit", true, 0, core::Time(0), true);
    AbortWorker workerB("WorkerBWithoutEmergencyExit", true, 0, core::Time(0), true);

    // Check aborting with usual emergency exit (should be implemented by user)
    workerA.setEnableEmergencyExit(true);
    ASSERT_EQ(workerA.start(), true);
    core::Time::sleep(0.01); // Wait some time and then try to stop
    ASSERT_EQ(workerA.join(core::Time(-1)), true);

    // Check aborting without usual emergency exit (however with reasonable execution time)
    workerB.setEnableEmergencyExit(false);
    ASSERT_EQ(workerB.start(), true);
    core::Time::sleep(0.01); // Wait some time and then try to stop
    ASSERT_EQ(workerB.join(core::Time(-1)), true);
}

//! Derived background worker class for test of synchronization
class SyncWorker : public BackgroundWorker {
public:
    SyncWorker(const core::Time& timeout = 0)
        : BackgroundWorker("SyncWorker", true, 0, 0.001, false)
        , m_synchronizationTimeout(timeout)
        , m_synchronizationResult(false)
        , m_synchronizationResumeTime(0)
    {
    }

private:
    //! Copy constructor (internal) (**not** thread-safe -> should only be called by the thread-safe wrapper)
    /*! \param [in] original Reference to original object. */
    SyncWorker(const SyncWorker& original, const int& /* <- trick used for locking mutex */)
        : BackgroundWorker(original)
        , m_synchronizationTimeout(original.m_synchronizationTimeout)
        , m_synchronizationResult(original.m_synchronizationResult)
        , m_synchronizationResumeTime(original.m_synchronizationResumeTime)
    {
    }

public:
    //! Copy constructor (wrapper) (**thread-safe**)
    /*!
     * Locks the mutex of the original object for reading before calling the internal (protected) constructor. Unlocks original object afterwards.
     * \param [in] original Reference to original object.
     */
    SyncWorker(const SyncWorker& original)
        : SyncWorker(original, original.lockForRead() /* <- lock mutex of original object first (for reading since also using "const") */)
    {
        original.unlock(); // Unlock mutex of original object after internal (protected) constructor has been called
    }

    //! Copy assignment operator (**thread-safe**)
    /*!
     * Uses own mutex and mutex of the reference object to guarantee thread-safe copying of members.
     * \param [in] reference Reference to reference object.
     * \return Pointer to this instance.
     */
    SyncWorker& operator=(const SyncWorker& reference)
    {
        // Avoid self-assignment
        if (this == &reference)
            return *this;

        // Try to lock ourselves and reference (while avoiding deadlocks)
        while (true) { // spinning
            lockForWrite(); // Lock ourselves for writing (blocking)
            if (reference.tryLockForRead() == true) // Try to lock reference for reading
                break; // ...success -> stop spinning
            else
                unlock(); // ...fail -> unlock ourselves to allow other threads to access us and thus resolve possible deadlocks
        }

        // Copy data
        BackgroundWorker::operator=(reference);
        m_synchronizationTimeout = reference.m_synchronizationTimeout;
        m_synchronizationResult = reference.m_synchronizationResult;
        m_synchronizationResumeTime = reference.m_synchronizationResumeTime;

        // Unlock reference object and ourselves
        reference.unlock();
        unlock();

        return *this;
    }

    virtual void execute()
    {
        // Initialize helpers
        const core::Time timeout = synchronizationTimeout();

        // Perform synchronization
        const bool result = waitForSynchronization(timeout);
        const core::Time resumeTime = core::Time::currentTime();

        // Remember results
        setSynchronizationResult(result);
        setSynchronizationResumeTime(resumeTime);

        // Stop execution
        stop();
    }

    // Protected worker data
    core::Time m_synchronizationTimeout; //!< Timeout to be used for waitForSynchronization()
    bool m_synchronizationResult; //!< Return value of waitForSynchronization()
    core::Time m_synchronizationResumeTime; //!< Time when thread resumes from waiting for synchronization

    // Getters
    core::Time synchronizationTimeout() const { return getProtectedData(m_synchronizationTimeout); }
    bool synchronizationResult() const { return getProtectedData(m_synchronizationResult); }
    core::Time synchronizationResumeTime() const { return getProtectedData(m_synchronizationResumeTime); }

    // Setters
    void setSynchronizationTimeout(const core::Time& newValue) { setProtectedData(m_synchronizationTimeout, newValue); }
    void setSynchronizationResult(const bool& newValue) { setProtectedData(m_synchronizationResult, newValue); }
    void setSynchronizationResumeTime(const core::Time& newValue) { setProtectedData(m_synchronizationResumeTime, newValue); }
};

//! Check synchronization without timeout
TEST(BackgroundWorker, SynchronizationWithoutTimeout)
{
    // Initialize helpers
    bool triggerResult = false;

    // Create worker
    SyncWorker worker;

    // Trigger synchronization (without waiting for it)
    triggerResult = worker.triggerSynchronization();
    ASSERT_EQ(triggerResult, false); // false since we did not wait for synchronization

    // Start worker
    ASSERT_EQ(worker.start(), true);

    // Wait until worker waits for synchronization
    while (worker.syncWaiting() == false)
        core::Time::sleep(0.001);

    // Trigger synchronization
    const core::Time synchronizationTriggerTime = core::Time::currentTime();
    triggerResult = worker.triggerSynchronization();
    ASSERT_EQ(triggerResult, true); // true because we waited for synchronization

    // Wait until worker stopped waiting (i.e. received the signal)
    while (worker.syncWaiting() == true)
        core::Time::sleep(0.001);

    // Stop worker
    ASSERT_EQ(worker.join(core::Time(-1)), true);

    // Check synchronization result
    ASSERT_EQ(worker.synchronizationResult(), true); // Properly received the signal

    // Compute synchronization delay
    const core::Time synchronizationdelay = worker.synchronizationResumeTime() - synchronizationTriggerTime;
    std::cout << "synchronization delay (without timeout): " << synchronizationdelay.encodeToDurationString() << "s\n";
}

//! Check synchronization with timeout
TEST(BackgroundWorker, SynchronizationWithTimeout)
{
    // Initialize helpers
    bool triggerResult = false;

    // Check worker, if timeout is NOT hit
    // -----------------------------------
    // Create worker
    SyncWorker workerA(core::Time(10.0));

    // Trigger synchronization (without waiting for it)
    triggerResult = workerA.triggerSynchronization();
    ASSERT_EQ(triggerResult, false); // false since we did not wait for synchronization

    // Start worker
    ASSERT_EQ(workerA.start(), true);

    // Wait until worker waits for synchronization
    while (workerA.syncWaiting() == false)
        core::Time::sleep(0.001);

    // Trigger synchronization
    const core::Time synchronizationTriggerTime = core::Time::currentTime();
    triggerResult = workerA.triggerSynchronization();
    ASSERT_EQ(triggerResult, true); // true because we waited for synchronization

    // Wait until worker stopped waiting (i.e. received the signal)
    while (workerA.syncWaiting() == true)
        core::Time::sleep(0.001);

    // Stop worker
    ASSERT_EQ(workerA.join(core::Time(-1)), true);

    // Check synchronization result
    ASSERT_EQ(workerA.synchronizationResult(), true); // Properly received the signal

    // Compute synchronization delay
    const core::Time synchronizationdelay = workerA.synchronizationResumeTime() - synchronizationTriggerTime;
    std::cout << "synchronization delay (with timeout): " << synchronizationdelay.encodeToDurationString() << "s\n";

    // Check worker, if timeout IS hit
    // -------------------------------
    // Create worker
    SyncWorker workerB(core::Time(0.1));

    // Start worker
    ASSERT_EQ(workerB.start(), true);

    // Wait until worker waits for synchronization
    while (workerB.syncWaiting() == false)
        core::Time::sleep(0.001);

    // Wait until worker stopped waiting (i.e. timeout)
    while (workerB.syncWaiting() == true)
        core::Time::sleep(0.001);

    // Stop worker
    ASSERT_EQ(workerB.join(core::Time(-1)), true);

    // Check synchronization result
    ASSERT_EQ(workerB.synchronizationResult(), false); // Timeout is hit
}
