/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/CGMesh.hpp"
#include "broccoli/geometry/CGMeshTools.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Test string representation of results
TEST(CGMesh, ResultToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(CGMeshResult::CGMESHRESULT_COUNT); i++) {
        ASSERT_GT(CGMeshResultString(static_cast<CGMeshResult>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(CGMeshResultString(static_cast<CGMeshResult>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(CGMeshResultString(static_cast<CGMeshResult>(i)) != "UNKNOWN");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(CGMeshResultString(CGMeshResult::CGMESHRESULT_COUNT) == "UNKNOWN");
#endif
}

//! Check comparison operators
TEST(CGMesh, ComparisonOperators)
{
    // Initialize variables
    CGMesh meshA, meshB;

    // Test equality
    ASSERT_TRUE(meshA == meshB);
    ASSERT_FALSE(meshA != meshB);

    // Test inequality
    meshB = CGMesh();
    meshB.m_vertexBuffer.resize(Eigen::NoChange, 1);
    ASSERT_TRUE(meshA != meshB);
    ASSERT_FALSE(meshA == meshB);
    meshB = CGMesh();
    meshB.m_normalBuffer.resize(Eigen::NoChange, 1);
    ASSERT_TRUE(meshA != meshB);
    ASSERT_FALSE(meshA == meshB);
    meshB = CGMesh();
    meshB.m_colorBuffer.resize(Eigen::NoChange, 1);
    ASSERT_TRUE(meshA != meshB);
    ASSERT_FALSE(meshA == meshB);
    meshB = CGMesh();
    meshB.m_materialBuffer.resize(1);
    ASSERT_TRUE(meshA != meshB);
    ASSERT_FALSE(meshA == meshB);
    meshB = CGMesh();
    meshB.m_indexBuffer.resize(4, 1);
    ASSERT_TRUE(meshA != meshB);
    ASSERT_FALSE(meshA == meshB);
}

//! Check isValid()
TEST(CGMesh, isValid)
{
    // Initialize variables
    CGMesh mesh;
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 3);
    CGMeshResult result;

    // Check invalid/valid normal buffer
    mesh.m_normalBuffer.resize(Eigen::NoChange, 2);
    ASSERT_FALSE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::ERROR_INVALID_NORMALBUFFER);
    mesh.m_normalBuffer.resize(Eigen::NoChange, 3);
    ASSERT_TRUE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::SUCCESS);

    // Check invalid/valid color buffer
    mesh.m_colorBuffer.resize(Eigen::NoChange, 2);
    ASSERT_FALSE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::ERROR_INVALID_COLORBUFFER);
    mesh.m_colorBuffer.resize(Eigen::NoChange, 3);
    mesh.m_colorBuffer.setConstant(128);
    ASSERT_TRUE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::SUCCESS);

    // Check invalid/valid index buffer (dimension)
    mesh.m_indexBuffer.resize(1, 1);
    ASSERT_FALSE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::ERROR_INVALID_INDEXBUFFER);

    // Check invalid/valid index buffer (without material index)
    mesh.m_indexBuffer.resize(3, 1);
    mesh.m_indexBuffer(0, 0) = 0;
    mesh.m_indexBuffer(1, 0) = 1;
    mesh.m_indexBuffer(2, 0) = 3;
    ASSERT_FALSE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::ERROR_INVALID_INDEXBUFFER);
    mesh.m_indexBuffer(2, 0) = 2;
    ASSERT_TRUE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::SUCCESS);

    // Check invalid/valid index buffer (witht material index)
    mesh.m_indexBuffer.resize(4, 1);
    mesh.m_indexBuffer(0, 0) = 0;
    mesh.m_indexBuffer(1, 0) = 1;
    mesh.m_indexBuffer(2, 0) = 2;
    mesh.m_indexBuffer(3, 0) = 1;
    ASSERT_FALSE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::ERROR_INVALID_INDEXBUFFER);
    mesh.m_indexBuffer(3, 0) = 0;
    mesh.m_materialBuffer.resize(1);
    ASSERT_TRUE(mesh.isValid(&result));
    ASSERT_TRUE(result == CGMeshResult::SUCCESS);
}

//! Check vertexTriangleMap()
TEST(CGMesh, vertexTriangleMap)
{
    // Initialize variables
    CGMesh mesh;

    // Check empty mesh
    std::vector<std::vector<CGMesh::IndexType>> expectedEmptyMap;
    auto computedEmptyMap = mesh.vertexTriangleMap();
    ASSERT_TRUE(computedEmptyMap == expectedEmptyMap);

    // Setup mesh
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 9);
    mesh.m_indexBuffer.resize(3, 8);
    mesh.m_indexBuffer << 0, 0, 1, 2, 5, 8, 7, 6,
        4, 1, 2, 5, 8, 7, 6, 3,
        3, 4, 4, 4, 4, 4, 4, 4;

    // Setup expected map
    std::vector<std::vector<CGMesh::IndexType>> expectedMap(9);
    expectedMap[0] = std::vector<CGMesh::IndexType>{ 0, 1 };
    expectedMap[1] = std::vector<CGMesh::IndexType>{ 1, 2 };
    expectedMap[2] = std::vector<CGMesh::IndexType>{ 2, 3 };
    expectedMap[3] = std::vector<CGMesh::IndexType>{ 0, 7 };
    expectedMap[4] = std::vector<CGMesh::IndexType>{ 0, 1, 2, 3, 4, 5, 6, 7 };
    expectedMap[5] = std::vector<CGMesh::IndexType>{ 3, 4 };
    expectedMap[6] = std::vector<CGMesh::IndexType>{ 6, 7 };
    expectedMap[7] = std::vector<CGMesh::IndexType>{ 5, 6 };
    expectedMap[8] = std::vector<CGMesh::IndexType>{ 4, 5 };

    // Compute map
    auto computedMap = mesh.vertexTriangleMap();

    // Compare
    ASSERT_TRUE(computedMap == expectedMap);
}

//! Check materialTriangleMap()
TEST(CGMesh, materialTriangleMap)
{
    // Initialize variables
    CGMesh mesh;

    // Check empty mesh
    std::vector<std::vector<CGMesh::IndexType>> expectedEmptyMap;
    auto computedEmptyMap = mesh.materialTriangleMap();
    ASSERT_TRUE(computedEmptyMap == expectedEmptyMap);

    // Setup mesh
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 9);
    mesh.m_indexBuffer.resize(4, 8);
    mesh.m_indexBuffer << 0, 0, 1, 2, 5, 8, 7, 6,
        4, 1, 2, 5, 8, 7, 6, 3,
        3, 4, 4, 4, 4, 4, 4, 4,
        0, 0, 0, 0, 0, 0, 1, 2;
    mesh.m_materialBuffer.resize(9);

    // Setup expected map
    std::vector<std::vector<CGMesh::IndexType>> expectedMap(9);
    expectedMap[0] = std::vector<CGMesh::IndexType>{ 0, 1, 2, 3, 4, 5 };
    expectedMap[1] = std::vector<CGMesh::IndexType>{ 6 };
    expectedMap[2] = std::vector<CGMesh::IndexType>{ 7 };

    // Compute map
    auto computedMap = mesh.materialTriangleMap();

    // Compare
    ASSERT_TRUE(computedMap == expectedMap);
}

//! Check normalizeNormals()
TEST(CGMesh, normalizeNormals)
{
    // Initialize variables
    CGMesh mesh;
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 3);

    // Test empty normals
#ifdef NDEBUG
    ASSERT_FALSE(mesh.normalizeNormals(0, 1));
#endif // NDEBUG
    mesh.normalizeNormals();

    // Setup normal buffer for test
    Eigen::Matrix<double, 3, 3> inputNormals;
    inputNormals << 2, 0, 0,
        0, 3, 0,
        0, 0, -2;
    Eigen::Matrix<double, 3, 3> expectedOutputNormals;
    expectedOutputNormals << 1, 0, 0,
        0, 1, 0,
        0, 0, -1;

    // Test invalid index
#ifdef NDEBUG
    mesh.m_normalBuffer = inputNormals;
    ASSERT_FALSE(mesh.normalizeNormals(0, 10));
#endif // NDEBUG

    // Test normalization of selection
    mesh.m_normalBuffer = inputNormals;
    ASSERT_TRUE(mesh.normalizeNormals(1, 1));
    ASSERT_LT((mesh.m_normalBuffer.block(0, 1, 3, 1) - expectedOutputNormals.block(0, 1, 3, 1)).eval().norm(), 1e-9);

    // Test normalization of all normals
    mesh.m_normalBuffer = inputNormals;
    mesh.normalizeNormals();
    ASSERT_LT((mesh.m_normalBuffer - expectedOutputNormals).norm(), 1e-9);
}

//! Check recomputeNormals()
TEST(CGMesh, recomputeNormals)
{
    // Initialize variables
    CGMesh mesh;
    CGMeshResult result;

    // Test computing normals for empty mesh (do nothing)
    mesh.recomputeNormals();

    // Setup mesh
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 4);
    mesh.m_vertexBuffer << 0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1;
    mesh.m_normalBuffer.resize(Eigen::NoChange, 4);
    mesh.m_normalBuffer.setZero();
    mesh.m_normalBuffer.block(0, 0, 1, mesh.m_normalBuffer.cols()).setOnes();
    mesh.m_indexBuffer.resize(3, 2);
    mesh.m_indexBuffer << 0, 0,
        1, 2,
        2, 3;
    Eigen::Matrix<double, 3, 4> expectedNormals;
    const double x = 1.0 / sqrt(2.0);
    expectedNormals << x, 0, x, 1,
        0, 0, 0, 0,
        x, 1, x, 0;

    // Test invalid indices
#ifdef NDEBUG
    ASSERT_FALSE(mesh.recomputeNormals(0, 10, &result));
    ASSERT_TRUE(result == CGMeshResult::ERROR_INVALID_INDEX);
#endif // NDEBUG

    // Test selected computation
    ASSERT_TRUE(mesh.recomputeNormals(0, 0, &result));
    ASSERT_TRUE(result == CGMeshResult::SUCCESS);
    ASSERT_LT((mesh.m_normalBuffer.block(0, 0, 3, 1) - expectedNormals.block(0, 0, 3, 1)).norm(), 1e-9);

    // Test full computation
    mesh.recomputeNormals();
    ASSERT_LT((mesh.m_normalBuffer - expectedNormals).norm(), 1e-9);
}

//! Check translate(), rotate(), scale(), transform(), ChangeCoordinateSystem()
TEST(CGMesh, Translate_Rotate_Scale_Transform_ChangeCoordinateSystem)
{
    // Initialize helpers
    CGMesh mesh;
    CGMesh originalMesh, originalMeshNonUniformScaling, expectedMeshTranslation, expectedMeshRotation, expectedMeshScaleUniform, expectedMeshScaleNonUniform, expectedMeshTransform;
    const Eigen::Vector3d translation(1, -1, 0.5);
    const Eigen::Quaterniond rotation(Eigen::AngleAxisd(-M_PI / 2.0, Eigen::Vector3d(0, 1, 0)));
    Eigen::Isometry3d transform;
    transform.linear() = rotation.toRotationMatrix();
    transform.translation() = translation;

    // Operations on empty mesh
    mesh.translate(translation);
    mesh.rotate(rotation);
    mesh.transform(transform);

    // Initialize original mesh
    originalMesh.m_vertexBuffer.resize(Eigen::NoChange, 3);
    originalMesh.m_vertexBuffer << 0, 1, 0,
        0, 0, 1,
        0, 0, 0;
    CGMeshTools::generateDefaultIndexBuffer(originalMesh);
    originalMesh.recomputeNormals();

    // Translate
    // ---------
    // Initialize expected result after translate()
    expectedMeshTranslation.m_vertexBuffer.resize(Eigen::NoChange, 3);
    expectedMeshTranslation.m_vertexBuffer << 1, 2, 1,
        -1, -1, 0,
        0.5, 0.5, 0.5;
    CGMeshTools::generateDefaultIndexBuffer(expectedMeshTranslation);
    expectedMeshTranslation.recomputeNormals();

    // Check translate()
    mesh = originalMesh;
    mesh.translate(translation);
    ASSERT_LT((mesh.m_vertexBuffer - expectedMeshTranslation.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - expectedMeshTranslation.m_normalBuffer).norm(), 1e-9);
    mesh.translate(-translation);
    ASSERT_LT((mesh.m_vertexBuffer - originalMesh.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - originalMesh.m_normalBuffer).norm(), 1e-9);

    // Rotate
    // ------
    // Initialize expected result after rotate()
    expectedMeshRotation.m_vertexBuffer.resize(Eigen::NoChange, 3);
    expectedMeshRotation.m_vertexBuffer << 0, 0, 0,
        0, 0, 1,
        0, 1, 0;
    CGMeshTools::generateDefaultIndexBuffer(expectedMeshRotation);
    expectedMeshRotation.recomputeNormals();

    // Check rotate()
    mesh = originalMesh;
    mesh.rotate(rotation);
    ASSERT_LT((mesh.m_vertexBuffer - expectedMeshRotation.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - expectedMeshRotation.m_normalBuffer).norm(), 1e-9);
    mesh.rotate(rotation.conjugate());
    ASSERT_LT((mesh.m_vertexBuffer - originalMesh.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - originalMesh.m_normalBuffer).norm(), 1e-9);

    // Scale (uniform)
    // -----
    // Initialize expected result after scale()
    expectedMeshScaleUniform.m_vertexBuffer.resize(Eigen::NoChange, 3);
    expectedMeshScaleUniform.m_vertexBuffer << 0, 2, 0,
        0, 0, 2,
        0, 0, 0;
    CGMeshTools::generateDefaultIndexBuffer(expectedMeshScaleUniform);
    expectedMeshScaleUniform.recomputeNormals();

    // Check scale()
    mesh = originalMesh;
    mesh.scale(2.0);
    ASSERT_LT((mesh.m_vertexBuffer - expectedMeshScaleUniform.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - expectedMeshScaleUniform.m_normalBuffer).norm(), 1e-9);
    mesh.scale(Eigen::Vector3d(0.5, 0.5, 0.5)); // Auto-detect uniform scaling
    ASSERT_LT((mesh.m_vertexBuffer - originalMesh.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - originalMesh.m_normalBuffer).norm(), 1e-9);

    // Scale (non-uniform)
    // -----
    // Initialize original mesh
    originalMeshNonUniformScaling.m_vertexBuffer.resize(Eigen::NoChange, 3);
    originalMeshNonUniformScaling.m_vertexBuffer << 0, 1, 0,
        0, 0, 1,
        0, 1, 0;
    CGMeshTools::generateDefaultIndexBuffer(originalMeshNonUniformScaling);
    originalMeshNonUniformScaling.recomputeNormals();

    // Initialize expected result after scale()
    expectedMeshScaleNonUniform.m_vertexBuffer.resize(Eigen::NoChange, 3);
    expectedMeshScaleNonUniform.m_vertexBuffer << 0, 2, 0,
        0, 0, 1,
        0, 1, 0;
    CGMeshTools::generateDefaultIndexBuffer(expectedMeshScaleNonUniform);
    expectedMeshScaleNonUniform.recomputeNormals();

    // Check scale()
    mesh = originalMeshNonUniformScaling;
    mesh.scale(Eigen::Vector3d(2.0, 1.0, 1.0));
    ASSERT_LT((mesh.m_vertexBuffer - expectedMeshScaleNonUniform.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - expectedMeshScaleNonUniform.m_normalBuffer).norm(), 1e-9);
    mesh.scale(Eigen::Vector3d(0.5, 1.0, 1.0));
    ASSERT_LT((mesh.m_vertexBuffer - originalMeshNonUniformScaling.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - originalMeshNonUniformScaling.m_normalBuffer).norm(), 1e-9);

    // Transform
    // ---------
    // Initialize expected result after transform()
    expectedMeshTransform.m_vertexBuffer.resize(Eigen::NoChange, 3);
    expectedMeshTransform.m_vertexBuffer << 1, 1, 1,
        -1, -1, 0,
        0.5, 1.5, 0.5;
    CGMeshTools::generateDefaultIndexBuffer(expectedMeshTransform);
    expectedMeshTransform.recomputeNormals();

    // Check transform()
    mesh = originalMesh;
    mesh.transform(transform);
    ASSERT_LT((mesh.m_vertexBuffer - expectedMeshTransform.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - expectedMeshTransform.m_normalBuffer).norm(), 1e-9);
    mesh.transform(transform.inverse());
    ASSERT_LT((mesh.m_vertexBuffer - originalMesh.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - originalMesh.m_normalBuffer).norm(), 1e-9);

    // Change Coordinate System
    // ------------------------
    // Check changeCoordinateSystem()
    mesh = originalMesh;
    mesh.changeCoordinateSystem(translation, Eigen::Vector3d(0, 0, 1), Eigen::Vector3d(-1, 0, 0));
    ASSERT_LT((mesh.m_vertexBuffer - expectedMeshTransform.m_vertexBuffer).norm(), 1e-9);
    ASSERT_LT((mesh.m_normalBuffer - expectedMeshTransform.m_normalBuffer).norm(), 1e-9);
}

//! Check serialize() and deserialize()
TEST(CGMesh, serialization)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        io::serialization::Endianness endianness = io::serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = io::serialization::Endianness::BIG;

        // Setup test mesh
        CGMesh inputMesh;
        inputMesh.m_vertexBuffer.resize(Eigen::NoChange, 3);
        inputMesh.m_vertexBuffer << 0, 1, 0,
            0, 0, 1,
            0, 0, 0;
        inputMesh.m_indexBuffer.resize(4, 1);
        inputMesh.m_indexBuffer << 0, 1, 2, 0;
        inputMesh.m_materialBuffer.resize(1);
        inputMesh.recomputeNormals();

        // Test serialization
        io::serialization::BinaryStream stream;
        io::serialization::BinaryStreamSize returnValueSerialization = inputMesh.serialize(stream, endianness);

        // Test deserialization
        CGMesh ouputMesh;
        io::serialization::BinaryStreamSize returnValueDeSerialization = ouputMesh.deSerialize(stream, 0, endianness);

        // Test result
        ASSERT_EQ(returnValueSerialization, returnValueDeSerialization);
        ASSERT_TRUE(ouputMesh == inputMesh);
    }
}

#endif // HAVE_EIGEN3
