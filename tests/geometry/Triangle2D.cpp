/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/Triangle2D.hpp"
#include "TriangleHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Helper to create a certain permutation for a triangle definition
static inline Triangle2D triangleFromPermutation(const std::array<Eigen::Vector2d, 3>& points, const size_t& permutation)
{
    if (permutation == 0)
        return Triangle2D(points[0], points[1], points[2]);
    else if (permutation == 1)
        return Triangle2D(points[1], points[2], points[0]);
    else if (permutation == 2)
        return Triangle2D(points[2], points[0], points[1]);
    else if (permutation == 3)
        return Triangle2D(points[2], points[1], points[0]);
    else if (permutation == 4)
        return Triangle2D(points[1], points[0], points[2]);
    else
        return Triangle2D(points[0], points[2], points[1]);
}

//! Helper to check intersection tests
static inline bool checkIntersectionTest(const Triangle2D& triangle, const Eigen::Vector2d& point, const bool& intersection)
{
    const auto intersectionResult = triangle.evaluatePointIntersection(point);
    if (intersectionResult.m_intersection != intersection)
        return false;
    if (intersection == true) {
        if (triangle.interpolatePoint(intersectionResult.m_intersectionPointCoordinates).isApprox(point) == false)
            return false;
    }
    return true;
}

//! Helper to check distance tests
static inline bool checkDistanceTest(const Triangle2D& triangle, const Eigen::Vector2d& point, const Eigen::Vector2d& closestPoint)
{
    const auto distanceResult = triangle.evaluatePointDistance(point);
    if (triangle.interpolatePoint(distanceResult.m_pointCoordinates).isApprox(point) == false)
        return false;
    if (triangle.interpolatePoint(distanceResult.m_closestPointCoordinates).isApprox(closestPoint) == false)
        return false;
    return true;
}

//! Tests basic construction and transformation
TEST(Triangle2D, BasicConstructionAndTransformation)
{
    // Setup triangle
    const Eigen::Vector2d v0(5, 5);
    const Eigen::Vector2d v1(10, 5);
    const Eigen::Vector2d v2(5, 15);
    const Triangle2D triangle(v0, v1, v2);
    ASSERT_TRUE(v0.isApprox(triangle.v0()));
    ASSERT_TRUE(v1.isApprox(triangle.v1()));
    ASSERT_TRUE(v2.isApprox(triangle.v2()));

    // Check auxillary parameters
    const Eigen::Vector2d e0 = v1 - v0;
    const Eigen::Vector2d e1 = v2 - v0;
    const Eigen::Vector2d e2 = v2 - v1;
    ASSERT_TRUE(triangle.e0().isApprox(e0));
    ASSERT_TRUE(triangle.e1().isApprox(e1));
    ASSERT_TRUE(triangle.e2().isApprox(e2));
    ASSERT_FLOAT_EQ(triangle.e0_dot_e0(), e0.dot(e0));
    ASSERT_FLOAT_EQ(triangle.e1_dot_e1(), e1.dot(e1));
    ASSERT_FLOAT_EQ(triangle.e2_dot_e2(), e2.dot(e2));
    ASSERT_FLOAT_EQ(triangle.e0_dot_e1(), e0.dot(e1));
    ASSERT_FLOAT_EQ(triangle.e0_dot_e2(), e0.dot(e2));
    ASSERT_FLOAT_EQ(triangle.e1_dot_e2(), e1.dot(e2));
    ASSERT_FLOAT_EQ(triangle.area(), 25.0);
    ASSERT_FALSE(triangle.degenerated());
    ASSERT_TRUE(triangle.n().isApprox(Eigen::Vector3d::UnitZ()));
    ASSERT_TRUE(triangle.boundingBoxMinimum().isApprox(v0));
    ASSERT_TRUE(triangle.boundingBoxMaximum().isApprox(v0 + e0 + e1));
    Eigen::Matrix2d D;
    D << e0.x(), e1.x(), //
        e0.y(), e1.y();
    const Eigen::Matrix2d invD = D.inverse();
    ASSERT_TRUE(triangle.invD().isApprox(invD));
    ASSERT_TRUE(triangle.counterClockwise());
    ASSERT_FALSE(triangle.clockwise());

    // Setup degenerated triangle
    const Triangle2D degenerated(Eigen::Vector2d(0, 0), Eigen::Vector2d(0, 0), Eigen::Vector2d(1, 0));
    ASSERT_TRUE(degenerated.degenerated());

    // Setup translation and rotation
    const Eigen::Vector2d translation(0.123, -2.345);
    const Eigen::Matrix2d rotation(Eigen::Rotation2D<double>(0.5674));
    const Eigen::Vector2d tv0 = translation + rotation * v0;
    const Eigen::Vector2d tv1 = translation + rotation * v1;
    const Eigen::Vector2d tv2 = translation + rotation * v2;
    Triangle2D transformedTriangle = triangle;
    ASSERT_TRUE(transformedTriangle == triangle);
    ASSERT_FALSE(transformedTriangle != triangle);
    ASSERT_TRUE(fullComparison(transformedTriangle, triangle));
    transformedTriangle.rotate(rotation);
    ASSERT_FALSE(transformedTriangle == triangle);
    ASSERT_TRUE(transformedTriangle != triangle);
    ASSERT_FALSE(fullComparison(transformedTriangle, triangle));
    transformedTriangle.translate(translation);
    const Triangle2D expectedTransformedTriangle(tv0, tv1, tv2);
    ASSERT_TRUE(expectedTransformedTriangle == transformedTriangle);
    ASSERT_FALSE(expectedTransformedTriangle != transformedTriangle);
    ASSERT_TRUE(fullComparison(expectedTransformedTriangle, transformedTriangle));

    // Check invalid projection
#ifdef NDEBUG
    const Triangle2D::BarycentricCoordinates invalidCoordinates(-0.4, -0.5, -0.1);
    ASSERT_FALSE(invalidCoordinates.isValid());
    const auto invalidProjectedCoordinates = triangle.projectToTriangle(Eigen::Vector2d::Zero(), invalidCoordinates);
    ASSERT_TRUE(invalidProjectedCoordinates == invalidCoordinates);
    ASSERT_FALSE(invalidProjectedCoordinates != invalidCoordinates);
#endif
}

//! Tests intersection and distance evaluations
TEST(Triangle2D, IntersectionAndDistanceTest)
{
    // Check for sharp angles
    // ----------------------
    const std::array<Eigen::Vector2d, 3> sharpPoints{ { Eigen::Vector2d(5, 5), Eigen::Vector2d(10, 5), Eigen::Vector2d(5, 15) } };
    for (size_t p = 0; p < 6; p++) {
        const Triangle2D triangle = triangleFromPermutation(sharpPoints, p);

        // Test inside points
        const Eigen::Vector2d insideA(7, 6);
        ASSERT_TRUE(checkIntersectionTest(triangle, insideA, true));
        ASSERT_TRUE(checkDistanceTest(triangle, insideA, insideA));
        const Eigen::Vector2d insideB(8, 8);
        ASSERT_TRUE(checkIntersectionTest(triangle, insideB, true));
        ASSERT_TRUE(checkDistanceTest(triangle, insideB, insideB));
        const Eigen::Vector2d insideC(6, 9);
        ASSERT_TRUE(checkIntersectionTest(triangle, insideC, true));
        ASSERT_TRUE(checkDistanceTest(triangle, insideC, insideC));

        // Test outside points
        const Eigen::Vector2d outsideA(1, 3);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideA, false));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideA, Eigen::Vector2d(5, 5)));
        const Eigen::Vector2d outsideB(8, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideB, false));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideB, Eigen::Vector2d(8, 5)));
        const Eigen::Vector2d outsideC(14, 3);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideC, false));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideC, Eigen::Vector2d(10, 5)));
        const Eigen::Vector2d outsideD(8, 14);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideD, false));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideD, Eigen::Vector2d(6, 13)));
        const Eigen::Vector2d outsideE(3, 22);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideE, false));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideE, Eigen::Vector2d(5, 15)));
        const Eigen::Vector2d outsideF(1, 14);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideF, false));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideF, Eigen::Vector2d(5, 14)));
    }

    // Check for obtuse angles
    // -----------------------
    const std::array<Eigen::Vector2d, 3> obtusePoints{ { Eigen::Vector2d(5, 5), Eigen::Vector2d(10, 0), Eigen::Vector2d(5, 15) } };
    for (size_t p = 0; p < 6; p++) {
        const Triangle2D triangle = triangleFromPermutation(obtusePoints, p);

        // Test corner points
        const Eigen::Vector2d cornerA(1, 7);
        ASSERT_TRUE(checkIntersectionTest(triangle, cornerA, false));
        ASSERT_TRUE(checkDistanceTest(triangle, cornerA, Eigen::Vector2d(5, 7)));
        const Eigen::Vector2d cornerB(1, 4);
        ASSERT_TRUE(checkIntersectionTest(triangle, cornerB, false));
        ASSERT_TRUE(checkDistanceTest(triangle, cornerB, Eigen::Vector2d(5, 5)));
        const Eigen::Vector2d cornerC(3, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, cornerC, false));
        ASSERT_TRUE(checkDistanceTest(triangle, cornerC, Eigen::Vector2d(6, 4)));
    }
}

#endif // HAVE_EIGEN3
