/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/CGMeshTools.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Check generateDefaultIndexBuffer()
TEST(CGMeshTools, generateDefaultIndexBuffer)
{
    // Initialize helpers
    CGMesh mesh;
    Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic> expectedIndexBuffer;
    expectedIndexBuffer.resize(4, 2);
    expectedIndexBuffer << 0, 3,
        1, 4,
        2, 5,
        0, 0;

    // Trigger method on empty mesh
    ASSERT_TRUE(CGMeshTools::generateDefaultIndexBuffer(mesh));
    ASSERT_TRUE(CGMeshTools::generateDefaultIndexBuffer(mesh, 0));

    // Trigger method on mesh with invalid vertex buffer
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 2);
    ASSERT_FALSE(CGMeshTools::generateDefaultIndexBuffer(mesh));
    ASSERT_FALSE(CGMeshTools::generateDefaultIndexBuffer(mesh, 0));

    // Trigger method on mesh with valid vertex buffer
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 6);
    ASSERT_TRUE(CGMeshTools::generateDefaultIndexBuffer(mesh));
    ASSERT_TRUE(mesh.m_indexBuffer.isApprox(expectedIndexBuffer.block<3, 2>(0, 0)));
    ASSERT_TRUE(CGMeshTools::generateDefaultIndexBuffer(mesh, 0));
    ASSERT_TRUE(mesh.m_indexBuffer.isApprox(expectedIndexBuffer));
}

//! Check computeVertexColorFromMaterial()
TEST(CGMeshTools, computeVertexColorFromMaterial)
{
    // Initialize helpers
    CGMesh mesh;

    // Test method with empty mesh
    // ---------------------------
    CGMeshTools::computeVertexColorFromMaterial(mesh);
    ASSERT_EQ(mesh.m_colorBuffer.cols(), 0);

    // Setup vertex buffer
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 4);
    mesh.m_vertexBuffer << 0, 1, 1, 0,
        0, 0, 1, 1,
        0, 0, 0, 0;

    // Test method without material indices
    // ------------------------------------
    mesh.m_indexBuffer.resize(3, 2);
    mesh.m_indexBuffer << 0, 0,
        1, 2,
        2, 3;
    Eigen::Matrix<CGMesh::ColorChannelType, 4, 4> expectedDefaultColorBuffer;
    expectedDefaultColorBuffer << 1, 1, 1, 1,
        2, 2, 2, 2,
        3, 3, 3, 3,
        4, 4, 4, 4;
    CGMeshTools::computeVertexColorFromMaterial(mesh, (Eigen::Matrix<CGMesh::ColorChannelType, 4, 1>() << 1, 2, 3, 4).finished());
    ASSERT_TRUE(mesh.m_colorBuffer.isApprox(expectedDefaultColorBuffer));

    // Test method with material indices
    // ---------------------------------
    mesh.m_indexBuffer.resize(4, 2);
    mesh.m_indexBuffer << 0, 0,
        1, 2,
        2, 3,
        0, 1;
    mesh.m_materialBuffer.resize(2);
    mesh.m_materialBuffer[0].m_diffuseColor.fill(1);
    mesh.m_materialBuffer[0].m_alpha = 2;
    mesh.m_materialBuffer[1].m_diffuseColor.fill(3);
    mesh.m_materialBuffer[1].m_alpha = 4;
    Eigen::Matrix<CGMesh::ColorChannelType, 4, 4> expectedColorBuffer;
    expectedColorBuffer << 3, 1, 3, 3,
        3, 1, 3, 3,
        3, 1, 3, 3,
        4, 2, 4, 4;
    CGMeshTools::computeVertexColorFromMaterial(mesh, (Eigen::Matrix<CGMesh::ColorChannelType, 4, 1>() << 1, 2, 3, 4).finished());
    ASSERT_TRUE(mesh.m_colorBuffer.isApprox(expectedColorBuffer));
}

//! Check convertMaterialsToVertexColor()
TEST(CGMeshTools, convertMaterialsToVertexColor)
{
    // Initialize mesh
    CGMesh mesh;
    mesh.m_vertexBuffer.resize(Eigen::NoChange, 4);
    mesh.m_vertexBuffer << 0, 1, 1, 0,
        0, 0, 1, 1,
        0, 0, 0, 0;
    mesh.m_indexBuffer.resize(4, 2);
    mesh.m_indexBuffer << 0, 0,
        1, 2,
        2, 3,
        0, 1;
    mesh.m_materialBuffer.resize(2);
    mesh.m_materialBuffer[0].m_diffuseColor.fill(1);
    mesh.m_materialBuffer[0].m_alpha = 2;
    mesh.m_materialBuffer[1].m_diffuseColor.fill(3);
    mesh.m_materialBuffer[1].m_alpha = 4;
    Eigen::Matrix<CGMesh::ColorChannelType, 4, 4> expectedColorBuffer;
    expectedColorBuffer << 3, 1, 3, 3,
        3, 1, 3, 3,
        3, 1, 3, 3,
        4, 2, 4, 4;
    CGMeshTools::convertMaterialsToVertexColor(mesh, "abc", (Eigen::Matrix<CGMesh::ColorChannelType, 4, 1>() << 1, 2, 3, 4).finished());
    ASSERT_TRUE(mesh.m_colorBuffer.isApprox(expectedColorBuffer));
    ASSERT_EQ(mesh.m_materialBuffer.size(), 1);
    ASSERT_EQ(mesh.m_materialBuffer[0].m_name, "abc");
    ASSERT_EQ(mesh.m_materialBuffer[0].m_diffuseColorFromVertexColor, true);
    ASSERT_EQ(mesh.m_materialBuffer[0].m_alphaFromVertexColor, true);
    Eigen::Matrix<CGMesh::IndexType, 4, 2> expectedIndexBuffer = mesh.m_indexBuffer;
    expectedIndexBuffer.row(3).setConstant(0);
    ASSERT_TRUE(mesh.m_indexBuffer.isApprox(expectedIndexBuffer));
}

//! Check cleanup()
TEST(CGMeshTools, cleanup)
{
    // Initialize input mesh
    CGMesh inputMesh;
    inputMesh.m_vertexBuffer.resize(Eigen::NoChange, 7);
    inputMesh.m_vertexBuffer << 0.1, 1.1, 2.1, 3.1, 4.1, 5.1, 6.1,
        0.2, 1.2, 2.2, 3.2, 4.2, 5.2, 6.2,
        0.3, 1.3, 2.3, 3.3, 4.3, 5.3, 6.3;
    inputMesh.m_indexBuffer.resize(4, 2);
    inputMesh.m_indexBuffer << 1, 1,
        2, 3,
        3, 5,
        1, 1;
    inputMesh.m_colorBuffer.resize(Eigen::NoChange, inputMesh.m_vertexBuffer.cols());
    inputMesh.m_colorBuffer << 10, 11, 12, 13, 14, 15, 16,
        20, 21, 22, 23, 24, 25, 26,
        30, 31, 32, 33, 34, 35, 36,
        40, 41, 42, 43, 44, 45, 46;
    inputMesh.m_materialBuffer.resize(3);
    inputMesh.m_materialBuffer[0].m_alpha = 1;
    inputMesh.m_materialBuffer[1].m_alpha = 2;
    inputMesh.m_materialBuffer[2].m_alpha = 3;
    inputMesh.recomputeNormals();

    // Initialize output mesh
    CGMesh outputMesh;
    outputMesh.m_vertexBuffer.resize(Eigen::NoChange, 4);
    outputMesh.m_vertexBuffer << 1.1, 2.1, 3.1, 5.1,
        1.2, 2.2, 3.2, 5.2,
        1.3, 2.3, 3.3, 5.3;
    outputMesh.m_indexBuffer.resize(4, 2);
    outputMesh.m_indexBuffer << 0, 0,
        1, 2,
        2, 3,
        0, 0;
    outputMesh.m_colorBuffer.resize(Eigen::NoChange, outputMesh.m_vertexBuffer.cols());
    outputMesh.m_colorBuffer << 11, 12, 13, 15,
        21, 22, 23, 25,
        31, 32, 33, 35,
        41, 42, 43, 45;
    outputMesh.m_materialBuffer.resize(1);
    outputMesh.m_materialBuffer[0].m_alpha = 2;
    outputMesh.recomputeNormals();

    // Trigger clean-up
    auto returnValue = CGMeshTools::cleanup(inputMesh);
    ASSERT_EQ(returnValue[0], 3);
    ASSERT_EQ(returnValue[1], 2);

    // Check result
    ASSERT_TRUE(inputMesh == outputMesh);
}

//! Helper for test of merge()
/*!
 * A...first mesh
 * B...second mesh
 * C...expected resulting mesh
 */
void checkCGMeshMerge(const Eigen::Matrix<double, 3, Eigen::Dynamic>& vertexBufferA,
    const Eigen::Matrix<double, 3, Eigen::Dynamic>& vertexBufferB,
    const Eigen::Matrix<double, 3, Eigen::Dynamic>& vertexBufferC,
    const Eigen::Matrix<double, 3, Eigen::Dynamic>& normalBufferA,
    const Eigen::Matrix<double, 3, Eigen::Dynamic>& normalBufferB,
    const Eigen::Matrix<double, 3, Eigen::Dynamic>& normalBufferC,
    const Eigen::Matrix<CGMesh::ColorChannelType, 4, Eigen::Dynamic>& colorBufferA,
    const Eigen::Matrix<CGMesh::ColorChannelType, 4, Eigen::Dynamic>& colorBufferB,
    const Eigen::Matrix<CGMesh::ColorChannelType, 4, Eigen::Dynamic>& colorBufferC,
    const std::vector<CGMaterial>& materialBufferA,
    const std::vector<CGMaterial>& materialBufferB,
    const std::vector<CGMaterial>& materialBufferC,
    const Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic>& indexBufferA,
    const Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic>& indexBufferB,
    const Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic>& indexBufferC)
{
    // Setup meshs
    CGMesh meshA;
    meshA.m_vertexBuffer = vertexBufferA;
    meshA.m_normalBuffer = normalBufferA;
    meshA.m_colorBuffer = colorBufferA;
    meshA.m_materialBuffer = materialBufferA;
    meshA.m_indexBuffer = indexBufferA;
    CGMesh meshB;
    meshB.m_vertexBuffer = vertexBufferB;
    meshB.m_normalBuffer = normalBufferB;
    meshB.m_colorBuffer = colorBufferB;
    meshB.m_materialBuffer = materialBufferB;
    meshB.m_indexBuffer = indexBufferB;
    CGMesh meshC;
    meshC.m_vertexBuffer = vertexBufferC;
    meshC.m_normalBuffer = normalBufferC;
    meshC.m_colorBuffer = colorBufferC;
    meshC.m_materialBuffer = materialBufferC;
    meshC.m_indexBuffer = indexBufferC;

    // Perform merge
    const auto mergeResult = CGMeshTools::merge(meshA, meshB);
    ASSERT_TRUE(mergeResult.isValid());
    ASSERT_TRUE(mergeResult == meshC);
}

//! Check merge()
TEST(CGMeshTools, merge)
{
    // Initialize vertex buffers
    // -------------------------
    Eigen::Matrix<double, 3, Eigen::Dynamic> vertexBufferA;
    vertexBufferA.resize(Eigen::NoChange, 4);
    vertexBufferA << 0, 1, 1, 0,
        0, 0, 1, 1,
        0, 0, 0, 0;
    Eigen::Matrix<double, 3, Eigen::Dynamic> vertexBufferB;
    vertexBufferB.resize(Eigen::NoChange, 3);
    vertexBufferB << 0, -1, 0,
        0, 0, -1,
        0, 0, 0;
    Eigen::Matrix<double, 3, Eigen::Dynamic> vertexBufferC;
    vertexBufferC.resize(Eigen::NoChange, vertexBufferA.cols() + vertexBufferB.cols());
    vertexBufferC.block(0, 0, 3, vertexBufferA.cols()) = vertexBufferA;
    vertexBufferC.block(0, vertexBufferA.cols(), 3, vertexBufferB.cols()) = vertexBufferB;
    Eigen::Matrix<double, 3, Eigen::Dynamic> emptyVertexBuffer;

    // Initialize normal buffers
    // -------------------------
    Eigen::Matrix<double, 3, Eigen::Dynamic> normalBufferA;
    normalBufferA.resize(Eigen::NoChange, 4);
    normalBufferA << 0, 0, 0, 0,
        0, 0, 0, 0,
        1, 1, 1, 1;
    Eigen::Matrix<double, 3, Eigen::Dynamic> normalBufferB;
    normalBufferB.resize(Eigen::NoChange, 3);
    normalBufferB << 0, 0, 0,
        0, 0, 0,
        -1, -1, -1;
    Eigen::Matrix<double, 3, Eigen::Dynamic> normalBufferC;
    normalBufferC.resize(Eigen::NoChange, normalBufferA.cols() + normalBufferB.cols());
    normalBufferC.block(0, 0, 3, normalBufferA.cols()) = normalBufferA;
    normalBufferC.block(0, normalBufferA.cols(), 3, normalBufferB.cols()) = normalBufferB;
    Eigen::Matrix<double, 3, Eigen::Dynamic> emptyNormalBuffer;

    // Initialize color buffers
    // ------------------------
    Eigen::Matrix<CGMesh::ColorChannelType, 4, Eigen::Dynamic> colorBufferA;
    colorBufferA.resize(Eigen::NoChange, 4);
    colorBufferA << 1, 2, 3, 4,
        6, 5, 4, 3,
        1, 2, 3, 4,
        6, 5, 4, 3;
    Eigen::Matrix<CGMesh::ColorChannelType, 4, Eigen::Dynamic> colorBufferB;
    colorBufferB.resize(Eigen::NoChange, 3);
    colorBufferB << 7, 8, 9,
        9, 8, 7,
        7, 8, 9,
        9, 8, 7;
    Eigen::Matrix<CGMesh::ColorChannelType, 4, Eigen::Dynamic> colorBufferC;
    colorBufferC.resize(Eigen::NoChange, colorBufferA.cols() + colorBufferB.cols());
    colorBufferC.block(0, 0, 4, colorBufferA.cols()) = colorBufferA;
    colorBufferC.block(0, colorBufferA.cols(), 4, colorBufferB.cols()) = colorBufferB;
    Eigen::Matrix<CGMesh::ColorChannelType, 4, Eigen::Dynamic> emptyColorBuffer;

    // Initialize material buffers
    // ---------------------------
    std::vector<CGMaterial> materialBufferA;
    materialBufferA.resize(1);
    materialBufferA[0].m_name = "materialA";
    materialBufferA[0].m_alpha = 1;
    std::vector<CGMaterial> materialBufferB;
    materialBufferB.resize(1);
    materialBufferB[0].m_name = "materialB";
    materialBufferB[0].m_alpha = 2;
    std::vector<CGMaterial> materialBufferC;
    materialBufferC.insert(materialBufferC.end(), materialBufferA.begin(), materialBufferA.end());
    materialBufferC.insert(materialBufferC.end(), materialBufferB.begin(), materialBufferB.end());
    std::vector<CGMaterial> emptyMaterialBuffer;

    // Initialize index buffers
    // ------------------------
    Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic> indexBufferAWOM; // without material
    indexBufferAWOM.resize(3, 2);
    indexBufferAWOM << 0, 1,
        1, 2,
        3, 3;
    Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic> indexBufferAWM; // with material
    indexBufferAWM.resize(4, 2);
    indexBufferAWM.block(0, 0, 3, indexBufferAWM.cols()) = indexBufferAWOM;
    indexBufferAWM(3, 0) = 0;
    indexBufferAWM(3, 1) = 0;
    Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic> indexBufferBWOM; // without material
    indexBufferBWOM.resize(3, 1);
    indexBufferBWOM << 0, 2, 1;
    Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic> indexBufferBWM; // with material
    indexBufferBWM.resize(4, 1);
    indexBufferBWM.block(0, 0, 3, indexBufferBWM.cols()) = indexBufferBWOM;
    indexBufferBWM(3, 0) = 0;
    Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic> indexBufferCWOM; // without material
    indexBufferCWOM.resize(3, 3);
    indexBufferCWOM << 0, 1, 4,
        1, 2, 6,
        3, 3, 5;
    Eigen::Matrix<CGMesh::IndexType, Eigen::Dynamic, Eigen::Dynamic> indexBufferCWM; // with material
    indexBufferCWM.resize(4, 3);
    indexBufferCWM << 0, 1, 4,
        1, 2, 6,
        3, 3, 5,
        0, 0, 1;
    Eigen::Matrix<CGMesh::IndexType, 3, Eigen::Dynamic> emptyindexBufferWOM; // without material
    Eigen::Matrix<CGMesh::IndexType, 4, Eigen::Dynamic> emptyindexBufferWM; // with material

    // Test merge of fully specified meshes
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, normalBufferA, normalBufferB, normalBufferC, colorBufferA, colorBufferB, colorBufferC, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, indexBufferAWOM, indexBufferBWOM, indexBufferCWOM);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, normalBufferA, normalBufferB, normalBufferC, colorBufferA, colorBufferB, colorBufferC, materialBufferA, materialBufferB, materialBufferC, indexBufferAWM, indexBufferBWM, indexBufferCWM);

    // Test merge of empty meshes
    checkCGMeshMerge(emptyVertexBuffer, emptyVertexBuffer, emptyVertexBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyColorBuffer, emptyColorBuffer, emptyColorBuffer, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, emptyindexBufferWOM, emptyindexBufferWOM, emptyindexBufferWOM);
    checkCGMeshMerge(emptyVertexBuffer, emptyVertexBuffer, emptyVertexBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyColorBuffer, emptyColorBuffer, emptyColorBuffer, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, emptyindexBufferWM, emptyindexBufferWM, emptyindexBufferWM);
    checkCGMeshMerge(vertexBufferA, emptyVertexBuffer, vertexBufferA, normalBufferA, emptyNormalBuffer, normalBufferA, colorBufferA, emptyColorBuffer, colorBufferA, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, indexBufferAWOM, emptyindexBufferWOM, indexBufferAWOM);
    checkCGMeshMerge(vertexBufferA, emptyVertexBuffer, vertexBufferA, normalBufferA, emptyNormalBuffer, normalBufferA, colorBufferA, emptyColorBuffer, colorBufferA, materialBufferA, emptyMaterialBuffer, materialBufferA, indexBufferAWM, emptyindexBufferWM, indexBufferAWM);
    checkCGMeshMerge(emptyVertexBuffer, vertexBufferB, vertexBufferB, emptyNormalBuffer, normalBufferB, normalBufferB, emptyColorBuffer, colorBufferB, colorBufferB, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, emptyindexBufferWOM, indexBufferBWOM, indexBufferBWOM);
    checkCGMeshMerge(emptyVertexBuffer, vertexBufferB, vertexBufferB, emptyNormalBuffer, normalBufferB, normalBufferB, emptyColorBuffer, colorBufferB, colorBufferB, emptyMaterialBuffer, materialBufferB, materialBufferB, emptyindexBufferWM, indexBufferBWM, indexBufferBWM);

    // Vertex buffers only
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyColorBuffer, emptyColorBuffer, emptyColorBuffer, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, indexBufferAWOM, indexBufferBWOM, indexBufferCWOM);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyColorBuffer, emptyColorBuffer, emptyColorBuffer, materialBufferA, materialBufferB, materialBufferC, indexBufferAWM, indexBufferBWM, indexBufferCWM);

    // Recomputed normals
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, normalBufferA, emptyNormalBuffer, normalBufferC, colorBufferA, colorBufferB, colorBufferC, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, indexBufferAWOM, indexBufferBWOM, indexBufferCWOM);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, emptyNormalBuffer, normalBufferB, normalBufferC, colorBufferA, colorBufferB, colorBufferC, materialBufferA, materialBufferB, materialBufferC, indexBufferAWM, indexBufferBWM, indexBufferCWM);

    // Recomputed colors
    auto colorBufferCRecomputedA = colorBufferC;
    colorBufferCRecomputedA.block(0, 0, 4, colorBufferA.cols()).setConstant(255);
    auto colorBufferCRecomputedB = colorBufferC;
    colorBufferCRecomputedB.block(0, colorBufferA.cols(), 4, colorBufferB.cols()).setConstant(255);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, normalBufferA, normalBufferB, normalBufferC, colorBufferA, emptyColorBuffer, colorBufferCRecomputedB, emptyMaterialBuffer, emptyMaterialBuffer, emptyMaterialBuffer, indexBufferAWOM, indexBufferBWOM, indexBufferCWOM);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, normalBufferA, normalBufferB, normalBufferC, emptyColorBuffer, colorBufferB, colorBufferCRecomputedA, materialBufferA, materialBufferB, materialBufferC, indexBufferAWM, indexBufferBWM, indexBufferCWM);

    // Initialize helpers for dummy material
    auto indexBufferCWMEmptyA = indexBufferCWM;
    indexBufferCWMEmptyA(3, 0) = 1;
    indexBufferCWMEmptyA(3, 1) = 1;
    indexBufferCWMEmptyA(3, 2) = 0;
    auto indexBufferCWMEmptyB = indexBufferCWM;
    indexBufferCWMEmptyB(3, 0) = 0;
    indexBufferCWMEmptyB(3, 1) = 0;
    indexBufferCWMEmptyB(3, 2) = 1;

    // Dummy material (without vertex color)
    CGMaterial dummyMaterialWOVC;
    auto materialBufferCEmptyAWOVC = materialBufferB;
    materialBufferCEmptyAWOVC.push_back(dummyMaterialWOVC);
    auto materialBufferCEmptyBWOVC = materialBufferA;
    materialBufferCEmptyBWOVC.push_back(dummyMaterialWOVC);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyColorBuffer, emptyColorBuffer, emptyColorBuffer, materialBufferA, emptyMaterialBuffer, materialBufferCEmptyBWOVC, indexBufferAWM, indexBufferBWOM, indexBufferCWMEmptyB);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, emptyColorBuffer, emptyColorBuffer, emptyColorBuffer, emptyMaterialBuffer, materialBufferB, materialBufferCEmptyAWOVC, indexBufferAWOM, indexBufferBWM, indexBufferCWMEmptyA);

    // Dummy material (with vertex color)
    CGMaterial dummyMaterialWVC;
    dummyMaterialWVC.m_diffuseColorFromVertexColor = true;
    dummyMaterialWVC.m_alphaFromVertexColor = true;
    auto materialBufferCEmptyAWVC = materialBufferB;
    materialBufferCEmptyAWVC.push_back(dummyMaterialWVC);
    auto materialBufferCEmptyBWVC = materialBufferA;
    materialBufferCEmptyBWVC.push_back(dummyMaterialWVC);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, colorBufferA, colorBufferB, colorBufferC, materialBufferA, emptyMaterialBuffer, materialBufferCEmptyBWVC, indexBufferAWM, indexBufferBWOM, indexBufferCWMEmptyB);
    checkCGMeshMerge(vertexBufferA, vertexBufferB, vertexBufferC, emptyNormalBuffer, emptyNormalBuffer, emptyNormalBuffer, colorBufferA, colorBufferB, colorBufferC, emptyMaterialBuffer, materialBufferB, materialBufferCEmptyAWVC, indexBufferAWOM, indexBufferBWM, indexBufferCWMEmptyA);
}

#endif // HAVE_EIGEN3
