/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/DHParameters.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Helper function to compute the homogeneous transform \f$ {}_{i-1} D_i \f$
Eigen::Isometry3d compute_im1Di(const double& a, const double& alpha, const double& d, const double& theta)
{
    Eigen::Isometry3d im1Di = Eigen::Isometry3d::Identity();
    const double cosAlpha = cos(alpha);
    const double sinAlpha = sin(alpha);
    const double cosTheta = cos(theta);
    const double sinTheta = sin(theta);
    im1Di(0, 0) = cosTheta;
    im1Di(0, 1) = -sinTheta;
    im1Di(0, 2) = 0;
    im1Di(0, 3) = a;
    im1Di(1, 0) = sinTheta * cosAlpha;
    im1Di(1, 1) = cosTheta * cosAlpha;
    im1Di(1, 2) = -sinAlpha;
    im1Di(1, 3) = -sinAlpha * d;
    im1Di(2, 0) = sinTheta * sinAlpha;
    im1Di(2, 1) = cosTheta * sinAlpha;
    im1Di(2, 2) = cosAlpha;
    im1Di(2, 3) = cosAlpha * d;
    return im1Di;
}

//! Test construction and basic usage of DH parameters
TEST(DHParameters, ConstructionBasicUsage)
{
    // Initialize helpers
    const double a = 1.23;
    const double alpha = 0.123;
    const double d = -2.34;
    const double theta = -0.234;
    const Eigen::Isometry3d im1Di = compute_im1Di(a, alpha, d, theta);
    const Eigen::Isometry3d iDim1 = im1Di.inverse();
    const Eigen::Isometry3d im1Di_Alpha90deg = compute_im1Di(a, M_PI_2, d, theta);
    const Eigen::Isometry3d iDim1_Alpha90deg = im1Di_Alpha90deg.inverse();
    const Eigen::Isometry3d im1Di_Theta90deg = compute_im1Di(a, alpha, d, M_PI_2);
    const Eigen::Isometry3d iDim1_Theta90deg = im1Di_Theta90deg.inverse();
    Eigen::Isometry3d Dinvalid = Eigen::Isometry3d::Identity();
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 4; j++)
            Dinvalid(i, j) = 0;

    // Construction
    // ------------
    // Default constructor
    DHParameters dhA;
    ASSERT_EQ(dhA.a(), 0);
    ASSERT_EQ(dhA.alpha(), 0);
    ASSERT_EQ(dhA.d(), 0);
    ASSERT_EQ(dhA.theta(), 0);
    ASSERT_TRUE(dhA.parameters().norm() == 0);

    // Specialized constructor (single parameters)
    DHParameters dhB(a, alpha, d, theta);
    ASSERT_EQ(dhB.a(), a);
    ASSERT_EQ(dhB.alpha(), alpha);
    ASSERT_EQ(dhB.d(), d);
    ASSERT_EQ(dhB.theta(), theta);

    // Specialized constructor (rotation and translation)
    DHParameters dhC(im1Di.linear(), im1Di.translation());
    ASSERT_EQ(dhC.a(), a);
    ASSERT_EQ(dhC.alpha(), alpha);
    ASSERT_EQ(dhC.d(), d);
    ASSERT_EQ(dhC.theta(), theta);

    // Specialized constructor (transform)
    DHParameters dhD(im1Di);
    ASSERT_EQ(dhD.a(), a);
    ASSERT_EQ(dhD.alpha(), alpha);
    ASSERT_EQ(dhD.d(), d);
    ASSERT_EQ(dhD.theta(), theta);

    // Conversions
    // -----------
    // toTransform()
    const Eigen::Isometry3d TA = DHParameters(a, alpha, d, theta).toTransform();
    ASSERT_LT((TA.linear() - im1Di.linear()).norm(), 1e-9);
    ASSERT_LT((TA.translation() - im1Di.translation()).norm(), 1e-9);

    // fromTransform()
    DHParameters dhE;
    ASSERT_EQ(dhE.fromTransform(im1Di.linear(), im1Di.translation(), false), 0);
    ASSERT_LT(dhE.fromTransform(im1Di.linear(), im1Di.translation(), true), 1e-9);
    ASSERT_EQ(dhE.a(), a);
    ASSERT_EQ(dhE.alpha(), alpha);
    ASSERT_EQ(dhE.d(), d);
    ASSERT_EQ(dhE.theta(), theta);
    DHParameters dhEAlpha90deg;
    ASSERT_EQ(dhEAlpha90deg.fromTransform(im1Di_Alpha90deg.linear(), im1Di_Alpha90deg.translation(), false), 0);
    ASSERT_LT(dhEAlpha90deg.fromTransform(im1Di_Alpha90deg.linear(), im1Di_Alpha90deg.translation(), true), 1e-9);
    ASSERT_EQ(dhEAlpha90deg.a(), a);
    ASSERT_EQ(dhEAlpha90deg.alpha(), M_PI_2);
    ASSERT_EQ(dhEAlpha90deg.d(), d);
    ASSERT_EQ(dhEAlpha90deg.theta(), theta);
    DHParameters dhETheta90deg;
    ASSERT_EQ(dhETheta90deg.fromTransform(im1Di_Theta90deg.linear(), im1Di_Theta90deg.translation(), false), 0);
    ASSERT_LT(dhETheta90deg.fromTransform(im1Di_Theta90deg.linear(), im1Di_Theta90deg.translation(), true), 1e-9);
    ASSERT_EQ(dhETheta90deg.a(), a);
    ASSERT_EQ(dhETheta90deg.alpha(), alpha);
    ASSERT_EQ(dhETheta90deg.d(), d);
    ASSERT_EQ(dhETheta90deg.theta(), M_PI_2);
    DHParameters dhEinvalid;
    ASSERT_GT(dhEinvalid.fromTransform(Dinvalid.linear(), Dinvalid.translation(), true), 1e-9);
    DHParameters dhF;
    ASSERT_EQ(dhF.fromTransform(im1Di, false), 0);
    ASSERT_LT(dhF.fromTransform(im1Di, true), 1e-9);
    ASSERT_EQ(dhF.a(), a);
    ASSERT_EQ(dhF.alpha(), alpha);
    ASSERT_EQ(dhF.d(), d);
    ASSERT_EQ(dhF.theta(), theta);
    DHParameters dhFinvalid;
    ASSERT_GT(dhFinvalid.fromTransform(Dinvalid.linear(), Dinvalid.translation(), true), 1e-9);

    // toTransformInverse()
    const Eigen::Isometry3d TB = DHParameters(a, alpha, d, theta).toTransformInverse();
    ASSERT_LT((TB.linear() - iDim1.linear()).norm(), 1e-9);
    ASSERT_LT((TB.translation() - iDim1.translation()).norm(), 1e-9);

    // fromTransformInverse()
    DHParameters dhG;
    ASSERT_EQ(dhG.fromTransformInverse(iDim1.linear(), iDim1.translation(), false), 0);
    ASSERT_LT(dhG.fromTransformInverse(iDim1.linear(), iDim1.translation(), true), 1e-9);
    ASSERT_EQ(dhG.a(), a);
    ASSERT_EQ(dhG.alpha(), alpha);
    ASSERT_EQ(dhG.d(), d);
    ASSERT_EQ(dhG.theta(), theta);
    DHParameters dhGAlpha90deg;
    ASSERT_EQ(dhGAlpha90deg.fromTransformInverse(iDim1_Alpha90deg.linear(), iDim1_Alpha90deg.translation(), false), 0);
    ASSERT_LT(dhGAlpha90deg.fromTransformInverse(iDim1_Alpha90deg.linear(), iDim1_Alpha90deg.translation(), true), 1e-9);
    ASSERT_EQ(dhGAlpha90deg.a(), a);
    ASSERT_EQ(dhGAlpha90deg.alpha(), M_PI_2);
    ASSERT_EQ(dhGAlpha90deg.d(), d);
    ASSERT_EQ(dhGAlpha90deg.theta(), theta);
    DHParameters dhGTheta90deg;
    ASSERT_EQ(dhGTheta90deg.fromTransformInverse(iDim1_Theta90deg.linear(), iDim1_Theta90deg.translation(), false), 0);
    ASSERT_LT(dhGTheta90deg.fromTransformInverse(iDim1_Theta90deg.linear(), iDim1_Theta90deg.translation(), true), 1e-9);
    ASSERT_EQ(dhGTheta90deg.a(), a);
    ASSERT_EQ(dhGTheta90deg.alpha(), alpha);
    ASSERT_EQ(dhGTheta90deg.d(), d);
    ASSERT_EQ(dhGTheta90deg.theta(), M_PI_2);
    DHParameters dhGinvalid;
    ASSERT_GT(dhGinvalid.fromTransformInverse(Dinvalid.linear(), Dinvalid.translation(), true), 1e-9);
    DHParameters dhH;
    ASSERT_EQ(dhH.fromTransformInverse(iDim1, false), 0);
    ASSERT_LT(dhH.fromTransformInverse(iDim1, true), 1e-9);
    ASSERT_EQ(dhH.a(), a);
    ASSERT_EQ(dhH.alpha(), alpha);
    ASSERT_EQ(dhH.d(), d);
    ASSERT_EQ(dhH.theta(), theta);
    DHParameters dhHinvalid;
    ASSERT_GT(dhHinvalid.fromTransformInverse(Dinvalid.linear(), Dinvalid.translation(), true), 1e-9);

    // Setters
    // -------
    DHParameters dhI;
    dhI.a() = a;
    ASSERT_EQ(dhI.a(), a);
    dhI.alpha() = alpha;
    ASSERT_EQ(dhI.alpha(), alpha);
    dhI.d() = d;
    ASSERT_EQ(dhI.d(), d);
    dhI.theta() = theta;
    ASSERT_EQ(dhI.theta(), theta);
    dhI.setParameters(1, 2, 3, 4);
    ASSERT_EQ(dhI.a(), 1);
    ASSERT_EQ(dhI.alpha(), 2);
    ASSERT_EQ(dhI.d(), 3);
    ASSERT_EQ(dhI.theta(), 4);

    // Getters
    // -------
    const DHParameters dhJ(a, alpha, d, theta);
    ASSERT_EQ(dhJ.a(), a);
    ASSERT_EQ(dhJ.alpha(), alpha);
    ASSERT_EQ(dhJ.d(), d);
    ASSERT_EQ(dhJ.theta(), theta);
    ASSERT_EQ(dhJ.parameters()(0), a);
    ASSERT_EQ(dhJ.parameters()(1), alpha);
    ASSERT_EQ(dhJ.parameters()(2), d);
    ASSERT_EQ(dhJ.parameters()(3), theta);
}

#endif // HAVE_EIGEN3
