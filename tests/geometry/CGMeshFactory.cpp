/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

// Flag to enable writing mesh to disk
#define WRITE_MESH

#include "broccoli/geometry/CGMeshFactory.hpp"
#include "broccoli/geometry/CGMeshTools.hpp"
#include "broccoli/io/ply/PLYFileConverter.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Helper method to test meshes
/*!
 * \param [in] mesh The mesh to check
 * \param [in] expectedTriangleCount The expected triangle count (use "-1" for unknown but greater than zero)
 * \param [in] filePath Path to output mesh file without extension (use "" for no file output)
 */
static inline void checkMesh(const CGMesh& mesh, const int& expectedTriangleCount, const std::string& filePath)
{
    // Check validity
    CGMeshResult result = CGMeshResult::UNKNOWN;
    if (mesh.isValid(&result) == false) {
        std::cout << "Invalid mesh '" << filePath << "': " << CGMeshResultString(result) << std::endl;
        ASSERT_TRUE(mesh.isValid());
    }

    // Check triangle count
    if (expectedTriangleCount >= 0)
        ASSERT_EQ(mesh.m_indexBuffer.cols(), expectedTriangleCount);
    else
        ASSERT_GT(mesh.m_indexBuffer.cols(), 0);

    // Check, if there is unused data
    CGMesh copy = mesh;
    const auto unusedDataCount = CGMeshTools::cleanup(copy);
    ASSERT_EQ(unusedDataCount[0], 0);
    ASSERT_EQ(unusedDataCount[1], 0);

    // Write mesh
    if (filePath.size() > 0) {
#ifdef WRITE_MESH
        io::PLYFile plyFile;
        ASSERT_TRUE(io::PLYFileConverter::convert(mesh, plyFile));
        ASSERT_TRUE(plyFile.writeFile(filePath + ".ply", broccoli::io::PLYFormat::Type::BINARY_LITTLE_ENDIAN));
#endif
    }
}

//! Check createPlane()
TEST(CGMeshFactory, createPlane)
{
    checkMesh(CGMeshFactory::createPlane(1.0, 2.0), 2, "CGMeshFactory_createPlane"); // Valid
#ifdef NDEBUG
    checkMesh(CGMeshFactory::createPlane(-1.0, 2.0), 0, ""); // Invalid: invalid first dimension
    checkMesh(CGMeshFactory::createPlane(0.0, 2.0), 0, ""); // Invalid: invalid first dimension
    checkMesh(CGMeshFactory::createPlane(1.0, -2.0), 0, ""); // Invalid: invalid second dimension
    checkMesh(CGMeshFactory::createPlane(1.0, 0.0), 0, ""); // Invalid: invalid second dimension
#endif
}

//! Check createBox()
TEST(CGMeshFactory, createBox)
{
    checkMesh(CGMeshFactory::createBox(1.0, 2.0, 3.0), 6 * 2, "CGMeshFactory_createBox"); // Valid
#ifdef NDEBUG
    checkMesh(CGMeshFactory::createBox(-1.0, 2.0, 3.0), 0, ""); // Invalid: invalid first dimension
    checkMesh(CGMeshFactory::createBox(0.0, 2.0, 3.0), 0, ""); // Invalid: invalid first dimension
    checkMesh(CGMeshFactory::createBox(1.0, -2.0, 3.0), 0, ""); // Invalid: invalid second dimension
    checkMesh(CGMeshFactory::createBox(1.0, 0.0, 3.0), 0, ""); // Invalid: invalid second dimension
    checkMesh(CGMeshFactory::createBox(1.0, 2.0, -3.0), 0, ""); // Invalid: invalid third dimension
    checkMesh(CGMeshFactory::createBox(1.0, 2.0, 0.0), 0, ""); // Invalid: invalid third dimension
#endif
}

//! Check createIcoSphere()
TEST(CGMeshFactory, createIcoSphere)
{
    checkMesh(CGMeshFactory::createIcoSphere(1.5, 2), -1, "CGMeshFactory_createIcoSphere"); // Valid
#ifdef NDEBUG
    checkMesh(CGMeshFactory::createIcoSphere(-1.0, 2), 0, ""); // Invalid: invalid radius
    checkMesh(CGMeshFactory::createIcoSphere(0.0, 2), 0, ""); // Invalid: invalid radius
#endif
}

//! Check createCircleSlice()
TEST(CGMeshFactory, createCircleSlice)
{
    checkMesh(CGMeshFactory::createCircleSlice(1.0, 2.0, 2, 0.0, 0.25 * M_PI, 10), -1, "CGMeshFactory_createCircleSlice"); // Valid
}

//! Check createCircleSector()
TEST(CGMeshFactory, createCircleSector)
{
    checkMesh(CGMeshFactory::createCircleSector(2.0, 0.0, 0.25 * M_PI, 10), -1, "CGMeshFactory_createCircleSector"); // Valid
}

//! Check createCircle()
TEST(CGMeshFactory, createCircle)
{
    checkMesh(CGMeshFactory::createCircle(2.0, 10), -1, "CGMeshFactory_createCircle"); // Valid
}

//! Check createCylinderSlice()
TEST(CGMeshFactory, createCylinderSlice)
{
    const double minimumRadiusDefault = 1.0;
    const double maximumRadiusDefault = 2.0;
    const size_t stepsRadiusDefault = 3;
    const double minimumPhiDefault = 0.0;
    const double maximumPhiDefault = 0.25 * M_PI;
    const size_t stepsPhiDefault = 10;
    const double minimumZDefault = 0.0;
    const double maximumZDefault = 1.0;
    const size_t stepsZDefault = 2;
    for (int r = 0; r < 3; r++) {
        double minimumRadius = minimumRadiusDefault;
        double maximumRadius = maximumRadiusDefault;
        size_t stepsRadius = stepsRadiusDefault;
        std::string tagRadius = "rX";
        if (r == 1) {
            minimumRadius = maximumRadius;
            tagRadius = "rS";
            stepsRadius = 0;
        } else if (r == 2) {
            minimumRadius = 0.0;
            tagRadius = "r0";
        }
        for (int p = 0; p < 3; p++) {
            double minimumPhi = minimumPhiDefault;
            double maximumPhi = maximumPhiDefault;
            size_t stepsPhi = stepsPhiDefault;
            std::string tagPhi = "pX";
            if (p == 1) {
                minimumPhi = maximumPhi;
                tagPhi = "pS";
                stepsPhi = 0;
            } else if (p == 2) {
                minimumPhi = 0.0;
                maximumPhi = 2.0 * M_PI;
                tagPhi = "pF";
            }
            for (int z = 0; z < 2; z++) {
                double minimumZ = minimumZDefault;
                double maximumZ = maximumZDefault;
                size_t stepsZ = stepsZDefault;
                std::string tagZ = "zX";
                if (z == 1) {
                    minimumZ = maximumZ;
                    tagZ = "zS";
                    stepsZ = 0;
                }
                if ((stepsRadius > 0 && stepsPhi) > 0 || (stepsPhi > 0 && stepsZ > 0) || (stepsRadius > 0 && stepsZ > 0))
                    checkMesh(CGMeshFactory::createCylinderSlice(minimumRadius, maximumRadius, stepsRadius, minimumPhi, maximumPhi, stepsPhi, minimumZ, maximumZ, stepsZ), -1, "CGMeshFactory_createCylinderSlice_" + tagRadius + "_" + tagPhi + "_" + tagZ);
                else {
#ifdef NDEBUG
                    // Invalid input -> no real surface
                    checkMesh(CGMeshFactory::createCylinderSlice(minimumRadius, maximumRadius, stepsRadius, minimumPhi, maximumPhi, stepsPhi, minimumZ, maximumZ, stepsZ), 0, "");
#endif
                }
            }
        }
    }
#ifdef NDEBUG
    checkMesh(CGMeshFactory::createCylinderSlice(-1.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: minimum radius < 0
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 0.0, 0, 0.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: maximum radius = 0
    checkMesh(CGMeshFactory::createCylinderSlice(3.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: maximum radius < minimum radius
    checkMesh(CGMeshFactory::createCylinderSlice(1.0, 1.0, 1, 0.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: maximum radius = minimum radius but steps != 0
    checkMesh(CGMeshFactory::createCylinderSlice(0.5, 1.0, 0, 0.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: maximum radius != minimum radius but steps = 0
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 2.0, 3, -1.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: minimum phi < 0
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 2.0, 3, M_PI / 2.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: maximum phi < minimum phi
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 2.0, 3, M_PI / 4.0, M_PI / 4.0, 10, 0.0, 1.0, 2), 0, ""); // Invalid: maximum phi = minimum phi but steps != 0
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 0, 0.0, 1.0, 2), 0, ""); // Invalid: maximum phi != minimum phi but steps = 0
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 2.0, 1.0, 2), 0, ""); // Invalid: maximum z < minimum z
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 1.0, 1.0, 2), 0, ""); // Invalid: maximum z = minimum z but steps != 0
    checkMesh(CGMeshFactory::createCylinderSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, 1.0, 0), 0, ""); // Invalid: maximum z != minimum z but steps = 0
#endif
}

//! Check createCylinderSector()
TEST(CGMeshFactory, createCylinderSector)
{
    checkMesh(CGMeshFactory::createCylinderSector(2.0, 0.0, 0.25 * M_PI, 10, 0.0, 1.0, 1), -1, "CGMeshFactory_createCylinderSector"); // Valid
}

//! Check createCylinder()
TEST(CGMeshFactory, createCylinder)
{
    checkMesh(CGMeshFactory::createCylinder(2.0, 10, 0.0, 1.0, 1), -1, "CGMeshFactory_createCylinder"); // Valid
}

//! Check createSphereSlice()
TEST(CGMeshFactory, createSphereSlice)
{
    const double minimumRadiusDefault = 1.0;
    const double maximumRadiusDefault = 2.0;
    const size_t stepsRadiusDefault = 3;
    const double minimumPhiDefault = 0.0;
    const double maximumPhiDefault = 0.25 * M_PI;
    const size_t stepsPhiDefault = 16;
    const double minimumThetaDefault = 0.25 * M_PI;
    const double maximumThetaDefault = 0.5 * M_PI;
    const size_t stepsThetaDefault = 8;
    for (int r = 0; r < 3; r++) {
        double minimumRadius = minimumRadiusDefault;
        double maximumRadius = maximumRadiusDefault;
        size_t stepsRadius = stepsRadiusDefault;
        std::string tagRadius = "rX";
        if (r == 1) {
            minimumRadius = maximumRadius;
            tagRadius = "rS";
            stepsRadius = 0;
        } else if (r == 2) {
            minimumRadius = 0.0;
            tagRadius = "r0";
        }
        for (int p = 0; p < 3; p++) {
            double minimumPhi = minimumPhiDefault;
            double maximumPhi = maximumPhiDefault;
            size_t stepsPhi = stepsPhiDefault;
            std::string tagPhi = "pX";
            if (p == 1) {
                minimumPhi = maximumPhi;
                tagPhi = "pS";
                stepsPhi = 0;
            } else if (p == 2) {
                minimumPhi = 0.0;
                maximumPhi = 2.0 * M_PI;
                tagPhi = "pF";
            }
            for (int t = 0; t < 5; t++) {
                double minimumTheta = minimumThetaDefault;
                double maximumTheta = maximumThetaDefault;
                size_t stepsTheta = stepsThetaDefault;
                std::string tagTheta = "tXX";
                if (t == 1) {
                    minimumTheta = maximumTheta;
                    tagTheta = "tSS";
                    stepsTheta = 0;
                } else if (t == 2) {
                    minimumTheta = 0.0;
                    maximumTheta = M_PI;
                    tagTheta = "tFF";
                } else if (t == 3) {
                    minimumTheta = 0.0;
                    tagTheta = "tFX";
                } else if (t == 4) {
                    maximumTheta = M_PI;
                    tagTheta = "tXF";
                }
                if ((stepsRadius > 0 && stepsPhi) > 0 || (stepsPhi > 0 && stepsTheta > 0) || (stepsRadius > 0 && stepsTheta > 0))
                    checkMesh(CGMeshFactory::createSphereSlice(minimumRadius, maximumRadius, stepsRadius, minimumPhi, maximumPhi, stepsPhi, minimumTheta, maximumTheta, stepsTheta), -1, "CGMeshFactory_createSphereSlice_" + tagRadius + "_" + tagPhi + "_" + tagTheta);
                else {
#ifdef NDEBUG
                    // Invalid input -> no real surface
                    checkMesh(CGMeshFactory::createSphereSlice(minimumRadius, maximumRadius, stepsRadius, minimumPhi, maximumPhi, stepsPhi, minimumTheta, maximumTheta, stepsTheta), 0, "");
#endif
                }
            }
        }
    }
#ifdef NDEBUG
    checkMesh(CGMeshFactory::createSphereSlice(-1.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: minimum radius < 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 0.0, 0, 0.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum radius = 0
    checkMesh(CGMeshFactory::createSphereSlice(3.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum radius < minimum radius
    checkMesh(CGMeshFactory::createSphereSlice(1.0, 1.0, 1, 0.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum radius = minimum radius but steps != 0
    checkMesh(CGMeshFactory::createSphereSlice(0.5, 1.0, 0, 0.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum radius != minimum radius but steps = 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, -1.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: minimum phi < 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, M_PI / 2.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum phi < minimum phi
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, M_PI / 4.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum phi = minimum phi but steps != 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 0, 0.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum phi != minimum phi but steps = 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, -1.0, M_PI, 0), 0, ""); // Invalid: minimum theta < 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, 2.0 * M_PI, 0), 0, ""); // Invalid: maximum theta > pi
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, M_PI, M_PI, 0), 0, ""); // Invalid: minimum theta = maximum theta = pi
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, 0.0, 0), 0, ""); // Invalid: minimum theta = maximum theta = 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, M_PI / 2.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum theta < minimum theta
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, M_PI / 4.0, M_PI / 4.0, 4), 0, ""); // Invalid: maximum theta = minimum theta but steps != 0
    checkMesh(CGMeshFactory::createSphereSlice(0.0, 2.0, 3, 0.0, M_PI / 4.0, 10, 0.0, M_PI / 4.0, 0), 0, ""); // Invalid: maximum theta != minimum theta but steps = 0
#endif
}

//! Check createSphere()
TEST(CGMeshFactory, createSphere)
{
    checkMesh(CGMeshFactory::createSphere(2.0, 32, 16), -1, "CGMeshFactory_createSphere"); // Valid
}

//! Check createCone()
TEST(CGMeshFactory, createCone)
{
    checkMesh(CGMeshFactory::createCone(1.0, 16, 1.0, 2.0, false), -1, "CGMeshFactory_createCone_withoutBase"); // Valid
    checkMesh(CGMeshFactory::createCone(1.0, 16, 1.0, 2.0, true), -1, "CGMeshFactory_createCone_withBase"); // Valid
#ifdef NDEBUG
    checkMesh(CGMeshFactory::createCone(-1.0, 16, 1.0, 2.0, false), 0, ""); // Invalid: invalid radius
    checkMesh(CGMeshFactory::createCone(1.0, 2, 1.0, 2.0, false), 0, ""); // Invalid: invalid step size for phi
    checkMesh(CGMeshFactory::createCone(1.0, 16, 1.0, 1.0, false), 0, ""); // Invalid: maximum z = minimum z
    checkMesh(CGMeshFactory::createCone(1.0, 16, 1.0, 0.5, false), 0, ""); // Invalid: maximum z < minimum z
#endif
}

//! Check extrudeOutlineAlong3DPath()
TEST(CGMeshFactory, extrudeOutlineAlong3DPath)
{
    // Create outline
    CGMesh outline;
    outline.m_vertexBuffer.resize(Eigen::NoChange, 4);
    outline.m_vertexBuffer << 1.0, -1.0, -1.0, 1.0,
        1.0, 1.0, -1.0, -1.0,
        0.0, 0.0, 0.0, 0.0;
    outline.m_normalBuffer = M_SQRT1_2 * outline.m_vertexBuffer;
    outline.scale(0.1);

    // Create path
    Eigen::Matrix<double, 3, Eigen::Dynamic> path;
    path.resize(Eigen::NoChange, 10);
    path.col(0) = Eigen::Vector3d(0.0, 0.0, 0.0);
    path.col(1) = Eigen::Vector3d(1.0, 0.0, 0.0);
    path.col(2) = Eigen::Vector3d(1.0, 0.0, 0.0); // coinciding
    path.col(3) = Eigen::Vector3d(1.0, 0.0, 0.0); // coinciding
    path.col(4) = Eigen::Vector3d(1.5, 1.0, 0.0);
    path.col(5) = Eigen::Vector3d(2.0, 2.0, 0.0); // collinear
    path.col(6) = Eigen::Vector3d(2.5, 3.0, 0.0); // collinear
    path.col(7) = Eigen::Vector3d(3.0, 3.0, 0.0);
    path.col(8) = Eigen::Vector3d(2.7, 3.0, 0.0); // reverse direction
    path.col(9) = Eigen::Vector3d(3.5, 3.0, 0.0); // normal direction

    // Check meshing
    checkMesh(CGMeshFactory::extrudeOutlineAlong3DPath(path, outline, false, Eigen::Vector3d(0.0, 0.0, 1.0)), -1, "CGMeshFactory_extrudeOutlineAlong3DPath_open"); // Valid
    checkMesh(CGMeshFactory::extrudeOutlineAlong3DPath(path, outline, true, Eigen::Vector3d(0.0, 0.0, 1.0)), -1, "CGMeshFactory_extrudeOutlineAlong3DPath_closed"); // Valid

#ifdef NDEBUG
    Eigen::Matrix<double, 3, Eigen::Dynamic> emptyPath;
    checkMesh(CGMeshFactory::extrudeOutlineAlong3DPath(emptyPath, outline, false, Eigen::Vector3d(0.0, 0.0, 1.0)), 0, ""); // Invalid: empty path
    Eigen::Matrix<double, 3, Eigen::Dynamic> invalidPathOnePoint;
    invalidPathOnePoint.resize(Eigen::NoChange, 1);
    invalidPathOnePoint.col(0) = Eigen::Vector3d(0.0, 0.0, 0.0);
    checkMesh(CGMeshFactory::extrudeOutlineAlong3DPath(invalidPathOnePoint, outline, false, Eigen::Vector3d(0.0, 0.0, 1.0)), 0, ""); // Invalid: path has only one point
    Eigen::Matrix<double, 3, Eigen::Dynamic> invalidPathCoincidingPoints;
    invalidPathCoincidingPoints.resize(Eigen::NoChange, 2);
    invalidPathCoincidingPoints.col(0) = Eigen::Vector3d(0.0, 0.0, 0.0);
    invalidPathCoincidingPoints.col(1) = invalidPathCoincidingPoints.col(0);
    checkMesh(CGMeshFactory::extrudeOutlineAlong3DPath(invalidPathCoincidingPoints, outline, false, Eigen::Vector3d(0.0, 0.0, 1.0)), 0, ""); // Invalid: path has two coinciding points
#endif
}

//! Check createCylindrical3DPath()
TEST(CGMeshFactory, createCylindrical3DPath)
{
    size_t wayPointCount = 100;

    // Create path
    Eigen::Matrix<double, 3, Eigen::Dynamic> path;
    path.resize(Eigen::NoChange, wayPointCount);
    CylindricalCoordinates coordinates(0.5, 0.0, 0.0);
    for (Eigen::Index i = 0; i < path.cols(); i++) {
        coordinates.setPhi((4.0 * M_PI / wayPointCount) * i);
        coordinates.setZ((0.5 / wayPointCount) * i);
        path.col(i) = coordinates.toCartesian();
    }

    // Check meshing
    checkMesh(CGMeshFactory::createCylindrical3DPath(path, 0.01, 16, false, false), -1, "CGMeshFactory_createCylindrical3DPath_noCaps"); // Valid
    checkMesh(CGMeshFactory::createCylindrical3DPath(path, 0.01, 16, true, false), -1, "CGMeshFactory_createCylindrical3DPath_flatCaps"); // Valid
    checkMesh(CGMeshFactory::createCylindrical3DPath(path, 0.01, 16, false, true), -1, "CGMeshFactory_createCylindrical3DPath_roundCaps"); // Valid

#ifdef NDEBUG
    Eigen::Matrix<double, 3, Eigen::Dynamic> emptyPath;
    checkMesh(CGMeshFactory::createCylindrical3DPath(emptyPath, 0.01, 16, false, false), 0, ""); // Invalid: empty path
    Eigen::Matrix<double, 3, Eigen::Dynamic> invalidPathOnePoint;
    invalidPathOnePoint.resize(Eigen::NoChange, 1);
    invalidPathOnePoint.col(0) = Eigen::Vector3d(0.0, 0.0, 0.0);
    checkMesh(CGMeshFactory::createCylindrical3DPath(invalidPathOnePoint, 0.01, 16, false, false), 0, ""); // Invalid: path has only one point
    Eigen::Matrix<double, 3, Eigen::Dynamic> invalidPathCoincidingPoints;
    invalidPathCoincidingPoints.resize(Eigen::NoChange, 2);
    invalidPathCoincidingPoints.col(0) = Eigen::Vector3d(0.0, 0.0, 0.0);
    invalidPathCoincidingPoints.col(1) = invalidPathCoincidingPoints.col(0);
    checkMesh(CGMeshFactory::createCylindrical3DPath(invalidPathCoincidingPoints, 0.01, 16, false, false), 0, ""); // Invalid: path has two coinciding points
    checkMesh(CGMeshFactory::createCylindrical3DPath(path, -0.01, 16, false, false), 0, ""); // Invalid: radius invalid
    checkMesh(CGMeshFactory::createCylindrical3DPath(path, 0.0, 16, false, false), 0, ""); // Invalid: radius invalid
    checkMesh(CGMeshFactory::createCylindrical3DPath(path, 0.01, 2, false, false), 0, ""); // Invalid: steps phi invalid
#endif
}

//! Check createVolumeSignedDistanceField()
TEST(CGMeshFactory, createVolumeSignedDistanceField)
{
    // Initialize helpers
    Eigen::Vector3d gridOrigin;
    std::array<size_t, 3> gridSize;
    CGMeshFactory::VoxelGridOfDoubles distanceGrid, distanceGridInverted;
    CGMeshFactory::VoxelGridOfColors colorGridNotInterpolated, colorGridInterpolated;
    const double voxelSize = 0.05;
    const Eigen::Vector3d voxelSizeVector = Eigen::Vector3d(voxelSize, voxelSize, voxelSize);

    // Create input mesh
    CGMesh inputMeshPartA = CGMeshFactory::createIcoSphere(0.5, 0);
    inputMeshPartA.translate(Eigen::Vector3d(-0.6, 0.0, 0.0));
    inputMeshPartA.m_colorBuffer.resize(Eigen::NoChange, inputMeshPartA.m_vertexBuffer.cols());
    inputMeshPartA.m_colorBuffer.row(0).fill(255);
    inputMeshPartA.m_colorBuffer.row(1).fill(0);
    inputMeshPartA.m_colorBuffer.row(2).fill(0);
    inputMeshPartA.m_colorBuffer.row(3).fill(255);
    CGMesh inputMeshPartB = CGMeshFactory::createIcoSphere(0.5, 0);
    inputMeshPartB.translate(Eigen::Vector3d(0.6, 0.0, 0.0));
    inputMeshPartB.m_colorBuffer.resize(Eigen::NoChange, inputMeshPartB.m_vertexBuffer.cols());
    inputMeshPartB.m_colorBuffer.row(0).fill(0);
    inputMeshPartB.m_colorBuffer.row(1).fill(255);
    inputMeshPartB.m_colorBuffer.row(2).fill(0);
    inputMeshPartB.m_colorBuffer.row(3).fill(255);
    CGMesh inputMesh = CGMeshTools::merge(inputMeshPartA, inputMeshPartB);
    const CGMesh emptyMesh;

    // Create signed distance field (without color interpolation)
    distanceGrid.clear();
    colorGridNotInterpolated.clear();
    ASSERT_TRUE(CGMeshFactory::createVolumeSignedDistanceField(inputMesh, voxelSizeVector, true, gridOrigin, gridSize, distanceGrid, false, &colorGridNotInterpolated));
    ASSERT_GT(distanceGrid.elementCount(), 0);
    ASSERT_EQ(colorGridNotInterpolated.elementCount(), distanceGrid.elementCount());
    for (size_t i = 0; i < colorGridNotInterpolated.elementCount(); i++) {
        ASSERT_TRUE(colorGridNotInterpolated[i][0] == 0 || colorGridNotInterpolated[i][0] == 255);
        ASSERT_TRUE(colorGridNotInterpolated[i][1] == 0 || colorGridNotInterpolated[i][1] == 255);
        ASSERT_EQ(colorGridNotInterpolated[i][2], 0);
        ASSERT_EQ(colorGridNotInterpolated[i][3], 255);
    }
    size_t interiorPoints = 0;
    size_t exteriorPoints = 0;
    for (size_t i = 0; i < distanceGrid.elementCount(); i++) {
        if (distanceGrid[i] < 0.0)
            interiorPoints++;
        else
            exteriorPoints++;
        ASSERT_GT(distanceGrid[i], -10.0);
        ASSERT_LT(distanceGrid[i], 10.0);
    }
    ASSERT_GT(interiorPoints, 0);
    ASSERT_GT(exteriorPoints, 0);

    // Create signed distance field with inverted distances
    ASSERT_TRUE(CGMeshFactory::createVolumeSignedDistanceField(inputMesh, voxelSizeVector, true, gridOrigin, gridSize, distanceGridInverted, true));
    ASSERT_EQ(distanceGridInverted.elementCount(), distanceGrid.elementCount());
    for (size_t i = 0; i < distanceGrid.elementCount(); i++)
        ASSERT_TRUE(distanceGridInverted[i] = -distanceGrid[i]);

    // Create signed distance field (with color interpolation)
    distanceGrid.clear();
    colorGridInterpolated.clear();
    ASSERT_TRUE(CGMeshFactory::createVolumeSignedDistanceField(inputMesh, voxelSizeVector, true, gridOrigin, gridSize, distanceGrid, false, &colorGridInterpolated, 1.0));
    ASSERT_GT(distanceGrid.elementCount(), 0);
    ASSERT_EQ(colorGridInterpolated.elementCount(), distanceGrid.elementCount());
    ASSERT_EQ(colorGridInterpolated.elementCount(), colorGridNotInterpolated.elementCount());
    ASSERT_TRUE(colorGridInterpolated != colorGridNotInterpolated);
    bool redWasInterpolated = false;
    bool greenWasInterpolated = false;
    for (size_t i = 0; i < colorGridInterpolated.elementCount(); i++) {
        if (colorGridInterpolated[i][0] != 0 && colorGridInterpolated[i][0] != 255)
            redWasInterpolated = true;
        if (colorGridInterpolated[i][1] != 0 && colorGridInterpolated[i][1] != 255)
            greenWasInterpolated = true;
        ASSERT_EQ(colorGridInterpolated[i][2], 0);
        ASSERT_EQ(colorGridInterpolated[i][3], 255);
    }
    ASSERT_TRUE(redWasInterpolated);
    ASSERT_TRUE(greenWasInterpolated);

    // Invalid input
#ifdef NDEBUG
    ASSERT_FALSE(CGMeshFactory::createVolumeSignedDistanceField(emptyMesh, voxelSizeVector, true, gridOrigin, gridSize, distanceGrid, false, &colorGridNotInterpolated));
    ASSERT_FALSE(CGMeshFactory::createVolumeSignedDistanceField(emptyMesh, voxelSizeVector, true, gridOrigin, gridSize, distanceGrid, false, &colorGridInterpolated, 1.0));
#endif
}

//! Check createVolumeMarchingCubes()
TEST(CGMeshFactory, createVolumeMarchingCubes)
{
    // Create color and density grid (sphere)
    const std::array<size_t, 3> gridSize{ 21, 21, 41 };
    const Eigen::Vector3d dimensions(2, 2, 4);
    const Eigen::Vector3d center = 0.5 * dimensions;
    const Eigen::Vector3d cellDimensions(dimensions(0) / (gridSize[0] - 1), dimensions(1) / (gridSize[1] - 1), dimensions(2) / (gridSize[2] - 1));
    CGMeshFactory::MarchingCubesDensityGrid densityGrid(gridSize);
    CGMeshFactory::MarchingCubesColorGrid colorGrid(gridSize);
    densityGrid.fill(0);
    colorGrid.fill(CGMesh::ColorType::Zero());
    for (size_t x = 0; x < gridSize[0]; x++) {
        for (size_t y = 0; y < gridSize[1]; y++) {
            for (size_t z = 0; z < gridSize[2]; z++) {
                const Eigen::Vector3d absolutePosition = Eigen::Vector3d(x * cellDimensions(0), y * cellDimensions(1), z * cellDimensions(2));
                const Eigen::Vector3d relativePosition = absolutePosition - center;
                const Eigen::Vector3d relativePositionNormalized = relativePosition.normalized();
                densityGrid(x, y, z) = 1.0 / relativePosition.norm();
                for (Eigen::Index c = 0; c < 3; c++)
                    colorGrid(x, y, z)(c) = fabs(relativePositionNormalized(c)) * 255.0;
                colorGrid(x, y, z)(3) = 255;
            }
        }
    }
    const double densityThreshold = 1.0;

    // Check mesh
    checkMesh(CGMeshFactory::createVolumeMarchingCubes(densityGrid, densityThreshold, dimensions, true, colorGrid, true), -1, "CGMeshFactory_createVolumeMarchingCubes"); // Valid
#ifdef NDEBUG
    CGMeshFactory::MarchingCubesDensityGrid emptyGrid;
    checkMesh(CGMeshFactory::createVolumeMarchingCubes(emptyGrid, densityThreshold, dimensions, true), 0, ""); // Invalid: empty density grid
#endif
}

//! Check remesh() with IcoSphere (obtuse angles)
TEST(CGMeshFactory, remeshIcosphere)
{
    // Initialize helpers
    const double voxelSize = 0.05;

    // Create input mesh
    CGMesh inputMeshPartA = CGMeshFactory::createIcoSphere(0.5, 0);
    inputMeshPartA.translate(Eigen::Vector3d(-0.6, 0.0, 0.0));
    inputMeshPartA.m_colorBuffer.resize(Eigen::NoChange, inputMeshPartA.m_vertexBuffer.cols());
    inputMeshPartA.m_colorBuffer.row(0).fill(255);
    inputMeshPartA.m_colorBuffer.row(1).fill(0);
    inputMeshPartA.m_colorBuffer.row(2).fill(0);
    inputMeshPartA.m_colorBuffer.row(3).fill(255);
    CGMesh inputMeshPartB = CGMeshFactory::createIcoSphere(0.5, 0);
    inputMeshPartB.translate(Eigen::Vector3d(0.6, 0.0, 0.0));
    inputMeshPartB.m_colorBuffer.resize(Eigen::NoChange, inputMeshPartB.m_vertexBuffer.cols());
    inputMeshPartB.m_colorBuffer.row(0).fill(0);
    inputMeshPartB.m_colorBuffer.row(1).fill(255);
    inputMeshPartB.m_colorBuffer.row(2).fill(0);
    inputMeshPartB.m_colorBuffer.row(3).fill(255);
    CGMesh inputMesh = CGMeshTools::merge(inputMeshPartA, inputMeshPartB);
    checkMesh(inputMesh, -1, "CGMeshFactory_remeshIcoSphere_input");
    const CGMesh emptyMesh;

    // Check variants
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, 0.0), -1, "CGMeshFactory_remeshIcoSphere_notinterpolated_thresholdzero"); // Valid, not interpolated, threshold 0
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, 0.2 * voxelSize), -1, "CGMeshFactory_remeshIcoSphere_notinterpolated_thresholdshrink"); // Valid, not interpolated, threshold for shrinking
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, -0.2 * voxelSize), -1, "CGMeshFactory_remeshIcoSphere_notinterpolated_thresholdgrow"); // Valid, not interpolated, threshold for growing
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, voxelSize, 0.0), -1, "CGMeshFactory_remeshIcoSphere_interpolated_thresholdzero"); // Valid, interpolated, threshold 0
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, 0.5 * voxelSize, 0.2 * voxelSize), -1, "CGMeshFactory_remeshIcoSphere_interpolated_thresholdshrink"); // Valid, interpolated, threshold for shrinking
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, 2.0 * voxelSize, -0.2 * voxelSize), -1, "CGMeshFactory_remeshIcoSphere_interpolated_thresholdgrow"); // Valid, interpolated, threshold for growing

#ifdef NDEBUG
    checkMesh(CGMeshFactory::remesh(emptyMesh, voxelSize, false, 0.0), 0, ""); // Invalid: empty input mesh
#endif
}

//! Check remesh() with Box (right angles)
TEST(CGMeshFactory, remeshBox)
{
    // Initialize helpers
    const double voxelSize = 0.02;

    // Create input mesh
    CGMesh inputMesh = CGMeshFactory::createBox(1.0, 0.1, 1.0);
    inputMesh.m_colorBuffer.resize(Eigen::NoChange, inputMesh.m_normalBuffer.cols());
    inputMesh.m_colorBuffer.row(3).fill(255);
    for (Eigen::Index i = 0; i < inputMesh.m_normalBuffer.cols(); i++) {
        for (Eigen::Index j = 0; j < 3; j++)
            inputMesh.m_colorBuffer(j, i) = core::math::clamp(255.0 * fabs(inputMesh.m_normalBuffer(j, i)), 0.0, 255.0);
    }
    checkMesh(inputMesh, -1, "CGMeshFactory_remeshBox_input");

    // Check variants
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, 0.0), -1, "CGMeshFactory_remeshBox_notinterpolated_thresholdzero"); // Valid, not interpolated, threshold 0
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, 0.2 * voxelSize), -1, "CGMeshFactory_remeshBox_notinterpolated_thresholdshrink"); // Valid, not interpolated, threshold for shrinking
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, -0.2 * voxelSize), -1, "CGMeshFactory_remeshBox_notinterpolated_thresholdgrow"); // Valid, not interpolated, threshold for growing
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, voxelSize, 0.0), -1, "CGMeshFactory_remeshBox_interpolated_thresholdzero"); // Valid, interpolated, threshold 0
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, 0.5 * voxelSize, 0.2 * voxelSize), -1, "CGMeshFactory_remeshBox_interpolated_thresholdshrink"); // Valid, interpolated, threshold for shrinking
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, 2.0 * voxelSize, -0.2 * voxelSize), -1, "CGMeshFactory_remeshBox_interpolated_thresholdgrow"); // Valid, interpolated, threshold for growing
}

//! Check remesh() with Cone (acute angles)
TEST(CGMeshFactory, remeshCone)
{
    // Initialize helpers
    const double voxelSize = 0.05;

    // Create input mesh
    CGMesh inputMesh = CGMeshFactory::createCone(0.5, 3, 0.0, 2.0, true);
    inputMesh.m_colorBuffer.resize(Eigen::NoChange, inputMesh.m_vertexBuffer.cols());
    inputMesh.m_colorBuffer.row(3).fill(255);
    for (Eigen::Index i = 0; i < inputMesh.m_vertexBuffer.cols(); i++) {
        for (Eigen::Index j = 0; j < 3; j++)
            inputMesh.m_colorBuffer(j, i) = core::math::clamp(255.0 * fabs(inputMesh.m_vertexBuffer(j, i)), 0.0, 255.0);
    }
    checkMesh(inputMesh, -1, "CGMeshFactory_remeshCone_input");

    // Check variants
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, 0.0), -1, "CGMeshFactory_remeshCone_notinterpolated_thresholdzero"); // Valid, not interpolated, threshold 0
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, 0.2 * voxelSize), -1, "CGMeshFactory_remeshCone_notinterpolated_thresholdshrink"); // Valid, not interpolated, threshold for shrinking
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, -1.0, -0.2 * voxelSize), -1, "CGMeshFactory_remeshCone_notinterpolated_thresholdgrow"); // Valid, not interpolated, threshold for growing
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, voxelSize, 0.0), -1, "CGMeshFactory_remeshCone_interpolated_thresholdzero"); // Valid, interpolated, threshold 0
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, 0.5 * voxelSize, 0.2 * voxelSize), -1, "CGMeshFactory_remeshCone_interpolated_thresholdshrink"); // Valid, interpolated, threshold for shrinking
    checkMesh(CGMeshFactory::remesh(inputMesh, voxelSize, 2.0 * voxelSize, -0.2 * voxelSize), -1, "CGMeshFactory_remeshCone_interpolated_thresholdgrow"); // Valid, interpolated, threshold for growing
}

#endif // HAVE_EIGEN3
