/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVDistanceEvaluator.hpp"
#include "broccoli/core/Time.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Computes all possible representations for the given point SSV element
std::array<SSVPointElement, 1> computePermutations(const SSVPointElement& element)
{
    std::array<SSVPointElement, 1> permutations{
        SSVPointElement(element.point0(), element.radius())
    };
    return permutations;
}

//! Computes all possible representations for the given line SSV element
std::array<SSVLineElement, 2> computePermutations(const SSVLineElement& element)
{
    std::array<SSVLineElement, 2> permutations{
        SSVLineElement(element.point0(), element.point1(), element.radius()),
        SSVLineElement(element.point1(), element.point0(), element.radius()),
    };
    return permutations;
}

//! Computes all possible representations for the given triangle SSV element
std::array<SSVTriangleElement, 6> computePermutations(const SSVTriangleElement& element)
{
    std::array<SSVTriangleElement, 6> permutations{
        SSVTriangleElement(element.point0(), element.point1(), element.point2(), element.radius()),
        SSVTriangleElement(element.point0(), element.point2(), element.point1(), element.radius()),
        SSVTriangleElement(element.point1(), element.point0(), element.point2(), element.radius()),
        SSVTriangleElement(element.point1(), element.point2(), element.point0(), element.radius()),
        SSVTriangleElement(element.point2(), element.point0(), element.point1(), element.radius()),
        SSVTriangleElement(element.point2(), element.point1(), element.point0(), element.radius())
    };
    return permutations;
}

//! Helper function to check the evaluation of two elements
template <class FirstElement, class SecondElement>
static inline void checkEvaluationSSVElements(const FirstElement& firstElement, const SecondElement& secondElement, const double& expectedDistance, const bool& uniqueDistance = true)
{
    // Compute permutations of first and second element
    const auto firstElementPermutations = computePermutations(firstElement);
    const auto secondElementPermutations = computePermutations(secondElement);

    // Loop over all representations of the first element
    for (size_t i = 0; i < firstElementPermutations.size(); i++) {
        // Loop over all representations of the second element
        for (size_t j = 0; j < secondElementPermutations.size(); j++) {
            // Compute distance (non-flipped)
            SSVElementDistance distance;
            SSVDistanceEvaluator::evaluate(firstElementPermutations[i], secondElementPermutations[j], distance);

            // Compute distance (flipped)
            SSVElementDistance flippedDistance;
            SSVDistanceEvaluator::evaluate(secondElementPermutations[j], firstElementPermutations[i], flippedDistance);

            // Check, if distance is as expected
            ASSERT_EQ(distance.distance(), expectedDistance);
            ASSERT_EQ(flippedDistance.distance(), expectedDistance);

            // Check, if flipped result is same as non-flipped result (only, if distance is unique)
            if (uniqueDistance == true) {
                ASSERT_EQ(distance.firstPoint(), flippedDistance.secondPoint());
                ASSERT_EQ(distance.secondPoint(), flippedDistance.firstPoint());
                ASSERT_EQ(distance.firstRadius(), flippedDistance.secondRadius());
                ASSERT_EQ(distance.secondRadius(), flippedDistance.firstRadius());
                const Eigen::Vector3d negativeConnection = -distance.connection();
                ASSERT_EQ(negativeConnection, flippedDistance.connection());
                ASSERT_EQ(distance.connectionLength(), flippedDistance.connectionLength());
                ASSERT_EQ(distance.distance(), flippedDistance.distance());
            }
        }
    }
}

//! Helper function to check the evaluation of two elements
template <class FirstSegment, class SecondSegment>
static inline void checkEvaluationSSVSegments(const FirstSegment& firstSegment, const SecondSegment& secondSegment, const double& expectedDistance, const bool& uniqueDistance = true)
{
    // Compute distance (non-flipped)
    SSVSegmentDistance distance;
    ASSERT_GT(SSVDistanceEvaluator::evaluate(firstSegment, secondSegment, distance), 0);

    // Compute distance (flipped)
    SSVSegmentDistance flippedDistance;
    ASSERT_GT(SSVDistanceEvaluator::evaluate(secondSegment, firstSegment, flippedDistance), 0);

    // Check, if distance is as expected
    ASSERT_EQ(distance.distance(), expectedDistance);
    ASSERT_EQ(flippedDistance.distance(), expectedDistance);

    // Check, if IDs are as expected
    ASSERT_EQ(distance.firstID(), firstSegment.id());
    ASSERT_EQ(distance.secondID(), secondSegment.id());

    // Check, if flipped result is same as non-flipped result (only, if distance is unique)
    if (uniqueDistance == true) {
        ASSERT_EQ(distance.firstPoint(), flippedDistance.secondPoint());
        ASSERT_EQ(distance.secondPoint(), flippedDistance.firstPoint());
        ASSERT_EQ(distance.firstRadius(), flippedDistance.secondRadius());
        ASSERT_EQ(distance.secondRadius(), flippedDistance.firstRadius());
        const Eigen::Vector3d negativeConnection = -distance.connection();
        ASSERT_EQ(negativeConnection, flippedDistance.connection());
        ASSERT_EQ(distance.connectionLength(), flippedDistance.connectionLength());
        ASSERT_EQ(distance.distance(), flippedDistance.distance());
        ASSERT_EQ(distance.firstID(), flippedDistance.secondID());
        ASSERT_EQ(distance.secondID(), flippedDistance.firstID());
    }
}

//! Helper function to benchmark the evaluation of elements
/*! \return Average runtime for one evaluation */
template <class FirstElement, class SecondElement>
static inline double benchmarkEvaluation(const FirstElement& firstElement, const SecondElement& secondElement)
{
    // Initialize helpers
    const size_t sampleCount = 1000;
    Eigen::Vector3d rotationAxis = Eigen::Vector3d(1.0, 0.0, 0.0);
    double rotationAngle = 0.0;
    Eigen::Matrix3d rotation = Eigen::Matrix3d::Identity();
    SSVElementDistance distance;

    // Compute permutations of first and second element
    const auto firstElementPermutations = computePermutations(firstElement);
    const auto secondElementPermutations = computePermutations(secondElement);
    std::vector<std::pair<FirstElement, SecondElement>, Eigen::aligned_allocator<std::pair<FirstElement, SecondElement>>> initialPairList;
    initialPairList.reserve(firstElementPermutations.size() * secondElementPermutations.size());
    for (size_t i = 0; i < firstElementPermutations.size(); i++) {
        for (size_t j = 0; j < secondElementPermutations.size(); j++) {
            initialPairList.push_back({ firstElementPermutations[i], secondElementPermutations[j] });
        }
    }

    // Increase sample count with random changes
    std::vector<std::pair<FirstElement, SecondElement>, Eigen::aligned_allocator<std::pair<FirstElement, SecondElement>>> pairList;
    pairList.reserve(initialPairList.size() * sampleCount);
    for (size_t j = 0; j < sampleCount; j++) {
        const double x = ((double)j) / ((double)sampleCount);
        for (size_t i = 0; i < initialPairList.size(); i++) {
            // Modify first element
            FirstElement firstElement = initialPairList[i].first;
            firstElement.translate(Eigen::Vector3d(0.0001 * x, -0.0002 * x, 0.0003 * x));
            rotationAxis = Eigen::Vector3d(1.0 + x, 2.0 + x * x, 3.0 + 1.0 / (x + 1.0));
            rotationAxis.normalize();
            rotationAngle = 1e-3 * 2.0 * M_PI * x;
            rotation = Eigen::AngleAxisd(rotationAngle, rotationAxis).toRotationMatrix();
            firstElement.rotate(rotation);

            // Modify second element
            SecondElement secondElement = initialPairList[i].second;
            secondElement.translate(Eigen::Vector3d(0.001 * x, -0.002 * x, 0.003 * x));
            rotationAxis = Eigen::Vector3d(2.0 + x, 1.0 + x * x, 2.0 + 1.0 / (x + 1.0));
            rotationAxis.normalize();
            rotationAngle = -1e-3 * 2.0 * M_PI * x;
            rotation = Eigen::AngleAxisd(rotationAngle, rotationAxis).toRotationMatrix();
            secondElement.rotate(rotation);
            pairList.push_back({ firstElement, secondElement });
        }
    }

    // Loop over all samples
    core::Time startTime = core::Time::currentTime();
    for (size_t i = 0; i < pairList.size(); i++) {
        // Compute distance (non-flipped)
        SSVDistanceEvaluator::evaluate(pairList[i].first, pairList[i].second, distance);
        if (distance.distance() > 1e9)
            throw std::runtime_error("Invalid Distance!");

        // Compute distance (flipped)
        SSVDistanceEvaluator::evaluate(pairList[i].second, pairList[i].first, distance);
        if (distance.distance() > 1e9)
            throw std::runtime_error("Invalid Distance!");
    }
    double totalRunTime = (core::Time::currentTime() - startTime).toDouble();
    return totalRunTime / ((double)pairList.size() * 2);
}

//! Test computation of the distance between two SSVPointElement%s
TEST(SSVDistanceEvaluator, PSS_PSS)
{
    SSVPointElement PSS1(Eigen::Vector3d(10, 10, 0), 1);
    SSVPointElement PSS2(Eigen::Vector3d(0, 0, 0), 1);
    SSVPointElement PSS3(Eigen::Vector3d(10, 10, 10), 1);
    checkEvaluationSSVElements(PSS1, PSS2, sqrt(200.0) - 2.0);
    checkEvaluationSSVElements(PSS1, PSS3, sqrt(100.0) - 2.0);
    checkEvaluationSSVElements(PSS2, PSS3, sqrt(300.0) - 2.0);

    // Benchmark
    double averageRunTime = 0;
    averageRunTime += benchmarkEvaluation(PSS1, PSS2);
    averageRunTime += benchmarkEvaluation(PSS1, PSS3);
    averageRunTime += benchmarkEvaluation(PSS2, PSS3);
    averageRunTime /= 3.0;
    std::cout << "Avg. runtime PSS<->PSS: " << averageRunTime << std::endl;
}

//! Test computation of the distance between a SSVPointElement and SSVLineElement
TEST(SSVDistanceEvaluator, PSS_LSS)
{
    SSVPointElement PSS1(Eigen::Vector3d(20, 10, 0), 1);
    SSVPointElement PSS2(Eigen::Vector3d(-10, 10, 0), 1);
    SSVPointElement PSS3(Eigen::Vector3d(5, 10, 10), 1);
    SSVLineElement LSS(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(10, 0, 0), 1);
    checkEvaluationSSVElements(PSS1, LSS, sqrt(200) - 2); // Point on the right of the line
    checkEvaluationSSVElements(PSS2, LSS, sqrt(200) - 2); // Point on the left of the line
    checkEvaluationSSVElements(PSS3, LSS, sqrt(200) - 2); // Point over the line

    // Benchmark
    double averageRunTime = 0;
    averageRunTime += benchmarkEvaluation(PSS1, LSS);
    averageRunTime += benchmarkEvaluation(PSS2, LSS);
    averageRunTime += benchmarkEvaluation(PSS3, LSS);
    averageRunTime /= 3.0;
    std::cout << "Avg. runtime PSS<->LSS: " << averageRunTime << std::endl;
}

//! Test computation of the distance between a SSVPointElement and a SSVTriangleElement
TEST(SSVDistanceEvaluator, PSS_TSS)
{
    // Acute triangle
    SSVTriangleElement TSS1(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(10, 0, 0), Eigen::Vector3d(10, 10, 0), 1);
    SSVPointElement PSS1(Eigen::Vector3d(5, -10, 0), 1);
    SSVPointElement PSS2(Eigen::Vector3d(20, 5, 0), 1);
    SSVPointElement PSS3(Eigen::Vector3d(20, -10, 0), 1);
    SSVPointElement PSS4(Eigen::Vector3d(7, 3, 10), 1);
    SSVPointElement PSS5(Eigen::Vector3d(0, 10, 0), 1);
    SSVPointElement PSS6(Eigen::Vector3d(-15, -5, 0), 1);
    SSVPointElement PSS7(Eigen::Vector3d(20, 20, 10), 1);
    checkEvaluationSSVElements(PSS1, TSS1, 10 - 2); // Region 2
    checkEvaluationSSVElements(PSS2, TSS1, 10 - 2); // Region 4
    checkEvaluationSSVElements(PSS3, TSS1, sqrt(200) - 2); // Region 3
    checkEvaluationSSVElements(PSS4, TSS1, 10 - 2); // Region 0
    checkEvaluationSSVElements(PSS5, TSS1, sqrt(200) / 2 - 2); // Region 6
    checkEvaluationSSVElements(PSS6, TSS1, sqrt(pow(15, 2) + pow(5, 2)) - 2); // Region 1
    checkEvaluationSSVElements(PSS7, TSS1, sqrt(200 + 100) - 2); // Region 5

    // Obtuse triangle
    SSVTriangleElement TSS2(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(2, 4, 0), Eigen::Vector3d(0, 2, 0), 1);
    // All elements lie in region 5
    SSVPointElement PSS8(Eigen::Vector3d(-10, 3, 0), 1);
    SSVPointElement PSS9(Eigen::Vector3d(-10, 1, 0), 1);
    SSVPointElement PSS10(Eigen::Vector3d(-1, 5, 0), 1);
    checkEvaluationSSVElements(PSS8, TSS2, sqrt(100 + 1) - 2); // Section 1
    checkEvaluationSSVElements(PSS9, TSS2, 10 - 2); // Section 2
    checkEvaluationSSVElements(PSS10, TSS2, sqrt(4 + 4) - 2); // Section 0

    // Benchmark
    double averageRunTime = 0;
    averageRunTime += benchmarkEvaluation(PSS1, TSS1);
    averageRunTime += benchmarkEvaluation(PSS2, TSS1);
    averageRunTime += benchmarkEvaluation(PSS3, TSS1);
    averageRunTime += benchmarkEvaluation(PSS4, TSS1);
    averageRunTime += benchmarkEvaluation(PSS5, TSS1);
    averageRunTime += benchmarkEvaluation(PSS6, TSS1);
    averageRunTime += benchmarkEvaluation(PSS7, TSS1);
    averageRunTime += benchmarkEvaluation(PSS8, TSS2);
    averageRunTime += benchmarkEvaluation(PSS9, TSS2);
    averageRunTime += benchmarkEvaluation(PSS10, TSS2);
    averageRunTime /= 10.0;
    std::cout << "Avg. runtime PSS<->TSS: " << averageRunTime << std::endl;
}

//! Test computation of the distance between two SSVLineElement%s
TEST(SSVDistanceEvaluator, LSS_LSS)
{
    SSVLineElement LSS1(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(10, 0, 0), 1);
    SSVLineElement LSS2(Eigen::Vector3d(5, 10, 0), Eigen::Vector3d(5, 20, 0), 1);
    SSVLineElement LSS3(Eigen::Vector3d(-5, 0, 0), Eigen::Vector3d(0, 5, 0), 1);
    SSVLineElement LSS4(Eigen::Vector3d(0, -5, 0), Eigen::Vector3d(10, 5, 0), 1);
    SSVLineElement LSS5(Eigen::Vector3d(-9, -5, 0), Eigen::Vector3d(0, -5, 0), 1);
    SSVLineElement LSS6(Eigen::Vector3d(0, 5, 0), Eigen::Vector3d(-5, 0, 0), 1);
    SSVLineElement LSS7(Eigen::Vector3d(0, 15, 0), Eigen::Vector3d(10, 5, 0), 1);
    SSVLineElement LSS8(Eigen::Vector3d(15, 5, 0), Eigen::Vector3d(15, -5, 0), 1);
    SSVLineElement LSS9(Eigen::Vector3d(5, 20, 0), Eigen::Vector3d(5, 10, 0), 1);
    SSVLineElement LSS10(Eigen::Vector3d(-10, -5, 0), Eigen::Vector3d(-15, -10, 0), 1);
    checkEvaluationSSVElements(LSS1, LSS2, 10 - 2); // Region 7, s = 0.5, t = -1, perpendicular
    checkEvaluationSSVElements(LSS1, LSS3, sqrt(50) / 2 - 2); // Region 5, s < 0, t = 0.5
    checkEvaluationSSVElements(LSS1, LSS4, -2); // Region 0, s = 0.5  , t = 0.5
    checkEvaluationSSVElements(LSS3, LSS4, sqrt(50) - 2, false); // Parallel, table 6.5, case 3.3
    checkEvaluationSSVElements(LSS3, LSS5, 5 - 2); // Region 6, s < 0, t < 0
    checkEvaluationSSVElements(LSS5, LSS6, 5 - 2); // Region 4, s < 0, t > 1
    checkEvaluationSSVElements(LSS6, LSS5, 5 - 2); // Region 8, s > 1, t < 0
    checkEvaluationSSVElements(LSS7, LSS1, 5 - 2); // Region 2, s > 1, t > 1
    checkEvaluationSSVElements(LSS1, LSS8, 5 - 2); // Region 1, s > 1, t = 0.5, perpendicular
    checkEvaluationSSVElements(LSS1, LSS9, 10 - 2); // Region 3, s = 0.5, t > 1
    checkEvaluationSSVElements(LSS4, LSS3, sqrt(50) - 2, false); // Parallel, table 6.5, case 3.3
    checkEvaluationSSVElements(LSS4, LSS10, 10 - 2, false); // Parallel, table 6.5, case 1.1

    // Benchmark
    double averageRunTime = 0;
    averageRunTime += benchmarkEvaluation(LSS1, LSS2);
    averageRunTime += benchmarkEvaluation(LSS1, LSS3);
    averageRunTime += benchmarkEvaluation(LSS1, LSS4);
    averageRunTime += benchmarkEvaluation(LSS3, LSS4);
    averageRunTime += benchmarkEvaluation(LSS3, LSS5);
    averageRunTime += benchmarkEvaluation(LSS5, LSS6);
    averageRunTime += benchmarkEvaluation(LSS6, LSS5);
    averageRunTime += benchmarkEvaluation(LSS7, LSS1);
    averageRunTime += benchmarkEvaluation(LSS1, LSS8);
    averageRunTime += benchmarkEvaluation(LSS1, LSS9);
    averageRunTime += benchmarkEvaluation(LSS4, LSS3);
    averageRunTime += benchmarkEvaluation(LSS4, LSS10);
    averageRunTime /= 12.0;
    std::cout << "Avg. runtime LSS<->LSS: " << averageRunTime << std::endl;
}

//! Test computation of the distance between a SSVLineElement and a SSVTriangleElement
TEST(SSVDistanceEvaluator, LSS_TSS)
{
    SSVLineElement LSS1(Eigen::Vector3d(0, 10, 0), Eigen::Vector3d(10, 10, 0), 1);
    SSVLineElement LSS2(Eigen::Vector3d(5, 10, 2), Eigen::Vector3d(10, 20, 0), 1);
    SSVLineElement LSS3(Eigen::Vector3d(5, 10, 5), Eigen::Vector3d(5, 0, -5), 1);
    SSVLineElement LSS4(Eigen::Vector3d(3, 0.5, 2), Eigen::Vector3d(2, 0.5, 3), 1);
    SSVLineElement LSS5(Eigen::Vector3d(-10, 0.5, 2), Eigen::Vector3d(20, 0.5, 3), 1);
    SSVLineElement LSS6(Eigen::Vector3d(5, 20, 2), Eigen::Vector3d(4, 10, 6), 1);
    SSVLineElement LSS7(Eigen::Vector3d(0, 10, 10), Eigen::Vector3d(0, -10, 10), 1);
    SSVLineElement LSS8(Eigen::Vector3d(13, 5, 5), Eigen::Vector3d(13, -5, -5), 1);
    SSVLineElement LSS9(Eigen::Vector3d(0, 10, 5), Eigen::Vector3d(0, 0, 15), 1);
    SSVLineElement LSS10(Eigen::Vector3d(0, 25, 0), Eigen::Vector3d(0, 15, 0), 1);
    SSVTriangleElement TSS1(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(10, 0, 0), Eigen::Vector3d(10, 0, 10), 1);
    SSVTriangleElement TSS2(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(10, 0, 0), Eigen::Vector3d(0, 0, 10), 1);
    SSVTriangleElement TSS3(Eigen::Vector3d(-5, 0, 0), Eigen::Vector3d(5, 0, 0), Eigen::Vector3d(0, 10, 0), 0.5);
    checkEvaluationSSVElements(LSS1, TSS1, 10 - 2, false); // Parallel
    checkEvaluationSSVElements(LSS2, TSS1, 10 - 2);
    checkEvaluationSSVElements(LSS3, TSS1, (5 * sqrt(2)) / 2 - 2);
    checkEvaluationSSVElements(LSS4, TSS2, 0.5 - 2, false); // Parallel
    checkEvaluationSSVElements(LSS5, TSS1, 0.5 - 2, false); // Parallel
    checkEvaluationSSVElements(LSS6, TSS2, 10 - 2);
    checkEvaluationSSVElements(LSS6, TSS1, sqrt(100 + 2) - 2);
    checkEvaluationSSVElements(LSS7, TSS1, sqrt(200) / 2 - 2); // Perpendicular
    checkEvaluationSSVElements(LSS7, TSS2, -2); // Perpendicular
    checkEvaluationSSVElements(LSS8, TSS1, 3 - 2);
    checkEvaluationSSVElements(LSS8, TSS2, 3 - 2);
    checkEvaluationSSVElements(LSS9, TSS2, sqrt(12.5) - 2);
    checkEvaluationSSVElements(LSS10, TSS3, 5 - 1.5);

    // Benchmark
    double averageRunTime = 0;
    averageRunTime += benchmarkEvaluation(LSS1, TSS1);
    averageRunTime += benchmarkEvaluation(LSS2, TSS1);
    averageRunTime += benchmarkEvaluation(LSS3, TSS1);
    averageRunTime += benchmarkEvaluation(LSS4, TSS2);
    averageRunTime += benchmarkEvaluation(LSS5, TSS1);
    averageRunTime += benchmarkEvaluation(LSS6, TSS2);
    averageRunTime += benchmarkEvaluation(LSS6, TSS1);
    averageRunTime += benchmarkEvaluation(LSS7, TSS1);
    averageRunTime += benchmarkEvaluation(LSS7, TSS2);
    averageRunTime += benchmarkEvaluation(LSS8, TSS1);
    averageRunTime += benchmarkEvaluation(LSS8, TSS2);
    averageRunTime += benchmarkEvaluation(LSS9, TSS2);
    averageRunTime += benchmarkEvaluation(LSS10, TSS3);
    averageRunTime /= 13.0;
    std::cout << "Avg. runtime LSS<->TSS: " << averageRunTime << std::endl;
}

//! Test computation of the distance between two SSVTriangleElement%s
TEST(SSVDistanceEvaluator, TSS_TSS)
{
    SSVTriangleElement TSS1(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(10, 0, 0), Eigen::Vector3d(10, 0, 10), 1);
    SSVTriangleElement TSS2(Eigen::Vector3d(2, 5, 1), Eigen::Vector3d(10, 15, 0), Eigen::Vector3d(10, 15, 10), 1);
    SSVTriangleElement TSS3(Eigen::Vector3d(-10, 10, -10), Eigen::Vector3d(20, 10, -10), Eigen::Vector3d(20, 10, 20), 1);
    SSVTriangleElement TSS4(Eigen::Vector3d(5, 10, 5), Eigen::Vector3d(5, 0, -5), Eigen::Vector3d(5, 10, -5), 1);
    SSVTriangleElement TSS5(Eigen::Vector3d(-40, 5, -30), Eigen::Vector3d(30, 5, -30), Eigen::Vector3d(30, 5, 40), 1);
    SSVTriangleElement TSS6(Eigen::Vector3d(-18, 3, 20), Eigen::Vector3d(22, 3, -20), Eigen::Vector3d(-18, 3, -20), 1);
    SSVTriangleElement TSS7(Eigen::Vector3d(13, 10, 20), Eigen::Vector3d(13, 10, -20), Eigen::Vector3d(13, -10, -20), 1);
    SSVTriangleElement TSS8(Eigen::Vector3d(-40, 5, -20), Eigen::Vector3d(30, 5, -20), Eigen::Vector3d(30, 5, 50), 1);
    checkEvaluationSSVElements(TSS1, TSS2, 5 - 2);
    checkEvaluationSSVElements(TSS1, TSS3, 10 - 2, false); // parallel
    checkEvaluationSSVElements(TSS1, TSS4, (5 * sqrt(2)) / 2 - 2);
    checkEvaluationSSVElements(TSS1, TSS5, 5 - 2, false); // parallel
    checkEvaluationSSVElements(TSS1, TSS6, 3 - 2, false); // parallel
    checkEvaluationSSVElements(TSS1, TSS7, 3 - 2);
    checkEvaluationSSVElements(TSS1, TSS8, 5 - 2, false); // parallel

    // Benchmark
    double averageRunTime = 0;
    averageRunTime += benchmarkEvaluation(TSS1, TSS2);
    averageRunTime += benchmarkEvaluation(TSS1, TSS3);
    averageRunTime += benchmarkEvaluation(TSS1, TSS4);
    averageRunTime += benchmarkEvaluation(TSS1, TSS5);
    averageRunTime += benchmarkEvaluation(TSS1, TSS6);
    averageRunTime += benchmarkEvaluation(TSS1, TSS7);
    averageRunTime += benchmarkEvaluation(TSS1, TSS8);
    averageRunTime /= 7.0;
    std::cout << "Avg. runtime TSS<->TSS: " << averageRunTime << std::endl;
}

//! Test computation of the distance between two SSVSegment%s
TEST(SSVDistanceEvaluator, SSS_SSS)
{
    // Initialize helpers
    Eigen::Vector3d position = Eigen::Vector3d::Zero();
    Eigen::Matrix3d orientation = Eigen::Matrix3d::Identity();

    // Definition of elements
    SSVPointElement PSS1(Eigen::Vector3d(-5, 0, 0), 1);
    SSVPointElement PSS2(Eigen::Vector3d(5, 0, 0), 1);
    SSVLineElement LSS(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(0, 0, 10), 1);
    SSVTriangleElement TSS(Eigen::Vector3d(-5, 0, 0), Eigen::Vector3d(5, 0, 0), Eigen::Vector3d(0, 10, 0), 0.5);

    // Creating two segments and a distance objects
    SSVSegment SSS1(120201120, "Segment1", 2, 1, 1);
    SSS1.addElement(PSS1);
    SSS1.addElement(PSS2);
    SSS1.addElement(LSS);
    SSS1.addElement(TSS);
    SSS1.update();
    SSVSegment SSS2 = SSS1;
    SSS2.setID(220201120);
    SSS2.setName("Segment2");
    SSS2.addElement(PSS1);
    SSS2.addElement(PSS2);
    SSS2.addElement(LSS);
    SSS2.addElement(TSS);
    SSS2.update();

    // Evaluation 1 (coinciding segments)
    // ------------
    checkEvaluationSSVSegments(SSS1, SSS2, 0 - 2, false);

    // Evaluation 2 (translation in z by -20) (line to line - on same ray)
    // ------------
    position = Eigen::Vector3d(0, 0, -20);
    orientation = Eigen::Matrix3d::Identity();
    SSS2.setPosition(position);
    ASSERT_EQ(SSS2.updateRequired(), true);
    SSS2.update();
    ASSERT_EQ(SSS2.updateRequired(), false);
    SSS2.setOrientation(orientation);
    ASSERT_EQ(SSS2.updateRequired(), true);
    SSS2.update();
    ASSERT_EQ(SSS2.updateRequired(), false);
    checkEvaluationSSVSegments(SSS1, SSS2, 10 - 1 - 1);

    // Evaluation 3 (translation in z by -20 and rotation around y axis by 90deg) (point to line)
    // ------------
    position = Eigen::Vector3d(0, 0, -20);
    orientation = Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d(0, 1, 0)).toRotationMatrix();
    SSS2.setPosition(position);
    SSS2.setOrientation(orientation);
    SSS2.update();
    checkEvaluationSSVSegments(SSS1, SSS2, 15 - 1 - 1);

    // Evaluation 4 (translation in y by 25 and rotation around x axis by 90deg) (triangle to line)
    // ------------
    position = Eigen::Vector3d(0, 25, 0);
    orientation = Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d(1, 0, 0)).toRotationMatrix();
    SSS2.setPosition(position);
    SSS2.setOrientation(orientation);
    SSS2.update();
    checkEvaluationSSVSegments(SSS1, SSS2, 5 - 1 - 0.5);

    // Evaluation 5 (translation in x by 15) (point to point)
    // ------------
    position = Eigen::Vector3d(15, 0, 0);
    orientation = Eigen::Matrix3d::Identity();
    SSS2.setPosition(position);
    SSS2.setOrientation(orientation);
    SSS2.update();
    checkEvaluationSSVSegments(SSS1, SSS2, 5 - 1 - 1);

    // Evaluation 6 (translation in y by 10 and in z by -15) (line to triangle)
    // ------------
    position = Eigen::Vector3d(0, 10, -15);
    orientation = Eigen::Matrix3d::Identity();
    SSS2.setPosition(position);
    SSS2.setOrientation(orientation);
    SSS2.update();
    checkEvaluationSSVSegments(SSS1, SSS2, 5 - 1 - 0.5);

    // Evaluation 7 (translation in z by -20 and rotation around y axis by 180deg) (point to point or line to line)
    // ------------
    position = Eigen::Vector3d(0, 0, -20);
    orientation = Eigen::AngleAxisd(M_PI, Eigen::Vector3d(0, 1, 0)).toRotationMatrix();
    SSS2.setPosition(position);
    SSS2.setOrientation(orientation);
    SSS2.update();
    checkEvaluationSSVSegments(SSS1, SSS2, 20 - 1 - 1, false);

    // Evaluation 8 (translation in z by 100 with maximum bounding box distance set to 50)
    // ------------
    position = Eigen::Vector3d(0, 0, 100);
    orientation = Eigen::Matrix3d::Identity();
    SSS2.setPosition(position);
    SSS2.setOrientation(orientation);
    SSS2.update();
    SSVSegmentDistance unusedDistance;
    ASSERT_EQ(SSVDistanceEvaluator::evaluate(SSS1, SSS2, unusedDistance, 50.0), 0);
}

#endif // HAVE_EIGEN3
