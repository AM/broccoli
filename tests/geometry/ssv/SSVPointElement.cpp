/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVPointElement.hpp"
#include "SSVHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Helper function to compare two SSV point elements
/*! \return `true`, if both elements are equal, `false` otherwise
 */
static inline bool compareSSVElements(const SSVPointElement& firstElement, const SSVPointElement& secondElement)
{
    if (firstElement != secondElement)
        return false;
    if ((firstElement.point0() - secondElement.point0()).norm() > 1e-9)
        return false;
    if (fabs(firstElement.radius() - secondElement.radius()) > 1e-9)
        return false;
    return true;
}

TEST(SSVPointElement, BasicUsage)
{
    // Construction
    // ------------
    // Valid construction
    const Eigen::Vector3d v0 = Eigen::Vector3d(1.23, 2.34, -3.45);
    const double radius = 6.78;
    const SSVPointElement element(v0, radius);
    ASSERT_TRUE(element.isValid());

    // Invalid construction
#ifdef NDEBUG
    // Invalid radius
    SSVPointElement invalidElement(v0, -radius);
    ASSERT_FALSE(invalidElement.isValid());
#endif

    // Getters
    // -------
    ASSERT_EQ(element.point0(), v0);
    ASSERT_EQ(element.radius(), radius);

    // Setters
    // -------
    SSVPointElement testElement = element;
    SSVPointElement expectedElement = element;

    // Test setRadius()
    testElement = element;
    const double newRadius = radius + 1.0;
    testElement.setRadius(newRadius);
    expectedElement = SSVPointElement(v0, newRadius);
    ASSERT_EQ(testElement.radius(), newRadius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (uniform - positive)
    testElement = element;
    const double scalingFactor = 3.2;
    testElement.scale(scalingFactor);
    expectedElement = SSVPointElement(scalingFactor * v0, scalingFactor * radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (uniform - negative)
    testElement = element;
    const double negativeScalingFactor = -2.3;
    testElement.scale(negativeScalingFactor);
    expectedElement = SSVPointElement(negativeScalingFactor * v0, (-negativeScalingFactor) * radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (non-uniform)
    testElement = element;
    const Eigen::Vector3d scalingVector = Eigen::Vector3d(1.2, 2.3, -4.5);
    testElement.scale(scalingVector);
    Eigen::Vector3d v0Scaled;
    expectedElement = SSVPointElement((scalingVector.array() * v0.array()).matrix(), radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test rotate()
    testElement = element;
    Eigen::Vector3d rotationAxis = Eigen::Vector3d(1.0, 2.0, 3.0);
    rotationAxis.normalize();
    const Eigen::Matrix3d rotation = Eigen::AngleAxisd(1.23, rotationAxis).toRotationMatrix();
    testElement.rotate(rotation);
    expectedElement = SSVPointElement(rotation * v0, radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test translate()
    testElement = element;
    Eigen::Vector3d translation = Eigen::Vector3d(-0.123, 0.345, 1.23);
    testElement.translate(translation);
    expectedElement = SSVPointElement(v0 + translation, radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));
}

//! Benchmark of translation and rotation performance
TEST(SSVPointElement, Benchmark)
{
    SSVPointElement element(Eigen::Vector3d(1.23, 2.34, -3.45), 0.5);
    const double translationRunTime = benchmarkSSVElementTranslation(element);
    const double rotationRunTime = benchmarkSSVElementRotation(element);
    if (element.point0().norm() > 1e9) // DUMMY to avoid optimization
        throw std::runtime_error("Invalid translation or rotation!");
    std::cout << "Avg. runtime PSS-translation: " << translationRunTime << std::endl;
    std::cout << "Avg. runtime PSS-rotation: " << rotationRunTime << std::endl;
    std::cout << "Avg. runtime PSS-translation-rotation: " << (translationRunTime + rotationRunTime) << std::endl;
}

//! Test (de-)serialization
TEST(SSVPointElement, Serialization)
{
    const SSVPointElement input(Eigen::Vector3d(1.23, 2.34, -3.45), 6.78);
    SSVPointElement output(Eigen::Vector3d(0.0, 0.0, 0.0), 1.0);
    checkSSVSerialization(input, output);
    ASSERT_TRUE(output == input);
    ASSERT_TRUE(output.point0().isApprox(input.point0()));
    ASSERT_TRUE(output.radius() == input.radius());
}

//! Test triangulation
TEST(SSVPointElement, Triangulation)
{
    const SSVPointElement input(Eigen::Vector3d(1.23, 2.34, -3.45), 6.78);
    checkSSVMesh(input.createMesh(16), "SSVPointElement_createMesh");
#ifdef NDEBUG
    const CGMesh invalidMesh = input.createMesh(1);
    ASSERT_EQ(invalidMesh.m_indexBuffer.cols(), 0);
#endif
}

#endif // HAVE_EIGEN3
