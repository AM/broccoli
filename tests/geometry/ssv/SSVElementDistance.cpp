/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVElementDistance.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Test basic usage of SSVElementDistance
TEST(SSVElementDistance, BasicUsage)
{
    // Initialize helpers
    SSVElementDistance distance;
    const Eigen::Vector3d firstPoint(1.23, 2.34, -4.56);
    const Eigen::Vector3d secondPoint(-2.31, 5.67, 4.32);
    const double firstRadius = 0.123;
    const double secondRadius = 0.234;
    const Eigen::Vector3d expectedConnection = secondPoint - firstPoint;
    const double expectedConnectionLength = expectedConnection.norm();
    const double expectedDistance = expectedConnectionLength - firstRadius - secondRadius;

    // Test non-flipped constructor
    distance = SSVElementDistance(firstPoint, secondPoint, firstRadius, secondRadius, false);
    ASSERT_EQ(distance.firstPoint(), firstPoint);
    ASSERT_EQ(distance.secondPoint(), secondPoint);
    ASSERT_EQ(distance.firstRadius(), firstRadius);
    ASSERT_EQ(distance.secondRadius(), secondRadius);
    ASSERT_LT((distance.connection() - expectedConnection).norm(), 1e-9);
    ASSERT_LT(fabs(distance.connectionLength() - expectedConnectionLength), 1e-9);
    ASSERT_LT(fabs(distance.distance() - expectedDistance), 1e-9);

    // Test flipped constructor
    distance = SSVElementDistance(firstPoint, secondPoint, firstRadius, secondRadius, true);
    ASSERT_EQ(distance.firstPoint(), secondPoint);
    ASSERT_EQ(distance.secondPoint(), firstPoint);
    ASSERT_EQ(distance.firstRadius(), secondRadius);
    ASSERT_EQ(distance.secondRadius(), firstRadius);
    ASSERT_LT((distance.connection() + expectedConnection).norm(), 1e-9);
    ASSERT_LT(fabs(distance.connectionLength() - expectedConnectionLength), 1e-9);
    ASSERT_LT(fabs(distance.distance() - expectedDistance), 1e-9);

    // Test non-flipped setter
    distance = SSVElementDistance();
    distance.set(firstPoint, secondPoint, firstRadius, secondRadius, false);
    ASSERT_EQ(distance.firstPoint(), firstPoint);
    ASSERT_EQ(distance.secondPoint(), secondPoint);
    ASSERT_EQ(distance.firstRadius(), firstRadius);
    ASSERT_EQ(distance.secondRadius(), secondRadius);
    ASSERT_LT((distance.connection() - expectedConnection).norm(), 1e-9);
    ASSERT_LT(fabs(distance.connectionLength() - expectedConnectionLength), 1e-9);
    ASSERT_LT(fabs(distance.distance() - expectedDistance), 1e-9);

    // Test flipped setter
    distance = SSVElementDistance();
    distance.set(firstPoint, secondPoint, firstRadius, secondRadius, true);
    ASSERT_EQ(distance.firstPoint(), secondPoint);
    ASSERT_EQ(distance.secondPoint(), firstPoint);
    ASSERT_EQ(distance.firstRadius(), secondRadius);
    ASSERT_EQ(distance.secondRadius(), firstRadius);
    ASSERT_LT((distance.connection() + expectedConnection).norm(), 1e-9);
    ASSERT_LT(fabs(distance.connectionLength() - expectedConnectionLength), 1e-9);
    ASSERT_LT(fabs(distance.distance() - expectedDistance), 1e-9);

    // Test minimum (first distance is minimum)
    const SSVElementDistance smallDistance(Eigen::Vector3d(-1, 0, 0), Eigen::Vector3d(1, 0, 0), 0.1, 0.2);
    const SSVElementDistance bigDistance(Eigen::Vector3d(-2, 0, 0), Eigen::Vector3d(2, 0, 0), 0.3, 0.4);
    distance = smallDistance;
    distance.minimum(bigDistance);
    ASSERT_EQ(distance.firstPoint(), smallDistance.firstPoint());
    ASSERT_EQ(distance.secondPoint(), smallDistance.secondPoint());
    ASSERT_EQ(distance.firstRadius(), smallDistance.firstRadius());
    ASSERT_EQ(distance.secondRadius(), smallDistance.secondRadius());
    ASSERT_EQ(distance.connection(), smallDistance.connection());
    ASSERT_EQ(distance.connectionLength(), smallDistance.connectionLength());
    ASSERT_EQ(distance.distance(), smallDistance.distance());

    // Test minimum (second distance is minimum)
    distance = bigDistance;
    distance.minimum(smallDistance);
    ASSERT_EQ(distance.firstPoint(), smallDistance.firstPoint());
    ASSERT_EQ(distance.secondPoint(), smallDistance.secondPoint());
    ASSERT_EQ(distance.firstRadius(), smallDistance.firstRadius());
    ASSERT_EQ(distance.secondRadius(), smallDistance.secondRadius());
    ASSERT_EQ(distance.connection(), smallDistance.connection());
    ASSERT_EQ(distance.connectionLength(), smallDistance.connectionLength());
    ASSERT_EQ(distance.distance(), smallDistance.distance());
}

#endif // HAVE_EIGEN3
