/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVScene.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

class MockupSSVScene : public SSVScene {
public:
    inline bool recomputeSegmentsToUpdate() { return SSVScene::recomputeSegmentsToUpdate(); }
    inline bool recomputePairsToEvaluate() { return SSVScene::recomputePairsToEvaluate(); }
};

//! Helper function to create a segment
static inline SSVSegment createSegment(const SSVSegment::ID& id, const std::string& name)
{
    // Definition of elements
    SSVPointElement PSS1(Eigen::Vector3d(-5, 0, 0), 1);
    SSVPointElement PSS2(Eigen::Vector3d(5, 0, 0), 1);
    SSVLineElement LSS(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(0, 0, 10), 1);
    SSVTriangleElement TSS(Eigen::Vector3d(-5, 0, 0), Eigen::Vector3d(5, 0, 0), Eigen::Vector3d(0, 10, 0), 0.5);

    // Definition of segment
    SSVSegment segment(id, name, 2, 1, 1);
    segment.addElement(PSS1);
    segment.addElement(PSS2);
    segment.addElement(LSS);
    segment.addElement(TSS);

    // Pass back created segment
    return segment;
}

//! Helper function to create a test scene
static inline void fillScene(SSVScene& scene)
{
    // Initialize helpers
    Eigen::Vector3d position = Eigen::Vector3d::Zero();
    Eigen::Matrix3d orientation = Eigen::Matrix3d::Identity();

    // Set maximum distance
    scene.setMaximumDistance(50.0);
    ASSERT_EQ(scene.maximumDistance(), 50.0);

    // Clear all and clear pairs
    scene.clear();
    ASSERT_EQ(scene.pairs().size(), 0);
    scene.setPairsAll();
    ASSERT_EQ(scene.pairs().size(), 0);
    ASSERT_TRUE(scene.addSegment(createSegment(147, "asdf")));
    ASSERT_TRUE(scene.addSegment(createSegment(258, "jklo")));
    ASSERT_TRUE(scene.addPair(147, 258));
    ASSERT_EQ(scene.segments().size(), 2);
    ASSERT_EQ(scene.pairs().size(), 1);
    scene.clearPairs();
    ASSERT_EQ(scene.segments().size(), 2);
    ASSERT_EQ(scene.pairs().size(), 0);
    scene.clear();
    ASSERT_EQ(scene.segments().size(), 0);
    ASSERT_EQ(scene.pairs().size(), 0);

    // Create base segment
    const SSVSegment segment1 = createSegment(1, "segment1");
    ASSERT_FALSE(scene.segmentExists(1));
    ASSERT_TRUE(scene.addSegment(segment1));
    ASSERT_FALSE(scene.addSegment(segment1));
    ASSERT_EQ(scene.segments().size(), 1);
    ASSERT_TRUE(scene.segmentExists(1));

    // Create segment 2 [segment1, segment2] -> Expected distance: 0 - 2
    const SSVSegment segment2 = createSegment(2, "segment2");
    ASSERT_FALSE(scene.segmentExists(2));
    ASSERT_TRUE(scene.addSegment(segment2));
    ASSERT_EQ(scene.segments().size(), 2);
    ASSERT_TRUE(scene.segmentExists(2));
    ASSERT_EQ(scene.pairs().size(), 0);
    ASSERT_FALSE(scene.pairExists(1, 2));
    ASSERT_TRUE(scene.addPair(1, 2));
    ASSERT_FALSE(scene.addPair(1, 2));
    ASSERT_FALSE(scene.addPair(2, 1));
#ifdef NDEBUG
    ASSERT_FALSE(scene.addPair(1, 1));
#endif
    ASSERT_EQ(scene.pairs().size(), 1);
    ASSERT_TRUE(scene.pairExists(1, 2));

    // Create segment 3 [segment1, segment3] -> Expected distance: 10 - 1 - 1
    const SSVSegment segment3 = createSegment(3, "segment3");
    ASSERT_FALSE(scene.segmentExists(3));
    scene.addSegment(segment3);
    ASSERT_EQ(scene.segments().size(), 3);
    ASSERT_TRUE(scene.segmentExists(3));
    position = Eigen::Vector3d(0, 0, -20);
    orientation = Eigen::Matrix3d::Identity();
    scene.setSegmentPosition(3, Eigen::Vector3d::Zero());
    scene.setSegmentOrientation(3, orientation);
    scene.translateSegmentAndUpdate(3, position);
    ASSERT_FALSE(scene.pairExists(1, 3));
    scene.addPair(1, 3);
    ASSERT_EQ(scene.pairs().size(), 2);
    ASSERT_TRUE(scene.pairExists(1, 3));

    // Create segment 4 [segment1, segment4] -> Expected distance: 15 - 1 - 1
    const SSVSegment segment4 = createSegment(4, "segment4");
    ASSERT_FALSE(scene.segmentExists(4));
    scene.addSegment(segment4);
    ASSERT_EQ(scene.segments().size(), 4);
    ASSERT_TRUE(scene.segmentExists(4));
    position = Eigen::Vector3d(0, 0, -20);
    orientation = Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d(0, 1, 0)).toRotationMatrix();
    scene.setSegmentPose(4, position, orientation);
    ASSERT_FALSE(scene.pairExists(1, 4));
    scene.addPair(1, 4);
    ASSERT_EQ(scene.pairs().size(), 3);
    ASSERT_TRUE(scene.pairExists(1, 4));

    // Create segment 5 [segment1, segment5] -> Expected distance: 5 - 1 - 0.5
    const SSVSegment segment5 = createSegment(5, "segment5");
    ASSERT_FALSE(scene.segmentExists(5));
    scene.addSegment(segment5);
    ASSERT_EQ(scene.segments().size(), 5);
    ASSERT_TRUE(scene.segmentExists(5));
    position = Eigen::Vector3d(0, 25, 0);
    orientation = Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d(1, 0, 0)).toRotationMatrix();
    scene.setSegmentPose(5, position, orientation);
    ASSERT_FALSE(scene.pairExists(1, 5));
    scene.addPair(1, 5);
    ASSERT_EQ(scene.pairs().size(), 4);
    ASSERT_TRUE(scene.pairExists(1, 5));

    // Create segment 6 [segment1, segment6] -> Expected distance: 5 - 1 - 1
    const SSVSegment segment6 = createSegment(6, "segment6");
    ASSERT_FALSE(scene.segmentExists(6));
    scene.addSegment(segment6);
    ASSERT_EQ(scene.segments().size(), 6);
    ASSERT_TRUE(scene.segmentExists(6));
    position = Eigen::Vector3d(15, 0, 0);
    orientation = Eigen::Matrix3d::Identity();
    scene.setSegmentPose(6, position, orientation);
    ASSERT_FALSE(scene.pairExists(1, 6));
    scene.addPair(1, 6);
    ASSERT_EQ(scene.pairs().size(), 5);
    ASSERT_TRUE(scene.pairExists(1, 6));

    // Create segment 7 [segment1, segment7] -> Expected distance: 5 - 1 - 0.5
    const SSVSegment segment7 = createSegment(7, "segment7");
    ASSERT_FALSE(scene.segmentExists(7));
    scene.addSegment(segment7);
    ASSERT_EQ(scene.segments().size(), 7);
    ASSERT_TRUE(scene.segmentExists(7));
    position = Eigen::Vector3d(0, 10, -15);
    orientation = Eigen::Matrix3d::Identity();
    scene.setSegmentPose(7, position, orientation);
    ASSERT_FALSE(scene.pairExists(1, 7));
    scene.addPair(1, 7);
    ASSERT_EQ(scene.pairs().size(), 6);
    ASSERT_TRUE(scene.pairExists(1, 7));

    // Create segment 8 [segment1, segment8] -> Expected distance: 20 - 1 - 1
    const SSVSegment segment8 = createSegment(8, "segment8");
    ASSERT_FALSE(scene.segmentExists(8));
    scene.addSegment(segment8);
    ASSERT_EQ(scene.segments().size(), 8);
    ASSERT_TRUE(scene.segmentExists(8));
    position = Eigen::Vector3d(0, 0, -20);
    orientation = Eigen::AngleAxisd(M_PI, Eigen::Vector3d(0, 1, 0)).toRotationMatrix();
    scene.setSegmentPose(8, position, orientation);
    ASSERT_FALSE(scene.pairExists(1, 8));
    scene.addPair(1, 8);
    ASSERT_EQ(scene.pairs().size(), 7);
    ASSERT_TRUE(scene.pairExists(1, 8));

    // Create segment 9 [segment1, segment9] -> Expected distance: none because distance between bounding boxes is greater than specified maximum
    const SSVSegment segment9 = createSegment(9, "segment9");
    ASSERT_FALSE(scene.segmentExists(9));
    scene.addSegment(segment9);
    ASSERT_EQ(scene.segments().size(), 9);
    ASSERT_TRUE(scene.segmentExists(9));
    position = Eigen::Vector3d(0, 0, 100);
    orientation = Eigen::Matrix3d::Identity();
    scene.setSegmentPose(9, position, orientation);
    ASSERT_FALSE(scene.pairExists(1, 9));
    scene.addPair(1, 9);
    ASSERT_EQ(scene.pairs().size(), 8);
    ASSERT_TRUE(scene.pairExists(1, 9));

    // Remove segment
    const SSVSegment segmentX = createSegment(99, "segmentX");
    scene.addSegment(segmentX);
    ASSERT_TRUE(scene.segmentExists(99));
    ASSERT_TRUE(scene.removeSegment(99));
    ASSERT_FALSE(scene.removeSegment(99));
    ASSERT_FALSE(scene.segmentExists(99));

    // Remove pair
    scene.addPair(123, 456);
    ASSERT_TRUE(scene.pairExists(123, 456));
    ASSERT_TRUE(scene.pairExists(456, 123));
    ASSERT_TRUE(scene.removePair(456, 123));
    ASSERT_FALSE(scene.removePair(123, 456));
    ASSERT_FALSE(scene.pairExists(123, 456));
    ASSERT_FALSE(scene.pairExists(456, 123));

    // Cleanup (remove unused segment)
    scene.addSegment(segmentX);
    ASSERT_TRUE(scene.segmentExists(99));
    ASSERT_EQ(scene.segments().size(), 10);
    ASSERT_EQ(scene.pairs().size(), 8);
    std::pair<size_t, size_t> cleanUpResult = scene.cleanUp();
    ASSERT_EQ(cleanUpResult.first, 1);
    ASSERT_EQ(cleanUpResult.second, 0);
    ASSERT_FALSE(scene.segmentExists(99));
    ASSERT_EQ(scene.segments().size(), 9);
    ASSERT_EQ(scene.pairs().size(), 8);

    // Cleanup (remove invalid pair)
    scene.addPair(123, 456);
    ASSERT_TRUE(scene.pairExists(123, 456));
    ASSERT_EQ(scene.segments().size(), 9);
    ASSERT_EQ(scene.pairs().size(), 9);
    cleanUpResult = scene.cleanUp();
    ASSERT_EQ(cleanUpResult.first, 0);
    ASSERT_EQ(cleanUpResult.second, 1);
    ASSERT_FALSE(scene.pairExists(123, 456));
    ASSERT_EQ(scene.segments().size(), 9);
    ASSERT_EQ(scene.pairs().size(), 8);

    // Cleanup (nothing to do)
    cleanUpResult = scene.cleanUp();
    ASSERT_EQ(cleanUpResult.first, 0);
    ASSERT_EQ(cleanUpResult.second, 0);
    ASSERT_EQ(scene.segments().size(), 9);
    ASSERT_EQ(scene.pairs().size(), 8);

    // Check, access to non-existing segment
#ifdef NDEBUG
    SSVSegment nonExistingSegment;
    EXPECT_THROW((nonExistingSegment = scene.segment(666)), std::runtime_error);
#endif

    // Check validity (valid)
    ASSERT_TRUE(scene.isValid());

    // Check validity (invalid segment)
#ifdef NDEBUG
    SSVSegment invalidSegment(999);
    ASSERT_TRUE(scene.addSegment(invalidSegment));
    ASSERT_FALSE(scene.isValid());
    ASSERT_TRUE(scene.removeSegment(999));
    ASSERT_TRUE(scene.isValid());
#endif

    // Check validity (invalid pair)
#ifdef NDEBUG
    ASSERT_TRUE(scene.addPair(999, 1000));
    ASSERT_FALSE(scene.isValid());
    ASSERT_TRUE(scene.removePair(999, 1000));
    ASSERT_TRUE(scene.isValid());
#endif

    // Check segment ID list
    const auto segmentIDs = scene.segmentIDList(true);
    ASSERT_EQ(segmentIDs.size(), 9);
    ASSERT_EQ(segmentIDs[0], 1);
    ASSERT_EQ(segmentIDs[1], 2);
    ASSERT_EQ(segmentIDs[2], 3);
    ASSERT_EQ(segmentIDs[3], 4);
    ASSERT_EQ(segmentIDs[4], 5);
    ASSERT_EQ(segmentIDs[5], 6);
    ASSERT_EQ(segmentIDs[6], 7);
    ASSERT_EQ(segmentIDs[7], 8);
    ASSERT_EQ(segmentIDs[8], 9);
}

//! Test of construction
TEST(SSVScene, Construction)
{
    // Construction (default)
    SSVScene defaultScene;
    ASSERT_EQ(defaultScene.name(), "SSVScene");
    ASSERT_EQ(defaultScene.threadCountPreProcessing(), 0);
    ASSERT_EQ(defaultScene.threadCountProcessing(), 0);
    ASSERT_EQ(defaultScene.threadPriorityPreProcessing(), 0);
    ASSERT_EQ(defaultScene.threadPriorityProcessing(), 0);

    // Construction (with name)
    SSVScene simpleScene("asdf");
    ASSERT_EQ(simpleScene.name(), "asdf");

    // Construction (full specification)
    SSVScene scene("test", 2, 1);
    ASSERT_EQ(scene.name(), "test");
    ASSERT_EQ(scene.threadCountPreProcessing(), 2);
    ASSERT_EQ(scene.threadCountProcessing(), 2);
    ASSERT_EQ(scene.threadPriorityPreProcessing(), 1);
    ASSERT_EQ(scene.threadPriorityProcessing(), 1);

    // Copy construction
    SSVScene copyConstructedScene(scene);
    ASSERT_EQ(copyConstructedScene.name(), "test");
    ASSERT_EQ(copyConstructedScene.threadCountPreProcessing(), 2);
    ASSERT_EQ(copyConstructedScene.threadCountProcessing(), 2);
    ASSERT_EQ(copyConstructedScene.threadPriorityPreProcessing(), 1);
    ASSERT_EQ(copyConstructedScene.threadPriorityProcessing(), 1);

    // Copy assignment
    SSVScene copyAssignedScene;
    copyAssignedScene = scene;
    ASSERT_EQ(copyAssignedScene.name(), "test");
    ASSERT_EQ(copyAssignedScene.threadCountPreProcessing(), 2);
    ASSERT_EQ(copyAssignedScene.threadCountProcessing(), 2);
    ASSERT_EQ(copyAssignedScene.threadPriorityPreProcessing(), 1);
    ASSERT_EQ(copyAssignedScene.threadPriorityProcessing(), 1);
}

//! Test setters and getters
TEST(SSVScene, SettersAndGetters)
{
    SSVScene scene("asdf");

    // Test name
    ASSERT_EQ(scene.name(), "asdf");
    scene.setName("test");
    scene.setName("test");
    ASSERT_EQ(scene.name(), "test");

    // Test maximum distance
    scene.evaluate();
    ASSERT_EQ(scene.maximumDistance(), -1.0);
    scene.setMaximumDistance(-2.0);
    ASSERT_EQ(scene.maximumDistance(), -2.0);
    ASSERT_EQ(scene.distancesValid(), true);
    scene.evaluate();
    scene.setMaximumDistance(2.0);
    ASSERT_EQ(scene.maximumDistance(), 2.0);
    ASSERT_EQ(scene.distancesValid(), false);
    scene.evaluate();
    scene.setMaximumDistance(-3.0);
    ASSERT_EQ(scene.maximumDistance(), -3.0);
    ASSERT_EQ(scene.distancesValid(), false);

    // Test thread count
    ASSERT_EQ(scene.threadCountPreProcessing(), 0);
    scene.setThreadCountPreProcessing(1);
    scene.setThreadCountPreProcessing(1);
    ASSERT_EQ(scene.threadCountPreProcessing(), 1);
    ASSERT_EQ(scene.threadCountProcessing(), 0);
    scene.setThreadCountProcessing(2);
    scene.setThreadCountProcessing(2);
    ASSERT_EQ(scene.threadCountProcessing(), 2);

    // Test thread priority
    ASSERT_EQ(scene.threadPriorityPreProcessing(), 0);
    scene.setThreadPriorityPreProcessing(3);
    scene.setThreadPriorityPreProcessing(3);
    ASSERT_EQ(scene.threadPriorityPreProcessing(), 3);
    ASSERT_EQ(scene.threadPriorityProcessing(), 0);
    scene.setThreadPriorityProcessing(4);
    scene.setThreadPriorityProcessing(4);
    ASSERT_EQ(scene.threadPriorityProcessing(), 4);
}

//! Checks evaluation in single-threaded or multi-threaded mode
static void checkEvaluation(const size_t& threadCount)
{
    // Create scene
    MockupSSVScene scene;
    fillScene(scene);

    // Check distances (zero)
#ifdef NDEBUG
    ASSERT_EQ(scene.distances().size(), 0);
    double invalidMinimumDistance = 0;
    EXPECT_THROW(invalidMinimumDistance = scene.minimumDistance().distance(), std::out_of_range);
#endif

    // Set thread count
    scene.setThreadCountPreProcessing(threadCount);
    scene.setThreadCountProcessing(threadCount);

    // Trigger evaluation
    ASSERT_FALSE(scene.distancesValid());
    ASSERT_TRUE(scene.evaluate());
    ASSERT_TRUE(scene.distancesValid());
    ASSERT_FALSE(scene.evaluate()); // Second call -> no update
    ASSERT_TRUE(scene.distancesValid());

    // Re-compute intermediate data
    ASSERT_FALSE(scene.recomputeSegmentsToUpdate());
    ASSERT_FALSE(scene.recomputePairsToEvaluate());

    // Check distances
    ASSERT_EQ(scene.distances().size(), 7);
    std::vector<bool> pairsFound;
    pairsFound.resize(7, false);
    for (size_t i = 0; i < scene.distances().size(); i++) {
        const auto& currentDistance = scene.distances()[i];
        ASSERT_EQ(currentDistance.firstID(), 1);
        if (currentDistance.secondID() == 2) {
            ASSERT_EQ(currentDistance.distance(), 0 - 2);
            pairsFound[0] = true;
        } else if (currentDistance.secondID() == 3) {
            ASSERT_EQ(currentDistance.distance(), 10 - 1 - 1);
            pairsFound[1] = true;
        } else if (currentDistance.secondID() == 4) {
            ASSERT_EQ(currentDistance.distance(), 15 - 1 - 1);
            pairsFound[2] = true;
        } else if (currentDistance.secondID() == 5) {
            ASSERT_EQ(currentDistance.distance(), 5 - 1 - 0.5);
            pairsFound[3] = true;
        } else if (currentDistance.secondID() == 6) {
            ASSERT_EQ(currentDistance.distance(), 5 - 1 - 1);
            pairsFound[4] = true;
        } else if (currentDistance.secondID() == 7) {
            ASSERT_EQ(currentDistance.distance(), 5 - 1 - 0.5);
            pairsFound[5] = true;
        } else if (currentDistance.secondID() == 8) {
            ASSERT_EQ(currentDistance.distance(), 20 - 1 - 1);
            pairsFound[6] = true;
        } else
            ASSERT_TRUE(false);
    }
    for (size_t i = 0; i < pairsFound.size(); i++)
        ASSERT_TRUE(pairsFound[i]);
    ASSERT_EQ(scene.minimumDistance().distance(), -2);
    ASSERT_EQ(scene.distances()[scene.minimumDistanceIndex()].distance(), -2);

    // Print statistics
    std::cout << "Evaluated element pairs: " << scene.evaluatedElementPairs() << std::endl;
    std::cout << "Runtime reInitializeWorkersPreProcess(): " << scene.runTimeReInitializeWorkersPreProcess().encodeToDurationString() << std::endl;
    std::cout << "Runtime reInitializeWorkersProcess(): " << scene.runTimeReInitializeWorkersProcess().encodeToDurationString() << std::endl;
    std::cout << "Runtime recomputeSegmentsToUpdate(): " << scene.runTimeRecomputeSegmentsToUpdate().encodeToDurationString() << std::endl;
    std::cout << "Runtime recomputePairsToEvaluate(): " << scene.runTimeRecomputePairsToEvaluate().encodeToDurationString() << std::endl;
    std::cout << "Runtime pre-processing: " << scene.runTimePreProcessing().encodeToDurationString() << std::endl;
    std::cout << "Runtime processing: " << scene.runTimeProcessing().encodeToDurationString() << std::endl;
    std::cout << "Runtime: " << scene.runTime().encodeToDurationString() << std::endl;
}

//! Test single-threaded evaluation
TEST(SSVScene, EvaluationSingleThreaded)
{
    checkEvaluation(0);
}

//! Test multi-threaded evaluation
TEST(SSVScene, EvaluationMultiThreaded)
{
    checkEvaluation(2);
}

//! Helper function to create a large test scene
static inline SSVScene createLargeScene(const size_t& segmentCount)
{
    // Initialize helpers
    Eigen::Vector3d position = Eigen::Vector3d::Zero();
    Eigen::Vector3d rotationAxis = Eigen::Vector3d(1.0, 0.0, 0.0);
    double rotationAngle = 0.0;
    Eigen::Matrix3d orientation = Eigen::Matrix3d::Identity();

    // Setup scene
    SSVScene scene("LargeScene");

    // Add segments
    for (size_t i = 0; i < segmentCount; i++) {
        const double x = ((double)i) / ((double)segmentCount);

        // Vary position and orientation
        position = Eigen::Vector3d(x, x * x, 1.0 / (x + 1.0));
        if (i % 2 == 0)
            position(2) += 100.0; // Put every second segment far away
        rotationAxis = Eigen::Vector3d(1.0 + x, 2.0 + x * x, 3.0 + 1.0 / (x + 1.0));
        rotationAxis.normalize();
        rotationAngle = 2.0 * M_PI * x;
        orientation = Eigen::AngleAxisd(rotationAngle, rotationAxis).toRotationMatrix();

        // Add segment to scene
        SSVSegment newSegment = createSegment(i, "segment_" + std::to_string(i));
        newSegment.setPosition(position);
        newSegment.setOrientation(orientation);
        scene.addSegment(newSegment);
    }

    // Auto-set pairs
    scene.setPairsAll();

    // Pass back scene
    return scene;
}

//! Test accelerated evaluation
TEST(SSVScene, EvaluationAccelerated)
{
    // Initialize helpers
    const size_t segmentCount = 100;
    const size_t threadCount = 4;

    // Create test scenes
    SSVScene baseScene = createLargeScene(segmentCount);
    std::pair<size_t, size_t> cleanUpResult = baseScene.cleanUp();
    ASSERT_EQ(cleanUpResult.first, 0);
    ASSERT_EQ(cleanUpResult.second, 0);

    // Test without bounding boxes - single threaded
    SSVScene scene_WOBB_ST = baseScene;
    scene_WOBB_ST.setName("WOBB_ST");
    scene_WOBB_ST.setMaximumDistance(-1.0);
    ASSERT_TRUE(scene_WOBB_ST.evaluate());
    std::cout << "Runtime WOBB_ST: " << scene_WOBB_ST.runTime().encodeToDurationString() << " (evaluated element pairs: " << scene_WOBB_ST.evaluatedElementPairs() << ")" << std::endl;

    // Test with bounding boxes - single threaded
    SSVScene scene_WBB_ST = baseScene;
    scene_WBB_ST.setName("WBB_ST");
    scene_WBB_ST.setMaximumDistance(50.0);
    ASSERT_TRUE(scene_WBB_ST.evaluate());
    std::cout << "Runtime WBB_ST: " << scene_WBB_ST.runTime().encodeToDurationString() << " (evaluated element pairs: " << scene_WBB_ST.evaluatedElementPairs() << ")" << std::endl;

    // Test without bounding boxes - multi threaded
    SSVScene scene_WOBB_MT = baseScene;
    scene_WOBB_MT.setName("WOBB_MT");
    scene_WOBB_MT.setMaximumDistance(-1.0);
    scene_WOBB_MT.setThreadCountPreProcessing(threadCount);
    scene_WOBB_MT.setThreadCountProcessing(threadCount);
    ASSERT_TRUE(scene_WOBB_MT.evaluate());
    ASSERT_EQ(scene_WOBB_MT.evaluatedElementPairs(), scene_WOBB_ST.evaluatedElementPairs());
    std::cout << "Runtime WOBB_MT (" << threadCount << " cores): " << scene_WOBB_MT.runTime().encodeToDurationString() << " (evaluated element pairs: " << scene_WOBB_MT.evaluatedElementPairs() << ")" << std::endl;

    // Test with bounding boxes - multi threaded
    SSVScene scene_WBB_MT = baseScene;
    scene_WBB_MT.setName("WBB_MT");
    scene_WBB_MT.setMaximumDistance(50.0);
    scene_WBB_MT.setThreadCountPreProcessing(threadCount);
    scene_WBB_MT.setThreadCountProcessing(threadCount);
    ASSERT_TRUE(scene_WBB_MT.evaluate());
    ASSERT_EQ(scene_WBB_MT.evaluatedElementPairs(), scene_WBB_ST.evaluatedElementPairs());
    std::cout << "Runtime WBB_MT (" << threadCount << " cores): " << scene_WBB_MT.runTime().encodeToDurationString() << " (evaluated element pairs: " << scene_WBB_MT.evaluatedElementPairs() << ")" << std::endl;
}

//! Test auto-tuning of thread-counts
TEST(SSVScene, AutoTune)
{
    // Initialize helpers
    const size_t segmentCount = 100;
    const size_t maximumThreadCount = 2;

    // Create test scene
    SSVScene scene = createLargeScene(segmentCount);

    // Run auto-tune
    const core::Time startTime = core::Time::currentTime();
    scene.autoTune(0, maximumThreadCount, 0, maximumThreadCount);
    const core::Time endTime = core::Time::currentTime();
    ASSERT_GE(scene.threadCountPreProcessing(), 0);
    ASSERT_LE(scene.threadCountPreProcessing(), maximumThreadCount);
    ASSERT_GE(scene.threadCountProcessing(), 0);
    ASSERT_LE(scene.threadCountProcessing(), maximumThreadCount);

    // Output
    std::cout << "Optimal thread count for pre-processing: " << scene.threadCountPreProcessing() << std::endl;
    std::cout << "Optimal thread count for processing: " << scene.threadCountProcessing() << std::endl;
    std::cout << "Runtime of autoTune(): " << (endTime - startTime).encodeToDurationString() << std::endl;
}

#endif // HAVE_EIGEN3
