/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVLineElement.hpp"
#include "SSVHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Helper function to compare two SSV line elements
/*! \return `true`, if both elements are equal, `false` otherwise
 */
static inline bool compareSSVElements(const SSVLineElement& firstElement, const SSVLineElement& secondElement)
{
    if (firstElement != secondElement)
        return false;
    if ((firstElement.point0() - secondElement.point0()).norm() > 1e-9 || (firstElement.point1() - secondElement.point1()).norm() > 1e-9)
        return false;
    if ((firstElement.edge0() - secondElement.edge0()).norm() > 1e-9)
        return false;
    if (fabs(firstElement.radius() - secondElement.radius()) > 1e-9)
        return false;
    return true;
}

TEST(SSVLineElement, BasicUsage)
{
    // Construction
    // ------------
    // Valid construction
    const Eigen::Vector3d v0 = Eigen::Vector3d(1.23, 2.34, -3.45);
    const Eigen::Vector3d v1 = Eigen::Vector3d(5.78, -4.63, 3.21);
    const Eigen::Vector3d e0 = v1 - v0;
    const double radius = 6.78;
    const SSVLineElement element(v0, v1, radius);
    ASSERT_TRUE(element.isValid());

    // Invalid construction
#ifdef NDEBUG
    // Invalid radius
    SSVLineElement invalidElement(v0, v1, -radius);
    ASSERT_FALSE(invalidElement.isValid());

    // Points coincide
    SSVLineElement invalidElement2(v0, v0, radius);
    ASSERT_FALSE(invalidElement2.isValid());
#endif

    // Getters
    // -------
    ASSERT_EQ(element.point0(), v0);
    ASSERT_EQ(element.point1(), v1);
    ASSERT_EQ(element.edge0(), e0);
    ASSERT_EQ(element.radius(), radius);

    // Setters
    // -------
    SSVLineElement testElement = element;
    SSVLineElement expectedElement = element;

    // Test setRadius()
    testElement = element;
    const double newRadius = radius + 1.0;
    testElement.setRadius(newRadius);
    expectedElement = SSVLineElement(v0, v1, newRadius);
    ASSERT_EQ(testElement.radius(), newRadius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (uniform - positive)
    testElement = element;
    const double scalingFactor = 3.2;
    testElement.scale(scalingFactor);
    expectedElement = SSVLineElement(scalingFactor * v0, scalingFactor * v1, scalingFactor * radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (uniform - negative)
    testElement = element;
    const double negativeScalingFactor = -2.3;
    testElement.scale(negativeScalingFactor);
    expectedElement = SSVLineElement(negativeScalingFactor * v0, negativeScalingFactor * v1, (-negativeScalingFactor) * radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (non-uniform)
    testElement = element;
    const Eigen::Vector3d scalingVector = Eigen::Vector3d(1.2, 2.3, -4.5);
    testElement.scale(scalingVector);
    Eigen::Vector3d v0Scaled;
    expectedElement = SSVLineElement((scalingVector.array() * v0.array()).matrix(), (scalingVector.array() * v1.array()).matrix(), radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test rotate()
    testElement = element;
    Eigen::Vector3d rotationAxis = Eigen::Vector3d(1.0, 2.0, 3.0);
    rotationAxis.normalize();
    const Eigen::Matrix3d rotation = Eigen::AngleAxisd(1.23, rotationAxis).toRotationMatrix();
    testElement.rotate(rotation);
    expectedElement = SSVLineElement(rotation * v0, rotation * v1, radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test translate()
    testElement = element;
    Eigen::Vector3d translation = Eigen::Vector3d(-0.123, 0.345, 1.23);
    testElement.translate(translation);
    expectedElement = SSVLineElement(v0 + translation, v1 + translation, radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));
}

//! Benchmark of translation and rotation performance
TEST(SSVLineElement, Benchmark)
{
    SSVLineElement element(Eigen::Vector3d(1.23, 2.34, -3.45), Eigen::Vector3d(5.78, -4.63, 3.21), 0.5);
    const double translationRunTime = benchmarkSSVElementTranslation(element);
    const double rotationRunTime = benchmarkSSVElementRotation(element);
    if (element.point0().norm() > 1e9 || element.point1().norm() > 1e9 || element.edge0().norm() > 1e9) // DUMMY to avoid optimization
        throw std::runtime_error("Invalid translation or rotation!");
    std::cout << "Avg. runtime LSS-translation: " << translationRunTime << std::endl;
    std::cout << "Avg. runtime LSS-rotation: " << rotationRunTime << std::endl;
    std::cout << "Avg. runtime LSS-translation-rotation: " << (translationRunTime + rotationRunTime) << std::endl;
}

//! Test (de-)serialization
TEST(SSVLineElement, Serialization)
{
    const SSVLineElement input(Eigen::Vector3d(1.23, 2.34, -3.45), Eigen::Vector3d(5.78, -4.63, 3.21), 0.5);
    SSVLineElement output(Eigen::Vector3d(0.0, 0.0, 0.0), Eigen::Vector3d(1.0, 0.0, 0.0), 1.0);
    checkSSVSerialization(input, output);
    ASSERT_TRUE(output == input);
    ASSERT_TRUE(output.point0().isApprox(input.point0()));
    ASSERT_TRUE(output.point1().isApprox(input.point1()));
    ASSERT_TRUE(output.edge0().isApprox(input.edge0()));
    ASSERT_TRUE(output.radius() == input.radius());
}

//! Test triangulation
TEST(SSVLineElement, Triangulation)
{
    const SSVLineElement input(Eigen::Vector3d(1.23, 2.34, -3.45), Eigen::Vector3d(5.78, -4.63, 3.21), 0.5);
    checkSSVMesh(input.createMesh(16), "SSVLineElement_createMesh");
#ifdef NDEBUG
    const CGMesh invalidMesh = input.createMesh(1);
    ASSERT_EQ(invalidMesh.m_indexBuffer.cols(), 0);
#endif
}

#endif // HAVE_EIGEN3
