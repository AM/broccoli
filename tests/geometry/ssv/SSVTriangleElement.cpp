/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVTriangleElement.hpp"
#include "SSVHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Helper function to compare two SSV triangle elements
/*! \return `true`, if both elements are equal, `false` otherwise
 */
static inline bool compareSSVElements(const SSVTriangleElement& firstElement, const SSVTriangleElement& secondElement)
{
    if (firstElement != secondElement)
        return false;
    if ((firstElement.point0() - secondElement.point0()).norm() > 1e-9 || (firstElement.point1() - secondElement.point1()).norm() > 1e-9 || (firstElement.point2() - secondElement.point2()).norm() > 1e-9)
        return false;
    if ((firstElement.edge0() - secondElement.edge0()).norm() > 1e-9 || (firstElement.edge1() - secondElement.edge1()).norm() > 1e-9 || (firstElement.edge2() - secondElement.edge2()).norm() > 1e-9)
        return false;
    if (fabs(firstElement.radius() - secondElement.radius()) > 1e-9)
        return false;
    if ((firstElement.normal() - secondElement.normal()).norm() > 1e-9)
        return false;
    if ((firstElement.matrixS() - secondElement.matrixS()).norm() > 1e-9)
        return false;
    return true;
}

TEST(SSVTriangleElement, BasicUsage)
{
    // Construction
    // ------------
    // Valid construction
    const Eigen::Vector3d v0 = Eigen::Vector3d(1.23, 2.34, -3.45);
    const Eigen::Vector3d v1 = Eigen::Vector3d(5.78, -4.63, 3.21);
    const Eigen::Vector3d v2 = Eigen::Vector3d(-7.89, 3.57, 8.24);
    const Eigen::Vector3d e0 = v1 - v0;
    const Eigen::Vector3d e1 = v2 - v0;
    const Eigen::Vector3d e2 = v2 - v1;
    const double radius = 6.78;
    const SSVTriangleElement element(v0, v1, v2, radius);
    ASSERT_TRUE(element.isValid());

    // Invalid construction
#ifdef NDEBUG
    // Invalid radius
    SSVTriangleElement invalidElement(v0, v1, v2, -radius);
    ASSERT_FALSE(invalidElement.isValid());

    // Points coincide
    SSVTriangleElement invalidElement2(v0, v0, v2, radius);
    ASSERT_FALSE(invalidElement2.isValid());
    SSVTriangleElement invalidElement3(v0, v1, v1, radius);
    ASSERT_FALSE(invalidElement3.isValid());
    SSVTriangleElement invalidElement4(v0, v1, v0, radius);
    ASSERT_FALSE(invalidElement4.isValid());

    // All points on one line
    SSVTriangleElement invalidElement5(v0, 0.5 * (v0 + v1), v1, radius);
    ASSERT_FALSE(invalidElement5.isValid());
#endif

    // Getters
    // -------
    ASSERT_EQ(element.point0(), v0);
    ASSERT_EQ(element.point1(), v1);
    ASSERT_EQ(element.point2(), v2);
    ASSERT_EQ(element.edge0(), e0);
    ASSERT_EQ(element.edge1(), e1);
    ASSERT_EQ(element.edge2(), e2);
    ASSERT_EQ(element.radius(), radius);
    const Eigen::Vector3d normal = (v1 - v0).cross(v2 - v0).normalized();
    ASSERT_EQ(element.normal(), normal);
    Eigen::Matrix<double, 3, 2> D;
    D.col(0) = e0;
    D.col(1) = e1;
    const Eigen::Matrix<double, 2, 3> Dtranspose = D.transpose();
    const Eigen::Matrix<double, 2, 3> matrixS = (Dtranspose * D).inverse() * Dtranspose;
    ASSERT_LT((element.matrixS() - matrixS).norm(), 1e-9);

    // Setters
    // -------
    SSVTriangleElement testElement = element;
    SSVTriangleElement expectedElement = element;

    // Test setRadius()
    testElement = element;
    const double newRadius = radius + 1.0;
    testElement.setRadius(newRadius);
    expectedElement = SSVTriangleElement(v0, v1, v2, newRadius);
    ASSERT_EQ(testElement.radius(), newRadius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (uniform - positive)
    testElement = element;
    const double scalingFactor = 3.2;
    testElement.scale(scalingFactor);
    expectedElement = SSVTriangleElement(scalingFactor * v0, scalingFactor * v1, scalingFactor * v2, scalingFactor * radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (uniform - negative)
    testElement = element;
    const double negativeScalingFactor = -2.3;
    testElement.scale(negativeScalingFactor);
    expectedElement = SSVTriangleElement(negativeScalingFactor * v0, negativeScalingFactor * v1, negativeScalingFactor * v2, (-negativeScalingFactor) * radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test scale() (non-uniform)
    testElement = element;
    const Eigen::Vector3d scalingVector = Eigen::Vector3d(1.2, 2.3, -4.5);
    testElement.scale(scalingVector);
    Eigen::Vector3d v0Scaled;
    expectedElement = SSVTriangleElement((scalingVector.array() * v0.array()).matrix(), (scalingVector.array() * v1.array()).matrix(), (scalingVector.array() * v2.array()).matrix(), radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test rotate()
    testElement = element;
    Eigen::Vector3d rotationAxis = Eigen::Vector3d(1.0, 2.0, 3.0);
    rotationAxis.normalize();
    const Eigen::Matrix3d rotation = Eigen::AngleAxisd(1.23, rotationAxis).toRotationMatrix();
    testElement.rotate(rotation);
    expectedElement = SSVTriangleElement(rotation * v0, rotation * v1, rotation * v2, radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));

    // Test translate()
    testElement = element;
    Eigen::Vector3d translation = Eigen::Vector3d(-0.123, 0.345, 1.23);
    testElement.translate(translation);
    expectedElement = SSVTriangleElement(v0 + translation, v1 + translation, v2 + translation, radius);
    ASSERT_FALSE(compareSSVElements(testElement, element));
    ASSERT_TRUE(compareSSVElements(testElement, expectedElement));
}

//! Benchmark of translation and rotation performance
TEST(SSVTriangleElement, Benchmark)
{
    SSVTriangleElement element(Eigen::Vector3d(1.23, 2.34, -3.45), Eigen::Vector3d(5.78, -4.63, 3.21), Eigen::Vector3d(-7.89, 3.57, 8.24), 0.5);
    const double translationRunTime = benchmarkSSVElementTranslation(element);
    const double rotationRunTime = benchmarkSSVElementRotation(element);
    if (element.point0().norm() > 1e9 || element.point1().norm() > 1e9 || element.point2().norm() > 1e9 || element.edge0().norm() > 1e9 || element.edge1().norm() > 1e9 || element.edge2().norm() > 1e9 || element.normal().norm() > 1e9 || element.matrixS().norm() > 1e9) // DUMMY to avoid optimization
        throw std::runtime_error("Invalid translation or rotation!");
    std::cout << "Avg. runtime TSS-translation: " << translationRunTime << std::endl;
    std::cout << "Avg. runtime TSS-rotation: " << rotationRunTime << std::endl;
    std::cout << "Avg. runtime TSS-translation-rotation: " << (translationRunTime + rotationRunTime) << std::endl;
}

//! Test (de-)serialization
TEST(SSVTriangleElement, Serialization)
{
    const SSVTriangleElement input(Eigen::Vector3d(1.23, 2.34, -3.45), Eigen::Vector3d(5.78, -4.63, 3.21), Eigen::Vector3d(-7.89, 3.57, 8.24), 0.5);
    SSVTriangleElement output(Eigen::Vector3d(0.0, 0.0, 0.0), Eigen::Vector3d(1.0, 0.0, 0.0), Eigen::Vector3d(0.0, 1.0, 0.0), 1.0);
    checkSSVSerialization(input, output);
    ASSERT_TRUE(output == input);
    ASSERT_TRUE(output.point0().isApprox(input.point0()));
    ASSERT_TRUE(output.point1().isApprox(input.point1()));
    ASSERT_TRUE(output.point2().isApprox(input.point2()));
    ASSERT_TRUE(output.edge0().isApprox(input.edge0()));
    ASSERT_TRUE(output.edge1().isApprox(input.edge1()));
    ASSERT_TRUE(output.edge2().isApprox(input.edge2()));
    ASSERT_TRUE(output.radius() == input.radius());
    ASSERT_TRUE(output.matrixS().isApprox(input.matrixS()));
    ASSERT_TRUE(output.normal().isApprox(input.normal()));
}

//! Test triangulation
TEST(SSVTriangleElement, Triangulation)
{
    const SSVTriangleElement input(Eigen::Vector3d(1.23, 2.34, -3.45), Eigen::Vector3d(5.78, -4.63, 3.21), Eigen::Vector3d(-7.89, 3.57, 8.24), 0.5);
    checkSSVMesh(input.createMesh(16), "SSVTriangleElement_createMesh");
#ifdef NDEBUG
    const CGMesh invalidMesh = input.createMesh(1);
    ASSERT_EQ(invalidMesh.m_indexBuffer.cols(), 0);
#endif
}

#endif // HAVE_EIGEN3
