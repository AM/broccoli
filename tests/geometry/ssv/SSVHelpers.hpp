/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/core/Time.hpp"
#include "broccoli/geometry/CGMeshTools.hpp"
#include "broccoli/io/ply/PLYFileConverter.hpp"
#include "broccoli/io/serialization/serialization.hpp"
#include "gtest/gtest.h"
#include <Eigen/Dense>
#include <vector>

// Option to enable mesh output
#define WRITE_MESH

using namespace broccoli;

//! Helper function to benchmark the translation performance of a SSV element
template <typename ElementType>
static inline double benchmarkSSVElementTranslation(ElementType& element)
{
    // Initialize helpers
    const size_t sampleCount = 10000;

    // Pre-compute translation vectors
    std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d>> translations;
    translations.reserve(sampleCount);
    for (size_t i = 0; i < sampleCount; i++) {
        const double x = ((double)i) / ((double)sampleCount);
        translations.push_back(Eigen::Vector3d(0.0001 * x, -0.0002 * x, 0.0003 * x));
    }

    // Perform benchmark
    const core::Time startTime = core::Time::currentTime();
    for (size_t i = 0; i < sampleCount; i++)
        element.translate(translations[i]);
    const core::Time endTime = core::Time::currentTime();

    // DUMMY to avoid optimization
    if (element.point0().norm() > 1e9)
        throw std::runtime_error("Invalid translation!");

    // Compute average runtime
    return (endTime - startTime).toDouble() / ((double)sampleCount);
}

//! Helper function to benchmark the rotation performance of a SSV element
template <typename ElementType>
static inline double benchmarkSSVElementRotation(ElementType& element)
{
    // Initialize helpers
    const size_t sampleCount = 10000;
    Eigen::Vector3d rotationAxis = Eigen::Vector3d(1.0, 0.0, 0.0);
    double rotationAngle = 0.0;

    // Pre-compute rotation matrices
    std::vector<Eigen::Matrix3d, Eigen::aligned_allocator<Eigen::Matrix3d>> rotations;
    rotations.reserve(sampleCount);
    for (size_t i = 0; i < sampleCount; i++) {
        const double x = ((double)i) / ((double)sampleCount);
        rotationAxis = Eigen::Vector3d(1.0 + x, 2.0 + x * x, 3.0 + 1.0 / (x + 1.0));
        rotationAxis.normalize();
        rotationAngle = 1e-3 * 2.0 * M_PI * x;
        rotations.push_back(Eigen::AngleAxisd(rotationAngle, rotationAxis).toRotationMatrix());
    }

    // Perform benchmark
    const core::Time startTime = core::Time::currentTime();
    for (size_t i = 0; i < sampleCount; i++)
        element.rotate(rotations[i]);
    const core::Time endTime = core::Time::currentTime();

    // DUMMY to avoid optimization
    if (element.point0().norm() > 1e9)
        throw std::runtime_error("Invalid rotation!");

    // Compute average runtime
    return (endTime - startTime).toDouble() / ((double)sampleCount);
}

//! Helper function to test serialization and deserialization of SSV data
template <typename T>
static inline void checkSSVSerialization(const T& input, T& output)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        io::serialization::Endianness endianness = io::serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = io::serialization::Endianness::BIG;

        // Test serialization
        io::serialization::BinaryStream stream;
        const io::serialization::BinaryStreamSize returnValueSerialization = input.serialize(stream, endianness);
        ASSERT_EQ(stream.size(), returnValueSerialization);

        // Test deserialization
        const io::serialization::BinaryStreamSize returnValueDeSerialization = output.deSerialize(stream, 0, endianness);
        ASSERT_EQ(stream.size(), returnValueDeSerialization);
    }
}

//! Helper function to check SSV meshes
static inline void checkSSVMesh(const geometry::CGMesh& mesh, const std::string& filePath = "")
{
    // Check validity
    geometry::CGMeshResult result = geometry::CGMeshResult::UNKNOWN;
    if (mesh.isValid(&result) == false) {
        std::cout << "Invalid mesh '" << filePath << "': " << geometry::CGMeshResultString(result) << std::endl;
        ASSERT_TRUE(mesh.isValid());
    }

    // Check triangle count (not-empty)
    ASSERT_GT(mesh.m_indexBuffer.cols(), 0);

    // Check, if there is unused data
    geometry::CGMesh copy = mesh;
    const auto unusedDataCount = geometry::CGMeshTools::cleanup(copy);
    ASSERT_EQ(unusedDataCount[0], 0);
    ASSERT_EQ(unusedDataCount[1], 0);

    // Write mesh
    if (filePath.size() > 0) {
#ifdef WRITE_MESH
        io::PLYFile plyFile;
        ASSERT_TRUE(io::PLYFileConverter::convert(mesh, plyFile));
        ASSERT_TRUE(plyFile.writeFile(filePath + ".ply", broccoli::io::PLYFormat::Type::BINARY_LITTLE_ENDIAN));
#endif
    }
}

#endif // HAVE_EIGEN3
