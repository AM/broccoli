/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVSegmentDistance.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Test basic usage of SSVSegmentDistance
TEST(SSVSegmentDistance, BasicUsage)
{
    // Initialize helpers
    SSVSegment::ID firstID = 123;
    SSVSegment::ID secondID = 456;
    SSVElementDistance elementDistance(Eigen::Vector3d(1.23, -2.34, 3.45), Eigen::Vector3d(-4.56, 5.67, -6.78), 0.4, 0.6, false);

    // Check specialized constructor
    SSVSegmentDistance segmentDistance(elementDistance, firstID, secondID);
    ASSERT_EQ(segmentDistance.firstID(), firstID);
    ASSERT_EQ(segmentDistance.secondID(), secondID);
    ASSERT_EQ(segmentDistance.distance(), elementDistance.distance());

    // Check setters and getters
    firstID += 5;
    secondID -= 6;
    ASSERT_NE(segmentDistance.firstID(), firstID);
    ASSERT_NE(segmentDistance.secondID(), secondID);
    segmentDistance.setFirstID(firstID);
    ASSERT_EQ(segmentDistance.firstID(), firstID);
    segmentDistance.setSecondID(secondID);
    ASSERT_EQ(segmentDistance.secondID(), secondID);

    // Test copy assignment operator
    segmentDistance = SSVSegmentDistance();
    ASSERT_NE(segmentDistance.distance(), elementDistance.distance());
    segmentDistance = elementDistance;
    ASSERT_EQ(segmentDistance.distance(), elementDistance.distance());

    // Test minimum (first distance is minimum)
    const SSVElementDistance smallElementDistance(Eigen::Vector3d(-1, 0, 0), Eigen::Vector3d(1, 0, 0), 0.1, 0.2);
    const SSVSegmentDistance smallDistance(smallElementDistance, 1, 2);
    const SSVElementDistance bigElementDistance(Eigen::Vector3d(-2, 0, 0), Eigen::Vector3d(2, 0, 0), 0.3, 0.4);
    const SSVSegmentDistance bigDistance(bigElementDistance, 3, 4);
    segmentDistance = smallDistance;
    segmentDistance.minimum(bigDistance);
    ASSERT_EQ(segmentDistance.firstPoint(), smallDistance.firstPoint());
    ASSERT_EQ(segmentDistance.secondPoint(), smallDistance.secondPoint());
    ASSERT_EQ(segmentDistance.firstRadius(), smallDistance.firstRadius());
    ASSERT_EQ(segmentDistance.secondRadius(), smallDistance.secondRadius());
    ASSERT_EQ(segmentDistance.connection(), smallDistance.connection());
    ASSERT_EQ(segmentDistance.connectionLength(), smallDistance.connectionLength());
    ASSERT_EQ(segmentDistance.distance(), smallDistance.distance());
    ASSERT_EQ(segmentDistance.firstID(), smallDistance.firstID());
    ASSERT_EQ(segmentDistance.secondID(), smallDistance.secondID());

    // Test minimum (second distance is minimum)
    segmentDistance = bigDistance;
    segmentDistance.minimum(smallDistance);
    ASSERT_EQ(segmentDistance.firstPoint(), smallDistance.firstPoint());
    ASSERT_EQ(segmentDistance.secondPoint(), smallDistance.secondPoint());
    ASSERT_EQ(segmentDistance.firstRadius(), smallDistance.firstRadius());
    ASSERT_EQ(segmentDistance.secondRadius(), smallDistance.secondRadius());
    ASSERT_EQ(segmentDistance.connection(), smallDistance.connection());
    ASSERT_EQ(segmentDistance.connectionLength(), smallDistance.connectionLength());
    ASSERT_EQ(segmentDistance.distance(), smallDistance.distance());
    ASSERT_EQ(segmentDistance.firstID(), smallDistance.firstID());
    ASSERT_EQ(segmentDistance.secondID(), smallDistance.secondID());
}

#endif // HAVE_EIGEN3
