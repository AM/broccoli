/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// Note: Original implementation by Poscher, Reinhold, "Effiziente Abstandsberechnungen mit Swept-Sphere Volumen für Echtzeit Kollisionsvermeidung in der Robotik", Technical University of Munich, 2020, Bachelor's thesis, https://mediatum.ub.tum.de/1580089

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/ssv/SSVSegment.hpp"
#include "SSVHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

// Helper function to test empty ssv segments
void testEmptySSVSegment(const SSVSegment& segment)
{
    ASSERT_EQ(segment.position(), Eigen::Vector3d(0, 0, 0));
    ASSERT_EQ(segment.orientation(), Eigen::Matrix3d::Identity());
    ASSERT_EQ(segment.updateRequired(), false);
    ASSERT_EQ(segment.localPointElements().size(), 0);
    ASSERT_EQ(segment.localLineElements().size(), 0);
    ASSERT_EQ(segment.localTriangleElements().size(), 0);
    ASSERT_EQ(segment.globalPointElements().size(), 0);
    ASSERT_EQ(segment.globalLineElements().size(), 0);
    ASSERT_EQ(segment.globalTriangleElements().size(), 0);
    ASSERT_EQ(segment.isValid(), false);
}

// Helper function to test equality of the given segments
bool checkEquality(const SSVSegment& first, const SSVSegment& second)
{
    static constexpr double tolerance = 1e-9;
    if (first.id() != second.id() || //
        first.name() != second.name() || //
        !first.position().isApprox(second.position(), tolerance) || //
        !first.orientation().isApprox(second.orientation(), tolerance) || //
        first.updateRequired() != second.updateRequired() || //
        first.localPointElements() != second.localPointElements() || //
        first.localLineElements() != second.localLineElements() || //
        first.localTriangleElements() != second.localTriangleElements() || //
        first.globalPointElements() != second.globalPointElements() || //
        first.globalLineElements() != second.globalLineElements() || //
        first.globalTriangleElements() != second.globalTriangleElements() || //
        !first.globalBoundingBoxMinimum().isApprox(second.globalBoundingBoxMinimum(), tolerance) || //
        !first.globalBoundingBoxMaximum().isApprox(second.globalBoundingBoxMaximum(), tolerance))
        return false;
    else
        return true;
}

//! Tests basic usage of SSVSegment
TEST(SSVSegment, BasicUsage)
{
    // Initialize helpers
    const double scalingFactor = 1.23;
    const Eigen::Vector3d scalingVector = Eigen::Vector3d(1.23, 2.34, 3.45);
    const Eigen::Vector3d rotationAxis = Eigen::Vector3d(1.0, 2.0, 3.0).normalized();
    const Eigen::Matrix3d rotation = Eigen::AngleAxisd(1.23, rotationAxis).toRotationMatrix();
    const Eigen::Vector3d translation = Eigen::Vector3d(4.56, -7.89, 8.9);
    const SSVPointElement pointElement(Eigen::Vector3d(1.23, -3.45, 5.64), 2.34);
    const SSVLineElement lineElement(Eigen::Vector3d(1.23, -3.45, 5.64), Eigen::Vector3d(-2.34, 45.6, 7.64), 3.45);
    const SSVTriangleElement triangleElement(Eigen::Vector3d(1.23, -3.45, 5.64), Eigen::Vector3d(-2.34, 45.6, 7.64), Eigen::Vector3d(6.54, 4.23, -7.84), 4.54);

    // Construction
    // ------------
    // Default constructor
    const SSVSegment defaultSegment;
    ASSERT_EQ(defaultSegment.id(), 0);
    ASSERT_EQ(defaultSegment.name(), "");
    testEmptySSVSegment(defaultSegment);

    // Empty construction
    const SSVSegment emptySegment(123, "asdf");
    ASSERT_EQ(emptySegment.id(), 123);
    ASSERT_EQ(emptySegment.name(), "asdf");
    testEmptySSVSegment(emptySegment);

    // Pre-allocated construction
    SSVSegment segment(123, "asdf", 1, 1, 1);
    ASSERT_EQ(segment.id(), 123);
    ASSERT_EQ(segment.name(), "asdf");
    testEmptySSVSegment(segment);

    // Setters
    // -------
    // Setting ID
    ASSERT_EQ(segment.id(), 123);
    segment.setID(456);
    ASSERT_EQ(segment.id(), 456);

    // Setting name
    ASSERT_EQ(segment.name(), "asdf");
    segment.setName("test");
    ASSERT_EQ(segment.name(), "test");

    // Setting pose
    ASSERT_EQ(segment.updateRequired(), false);
    ASSERT_EQ(segment.position(), Eigen::Vector3d(0, 0, 0));
    segment.setPosition(translation);
    ASSERT_EQ(segment.position(), translation);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);
    ASSERT_EQ(segment.orientation(), Eigen::Matrix3d::Identity());
    segment.setOrientation(rotation);
    ASSERT_EQ(segment.orientation(), rotation);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);
    ASSERT_EQ(segment.update(), false);

    // Element handling
    // ----------------
    // Adding point element
    ASSERT_EQ(segment.localPointElements().size(), 0);
    ASSERT_EQ(segment.globalPointElements().size(), 0);
    ASSERT_EQ(segment.updateRequired(), false);
    segment.addElement(pointElement);
    ASSERT_EQ(segment.localPointElements().size(), 1);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.localPointElements().size(), 1);
    ASSERT_EQ(segment.globalPointElements().size(), 1);

    // Adding line element
    ASSERT_EQ(segment.localLineElements().size(), 0);
    ASSERT_EQ(segment.globalLineElements().size(), 0);
    ASSERT_EQ(segment.updateRequired(), false);
    segment.addElement(lineElement);
    ASSERT_EQ(segment.localLineElements().size(), 1);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);
    ASSERT_EQ(segment.localLineElements().size(), 1);
    ASSERT_EQ(segment.globalLineElements().size(), 1);

    // Adding triangle element
    ASSERT_EQ(segment.localTriangleElements().size(), 0);
    ASSERT_EQ(segment.globalTriangleElements().size(), 0);
    ASSERT_EQ(segment.updateRequired(), false);
    segment.addElement(triangleElement);
    ASSERT_EQ(segment.localTriangleElements().size(), 1);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);
    ASSERT_EQ(segment.localTriangleElements().size(), 1);
    ASSERT_EQ(segment.globalTriangleElements().size(), 1);

    // Initialize helpers
    SSVPointElement transformedPointElement = pointElement;
    SSVLineElement transformedLineElement = lineElement;
    SSVTriangleElement transformedTriangleElement = triangleElement;

    // Scale (uniform)
    transformedPointElement.scale(scalingFactor);
    transformedLineElement.scale(scalingFactor);
    transformedTriangleElement.scale(scalingFactor);
    segment.scaleElements(scalingFactor);
    ASSERT_EQ(segment.localPointElements()[0], transformedPointElement);
    ASSERT_EQ(segment.localLineElements()[0], transformedLineElement);
    ASSERT_EQ(segment.localTriangleElements()[0], transformedTriangleElement);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);

    // Scale (non-uniform)
    transformedPointElement.scale(scalingVector);
    transformedLineElement.scale(scalingVector);
    transformedTriangleElement.scale(scalingVector);
    segment.scaleElements(scalingVector);
    ASSERT_EQ(segment.localPointElements()[0], transformedPointElement);
    ASSERT_EQ(segment.localLineElements()[0], transformedLineElement);
    ASSERT_EQ(segment.localTriangleElements()[0], transformedTriangleElement);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);

    // Rotate
    transformedPointElement.rotate(rotation);
    transformedLineElement.rotate(rotation);
    transformedTriangleElement.rotate(rotation);
    segment.rotateElements(rotation);
    ASSERT_EQ(segment.localPointElements()[0], transformedPointElement);
    ASSERT_EQ(segment.localLineElements()[0], transformedLineElement);
    ASSERT_EQ(segment.localTriangleElements()[0], transformedTriangleElement);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);

    // Translate
    transformedPointElement.translate(translation);
    transformedLineElement.translate(translation);
    transformedTriangleElement.translate(translation);
    segment.translateElements(translation);
    ASSERT_EQ(segment.localPointElements()[0], transformedPointElement);
    ASSERT_EQ(segment.localLineElements()[0], transformedLineElement);
    ASSERT_EQ(segment.localTriangleElements()[0], transformedTriangleElement);
    ASSERT_EQ(segment.updateRequired(), true);
    ASSERT_EQ(segment.update(), true);
    ASSERT_EQ(segment.updateRequired(), false);

    // Validity checking
    // -----------------
    ASSERT_EQ(segment.isValid(), true);
    segment.clearElements();
    ASSERT_EQ(segment.isValid(), false);

    // Invalid
#ifdef NDEBUG
    const SSVPointElement invalidPointElement(Eigen::Vector3d(1.23, -3.45, 5.64), -2.34);
    const SSVLineElement invalidLineElement(Eigen::Vector3d(1.23, -3.45, 5.64), Eigen::Vector3d(-2.34, 45.6, 7.64), -3.45);
    const SSVTriangleElement invalidTriangleElement(Eigen::Vector3d(1.23, -3.45, 5.64), Eigen::Vector3d(-2.34, 45.6, 7.64), Eigen::Vector3d(6.54, 4.23, -7.84), -4.54);
    segment.clearElements();
    segment.addElement(invalidPointElement);
    ASSERT_FALSE(segment.isValid());
    segment.clearElements();
    segment.addElement(invalidLineElement);
    ASSERT_FALSE(segment.isValid());
    segment.clearElements();
    segment.addElement(invalidTriangleElement);
    ASSERT_FALSE(segment.isValid());
#endif
}

//! Tests computation of bounding boxes
TEST(SSVSegment, BoundingBoxes)
{
    // Initialize helpers
    const Eigen::Vector3d translation = Eigen::Vector3d(1.0, 2.0, 3.0);

    // Create segment
    const SSVPointElement pointElement(Eigen::Vector3d(1.0, 0.0, 0.0), 0.1);
    const SSVLineElement lineElement(Eigen::Vector3d(0.0, -1.0, 0.0), Eigen::Vector3d(0.0, 1.0, 0.0), 0.2);
    const SSVTriangleElement triangleElement(Eigen::Vector3d(0.5, -0.5, 0.5), Eigen::Vector3d(0.5, 0.5, 0.5), Eigen::Vector3d(0.5, 0.0, 1.0), 0.3);
    SSVSegment segment(123, "BBTest", 1, 1, 1);
    segment.addElement(pointElement);
    segment.addElement(lineElement);
    segment.addElement(triangleElement);
    segment.setPosition(translation);
    ASSERT_TRUE(segment.isValid());

    // Update segment
    ASSERT_TRUE(segment.update());

    // Check bounding boxes
    const Eigen::Vector3d expectedBoundingBoxMinimum = Eigen::Vector3d(-0.2, -1.2, -0.2) + translation;
    const Eigen::Vector3d expectedBoundingBoxMaximum = Eigen::Vector3d(1.1, 1.2, 1.3) + translation;
    ASSERT_EQ(segment.globalBoundingBoxMinimum(), expectedBoundingBoxMinimum);
    ASSERT_EQ(segment.globalBoundingBoxMaximum(), expectedBoundingBoxMaximum);
}

//! Tests translateSegmentAndUpdate()
TEST(SSVSegment, TranslateSegmentAndUpdate)
{
    // Initialize helpers
    const Eigen::Vector3d position = Eigen::Vector3d(0.1, 0.2, -0.3);
    const Eigen::Vector3d translation = Eigen::Vector3d(1.0, 2.0, 3.0);
    const Eigen::Matrix3d orientation = Eigen::Matrix3d(Eigen::AngleAxisd(0.123, Eigen::Vector3d(-0.1, 0.2, -0.3)));

    // Create segment
    const SSVPointElement pointElement(Eigen::Vector3d(1.23, -3.45, 5.64), 2.34);
    const SSVLineElement lineElement(Eigen::Vector3d(1.23, -3.45, 5.64), Eigen::Vector3d(-2.34, 45.6, 7.64), 3.45);
    const SSVTriangleElement triangleElement(Eigen::Vector3d(1.23, -3.45, 5.64), Eigen::Vector3d(-2.34, 45.6, 7.64), Eigen::Vector3d(6.54, 4.23, -7.84), 4.54);
    SSVSegment segment(123, "Test", 1, 1, 1);
    segment.addElement(pointElement);
    segment.addElement(lineElement);
    segment.addElement(triangleElement);
    segment.setPosition(position);
    segment.setOrientation(orientation);
    segment.update();

    // "Conservative" translation
    SSVSegment conservativeTranslation = segment;
    ASSERT_FALSE(conservativeTranslation.updateRequired());
    conservativeTranslation.setPosition(position + translation);
    ASSERT_TRUE(conservativeTranslation.updateRequired());
    conservativeTranslation.update();
    ASSERT_FALSE(conservativeTranslation.updateRequired());

    // "Efficient" translation
    SSVSegment efficientTranslation = segment;
    ASSERT_FALSE(efficientTranslation.updateRequired());
    efficientTranslation.translateSegmentAndUpdate(translation);
    ASSERT_FALSE(conservativeTranslation.updateRequired());

    // Compare segments
    ASSERT_TRUE(checkEquality(conservativeTranslation, efficientTranslation));
}

//! Test (de-)serialization
TEST(SSVSegment, Serialization)
{
    const Eigen::Vector3d translation = Eigen::Vector3d(1.0, 2.0, 3.0);
    const Eigen::Quaterniond rotation = Eigen::Quaterniond(Eigen::AngleAxisd(0.123, Eigen::Vector3d(1.0, 2.0, 3.0).normalized()));
    const SSVPointElement pointElement(Eigen::Vector3d(1.0, 0.0, 0.0), 0.1);
    const SSVLineElement lineElement(Eigen::Vector3d(0.0, -1.0, 0.0), Eigen::Vector3d(0.0, 1.0, 0.0), 0.2);
    const SSVTriangleElement triangleElement(Eigen::Vector3d(0.5, -0.5, 0.5), Eigen::Vector3d(0.5, 0.5, 0.5), Eigen::Vector3d(0.5, 0.0, 1.0), 0.3);
    SSVSegment input(123, "serializationTest", 1, 1, 1);
    input.addElement(pointElement);
    input.addElement(lineElement);
    input.addElement(triangleElement);
    input.setPosition(translation);
    input.setOrientation(rotation.toRotationMatrix());
    input.update();
    SSVSegment output;
    checkSSVSerialization(input, output);
    ASSERT_TRUE(output.id() == input.id());
    ASSERT_TRUE(output.name() == input.name());
    ASSERT_TRUE(output.position().isApprox(input.position()));
    ASSERT_TRUE(output.orientation().isApprox(input.orientation()));
    ASSERT_TRUE(output.updateRequired() == true);
    ASSERT_TRUE(output.localPointElements() == input.localPointElements());
    ASSERT_TRUE(output.localLineElements() == input.localLineElements());
    ASSERT_TRUE(output.localTriangleElements() == input.localTriangleElements());
    output.update();
    ASSERT_TRUE(output.globalPointElements() == input.globalPointElements());
    ASSERT_TRUE(output.globalLineElements() == input.globalLineElements());
    ASSERT_TRUE(output.globalTriangleElements() == input.globalTriangleElements());
    ASSERT_TRUE(output.globalBoundingBoxMinimum().isApprox(input.globalBoundingBoxMinimum()));
    ASSERT_TRUE(output.globalBoundingBoxMaximum().isApprox(input.globalBoundingBoxMaximum()));
}

//! Test triangulation
TEST(SSVSegment, Triangulation)
{
    const Eigen::Vector3d translation = Eigen::Vector3d(1.0, 2.0, 3.0);
    const Eigen::Quaterniond rotation = Eigen::Quaterniond(Eigen::AngleAxisd(0.123, Eigen::Vector3d(1.0, 2.0, 3.0).normalized()));
    const SSVPointElement pointElement(Eigen::Vector3d(1.0, 0.0, 0.0), 0.1);
    const SSVLineElement lineElement(Eigen::Vector3d(0.0, -1.0, 0.0), Eigen::Vector3d(0.0, 1.0, 0.0), 0.2);
    const SSVTriangleElement triangleElement(Eigen::Vector3d(0.5, -0.5, 0.5), Eigen::Vector3d(0.5, 0.5, 0.5), Eigen::Vector3d(0.5, 0.0, 1.0), 0.3);

    // Check empty segment
    SSVSegment segmentEmpty(123, "Empty", 0, 0, 0);
    const CGMesh emptyMesh = segmentEmpty.createMesh(16);
    ASSERT_EQ(emptyMesh.m_indexBuffer.cols(), 0);

    // Check point-only segment
    SSVSegment segmentPointOnly(123, "PointOnly", 1, 0, 0);
    segmentPointOnly.addElement(pointElement);
    checkSSVMesh(segmentPointOnly.createMesh(16), "SSVSegment_createMesh_PointOnly");

    // Check line-only segment
    SSVSegment segmentLineOnly(123, "LineOnly", 0, 1, 0);
    segmentLineOnly.addElement(lineElement);
    checkSSVMesh(segmentLineOnly.createMesh(16), "SSVSegment_createMesh_LineOnly");

    // Check triangle-only segment
    SSVSegment segmentTriangleOnly(123, "TriangleOnly", 0, 0, 1);
    segmentTriangleOnly.addElement(triangleElement);
    checkSSVMesh(segmentTriangleOnly.createMesh(16), "SSVSegment_createMesh_TriangleOnly");

    // Check full (local) segment
    SSVSegment segmentFullLocal(123, "FullLocal", 1, 1, 1);
    segmentFullLocal.addElement(pointElement);
    segmentFullLocal.addElement(lineElement);
    segmentFullLocal.addElement(triangleElement);
    checkSSVMesh(segmentFullLocal.createMesh(16), "SSVSegment_createMesh_FullLocal");

    // Check full (global) segment
    SSVSegment segmentFullGlobal(123, "FullGlobal", 1, 1, 1);
    segmentFullGlobal.addElement(pointElement);
    segmentFullGlobal.addElement(lineElement);
    segmentFullGlobal.addElement(triangleElement);
    segmentFullGlobal.setPosition(translation);
    segmentFullGlobal.setOrientation(rotation.toRotationMatrix());
#ifdef NDEBUG
    const CGMesh invalidMeshNoUpdate = segmentFullGlobal.createMesh(16, true);
    ASSERT_EQ(invalidMeshNoUpdate.m_indexBuffer.cols(), 0);
#endif
    segmentFullGlobal.update();
    checkSSVMesh(segmentFullGlobal.createMesh(16, true), "SSVSegment_createMesh_FullGlobal");
#ifdef NDEBUG
    const CGMesh invalidMeshSteps = segmentFullGlobal.createMesh(1);
    ASSERT_EQ(invalidMeshSteps.m_indexBuffer.cols(), 0);
#endif
}

#endif // HAVE_EIGEN3
