/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/Triangle2D.hpp"
#include "broccoli/geometry/Triangle3D.hpp"
#include "gtest/gtest.h"

// Full comparison of all members
template <unsigned int N>
static inline bool fullComparisonBase(const broccoli::geometry::TriangleND<N>& first, const broccoli::geometry::TriangleND<N>& second, const double& tolerance = 1e-9)
{
    return first.v0().isApprox(second.v0(), tolerance) && first.v1().isApprox(second.v1(), tolerance) && first.v2().isApprox(second.v2(), tolerance) && //
        first.e0().isApprox(second.e0(), tolerance) && first.e1().isApprox(second.e1(), tolerance) && first.e2().isApprox(second.e2(), tolerance) && //
        fabs(first.e0_dot_e0() - second.e0_dot_e0()) < tolerance && fabs(first.e1_dot_e1() - second.e1_dot_e1()) < tolerance && fabs(first.e2_dot_e2() - second.e2_dot_e2()) < tolerance && //
        fabs(first.e0_dot_e1() - second.e0_dot_e1()) < tolerance && fabs(first.e0_dot_e2() - second.e0_dot_e2()) < tolerance && fabs(first.e1_dot_e2() - second.e1_dot_e2()) < tolerance && //
        fabs(first.area() - second.area()) < tolerance && //
        first.degenerated() == second.degenerated() && //
        first.n().isApprox(second.n(), tolerance) && //
        first.boundingBoxMinimum().isApprox(second.boundingBoxMinimum(), tolerance) && //
        first.boundingBoxMaximum().isApprox(second.boundingBoxMaximum(), tolerance);
}

// Full comparison of all members
static inline bool fullComparison(const broccoli::geometry::Triangle2D& first, const broccoli::geometry::Triangle2D& second, const double& tolerance = 1e-9)
{
    return fullComparisonBase(first, second, tolerance) && first.invD().isApprox(second.invD(), tolerance);
}

// Full comparison of all members
static inline bool fullComparison(const broccoli::geometry::Triangle3D& first, const broccoli::geometry::Triangle3D& second, const double& tolerance = 1e-9)
{
    return fullComparisonBase(first, second, tolerance) && first.invD().isApprox(second.invD(), tolerance);
}

#endif // HAVE_EIGEN3
