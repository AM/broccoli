/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/SphericalCoordinates.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Computes the "wrapped" difference between two angles
static inline double angleDifference(const double& first, const double& second)
{
    double difference = fmod(fabs(first - second), 2.0 * M_PI);
    if (difference > M_PI)
        difference -= M_PI;
    return difference;
}

//! Test construction and basic usage of DH parameters
TEST(SphericalCoordinates, ConstructionBasicUsage)
{
    // Initialize helpers
    const double radiusMinimum = -2.0;
    const double radiusMaximum = 2.0;
    const size_t radiusStepCount = 10;
    const double radiusStepSize = (radiusMaximum - radiusMinimum) / (double)radiusStepCount;
    const double phiMinimum = -3.0 * M_PI;
    const double phiMaximum = 3.0 * M_PI;
    const size_t phiStepCount = 30;
    const double phiStepSize = (phiMaximum - phiMinimum) / (double)phiStepCount;
    const double thetaMinimum = -3.0 * M_PI;
    const double thetaMaximum = 3.0 * M_PI;
    const size_t thetaStepCount = 30;
    const double thetaStepSize = (thetaMaximum - thetaMinimum) / (double)thetaStepCount;

    // Default construction
    SphericalCoordinates coordinatesDefault;
    ASSERT_EQ(coordinatesDefault.radius(), 0.0);
    ASSERT_EQ(coordinatesDefault.phi(), 0.0);
    ASSERT_EQ(coordinatesDefault.cosPhi(), cos(0.0));
    ASSERT_EQ(coordinatesDefault.sinPhi(), sin(0.0));
    ASSERT_EQ(coordinatesDefault.theta(), 0.0);
    ASSERT_EQ(coordinatesDefault.cosTheta(), cos(0.0));
    ASSERT_EQ(coordinatesDefault.sinTheta(), sin(0.0));

    // Special construction and setters
    for (size_t i = 0; i <= radiusStepCount; i++) {
        const double radius = radiusMinimum + i * radiusStepSize;
        for (size_t j = 0; j <= phiStepCount; j++) {
            const double phi = phiMinimum + j * phiStepSize;
            for (size_t k = 0; k <= thetaStepCount; k++) {
                const double theta = thetaMinimum + k * thetaStepSize;

                // Manually compute normalized version
                double radiusNormalized = radius;
                double phiNormalized = phi;
                double thetaNormalized = theta;
                if (broccoli::core::isZero(radiusNormalized)) {
                    radiusNormalized = 0.0;
                    phiNormalized = 0.0;
                    thetaNormalized = 0.0;
                } else {
                    if (radiusNormalized < 0.0) {
                        radiusNormalized = -radiusNormalized;
                        thetaNormalized += M_PI;
                    }
                    while (thetaNormalized >= 2.0 * M_PI)
                        thetaNormalized -= 2.0 * M_PI;
                    while (thetaNormalized < 0.0)
                        thetaNormalized += 2.0 * M_PI;
                    if (thetaNormalized > M_PI) {
                        thetaNormalized = 2.0 * M_PI - thetaNormalized;
                        phiNormalized += M_PI;
                    }
                    if (broccoli::core::isZero(thetaNormalized) || broccoli::core::isZero(thetaNormalized - M_PI))
                        phiNormalized = 0.0;
                    else {
                        while (phiNormalized >= 2.0 * M_PI)
                            phiNormalized -= 2.0 * M_PI;
                        while (phiNormalized < 0.0)
                            phiNormalized += 2.0 * M_PI;
                    }
                }
                ASSERT_GE(radiusNormalized, 0.0);
                ASSERT_GE(phiNormalized, 0.0);
                ASSERT_LT(phiNormalized, 2.0 * M_PI);
                ASSERT_GE(thetaNormalized, 0.0);
                ASSERT_LE(thetaNormalized, M_PI);

                // Constructor
                const SphericalCoordinates coordinatesConstructor(radius, phi, theta);
                ASSERT_FLOAT_EQ(coordinatesConstructor.radius(), radius);
                ASSERT_FLOAT_EQ(coordinatesConstructor.phi(), phi);
                ASSERT_FLOAT_EQ(coordinatesConstructor.cosPhi(), cos(phi));
                ASSERT_FLOAT_EQ(coordinatesConstructor.sinPhi(), sin(phi));
                ASSERT_FLOAT_EQ(coordinatesConstructor.theta(), theta);
                ASSERT_FLOAT_EQ(coordinatesConstructor.cosTheta(), cos(theta));
                ASSERT_FLOAT_EQ(coordinatesConstructor.sinTheta(), sin(theta));

                // Check equality operators
                const SphericalCoordinates same(radius, phi, theta);
                ASSERT_TRUE(same == coordinatesConstructor);
                const SphericalCoordinates otherRadius(radius + 0.1, phi, theta);
                ASSERT_TRUE(otherRadius != coordinatesConstructor);
                const SphericalCoordinates otherPhi(radius, phi + 0.1, theta);
                ASSERT_TRUE(otherPhi != coordinatesConstructor);
                const SphericalCoordinates otherTheta(radius, phi, theta + 0.1);
                ASSERT_TRUE(otherTheta != coordinatesConstructor);

                // Constructor (normalized)
                SphericalCoordinates coordinatesConstructorNormalized(radius, phi, theta, true);
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.radius(), radiusNormalized);
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.phi(), phiNormalized);
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.cosPhi(), cos(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.sinPhi(), sin(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.theta(), thetaNormalized);
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.cosTheta(), cos(thetaNormalized));
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.sinTheta(), sin(thetaNormalized));

                // Check equivalence
                ASSERT_TRUE(coordinatesConstructor.isEquivalent(coordinatesConstructorNormalized));

                // Setter
                SphericalCoordinates coordinatesSetter;
                coordinatesSetter.setRadius(radius);
                coordinatesSetter.setPhi(phi);
                coordinatesSetter.setTheta(theta);
                ASSERT_FLOAT_EQ(coordinatesSetter.radius(), radius);
                ASSERT_FLOAT_EQ(coordinatesSetter.phi(), phi);
                ASSERT_FLOAT_EQ(coordinatesSetter.cosPhi(), cos(phi));
                ASSERT_FLOAT_EQ(coordinatesSetter.sinPhi(), sin(phi));
                ASSERT_FLOAT_EQ(coordinatesSetter.theta(), theta);
                ASSERT_FLOAT_EQ(coordinatesSetter.cosTheta(), cos(theta));
                ASSERT_FLOAT_EQ(coordinatesSetter.sinTheta(), sin(theta));

                // Setter (normalized)
                SphericalCoordinates coordinatesSetterNormalized;
                coordinatesSetterNormalized.setRadius(radius);
                coordinatesSetterNormalized.setPhi(phi);
                coordinatesSetterNormalized.setTheta(theta);
                coordinatesSetterNormalized.normalize();
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.radius(), radiusNormalized);
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.phi(), phiNormalized);
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.cosPhi(), cos(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.sinPhi(), sin(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.theta(), thetaNormalized);
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.cosTheta(), cos(thetaNormalized));
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.sinTheta(), sin(thetaNormalized));

                // To cartesian
                const Eigen::Vector3d x(radiusNormalized * sin(thetaNormalized) * cos(phiNormalized), radiusNormalized * sin(thetaNormalized) * sin(phiNormalized), radiusNormalized * cos(thetaNormalized));
                ASSERT_LE((x - coordinatesConstructorNormalized.toCartesian()).norm(), 1e-9);
                const Eigen::Vector3d x_dr(sin(thetaNormalized) * cos(phiNormalized), sin(thetaNormalized) * sin(phiNormalized), cos(thetaNormalized));
                ASSERT_LE((x_dr - coordinatesConstructorNormalized.toCartesianFirstDerivativeRadius()).norm(), 1e-9);
                const Eigen::Vector3d x_dr_normalized(sin(thetaNormalized) * cos(phiNormalized), sin(thetaNormalized) * sin(phiNormalized), cos(thetaNormalized));
                ASSERT_LE((x_dr_normalized - coordinatesConstructorNormalized.toCartesianFirstDerivativeRadiusNormalized()).norm(), 1e-9);

                const Eigen::Vector3d x_dphi(-radiusNormalized * sin(thetaNormalized) * sin(phiNormalized), radiusNormalized * sin(thetaNormalized) * cos(phiNormalized), 0.0);
                ASSERT_LE((x_dphi - coordinatesConstructorNormalized.toCartesianFirstDerivativePhi()).norm(), 1e-9);
                const Eigen::Vector3d x_dphi_normalized(-sin(phiNormalized), cos(phiNormalized), 0.0);
                ASSERT_LE((x_dphi_normalized - coordinatesConstructorNormalized.toCartesianFirstDerivativePhiNormalized()).norm(), 1e-9);
                const Eigen::Vector3d x_dtheta(radiusNormalized * cos(thetaNormalized) * cos(phiNormalized), radiusNormalized * cos(thetaNormalized) * sin(phiNormalized), -radiusNormalized * sin(thetaNormalized));
                ASSERT_LE((x_dtheta - coordinatesConstructorNormalized.toCartesianFirstDerivativeTheta()).norm(), 1e-9);
                const Eigen::Vector3d x_dtheta_normalized(cos(thetaNormalized) * cos(phiNormalized), cos(thetaNormalized) * sin(phiNormalized), -sin(thetaNormalized));
                ASSERT_LE((x_dtheta_normalized - coordinatesConstructorNormalized.toCartesianFirstDerivativeThetaNormalized()).norm(), 1e-9);

                // From cartesian
                const SphericalCoordinates coordinatesFromCartesian = SphericalCoordinates::fromCartesian(SphericalCoordinates(radius, phi, theta).toCartesian());
                ASSERT_LE(fabs(coordinatesFromCartesian.radius() - radiusNormalized), 1e-9);
                ASSERT_LE(angleDifference(coordinatesFromCartesian.phi(), phiNormalized), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesian.cosPhi() - cos(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesian.sinPhi() - sin(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(angleDifference(coordinatesFromCartesian.theta(), thetaNormalized), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesian.cosTheta() - cos(coordinatesFromCartesian.theta())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesian.sinTheta() - sin(coordinatesFromCartesian.theta())), 1e-9);

                // From cartesian (normalized)
                const SphericalCoordinates coordinatesFromCartesianNormalized = SphericalCoordinates::fromCartesian(SphericalCoordinates(radiusNormalized, phiNormalized, thetaNormalized).toCartesian());
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.radius() - radiusNormalized), 1e-9);
                ASSERT_LE(angleDifference(coordinatesFromCartesianNormalized.phi(), phiNormalized), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.cosPhi() - cos(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.sinPhi() - sin(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(angleDifference(coordinatesFromCartesianNormalized.theta(), thetaNormalized), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.cosTheta() - cos(coordinatesFromCartesian.theta())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.sinTheta() - sin(coordinatesFromCartesian.theta())), 1e-9);
            }
        }
    }
}

#endif // HAVE_EIGEN3
