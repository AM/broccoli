/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/Triangle3D.hpp"
#include "TriangleHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Helper to create a certain permutation for a triangle definition
static inline Triangle3D triangleFromPermutation(const std::array<Eigen::Vector3d, 3>& points, const size_t& permutation)
{
    if (permutation == 0)
        return Triangle3D(points[0], points[1], points[2]);
    else if (permutation == 1)
        return Triangle3D(points[1], points[2], points[0]);
    else if (permutation == 2)
        return Triangle3D(points[2], points[0], points[1]);
    else if (permutation == 3)
        return Triangle3D(points[2], points[1], points[0]);
    else if (permutation == 4)
        return Triangle3D(points[1], points[0], points[2]);
    else
        return Triangle3D(points[0], points[2], points[1]);
}

//! Helper to check intersection tests
static inline bool checkIntersectionTest(const Triangle3D& triangle, const Eigen::Vector3d& rayOrigin, const Eigen::Vector3d& rayDirection, const bool& intersection, const Eigen::Vector3d& intersectionPoint, const double& rayPosition, const double& tolerance = 1e-9)
{
    const auto intersectionResult = triangle.evaluateRayIntersection(rayOrigin, rayDirection);
    if (intersectionResult.m_intersection != intersection)
        return false;
    if (intersection == true) {
        if (triangle.interpolatePoint(intersectionResult.m_intersectionPointCoordinates).isApprox(intersectionPoint, tolerance) == false)
            return false;
        if (fabs(intersectionResult.m_rayPosition - rayPosition) > tolerance)
            return false;
    }
    return true;
}

//! Helper to check distance tests
static inline bool checkDistanceTest(const Triangle3D& triangle, const Eigen::Vector3d& point, const Eigen::Vector3d& projectedPoint, const double& distanceToTrianglePlane, const Eigen::Vector3d& closestPoint, const double& tolerance = 1e-9)
{
    const auto distanceResult = triangle.evaluatePointDistance(point);
    if (triangle.interpolatePoint(distanceResult.m_projectedPointCoordinates).isApprox(projectedPoint, tolerance) == false)
        return false;
    if (fabs(distanceResult.m_distanceToTrianglePlane - distanceToTrianglePlane) > tolerance)
        return false;
    if (triangle.interpolatePoint(distanceResult.m_closestPointCoordinates).isApprox(closestPoint, tolerance) == false)
        return false;
    return true;
}

//! Tests basic construction and transformation
TEST(Triangle3D, BasicConstructionAndTransformation)
{
    // Setup triangle
    const Eigen::Vector3d v0(5, 5, 1);
    const Eigen::Vector3d v1(10, 5, 1);
    const Eigen::Vector3d v2(5, 15, 1);
    const Triangle3D triangle(v0, v1, v2);
    ASSERT_TRUE(v0.isApprox(triangle.v0()));
    ASSERT_TRUE(v1.isApprox(triangle.v1()));
    ASSERT_TRUE(v2.isApprox(triangle.v2()));

    // Check auxillary parameters
    const Eigen::Vector3d e0 = v1 - v0;
    const Eigen::Vector3d e1 = v2 - v0;
    const Eigen::Vector3d e2 = v2 - v1;
    const Eigen::Vector3d n = Eigen::Vector3d::UnitZ();
    ASSERT_TRUE(triangle.e0().isApprox(e0));
    ASSERT_TRUE(triangle.e1().isApprox(e1));
    ASSERT_TRUE(triangle.e2().isApprox(e2));
    ASSERT_FLOAT_EQ(triangle.e0_dot_e0(), e0.dot(e0));
    ASSERT_FLOAT_EQ(triangle.e1_dot_e1(), e1.dot(e1));
    ASSERT_FLOAT_EQ(triangle.e2_dot_e2(), e2.dot(e2));
    ASSERT_FLOAT_EQ(triangle.e0_dot_e1(), e0.dot(e1));
    ASSERT_FLOAT_EQ(triangle.e0_dot_e2(), e0.dot(e2));
    ASSERT_FLOAT_EQ(triangle.e1_dot_e2(), e1.dot(e2));
    ASSERT_FLOAT_EQ(triangle.area(), 25.0);
    ASSERT_FALSE(triangle.degenerated());
    ASSERT_TRUE(triangle.n().isApprox(Eigen::Vector3d::UnitZ()));
    ASSERT_TRUE(triangle.boundingBoxMinimum().isApprox(v0));
    ASSERT_TRUE(triangle.boundingBoxMaximum().isApprox(v0 + e0 + e1));
    Eigen::Matrix3d D;
    D << e0.x(), e1.x(), n.x(), //
        e0.y(), e1.y(), n.y(), //
        e0.z(), e1.z(), n.z();
    const Eigen::Matrix3d invD = D.inverse();
    ASSERT_TRUE(triangle.invD().isApprox(invD));

    // Setup degenerated triangle
    const Triangle3D degenerated(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(1, 0, 0));
    ASSERT_TRUE(degenerated.degenerated());

    // Setup translation and rotation
    const Eigen::Vector3d translation(0.123, -2.345, 1.45);
    const Eigen::Matrix3d rotation = (Eigen::AngleAxisd(0.123, Eigen::Vector3d::UnitX()) * Eigen::AngleAxisd(-0.234, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(0.345, Eigen::Vector3d::UnitZ())).toRotationMatrix();
    const Eigen::Vector3d tv0 = translation + rotation * v0;
    const Eigen::Vector3d tv1 = translation + rotation * v1;
    const Eigen::Vector3d tv2 = translation + rotation * v2;
    Triangle3D transformedTriangle = triangle;
    ASSERT_TRUE(transformedTriangle == triangle);
    ASSERT_FALSE(transformedTriangle != triangle);
    ASSERT_TRUE(fullComparison(transformedTriangle, triangle));
    transformedTriangle.rotate(rotation);
    ASSERT_FALSE(transformedTriangle == triangle);
    ASSERT_TRUE(transformedTriangle != triangle);
    ASSERT_FALSE(fullComparison(transformedTriangle, triangle));
    transformedTriangle.translate(translation);
    const Triangle3D expectedTransformedTriangle(tv0, tv1, tv2);
    ASSERT_TRUE(expectedTransformedTriangle == transformedTriangle);
    ASSERT_FALSE(expectedTransformedTriangle != transformedTriangle);
    ASSERT_TRUE(fullComparison(expectedTransformedTriangle, transformedTriangle));
}

//! Tests intersection and distance evaluations
TEST(Triangle3D, IntersectionAndDistanceTest)
{
    const double rayPosition = 1.23;
    const Eigen::Vector3d rayDirection(-1.0, -1.0, -1.0);
    const double distanceToPlane = 2.34;
    const Eigen::Vector3d distanceShift(0.0, 0.0, distanceToPlane);

    // Check for sharp angles
    // ----------------------
    const std::array<Eigen::Vector3d, 3> sharpPoints{ { Eigen::Vector3d(5, 5, 1), Eigen::Vector3d(10, 5, 1), Eigen::Vector3d(5, 15, 1) } };
    for (size_t p = 0; p < 6; p++) {
        const Triangle3D triangle = triangleFromPermutation(sharpPoints, p);
        const double distanceToTrianglePlane = (rayDirection.dot(triangle.n()) < 0) ? distanceToPlane : -distanceToPlane;

        // Test inside points
        const Eigen::Vector3d insideA(7, 6, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, insideA - rayPosition * rayDirection, rayDirection, true, insideA, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, insideA + distanceShift, insideA, distanceToTrianglePlane, insideA));
        const Eigen::Vector3d insideB(8, 8, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, insideB - rayPosition * rayDirection, rayDirection, true, insideB, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, insideB + distanceShift, insideB, distanceToTrianglePlane, insideB));
        const Eigen::Vector3d insideC(6, 9, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, insideC - rayPosition * rayDirection, rayDirection, true, insideC, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, insideC + distanceShift, insideC, distanceToTrianglePlane, insideC));

        // Test outside points
        const Eigen::Vector3d outsideA(1, 3, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideA - rayPosition * rayDirection, rayDirection, false, outsideA, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideA + distanceShift, outsideA, distanceToTrianglePlane, Eigen::Vector3d(5, 5, 1)));
        const Eigen::Vector3d outsideB(8, 1, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideB - rayPosition * rayDirection, rayDirection, false, outsideB, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideB + distanceShift, outsideB, distanceToTrianglePlane, Eigen::Vector3d(8, 5, 1)));
        const Eigen::Vector3d outsideC(14, 3, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideC - rayPosition * rayDirection, rayDirection, false, outsideC, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideC + distanceShift, outsideC, distanceToTrianglePlane, Eigen::Vector3d(10, 5, 1)));
        const Eigen::Vector3d outsideD(8, 14, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideD - rayPosition * rayDirection, rayDirection, false, outsideD, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideD + distanceShift, outsideD, distanceToTrianglePlane, Eigen::Vector3d(6, 13, 1)));
        const Eigen::Vector3d outsideE(3, 22, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideE - rayPosition * rayDirection, rayDirection, false, outsideE, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideE + distanceShift, outsideE, distanceToTrianglePlane, Eigen::Vector3d(5, 15, 1)));
        const Eigen::Vector3d outsideF(1, 14, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, outsideF - rayPosition * rayDirection, rayDirection, false, outsideF, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, outsideF + distanceShift, outsideF, distanceToTrianglePlane, Eigen::Vector3d(5, 14, 1)));

        // Test parallel ray
        ASSERT_TRUE(checkIntersectionTest(triangle, insideA + Eigen::Vector3d(0, 0, 1.23), Eigen::Vector3d(1.23, -2.34, 0.0), false, insideA, 0.0));
    }

    // Check for obtuse angles
    // -----------------------
    const std::array<Eigen::Vector3d, 3> obtusePoints{ { Eigen::Vector3d(5, 5, 1), Eigen::Vector3d(10, 0, 1), Eigen::Vector3d(5, 15, 1) } };
    for (size_t p = 0; p < 6; p++) {
        const Triangle3D triangle = triangleFromPermutation(obtusePoints, p);
        const double distanceToTrianglePlane = (rayDirection.dot(triangle.n()) < 0) ? distanceToPlane : -distanceToPlane;

        // Test corner points
        const Eigen::Vector3d cornerA(1, 7, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, cornerA - rayPosition * rayDirection, rayDirection, false, cornerA, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, cornerA + distanceShift, cornerA, distanceToTrianglePlane, Eigen::Vector3d(5, 7, 1)));
        const Eigen::Vector3d cornerB(1, 4, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, cornerB - rayPosition * rayDirection, rayDirection, false, cornerB, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, cornerB + distanceShift, cornerB, distanceToTrianglePlane, Eigen::Vector3d(5, 5, 1)));
        const Eigen::Vector3d cornerC(3, 1, 1);
        ASSERT_TRUE(checkIntersectionTest(triangle, cornerC - rayPosition * rayDirection, rayDirection, false, cornerC, rayPosition));
        ASSERT_TRUE(checkDistanceTest(triangle, cornerC + distanceShift, cornerC, distanceToTrianglePlane, Eigen::Vector3d(6, 4, 1)));
    }
}

#endif // HAVE_EIGEN3
