/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/geometry/CGMaterial.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Check comparison operators
TEST(CGMaterial, ComparisonOperators)
{
    // Initialize variables
    CGMaterial materialA, materialB;

    // Test equality
    ASSERT_TRUE(materialA == materialB);
    ASSERT_FALSE(materialA != materialB);

    // Test inequality
    materialB.m_name = "test";
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_ambientColor[0] = 128;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_ambientColorFromVertexColor = !materialA.m_ambientColorFromVertexColor;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_ambientIntensity = 0.5;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_diffuseColor[0] = 128;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_diffuseColorFromVertexColor = !materialA.m_diffuseColorFromVertexColor;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_diffuseIntensity = 0.5;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_specularColor[0] = 128;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_specularColorFromVertexColor = !materialA.m_specularColorFromVertexColor;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_specularIntensity = 0.5;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_shininess = 0.5;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_alpha = 128;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_alphaFromVertexColor = !materialA.m_alphaFromVertexColor;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
    materialB = materialA;
    materialB.m_shadeless = !materialA.m_shadeless;
    ASSERT_TRUE(materialA != materialB);
    ASSERT_FALSE(materialA == materialB);
}

//! Check serialize() and deserialize()
TEST(CGMaterial, serialization)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        io::serialization::Endianness endianness = io::serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = io::serialization::Endianness::BIG;

        // Setup test material
        CGMaterial inputMaterial;
        inputMaterial.m_name = "abc";
        inputMaterial.m_ambientColor = std::array<uint8_t, 3>{ 10, 20, 30 };
        inputMaterial.m_ambientColorFromVertexColor = !inputMaterial.m_ambientColorFromVertexColor;
        inputMaterial.m_ambientIntensity = 0.1234;
        inputMaterial.m_diffuseColor = std::array<uint8_t, 3>{ 40, 50, 60 };
        inputMaterial.m_diffuseColorFromVertexColor = !inputMaterial.m_diffuseColorFromVertexColor;
        inputMaterial.m_diffuseIntensity = 0.2345;
        inputMaterial.m_specularColor = std::array<uint8_t, 3>{ 70, 80, 90 };
        inputMaterial.m_specularColorFromVertexColor = !inputMaterial.m_specularColorFromVertexColor;
        inputMaterial.m_specularIntensity = 0.3456;
        inputMaterial.m_shininess = 0.567;
        inputMaterial.m_alpha = 128;
        inputMaterial.m_alphaFromVertexColor = !inputMaterial.m_alphaFromVertexColor;
        inputMaterial.m_shadeless = !inputMaterial.m_shadeless;

        // Test serialization
        io::serialization::BinaryStream stream;
        io::serialization::BinaryStreamSize returnValueSerialization = inputMaterial.serialize(stream, endianness);

        // Test deserialization
        CGMaterial outputMaterial;
        io::serialization::BinaryStreamSize returnValueDeSerialization = outputMaterial.deSerialize(stream, 0, endianness);

        // Test result
        ASSERT_EQ(returnValueSerialization, returnValueDeSerialization);
        ASSERT_TRUE(outputMaterial == inputMaterial);
    }
}
