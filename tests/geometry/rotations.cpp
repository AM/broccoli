/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/geometry/rotations.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Test computation of exponential of quaternions
TEST(quaternions, quaternionExponential)
{
    // Compute exponential for various cases
    EXPECT_TRUE(quaternionExponential(Eigen::Quaterniond(1.0, 0, 0, 0)).isApprox(Eigen::Quaterniond(exp(1), 0, 0, 0), 0.01));
    EXPECT_TRUE(quaternionExponential(Eigen::Quaterniond(2.0, 0, 0, 0)).isApprox(Eigen::Quaterniond(exp(2), 0, 0, 0), 0.01));
    EXPECT_TRUE(quaternionExponential(Eigen::Quaterniond(0.5, 0, 0, 0)).isApprox(Eigen::Quaterniond(exp(0.5), 0, 0, 0), 0.01));
    EXPECT_TRUE(quaternionExponential(Eigen::Quaterniond(1.0, 2.0, 3.0, 4.0)).isApprox(Eigen::Quaterniond(1.694, -0.7896, -1.184, -1.579), 0.01));
}

//! Test computation of quaternion natural logarithm
TEST(quaternions, quaternionNaturalLogarithm)
{
    // Create test quaternion
    Eigen::Quaterniond qtest(1, 0, 1, 0);
    qtest.normalize();
    Eigen::Quaterniond naturalLogarithmTest(0, 0, 0.7854, 0);

    // Compute natural logarithm
    Eigen::Quaterniond naturalLogarithm = quaternionNaturalLogarithm(qtest);
    EXPECT_TRUE(naturalLogarithm.isApprox(naturalLogarithmTest, 0.01));

    // Exp Ln relation
    Eigen::Quaterniond anyQ(1.2, 3.4, -1.0, 3.2);
    EXPECT_TRUE(quaternionExponential(quaternionNaturalLogarithm(anyQ)).isApprox(anyQ));

    // Zero norm
    EXPECT_TRUE(quaternionNaturalLogarithm(Eigen::Quaterniond(0.0, 0.0, 0.0, 0.0)).norm() == 0.0);

    // small angle
    Eigen::Quaterniond smallAngle(1.0, std::numeric_limits<double>::epsilon(), 0.0, 0.0);
    Eigen::Quaterniond smallAngleLn = quaternionNaturalLogarithm(smallAngle);
    EXPECT_DOUBLE_EQ(smallAngleLn.w(), 0.0);
    EXPECT_TRUE(quaternionExponential(smallAngleLn).isApprox(smallAngle, std::numeric_limits<double>::epsilon()));
}

//! Test computation of power of quaternions
TEST(quaternions, quaternionPower)
{
    // Quaternion^Quaternion
    // ---------------------
    // Compute power for various cases
    ASSERT_TRUE(quaternionPower(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(1.0, 0, 0, 0)).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionPower(Eigen::Quaterniond(1.0, 2.0, 3.0, 4.0), Eigen::Quaterniond(0.5, 0.6, 0.7, 0.8)).isApprox(Eigen::Quaterniond(-0.4095, 0.0665, 0.101, 0.1004), 0.01));

    // Quaternion^Quaternion
    // ---------------------
    // Compute power for various cases
    ASSERT_TRUE(quaternionPower(Eigen::Quaterniond(1.0, 0, 0, 0), 2.0).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionPower(Eigen::Quaterniond(1.0, 2.0, 3.0, 4.0), 2.5).isApprox(Eigen::Quaterniond(-66.5, -8.36, -12.54, -16.72), 0.01));
}

//! Test computation of SLERP
TEST(quaternions, quaternionSLERP)
{
    // Check singularity avoidance
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(1.0, 0, 0, 0), 0.0, true).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(1.0, 0, 0, 0), 0.5, true).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(1.0, 0, 0, 0), 1.0, true).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));

    // Check shortest path computation
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 0.0, false).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 0.5, false).isApprox(Eigen::Quaterniond(0.92388, 0, 0.382683, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 1.0, false).isApprox(Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 0.0, true).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 0.5, true).isApprox(Eigen::Quaterniond(0.92388, 0, 0.382683, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 1.0, true).isApprox(Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 0.01));

    // Check non-shortest path computation
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(-0.707107, 0, -0.707107, 0), 0.0, false).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(-0.707107, 0, -0.707107, 0), 0.5, false).isApprox(Eigen::Quaterniond(0.382683, 0, -0.92388, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(-0.707107, 0, -0.707107, 0), 1.0, false).isApprox(Eigen::Quaterniond(-0.707107, 0, -0.707107, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(-0.707107, 0, -0.707107, 0), 0.0, true).isApprox(Eigen::Quaterniond(1.0, 0, 0, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(-0.707107, 0, -0.707107, 0), 0.5, true).isApprox(Eigen::Quaterniond(0.92388, 0, 0.382683, 0), 0.01));
    ASSERT_TRUE(quaternionSLERP(Eigen::Quaterniond(1.0, 0, 0, 0), Eigen::Quaterniond(-0.707107, 0, -0.707107, 0), 1.0, true).isApprox(Eigen::Quaterniond(0.707107, 0, 0.707107, 0), 0.01));
}

TEST(quaternions, rotationVector)
{
    Eigen::Quaterniond q(1.0, 0.2, 0.4, -0.2);
    q.normalize();

    Eigen::AngleAxisd reference(q);
    EXPECT_TRUE(rotationVectorFrom(q).isApprox(reference.angle() * reference.axis()));
    EXPECT_TRUE(rotationVectorFrom(q.coeffs()).isApprox(reference.angle() * reference.axis()));
}

TEST(quaternions, quaternionFrom)
{
    // from rotation vector
    Eigen::Vector3d rotationVector(0.1, 0.2, 0.3);
    Eigen::Quaterniond reference(Eigen::AngleAxisd(rotationVector.norm(), rotationVector.normalized()));

    EXPECT_TRUE(quaternionFrom(rotationVector).isApprox(reference, 1.0e-05));
}

TEST(quaternions, tildeOperator)
{
    Eigen::Vector3d input(1.0, 2.0, 3.0);
    Eigen::Matrix3d result = tildeOperator(input);
    EXPECT_TRUE(result.isApprox(-result.transpose()));

    EXPECT_DOUBLE_EQ(result(0, 1), -3.0);
    EXPECT_DOUBLE_EQ(result(0, 2), 2.0);
    EXPECT_DOUBLE_EQ(result(1, 2), -1.0);
}

TEST(quaternions, quaternionDerivativeLeft)
{
    // Reference:
    // Given a constant position vector _i r, a rotation matrix _j A_i, and an
    // angular velocity _i omega_j,i between the frames i and j,
    // we can compute the absolute velocity of r in j:
    // _j v_ref = _j A_i * (_i omega_j,i x _i r)
    Eigen::Vector3d i_r(1.0, 1.0, 1.0);
    Eigen::Vector3d j_omega_ji(1.0, -2.0, 0.0);
    Eigen::AngleAxisd j_A_i(30.0 * M_PI / 180.0, Eigen::Vector3d::UnitZ());

    Eigen::Vector3d i_omega_ji = j_A_i.toRotationMatrix().transpose() * j_omega_ji;
    Eigen::Vector3d j_v_ref = j_A_i.toRotationMatrix() * tildeOperator(i_omega_ji) * i_r;

    // This velocity is now compared to the velocity, which results
    // from the corresponding quaternion operation:
    // j_v_q = dot_j_s_i * i_r_q * j_s_i^* + j_s_i * i_r_q dot_j_s_i^*
    Eigen::Quaterniond i_r_q;
    i_r_q.w() = 0.0;
    i_r_q.vec() = i_r;
    Eigen::Quaterniond dot_j_s_i = quaternionDerivativeLeftFoR(Eigen::Quaterniond(j_A_i), j_omega_ji);
    Eigen::Vector3d j_v_q = (dot_j_s_i * i_r_q * Eigen::Quaterniond(j_A_i).conjugate()).vec() + (Eigen::Quaterniond(j_A_i) * i_r_q * dot_j_s_i.conjugate()).vec();

    EXPECT_TRUE(j_v_q.isApprox(j_v_ref, 1.0e-06));
}

TEST(quaternions, quaternionDerivativeRight)
{
    // Reference:
    // Given a constant position vector _i r, a rotation matrix _j A_i, and an
    // angular velocity _i omega_j,i between the frames i and j,
    // we can compute the absolute velocity of r in j:
    // _j v_ref = _j A_i * (_i omega_j,i x _i r)
    Eigen::Vector3d i_r(1.0, 1.0, 1.0);
    Eigen::Vector3d i_omega_ji(1.0, -2.0, 0.0);
    Eigen::AngleAxisd j_A_i(30.0 * M_PI / 180.0, Eigen::Vector3d::UnitZ());

    Eigen::Vector3d j_v_ref = j_A_i.toRotationMatrix() * tildeOperator(i_omega_ji) * i_r;

    // This velocity is now compared to the velocity, which results
    // from the corresponding quaternion operation:
    // j_v_q = dot_j_s_i * i_r_q * j_s_i^* + j_s_i * i_r_q dot_j_s_i^*
    Eigen::Quaterniond i_r_q;
    i_r_q.w() = 0.0;
    i_r_q.vec() = i_r;
    Eigen::Quaterniond dot_j_s_i = quaternionDerivativeRightFoR(Eigen::Quaterniond(j_A_i), i_omega_ji);
    Eigen::Vector3d j_v_q = (dot_j_s_i * i_r_q * Eigen::Quaterniond(j_A_i).conjugate()).vec() + (Eigen::Quaterniond(j_A_i) * i_r_q * dot_j_s_i.conjugate()).vec();

    EXPECT_TRUE(j_v_q.isApprox(j_v_ref, 1.0e-06));
}

TEST(quaternions, convenienceOverloads)
{
    Eigen::Quaterniond q = Eigen::Quaterniond::UnitRandom();
    Eigen::Quaterniond dotq = Eigen::Quaterniond::UnitRandom();
    Eigen::Vector3d omega = Eigen::Vector3d::Random();

    // Test overloaded interfaces
    EXPECT_TRUE(rotationVectorFrom(q).isApprox(rotationVectorFrom(q.coeffs())));
    EXPECT_TRUE(quaternionDerivativeLeftFoR(q, omega).isApprox(quaternionDerivativeLeftFoR(q.coeffs(), omega)));
    EXPECT_TRUE(quaternionDerivativeRightFoR(q, omega).isApprox(quaternionDerivativeRightFoR(q.coeffs(), omega)));
    EXPECT_TRUE(angularVelocityLeftFoR(q, dotq).isApprox(angularVelocityLeftFoR(q.coeffs(), dotq.coeffs())));
    EXPECT_TRUE(angularVelocityRightFoR(q, dotq).isApprox(angularVelocityRightFoR(q.coeffs(), dotq.coeffs())));
    EXPECT_TRUE(quaternionJacobianLeftFoR(q).isApprox(quaternionJacobianLeftFoR(q.coeffs())));
    EXPECT_TRUE(quaternionJacobianRightFoR(q).isApprox(quaternionJacobianRightFoR(q.coeffs())));
}

TEST(quaternions, angularVelocitiesFromQuaternion)
{
    // The relation between angular velocities and the quaternion
    // derivatives has already been tested.
    // We only test the identity of the two operations here
    Eigen::Quaterniond q = Eigen::Quaterniond::UnitRandom();
    Eigen::Vector3d omega = Eigen::Vector3d::Random();

    Eigen::Quaterniond dot_s_left = quaternionDerivativeLeftFoR(q, omega);
    EXPECT_TRUE(angularVelocityLeftFoR(q, dot_s_left).isApprox(omega));

    Eigen::Quaterniond dot_s_right = quaternionDerivativeRightFoR(q, omega);
    EXPECT_TRUE(angularVelocityRightFoR(q, dot_s_right).isApprox(omega));
}

TEST(quaternions, Jacobians)
{
    Eigen::Quaterniond q = Eigen::Quaterniond::UnitRandom();
    Eigen::Vector3d omega = Eigen::Vector3d::Random();

    // dot_s = J * omega
    EXPECT_TRUE(quaternionDerivativeLeftFoR(q, omega).coeffs().isApprox(quaternionJacobianLeftFoR(q) * omega));
    EXPECT_TRUE(quaternionDerivativeRightFoR(q, omega).coeffs().isApprox(quaternionJacobianRightFoR(q) * omega));
}