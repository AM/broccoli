/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/Polygon2D.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Check comparison operators
TEST(Polygon2D, ComparisonOperators)
{
    // Initialize variables
    Polygon2D<> polygonA, polygonB; // Polygons to test
    Polygon2DVertex points[3]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 1, 1;
    for (int i = 0; i < 3; i++) {
        polygonA.m_vertices.push_back(points[i]);
        polygonB.m_vertices.push_back(2.0 * points[i]);
    }

    // Inequality
    ASSERT_TRUE(polygonA != polygonB);

    // Equality
    Polygon2D<> polygonC = polygonA;
    ASSERT_TRUE(polygonA == polygonC);
}

//! Test string representation of types
TEST(Polygon2D, TypesToString)
{
    uint8_t i;

    for (i = 0; i < static_cast<uint8_t>(Polygon2DResult::RESULT_COUNT); i++) {
        ASSERT_GT(polygon2DResultString(static_cast<Polygon2DResult>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(polygon2DResultString(static_cast<Polygon2DResult>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(polygon2DResultString(static_cast<Polygon2DResult>(i)) != "UNKNOWN");
        }
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(Polygon2DResult::RESULT_COUNT);
    ASSERT_TRUE(polygon2DResultString(static_cast<Polygon2DResult>(i)) == "UNKNOWN");
#endif
}

//! Check encoding
TEST(Polygon2D, encode)
{
    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[3]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 1, 1;
    for (int i = 0; i < 3; i++)
        polygon.m_vertices.push_back(points[i]);
    io::encoding::CharacterStream stream;

    // Test encoding
    io::encoding::CharacterStreamSize streamSize = polygon.encodeToXML(stream, 1, 4);
    ASSERT_EQ(streamSize > 0, true);
    ASSERT_EQ(streamSize, stream.size());
}

//! Check computation of the perimeter of polygon
TEST(Polygon2D, perimeter)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[3]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 1, 1;
    for (int i = 0; i < 3; i++)
        polygon.m_vertices.push_back(points[i]);

    // Check perimeter
    ASSERT_TRUE(polygon.perimeter(&result) == (double)2.0 + 2.0 * sqrt(2.0));
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check detection of too few points
#ifdef NDEBUG
    polygon.m_vertices.resize(2);
    polygon.perimeter(&result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif
}

//! Check computation of the (signed) area of polygon
TEST(Polygon2D, area)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[3]; // Point set
    points[0] << 0, 0;
    points[1] << 1, 1;
    points[2] << 2, 0;
    for (int i = 0; i < 3; i++)
        polygon.m_vertices.push_back(points[i]);

    // Check area
    ASSERT_TRUE(polygon.areaSigned(&result) == (double)-1.0);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.area(&result) == (double)1.0);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check detection of too few points
#ifdef NDEBUG
    polygon.m_vertices.resize(2);
    polygon.areaSigned(&result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
    polygon.area(&result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif
}

//! Check computation of the centroid of polygon
TEST(Polygon2D, Centroid)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[3]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 1, 1;
    for (int i = 0; i < 3; i++)
        polygon.m_vertices.push_back(points[i]);

    // Check area
    Polygon2DVertex centroid = polygon.centroid(&result);
    ASSERT_TRUE(centroid(0) == (double)1.0);
    ASSERT_TRUE(centroid(1) == (double)1.0 / 3.0);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check detection of too few points
#ifdef NDEBUG
    polygon.m_vertices.resize(2);
    polygon.centroid(&result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif
}

//! Check computation of convex hull of polygon
TEST(Polygon2D, reComputeConvexHull)
{
    // Initialize helpers
    Polygon2DResult result;
    bool returnValue;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[18]; // Point set
    points[0] << 2, 1; // -|
    points[1] << 3, 1; // -|--> all on one line
    points[2] << 4, 1; // -|
    points[3] << -0.5, -1;
    points[4] << 2.5, 2.5;
    points[5] << 4, -1;
    points[6] << 5.5, 0.5;
    points[7] << 2.5, -2;
    points[8] << 5.5, -3;
    points[9] << 4, 4;
    points[10] << -2.5, -3;
    points[11] << 7.5, 0;
    points[12] << 6.5, -1.5;
    points[13] << 1.5, -3;
    points[14] << -1, 2;
    points[15] << 2.5, 0;
    points[16] << 0.5, -2;
    points[17] << 5.5, -3; // Coincides with [8]

    // Test: less than three points are given
    // --------------------------------------
#ifdef NDEBUG
    // 0 points
    returnValue = polygon.reComputeConvexHull(&result);
    ASSERT_TRUE(polygon.m_vertices.size() == 0);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);

    // 1 point
    polygon.m_vertices.clear();
    for (int i = 0; i < 1; i++)
        polygon.m_vertices.push_back(points[i]);
    returnValue = polygon.reComputeConvexHull(&result);
    ASSERT_TRUE(polygon.m_vertices.size() == 0);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);

    // 2 points
    polygon.m_vertices.clear();
    for (int i = 0; i < 2; i++)
        polygon.m_vertices.push_back(points[i]);
    returnValue = polygon.reComputeConvexHull(&result);
    ASSERT_TRUE(polygon.m_vertices.size() == 0);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif

    // Test: three points, which are on one line
    // -----------------------------------------
#ifdef NDEBUG
    polygon.m_vertices.clear();
    for (int i = 0; i < 3; i++)
        polygon.m_vertices.push_back(points[i]);
    returnValue = polygon.reComputeConvexHull(&result);
    ASSERT_TRUE(polygon.m_vertices.size() == 0);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_NONCOINCIDING_POINTS_NOT_ON_LINE);
#endif

    // Test: small point set (4 points)
    // --------------------------------
    polygon.m_vertices.clear();
    for (int i = 0; i < 4; i++)
        polygon.m_vertices.push_back(points[i]);
    returnValue = polygon.reComputeConvexHull(&result);
    ASSERT_TRUE(polygon.m_vertices.size() == 3);
    if (polygon.m_vertices.size() == 3) {
        ASSERT_TRUE(polygon.m_vertices[0].isApprox(points[3]));
        ASSERT_TRUE(polygon.m_vertices[1].isApprox(points[2]));
        ASSERT_TRUE(polygon.m_vertices[2].isApprox(points[0]));
    }
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Test: full point set (18 points)
    // -------------------------------
    polygon.m_vertices.clear();
    for (int i = 0; i < 18; i++)
        polygon.m_vertices.push_back(points[i]);
    returnValue = polygon.reComputeConvexHull(&result);
    ASSERT_TRUE(polygon.m_vertices.size() == 5);
    if (polygon.m_vertices.size() == 5) {
        ASSERT_TRUE(polygon.m_vertices[0].isApprox(points[10]));
        ASSERT_TRUE(polygon.m_vertices[1].isApprox(points[8]));
        ASSERT_TRUE(polygon.m_vertices[2].isApprox(points[11]));
        ASSERT_TRUE(polygon.m_vertices[3].isApprox(points[9]));
        ASSERT_TRUE(polygon.m_vertices[4].isApprox(points[14]));
    }
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
}

//! Check computation of point / convex polygon intersection
TEST(Polygon2D, checkPointIntersectionConvex)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[3]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 1, 1;
    for (int i = 0; i < 3; i++)
        polygon.m_vertices.push_back(points[i]);

    // Testing point set
    Polygon2DVertex testPoints[3]; // Point set
    testPoints[0] << 1, 0.5;
    testPoints[1] << 1, -0.5;
    testPoints[2] << 2, 1;

    // Check intersection
    ASSERT_TRUE(polygon.checkPointIntersection(testPoints[0], &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.checkPointIntersection(testPoints[1], &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.checkPointIntersection(testPoints[2], &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check detection of too few points
#ifdef NDEBUG
    polygon.m_vertices.resize(2);
    polygon.checkPointIntersection(testPoints[0], &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif
}

//! Check computation of point / concave polygon intersection
TEST(Polygon2D, checkPointIntersectionConcave)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[4]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 0.5, 0.5;
    points[3] << 0, 2;
    for (int i = 0; i < 4; i++)
        polygon.m_vertices.push_back(points[i]);

    // Testing point set
    Polygon2DVertex testPoints[4]; // Point set
    testPoints[0] << 0.1, 0.1;
    testPoints[1] << 0.6, 0.6;
    testPoints[2] << 1, 0.001;
    testPoints[3] << 1, -0.001;

    // Check intersection
    ASSERT_TRUE(polygon.checkPointIntersection(testPoints[0], &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.checkPointIntersection(testPoints[1], &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.checkPointIntersection(testPoints[2], &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.checkPointIntersection(testPoints[3], &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
}

//! Check computation of ray intersection
TEST(Polygon2D, checkRayIntersections)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[4]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 0.5, 0.5;
    points[3] << 0, 2;
    for (int i = 0; i < 4; i++)
        polygon.m_vertices.push_back(points[i]);

    // Testing ray set
    Polygon2DVertex rayOrigins[4];
    Polygon2DVertex rayDirections[4];
    // Two-point intersection
    rayOrigins[0] << -1, -1;
    rayDirections[0] << 1, 1;
    // Single-point intersection
    rayOrigins[1] << -2, 2;
    rayDirections[1] << 1, -1;
    // No intersection
    rayOrigins[2] << 3, 0;
    rayDirections[2] << 0, 1;
    // Parallel intersection
    rayOrigins[3] << -1, 0;
    rayDirections[3] << 1, 0;

    // Check intersections
    // -------------------
    std::vector<double> intersectionResult;

    // Check two-point intersection
    intersectionResult = polygon.checkRayIntersections(rayOrigins[0], rayDirections[0], 0, 0, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 2);
    ASSERT_TRUE(intersectionResult[0] == 1.0);
    ASSERT_TRUE(intersectionResult[1] == 1.5);

    // Check two-point intersection with lambda-bounds
    intersectionResult = polygon.checkRayIntersections(rayOrigins[0], rayDirections[0], 0, 1.2, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 1.0);
    intersectionResult = polygon.checkRayIntersections(rayOrigins[0], rayDirections[0], 1.2, 2.0, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 1.5);

    // Check two-point intersection with first solution only (no lambda bounds)
    intersectionResult = polygon.checkRayIntersections(rayOrigins[0], rayDirections[0], 0, 0, true, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 1.0);

    // Check two-point intersection with first solution only (with lambda bounds)
    intersectionResult = polygon.checkRayIntersections(rayOrigins[0], rayDirections[0], 1.2, 2.0, true, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 1.5);

    // Check single-point intersection
    intersectionResult = polygon.checkRayIntersections(rayOrigins[1], rayDirections[1], 0, 0, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 2.0);

    // Check single-point intersection with bounds
    intersectionResult = polygon.checkRayIntersections(rayOrigins[1], rayDirections[1], 0, 3.0, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 2.0);
    intersectionResult = polygon.checkRayIntersections(rayOrigins[1], rayDirections[1], 0, 1.0, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 0);

    // Check no intersection
    intersectionResult = polygon.checkRayIntersections(rayOrigins[2], rayDirections[2], 0, 0, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 0);

    // Check parallel intersection
    intersectionResult = polygon.checkRayIntersections(rayOrigins[3], rayDirections[3], 0, 0, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() > 0);

    // Check parallel intersection with bounds
    intersectionResult = polygon.checkRayIntersections(rayOrigins[3], rayDirections[3], 0, 0.5, false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 0);

    // Check detection of invalid ray direction
#ifdef NDEBUG
    polygon.checkRayIntersections(rayOrigins[0], Eigen::Vector2d(0, 0), 1.2, 2.0, true, &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_RAY_DIRECTION);
#endif

    // Check detection of invalid lambda boundaries
#ifdef NDEBUG
    polygon.checkRayIntersections(rayOrigins[0], rayDirections[0], 2, 1, true, &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_LAMBDA_BOUNDARIES);
#endif

    // Check detection of too few points
#ifdef NDEBUG
    polygon.m_vertices.resize(2);
    polygon.checkRayIntersections(rayOrigins[0], rayDirections[0], 1.2, 2.0, true, &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif
}

//! Check computation of line intersection
TEST(Polygon2D, checkLineIntersections)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[4]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 0.5, 0.5;
    points[3] << 0, 2;
    for (int i = 0; i < 4; i++)
        polygon.m_vertices.push_back(points[i]);

    // Testing line set
    Polygon2DVertex lineStarts[4];
    Polygon2DVertex lineEnds[4];
    // Two-point intersection
    lineStarts[0] << -1, -1;
    lineEnds[0] << 1, 1;
    // Single-point intersection
    lineStarts[1] << -2, 2;
    lineEnds[1] << 2, -2;
    // No intersection
    lineStarts[2] << 1, 1;
    lineEnds[2] << 2, 1;
    // Parallel intersection
    lineStarts[3] << -1, 0;
    lineEnds[3] << 5, 0;

    // Check intersections
    // -------------------
    std::vector<double> intersectionResult;

    // Check two-point intersection
    intersectionResult = polygon.checkLineIntersections(lineStarts[0], lineEnds[0], false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 2);
    ASSERT_TRUE(intersectionResult[0] == 0.5);
    ASSERT_TRUE(intersectionResult[1] == 0.75);

    // Check two-point intersection (only first)
    intersectionResult = polygon.checkLineIntersections(lineStarts[0], lineEnds[0], true, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 0.5);

    // Check single-point intersection
    intersectionResult = polygon.checkLineIntersections(lineStarts[1], lineEnds[1], false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 1);
    ASSERT_TRUE(intersectionResult[0] == 0.5);

    // Check no intersection
    intersectionResult = polygon.checkLineIntersections(lineStarts[2], lineEnds[2], false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() == 0);

    // Check parallel intersection
    intersectionResult = polygon.checkLineIntersections(lineStarts[3], lineEnds[3], false, &result);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(intersectionResult.size() > 0);
}

//! Check computation of polygon-polygon intersection
TEST(Polygon2D, checkPolygonIntersection)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygonA, polygonB, polygonC, polygonD, polygonE; // Polygons to test
    Polygon2DVertex points[17]; // Point set
    points[0] << 2, 2;
    points[1] << -2, 2;
    points[2] << -2, -2;
    points[3] << 0, -1;
    points[4] << 2, -2;
    points[5] << 0, 0;
    points[6] << 1, 0;
    points[7] << 1, 1;
    points[8] << 0, 1;
    points[9] << 3, 2;
    points[10] << 4, 0;
    points[11] << 4, 2;
    points[12] << 0, 3;
    points[13] << 0, -1.5;
    points[14] << -1, -2;
    points[15] << 0, -2.5;
    points[16] << 1, -2;

    // Create polygon A
    polygonA.m_vertices.push_back(points[0]);
    polygonA.m_vertices.push_back(points[1]);
    polygonA.m_vertices.push_back(points[2]);
    polygonA.m_vertices.push_back(points[3]);
    polygonA.m_vertices.push_back(points[4]);

    // Create polygon B
    polygonB.m_vertices.push_back(points[5]);
    polygonB.m_vertices.push_back(points[6]);
    polygonB.m_vertices.push_back(points[7]);
    polygonB.m_vertices.push_back(points[8]);

    // Create polygon C
    polygonC.m_vertices.push_back(points[9]);
    polygonC.m_vertices.push_back(points[10]);
    polygonC.m_vertices.push_back(points[11]);

    // Create polygon D
    polygonD.m_vertices.push_back(points[13]);
    polygonD.m_vertices.push_back(points[14]);
    polygonD.m_vertices.push_back(points[15]);
    polygonD.m_vertices.push_back(points[16]);

    // Create polygon E
    polygonE.m_vertices.push_back(points[0]);
    polygonE.m_vertices.push_back(points[12]);
    polygonE.m_vertices.push_back(points[1]);

    // Check intersections of polygon A
    ASSERT_TRUE(polygonA.checkPolygonIntersection(polygonA, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonA.checkPolygonIntersection(polygonB, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonA.checkPolygonIntersection(polygonC, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonA.checkPolygonIntersection(polygonD, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonA.checkPolygonIntersection(polygonE, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check intersections of polygon B
    ASSERT_TRUE(polygonB.checkPolygonIntersection(polygonA, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonB.checkPolygonIntersection(polygonB, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonB.checkPolygonIntersection(polygonC, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonB.checkPolygonIntersection(polygonD, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonB.checkPolygonIntersection(polygonE, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check intersections of polygon C
    ASSERT_TRUE(polygonC.checkPolygonIntersection(polygonA, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonC.checkPolygonIntersection(polygonB, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonC.checkPolygonIntersection(polygonC, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonC.checkPolygonIntersection(polygonD, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonC.checkPolygonIntersection(polygonE, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check intersections of polygon D
    ASSERT_TRUE(polygonD.checkPolygonIntersection(polygonA, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonD.checkPolygonIntersection(polygonB, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonD.checkPolygonIntersection(polygonC, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonD.checkPolygonIntersection(polygonD, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonD.checkPolygonIntersection(polygonE, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check intersections of polygon E
    ASSERT_TRUE(polygonE.checkPolygonIntersection(polygonA, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonE.checkPolygonIntersection(polygonB, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonE.checkPolygonIntersection(polygonC, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonE.checkPolygonIntersection(polygonD, &result) == false);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygonE.checkPolygonIntersection(polygonE, &result) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check detection of too few points
#ifdef NDEBUG
    polygonA.m_vertices.resize(2);
    polygonA.checkPolygonIntersection(polygonB, &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
    polygonB.checkPolygonIntersection(polygonA, &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif
}

//! Check computation of point projection
TEST(Polygon2D, projectPoint)
{
    // Initialize helpers
    Polygon2DResult result;

    // Initialize variables
    Polygon2D<> polygon; // Polygon to test
    Polygon2DVertex points[4]; // Point set
    points[0] << 0, 0;
    points[1] << 2, 0;
    points[2] << 0.5, 0.5;
    points[3] << 0, 2;
    for (int i = 0; i < 4; i++)
        polygon.m_vertices.push_back(points[i]);

    // Testing point set
    Polygon2DVertex testPoints[5];
    testPoints[0] << 0, 0;
    testPoints[1] << -1, -1;
    testPoints[2] << 0, 5;
    testPoints[3] << 5, 0;
    testPoints[4] << 0.001, 1;

    // Check projections
    ASSERT_TRUE(polygon.projectPointToBorder(testPoints[0], &result).isApprox(points[0]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.projectPointToBorder(testPoints[1], &result).isApprox(points[0]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.projectPoint(testPoints[1], &result).isApprox(points[0]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.projectPointToBorder(testPoints[2], &result).isApprox(points[3]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.projectPoint(testPoints[2], &result).isApprox(points[3]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.projectPointToBorder(testPoints[3], &result).isApprox(points[1]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.projectPoint(testPoints[3], &result).isApprox(points[1]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    ASSERT_TRUE(polygon.projectPoint(testPoints[4], &result).isApprox(testPoints[4]) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);
    Polygon2DVertex desiredProjectedPoint;
    desiredProjectedPoint << 0, 1;
    ASSERT_TRUE(polygon.projectPointToBorder(testPoints[4], &result).isApprox(desiredProjectedPoint) == true);
    ASSERT_EQ(result, Polygon2DResult::SUCCESS);

    // Check detection of too few points
#ifdef NDEBUG
    polygon.m_vertices.resize(2);
    polygon.projectPointToBorder(testPoints[0], &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
    polygon.projectPoint(testPoints[0], &result);
    ASSERT_EQ(result, Polygon2DResult::ERROR_INVALID_POLYGON_MIN_THREE_POINTS);
#endif
}

//! Helper function to test computation of convex polygon intersection
void testConvexPolygonIntersection(const Polygon2D<>& firstPolygon, const Polygon2D<>& secondPolygon, const Polygon2D<>& expectedIntersection, const bool& expectedReturnValue, const Polygon2DResult& expectedResult)
{
    // Initialize helpers
    Polygon2DResult result;

    // Create polygon storing the intersection
    Polygon2D<> intersectionPolygon;
    bool returnCode = intersectionPolygon.computeAsConvexPolygonIntersection(firstPolygon, secondPolygon, &result);
    ASSERT_EQ(result, expectedResult);
    ASSERT_TRUE(returnCode == expectedReturnValue);
    ASSERT_TRUE(intersectionPolygon.isEquivalent(expectedIntersection) == true);
}

//! Check computation of intersection of two convex polygons
TEST(Polygon2D, IntersectionOfConvexPolygons)
{
    // Initialize variables
    Polygon2D<> polygonA, polygonB, polygonC, polygonD, polygonE; // Polygons to test
    Polygon2D<> expectedPolygon; // Expected results
    Polygon2DVertex points[21]; // Point set
    points[0] << 1, 1;
    points[1] << -1, 1;
    points[2] << -1, -1;
    points[3] << 1, -1;
    points[4] << 0, 0;
    points[5] << 2, 0;
    points[6] << 2, 2;
    points[7] << 0, 2;
    points[8] << -1, -3;
    points[9] << 1, -3;
    points[10] << 3, -1;
    points[11] << -1, 3;
    points[12] << -2.5, 1.5;
    points[13] << 2.5, -3.5;
    points[14] << 4, -2;

    points[15] << 0, 1;
    points[16] << 1, 0;
    points[17] << -1, 0;
    points[18] << 0, -1;
    points[19] << 1, -2;
    points[20] << 1.5, -2.5;

    // Create polygon A
    polygonA.m_vertices.push_back(points[0]);
    polygonA.m_vertices.push_back(points[1]);
    polygonA.m_vertices.push_back(points[2]);
    polygonA.m_vertices.push_back(points[3]);

    // Create polygon B
    polygonB.m_vertices.push_back(points[4]);
    polygonB.m_vertices.push_back(points[5]);
    polygonB.m_vertices.push_back(points[6]);
    polygonB.m_vertices.push_back(points[7]);

    // Create polygon C
    polygonC.m_vertices.push_back(points[8]);
    polygonC.m_vertices.push_back(points[9]);
    polygonC.m_vertices.push_back(points[3]);
    polygonC.m_vertices.push_back(points[2]);

    // Create polygon D
    polygonD.m_vertices.push_back(points[2]);
    polygonD.m_vertices.push_back(points[8]);
    polygonD.m_vertices.push_back(points[9]);
    polygonD.m_vertices.push_back(points[10]);

    // Create polygon E
    polygonE.m_vertices.push_back(points[11]);
    polygonE.m_vertices.push_back(points[12]);
    polygonE.m_vertices.push_back(points[13]);
    polygonE.m_vertices.push_back(points[14]);

    // Check intersections of polygon A
    testConvexPolygonIntersection(polygonA, polygonA, polygonA, true, Polygon2DResult::SUCCESS);
    expectedPolygon.m_vertices.clear();
    expectedPolygon.m_vertices.push_back(points[4]);
    expectedPolygon.m_vertices.push_back(points[16]);
    expectedPolygon.m_vertices.push_back(points[0]);
    expectedPolygon.m_vertices.push_back(points[15]);
    testConvexPolygonIntersection(polygonA, polygonB, expectedPolygon, true, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonB, polygonA, expectedPolygon, true, Polygon2DResult::SUCCESS);
    expectedPolygon.m_vertices.clear();
    testConvexPolygonIntersection(polygonA, polygonC, expectedPolygon, false, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonC, polygonA, expectedPolygon, false, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonA, polygonD, expectedPolygon, false, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonD, polygonA, expectedPolygon, false, Polygon2DResult::SUCCESS);
    expectedPolygon.m_vertices.clear();
    expectedPolygon.m_vertices.push_back(points[18]);
    expectedPolygon.m_vertices.push_back(points[3]);
    expectedPolygon.m_vertices.push_back(points[0]);
    expectedPolygon.m_vertices.push_back(points[1]);
    expectedPolygon.m_vertices.push_back(points[17]);
    testConvexPolygonIntersection(polygonA, polygonE, expectedPolygon, true, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonE, polygonA, expectedPolygon, true, Polygon2DResult::SUCCESS);

    // Check intersections of polygon B
    testConvexPolygonIntersection(polygonB, polygonB, polygonB, true, Polygon2DResult::SUCCESS);
    expectedPolygon.m_vertices.clear();
    testConvexPolygonIntersection(polygonB, polygonC, expectedPolygon, false, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonC, polygonB, expectedPolygon, false, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonB, polygonD, expectedPolygon, false, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonD, polygonB, expectedPolygon, false, Polygon2DResult::SUCCESS);
    expectedPolygon.m_vertices.clear();
    expectedPolygon.m_vertices.push_back(points[4]);
    expectedPolygon.m_vertices.push_back(points[5]);
    expectedPolygon.m_vertices.push_back(points[7]);
    testConvexPolygonIntersection(polygonB, polygonE, expectedPolygon, true, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonE, polygonB, expectedPolygon, true, Polygon2DResult::SUCCESS);

    // Check intersections of polygon C
    testConvexPolygonIntersection(polygonC, polygonC, polygonC, true, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonC, polygonD, polygonC, true, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonD, polygonC, polygonC, true, Polygon2DResult::SUCCESS);
    expectedPolygon.m_vertices.clear();
    expectedPolygon.m_vertices.push_back(points[3]);
    expectedPolygon.m_vertices.push_back(points[18]);
    expectedPolygon.m_vertices.push_back(points[19]);
    testConvexPolygonIntersection(polygonC, polygonE, expectedPolygon, true, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonE, polygonC, expectedPolygon, true, Polygon2DResult::SUCCESS);

    // Check intersections of polygon D
    testConvexPolygonIntersection(polygonD, polygonD, polygonD, true, Polygon2DResult::SUCCESS);
    expectedPolygon.m_vertices.clear();
    expectedPolygon.m_vertices.push_back(points[10]);
    expectedPolygon.m_vertices.push_back(points[18]);
    expectedPolygon.m_vertices.push_back(points[20]);
    testConvexPolygonIntersection(polygonD, polygonE, expectedPolygon, true, Polygon2DResult::SUCCESS);
    testConvexPolygonIntersection(polygonE, polygonD, expectedPolygon, true, Polygon2DResult::SUCCESS);

    // Check intersections of polygon E
    testConvexPolygonIntersection(polygonE, polygonE, polygonE, true, Polygon2DResult::SUCCESS);
}

#endif // HAVE_EIGEN3
