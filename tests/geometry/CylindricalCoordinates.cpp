/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/geometry/CylindricalCoordinates.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace geometry;

//! Computes the "wrapped" difference between two angles
static inline double angleDifference(const double& first, const double& second)
{
    double difference = fmod(fabs(first - second), 2.0 * M_PI);
    if (difference > M_PI)
        difference -= M_PI;
    return difference;
}

//! Test construction and basic usage of DH parameters
TEST(CylindricalCoordinates, ConstructionBasicUsage)
{
    // Initialize helpers
    const double radiusMinimum = -2.0;
    const double radiusMaximum = 2.0;
    const size_t radiusStepCount = 10;
    const double radiusStepSize = (radiusMaximum - radiusMinimum) / (double)radiusStepCount;
    const double phiMinimum = -3.0 * M_PI;
    const double phiMaximum = 3.0 * M_PI;
    const size_t phiStepCount = 30;
    const double phiStepSize = (phiMaximum - phiMinimum) / (double)phiStepCount;
    const double zMinimum = -2.0;
    const double zMaximum = 2.0;
    const size_t zStepCount = 10;
    const double zStepSize = (zMaximum - zMinimum) / (double)zStepCount;

    // Default construction
    CylindricalCoordinates coordinatesDefault;
    ASSERT_EQ(coordinatesDefault.radius(), 0.0);
    ASSERT_EQ(coordinatesDefault.phi(), 0.0);
    ASSERT_EQ(coordinatesDefault.cosPhi(), cos(0.0));
    ASSERT_EQ(coordinatesDefault.sinPhi(), sin(0.0));
    ASSERT_EQ(coordinatesDefault.z(), 0.0);

    // Special construction and setters
    for (size_t i = 0; i <= radiusStepCount; i++) {
        const double radius = radiusMinimum + i * radiusStepSize;
        for (size_t j = 0; j <= phiStepCount; j++) {
            const double phi = phiMinimum + j * phiStepSize;
            for (size_t k = 0; k <= zStepCount; k++) {
                const double z = zMinimum + k * zStepSize;

                // Manually compute normalized version
                double radiusNormalized = radius;
                double phiNormalized = phi;
                double zNormalized = z;
                if (broccoli::core::isZero(radiusNormalized)) {
                    radiusNormalized = 0.0;
                    phiNormalized = 0.0;
                } else {
                    if (radiusNormalized < 0.0) {
                        radiusNormalized = -radiusNormalized;
                        phiNormalized += M_PI;
                    }
                    while (phiNormalized >= 2.0 * M_PI)
                        phiNormalized -= 2.0 * M_PI;
                    while (phiNormalized < 0.0)
                        phiNormalized += 2.0 * M_PI;
                }
                ASSERT_GE(radiusNormalized, 0.0);
                ASSERT_GE(phiNormalized, 0.0);
                ASSERT_LT(phiNormalized, 2.0 * M_PI);
                ASSERT_EQ(zNormalized, z);

                // Constructor
                const CylindricalCoordinates coordinatesConstructor(radius, phi, z);
                ASSERT_FLOAT_EQ(coordinatesConstructor.radius(), radius);
                ASSERT_FLOAT_EQ(coordinatesConstructor.phi(), phi);
                ASSERT_FLOAT_EQ(coordinatesConstructor.cosPhi(), cos(phi));
                ASSERT_FLOAT_EQ(coordinatesConstructor.sinPhi(), sin(phi));
                ASSERT_FLOAT_EQ(coordinatesConstructor.z(), z);

                // Check equality operators
                const CylindricalCoordinates same(radius, phi, z);
                ASSERT_TRUE(same == coordinatesConstructor);
                const CylindricalCoordinates otherRadius(radius + 0.1, phi, z);
                ASSERT_TRUE(otherRadius != coordinatesConstructor);
                const CylindricalCoordinates otherPhi(radius, phi + 0.1, z);
                ASSERT_TRUE(otherPhi != coordinatesConstructor);
                const CylindricalCoordinates otherZ(radius, phi, z + 0.1);
                ASSERT_TRUE(otherZ != coordinatesConstructor);

                // Constructor (normalized)
                CylindricalCoordinates coordinatesConstructorNormalized(radius, phi, z, true);
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.radius(), radiusNormalized);
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.phi(), phiNormalized);
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.cosPhi(), cos(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.sinPhi(), sin(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesConstructorNormalized.z(), zNormalized);

                // Check equivalence
                ASSERT_TRUE(coordinatesConstructor.isEquivalent(coordinatesConstructorNormalized));

                // Setter
                CylindricalCoordinates coordinatesSetter;
                coordinatesSetter.setRadius(radius);
                coordinatesSetter.setPhi(phi);
                coordinatesSetter.setZ(z);
                ASSERT_FLOAT_EQ(coordinatesSetter.radius(), radius);
                ASSERT_FLOAT_EQ(coordinatesSetter.phi(), phi);
                ASSERT_FLOAT_EQ(coordinatesSetter.cosPhi(), cos(phi));
                ASSERT_FLOAT_EQ(coordinatesSetter.sinPhi(), sin(phi));
                ASSERT_FLOAT_EQ(coordinatesSetter.z(), z);

                // Setter (normalized)
                CylindricalCoordinates coordinatesSetterNormalized;
                coordinatesSetterNormalized.setRadius(radius);
                coordinatesSetterNormalized.setPhi(phi);
                coordinatesSetterNormalized.setZ(z);
                coordinatesSetterNormalized.normalize();
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.radius(), radiusNormalized);
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.phi(), phiNormalized);
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.cosPhi(), cos(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.sinPhi(), sin(phiNormalized));
                ASSERT_FLOAT_EQ(coordinatesSetterNormalized.z(), zNormalized);

                // To cartesian
                const Eigen::Vector3d x(radiusNormalized * cos(phiNormalized), radiusNormalized * sin(phiNormalized), zNormalized);
                ASSERT_LE((x - coordinatesConstructorNormalized.toCartesian()).norm(), 1e-9);
                const Eigen::Vector3d x_dr(cos(phiNormalized), sin(phiNormalized), 0.0);
                ASSERT_LE((x_dr - coordinatesConstructorNormalized.toCartesianFirstDerivativeRadius()).norm(), 1e-9);
                const Eigen::Vector3d x_dr_normalized(cos(phiNormalized), sin(phiNormalized), 0.0);
                ASSERT_LE((x_dr_normalized - coordinatesConstructorNormalized.toCartesianFirstDerivativeRadiusNormalized()).norm(), 1e-9);
                const Eigen::Vector3d x_dphi(-radiusNormalized * sin(phiNormalized), radiusNormalized * cos(phiNormalized), 0.0);
                ASSERT_LE((x_dphi - coordinatesConstructorNormalized.toCartesianFirstDerivativePhi()).norm(), 1e-9);
                const Eigen::Vector3d x_dphi_normalized(-sin(phiNormalized), cos(phiNormalized), 0.0);
                ASSERT_LE((x_dphi_normalized - coordinatesConstructorNormalized.toCartesianFirstDerivativePhiNormalized()).norm(), 1e-9);
                const Eigen::Vector3d x_dz(0.0, 0.0, 1.0);
                ASSERT_LE((x_dz - coordinatesConstructorNormalized.toCartesianFirstDerivativeZ()).norm(), 1e-9);
                const Eigen::Vector3d x_dz_normalized(0.0, 0.0, 1.0);
                ASSERT_LE((x_dz_normalized - coordinatesConstructorNormalized.toCartesianFirstDerivativeZNormalized()).norm(), 1e-9);

                // From cartesian
                const CylindricalCoordinates coordinatesFromCartesian = CylindricalCoordinates::fromCartesian(CylindricalCoordinates(radius, phi, z).toCartesian());
                ASSERT_LE(fabs(coordinatesFromCartesian.radius() - radiusNormalized), 1e-9);
                ASSERT_LE(angleDifference(coordinatesFromCartesian.phi(), phiNormalized), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesian.cosPhi() - cos(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesian.sinPhi() - sin(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesian.z() - zNormalized), 1e-9);

                // From cartesian (normalized
                const CylindricalCoordinates coordinatesFromCartesianNormalized = CylindricalCoordinates::fromCartesian(CylindricalCoordinates(radiusNormalized, phiNormalized, zNormalized).toCartesian());
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.radius() - radiusNormalized), 1e-9);
                ASSERT_LE(angleDifference(coordinatesFromCartesianNormalized.phi(), phiNormalized), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.cosPhi() - cos(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.sinPhi() - sin(coordinatesFromCartesian.phi())), 1e-9);
                ASSERT_LE(fabs(coordinatesFromCartesianNormalized.z() - zNormalized), 1e-9);
            }
        }
    }
}

#endif // HAVE_EIGEN3
