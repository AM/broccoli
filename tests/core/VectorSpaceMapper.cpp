/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/core/VectorSpaceMapper.hpp"
#include "gtest/gtest.h"

using namespace broccoli::core;

TEST(VectorSpaceMapper, Initialization)
{
    VectorSpaceMapper<4, 3> mapper({ 0, 2, 3 });

    Eigen::Matrix<double, 3, 4> expectedResult;
    expectedResult << 1, 0, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1;

    EXPECT_EQ(expectedResult, mapper.selectionMatrix());
}

TEST(VectorSpaceMapper, SubSpaceFullSpaceConversion)
{
    VectorSpaceMapper<4, 3> mapper({ 0, 2, 3 });
    Eigen::Vector4d full = Eigen::Vector4d(1, 2, 3, 4);
    Eigen::Vector3d expectedSub = Eigen::Vector3d(1, 3, 4);
    Eigen::Vector4d expectedFull = Eigen::Vector4d(1, 0, 3, 4);

    EXPECT_EQ(expectedSub, mapper.subSpaceOf(full));
    EXPECT_EQ(expectedFull, mapper.fullSpaceOf(expectedSub));
}
