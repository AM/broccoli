/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/core/Time.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace core;

//! Test of time constructors and simplification
TEST(Time, ConstructorsAndSimplify)
{
    // Create time instances
    Time timeA;
    ASSERT_EQ(timeA.isZero(), true);
    Time timeB(1, -1);
    timespec timeSpecification;
    timeSpecification.tv_sec = -5;
    timeSpecification.tv_nsec = 300;
    Time timeC(timeSpecification);
    Time timeD(0.5);

    // Simplify times
    timeA.simplify();
    timeB.simplify();
    timeC.simplify();
    timeD.simplify();

    // Check members
    ASSERT_EQ(timeA.m_seconds, 0);
    ASSERT_EQ(timeA.m_nanoSeconds, 0);
    ASSERT_EQ(timeB.m_seconds, 0);
    ASSERT_EQ(timeB.m_nanoSeconds, 1e9 - 1);
    ASSERT_EQ(timeC.m_seconds, -4);
    ASSERT_EQ(timeC.m_nanoSeconds, 300 - 1e9);
    ASSERT_EQ(timeD.m_seconds, 0);
    ASSERT_EQ(timeD.m_nanoSeconds, 5e8);
}

//! Test of operators
TEST(Time, Operators)
{
    // Create time instances
    Time timeA(5, 4e8);
    Time timeB(1, 7e8);

    // Test addition
    Time timeC = timeA + timeB;
    ASSERT_EQ(timeC.m_seconds, 7);
    ASSERT_EQ(timeC.m_nanoSeconds, 1e8);

    // Test subtraction
    Time timeD = timeA - timeB;
    ASSERT_EQ(timeD.m_seconds, 3);
    ASSERT_EQ(timeD.m_nanoSeconds, 7e8);

    // Test multiplication
    Time timeAScalarMultiRight = timeA * 1.5;
    Time timeAScalarMultiLeft = 1.5 * timeA;
    ASSERT_EQ(timeAScalarMultiRight.m_seconds, 8);
    ASSERT_EQ(timeAScalarMultiRight.m_nanoSeconds, 1e8);
    ASSERT_EQ(timeAScalarMultiLeft.m_seconds, 8);
    ASSERT_EQ(timeAScalarMultiLeft.m_nanoSeconds, 1e8);

    // Test division
    Time timeAScalarDivRight = timeA / 1.5;
    ASSERT_EQ(timeAScalarDivRight.m_seconds, 3);
    ASSERT_EQ(timeAScalarDivRight.m_nanoSeconds, 6e8);

    // Test equality
    Time timeE(4, 1e9 + 4e8);
    Time timeF(-2, 5e8);
    Time timeG(-2 - 2, 5e8 + 2e9);
    ASSERT_EQ(timeA == timeB, false);
    ASSERT_EQ(timeA == timeA, true);
    ASSERT_EQ(timeA == timeE, true);
    ASSERT_EQ(timeA == timeF, false);
    ASSERT_EQ(timeF == timeG, true);

    // Test inequality
    ASSERT_EQ(timeA != timeB, true);
    ASSERT_EQ(timeA != timeA, false);
    ASSERT_EQ(timeA != timeE, false);
    ASSERT_EQ(timeA != timeF, true);
    ASSERT_EQ(timeF != timeG, false);

    // Test greater than
    ASSERT_EQ(timeA > timeB, true);
    ASSERT_EQ(timeB > timeA, false);
    ASSERT_EQ(timeA > timeA, false);
    ASSERT_EQ(timeF > timeG, false);

    // Test greater or equal
    ASSERT_EQ(timeA >= timeB, true);
    ASSERT_EQ(timeB >= timeA, false);
    ASSERT_EQ(timeA >= timeA, true);
    ASSERT_EQ(timeF >= timeG, true);

    // Test lesser than
    ASSERT_EQ(timeA < timeB, false);
    ASSERT_EQ(timeB < timeA, true);
    ASSERT_EQ(timeA < timeA, false);
    ASSERT_EQ(timeF < timeG, false);

    // Test lesser or equal
    ASSERT_EQ(timeA <= timeB, false);
    ASSERT_EQ(timeB <= timeA, true);
    ASSERT_EQ(timeA <= timeA, true);
    ASSERT_EQ(timeF <= timeG, true);

    // Enhanced cross-checking with double
    for (int64_t secA = -5; secA < 5; secA++) {
        for (int64_t nanoSecA = -5e8; nanoSecA < 5e8; nanoSecA = nanoSecA + 1e8) {
            for (int64_t secB = -5; secB < 5; secB++) {
                for (int64_t nanoSecB = -5e8; nanoSecB < 5e8; nanoSecB = nanoSecB + 1e8) {
                    // Setup time objects
                    Time timeA(secA, nanoSecA);
                    Time timeB(secB, nanoSecB);

                    // Setup doubles for comparison
                    double refA = secA + 1e-9 * nanoSecA;
                    double refB = secB + 1e-9 * nanoSecB;

                    // Test operators
                    ASSERT_EQ(timeA == timeB, refA == refB);
                    ASSERT_EQ(timeA != timeB, refA != refB);
                    ASSERT_EQ(timeA < timeB, refA < refB);
                    ASSERT_EQ(timeA > timeB, refA > refB);
                    ASSERT_EQ(timeA <= timeB, refA <= refB);
                    ASSERT_EQ(timeA >= timeB, refA >= refB);
                }
            }
        }
    }

    // Inline addition
    Time timeX(1, 1e8);
    Time timeY(0, 1e8);
    timeX += 0.01;
    ASSERT_EQ(timeX, Time(1, 11e7));
    timeX += timeY;
    ASSERT_EQ(timeX, Time(1, 21e7));

    // Inline subtraction
    timeX = Time(1, 1e8);
    timeY = Time(0, 1e7);
    timeX -= 0.01;
    ASSERT_EQ(timeX, Time(1, 9e7));
    timeX -= timeY;
    ASSERT_EQ(timeX, Time(1, 8e7));

    // Inline multiplication
    timeX = Time(1, 1e8);
    timeX *= 2.0;
    ASSERT_EQ(timeX, Time(2, 2e8));

    // Inline division
    timeX = Time(2, 2e8);
    timeX /= 2.0;
    ASSERT_EQ(timeX, Time(1, 1e8));
}

//! Test of scalar operators
TEST(Time, ScalarOperators)
{
    // Create time instances
    Time timeA(5, 4e8);
    Time timeB(1, 7e8);
    Time timeX(2, 5e8);

    // Test addition
    Time timeC = timeA + 2.1;
    Time timeD = 2.1 + timeA;
    ASSERT_EQ(timeC.m_seconds, 7);
    ASSERT_EQ(timeC.m_nanoSeconds, 5e8);
    ASSERT_EQ(timeD.m_seconds, 7);
    ASSERT_EQ(timeD.m_nanoSeconds, 5e8);
    ASSERT_EQ(timeC == timeD, true);

    // Test subtraction
    timeC = timeA - 2.1;
    timeD = 2.1 - timeB;
    ASSERT_EQ(timeC.m_seconds, 3);
    ASSERT_EQ(timeC.m_nanoSeconds, 3e8);
    ASSERT_EQ(timeD.m_seconds, 0);
    ASSERT_EQ(timeD.m_nanoSeconds, 4e8);

    // Test multiplication
    timeC = timeA * 2.5;
    timeD = 2.5 * timeA;
    ASSERT_EQ(timeC.m_seconds, 13);
    ASSERT_EQ(timeC.m_nanoSeconds, 5e8);
    ASSERT_EQ(timeD.m_seconds, 13);
    ASSERT_EQ(timeD.m_nanoSeconds, 5e8);
    ASSERT_EQ(timeC == timeD, true);

    // Test division
    timeC = timeA / 1.5;
    timeD = 5.0 / timeX;
    ASSERT_EQ(timeC.m_seconds, 3);
    ASSERT_EQ(timeC.m_nanoSeconds, 6e8);
    ASSERT_EQ(timeD.m_seconds, 2);
    ASSERT_EQ(timeD.m_nanoSeconds, 0);

    // Test equality
    ASSERT_EQ(timeA == 5.4, true);
    ASSERT_EQ(5.4 == timeA, true);
    ASSERT_EQ(timeA == 5.2, false);
    ASSERT_EQ(5.2 == timeA, false);

    // Test inequality
    ASSERT_EQ(timeA != 5.4, false);
    ASSERT_EQ(5.4 != timeA, false);
    ASSERT_EQ(timeA != 5.2, true);
    ASSERT_EQ(5.2 != timeA, true);

    // Test greater than
    ASSERT_EQ(timeA > 5.3, true);
    ASSERT_EQ(timeA > 5.4, false);
    ASSERT_EQ(timeA > 5.5, false);
    ASSERT_EQ(timeA > -6.0, true);
    ASSERT_EQ(5.3 > timeA, false);
    ASSERT_EQ(5.4 > timeA, false);
    ASSERT_EQ(5.5 > timeA, true);
    ASSERT_EQ(-6.0 > timeA, false);

    // Test greater-or-equal
    ASSERT_EQ(timeA >= 5.3, true);
    ASSERT_EQ(timeA >= 5.4, true);
    ASSERT_EQ(timeA >= 5.5, false);
    ASSERT_EQ(timeA >= -6.0, true);
    ASSERT_EQ(5.3 >= timeA, false);
    ASSERT_EQ(5.4 >= timeA, true);
    ASSERT_EQ(5.5 >= timeA, true);
    ASSERT_EQ(-6.0 >= timeA, false);

    // Test lesser than
    ASSERT_EQ(timeA < 5.3, false);
    ASSERT_EQ(timeA < 5.4, false);
    ASSERT_EQ(timeA < 5.5, true);
    ASSERT_EQ(timeA < -6.0, false);
    ASSERT_EQ(5.3 < timeA, true);
    ASSERT_EQ(5.4 < timeA, false);
    ASSERT_EQ(5.5 < timeA, false);
    ASSERT_EQ(-6.0 < timeA, true);

    // Test lesser-or-equal
    ASSERT_EQ(timeA <= 5.3, false);
    ASSERT_EQ(timeA <= 5.4, true);
    ASSERT_EQ(timeA <= 5.5, true);
    ASSERT_EQ(timeA <= -6.0, false);
    ASSERT_EQ(5.3 <= timeA, true);
    ASSERT_EQ(5.4 <= timeA, true);
    ASSERT_EQ(5.5 <= timeA, false);
    ASSERT_EQ(-6.0 <= timeA, true);
}

//! Test of conversion to timespec
TEST(Time, toTimeSpec)
{
    // Setup time objects
    Time timeA(5, -1e8);
    Time timeB(-2, 5e8);
    timeA.simplify();
    timeB.simplify();

    // Compute time specs
    timespec timespecA = timeA.toTimeSpec();
    timespec timespecB = timeB.toTimeSpec();

    // Compare
    ASSERT_EQ(timespecA.tv_sec, 4);
    ASSERT_EQ(timespecA.tv_nsec, 9e8);
    ASSERT_EQ(timespecB.tv_sec, -1);
    ASSERT_EQ(timespecB.tv_nsec, -5e8);
}

//! Test of conversion to double
TEST(Time, toDouble)
{
    // Setup time objects
    Time timeA(5, -1e8);
    Time timeB(-2, 5e8);

    // Compare
    ASSERT_NEAR(timeA.toDouble(), 4.9, 1e-10);
    ASSERT_NEAR(timeB.toDouble(), -1.5, 1e-10);
}

//! Test of sign detection
TEST(Time, isPositive)
{
    // Setup time objects
    Time timeA(5, -1e8);
    Time timeB(-2, 5e8);

    // Compare
    ASSERT_EQ(timeA.isPositive(), true);
    ASSERT_EQ(timeB.isPositive(), false);
}

//! Test of encoding as duration
TEST(Time, encodeToDurationString)
{
    // Setup time object
    Time timeA(-5, 2e8);
    Time timeB(2, -2e8);

    // Convert to strings
    std::string timeAstring = timeA.encodeToDurationString(4, ',');
    std::string timeBstring = timeB.encodeToDurationString(0);

    // Check for empty strings (error occured)
    ASSERT_GT(timeAstring.length(), 0);
    ASSERT_GT(timeBstring.length(), 0);

    // Compare strings
    std::string refAstring = "-4,8000";
    std::string refBstring = "+1";
    ASSERT_EQ(timeAstring == refAstring, true);
    ASSERT_EQ(timeBstring == refBstring, true);
    ASSERT_EQ(timeAstring == timeBstring, false);
}

//! Test of encoding as date time
TEST(Time, encodeToDateTimeString)
{
    // Setup time object
    Time timeA(5, 2e8);
    Time timeB(2, -2e8);

    // Convert to strings
    std::string timeAstring = timeA.encodeToDateTimeString(false, "%Y-%m-%d_%H-%M-%S", 4, ',');
    std::string timeBstring = timeB.encodeToDateTimeString(false, "%Y-%m-%d_%H-%M-%S", 0);

    // Check for empty strings (error occured)
    ASSERT_GT(timeAstring.length(), 0);
    ASSERT_GT(timeBstring.length(), 0);

    // Compare strings
    std::string refAstring = "1970-01-01_00-00-05,2000";
    std::string refBstring = "1970-01-01_00-00-01";
    ASSERT_EQ(timeAstring == refAstring, true);
    ASSERT_EQ(timeBstring == refBstring, true);
    ASSERT_EQ(timeAstring == timeBstring, false);
}

//! Test of getting current time
TEST(Time, currentTime)
{
    // Obtain current time
    Time currentTimeA = Time::currentTime();
    Time currentTimeB = Time::currentTime();

    // Check if members are non-zero
    ASSERT_EQ((currentTimeA.m_seconds == 0 && currentTimeA.m_nanoSeconds == 0), false);
    ASSERT_EQ((currentTimeB.m_seconds == 0 && currentTimeB.m_nanoSeconds == 0), false);

    // Output to console
    std::cout << "Current date and time: " << currentTimeA.encodeToDateTimeString(true, "%Y-%m-%d_%H-%M-%S", 9) << " (" << currentTimeA.encodeToDurationString(9) << "s from Unix Epoch)" << std::endl;
    std::cout << "Current date and time: " << currentTimeB.encodeToDateTimeString(true, "%Y-%m-%d_%H-%M-%S", 9) << " (" << currentTimeB.encodeToDurationString(9) << "s from Unix Epoch)" << std::endl;
}

//! Test of getting clock resolution
TEST(Time, clockResolution)
{
    // Obtain clock resolution
    Time resolutionTheory = Time::getClockResolution();
    Time resolutionReal = Time::evaluateClockResolution();

    // Output to console
    std::cout << "Clock resolution (theory): " << resolutionTheory.encodeToDurationString(9) << "s" << std::endl;
    std::cout << "Clock resolution (real): " << resolutionReal.encodeToDurationString(9) << "s" << std::endl;
}

//! Test sleeping function
TEST(Time, Sleep)
{
    // Measure start time
    Time startTime = Time::currentTime();

    // Sleep
    Time::sleep(0.01);

    // Measure end time
    Time endTime = Time::currentTime();

    // Check if minimum sleep time was respected
    ASSERT_EQ(endTime - startTime >= 0.01, true);

    // Test minimum sleep duration for non-blocking mode
    auto nonBlockingSleepStart = Time::currentTime();
    Time::sleep(Time(0, 1), false);
    auto nonBlockingSleepEnd = Time::currentTime();
    std::cout << "minimum non-blocking sleep time: " << (nonBlockingSleepEnd - nonBlockingSleepStart).toDouble() << "s\n";

    // Test minimum sleep duration for blocking mode
    auto blockingSleepStart = Time::currentTime();
    Time::sleep(Time(0, 1), true);
    auto blockingSleepEnd = Time::currentTime();
    std::cout << "minimum blocking sleep time: " << (blockingSleepEnd - blockingSleepStart).toDouble() << "s\n";
}
