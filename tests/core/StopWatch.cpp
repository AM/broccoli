/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/core/StopWatch.hpp"
#include "broccoli/core/platform/PlatformHelperFactory.hpp"
#include "gtest/gtest.h"

using namespace broccoli::core;

TEST(StopWatch, tictoc)
{
    const auto platformHelper = PlatformHelperFactory::create();
    StopWatch watch;
    EXPECT_DOUBLE_EQ(watch.elapsedTime(), 0.0);

    watch.tic();
    platformHelper->waitFor(0, 100000);
    double result = watch.toc();

    ASSERT_TRUE(watch.elapsedTime() > 0.0);
    EXPECT_TRUE(watch.elapsedTime() == result);
}

TEST(StopWatch, startstop)
{
    const auto platformHelper = PlatformHelperFactory::create();
    StopWatch watch;
    EXPECT_DOUBLE_EQ(watch.elapsedTime(), 0.0);

    watch.start();
    platformHelper->waitFor(0, 100000);
    double result = watch.stop();

    ASSERT_TRUE(watch.elapsedTime() > 0.0);
    EXPECT_TRUE(watch.elapsedTime() == result);
}

TEST(StopWatch, Statistics)
{
    const auto platformHelper = PlatformHelperFactory::create();
    StopWatchStatistical watch;
    EXPECT_DOUBLE_EQ(watch.elapsedTime(), 0.0);
    EXPECT_DOUBLE_EQ(watch.statistics().mean(), 0.0);

    watch.start();
    platformHelper->waitFor(0, 100000);
    watch.stop();

    ASSERT_TRUE(watch.elapsedTime() > 0.0);
    EXPECT_DOUBLE_EQ(watch.elapsedTime(), watch.statistics().mean());

    watch.resetStatistics();
    EXPECT_DOUBLE_EQ(watch.statistics().mean(), 0.0);
}
