

#
# Executable name and options
#

# Target name
set(target core)
message(STATUS "Test ${target}")

#
# Dependencies
#
find_package(broccoli QUIET COMPONENTS eigen)

if (NOT broccoli_FOUND)
    message("Skipping test ${target} - dependencies not installed.")
    return()
endif()

#
# Sources
#

# Add your headers and sources here
set(headers
)

set(sources
    CycleTimer.cpp
    EnumerableState.cpp
    FixedStepArithmeticNumber.cpp
    floats.cpp
    math.cpp
    Nameable.cpp
    Time.cpp
    string.cpp
    PlatformHelper.cpp
    StatisticsEstimator.cpp
    StopWatch.cpp
    type_traits.cpp
    utils.cpp
    VectorSpaceMapper.cpp)

# Group source files
set(header_group "Header Files (API)")
set(source_group "Source Files")
source_group_by_path(${CMAKE_CURRENT_SOURCE_DIR} "\\\\.h$|\\\\.hpp$"
        ${header_group} ${headers})
source_group_by_path(${CMAKE_CURRENT_SOURCE_DIR}  "\\\\.cpp$|\\\\.c$|\\\\.h$|\\\\.hpp$"
        ${source_group} ${sources})

#
# Create executable
#

# Build executable
add_executable(${target}
    ${sources}
    ${headers}
)


# Create namespaced alias
add_executable(${META_PROJECT_NAME}::${target} ALIAS ${target})


#
# Project options
#

set_target_properties(${target}
    PROPERTIES
    ${DEFAULT_PROJECT_OPTIONS}
    FOLDER "${IDE_FOLDER}"
)


#
# Include directories
#

target_include_directories(${target}
    PRIVATE
    ${DEFAULT_INCLUDE_DIRECTORIES}
)


#
# Libraries
#

target_link_libraries(${target}
    PRIVATE
    ${DEFAULT_LIBRARIES}
    eat::broccoli
    gtest_main
)


#
# Compile definitions
#

target_compile_definitions(${target}
    PRIVATE
    ${DEFAULT_COMPILE_DEFINITIONS}
)


#
# Compile options
#

target_compile_options(${target}
    PRIVATE
    ${DEFAULT_COMPILE_OPTIONS}
)


#
# Linker options
#

target_link_libraries(${target}
    PRIVATE
    ${DEFAULT_LINKER_OPTIONS}
)
