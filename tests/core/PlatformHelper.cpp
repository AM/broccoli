/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/platform/PlatformHelperFactory.hpp"
#include "gtest/gtest.h"

using namespace broccoli::core;

TEST(PlatformHelper, threads)
{
    const auto platform = PlatformHelperFactory::create();
    platform->setThreadName("test");
    platform->setThreadPriority(10);
}

TEST(PlatformHelper, Scheduling)
{
    const auto platform = PlatformHelperFactory::create();
    platform->setCpuAffinity(0);
    platform->waitFor(0, 1000000ULL);
    timespec delay;
    delay.tv_sec = 0;
    delay.tv_nsec = 100000ULL;
    platform->waitFor(delay);
}

TEST(PlatformHelper, Endianness)
{
    const auto platform = PlatformHelperFactory::create();
    const bool littleEndian = platform->usesLittleEndian();
    uint16_t testVariable = 0x00FF;
    ASSERT_TRUE(littleEndian == ((*((uint8_t*)&testVariable) == 0xFF)));
}
