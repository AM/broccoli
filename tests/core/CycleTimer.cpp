/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/core/CycleTimer.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace core;
using namespace ::testing;

TEST(CycleTimer, cycle)
{
    CycleTimer timer;
    timer.setDuration(3, 1);

    EXPECT_FALSE(timer.cycle());
    EXPECT_FALSE(timer.cycle());
    EXPECT_TRUE(timer.cycle());
    EXPECT_FALSE(timer.cycle());
    EXPECT_FALSE(timer.cycle());
    EXPECT_TRUE(timer.cycle());
}

TEST(CycleTimer, reset)
{
    CycleTimer timer;
    timer.setDuration(2, 1);

    EXPECT_FALSE(timer.cycle());
    timer.reset();
    EXPECT_FALSE(timer.cycle());
}
