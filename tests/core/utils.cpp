/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/utils.hpp"
#include "gtest/gtest.h"

using namespace broccoli::core;

struct NotCopyable {
    int a;
    int b;

    explicit NotCopyable(int avalue, int bvalue)
        : a(avalue)
        , b(bvalue)
    {
    }

    NotCopyable(const NotCopyable&) = delete;
    NotCopyable& operator=(const NotCopyable&) = delete;

    static implicit_construct<NotCopyable> create()
    {
        return { 10, 20 };
    }
};

TEST(utils, implicitConstruct)
{
    auto&& notCopyable = NotCopyable::create();
    EXPECT_EQ(notCopyable.a, 10);
    EXPECT_EQ(notCopyable.b, 20);
}

TEST(utils, reverseMemcpy)
{
    char a[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    char b[8];

    reverse_memcpy(b, a, 2);

    EXPECT_EQ(b[0], 2);
    EXPECT_EQ(b[1], 1);

    reverse_memcpy(b, a, 8);
    EXPECT_EQ(b[0], 8);
    EXPECT_EQ(b[1], 7);
    EXPECT_EQ(b[2], 6);
    EXPECT_EQ(b[3], 5);
    EXPECT_EQ(b[4], 4);
    EXPECT_EQ(b[5], 3);
    EXPECT_EQ(b[6], 2);
    EXPECT_EQ(b[7], 1);
}
