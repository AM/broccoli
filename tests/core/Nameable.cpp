/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/core/Nameable.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace core;
using namespace ::testing;

TEST(Nameable, Construction)
{
    Nameable obj;
    EXPECT_EQ(obj.name(), "");

    Nameable another("hello");
EXPECT_EQ(another.name(), "hello");

    another.setName("world");
    EXPECT_EQ(another.name(), "world");
}

TEST(Nameable, prefix)
{
    Nameable obj("hello");
    EXPECT_EQ(obj.prefixName("world"), "hello world");
    EXPECT_EQ(obj.prefixName("world", "."), "hello.world");
}
