/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/floats.hpp"
#include "gtest/gtest.h"
#include "broccoli/core/FixedStepArithmeticNumber.hpp"

using namespace broccoli::core;

//! Test construction of fixed-step arithmetic numbers
TEST(FixedStepArithmeticNumber, Constructors)
{
    FixedStepArithmeticNumber<int32_t> number1(0.1);
    EXPECT_EQ(number1.integer(), 1);
    EXPECT_DOUBLE_EQ(number1.toDouble(), 0.1);

    FixedStepArithmeticNumber<int32_t> number2((int32_t) 0, 0.1);
    EXPECT_EQ(number2.integer(), 0);
    EXPECT_DOUBLE_EQ(number2.stepSize(), 0.1);

    FixedStepArithmeticNumber<int32_t> number3(0.2, 0.1);
    EXPECT_EQ(number3.integer(), 2);
    EXPECT_DOUBLE_EQ(number3.stepSize(), 0.1);

    FixedStepArithmeticNumber<int32_t> number4(0.0);
    EXPECT_EQ(number4.integer(), 0);
    EXPECT_EQ(number4.stepSize(), 1.0);
}

//! Test increment/decrement operators
TEST(FixedStepArithmeticNumber, IncrementDecrement)
{
    FixedStepArithmeticNumber<int32_t> number(0.0, 0.1);

    EXPECT_EQ(number++.integer(), 0);
    EXPECT_EQ(number.integer(), 1);

    EXPECT_EQ((++number).integer(), 2);

    EXPECT_EQ((--number).integer(), 1);

    EXPECT_EQ(number--.integer(), 1);
    EXPECT_EQ(number.integer(), 0);

    EXPECT_EQ(number.next().integer(), 1);
    EXPECT_EQ(number.previous().integer(), -1);
}

//! Test conversion operators / methods
TEST(FixedStepArithmeticNumber, Conversion)
{
    FixedStepArithmeticNumber<int64_t> number(32343323.02, 0.01);

    EXPECT_DOUBLE_EQ(number.toDouble(), 32343323.02);
    EXPECT_DOUBLE_EQ((double) number, 32343323.02);

    number.fromDouble(2.4);
    EXPECT_DOUBLE_EQ(number.toDouble(), 2.4);
    EXPECT_EQ((int64_t)number, 240);
}

//! Test comparison operators
TEST(FixedStepArithmeticNumber, Comparison)
{
    FixedStepArithmeticNumber<int64_t> number(2.4, 0.01);

    // pertubate the double value by 4 ULPs
    internal::IntegerUnionMatchingType<double> doubleUnion(2.4);
    doubleUnion.asInteger += 4;

    EXPECT_FALSE(number.toDouble() == doubleUnion.value); //unsafe
    EXPECT_TRUE(number == doubleUnion.value); // safe
    EXPECT_FALSE(number != doubleUnion.value);

    EXPECT_FALSE(number < 2.4);
    EXPECT_FALSE(number > 2.4);
    EXPECT_TRUE(number >= 2.4);
    EXPECT_TRUE(number <= 2.4);

    number.fromDouble(3.0);
    EXPECT_TRUE(number >= 2.4);
    EXPECT_TRUE(number >= 2.4);
}
