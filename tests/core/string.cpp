/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/string.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace core;

//! Test split()
TEST(string, split)
{
    // Test empty string
    const std::string stringA = "";
    std::vector<std::string> expectedResultADontSkipEmpty;
    expectedResultADontSkipEmpty.push_back("");
    std::vector<std::string> expectedResultASkipEmpty;
    ASSERT_TRUE(expectedResultADontSkipEmpty == stringSplit(stringA, '|', false));
    ASSERT_TRUE(expectedResultASkipEmpty == stringSplit(stringA, '|', true));

    // Test single character string
    const std::string stringB = "a";
    std::vector<std::string> expectedResultBDontSkipEmpty;
    expectedResultBDontSkipEmpty.push_back("a");
    std::vector<std::string> expectedResultBSkipEmpty;
    expectedResultBSkipEmpty.push_back("a");
    ASSERT_TRUE(expectedResultBDontSkipEmpty == stringSplit(stringB, '|', false));
    ASSERT_TRUE(expectedResultBSkipEmpty == stringSplit(stringB, '|', true));

    // Test single delimiter
    const std::string stringC = "|";
    std::vector<std::string> expectedResultCDontSkipEmpty;
    expectedResultCDontSkipEmpty.push_back("");
    expectedResultCDontSkipEmpty.push_back("");
    std::vector<std::string> expectedResultCSkipEmpty;
    ASSERT_TRUE(expectedResultCDontSkipEmpty == stringSplit(stringC, '|', false));
    ASSERT_TRUE(expectedResultCSkipEmpty == stringSplit(stringC, '|', true));

    // Test beginning delimiter
    const std::string stringD = "|abc";
    std::vector<std::string> expectedResultDDontSkipEmpty;
    expectedResultDDontSkipEmpty.push_back("");
    expectedResultDDontSkipEmpty.push_back("abc");
    std::vector<std::string> expectedResultDSkipEmpty;
    expectedResultDSkipEmpty.push_back("abc");
    ASSERT_TRUE(expectedResultDDontSkipEmpty == stringSplit(stringD, '|', false));
    ASSERT_TRUE(expectedResultDSkipEmpty == stringSplit(stringD, '|', true));

    // Test ending delimiter
    const std::string stringE = "def|";
    std::vector<std::string> expectedResultEDontSkipEmpty;
    expectedResultEDontSkipEmpty.push_back("def");
    expectedResultEDontSkipEmpty.push_back("");
    std::vector<std::string> expectedResultESkipEmpty;
    expectedResultESkipEmpty.push_back("def");
    ASSERT_TRUE(expectedResultEDontSkipEmpty == stringSplit(stringE, '|', false));
    ASSERT_TRUE(expectedResultESkipEmpty == stringSplit(stringE, '|', true));

    // Test beginning and ending delimiter
    const std::string stringF = "|abc|def|";
    std::vector<std::string> expectedResultFDontSkipEmpty;
    expectedResultFDontSkipEmpty.push_back("");
    expectedResultFDontSkipEmpty.push_back("abc");
    expectedResultFDontSkipEmpty.push_back("def");
    expectedResultFDontSkipEmpty.push_back("");
    std::vector<std::string> expectedResultFSkipEmpty;
    expectedResultFSkipEmpty.push_back("abc");
    expectedResultFSkipEmpty.push_back("def");
    ASSERT_TRUE(expectedResultFDontSkipEmpty == stringSplit(stringF, '|', false));
    ASSERT_TRUE(expectedResultFSkipEmpty == stringSplit(stringF, '|', true));

    // Test interior delimiter
    const std::string stringG = "abc|def|ghi";
    std::vector<std::string> expectedResultGDontSkipEmpty;
    expectedResultGDontSkipEmpty.push_back("abc");
    expectedResultGDontSkipEmpty.push_back("def");
    expectedResultGDontSkipEmpty.push_back("ghi");
    std::vector<std::string> expectedResultGSkipEmpty;
    expectedResultGSkipEmpty.push_back("abc");
    expectedResultGSkipEmpty.push_back("def");
    expectedResultGSkipEmpty.push_back("ghi");
    ASSERT_TRUE(expectedResultGDontSkipEmpty == stringSplit(stringG, '|', false));
    ASSERT_TRUE(expectedResultGSkipEmpty == stringSplit(stringG, '|', true));

    // Test interior empty
    const std::string stringH = "abc||def|ghi";
    std::vector<std::string> expectedResultHDontSkipEmpty;
    expectedResultHDontSkipEmpty.push_back("abc");
    expectedResultHDontSkipEmpty.push_back("");
    expectedResultHDontSkipEmpty.push_back("def");
    expectedResultHDontSkipEmpty.push_back("ghi");
    std::vector<std::string> expectedResultHSkipEmpty;
    expectedResultHSkipEmpty.push_back("abc");
    expectedResultHSkipEmpty.push_back("def");
    expectedResultHSkipEmpty.push_back("ghi");
    ASSERT_TRUE(expectedResultHDontSkipEmpty == stringSplit(stringH, '|', false));
    ASSERT_TRUE(expectedResultHSkipEmpty == stringSplit(stringH, '|', true));
}

//! Test startsWith()
TEST(string, startsWith)
{
    const std::string baseString = "test";
    ASSERT_TRUE(stringStartsWith(baseString, "te"));
    ASSERT_FALSE(stringStartsWith(baseString, "es"));
    ASSERT_FALSE(stringStartsWith(baseString, "st"));
    ASSERT_FALSE(stringStartsWith(baseString, "testX"));
}

//! Test endsWith()
TEST(string, endsWith)
{
    const std::string baseString = "test";
    ASSERT_TRUE(stringEndsWith(baseString, "st"));
    ASSERT_FALSE(stringEndsWith(baseString, "te"));
    ASSERT_FALSE(stringEndsWith(baseString, "es"));
    ASSERT_FALSE(stringEndsWith(baseString, "Xtest"));
}
