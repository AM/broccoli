/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/floats.hpp"
#include "gtest/gtest.h"

using namespace broccoli::core;

//! Test isEqual()
TEST(floats, isEqual)
{
    EXPECT_FALSE(isEqual(0.0000000000000022204460492503131, 0.0, 4, 2));
    EXPECT_TRUE(isEqual(std::numeric_limits<double>::epsilon(), 0.0, 4, 2));

    internal::IntegerUnionMatchingType<double> doubleUnion(32343323.02);
    doubleUnion.asInteger += 4;

    EXPECT_FALSE(isEqual(doubleUnion.value, 32343323.02, 0, 2)); // Just using epsilon
    EXPECT_TRUE(isEqual(doubleUnion.value, 32343323.02, 4, 2)); // BUT: Epsilon is NOT enough for big numbers!

    EXPECT_FALSE(isEqual(doubleUnion.value, -32343323.02));
    EXPECT_FALSE(isEqual(nan(""), 1.0));

    internal::IntegerUnionMatchingType<double> minusInteger(-1.0);
    internal::IntegerUnionMatchingType<double> plusZero(0.0);
    plusZero.asInteger = 1;

    ASSERT_LT(minusInteger.asInteger, 0);
    ASSERT_GT(plusZero.asInteger, 0);
    EXPECT_FALSE(isEqual(minusInteger.value, plusZero.value, 4, 0));
    EXPECT_TRUE(isEqual(-0.0, 0.0, 4, 0));
}

TEST(floats, isZero)
{
    EXPECT_FALSE(isZero(0.0000000000000022204460492503131, 2));
    EXPECT_TRUE(isZero(std::numeric_limits<double>::epsilon(), 2));
    EXPECT_TRUE(isZero(-0.0));
    EXPECT_FALSE(isZero(nan("")));
}

TEST(floats, isEqualEigen)
{
    internal::IntegerUnionMatchingType<double> doubleUnion(32343323.02);

    Eigen::Vector3d reference = Eigen::Vector3d::Identity();
    Eigen::Vector3d modified = Eigen::Vector3d::Identity();

    EXPECT_TRUE(isEqual(reference, modified));

    reference(0) = doubleUnion.value;
    doubleUnion.asInteger += 4;
    modified(0) = doubleUnion.value;

    EXPECT_FALSE(isEqual(reference, modified, 0, 2));
    EXPECT_TRUE(isEqual(reference, modified, 4, 2));
}

//! Test isLess(), isEqualOrLess(), isGreater(), isEqualOrGreater()
TEST(floats, notEqual)
{
    double NotANumber = nan("");
    EXPECT_FALSE(isLess(NotANumber, 1.0));
    EXPECT_FALSE(isLessOrEqual(NotANumber, 1.0));

    EXPECT_TRUE(isLess(1.0, 2.0));
    EXPECT_FALSE(isLess(1.0, 1.0));

    EXPECT_TRUE(isLessOrEqual(1.0, 2.0));
    EXPECT_TRUE(isLessOrEqual(1.0, 1.0));

    EXPECT_FALSE(isGreater(nan(""), 1.0));
    EXPECT_FALSE(isGreaterOrEqual(nan(""), 1.0));

    EXPECT_TRUE(isGreater(2.0, 1.0));
    EXPECT_FALSE(isGreater(1.0, 1.0));

    EXPECT_TRUE(isGreaterOrEqual(2.0, 1.0));
    EXPECT_TRUE(isGreaterOrEqual(1.0, 1.0));
}
