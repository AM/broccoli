/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/StatisticsEstimator.hpp"
#include "gtest/gtest.h"
#include <Eigen/Dense>
#include <array>

using namespace broccoli::core;

namespace {

TEST(StatisticsEstimator, Constructor)
{
    StatisticsEstimator<double> doubleEstimator;
    EXPECT_DOUBLE_EQ(doubleEstimator.mean(), 0.0);
    EXPECT_DOUBLE_EQ(doubleEstimator.standardDeviation(), 0.0);
    EXPECT_DOUBLE_EQ(doubleEstimator.minimum(), 0.0);
    EXPECT_DOUBLE_EQ(doubleEstimator.maximum(), 0.0);
    EXPECT_EQ(doubleEstimator.numberOfSamples(), 0);

    StatisticsEstimator<Eigen::Vector2d> vectorEstimator(Eigen::Vector2d::Ones());
    EXPECT_EQ(vectorEstimator.mean(), Eigen::Vector2d::Ones());
    EXPECT_EQ(vectorEstimator.standardDeviation(), Eigen::Vector2d::Ones());
    EXPECT_EQ(vectorEstimator.minimum(), Eigen::Vector2d::Ones());
    EXPECT_EQ(vectorEstimator.maximum(), Eigen::Vector2d::Ones());
    EXPECT_EQ(vectorEstimator.numberOfSamples(), 0);
}

TEST(StatisticsEstimator, sequenceOfDoubles)
{
    std::array<double, 10> population = { 1, 5, 8, -1, 4, 3, 8, 9, 20, -4 };
    StatisticsEstimator<double> doubleEstimator;

    for (double element : population) {
        doubleEstimator.process(element);
    }

    EXPECT_EQ(doubleEstimator.numberOfSamples(), population.size());
    EXPECT_DOUBLE_EQ(doubleEstimator.mean(), 5.3);
    EXPECT_NEAR(doubleEstimator.standardDeviation(), 6.634087059355726, 1.0e-09);
    EXPECT_DOUBLE_EQ(doubleEstimator.minimum(), -4);
    EXPECT_DOUBLE_EQ(doubleEstimator.maximum(), 20);

    EXPECT_EQ(std::string("mean: 5.300000e+00, std_dev: 6.634087e+00, min: -4.000000e+00, max: 2.000000e+01, samples: 10"), doubleEstimator.toString());

    doubleEstimator.reset();
    EXPECT_DOUBLE_EQ(doubleEstimator.mean(), 0.0);
    EXPECT_DOUBLE_EQ(doubleEstimator.standardDeviation(), 0.0);
    EXPECT_DOUBLE_EQ(doubleEstimator.minimum(), 0.0);
    EXPECT_DOUBLE_EQ(doubleEstimator.maximum(), 0.0);
    EXPECT_EQ(doubleEstimator.numberOfSamples(), 0);
}

TEST(StatisticsEstimator, sequenceOfVectors)
{
    std::array<Eigen::Vector2d, 10> population{ Eigen::Vector2d{ 1.0, 8.0 }, Eigen::Vector2d{ 5, 6 }, Eigen::Vector2d{ 8, 9 }, Eigen::Vector2d{ -1, 56 }, Eigen::Vector2d{ 4, 167 }, Eigen::Vector2d{ 3, -172 }, Eigen::Vector2d{ 8, 2 }, Eigen::Vector2d{ 9, -5 }, Eigen::Vector2d{ 20, 200 }, Eigen::Vector2d{ -4, 7 } };
    StatisticsEstimator<Eigen::Vector2d> vectorEstimator;

    for (Eigen::Vector2d element : population) {
        vectorEstimator.process(element);
    }

    EXPECT_EQ(vectorEstimator.numberOfSamples(), population.size());
    EXPECT_TRUE(vectorEstimator.mean().isApprox(Eigen::Vector2d(5.3, 27.8)));
    EXPECT_TRUE(vectorEstimator.standardDeviation().isApprox(Eigen::Vector2d(6.634087059355726, 1.017292703426327e+02)));
    EXPECT_TRUE(vectorEstimator.minimum().isApprox(Eigen::Vector2d(-4, -172)));
    EXPECT_TRUE(vectorEstimator.maximum().isApprox(Eigen::Vector2d(20, 200)));

    vectorEstimator.reset();
    EXPECT_EQ(vectorEstimator.mean(), Eigen::Vector2d::Zero());
    EXPECT_EQ(vectorEstimator.standardDeviation(), Eigen::Vector2d::Zero());
    EXPECT_EQ(vectorEstimator.minimum(), Eigen::Vector2d::Zero());
    EXPECT_EQ(vectorEstimator.maximum(), Eigen::Vector2d::Zero());
    EXPECT_EQ(vectorEstimator.numberOfSamples(), 0);
}

} // namespace
