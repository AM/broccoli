/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/math.hpp"
#include "broccoli/core/floats.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace core;

//! Test string representation of result types
TEST(math, ResultToString)
{
    uint8_t i;
    math::Result testResult;
    for (i = 0; i < static_cast<uint8_t>(math::Result::RESULT_COUNT); i++) {
        testResult = static_cast<math::Result>(i);
        ASSERT_GT(math::resultString(testResult).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(math::resultString(testResult) == "UNKNOWN");
        } else {
            ASSERT_TRUE(math::resultString(testResult) != "UNKNOWN");
        }
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(math::Result::RESULT_COUNT);
    testResult = static_cast<math::Result>(i);
    ASSERT_TRUE(math::resultString(testResult) == "UNKNOWN");
#endif
}

//! Test of factorial computation
TEST(math, factorial)
{
    // Initialize helpers
    math::Result result;

    // Check factorial computation y=f(x)
    for (uint64_t x = 0; x <= 11; x++) {
        // Compute "real" result
        uint64_t yReal = 1;
        for (uint64_t i = 1; i <= x; i++)
            yReal = yReal * i;

        // Compute result by library
        uint64_t yLibrary = math::factorial(x, &result);

        // Compare results
        ASSERT_EQ(yReal, yLibrary);
        ASSERT_EQ(result, math::Result::SUCCESS);
    }

    // Check for overflow
#ifdef NDEBUG
    math::factorial(21, &result);
    ASSERT_EQ(result, math::Result::ERROR_OVERFLOW);
#endif
}

//! Test of computation of binomial coefficients
TEST(math, BinomialCoefficient)
{
    // Initialize helpers
    math::Result result;

    // Check invalid input
#ifdef NDEBUG
    math::binomialCoefficient(0, 1, &result);
    ASSERT_EQ(result, math::Result::ERROR_INVALID_INPUT);
#endif

    // Check for overflow
#ifdef NDEBUG
    math::binomialCoefficient(30, 25, &result);
    ASSERT_EQ(result, math::Result::ERROR_OVERFLOW);
#endif

    // Check for various examples
    ASSERT_EQ(math::binomialCoefficient(0, 0, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(1, 0, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(1, 1, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(2, 0, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(2, 1, &result), 2);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(2, 2, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(3, 0, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(3, 1, &result), 3);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(3, 2, &result), 3);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(3, 3, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(4, 0, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(4, 1, &result), 4);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(4, 2, &result), 6);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(4, 3, &result), 4);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(4, 4, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(5, 0, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(5, 1, &result), 5);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(5, 2, &result), 10);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(5, 3, &result), 10);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(5, 4, &result), 5);
    ASSERT_EQ(result, math::Result::SUCCESS);
    ASSERT_EQ(math::binomialCoefficient(5, 5, &result), 1);
    ASSERT_EQ(result, math::Result::SUCCESS);
}

//! Test of computation of multinomial coefficients
TEST(math, MultinomialCoefficient)
{
    // Initialize helpers
    math::Result result;
    math::MultinomialCoefficientInputType k;

    // Check invalid input (empty tuple)
#ifdef NDEBUG
    math::multinomialCoefficient(k, &result);
    ASSERT_EQ(result, math::Result::ERROR_INVALID_INPUT);
#endif

    // Check for overflow
#ifdef NDEBUG
    k.resize(1);
    k[0] = 1000;
    math::multinomialCoefficient(k, &result);
    ASSERT_EQ(result, math::Result::ERROR_OVERFLOW);
#endif

    // Check for various examples
    k.resize(3);
    k[0] = 2;
    k[1] = 0;
    k[2] = 1;
    ASSERT_TRUE(math::multinomialCoefficient(k, &result) == 3);
    ASSERT_EQ(result, math::Result::SUCCESS);
    k.resize(3);
    k[0] = 1;
    k[1] = 1;
    k[2] = 1;
    ASSERT_TRUE(math::multinomialCoefficient(k, &result) == 6);
    ASSERT_EQ(result, math::Result::SUCCESS);
    k.resize(4);
    k[0] = 1;
    k[1] = 4;
    k[2] = 4;
    k[3] = 2;
    ASSERT_TRUE(math::multinomialCoefficient(k, &result) == 34650);
    ASSERT_EQ(result, math::Result::SUCCESS);
}

//! Test of computation of tuples of Faà di Bruno's formula
TEST(math, FaaDiBrunoTuples)
{
    // Initialize helpers
    math::Result result;

    // Check Faà di Bruno's tuples
    for (uint64_t n = 0; n <= 6; n++) {
        // Compute all tuples (m1, m2, ..., mn) of nonnegative integers satisfying 1*m1 + 2*m2 + ... + n*mn = n
        math::FaaDiBrunoTuples tuples = math::faaDiBrunoTuples(n, &result);
        ASSERT_EQ(result, math::Result::SUCCESS);

        // Pass through all tuples
        for (size_t i = 0; i < tuples.size(); i++) {
            // Check constraint
            uint64_t sum = 0;
            for (size_t j = 0; j < tuples[i].size(); j++)
                sum += (j + 1) * tuples[i][j];
            ASSERT_EQ(sum, n);
        }
    }

    // Check for overflow
#ifdef NDEBUG
    math::faaDiBrunoTuples(100, &result);
    ASSERT_EQ(result, math::Result::ERROR_OVERFLOW);
#endif
}

//! Test of computation of tuples of general Leibniz rule
TEST(math, GeneralLeibnizTuples)
{
    // Initialize helpers
    math::Result result;

    // Check for invalid input
#ifdef NDEBUG
    math::generalLeibnizTuples(2, 0, &result);
    ASSERT_EQ(result, math::Result::ERROR_INVALID_INPUT);
#endif

    // Check general Leibniz tuples
    for (uint64_t m = 1; m <= 6; m++) {
        for (uint64_t n = 0; n <= 6; n++) {
            // Compute all tuples (k1, k2, ..., km) of nonnegative integers satisfying k1 + k2 + ... + km = n
            math::GeneralLeibnizTuples tuples = math::generalLeibnizTuples(n, m, &result);
            ASSERT_EQ(result, math::Result::SUCCESS);

            // Pass through all tuples
            for (size_t i = 0; i < tuples.size(); i++) {
                // Check constraint
                uint64_t sum = 0;
                for (size_t j = 0; j < tuples[i].size(); j++)
                    sum += tuples[i][j];
                ASSERT_EQ(sum, n);
            }
        }
    }

    // Check for overflow
#ifdef NDEBUG
    math::generalLeibnizTuples(10, 10000, &result);
    ASSERT_EQ(result, math::Result::ERROR_OVERFLOW);
    math::generalLeibnizTuples(10000, 10, &result);
    ASSERT_EQ(result, math::Result::ERROR_OVERFLOW);
#endif
}

#ifdef HAVE_EIGEN3
//! Test solving tridiagonal system of equations
TEST(math, solveTriDiagonalSystemOfEquations)
{
    // Initialize helpers
    math::Result result;

    // Run several tests for different system dimensions
    const size_t minimumSystemDimension = 2;
    const size_t maximumSystemDimension = 20;
    for (size_t n = minimumSystemDimension; n <= maximumSystemDimension; n++) {
        double randomDouble = (double)(n - (maximumSystemDimension - minimumSystemDimension) / 2.0) / maximumSystemDimension; // (Pseudo-) random parameter

        // Setup input system for library
        Eigen::VectorXd lowerDiagonalOfA = Eigen::VectorXd::Zero(n - 1); // First element is "outside" the matrix A (to be ignored)
        Eigen::VectorXd diagonalOfA = Eigen::VectorXd::Zero(n - 1);
        Eigen::VectorXd upperDiagonalOfA = Eigen::VectorXd::Zero(n - 1); // Last element is "outside" the matrix A (to be ignored)
        Eigen::MatrixXd BLibrary = Eigen::MatrixXd::Zero(n - 1, 2);
        Eigen::MatrixXd XLibrary = Eigen::MatrixXd::Zero(n - 1, 2);
        for (size_t i = 0; i < n - 1; i++) {
            // Setup lower diagonal of A
            if (i > 0)
                lowerDiagonalOfA(i) = 1 + i + randomDouble;
            else
                lowerDiagonalOfA(i) = 0;

            // Setup diagonal of A
            diagonalOfA(i) = 100 + randomDouble; // Keep diagonal dominant

            // Setup upper diagonal of A
            if (i < n - 2)
                upperDiagonalOfA(i) = 1 + 2 * randomDouble + 0.5 * i;
            else
                upperDiagonalOfA(i) = 0;

            // Setup right hand side
            BLibrary(i, 0) = 0.5 * i + 3 * randomDouble;
            BLibrary(i, 1) = 1.5 * i + 2 * randomDouble;
        }

        // Setup test system (full matrix)
        Eigen::MatrixXd AFull = Eigen::MatrixXd::Zero(n - 1, n - 1);
        Eigen::MatrixXd BFull = Eigen::MatrixXd::Zero(n - 1, 2);
        Eigen::MatrixXd XFull = Eigen::MatrixXd::Zero(n - 1, 2);
        for (size_t i = 0; i < n - 1; i++) {
            // Setup lower diagonal of A
            if (i > 0)
                AFull(i, i - 1) = lowerDiagonalOfA[i];

            // Setup diagonal of A
            AFull(i, i) = diagonalOfA[i];

            // Setup upper diagonal of A
            if (i < n - 2)
                AFull(i, i + 1) = upperDiagonalOfA[i];

            // Setup right hand side
            BFull.block(i, 0, 1, 2) = BLibrary.block(i, 0, 1, 2);
        }

        // Solve system by library
        bool returnValue = math::solveTriDiagonalSystemOfEquations(lowerDiagonalOfA, diagonalOfA, upperDiagonalOfA, BLibrary, XLibrary, &result);
        ASSERT_EQ(returnValue, true);
        ASSERT_EQ(result, math::Result::SUCCESS);

        // Solve full system
        Eigen::ColPivHouseholderQR<Eigen::MatrixXd> QRFull(AFull); // Compute QR decomposition of AFull
        XFull = QRFull.solve(BFull);

        // Compare solutions
        double errorNorm = (XFull - XLibrary).norm();
        ASSERT_NEAR(errorNorm, 0, 1e-9);

        // Check for singularity detection
        diagonalOfA = Eigen::VectorXd::Zero(n - 1);
        returnValue = math::solveTriDiagonalSystemOfEquations(lowerDiagonalOfA, diagonalOfA, upperDiagonalOfA, BLibrary, XLibrary, &result);
        ASSERT_EQ(returnValue, true);
        ASSERT_EQ(result, math::Result::WARNING_CLOSE_TO_SINGULAR);

        // Check for dimension mismatch
#ifdef NDEBUG
        lowerDiagonalOfA = Eigen::VectorXd::Zero(n);
        returnValue = math::solveTriDiagonalSystemOfEquations(lowerDiagonalOfA, diagonalOfA, upperDiagonalOfA, BLibrary, XLibrary, &result);
        ASSERT_EQ(returnValue, false);
        ASSERT_EQ(result, math::Result::ERROR_DIMENSION_MISMATCH);
#endif
    }
}

//! Test solving block-tridiagonal system of equations
TEST(math, solveBlockTriDiagonalSystemOfEquations)
{
    // Initialize helpers
    math::Result result;

    // Run several tests for different system dimensions
    const size_t minimumBlockRows = 1;
    const size_t maximumBlockRows = 4;
    const size_t minimumBlockColumnsXB = 1;
    const size_t maximumBlockColumnsXB = 6;
    const size_t minimumSystemDimension = 2;
    const size_t maximumSystemDimension = 5;
    for (size_t blockRows = minimumBlockRows; blockRows <= maximumBlockRows; blockRows++) {
        for (size_t blockColumnsXB = minimumBlockColumnsXB; blockColumnsXB <= maximumBlockColumnsXB; blockColumnsXB++) {
            for (size_t n = minimumSystemDimension; n <= maximumSystemDimension; n++) {
                // (Pseudo-) random parameter
                double randomDouble = (double)(n - (maximumSystemDimension - minimumSystemDimension) / 2.0) / maximumSystemDimension;

                // Create pseudo-random matrix A
                Eigen::MatrixXd randomMatrixA = Eigen::MatrixXd::Zero(blockRows, blockRows);
                for (Eigen::Index row = 0; row < randomMatrixA.rows(); row++)
                    for (Eigen::Index column = 0; column < randomMatrixA.cols(); column++)
                        randomMatrixA(row, column) = row * randomDouble - sqrt(column) * randomDouble * randomDouble;

                // Create pseudo-random matrix B
                Eigen::MatrixXd randomMatrixB = Eigen::MatrixXd::Zero(blockRows, blockColumnsXB);
                for (Eigen::Index row = 0; row < randomMatrixB.rows(); row++)
                    for (Eigen::Index column = 0; column < randomMatrixB.cols(); column++)
                        randomMatrixB(row, column) = sqrt(row) * randomDouble * randomDouble - column * randomDouble;

                // Setup input system for library
                std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> lowerDiagonalOfA;
                std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> diagonalOfA;
                std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> upperDiagonalOfA;
                std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> bLibrary;
                std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> xLibrary;
                lowerDiagonalOfA.resize(n - 1); // First element is "outside" the matrix A (to be ignored)
                diagonalOfA.resize(n - 1);
                upperDiagonalOfA.resize(n - 1); // Last element is "outside" the matrix A (to be ignored)
                bLibrary.resize(n - 1);
                xLibrary.resize(n - 1);
                for (size_t i = 0; i < n - 1; i++) {
                    // Setup lower diagonal of A
                    if (i > 0)
                        lowerDiagonalOfA[i] = Eigen::MatrixXd::Constant(blockRows, blockRows, 2 + i) + randomMatrixA;
                    else
                        lowerDiagonalOfA[i] = Eigen::MatrixXd::Zero(blockRows, blockRows); // First element is "outside" the matrix A (to be ignored)

                    // Setup diagonal of A
                    diagonalOfA[i] = Eigen::MatrixXd::Constant(blockRows, blockRows, 20 + 3 * i) + randomMatrixA;

                    // Keep diagonal dominant
                    for (size_t x = 0; x < blockRows; x++)
                        diagonalOfA[i](x, x) = diagonalOfA[i](x, x) + 1000 + 500 * i;

                    // Setup upper diagonal of A
                    if (i < n - 2) {
                        upperDiagonalOfA[i] = Eigen::MatrixXd::Constant(blockRows, blockRows, 1 + i) + randomMatrixA;
                    } else {
                        upperDiagonalOfA[i] = Eigen::MatrixXd::Zero(blockRows, blockRows); // Last element is "outside" the matrix A (to be ignored)
                    }

                    // Setup right hand side
                    bLibrary[i] = Eigen::MatrixXd::Constant(blockRows, blockColumnsXB, 5 + 0.3 * i) + randomMatrixB;
                }

                // Setup test system (full matrix)
                Eigen::MatrixXd AFull = Eigen::MatrixXd::Zero((n - 1) * blockRows, (n - 1) * blockRows);
                Eigen::MatrixXd bFull = Eigen::MatrixXd::Zero((n - 1) * blockRows, blockColumnsXB);
                Eigen::MatrixXd xFull = Eigen::MatrixXd::Zero((n - 1) * blockRows, blockColumnsXB);
                for (size_t i = 0; i < n - 1; i++) {
                    // Setup lower diagonal of A
                    if (i > 0)
                        AFull.block(blockRows * i, blockRows * (i - 1), blockRows, blockRows) = lowerDiagonalOfA[i];

                    // Setup diagonal of A
                    AFull.block(blockRows * i, blockRows * i, blockRows, blockRows) = diagonalOfA[i];

                    // Setup upper diagonal of A
                    if (i < n - 2)
                        AFull.block(blockRows * i, blockRows * (i + 1), blockRows, blockRows) = upperDiagonalOfA[i];

                    // Setup right hand side
                    bFull.block(blockRows * i, 0, blockRows, blockColumnsXB) = bLibrary[i];
                }

                // Solve system by library
                bool returnValue = math::solveBlockTriDiagonalSystemOfEquations<Eigen::MatrixXd, Eigen::MatrixXd>(lowerDiagonalOfA, diagonalOfA, upperDiagonalOfA, bLibrary, xLibrary, &result);
                ASSERT_EQ(returnValue, true);
                ASSERT_EQ(result, math::Result::SUCCESS);

                // Concatenate solution to one matrix
                Eigen::MatrixXd xLibraryConcat = Eigen::MatrixXd::Zero((n - 1) * blockRows, blockColumnsXB);
                for (size_t i = 0; i < n - 1; i++)
                    xLibraryConcat.block(blockRows * i, 0, blockRows, blockColumnsXB) = xLibrary[i];

                // Solve full system
                Eigen::ColPivHouseholderQR<Eigen::MatrixXd> QRFull(AFull); // Compute QR decomposition of AFull
                xFull = QRFull.solve(bFull);

                // Compare solutions
                double errorNorm = (xFull - xLibraryConcat).norm();
                ASSERT_NEAR(errorNorm, 0, 1e-9);

                // Check for singularity detection
                diagonalOfA[0] = Eigen::MatrixXd::Zero(blockRows, blockRows);
                returnValue = math::solveBlockTriDiagonalSystemOfEquations(lowerDiagonalOfA, diagonalOfA, upperDiagonalOfA, bLibrary, xLibrary, &result);
                ASSERT_EQ(returnValue, true);
                ASSERT_EQ(result, math::Result::WARNING_CLOSE_TO_SINGULAR);

                // Check for dimension mismatch
#ifdef NDEBUG
                lowerDiagonalOfA.resize(lowerDiagonalOfA.size() - 1);
                returnValue = math::solveBlockTriDiagonalSystemOfEquations(lowerDiagonalOfA, diagonalOfA, upperDiagonalOfA, bLibrary, xLibrary, &result);
                ASSERT_EQ(returnValue, false);
                ASSERT_EQ(result, math::Result::ERROR_DIMENSION_MISMATCH);
#endif
            }
        }
    }
}
#endif // HAVE_EIGEN3

//! Test greatest common divisor
TEST(math, greatestCommonDivisor)
{
    // Integers
    EXPECT_EQ(math::greatestCommonDivisor(54, 24), 6);

    EXPECT_EQ(math::greatestCommonDivisor(48, 180), 12);

    EXPECT_EQ(math::greatestCommonDivisor(2, 0), 2);

    EXPECT_EQ(math::greatestCommonDivisor(0, 223), 223);

    EXPECT_EQ(math::greatestCommonDivisor(0, 0), 0);
}

//! Test least common multiple
TEST(math, leastCommonMultiple)
{
    EXPECT_EQ(math::leastCommonMultiple(4, 6), 12);

    EXPECT_EQ(math::leastCommonMultiple(21, 6), 42);

    EXPECT_EQ(math::leastCommonMultiple(math::leastCommonMultiple(8, 9), 21), 504);

    EXPECT_EQ(math::leastCommonMultiple(0, 0), 0);
}

TEST(math, clamp)
{
    EXPECT_EQ(math::clamp(1.0, -1.0, 2.0), 1.0);

    EXPECT_EQ(math::clamp(3.0, -1.0, 2.0), 2.0);

    EXPECT_EQ(math::clamp(-3.0, -1.0, 2.0), -1.0);

    double value = 1.0;
    double min = -1.0;

    EXPECT_EQ(math::clamp(value, min, 3.0), 1.0);
}

TEST(math, solvePseudoInverseEquation)
{
    Eigen::Matrix3d W = Eigen::Matrix3d::Identity();
    Eigen::Matrix<double, 2, 3> A;
    A << 1, -1, 0,
        0, 1, 1;

    Eigen::Vector2d y(1.0, 0.0);
    Eigen::Vector3d z(1.0, 1.0, 1.0);

    auto result = math::solvePseudoInverseEquation(A, W, y, z);

    EXPECT_DOUBLE_EQ(result(0), 1.0);
    EXPECT_DOUBLE_EQ(result(1), 0.0);
    EXPECT_DOUBLE_EQ(result(2), 0.0);
}

TEST(math, solveDampedPseudoInverseEquation)
{
    Eigen::Matrix3d W = Eigen::Matrix3d::Identity();
    Eigen::Matrix<double, 2, 3> A;
    A << 1, -1, 0,
        0, 1, 1;

    Eigen::Vector2d y(1.0, 0.0);
    Eigen::Vector3d z(1.0, 1.0, 1.0);

    auto result = math::solveDampedPseudoInverseEquation(A, 0.1, W, y, z);

    EXPECT_DOUBLE_EQ(result(0), 1.0293255131964809);
    EXPECT_DOUBLE_EQ(result(1), 0.032258064516129115);
    EXPECT_DOUBLE_EQ(result(2), 0.061583577712610138);
}

#ifdef HAVE_EIGEN3
TEST(math, findPerpendicularVector)
{
    const double min = -2.0;
    const double max = 2.0;
    const double step = 0.1;
    Eigen::Vector3d vector(min, min, min);
    while (vector.x() <= max) {
        vector.y() = min;
        while (vector.y() <= max) {
            vector.z() = min;
            while (vector.z() <= max) {
                if (vector.x() != 0.0 || vector.y() != 0.0 || vector.z() != 0.0) {
                    const Eigen::Vector3d result = math::findPerpendicularVector(vector);
                    ASSERT_LT(fabs(result.dot(vector)), 1e-9);
                }
                vector.z() += step;
            }
            vector.y() += step;
        }
        vector.x() += step;
    }

#ifdef NDEBUG
    const Eigen::Vector3d zeroResult = math::findPerpendicularVector(Eigen::Vector3d::Zero());
    ASSERT_TRUE(zeroResult.isZero());
#endif
}
#endif
