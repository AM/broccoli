/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/core/EnumerableState.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace broccoli::core;
using namespace std;

class Color : public EnumerableState<uint8_t> {
public:
    static constexpr Color red() { return Color(0); }

    static constexpr Color green() { return Color(1); }

    static constexpr Color yellow() { return Color(2); }

    static constexpr Type count() { return 3; }

    //! Explicit contructor from given integer value
    //! You may want to make this private for even more type-safety
    constexpr explicit Color(Type state)
        : EnumerableState<Type>(state)
    {
    }
};

TEST(EnumerableState, Test)
{
    // Test index() and copy construct
    Color color = Color::red();
    EXPECT_EQ(color.integer(), 0);

    Color color2 = Color::yellow();
    EXPECT_EQ(color2.integer(), 2);

    // Comparison operators
    EXPECT_FALSE(color2 == Color::red());
    EXPECT_TRUE(color2 != Color::red());

    // Test explicit color constructor (non-private)
    Color color3(3);
    EXPECT_EQ(color3.integer(), 3);

    color3 = color2;

    EXPECT_EQ(color3.integer(), 2);
    EXPECT_TRUE(color3 == Color::yellow());
}
