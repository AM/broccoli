/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/core/type_traits.hpp"
#include "broccoli/control/Signal.hpp"
#include "broccoli/control/Wrench.hpp"
#include "gtest/gtest.h"

using namespace broccoli::core;

namespace {

TEST(type_traits, Eigen)
{
    EXPECT_FALSE(is_eigen_matrix<double>::value);
    EXPECT_TRUE(is_eigen_matrix<Eigen::Vector2d>::value);
    EXPECT_TRUE(is_eigen_matrix<Eigen::Matrix3d>::value);
}

TEST(type_traits, Signal)
{
    EXPECT_FALSE(is_signal<double>::value);
    EXPECT_FALSE(is_signal<Eigen::Vector3d>::value);
    EXPECT_TRUE(is_signal<broccoli::control::Signal<double>>::value);
    EXPECT_TRUE(is_signal<broccoli::control::SignalBase<broccoli::control::Signal<double>>>::value);
}

TEST(type_traits, zero)
{
    double zero = 0.0;
    EXPECT_DOUBLE_EQ(Traits<double>::zero(), zero);

    Eigen::Vector3d zeroVector = Eigen::Vector3d::Zero();
    EXPECT_TRUE(zeroVector.isApprox(Traits<Eigen::Vector3d>::zero()));

    broccoli::control::Wrench myWrench = broccoli::control::Wrench::Zero();
    EXPECT_TRUE(myWrench.vector().isApprox(Traits<broccoli::control::Wrench>::zero().vector()));
}
}
