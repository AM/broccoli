/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/MultiLevelGrid.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace memory;

//! Mockup class of a grid cell for testing
class TestGridCell {
public:
    //! Default constructor
    TestGridCell(const int64_t& value = 0)
        : m_value(value)
    {
    }

    // Members
    int64_t m_value; //!< Value of this cell

    //! Comparison operator: **equality**
    bool operator==(const TestGridCell& reference) const
    {
        // Compare members
        if (m_value != reference.m_value)
            return false;

        // Otherwise -> equality
        return true;
    }

    //! Comparison operator: **inequality**
    inline bool operator!=(const TestGridCell& reference) const { return !(*this == reference); }
};

//! Mockup class of a multi-level grid for testing
template <unsigned int N, unsigned int L>
class TestGrid : public MultiLevelGrid<N, L, TestGridCell, std::allocator<TestGridCell>> {
public:
    // Default constructor - constructs an **empty** structure (see base class for details)
    TestGrid()
        : MultiLevelGrid<N, L, TestGridCell, std::allocator<TestGridCell>>()
    {
    }

    // Specialized constructor - constructs a multi-level grid with the given first level size (see base class for details)
    TestGrid(const std::array<size_t, N>& firstLevelSize)
        : MultiLevelGrid<N, L, TestGridCell, std::allocator<TestGridCell>>(firstLevelSize)
    {
    }

    // Specialized constructor - constructs a multi-level grid with the given first level size (see base class for details)
    template <typename... Targs>
    TestGrid(const size_t& firstSize, const Targs&... remainingSizes)
        : MultiLevelGrid<N, L, TestGridCell, std::allocator<TestGridCell>>(firstSize, remainingSizes...)
    {
    }

    // Cell operations
    // ---------------
protected:
    // Resets the given cell to its initial state (see base class for details)
    virtual void cellReset(TestGridCell& cell, const size_t& level)
    {
        // Call base function
        MultiLevelGrid<N, L, TestGridCell, std::allocator<TestGridCell>>::cellReset(cell, level);

        // Reset value
        cell.m_value = 0;
    }

    // Combines the data of the lower level cells to the data of the upper level cell (see base class for details)
    virtual void cellPropagateUp(TestGridCell& upperLevelCell, const std::vector<TestGridCell const*>& lowerLevelCells, const size_t& upperLevel, const std::array<size_t, N>& upperLevelIndices)
    {
        // Call base function
        MultiLevelGrid<N, L, TestGridCell, std::allocator<TestGridCell>>::cellPropagateUp(upperLevelCell, lowerLevelCells, upperLevel, upperLevelIndices);

        // Trigger propagation
        upperLevelCell.m_value = 0;
        for (size_t i = 0; i < lowerLevelCells.size(); i++)
            upperLevelCell.m_value += lowerLevelCells[i]->m_value;
    }

    // Propagates the data of the upper level cell to the data of the linked lower level cells (see base class for details)
    virtual void cellPropagateDown(const TestGridCell& upperLevelCell, const std::vector<TestGridCell*>& lowerLevelCells, const size_t& upperLevel, const std::array<size_t, N>& upperLevelIndices)
    {
        // Call base function
        MultiLevelGrid<N, L, TestGridCell, std::allocator<TestGridCell>>::cellPropagateDown(upperLevelCell, lowerLevelCells, upperLevel, upperLevelIndices);

        // Trigger propagation
        const int64_t lowerLevelCellValue = upperLevelCell.m_value / lowerLevelCells.size();
        for (size_t i = 0; i < lowerLevelCells.size(); i++)
            lowerLevelCells[i]->m_value = lowerLevelCellValue;
    }
};

//! Template function to perform construction and basic handling for multi-level grids
template <unsigned int N, unsigned int L>
inline void multiLevelGridCheckConstructionAndBasicUsage()
{
    static_assert(N <= 3, "Test function is only designed for dimensions <= 3!");

    // Initialize helpers
    std::array<size_t, N> elementIndices{};

    // Construction and Resizing
    // -------------------------
    // Construct
    TestGrid<N, L> gridA;

    // Test statics
    ASSERT_EQ(gridA.gridDimensions(), N);
    ASSERT_EQ(gridA.levelCount(), L);
    if (N == 1)
        ASSERT_EQ(gridA.linkedLowerLevelCellCount(), 2); // Binary tree
    if (N == 2)
        ASSERT_EQ(gridA.linkedLowerLevelCellCount(), 4); // Quadtree
    if (N == 3)
        ASSERT_EQ(gridA.linkedLowerLevelCellCount(), 8); // Octree

    // Resize
    std::array<size_t, N> firstLevelSize{};
    firstLevelSize[0] = 3;
    if (N > 1)
        firstLevelSize[1] = 2;
    if (N > 2)
        firstLevelSize[2] = 2;
#ifdef NDEBUG
    // Test bad allocation
    if (N == 3) {
        std::array<size_t, N> tooLargeSize;
        tooLargeSize.fill(1000000ULL); // Try to allocate >512 Exa-Bytes
        ASSERT_FALSE(gridA.resize(tooLargeSize));
    }
#endif
    ASSERT_TRUE(gridA.resize(firstLevelSize));
    gridA = TestGrid<N, L>(firstLevelSize);

    // Operators
    // ---------
    TestGrid<N, L> gridB;
    ASSERT_FALSE(gridA == gridB);
    ASSERT_TRUE(gridA != gridB);
    ASSERT_TRUE(gridA == gridA);
    ASSERT_FALSE(gridA != gridA);

    // Reset
    gridA.reset();

    // Element access
    // --------------
    for (size_t i = 0; i < L; i++) {
        // Setters
        elementIndices.fill(0);
        gridA(i, elementIndices).m_value = 12;
        elementIndices.fill(1);
        gridA(i, elementIndices).m_value = 34;

        // Getters
        const TestGrid<N, L> gridC = gridA;
        elementIndices.fill(0);
        ASSERT_EQ(gridC(i, elementIndices).m_value, 12);
        elementIndices.fill(1);
        ASSERT_EQ(gridC(i, elementIndices).m_value, 34);

        // Setters (fixed dimension)
        TestGrid<3, L> gridX(3, 2, 2);
        gridX(i, 0, 0, 0).m_value = 21;
        gridX(i, 1, 1, 1).m_value = 43;

        // Getters (fixed dimension)
        const TestGrid<3, L> gridY = gridX;
        ASSERT_EQ(gridY(i, 0, 0, 0).m_value, 21);
        ASSERT_EQ(gridY(i, 1, 1, 1).m_value, 43);
    }

    // Capacity
    // --------
    // Size
    TestGrid<N, L> gridC(firstLevelSize);
    ASSERT_TRUE(gridC.size(0) == firstLevelSize);
    for (size_t i = 1; i < L; i++) {
        std::array<size_t, N> expectedLevelSize;
        for (size_t j = 0; j < N; j++)
            expectedLevelSize[j] = firstLevelSize[j] * (1 << i);
        std::array<size_t, N> actualLevelSize = gridC.size(i);
        ASSERT_TRUE(actualLevelSize == expectedLevelSize);
    }

    // Cell count
    size_t firstLevelCellCount = firstLevelSize[0];
    for (size_t i = 1; i < N; i++)
        firstLevelCellCount *= firstLevelSize[i];
    ASSERT_EQ(gridC.cellCount(0), firstLevelCellCount);
    size_t previousLevelCellCount = firstLevelCellCount;
    size_t totalCellCount = firstLevelCellCount;
    for (size_t i = 1; i < L; i++) {
        size_t currentLevelCellCount = previousLevelCellCount * gridC.linkedLowerLevelCellCount();
        ASSERT_EQ(gridC.cellCount(i), currentLevelCellCount);
        previousLevelCellCount = currentLevelCellCount;
        totalCellCount += currentLevelCellCount;
    }
    ASSERT_EQ(gridC.totalCellCount(), totalCellCount);

    // Modifiers
    // ---------
    // Fill
    TestGrid<N, L> gridD(firstLevelSize);
    for (size_t i = 0; i < L; i++)
        gridD.fill(i, TestGridCell(i));
    for (size_t i = 0; i < L; i++) {
        for (size_t j = 0; j < gridD.cellCount(i); j++)
            ASSERT_EQ(gridD.cell(i, j).m_value, i);
    }
    gridD.fill(TestGridCell(123));
    for (size_t i = 0; i < L; i++) {
        for (size_t j = 0; j < gridD.cellCount(i); j++)
            ASSERT_EQ(gridD.cell(i, j).m_value, 123);
    }

    // Grid operations
    // ---------------
    // Reset
    for (size_t i = 0; i < L; i++) {
        for (size_t j = 0; j < gridD.cellCount(i); j++)
            ASSERT_EQ(gridD.cell(i, j).m_value, 123);
        gridD.reset(i);
        for (size_t j = 0; j < gridD.cellCount(i); j++)
            ASSERT_EQ(gridD.cell(i, j).m_value, 0);
        if (i + 1 < L)
            for (size_t j = 0; j < gridD.cellCount(i + 1); j++)
                ASSERT_EQ(gridD.cell(i + 1, j).m_value, 123);
    }
    gridD.fill(TestGridCell(456));
    gridD.reset();
    for (size_t i = 0; i < L; i++) {
        for (size_t j = 0; j < gridD.cellCount(i); j++)
            ASSERT_EQ(gridD.cell(i, j).m_value, 0);
    }

    // Test propagate up
    TestGrid<N, L> gridE(firstLevelSize);
    gridE.reset();
    gridE.fill(L - 1, TestGridCell(1));
    gridE.propagateBottomUp();
    uint64_t multiplier = 1;
    for (int i = L - 1; i >= 0; i--) {
        for (size_t j = 0; j < gridE.cellCount(i); j++) {
            const int64_t expectedValue = multiplier;
            const int64_t actualValue = gridE.cell(i, j).m_value;
            ASSERT_EQ(actualValue, expectedValue);
        }
        multiplier *= gridE.linkedLowerLevelCellCount();
    }

    // Test propagate down
    TestGrid<N, L> gridF(firstLevelSize);
    gridF.reset();
    gridF.fill(0, TestGridCell(multiplier));
    gridF.propagateTopDown();
    for (size_t i = 0; i < L; i++) {
        for (size_t j = 0; j < gridF.cellCount(i); j++) {
            const int64_t expectedValue = multiplier;
            const int64_t actualValue = gridF.cell(i, j).m_value;
            ASSERT_EQ(actualValue, expectedValue);
        }
        multiplier /= gridF.linkedLowerLevelCellCount();
    }
}

//! Check creation and basic handling for multi-level grids
TEST(MultiLevelGrid, ConstructionAndBasicUsageInteger)
{
    // Check "binary trees"
    multiLevelGridCheckConstructionAndBasicUsage<1, 1>();
    multiLevelGridCheckConstructionAndBasicUsage<1, 2>();
    multiLevelGridCheckConstructionAndBasicUsage<1, 3>();

    // Check "quadtrees"
    multiLevelGridCheckConstructionAndBasicUsage<2, 1>();
    multiLevelGridCheckConstructionAndBasicUsage<2, 2>();
    multiLevelGridCheckConstructionAndBasicUsage<2, 3>();

    // Check "octrees"
    multiLevelGridCheckConstructionAndBasicUsage<3, 1>();
    multiLevelGridCheckConstructionAndBasicUsage<3, 2>();
    multiLevelGridCheckConstructionAndBasicUsage<3, 3>();
}

//! Check creation and basic handling for (dense) binary trees
TEST(MultiLevelGrid, DenseBinaryTree)
{
    TestGrid<1, 3> tree(2);
    tree(2, 0).m_value = 1;
    tree(2, 3).m_value = 2;
    tree(2, 7).m_value = 1;
    tree.propagateBottomUp();
    ASSERT_EQ(tree(0, 0).m_value, 3);
    ASSERT_EQ(tree(0, 1).m_value, 1);
    ASSERT_EQ(tree(1, 0).m_value, 1);
    ASSERT_EQ(tree(1, 1).m_value, 2);
    ASSERT_EQ(tree(1, 2).m_value, 0);
    ASSERT_EQ(tree(1, 3).m_value, 1);
    ASSERT_EQ(tree(2, 0).m_value, 1);
    ASSERT_EQ(tree(2, 1).m_value, 0);
    ASSERT_EQ(tree(2, 2).m_value, 0);
    ASSERT_EQ(tree(2, 3).m_value, 2);
    ASSERT_EQ(tree(2, 4).m_value, 0);
    ASSERT_EQ(tree(2, 5).m_value, 0);
    ASSERT_EQ(tree(2, 6).m_value, 0);
    ASSERT_EQ(tree(2, 7).m_value, 1);
}

//! Check creation and basic handling for (dense) quadtrees
TEST(MultiLevelGrid, DenseQuadtree)
{
    TestGrid<2, 3> tree(1, 1);
    tree(2, 0, 0).m_value = 1;
    tree(2, 1, 1).m_value = 2;
    tree(2, 1, 2).m_value = 3;
    tree(2, 3, 3).m_value = 4;
    tree.propagateBottomUp();
    ASSERT_EQ(tree(0, 0, 0).m_value, 10);
    ASSERT_EQ(tree(1, 0, 0).m_value, 3);
    ASSERT_EQ(tree(1, 0, 1).m_value, 3);
    ASSERT_EQ(tree(1, 1, 0).m_value, 0);
    ASSERT_EQ(tree(1, 1, 1).m_value, 4);
    ASSERT_EQ(tree(2, 0, 0).m_value, 1);
    ASSERT_EQ(tree(2, 0, 1).m_value, 0);
    ASSERT_EQ(tree(2, 0, 2).m_value, 0);
    ASSERT_EQ(tree(2, 0, 3).m_value, 0);
    ASSERT_EQ(tree(2, 1, 0).m_value, 0);
    ASSERT_EQ(tree(2, 1, 1).m_value, 2);
    ASSERT_EQ(tree(2, 1, 2).m_value, 3);
    ASSERT_EQ(tree(2, 1, 3).m_value, 0);
    ASSERT_EQ(tree(2, 2, 0).m_value, 0);
    ASSERT_EQ(tree(2, 2, 1).m_value, 0);
    ASSERT_EQ(tree(2, 2, 2).m_value, 0);
    ASSERT_EQ(tree(2, 2, 3).m_value, 0);
    ASSERT_EQ(tree(2, 3, 0).m_value, 0);
    ASSERT_EQ(tree(2, 3, 1).m_value, 0);
    ASSERT_EQ(tree(2, 3, 2).m_value, 0);
    ASSERT_EQ(tree(2, 3, 3).m_value, 4);
}

//! Check creation and basic handling for (dense) octrees
TEST(MultiLevelGrid, DenseOctree)
{
    TestGrid<3, 2> tree(1, 2, 3);
    tree(1, 0, 0, 0).m_value = 1;
    tree(1, 1, 0, 5).m_value = 2;
    tree(1, 1, 3, 0).m_value = 3;
    tree.propagateBottomUp();
    for (size_t x = 0; x < 1; x++) {
        for (size_t y = 0; y < 2; y++) {
            for (size_t z = 0; z < 3; z++) {
                int64_t expectedValue = 0;
                if (x == 0 && y == 0 && z == 0)
                    expectedValue = 1;
                else if (x == 0 && y == 0 && z == 2)
                    expectedValue = 2;
                else if (x == 0 && y == 1 && z == 0)
                    expectedValue = 3;
                ASSERT_EQ(tree(0, x, y, z).m_value, expectedValue);
            }
        }
    }
    for (size_t x = 0; x < 2; x++) {
        for (size_t y = 0; y < 4; y++) {
            for (size_t z = 0; z < 6; z++) {
                int64_t expectedValue = 0;
                if (x == 0 && y == 0 && z == 0)
                    expectedValue = 1;
                else if (x == 1 && y == 0 && z == 5)
                    expectedValue = 2;
                else if (x == 1 && y == 3 && z == 0)
                    expectedValue = 3;
                ASSERT_EQ(tree(1, x, y, z).m_value, expectedValue);
            }
        }
    }
}
