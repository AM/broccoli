/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/memory/ThreadSafeData.hpp"
#include "gtest/gtest.h"
#include <type_traits>

using namespace broccoli;
using namespace memory;
using namespace ::testing;

template <typename T>
class TestThreadSafeData : public Test {
protected:
    TestThreadSafeData()
        : m_otherData(m_mutex, {})
        , m_data(m_mutex)
    {
    }

    T value{};
    ThreadSafeData<T> m_otherData;
    ThreadSafeData<T> m_data;
    typename ThreadSafeData<T>::MutexType m_mutex;
};

struct TestThreadSafeDataPropertyTest {
    using D = uint8_t;
    using T = ThreadSafeData<D>;
    using M = T::MutexType;

    static_assert(std::is_constructible<T, M&>::value, "");
    static_assert(std::is_constructible<T, M&, const D&>::value, "");
    static_assert(std::is_constructible<T, M&, D&&>::value, "");

    static_assert(!std::is_copy_constructible<T>::value, "");
    static_assert(std::is_copy_assignable<T>::value, "");
    static_assert(std::is_move_constructible<T>::value, "");
    static_assert(std::is_move_assignable<T>::value, "");

    static_assert(std::is_nothrow_move_assignable<T>::value, "");
    static_assert(std::is_nothrow_move_constructible<T>::value, "");

    // static_assert(std::is_convertible<T, D>::value, "");
    // static_assert(std::is_convertible<T::DataGuard<D, T::DefaultWriteLock<D>>, D>::value, "");
};

using TestThreadSafeDataUint8 = TestThreadSafeData<uint8_t>;

TEST_F(TestThreadSafeDataUint8, Creation)
{
    uint8_t value = 10;
    ThreadSafeData<uint8_t> dataCopied(m_mutex, ++value);
    EXPECT_EQ((uint8_t)dataCopied, 11);

    ThreadSafeData<uint8_t> moved(ThreadSafeData<uint8_t>(m_mutex, 2));
    EXPECT_EQ(moved.copy(), 2);

    // move assignment
    dataCopied = ThreadSafeData<uint8_t>(m_mutex, 10);
    EXPECT_EQ(dataCopied.copy(), 10);

    // copy assignment
    dataCopied = moved;
    EXPECT_EQ((uint8_t)dataCopied, 2);
}

TEST_F(TestThreadSafeDataUint8, LockAndAccess)
{
    // Use default lock
    const uint8_t solution = 42;
    m_data = solution;
    uint8_t tmp = m_data.copy();

    EXPECT_EQ(tmp, solution);
}

TEST_F(TestThreadSafeDataUint8, LockGuardAccess)
{
    {
        auto guard = m_data.lockWithGuard();
        EXPECT_TRUE(guard);
        guard.get() = 41;
        (*guard)++;
    }

    {
        auto guard = m_data.lockWithGuard<std::unique_lock>(std::try_to_lock);
        EXPECT_TRUE(guard);
        if (guard) {
            // got lock
            EXPECT_EQ(*guard, 42);
            return;
        }
    }
}

TEST_F(TestThreadSafeDataUint8, ReadOnlyGuard)
{
    m_otherData = 4;
    uint8_t result;
    {
        auto guard = m_data.lockConstWithGuard();
        auto secondGuard = m_otherData.lockConstWithGuard();
        result = *secondGuard;
    }
    EXPECT_EQ(result, 4);
}

struct Data {
    uint8_t value = 4;
    uint16_t address;
};
using TestThreadSafeDataStruct = TestThreadSafeData<Data>;
TEST_F(TestThreadSafeDataStruct, MemberAccess)
{
    uint8_t result;
    {
        auto guard = m_data.lockConstWithGuard();
        auto secondGuard = m_otherData.lockConstWithGuard();
        result = secondGuard->value;
    }
    EXPECT_EQ(result, 4);
}
