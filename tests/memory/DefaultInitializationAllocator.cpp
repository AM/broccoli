/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/DefaultInitializationAllocator.hpp"
#include "gtest/gtest.h"
#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#include <Eigen/StdVector>
#endif

using namespace broccoli;
using namespace memory;

TEST(DefaultInitializationAllocator, NonEigen)
{
    static const size_t length = 100;
    static const int testValue = 123;

    // Setup vectors
    std::vector<int> defaultVector;
    std::vector<int, DefaultInitializationAllocator<int>> enhancedVector;

    // Resize and fill with numbers
    defaultVector.resize(length, testValue);
    enhancedVector.resize(length, testValue);

    // Empty
    defaultVector.clear();
    enhancedVector.clear();

    // Resize without filling
    defaultVector.resize(length);
    enhancedVector.resize(length);
    for (size_t i = 0; i < length; i++) {
        ASSERT_EQ(defaultVector[i], 0);
        ASSERT_EQ(enhancedVector[i], testValue);
    }
}

#ifdef HAVE_EIGEN3
TEST(DefaultInitializationAllocator, Eigen)
{
    static const size_t length = 100;
    static const Eigen::Vector2d testValue = Eigen::Vector2d(1.23, -2.34);

    // Setup vector
    std::vector<Eigen::Vector2d, DefaultInitializationAllocator<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d>>> enhancedVector;

    // Resize and fill with numbers
    enhancedVector.resize(length, testValue);

    // Empty
    enhancedVector.clear();

    // Resize without filling
    enhancedVector.resize(length);
    for (size_t i = 0; i < length; i++)
        ASSERT_TRUE(enhancedVector[i].isApprox(testValue));
}
#endif
