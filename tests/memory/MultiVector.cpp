/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/MultiVector.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace memory;

//! Template function to perform construction and basic handling for vectors with different element types
template <class ElementType>
inline void multiVectorCheckConstructionAndBasicUsage(const ElementType& elementA, const ElementType& elementB)
{
    // Initialize helpers
    bool error = false;

    // Construction
    // ------------
    // Default constructor (no parameters)
    MultiVector<ElementType, 1> VectorA;
    ASSERT_TRUE(VectorA.size() == (std::array<size_t, 1>{ 0 }));
    ASSERT_TRUE(VectorA.elementCount() == 0);

    // Specialized constructor (size as array - one dimension)
    MultiVector<ElementType, 1> VectorC(std::array<size_t, 1>{ 1 });
    ASSERT_TRUE(VectorC.size() == (std::array<size_t, 1>{ 1 }));
    ASSERT_TRUE(VectorC.elementCount() == 1);

    // Specialized constructor (size as array - two dimensions)
    MultiVector<ElementType, 2> VectorD(std::array<size_t, 2>{ 1, 2 });
    ASSERT_TRUE(VectorD.size() == (std::array<size_t, 2>{ 1, 2 }));
    ASSERT_TRUE(VectorD.elementCount() == 2);

    // Specialized constructor (size as array - three dimensions)
    MultiVector<ElementType, 3> VectorE(std::array<size_t, 3>{ 1, 2, 3 });
    ASSERT_TRUE(VectorE.size() == (std::array<size_t, 3>{ 1, 2, 3 }));
    ASSERT_TRUE(VectorE.elementCount() == 6);

    // Specialized constructor (size as paramater - one dimension)
    MultiVector<ElementType, 1> VectorF(1);
    ASSERT_TRUE(VectorF.size() == (std::array<size_t, 1>{ 1 }));
    ASSERT_TRUE(VectorF.elementCount() == 1);

    // Specialized constructor (size as paramater - two dimensions)
    MultiVector<ElementType, 2> VectorG(1, 2);
    ASSERT_TRUE(VectorG.size() == (std::array<size_t, 2>{ 1, 2 }));
    ASSERT_TRUE(VectorG.elementCount() == 2);

    // Specialized constructor (size as paramater - three dimensions)
    MultiVector<ElementType, 3> VectorH(1, 2, 3);
    ASSERT_TRUE(VectorH.size() == (std::array<size_t, 3>{ 1, 2, 3 }));
    ASSERT_TRUE(VectorH.elementCount() == 6);

    // Element mapping
    // ---------------
    MultiVector<ElementType, 3> VectorB(std::array<size_t, 3>{ 2, 4, 3 });
    size_t expectedElementBufferIndex = 0;
    for (size_t i = 0; i < 2; i++) {
        for (size_t j = 0; j < 4; j++) {
            for (size_t k = 0; k < 3; k++) {
                const size_t elementBufferIndex = VectorB.elementBufferIndex(i, j, k);
                const std::array<size_t, 3> indices = VectorB.indices(elementBufferIndex);
                ASSERT_EQ(elementBufferIndex, expectedElementBufferIndex);
                ASSERT_EQ(indices[0], i);
                ASSERT_EQ(indices[1], j);
                ASSERT_EQ(indices[2], k);
                expectedElementBufferIndex++;
            }
        }
    }
    // Test out-of range
#ifdef NDEBUG
    std::array<size_t, 3> invalidIndices;
    EXPECT_THROW((invalidIndices = VectorB.indices(1000)), std::out_of_range);
#endif

    // Element access
    // --------------
    // Access through indices
    MultiVector<ElementType, 3> VectorI(2, 2, 3);
    ASSERT_TRUE(VectorI.size() == (std::array<size_t, 3>{ 2, 2, 3 }));
    ASSERT_TRUE(VectorI.elementCount() == 12);
    for (size_t i = 0; i < 2; i++) {
        for (size_t j = 0; j < 2; j++) {
            for (size_t k = 0; k < 3; k++) {
                if (j == 0)
                    VectorI(std::array<size_t, 3>{ i, j, k }) = elementA;
                else
                    VectorI(i, j, k) = elementB;
            }
        }
    }
    const auto VectorJ = VectorI;
    for (size_t i = 0; i < 2; i++) {
        for (size_t j = 0; j < 2; j++) {
            for (size_t k = 0; k < 3; k++) {
                if (j == 0)
                    ASSERT_TRUE(VectorJ(std::array<size_t, 3>{ i, j, k }) == elementA);
                else
                    ASSERT_TRUE(VectorJ(i, j, k) == elementB);
            }
        }
    }

    // Test out-of range
#ifdef NDEBUG
    // Wrong indices
    EXPECT_THROW(error = (VectorI(std::array<size_t, 3>{ 10, 0, 0 }) == elementA), std::out_of_range);
    EXPECT_THROW(error = (VectorI(10, 0, 0) == elementA), std::out_of_range);
    EXPECT_THROW(error = (VectorI(std::array<size_t, 3>{ 0, 10, 0 }) == elementA), std::out_of_range);
    EXPECT_THROW(error = (VectorI(0, 10, 0) == elementA), std::out_of_range);
    EXPECT_THROW(error = (VectorI(std::array<size_t, 3>{ 0, 0, 10 }) == elementA), std::out_of_range);
    EXPECT_THROW(error = (VectorI(0, 0, 10) == elementA), std::out_of_range);
#endif

    // Access through element buffer
    MultiVector<ElementType, 2> VectorY(2, 3);
    for (size_t i = 0; i < VectorY.elementCount(); i++) {
        if (i % 2 == 0)
            VectorY[i] = elementA;
        else
            VectorY[i] = elementB;
    }
    const auto VectorZ = VectorY;
    for (size_t i = 0; i < VectorY.elementCount(); i++) {
        if (i % 2 == 0)
            ASSERT_TRUE(VectorZ[i] == elementA);
        else
            ASSERT_TRUE(VectorZ[i] == elementB);
    }

    // Modifiers
    // ---------
    // Test fill()
    auto VectorX = VectorI;
    VectorX.fill(elementA);
    for (size_t i = 0; i < 2; i++)
        for (size_t j = 0; j < 2; j++)
            for (size_t k = 0; k < 3; k++)
                ASSERT_TRUE(VectorX(i, j, k) == elementA);

    // Test clear()
    VectorX.clear(false);
    VectorX.clear(true);
    ASSERT_TRUE(VectorX.elementCount() == 0);

    // Test reserve()
    ASSERT_TRUE(VectorX.reserve(std::array<size_t, 3>{ 1, 2, 3 }));
    ASSERT_TRUE(VectorX.elementCount() == 0);

    // Test resize() (with vector)
    ASSERT_TRUE(VectorX.resize(std::array<size_t, 3>{ 1, 2, 3 }));
    ASSERT_TRUE(VectorX.size() == (std::array<size_t, 3>{ 1, 2, 3 }));
    ASSERT_TRUE(VectorX.elementCount() == 6);

    // Test resize() (with vector - copy initialized)
    VectorX.clear(true);
    ASSERT_TRUE(VectorX.resize(std::array<size_t, 3>{ 1, 2, 3 }, elementA));
    ASSERT_TRUE(VectorX.size() == (std::array<size_t, 3>{ 1, 2, 3 }));
    ASSERT_TRUE(VectorX.elementCount() == 6);
    for (size_t i = 0; i < VectorX.elementCount(); i++) {
        ASSERT_TRUE(VectorX[i] == elementA);
    }

    // Test bad allocation
#ifdef NDEBUG
    MultiVector<uint64_t, 6> tooLargeVector;
    ASSERT_FALSE(tooLargeVector.reserve(1000, 1000, 1000, 1000, 1000, 1000)); // <-- Tries to allocate 8 Exa-Bytes
    ASSERT_FALSE(tooLargeVector.resize(1000, 1000, 1000, 1000, 1000, 1000)); // <-- Tries to allocate 8 Exa-Bytes
    ASSERT_FALSE(tooLargeVector.resize(std::array<size_t, 6>{ 1000, 1000, 1000, 1000, 1000, 1000 }, 123)); // <-- Tries to allocate 8 Exa-Bytes
#endif

    // Test resize() (with parameter pack)
    VectorX.clear();
    VectorX.resize(2, 3, 4);
    ASSERT_TRUE(VectorX.size() == (std::array<size_t, 3>{ 2, 3, 4 }));
    ASSERT_TRUE(VectorX.elementCount() == 24);

    // Operators
    // ---------
    // Inequality operator
    ASSERT_TRUE(VectorX != VectorI);

    // Equality operator
    ASSERT_TRUE(VectorJ == VectorI);

    // Finalize
    if (error == true)
        std::cout << "ERROR" << std::endl;
}

//! Check creation and basic handling for integer element types
TEST(MultiVector, ConstructionAndBasicUsageInteger)
{
    const int elementA = 1;
    const int elementB = -2;
    multiVectorCheckConstructionAndBasicUsage<int>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<int>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<int>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<int>(elementA, elementB);
}

//! Check creation and basic handling for floating point (double precision) element types
TEST(MultiVector, ConstructionAndBasicUsageDouble)
{
    const double elementA = 1.23;
    const double elementB = -2.34;
    multiVectorCheckConstructionAndBasicUsage<double>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<double>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<double>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<double>(elementA, elementB);
}

//! Check creation and basic handling for string element types
TEST(MultiVector, ConstructionAndBasicUsageString)
{
    const std::string elementA = "abc";
    const std::string elementB = "defg";
    multiVectorCheckConstructionAndBasicUsage<std::string>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<std::string>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<std::string>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<std::string>(elementA, elementB);
}

#ifdef HAVE_EIGEN3
//! Check creation and basic handling for Eigen element types
TEST(MultiVector, ConstructionAndBasicUsageEigen)
{
    const Eigen::Vector2d elementA = Eigen::Vector2d(1.23, -2.34);
    const Eigen::Vector2d elementB = Eigen::Vector2d(-3.45, 4.56);
    multiVectorCheckConstructionAndBasicUsage<Eigen::Vector2d>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<Eigen::Vector2d>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<Eigen::Vector2d>(elementA, elementB);
    multiVectorCheckConstructionAndBasicUsage<Eigen::Vector2d>(elementA, elementB);
}
#endif // HAVE_EIGEN3
