/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/CircularBuffer.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace memory;

//! Check creation and handling of circular buffers
TEST(CircularBuffer, Misc)
{
    // Initialize helpers
    double element = 0;
    std::vector<double> elements;

    // Create circular buffer for POD
    CircularBuffer<double, std::allocator<double>> buffer(3);
    ASSERT_EQ(buffer.maximumElementCount(), 3);

    // Fill with data
    ASSERT_EQ(buffer.elementCount(), 0);
    ASSERT_EQ(buffer.push(1), 0);
    ASSERT_EQ(buffer.elementCount(), 1);
    ASSERT_EQ(buffer.push(2), 0);
    ASSERT_EQ(buffer.elementCount(), 2);
    ASSERT_EQ(buffer.push(3), 0);
    ASSERT_EQ(buffer.elementCount(), 3);

    // Grab content and check it
    ASSERT_EQ(buffer.pop(element), 1);
    ASSERT_EQ(element, 1);
    ASSERT_EQ(buffer.pop(element), 1);
    ASSERT_EQ(element, 2);
    ASSERT_EQ(buffer.pop(element), 1);
    ASSERT_EQ(element, 3);
    ASSERT_EQ(buffer.pop(element), 0);

    // Fill up again (test overflow)
    ASSERT_EQ(buffer.totalOverwrittenElements(), 0);
    ASSERT_EQ(buffer.elementCount(), 0);
    ASSERT_EQ(buffer.push(4), 0);
    ASSERT_EQ(buffer.elementCount(), 1);
    ASSERT_EQ(buffer.push(5), 0);
    ASSERT_EQ(buffer.elementCount(), 2);
    ASSERT_EQ(buffer.push(6), 0);
    ASSERT_EQ(buffer.elementCount(), 3);
    ASSERT_EQ(buffer.push(7), 1);
    ASSERT_EQ(buffer.elementCount(), 3);
    ASSERT_EQ(buffer.totalOverwrittenElements(), 1);

    // Grab content and check it
    ASSERT_EQ(buffer.pop(element), 1);
    ASSERT_EQ(element, 5);
    ASSERT_EQ(buffer.pop(element), 1);
    ASSERT_EQ(element, 6);
    ASSERT_EQ(buffer.pop(element), 1);
    ASSERT_EQ(element, 7);
    ASSERT_EQ(buffer.pop(element), 0);

    // Fill with data (from list)
    elements.resize(5, 1);
    ASSERT_EQ(buffer.push(elements, 2), 0);
    ASSERT_EQ(buffer.elementCount(), 2);
    ASSERT_EQ(buffer.push(elements, 3), 2);
    ASSERT_EQ(buffer.elementCount(), 3);

    // Grab content (to list)
    elements.clear();
    ASSERT_EQ(buffer.pop(elements, 2), 2);
    ASSERT_EQ(buffer.elementCount(), 1);
    ASSERT_EQ(elements.size(), 2);
    ASSERT_EQ(buffer.pop(elements, 2), 1);
    ASSERT_EQ(buffer.elementCount(), 0);
    ASSERT_EQ(elements.size(), 1);

    // Test clearing
    ASSERT_EQ(buffer.push(1), 0);
    ASSERT_EQ(buffer.push(2), 0);
    ASSERT_EQ(buffer.push(3), 0);
    ASSERT_EQ(buffer.elementCount(), 3);
    buffer.clear();
    ASSERT_EQ(buffer.elementCount(), 0);

    // Test reallocation
    ASSERT_EQ(buffer.push(1), 0);
    ASSERT_EQ(buffer.push(2), 0);
    ASSERT_EQ(buffer.push(3), 0);
    ASSERT_EQ(buffer.elementCount(), 3);
    buffer.reAllocate();
    ASSERT_EQ(buffer.elementCount(), 0);

    // Test copy constructor
    ASSERT_EQ(buffer.push(1), 0);
    ASSERT_EQ(buffer.push(2), 0);
    ASSERT_EQ(buffer.push(3), 0);
    CircularBuffer<double, std::allocator<double>> secondBuffer(buffer);
    ASSERT_EQ(secondBuffer.pop(element), 1);
    ASSERT_EQ(element, 1);
    ASSERT_EQ(secondBuffer.pop(element), 1);
    ASSERT_EQ(element, 2);
    ASSERT_EQ(secondBuffer.pop(element), 1);
    ASSERT_EQ(element, 3);
    ASSERT_EQ(secondBuffer.pop(element), 0);

    // Test assignment operator
    ASSERT_EQ(buffer.elementCount(), 3);
    ASSERT_EQ(secondBuffer.elementCount(), 0);
    secondBuffer = buffer;
    ASSERT_EQ(secondBuffer.elementCount(), 3);

    // Test pop newest and clear
    ASSERT_EQ(secondBuffer.popNewestAndClear(element), 3);
    ASSERT_EQ(secondBuffer.elementCount(), 0);
}
