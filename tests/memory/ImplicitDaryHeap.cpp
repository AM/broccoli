/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/ImplicitDaryHeap.hpp"
#include "gtest/gtest.h"
#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#endif
#include <algorithm>

using namespace broccoli;
using namespace memory;

// Count of nodes
static constexpr size_t N = 100;

// A sequence of random numbers uniformly distributed between [0, 1[
static const std::array<double, N> uniformDistribution = { { 2.715316070679644e-01, 9.034731028904828e-01, 2.007726312743635e-01, 3.149633530547865e-01, 8.011310181888299e-01,
    2.100653452032356e-01, 6.873443460328854e-01, 2.718207797721542e-01, 5.033366176396696e-01, 8.882818472067450e-01,
    2.281617788067474e-01, 5.383482239337285e-01, 9.598616189989286e-01, 6.396240529956486e-01, 8.172301369873319e-01,
    6.630097087803316e-01, 1.968461135148585e-01, 1.673925120926890e-01, 1.983906897524466e-01, 2.393543681184692e-01,
    6.874243918126594e-01, 2.403951197004959e-01, 4.257425178473344e-02, 5.906919239951881e-01, 7.335930340833767e-01,
    9.801429892858231e-02, 3.069114544154680e-01, 3.655708858142077e-01, 7.258980232257797e-01, 4.319502750802619e-01,
    3.180487827394874e-01, 6.059536386493837e-01, 5.677865530183722e-01, 6.215803451432360e-01, 1.169299931434393e-01,
    1.827969840118043e-01, 5.117485362139109e-01, 4.438789207252490e-01, 7.907255048770092e-01, 9.751038764878075e-01,
    6.199686039147633e-01, 6.693899817673755e-01, 6.248641730892315e-01, 1.874887771484404e-01, 9.067165078101886e-01,
    9.241621240530914e-01, 3.457477355971220e-01, 1.638929268174460e-01, 4.248980741347242e-01, 8.888477860442005e-02,
    5.195454760275346e-02, 1.617010889427586e-01, 3.708479561375562e-01, 6.863179386279652e-01, 6.641418920015854e-01,
    1.176280064444968e-01, 3.781275680257678e-01, 5.286710839486153e-01, 4.004149556549875e-01, 1.553675288112048e-01,
    4.575118374859686e-01, 3.016636923627760e-01, 2.678233734103465e-01, 9.276898423890882e-01, 7.083056817615755e-01,
    3.429689052179222e-01, 4.829428778838054e-01, 1.846670892006309e-01, 7.875729896698697e-01, 5.327066157807179e-01,
    9.422152506556007e-01, 2.713070975517735e-01, 4.637082308208899e-02, 8.113488131039963e-02, 1.488603394229666e-01,
    7.491814305677990e-01, 8.403289114883613e-01, 1.522632492715010e-01, 9.489723831555782e-01, 1.657966585010681e-01,
    4.743990780806529e-01, 2.744857896912941e-01, 8.255924089678586e-01, 2.826461264382172e-01, 4.385440879451501e-02,
    8.525909180321567e-01, 2.628821590025725e-02, 2.027768676214199e-01, 6.112649964810021e-01, 5.753187325443079e-01,
    1.704959558446100e-02, 1.558636192876917e-01, 5.518404291722043e-01, 7.650637071628591e-01, 7.522748978564696e-01,
    5.679731823663106e-01, 3.971316367328546e-01, 6.935907672595831e-01, 3.099978918491966e-01, 4.232411308067943e-01 } };

// Mockup for a simplified node
struct MockNode {
    double m_key;
    size_t m_objectHash;
    bool operator<(const MockNode& other) const { return (m_key < other.m_key); }
};

// Setup list of input nodes (unsorted)
static inline std::array<MockNode, N> setupInputNodes()
{
    std::array<MockNode, N> nodes;
    for (size_t i = 0; i < N; i++) {
        nodes[i].m_key = uniformDistribution[i];
        nodes[i].m_objectHash = i;
    }
    return nodes;
}
static const std::array<MockNode, N> inputNodes = setupInputNodes();

// Setup sorted list of output nodes for a min-heap (lowest key first)
static inline std::array<MockNode, N> setupOutputNodesMinHeap()
{
    std::array<MockNode, N> nodes = inputNodes;
    std::sort(nodes.begin(), nodes.end());
    return nodes;
}
static const std::array<MockNode, N> outputNodesMinHeap = setupOutputNodesMinHeap();

// Setup sorted list of output nodes for a max-heap (highest key first)
static inline std::array<MockNode, N> setupOutputNodesMaxHeap()
{
    std::array<MockNode, N> nodes = outputNodesMinHeap;
    std::reverse(nodes.begin(), nodes.end());
    return nodes;
}
static const std::array<MockNode, N> outputNodesMaxHeap = setupOutputNodesMaxHeap();

//! Base class for test objects
class TestObject {
public:
    TestObject(const size_t& hash)
        : m_hash(hash)
    {
    }
    size_t m_hash; //!< Unique hash for this object
};

//! Test non-eigen object
template <unsigned int ByteCount>
class NonEigenObject : public TestObject {
public:
    NonEigenObject(const size_t& hash)
        : TestObject(hash)
    {
        for (uint8_t i = 0; i < ByteCount; i++)
            m_data[i] = i;
    }
    std::array<uint8_t, ByteCount> m_data; //!< Dummy data container
};

#ifdef HAVE_EIGEN3
//! Test eigen object
template <unsigned int ByteCount>
class EigenObject : public TestObject {
public:
    EigenObject(const size_t& hash)
        : TestObject(hash)
    {
        for (uint8_t i = 0; i < ByteCount; i++)
            m_data[i] = i;
    }
    Eigen::Matrix<uint8_t, ByteCount, 1> m_data; //!< Dummy data container
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
};
#endif

//! Hash function for test objects
template <typename T>
struct TestObjectHash {
    inline const size_t& operator()(const T& object) const { return object.m_hash; }
};

//! Adds input nodes to the given heap
template <typename T, typename Heap>
static void addInputNodesToHeap(Heap& heap, const size_t& count, const bool& duplicates)
{
    const size_t oldSize = heap.size();
    for (size_t i = 0; i < count; i++) {
        const T newObject = T(i);
        const bool insertResult = heap.insert(inputNodes[i].m_key, newObject);
        if (duplicates == false) {
            ASSERT_EQ(heap.size(), oldSize + i + 1);
            ASSERT_EQ(insertResult, true);
        } else {
            ASSERT_EQ(heap.size(), oldSize);
            ASSERT_EQ(insertResult, false);
        }
    }
}

//! Removes input nodes from the given heap
template <typename T, typename Heap>
static void removeInputNodesFromHeap(Heap& heap, const size_t& count, const bool& missing)
{
    const size_t oldSize = heap.size();
    for (size_t i = 0; i < count; i++) {
        const T object = T(i);
        const bool removeResult = heap.remove(object);
        if (missing == false) {
            ASSERT_EQ(heap.size(), oldSize - i - 1);
            ASSERT_EQ(removeResult, true);
        } else {
            ASSERT_EQ(heap.size(), oldSize);
            ASSERT_EQ(removeResult, false);
        }
    }
}

//! Check generic heap
template <typename T, typename Allocator>
static void checkGenericHeap()
{
    for (size_t D = 2; D <= 16; D++) {
        using MinHeap = ImplicitDaryMinHeap<double, T, TestObjectHash<T>, Allocator>;
        using MaxHeap = ImplicitDaryMaxHeap<double, T, TestObjectHash<T>, Allocator>;

        // Create heap
        MinHeap minHeap = MinHeap(D, 16, true); // Too small -> needs to reallocate
        ASSERT_TRUE(minHeap.empty());
        ASSERT_EQ(minHeap.size(), 0);
        MaxHeap maxHeap = MaxHeap(D, 16, false);
        maxHeap.reserve(128); // Big enough -> no reallocation necessary
        ASSERT_TRUE(maxHeap.empty());
        ASSERT_EQ(maxHeap.size(), 0);

        // Insert elements
        addInputNodesToHeap<T, MinHeap>(minHeap, N, false);
        addInputNodesToHeap<T, MaxHeap>(maxHeap, N, false);
        ASSERT_FALSE(minHeap.empty());
        ASSERT_FALSE(maxHeap.empty());

        // Extract and check sorting
        ASSERT_EQ(minHeap.size(), N);
        ASSERT_EQ(maxHeap.size(), N);
        for (size_t i = 0; i < N; i++) {
            // Extract and check root of min-heap
            const T expectedMinHeapObject = T(outputNodesMinHeap[i].m_objectHash);
            ASSERT_EQ(minHeap.rootKey(), outputNodesMinHeap[i].m_key);
            ASSERT_EQ(minHeap.rootData().m_hash, outputNodesMinHeap[i].m_objectHash);
            ASSERT_TRUE(minHeap.contains(expectedMinHeapObject));
            minHeap.removeRoot();
            ASSERT_FALSE(minHeap.contains(expectedMinHeapObject));
            ASSERT_EQ(minHeap.size(), N - i - 1);

            // Extract and check root of max-heap
            const T expectedMaxHeapObject = T(outputNodesMaxHeap[i].m_objectHash);
            ASSERT_EQ(maxHeap.rootKey(), outputNodesMaxHeap[i].m_key);
            ASSERT_TRUE(maxHeap.contains(expectedMaxHeapObject));
            ASSERT_EQ(maxHeap.extractRoot().m_hash, outputNodesMaxHeap[i].m_objectHash);
            ASSERT_FALSE(maxHeap.contains(expectedMaxHeapObject));
            ASSERT_EQ(maxHeap.size(), N - i - 1);
        }
        ASSERT_EQ(minHeap.size(), 0);
        ASSERT_EQ(maxHeap.size(), 0);

        // Add again free nodes
        addInputNodesToHeap<T, MinHeap>(minHeap, N, false);
        addInputNodesToHeap<T, MaxHeap>(maxHeap, N, false);

        // Copy-construct and copy-assign
        MinHeap minHeapCopyConstructed = MinHeap(minHeap);
        ASSERT_EQ(minHeapCopyConstructed.branchingFactor(), minHeap.branchingFactor());
        ASSERT_EQ(minHeapCopyConstructed.size(), minHeap.size());
        MaxHeap maxHeapCopyAssigned(D + 1, 0, true);
        maxHeapCopyAssigned = maxHeap;
        ASSERT_EQ(maxHeapCopyAssigned.branchingFactor(), maxHeap.branchingFactor());
        ASSERT_EQ(maxHeapCopyAssigned.size(), maxHeap.size());

        // Move-construct and move-assign
        MinHeap minHeapMoveConstructed = MinHeap(std::move(minHeapCopyConstructed));
        ASSERT_EQ(minHeapMoveConstructed.branchingFactor(), minHeap.branchingFactor());
        ASSERT_EQ(minHeapMoveConstructed.size(), minHeap.size());
        MaxHeap maxHeapMoveAssigned(D + 1, 0, true);
        maxHeapMoveAssigned = std::move(maxHeapCopyAssigned);
        ASSERT_EQ(maxHeapMoveAssigned.branchingFactor(), maxHeap.branchingFactor());
        ASSERT_EQ(maxHeapMoveAssigned.size(), maxHeap.size());

        // Clear
        minHeapMoveConstructed.clear(false);
        ASSERT_EQ(minHeapMoveConstructed.size(), 0);
        maxHeapMoveAssigned.clear(true);
        ASSERT_EQ(maxHeapMoveAssigned.size(), 0);

        // Set branching factor
        ASSERT_GT(minHeap.size(), 0);
        const size_t newBranchingFactor = minHeap.branchingFactor() + 1;
        minHeap.setBranchingFactor(newBranchingFactor);
        ASSERT_EQ(minHeap.branchingFactor(), newBranchingFactor);
        ASSERT_EQ(minHeap.size(), 0);
        minHeap.setBranchingFactor(0);
        ASSERT_EQ(minHeap.branchingFactor(), 2);
        ASSERT_EQ(minHeap.size(), 0);

        // Try to insert duplicates (has to fail)
        ASSERT_EQ(maxHeap.size(), N);
        addInputNodesToHeap<T, MaxHeap>(maxHeap, N, true);
        ASSERT_EQ(maxHeap.size(), N);

        // Remove existing/missing nodes
        ASSERT_EQ(maxHeap.size(), N);
        removeInputNodesFromHeap<T, MaxHeap>(maxHeap, N, false);
        ASSERT_EQ(maxHeap.size(), 0);
        removeInputNodesFromHeap<T, MaxHeap>(maxHeap, N, true);
        ASSERT_EQ(maxHeap.size(), 0);

        // Re-create heaps
        minHeap = MinHeap(D, 128, true);
        maxHeap = MaxHeap(D, 128, true);
        MinHeap emptyMinHeap = MinHeap(D, 128, true);
        MaxHeap emptyMaxHeap = MaxHeap(D, 128, true);
        addInputNodesToHeap<T, MinHeap>(minHeap, N, false);
        addInputNodesToHeap<T, MaxHeap>(maxHeap, N, false);
        ASSERT_EQ(minHeap.size(), N);
        ASSERT_EQ(maxHeap.size(), N);
        ASSERT_EQ(emptyMinHeap.size(), 0);
        ASSERT_EQ(emptyMaxHeap.size(), 0);

        // Invert keys to invert priority order
        for (size_t i = 0; i < N; i++) {
            const T object = T(i);
            ASSERT_TRUE(minHeap.updateKey(-inputNodes[i].m_key, object));
            ASSERT_TRUE(maxHeap.updateKey(-inputNodes[i].m_key, object));
            ASSERT_FALSE(emptyMinHeap.updateKey(-inputNodes[i].m_key, object));
            ASSERT_FALSE(emptyMaxHeap.updateKey(-inputNodes[i].m_key, object));
        }

        // Extract and check sorting (inverse order)
        ASSERT_EQ(minHeap.size(), N);
        ASSERT_EQ(maxHeap.size(), N);
        for (size_t i = 0; i < N; i++) {
            // Extract and check root of min-heap
            const T expectedMinHeapObject = T(outputNodesMaxHeap[i].m_objectHash);
            ASSERT_EQ(minHeap.rootKey(), -outputNodesMaxHeap[i].m_key);
            ASSERT_EQ(minHeap.rootData().m_hash, outputNodesMaxHeap[i].m_objectHash);
            ASSERT_TRUE(minHeap.contains(expectedMinHeapObject));
            minHeap.removeRoot();
            ASSERT_FALSE(minHeap.contains(expectedMinHeapObject));
            ASSERT_EQ(minHeap.size(), N - i - 1);

            // Extract and check root of max-heap
            const T expectedMaxHeapObject = T(outputNodesMinHeap[i].m_objectHash);
            ASSERT_EQ(maxHeap.rootKey(), -outputNodesMinHeap[i].m_key);
            ASSERT_TRUE(maxHeap.contains(expectedMaxHeapObject));
            ASSERT_EQ(maxHeap.extractRoot().m_hash, outputNodesMinHeap[i].m_objectHash);
            ASSERT_FALSE(maxHeap.contains(expectedMaxHeapObject));
            ASSERT_EQ(maxHeap.size(), N - i - 1);
        }
        ASSERT_EQ(minHeap.size(), 0);
        ASSERT_EQ(maxHeap.size(), 0);
    }
}

TEST(ImplicitDaryHeap, NonEigen)
{
    checkGenericHeap<NonEigenObject<0>, std::allocator<ImplicitDaryHeapNode<double, NonEigenObject<0>>>>();
    checkGenericHeap<NonEigenObject<1>, std::allocator<ImplicitDaryHeapNode<double, NonEigenObject<1>>>>();
    checkGenericHeap<NonEigenObject<7>, std::allocator<ImplicitDaryHeapNode<double, NonEigenObject<7>>>>();
    checkGenericHeap<NonEigenObject<8>, std::allocator<ImplicitDaryHeapNode<double, NonEigenObject<8>>>>();
    checkGenericHeap<NonEigenObject<9>, std::allocator<ImplicitDaryHeapNode<double, NonEigenObject<9>>>>();
}

#ifdef HAVE_EIGEN3
TEST(ImplicitDaryHeap, Eigen)
{
    checkGenericHeap<EigenObject<0>, Eigen::aligned_allocator<ImplicitDaryHeapNode<double, EigenObject<0>>>>();
    checkGenericHeap<EigenObject<1>, Eigen::aligned_allocator<ImplicitDaryHeapNode<double, EigenObject<1>>>>();
    checkGenericHeap<EigenObject<7>, Eigen::aligned_allocator<ImplicitDaryHeapNode<double, EigenObject<7>>>>();
    checkGenericHeap<EigenObject<8>, Eigen::aligned_allocator<ImplicitDaryHeapNode<double, EigenObject<8>>>>();
    checkGenericHeap<EigenObject<9>, Eigen::aligned_allocator<ImplicitDaryHeapNode<double, EigenObject<9>>>>();
}
#endif
