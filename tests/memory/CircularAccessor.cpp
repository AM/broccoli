/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/CircularAccessor.hpp"
#include "gtest/gtest.h"

using namespace broccoli::memory;

TEST(CircularAccessor, Construction)
{
    std::vector<int> storage(2);
    CircularAccessor accessor(1, 3);
    EXPECT_EQ(accessor.access(storage).count(), 2);
    EXPECT_EQ(accessor.tailElementIndex(), 1);
}

TEST(CircularAccessor, ElementAccess)
{
    std::vector<int> storage;
    storage.push_back(1);
    storage.push_back(2);

    CircularAccessor accessor(1, 3);

    EXPECT_EQ(accessor.access(storage)[0], 1);
    EXPECT_EQ(accessor.access(storage)[1], 2);

    accessor.access(storage).pushFront(3);
    EXPECT_EQ(accessor.access(storage)[0], 3);
    EXPECT_EQ(accessor.access(storage)[1], 1);
    EXPECT_EQ(accessor.access(storage).count(), 2);

    storage.push_back(4);
    EXPECT_EQ(accessor.access(storage)[0], 4);
    EXPECT_EQ(accessor.access(storage)[1], 3);
    EXPECT_EQ(accessor.access(storage)[2], 1);
    EXPECT_EQ(accessor.access(storage).count(), 2);

    accessor.access(storage).pushFront(5);
    EXPECT_EQ(accessor.access(storage).at(0), 5);
    EXPECT_EQ(accessor.access(storage).at(1), 4);
    EXPECT_EQ(accessor.access(storage).at(2), 3);
    EXPECT_EQ(accessor.access(storage).count(), 3);
}

TEST(CircularAccessor, ConstContainer)
{
    std::vector<int> storage;
    storage.push_back(1);
    storage.push_back(2);

    CircularAccessor accessor(1, 3);

    const std::vector<int>& constRef = storage;

    EXPECT_EQ(accessor.access(constRef)[0], 1);
}
