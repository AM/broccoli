/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/PoolAllocator.hpp"
#include "gtest/gtest.h"
#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#endif

using namespace broccoli;
using namespace memory;

class TestObject {
public:
    //! Constructor
    TestObject()
    {
        m_pointerToMySelf = this;
        constructorCalls()++;
    }

    //! Destructor
    ~TestObject()
    {
        m_pointerToMySelf = nullptr;
        destructorCalls()++;
    }

    TestObject* m_pointerToMySelf; //!< Stores a pointer to itself

    //! Counter for calls of constructor of this class
    static inline uint64_t& constructorCalls()
    {
        static uint64_t calls = 0;
        return calls;
    }

    //! Counter for calls of destructor of this class
    static inline uint64_t& destructorCalls()
    {
        static uint64_t calls = 0;
        return calls;
    }

    //! Reset call counter for
    static inline void resetCallCounters()
    {
        constructorCalls() = 0;
        destructorCalls() = 0;
    }
};

//! Test non-eigen object
template <unsigned int ByteCount>
class NonEigenObject : public TestObject {
public:
    NonEigenObject()
    {
        for (uint8_t i = 0; i < ByteCount; i++)
            m_data[i] = i;
    }
    std::array<uint8_t, ByteCount> m_data; //!< Dummy data container
};

#ifdef HAVE_EIGEN3
//! Test eigen object
template <unsigned int ByteCount>
class EigenObject : public TestObject {
public:
    EigenObject()
    {
        for (uint8_t i = 0; i < ByteCount; i++)
            m_data[i] = i;
    }
    Eigen::Matrix<uint8_t, ByteCount, 1> m_data; //!< Dummy data container
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
};
#endif

//! Check generic allocator
template <typename T, typename Allocator>
static void checkGenericAllocator()
{
    for (size_t objectsPerBlock = 1; objectsPerBlock <= 10; objectsPerBlock++) {
        for (int c = 0; c < 2; c++) {
            const bool constructObjects = (c == 0) ? false : true;
            for (int d = 0; d < 2; d++) {
                const bool destructObjects = (d == 0) ? false : true;

                // Reset call counters
                T::resetCallCounters();
                ASSERT_EQ(T::constructorCalls(), 0);
                ASSERT_EQ(T::destructorCalls(), 0);

                // Construct empty allocator
                PoolAllocator<T, Allocator> emptyAllocator;
                ASSERT_EQ(emptyAllocator.blockCount(), 0);
                ASSERT_EQ(emptyAllocator.capacity(), 0);
                ASSERT_EQ(T::constructorCalls(), 0);
                ASSERT_EQ(T::destructorCalls(), 0);

                // Test setters and getters
                PoolAllocator<T, Allocator> allocatorForSetterGetter(objectsPerBlock, constructObjects, destructObjects, objectsPerBlock);
                ASSERT_EQ(allocatorForSetterGetter.objectsPerBlock(), objectsPerBlock);
                allocatorForSetterGetter.setObjectsPerBlock(objectsPerBlock + 1);
                ASSERT_EQ(allocatorForSetterGetter.objectsPerBlock(), objectsPerBlock + 1);
                allocatorForSetterGetter.setObjectsPerBlock(0);
                ASSERT_EQ(allocatorForSetterGetter.objectsPerBlock(), 1);
                ASSERT_EQ(allocatorForSetterGetter.constructObjects(), constructObjects);
                allocatorForSetterGetter.setConstructObjects(!constructObjects);
                ASSERT_EQ(allocatorForSetterGetter.constructObjects(), !constructObjects);
                ASSERT_EQ(allocatorForSetterGetter.destructObjects(), destructObjects);
                allocatorForSetterGetter.setDestructObjects(!destructObjects);
                ASSERT_EQ(allocatorForSetterGetter.destructObjects(), !destructObjects);

                // Construct pre-allocated allocator
                PoolAllocator<T, Allocator> allocator(objectsPerBlock, constructObjects, destructObjects, objectsPerBlock);
                ASSERT_EQ(allocator.objectsPerBlock(), objectsPerBlock);
                ASSERT_EQ(allocator.constructObjects(), constructObjects);
                ASSERT_EQ(allocator.destructObjects(), destructObjects);
                ASSERT_EQ(allocator.blockCount(), 1);
                ASSERT_EQ(allocator.capacity(), objectsPerBlock);
                ASSERT_EQ(T::constructorCalls(), 0);
                ASSERT_EQ(T::destructorCalls(), 0);

                // Step 1: fill up with objects (2 blocks)
                std::vector<T*> objects;
                objects.reserve(2 * objectsPerBlock);
                for (size_t b = 0; b < 2; b++) {
                    for (size_t o = 0; o < objectsPerBlock; o++) {
                        objects.push_back(allocator.allocate());
                        if (constructObjects == true)
                            ASSERT_EQ(T::constructorCalls(), objects.size());
                        else
                            ASSERT_EQ(T::constructorCalls(), 0);
                        ASSERT_EQ(T::destructorCalls(), 0);
                    }
                    ASSERT_EQ(allocator.blockCount(), b + 1);
                }
                ASSERT_EQ(allocator.blockCount(), 2);
                ASSERT_EQ(allocator.capacity(), 2 * objectsPerBlock);

                // Step 2: check, if no object got overwritten
                if (constructObjects == true) {
                    for (size_t i = 0; i < objects.size(); i++) {
                        ASSERT_EQ(objects[i]->m_pointerToMySelf, objects[i]);
                        for (uint8_t j = 0; j < objects[i]->m_data.size(); j++)
                            ASSERT_EQ(objects[i]->m_data[j], j);
                    }
                }

                // Step 3: Move-construct and move-assign
                PoolAllocator<T, Allocator> movedAllocator = std::move(allocator);
                ASSERT_EQ(movedAllocator.blockCount(), 2);
                allocator = std::move(movedAllocator);
                ASSERT_EQ(allocator.blockCount(), 2);

                // Step 4: remove every second object
                for (size_t i = 0; i < objects.size(); i++) {
                    if (i % 2 == 1) {
                        allocator.deallocate(objects[i]);
                        if (destructObjects == true)
                            ASSERT_EQ(T::destructorCalls(), (i + 1) / 2);
                        else
                            ASSERT_EQ(T::destructorCalls(), 0);
                    }
                }
                if (constructObjects == true)
                    ASSERT_EQ(T::constructorCalls(), objects.size());
                else
                    ASSERT_EQ(T::constructorCalls(), 0);
                if (destructObjects == true)
                    ASSERT_EQ(T::destructorCalls(), objects.size() / 2);
                else
                    ASSERT_EQ(T::destructorCalls(), 0);

                // Step 5: check resetting allocator
                ASSERT_EQ(allocator.blockCount(), 2);
                ASSERT_EQ(allocator.capacity(), 2 * objectsPerBlock);
                allocator.reset();
                ASSERT_EQ(allocator.blockCount(), 0);
                ASSERT_EQ(allocator.capacity(), 0 * objectsPerBlock);
                if (constructObjects == true)
                    ASSERT_EQ(T::constructorCalls(), objects.size());
                else
                    ASSERT_EQ(T::constructorCalls(), 0);
                if (destructObjects == true)
                    ASSERT_EQ(T::destructorCalls(), objects.size());
                else
                    ASSERT_EQ(T::destructorCalls(), 0);
            }
        }
    }
}

//! Test for a non-eigen allocator
template <unsigned int ByteCount>
static void checkNonEigenAllocator()
{
    checkGenericAllocator<NonEigenObject<ByteCount>, std::allocator<NonEigenObject<ByteCount>>>();
}

//! Test of PoolAllocator for non-eigen objects
TEST(PoolAllocator, NonEigen)
{
    checkNonEigenAllocator<0>();
    checkNonEigenAllocator<1>();
    checkNonEigenAllocator<7>();
    checkNonEigenAllocator<8>();
    checkNonEigenAllocator<9>();
}

#ifdef HAVE_EIGEN3
//! Test for a eigen allocator
template <unsigned int ByteCount>
static void checkEigenAllocator()
{
    checkGenericAllocator<EigenObject<ByteCount>, Eigen::aligned_allocator<EigenObject<ByteCount>>>();
}

//! Test of PoolAllocator for eigen objects
TEST(PoolAllocator, Eigen)
{
    checkEigenAllocator<0>();
    checkEigenAllocator<1>();
    checkEigenAllocator<7>();
    checkEigenAllocator<8>();
    checkEigenAllocator<9>();
}
#endif
