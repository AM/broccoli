/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/memory/SmartVector.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace memory;

// Validity check
// --------------
//! Checks, if the given SmartVector is valid (i.e. dynamic buffer should only be allocated, if static buffer is full)
template <class ElementType, size_t StaticBufferSize>
inline bool smartVectorIsValid(const SmartVector<ElementType, StaticBufferSize>& vector)
{
    // Check, if static buffer is full
    if (vector.staticElementCount() < StaticBufferSize) {
        // ...no, static buffer is not full -> dynamic buffer has to be empty
        if (vector.dynamicBuffer().size() > 0)
            return false;
    } else if (vector.staticElementCount() > StaticBufferSize) {
        // ...count of elements in static buffer is invalid
        return false;
    }
    // else: m_staticElementCount == MaximumStaticElementCount --> dynamic buffer may be anything (always valid)

    // No error found -> valid!
    return true;
}

//! Template function to perform construction and basic handling for vectors with different element types
template <class ElementType, size_t StaticBufferSize>
inline void smartVectorCheckConstructionAndBasicUsage(const ElementType& elementA, const ElementType& elementB)
{
    // Run tests for multiple element counts
    for (size_t elementCount = 0; elementCount < 10; elementCount++) {
        // Construction
        // ------------
        // Default constructor (no parameters)
        SmartVector<ElementType, StaticBufferSize> vectorA;

        // Specialized constructor (count)
        SmartVector<ElementType, StaticBufferSize> vectorB(elementCount);

        // Specialized constructor (count, value)
        SmartVector<ElementType, StaticBufferSize> vectorC(elementCount, elementA);

        // Specialized constructor (initializer list, 1 element)
        SmartVector<ElementType, StaticBufferSize> vectorD{ elementA };

        // Specialized constructor (initializer list, 2 elements)
        SmartVector<ElementType, StaticBufferSize> vectorE{ elementA, elementB };

        // Copy constructor
        SmartVector<ElementType, StaticBufferSize> vectorF(vectorE);
        ASSERT_EQ(vectorF.size(), 2);
        ASSERT_TRUE(vectorF == vectorE);

        // Copy assignment
        SmartVector<ElementType, StaticBufferSize> vectorG;
        ASSERT_EQ(vectorG.size(), 0);
        vectorG = vectorE;
        ASSERT_EQ(vectorG.size(), 2);
        ASSERT_TRUE(vectorG == vectorE);

        // Move constructor
        ASSERT_EQ(vectorF.size(), 2);
        SmartVector<ElementType, StaticBufferSize> vectorH = std::move(vectorF);
        ASSERT_EQ(vectorH.size(), 2);
        ASSERT_TRUE(vectorH == vectorE);

        // Move assignment
        ASSERT_EQ(vectorG.size(), 2);
        SmartVector<ElementType, StaticBufferSize> vectorI;
        vectorI = std::move(vectorG);
        ASSERT_EQ(vectorI.size(), 2);
        ASSERT_TRUE(vectorI == vectorE);

        // Test validity check
        // -------------------
        ASSERT_TRUE((smartVectorIsValid<ElementType, StaticBufferSize>(vectorA)));
        ASSERT_TRUE((smartVectorIsValid<ElementType, StaticBufferSize>(vectorB)));
        ASSERT_TRUE((smartVectorIsValid<ElementType, StaticBufferSize>(vectorC)));
        ASSERT_TRUE((smartVectorIsValid<ElementType, StaticBufferSize>(vectorD)));
        ASSERT_TRUE((smartVectorIsValid<ElementType, StaticBufferSize>(vectorE)));

        // Operators
        // ---------
        // Equality operator
        ASSERT_EQ(vectorE == (SmartVector<ElementType, StaticBufferSize>{ elementA, elementB }), true);
        ASSERT_EQ(vectorE == (SmartVector<ElementType, StaticBufferSize>{ elementB, elementA }), false);

        // Inequality operator
        ASSERT_EQ(vectorE != (SmartVector<ElementType, StaticBufferSize>{ elementA, elementB }), false);
        ASSERT_EQ(vectorE != (SmartVector<ElementType, StaticBufferSize>{ elementB, elementA }), true);

        // Test element access
        // -------------------
        const SmartVector<ElementType, StaticBufferSize> constVectorC = vectorC;
        const SmartVector<ElementType, StaticBufferSize> constVectorD = vectorD;
        const SmartVector<ElementType, StaticBufferSize> constVectorE = vectorE;
        for (size_t i = 0; i < elementCount; i++) {
            // Check at()
            ElementType element_at_i = vectorC.at(i);
            ASSERT_EQ(element_at_i == elementA, true);

            // Check at() const
            const ElementType const_element_at_i = constVectorC.at(i);
            ASSERT_EQ(const_element_at_i == elementA, true);

            // Check []
            ElementType element_i = vectorC[i];
            ASSERT_EQ(element_i == elementA, true);

            // Check [] const
            const ElementType const_element_i = constVectorC[i];
            ASSERT_EQ(const_element_i == elementA, true);
        }

        // Check at()
        ElementType elementD_at_0 = vectorD.at(0);
        ASSERT_EQ(elementD_at_0 == elementA, true);
        ElementType elementE_at_0 = vectorE.at(0);
        ASSERT_EQ(elementE_at_0 == elementA, true);
        ElementType elementE_at_1 = vectorE.at(1);
        ASSERT_EQ(elementE_at_1 == elementB, true);

        // Check at() const
        const ElementType const_elementD_at_0 = constVectorD.at(0);
        ASSERT_EQ(const_elementD_at_0 == elementA, true);
        const ElementType const_elementE_at_0 = constVectorE.at(0);
        ASSERT_EQ(const_elementE_at_0 == elementA, true);
        const ElementType const_elementE_at_1 = constVectorE.at(1);
        ASSERT_EQ(const_elementE_at_1 == elementB, true);

        // Check []
        ElementType elementD_0 = vectorD[0];
        ASSERT_EQ(elementD_0 == elementA, true);
        ElementType elementE_0 = vectorE[0];
        ASSERT_EQ(elementE_0 == elementA, true);
        ElementType elementE_1 = vectorE[1];
        ASSERT_EQ(elementE_1 == elementB, true);

        // Check [] const
        const ElementType const_elementD_0 = constVectorD[0];
        ASSERT_EQ(const_elementD_0 == elementA, true);
        const ElementType const_elementE_0 = constVectorE[0];
        ASSERT_EQ(const_elementE_0 == elementA, true);
        const ElementType const_elementE_1 = constVectorE[1];
        ASSERT_EQ(const_elementE_1 == elementB, true);

        // Check front()
        ElementType frontE = vectorE.front();
        ASSERT_EQ(frontE == elementA, true);

        // Check front() const
        const ElementType const_frontE = constVectorE.front();
        ASSERT_EQ(const_frontE == elementA, true);

        // Check back()
        ElementType backE = vectorE.back();
        ASSERT_EQ(backE == elementB, true);

        // Check back() const
        const ElementType const_backE = constVectorE.back();
        ASSERT_EQ(const_backE == elementB, true);

        // Test capacity
        // -------------
        // Check empty()
        ASSERT_EQ(vectorA.empty(), true);
        if (elementCount == 0) {
            ASSERT_EQ(vectorB.empty(), true);
            ASSERT_EQ(vectorC.empty(), true);
        } else {
            ASSERT_EQ(vectorB.empty(), false);
            ASSERT_EQ(vectorC.empty(), false);
        }
        ASSERT_EQ(vectorD.empty(), false);
        ASSERT_EQ(vectorE.empty(), false);

        // Check staticBuffer_size()
        ASSERT_EQ(vectorA.staticBuffer_size(), 0);
        if (elementCount >= StaticBufferSize) {
            ASSERT_EQ(vectorB.staticBuffer_size(), StaticBufferSize);
            ASSERT_EQ(vectorC.staticBuffer_size(), StaticBufferSize);
        } else {
            ASSERT_EQ(vectorB.staticBuffer_size(), elementCount);
            ASSERT_EQ(vectorC.staticBuffer_size(), elementCount);
        }
        if (1 >= StaticBufferSize) {
            ASSERT_EQ(vectorD.staticBuffer_size(), StaticBufferSize);
        } else {
            ASSERT_EQ(vectorD.staticBuffer_size(), 1);
        }
        if (2 >= StaticBufferSize) {
            ASSERT_EQ(vectorE.staticBuffer_size(), StaticBufferSize);
        } else {
            ASSERT_EQ(vectorE.staticBuffer_size(), 2);
        }

        // Check dynamicBuffer_size()
        ASSERT_EQ(vectorA.dynamicBuffer_size(), 0);
        if (elementCount >= StaticBufferSize) {
            ASSERT_EQ(vectorB.dynamicBuffer_size(), elementCount - StaticBufferSize);
            ASSERT_EQ(vectorC.dynamicBuffer_size(), elementCount - StaticBufferSize);
        } else {
            ASSERT_EQ(vectorB.dynamicBuffer_size(), 0);
            ASSERT_EQ(vectorC.dynamicBuffer_size(), 0);
        }
        if (1 >= StaticBufferSize) {
            ASSERT_EQ(vectorD.dynamicBuffer_size(), 1 - StaticBufferSize);
        } else {
            ASSERT_EQ(vectorD.dynamicBuffer_size(), 0);
        }
        if (2 >= StaticBufferSize) {
            ASSERT_EQ(vectorE.dynamicBuffer_size(), 2 - StaticBufferSize);
        } else {
            ASSERT_EQ(vectorE.dynamicBuffer_size(), 0);
        }

        // Check size
        ASSERT_EQ(vectorA.size(), vectorA.staticBuffer_size() + vectorA.dynamicBuffer_size());
        ASSERT_EQ(vectorB.size(), vectorB.staticBuffer_size() + vectorB.dynamicBuffer_size());
        ASSERT_EQ(vectorC.size(), vectorC.staticBuffer_size() + vectorC.dynamicBuffer_size());
        ASSERT_EQ(vectorD.size(), vectorD.staticBuffer_size() + vectorD.dynamicBuffer_size());
        ASSERT_EQ(vectorE.size(), vectorE.staticBuffer_size() + vectorE.dynamicBuffer_size());

        // Check staticBuffer_max_size()
        ASSERT_EQ(vectorA.staticBuffer_max_size(), StaticBufferSize);
        ASSERT_EQ(vectorB.staticBuffer_max_size(), StaticBufferSize);
        ASSERT_EQ(vectorC.staticBuffer_max_size(), StaticBufferSize);
        ASSERT_EQ(vectorD.staticBuffer_max_size(), StaticBufferSize);
        ASSERT_EQ(vectorE.staticBuffer_max_size(), StaticBufferSize);

        // Check max_size()
        ASSERT_EQ(vectorA.max_size(), vectorA.staticBuffer_max_size() + vectorA.dynamicBuffer_max_size());
        ASSERT_EQ(vectorB.max_size(), vectorB.staticBuffer_max_size() + vectorB.dynamicBuffer_max_size());
        ASSERT_EQ(vectorC.max_size(), vectorC.staticBuffer_max_size() + vectorC.dynamicBuffer_max_size());
        ASSERT_EQ(vectorD.max_size(), vectorD.staticBuffer_max_size() + vectorD.dynamicBuffer_max_size());
        ASSERT_EQ(vectorE.max_size(), vectorE.staticBuffer_max_size() + vectorE.dynamicBuffer_max_size());

        // Check capacity()
        vectorE.reserve(100);
        ASSERT_EQ(vectorE.capacity() >= 100, true);

        // Check shrink_to_fit
        // Warning: according to the c++ standard it is not guaranteed, that the capacity is reduced!
        vectorE.shrink_to_fit();
        ASSERT_EQ(vectorE.size(), 2);

        // Test modifiers
        // --------------
        SmartVector<ElementType, StaticBufferSize> vectorX;
        ASSERT_EQ(vectorX.size(), 0);

        // Test push_back()
        vectorX.push_back(elementA);
        ASSERT_EQ(vectorX.size(), 1);
        ASSERT_EQ(vectorX[0] == elementA, true);
        vectorX.push_back(elementB);
        ASSERT_EQ(vectorX.size(), 2);
        ASSERT_EQ(vectorX[0] == elementA, true);
        ASSERT_EQ(vectorX[1] == elementB, true);

        // Test emplace_back() (Trick: use copy-constructor of elements)
        vectorX.clear();
        ASSERT_EQ(vectorX.size(), 0);
        vectorX.emplace_back(elementA);
        ASSERT_EQ(vectorX.size(), 1);
        vectorX.emplace_back(elementB);
        ASSERT_EQ(vectorX.size(), 2);
        ASSERT_EQ(vectorX[0] == elementA, true);
        ASSERT_EQ(vectorX[1] == elementB, true);

        // Test fill()
        vectorX.fill(elementA);
        ASSERT_EQ(vectorX.size(), 2);
        ASSERT_EQ(vectorX[0] == elementA, true);
        ASSERT_EQ(vectorX[1] == elementA, true);

        // Test pop_back()
        vectorX[0] = elementA;
        vectorX[1] = elementB;
        vectorX.pop_back();
        ASSERT_EQ(vectorX.size(), 1);
        ASSERT_EQ(vectorX[0] == elementA, true);

        // Test clear()
        vectorX.clear();
        ASSERT_EQ(vectorX.size(), 0);

        // Test resize()
        vectorX.resize(1);
        ASSERT_EQ(vectorX.size(), 1);

        // Test resize() with value
        vectorX.resize(1, elementB);
        ASSERT_EQ(vectorX[1] == elementB, true);
    }
}

//! Check creation and basic handling for integer element types
TEST(SmartVector, ConstructionAndBasicUsageInteger)
{
    const int elementA = 1;
    const int elementB = -2;
    smartVectorCheckConstructionAndBasicUsage<int, 0>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<int, 1>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<int, 2>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<int, 3>(elementA, elementB);
}

//! Check creation and basic handling for floating point (double precision) element types
TEST(SmartVector, ConstructionAndBasicUsageDouble)
{
    const double elementA = 1.23;
    const double elementB = -2.34;
    smartVectorCheckConstructionAndBasicUsage<double, 0>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<double, 1>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<double, 2>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<double, 3>(elementA, elementB);
}

//! Check creation and basic handling for string element types
TEST(SmartVector, ConstructionAndBasicUsageString)
{
    const std::string elementA = "abc";
    const std::string elementB = "defg";
    smartVectorCheckConstructionAndBasicUsage<std::string, 0>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<std::string, 1>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<std::string, 2>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<std::string, 3>(elementA, elementB);
}

#ifdef HAVE_EIGEN3
//! Check creation and basic handling for Eigen element types
TEST(SmartVector, ConstructionAndBasicUsageEigen)
{
    const Eigen::Vector2d elementA = Eigen::Vector2d(1.23, -2.34);
    const Eigen::Vector2d elementB = Eigen::Vector2d(-3.45, 4.56);
    smartVectorCheckConstructionAndBasicUsage<Eigen::Vector2d, 0>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<Eigen::Vector2d, 1>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<Eigen::Vector2d, 2>(elementA, elementB);
    smartVectorCheckConstructionAndBasicUsage<Eigen::Vector2d, 3>(elementA, elementB);
}
#endif // HAVE_EIGEN3

//! Template funciton to test functionality with different iterator types
template <typename Container, typename Iterator>
static inline void checkIterators(const Container& container, const Iterator& begin, const Iterator& end)
{
    // Initialize helpers
    double out = 0.0;
    bool b = false;
    int delta = 0;

    // Default constructor
    Iterator defaultIterator;
    EXPECT_THROW(out = *defaultIterator, std::runtime_error);

    // Invalid construction
    EXPECT_THROW(typename Container::ConstIterator invalidIterator(container, container.size() + 1), std::out_of_range);

    // Create iterators for all positions
    Iterator iteratorDefault = begin;
    Iterator iterator0 = begin + 0;
    Iterator iterator1 = begin + 1;
    Iterator iterator2 = begin + 2;
    Iterator iterator3 = begin + 3;
    Iterator iterator4 = begin + 4;

    // Comparison operators
    EXPECT_THROW(b = defaultIterator == iteratorDefault, std::runtime_error);
    ASSERT_TRUE(iteratorDefault == iterator0);
    ASSERT_TRUE(iteratorDefault != iterator1);
    ASSERT_TRUE(iterator1 < iterator2);
    ASSERT_FALSE(iterator2 < iterator1);
    ASSERT_TRUE(iterator1 <= iterator2);
    ASSERT_FALSE(iterator2 <= iterator1);
    ASSERT_TRUE(iterator4 > iterator3);
    ASSERT_FALSE(iterator3 > iterator4);
    ASSERT_TRUE(iterator4 >= iterator3);
    ASSERT_FALSE(iterator3 >= iterator4);

    // Dereferencing
    EXPECT_THROW(out = defaultIterator.operator*(), std::runtime_error);
    EXPECT_THROW(out = *(defaultIterator.operator->()), std::runtime_error);
    EXPECT_THROW(out = defaultIterator.operator[](0), std::runtime_error);
    ASSERT_EQ(iterator2.operator*(), container[2]);
    ASSERT_EQ(*(iterator3.operator->()), container[3]);
    ASSERT_EQ(iterator3.operator[](1), container[4]);
    ASSERT_EQ(iterator3.operator[](-1), container[2]);
#ifdef NDEBUG
    EXPECT_THROW(out = iteratorDefault.operator[](-1), std::out_of_range);
    EXPECT_THROW(out = iteratorDefault.operator[](container.size()), std::out_of_range);
    EXPECT_THROW(out = iteratorDefault.operator[](container.size() + 1), std::out_of_range);
#endif

    // Incrementing
    Iterator runningIterator = begin;
    ASSERT_EQ(runningIterator.operator*(), container[0]);
    ASSERT_EQ((runningIterator++).operator*(), container[0]); // Postfix
    ASSERT_EQ(runningIterator.operator*(), container[1]);
    ASSERT_EQ((++runningIterator).operator*(), container[2]); // Prefix
    ASSERT_EQ(runningIterator.operator*(), container[2]);
    ASSERT_EQ((runningIterator--).operator*(), container[2]); // Postfix
    ASSERT_EQ(runningIterator.operator*(), container[1]);
    ASSERT_EQ((--runningIterator).operator*(), container[0]); // Prefix
    ASSERT_EQ(runningIterator.operator*(), container[0]);
    runningIterator = begin + container.size();
    EXPECT_THROW(runningIterator.operator++(), std::out_of_range);
    runningIterator = begin;
    EXPECT_THROW(runningIterator.operator--(), std::out_of_range);

    // Addition
    runningIterator = begin + 1;
    ASSERT_TRUE(runningIterator == iterator1);
    runningIterator = 2 + begin;
    ASSERT_TRUE(runningIterator == iterator2);
    runningIterator += 1;
    ASSERT_TRUE(runningIterator == iterator3);
    EXPECT_THROW(runningIterator = (begin + (container.size() + 1)), std::out_of_range);

    // Subtraction
    runningIterator = (end - 1);
    ASSERT_TRUE(runningIterator == iterator4);
    runningIterator -= 1;
    ASSERT_TRUE(runningIterator == iterator3);
    EXPECT_THROW(runningIterator += (-100), std::out_of_range);

    // Difference
    ASSERT_EQ(iterator4 - iterator4, 0);
    ASSERT_EQ(iterator4 - iterator3, 1);
    ASSERT_EQ(iterator4 - iterator2, 2);
    ASSERT_EQ(iterator4 - iterator1, 3);
    ASSERT_EQ(iterator3 - iterator4, -1);
    ASSERT_EQ(iterator2 - iterator4, -2);
    ASSERT_EQ(iterator1 - iterator4, -3);
    EXPECT_THROW(delta = defaultIterator - iterator1, std::runtime_error);
}

//! Check usage of iterators
TEST(SmartVector, Iterators)
{
    using Container = SmartVector<double, 3, std::allocator<double>>;
    Container nonConstContainer{ 1.2, -2.3, 3.4, -4.5, 5.6 };
    const Container constContainer = nonConstContainer;

    // Check const iterators
    checkIterators<Container, Container::ConstIterator>(constContainer, constContainer.cbegin(), constContainer.cend());

    // Check non-const iterators
    checkIterators<Container, Container::Iterator>(nonConstContainer, nonConstContainer.begin(), nonConstContainer.end());

    // Check lvalue dereferencing
    Container::Iterator nonConstIterator = nonConstContainer.begin();
    ASSERT_EQ(nonConstContainer[0], 1.2);
    nonConstIterator.operator*() = 2.1;
    ASSERT_EQ(nonConstContainer[0], 2.1);
    *(nonConstIterator.operator->()) = 7.8;
    ASSERT_EQ(nonConstContainer[0], 7.8);
    nonConstIterator.operator[](2) = 1.2345;
    ASSERT_EQ(nonConstContainer[2], 1.2345);
}
