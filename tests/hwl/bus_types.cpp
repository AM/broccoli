/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/bus_types.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace ::testing;

template <typename ObjectIdentifierType>
struct StaticTestObjectIdentifier {
    static_assert(std::is_class<ObjectIdentifierType>::value, "");
    static_assert(std::is_member_function_pointer<decltype(&ObjectIdentifierType::toString)>::value, "");
    static_assert(std::is_member_function_pointer<decltype(&ObjectIdentifierType::operator==)>::value, "");
    static_assert(std::is_member_function_pointer<decltype(&ObjectIdentifierType::operator!=)>::value, "");
    static_assert(std::is_same<decltype(ObjectIdentifierType::random()), ObjectIdentifierType>::value, "");
};

template <typename BusType>
struct StaticTestBusTypes {
    static_assert(std::is_class<typename BusType::StateType>::value, "");

    struct in : StaticTestObjectIdentifier<typename BusType::InputObjectIdentifierType> {
    };
    struct out : StaticTestObjectIdentifier<typename BusType::OutputObjectIdentifierType> {
    };
    struct ain : StaticTestObjectIdentifier<typename BusType::AsyncInputObjectIdentifierType> {
    };
    struct aout : StaticTestObjectIdentifier<typename BusType::AsyncOutputObjectIdentifierType> {
    };
};

TEST(BusTypes, CANopen)
{
    struct StaticTest : StaticTestBusTypes<CANopen> {
    };

    CANopen::ObjectIdentifierType identifier{ 0x6000 };
    EXPECT_EQ(identifier.toString(), std::string("idx 0x6000"));

    CANopen::AsyncObjectIdentifierType asyncIdentifier{ 0x6000, 0x02 };
    EXPECT_EQ(asyncIdentifier.toString(), std::string("idx 0x6000, subidx 0x02"));

    std::hash<CANopen::ObjectIdentifierType> hash;
    std::hash<CANopen::AsyncObjectIdentifierType> asyncHash;

    EXPECT_NE(hash(identifier), hash(CANopen::ObjectIdentifierType{ 0x3000 }));
    EXPECT_EQ(hash(CANopen::ObjectIdentifierType{ 0x8124 }), hash(CANopen::ObjectIdentifierType{ 0x08124 }));

    EXPECT_NE(asyncHash(asyncIdentifier), asyncHash(CANopen::AsyncObjectIdentifierType{ 0x6000, 0x03 }));
    EXPECT_NE(asyncHash(asyncIdentifier), asyncHash(CANopen::AsyncObjectIdentifierType{ 0x7000, 0x02 }));
    EXPECT_EQ(asyncHash(CANopen::AsyncObjectIdentifierType{ 0x8124, 0x10 }), asyncHash(CANopen::AsyncObjectIdentifierType{ 0x8124, 0x10 }));
}

TEST(BusTypes, CAN)
{
    struct StaticTest : StaticTestBusTypes<CAN> {
    };

    CAN::InputMessageIdentifierType identifier{ 0x606060, 0x12 };
    EXPECT_EQ(identifier.toString(), std::string("id 0x606060, offset 0x12"));

    CAN::OutputMessageIdentifierType oIdentifier{ 0x050323 };
    EXPECT_EQ(oIdentifier.toString(), std::string("id 0x50323"));

    CAN::RemoteFrameIdentifierType rtfIdentifier{ 0x050326 };
    EXPECT_EQ(rtfIdentifier.toString(), std::string("id 0x50326"));

    std::hash<CAN::InputMessageIdentifierType> hash;
    std::hash<CAN::OutputMessageIdentifierType> oHash;
    std::hash<CAN::RemoteFrameIdentifierType> rtfHash;

    EXPECT_NE(hash(identifier), hash(CAN::InputMessageIdentifierType{ 0x4033000, 0x30 }));
    EXPECT_EQ(hash(CAN::InputMessageIdentifierType{ 0x813324, 0x30 }), hash(CAN::InputMessageIdentifierType{ 0x0813324, 0x30 }));

    EXPECT_NE(oHash(oIdentifier), oHash(CAN::OutputMessageIdentifierType{ 0x3643000 }));
    EXPECT_EQ(oHash(oIdentifier), oHash(CAN::OutputMessageIdentifierType{ 0x050323 }));

    EXPECT_NE(rtfHash(rtfIdentifier), rtfHash(CAN::RemoteFrameIdentifierType{ 0x3643000 }));
    EXPECT_EQ(rtfHash(rtfIdentifier), rtfHash(CAN::RemoteFrameIdentifierType{ 0x050326 }));
}

TEST(BusTypes, EtherCAT)
{
    struct StaticTest : StaticTestBusTypes<EtherCAT> {
    };

    EtherCAT::ObjectIdentifierType identifier{ "superVariable" };
    EXPECT_EQ(identifier.toString(), std::string("superVariable"));

    EtherCAT::AsyncObjectIdentifierType asyncIdentifier{ 0x050, 0x02 };
    EXPECT_EQ(asyncIdentifier.toString(), std::string("idx 0x0050, subidx 0x02"));

    std::hash<EtherCAT::ObjectIdentifierType> hash;

    EXPECT_NE(hash(identifier), hash(EtherCAT::ObjectIdentifierType{ "super" }));
    EXPECT_EQ(hash(EtherCAT::ObjectIdentifierType{ "key" }), hash(EtherCAT::ObjectIdentifierType{ "key" }));
}
