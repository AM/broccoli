/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/bus_types.hpp"
#include "broccoli/hwl/testing/BusVariableRegistryTest.hpp"
#include "broccoli/hwl/testing/FakeBusVariableRegistry.hpp"
#include "broccoli/hwl/variables.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

//! Registry to test TestSuite for BusVariableRegistry (weird ha?)
template <typename BusType>
struct BusVariableRegistryAsyncSupportOnly : public BusVariableRegistryBase<BusVariableRegistryAsyncSupportOnly<BusType>> {

    template <typename T>
    void registerVariable(AsyncInputBusVariable<T>&,
        const typename BusType::AsyncObjectIdentifierType&)
    {
    }

    template <typename T>
    void registerVariable(AsyncOutputBusVariable<T>&,
        const typename BusType::AsyncObjectIdentifierType&)
    {
    }
};

using BusVariableRegistryTestAsyncOnly = BusVariableRegistryTest<BusVariableRegistryAsyncSupportOnly<EtherCAT>, EtherCAT>;
TEST_F(BusVariableRegistryTestAsyncOnly, registerVariables)
{
    // Tests test of registries with reduced support
    m_device.linkVariablesASync(m_registry);
}

using FakeBusVariableRegistryTest = BusVariableRegistryTest<FakeBusVariableRegistry<EtherCAT>, EtherCAT>;
TEST_F(FakeBusVariableRegistryTest, FullVariableInterface)
{
    m_device.linkVariables(m_registry);
}
