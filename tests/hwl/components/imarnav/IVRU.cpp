/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/imarnav/IVRU.hpp"
#include "broccoli/hwl/bus_types/CAN.hpp"
#include "broccoli/hwl/testing/BusDeviceTest.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

using IVRUCAN = BusDeviceTest<IVRU<CAN>>;

TEST_F(IVRUCAN, StatusFailure)
{
    this->m_busDriver.requestState(CAN::StateType::op());

    this->dut().setTimeout(1);
    m_busDriver.registry().setSyncInputVariable(CAN::InputMessageIdentifierType{ 0, 0 }, 1);
    this->process();
    EXPECT_FALSE(this->dut().onFault());
    this->process();
    EXPECT_TRUE(this->dut().onFault());
}

TEST_F(IVRUCAN, CANMode)
{
    this->m_busDriver.requestState(CAN::StateType::op());

    EXPECT_FALSE(dut().accelerationsAvailable());
    EXPECT_FALSE(dut().omegaAvailable());
    EXPECT_FALSE(dut().rpyAvailable());

    uint32_t canMode = 1 | (1 << 1) | (1 << 5) | (1 << 9) | (1 << 19);
    m_busDriver.registry().setSyncInputVariable(CAN::InputMessageIdentifierType{ 0, 4 }, canMode);
    this->process();

    EXPECT_TRUE(dut().accelerationsAvailable());
    EXPECT_TRUE(dut().omegaAvailable());
    EXPECT_TRUE(dut().rpyAvailable());
}

TEST_F(IVRUCAN, DataConversion)
{
    this->m_busDriver.requestState(CAN::StateType::op());

    int16_t value = -3234;
    m_busDriver.registry().setSyncInputVariable(CAN::InputMessageIdentifierType{ 2, 2 }, value);
    m_busDriver.registry().setSyncInputVariable(CAN::InputMessageIdentifierType{ 4, 2 }, value);
    m_busDriver.registry().setSyncInputVariable(CAN::InputMessageIdentifierType{ 6, 2 }, value);
    this->process();

    EXPECT_DOUBLE_EQ(dut().accelerationX(), 0.01 * value);
    EXPECT_DOUBLE_EQ(dut().omegaX(), 0.003 * M_PI / 180.0 * value);
    EXPECT_DOUBLE_EQ(dut().roll(), M_PI / 180.0 * value);
}
