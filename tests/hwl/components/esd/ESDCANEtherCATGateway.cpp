/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/esd/ESDCANEtherCATGateway.hpp"
#include "broccoli/hwl/testing/BusGatewayTest.hpp"
#include "broccoli/hwl/testing/MockBusDevice.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

using ESDCANGateway = BusGatewayTest<ESDCANEtherCATGateway, EtherCAT, CAN>;
using ESDCANGatewayPriority = BusGatewayTest<ESDCANEtherCATGatewayPQ, EtherCAT, CAN>;

//! Provides methods to fake the ESD gateway device
struct FakeESDGateway {

    FakeESDGateway()
    {
        setupTestMessage();
    }

    void setupTestMessage(uint8_t payloadSize = 2)
    {
        bool RTRBit = false;
        uint16_t payLoadSizeRTRId = (payloadSize & 0xf) | (RTRBit ? (1 << 4) : 0) | ((CAN::InputMessageIdentifierType::random().messageIdentifier & 0x7ff) << 5);
        rxMessage.rawBytes[0] = payLoadSizeRTRId & 0xff;
        rxMessage.rawBytes[1] = (payLoadSizeRTRId & 0xff00) >> 8;
        rxMessage.rawBytes[2] = 0x12;
        rxMessage.rawBytes[3] = 0x34;
        rxMessage.rawBytes[4] = 0x56;
        rxMessage.rawBytes[5] = 0x78;
        rxMessage.rawBytes[6] = 0x9A;
        rxMessage.rawBytes[7] = 0xbc;
        rxMessage.rawBytes[8] = 0xde;
        rxMessage.rawBytes[9] = 0xff;
    }

    void fakeRXMessage(FakeBusVariableRegistry<EtherCAT>& registry)
    {
        registry.setSyncInputVariable({ "CAN TxPDO-Map.Number of RX Messages" }, 1);
        registry.setSyncInputVariable<uint16_t>({ "CAN TxPDO-Map.RX Counter" }, registry.syncOutputVariable<uint16_t>({ "CAN RxPDO-Map.RX Counter" }) + 1);
        registry.setSyncInputVariable({ "CAN TxPDO-Map.RX Message 1" }, rxMessage);
    }

    void validateRXHandling(FakeBusVariableRegistry<EtherCAT>& registry)
    {
        // working counter must match
        ASSERT_EQ(registry.syncOutputVariable<uint16_t>({ "CAN RxPDO-Map.RX Counter" }), registry.syncInputVariable<uint16_t>({ "CAN TxPDO-Map.RX Counter" }));
    }

    void ackTXMessages(FakeBusVariableRegistry<EtherCAT>& registry, int nrOfExpectedMessages = -1)
    {
        size_t nrOfMessages = registry.syncOutputVariable<uint16_t>({ "CAN RxPDO-Map.Number of TX Messages" });

        if (nrOfExpectedMessages >= 0) {
            EXPECT_EQ(nrOfMessages, nrOfExpectedMessages);
        }
        if (nrOfMessages != 0) {
            hwl::ESDTxQueueEntry txMessage = registry.syncOutputVariable<hwl::ESDTxQueueEntry>({ "CAN RxPDO-Map.TX Message " + std::to_string(nrOfMessages) });
            registry.setSyncInputVariable({ "CAN TxPDO-Map.TX Transaction Number" }, txMessage.transactionNumber());
            ackTXCounterOnly(registry);
        }
    }

    void ackTXCounterOnly(FakeBusVariableRegistry<EtherCAT>& registry)
    {
        uint16_t txCounter = registry.syncOutputVariable<uint16_t>({ "CAN RxPDO-Map.TX Counter" });
        ASSERT_NE(txCounter, registry.syncInputVariable<uint16_t>({ "CAN TxPDO-Map.TX Counter" }));
        registry.setSyncInputVariable({ "CAN TxPDO-Map.TX Counter" }, txCounter);
    }

    void rxQueueOverflow(FakeBusVariableRegistry<EtherCAT>& registry, bool value = true)
    {
        registry.setSyncInputVariable({ "CAN Status PDO.RX Overflow" }, value);
    }

    hwl::ESDRxQueueEntry rxMessage;
};

TEST_F(ESDCANGateway, ProcessStep)
{
    this->m_busDriver.requestState(CAN::StateType::op());

    auto&& deviceMock = dut().makeDevice<MockBusDevice<CAN>>();
    dut().addDevice(deviceMock, 0);
    EXPECT_CALL(deviceMock, processDevice()).Times(1);
    EXPECT_CALL(deviceMock, onStateChange()).Times(1);

    EXPECT_EQ(dut().state(), CAN::StateType::unknown());
    this->process();

    EXPECT_EQ(dut().cycleTimeInUs(), 1000);
    EXPECT_EQ(dut().state(), m_busDriver.state());
}

TEST_F(ESDCANGateway, MessageCycleSync)
{
    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::safeOp());
    this->process();
    this->m_busDriver.requestState(CAN::StateType::op());
    // The downstreamDevice does copy input to output data
    // This allows to validate the complete data path by injecting known values
    // via the upstream registry

    fake.fakeRXMessage(m_busDriver.registry());
    this->process();

    EXPECT_EQ(this->loopback().m_syncInput, static_cast<const char*>(fake.rxMessage.payload())[1]);
    fake.validateRXHandling(m_busDriver.registry());
    fake.ackTXMessages(m_busDriver.registry(), 2);

    hwl::ESDTxQueueEntry txMessage{};
    txMessage = m_busDriver.registry().syncOutputVariable<hwl::ESDTxQueueEntry>({ "CAN RxPDO-Map.TX Message 1" });
    EXPECT_EQ(*static_cast<char*>(txMessage.payload()), static_cast<const char*>(fake.rxMessage.payload())[1]);
    EXPECT_EQ(txMessage.rawBytes[2] & 0x7, 1); // payload size
}

TEST_F(ESDCANGateway, MessageCycleSyncRXCounterOverflow)
{
    FakeESDGateway fake;
    // Set the RX counter to definitely overflow
    this->m_busDriver.registry().setSyncInputVariable({ "CAN TxPDO-Map.RX Counter" }, std::numeric_limits<uint16_t>::max());
    this->m_busDriver.requestState(CAN::StateType::safeOp());
    this->process();
    this->m_busDriver.requestState(CAN::StateType::op());
    // The downstreamDevice does copy input to output data
    // This allows to validate the complete data path by injecting known values
    // via the upstream registry

    fake.fakeRXMessage(m_busDriver.registry());
    this->process();

    ASSERT_EQ(this->loopback().m_syncInput, static_cast<const char*>(fake.rxMessage.payload())[1]);
    fake.validateRXHandling(m_busDriver.registry());
    fake.ackTXMessages(m_busDriver.registry(), 2);

    hwl::ESDTxQueueEntry txMessage{};
    txMessage = m_busDriver.registry().syncOutputVariable<hwl::ESDTxQueueEntry>({ "CAN RxPDO-Map.TX Message 1" });
    ASSERT_EQ(*static_cast<char*>(txMessage.payload()), static_cast<const char*>(fake.rxMessage.payload())[1]);
    ASSERT_EQ(txMessage.rawBytes[2] & 0x7, 1); // payload size
}

TEST_F(ESDCANGateway, MessageCycleSyncTXCounterOverflow)
{
    FakeESDGateway fake;
    // Set the RX counter to definitely overflow
    this->m_busDriver.registry().setSyncInputVariable({ "CAN TxPDO-Map.TX Counter" }, std::numeric_limits<uint16_t>::max());
    this->m_busDriver.requestState(CAN::StateType::safeOp());
    this->process();
    this->m_busDriver.requestState(CAN::StateType::op());
    // The downstreamDevice does copy input to output data
    // This allows to validate the complete data path by injecting known values
    // via the upstream registry

    fake.fakeRXMessage(m_busDriver.registry());
    this->process();

    ASSERT_EQ(this->loopback().m_syncInput, static_cast<const char*>(fake.rxMessage.payload())[1]);
    fake.validateRXHandling(m_busDriver.registry());
    fake.ackTXMessages(m_busDriver.registry(), 2);

    hwl::ESDTxQueueEntry txMessage{};
    txMessage = m_busDriver.registry().syncOutputVariable<hwl::ESDTxQueueEntry>({ "CAN RxPDO-Map.TX Message 1" });
    ASSERT_EQ(*static_cast<char*>(txMessage.payload()), static_cast<const char*>(fake.rxMessage.payload())[1]);
    ASSERT_EQ(txMessage.rawBytes[2] & 0x7, 1); // payload size
}

TEST_F(ESDCANGatewayPriority, MessageCycleSyncPriorityQueue)
{
    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::op());
    // The downstreamDevice does copy input to output data
    // This allows to validate the complete data path by injecting known values
    // via the upstream registry

    fake.fakeRXMessage(m_busDriver.registry());
    this->process();

    EXPECT_EQ(this->loopback().m_syncInput, static_cast<const char*>(fake.rxMessage.payload())[1]);
    fake.validateRXHandling(m_busDriver.registry());
    fake.ackTXMessages(m_busDriver.registry(), 2);

    hwl::ESDTxQueueEntry txMessage{};
    txMessage = m_busDriver.registry().syncOutputVariable<hwl::ESDTxQueueEntry>({ "CAN RxPDO-Map.TX Message 1" });
    EXPECT_EQ(*static_cast<char*>(txMessage.payload()), static_cast<const char*>(fake.rxMessage.payload())[1]);
    EXPECT_EQ(txMessage.rawBytes[2] & 0x7, 1); // payload size
}

TEST_F(ESDCANGateway, MessageCycleAsync)
{
    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::op());

    // The downstreamDevice does copy input to output data
    // This allows to validate the complete data path by injecting known values
    // via the upstream registry

    this->process();
    EXPECT_FALSE(dut().onFault());

    fake.ackTXMessages(m_busDriver.registry(), 2);
    fake.fakeRXMessage(m_busDriver.registry());

    this->process();
    EXPECT_FALSE(dut().onFault());

    fake.ackTXMessages(m_busDriver.registry(), 2);
    fake.fakeRXMessage(m_busDriver.registry());

    hwl::ESDTxQueueEntry txMessage{};
    txMessage = m_busDriver.registry().syncOutputVariable<hwl::ESDTxQueueEntry>({ "CAN RxPDO-Map.TX Message 1" });
    EXPECT_EQ(*static_cast<char*>(txMessage.payload()), static_cast<const char*>(fake.rxMessage.payload())[1]);
    EXPECT_EQ(txMessage.rawBytes[2] & 0x7, 1); // payload size

    txMessage = m_busDriver.registry().syncOutputVariable<hwl::ESDTxQueueEntry>({ "CAN RxPDO-Map.TX Message 2" });
    EXPECT_EQ(*static_cast<char*>(txMessage.payload()), static_cast<const char*>(fake.rxMessage.payload())[1]);
    EXPECT_EQ(txMessage.rawBytes[2] & 0x7, 1); // payload size
}

TEST_F(ESDCANGateway, MessageCycleLoop)
{
    // This is mainly designed to simulate an overflowing transaction number.
    // However, all other counters also will overflow during this test

    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::op());

    // The downstreamDevice does copy input to output data
    // This allows to validate the complete data path by injecting known values
    // via the upstream registry

    for (size_t i = 0; i < 70000; i++) {
        this->process();
        ASSERT_FALSE(dut().onFault());
        ASSERT_FALSE(this->loopback().m_asyncInput.retryError());
        ASSERT_FALSE(this->loopback().m_asyncOutput.retryError());

        fake.ackTXMessages(m_busDriver.registry(), 2);
        fake.fakeRXMessage(m_busDriver.registry());
    }
}

TEST_F(ESDCANGateway, AsyncTimeout)
{
    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::op());

    dut().setAsyncCommunicationTimeout(0);

    this->process();
    fake.ackTXCounterOnly(m_busDriver.registry());

    this->process();
    fake.ackTXCounterOnly(m_busDriver.registry());

    this->process();

    EXPECT_TRUE(this->loopback().m_asyncInput.unsafeContainerRef().hasFailed());
    EXPECT_TRUE(this->loopback().m_asyncOutput.unsafeContainerRef().hasFailed());
}

TEST_F(ESDCANGateway, TXWorkingCounterMismatch)
{
    // Do not output errors
    io::ConsoleOptions options;
    options.setLoggingLevelConsole(100);
    io::Console::configure(options);

    FakeESDGateway fake;

    this->m_busDriver.requestState(CAN::StateType::op());
    this->process();
    EXPECT_FALSE(dut().onFault());

    // Takes 10 cycles at last for ack!
    for (int i = 0; i < 9; i++)
        this->process();

    EXPECT_FALSE(dut().onFault());
    this->process();
    EXPECT_TRUE(dut().onFault());

    dut().resetFault();
    this->process();
    EXPECT_FALSE(dut().onFault());
}

TEST_F(ESDCANGateway, TXWorkingCounterMismatchSafeOp)
{
    FakeESDGateway fake;

    this->m_busDriver.requestState(CAN::StateType::safeOp());
    this->process();
    EXPECT_FALSE(dut().onFault());

    this->process();
    EXPECT_FALSE(dut().onFault());

    this->m_busDriver.requestState(CAN::StateType::op());
    this->process();
    EXPECT_FALSE(dut().onFault());
}

TEST_F(ESDCANGateway, RXQueueOverflow)
{
    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::op());

    fake.rxQueueOverflow(m_busDriver.registry());
    this->process();
    EXPECT_TRUE(dut().onFault());
    EXPECT_EQ(dut().state(), CAN::StateType::safeOp());

    fake.ackTXMessages(m_busDriver.registry());
    fake.rxQueueOverflow(m_busDriver.registry(), false);
    dut().resetFault();
    this->process();
    EXPECT_FALSE(dut().onFault());
}

TEST_F(ESDCANGateway, TXQueueOverflow)
{
    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::op());

    dut().setTXQueueSize(1);

    this->process();
    EXPECT_TRUE(dut().onFault());

    fake.ackTXMessages(m_busDriver.registry());
    dut().resetFault();
    dut().setTXQueueSize(16);
    this->process();
    EXPECT_FALSE(dut().onFault());
}

// Will assert in debug mode
#ifdef NDEBUG
TEST_F(ESDCANGateway, RXDataMismatch)
{
    FakeESDGateway fake;
    this->m_busDriver.requestState(CAN::StateType::op());

    fake.setupTestMessage(1);
    fake.fakeRXMessage(m_busDriver.registry());
    this->process();

    EXPECT_TRUE(dut().onFault());
}
#endif
