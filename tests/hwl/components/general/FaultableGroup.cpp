/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/general/FaultableGroup.hpp"
#include "broccoli/hwl/components/general/testing/MockFaultable.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

TEST(FaultableGroup, Interface)
{
    MockFaultable faultable1;
    MockFaultable faultable2;
    FaultableGroup group{ faultable1, faultable2 };

    MockFaultable faultable3;
    group.add(faultable3);

    EXPECT_CALL(faultable1, onFault).WillRepeatedly(Return(false));
    EXPECT_CALL(faultable2, onFault).WillRepeatedly(Return(false));
    EXPECT_CALL(faultable3, onFault).WillRepeatedly(Return(true));

    EXPECT_TRUE(group.onFault());

    EXPECT_CALL(faultable3, resetFault).Times(1);
    group.resetFault();
}
