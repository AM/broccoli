/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/general/SensorGroup.hpp"
#include "broccoli/hwl/components/general/testing/MockSensor.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

TEST(SensorGroup, Interface)
{
    MockSensor sensor1;
    MockSensor sensor2;
    SensorGroup group{ sensor1, sensor2 };

    MockSensor sensor3;
    group.add(sensor3);

    EXPECT_CALL(sensor1, ready).WillRepeatedly(Return(true));
    EXPECT_CALL(sensor2, ready).WillRepeatedly(Return(true));
    EXPECT_CALL(sensor3, ready).WillOnce(Return(false)).WillRepeatedly(Return(true));
    EXPECT_FALSE(group.ready());
    EXPECT_TRUE(group.ready());

    EXPECT_CALL(sensor1, zeroState).WillRepeatedly(Return(true));
    EXPECT_CALL(sensor2, zeroState).WillRepeatedly(Return(true));
    EXPECT_CALL(sensor3, zeroState).WillOnce(Return(false)).WillRepeatedly(Return(true));
    EXPECT_FALSE(group.zeroState());
    EXPECT_TRUE(group.zeroState());
}
