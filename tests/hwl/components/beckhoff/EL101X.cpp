/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/beckhoff/EL101X.hpp"
#include "broccoli/hwl/bus_types/EtherCAT.hpp"
#include "broccoli/hwl/testing/BusDeviceTest.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

using BeckhoffEL1012 = BusDeviceTest<EL1012<EtherCAT>>;

TEST_F(BeckhoffEL1012, DefaultVariables)
{
    for (DeviceType::IndexType i = 0; i < DeviceType::nrOfChannels(); i++) {
        EXPECT_TRUE(m_busDriver.registry().syncInputVariableRegistered(EtherCAT::ObjectIdentifierType{ DeviceType::channelVariableName(i + 1, "Input") }));
    }

    process();
}

TEST_F(BeckhoffEL1012, SettingInput)
{
    m_busDriver.registry().setSyncInputVariable(EtherCAT::ObjectIdentifierType{ "Channel 1.Input" }, true);
    EXPECT_TRUE(dut().input(1));
    EXPECT_FALSE(dut().input(2));
}
