/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/beckhoff/MultiChannelDevice.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace ::testing;

class BeckhoffMultiChannelDevice : public testing::Test, public MultiChannelDevice<4> {
public:
    using MultiChannelDevice<4, IndexType>::assertChannelIndex;
    using MultiChannelDevice<4, IndexType>::isValidChannelIndex;
};

TEST_F(BeckhoffMultiChannelDevice, Interface)
{
    EXPECT_EQ(BeckhoffMultiChannelDevice::nrOfChannels(), 4);
    static_assert(std::is_same<IndexType, BeckhoffMultiChannelDevice::IndexType>::value, "IndexType must be public");
}

TEST_F(BeckhoffMultiChannelDevice, VariableAndChannelName)
{
    EXPECT_EQ(BeckhoffMultiChannelDevice::channelName(4), "Channel 4");
    EXPECT_EQ(BeckhoffMultiChannelDevice::channelVariableName(4, "In"), "Channel 4.In");
}

TEST_F(BeckhoffMultiChannelDevice, IndexAssertion)
{
    EXPECT_FALSE(BeckhoffMultiChannelDevice::isValidChannelIndex(0));
    EXPECT_FALSE(BeckhoffMultiChannelDevice::isValidChannelIndex(5));
    EXPECT_TRUE(BeckhoffMultiChannelDevice::isValidChannelIndex(4));

    EXPECT_EQ(BeckhoffMultiChannelDevice::isValidChannelIndex(4), BeckhoffMultiChannelDevice::assertChannelIndex(4));
}
