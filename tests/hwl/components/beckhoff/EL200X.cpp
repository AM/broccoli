/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/beckhoff/EL200X.hpp"
#include "broccoli/hwl/bus_types/EtherCAT.hpp"
#include "broccoli/hwl/testing/BusDeviceTest.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;

using BeckhoffEL2004 = BusDeviceTest<EL2004<EtherCAT>>;

TEST_F(BeckhoffEL2004, DefaultVariablesAndLinking)
{
    EXPECT_EQ(DeviceType::nrOfChannels(), 4);
    for (DeviceType::IndexType i = 1; i <= DeviceType::nrOfChannels(); i++) {
        EXPECT_FALSE(m_busDriver.registry().syncOutputVariable<bool>(EtherCAT::ObjectIdentifierType{ DeviceType::channelVariableName(i, "Output") }));
    }

    process();
}

TEST_F(BeckhoffEL2004, SettingOutput)
{
    dut().setOutput(1, true);

    EXPECT_TRUE(m_busDriver.registry().syncOutputVariable<bool>(EtherCAT::ObjectIdentifierType{ "Channel 1.Output" }));

    EXPECT_TRUE(m_busDriver.registry().syncOutputVariable<bool>(EtherCAT::ObjectIdentifierType{ "Channel 1.Output" }));
    EXPECT_FALSE(m_busDriver.registry().syncOutputVariable<bool>(EtherCAT::ObjectIdentifierType{ "Channel 2.Output" }));

    dut().toggleOutput(1);
    EXPECT_FALSE(m_busDriver.registry().syncOutputVariable<bool>(EtherCAT::ObjectIdentifierType{ "Channel 1.Output" }));
}
