/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/components/ati/Axia80.hpp"
#include "broccoli/hwl/bus_types/EtherCAT.hpp"
#include "broccoli/hwl/testing/BusDeviceTest.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

using Axia80EtherCAT = BusDeviceTest<Axia80<EtherCAT>>;

TEST_F(Axia80EtherCAT, DefaultVariables)
{
    process();
}
