/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/type_traits.hpp"
#include "broccoli/hwl/variables.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace ::testing;

template <template <typename> class Variable, typename DataType>
struct TestBusVariablesProperties {
    using T = Variable<DataType>;

    // DataType
    static_assert(std::is_same<typename T::DataType, typename T::DataType>::value, "");

    // Construction
    static_assert(std::is_constructible<T, const typename T::DataType&>::value, "");
    static_assert(std::is_constructible<T, typename T::DataType&&>::value, "");
    static_assert(std::is_constructible<T>::value, "");

    // copy/move construction
    static_assert(std::is_copy_constructible<T>::value, "");
    static_assert(std::is_nothrow_move_constructible<T>::value, "");

    // implicit value conversion
    static_assert(std::is_convertible<T, typename T::DataType>::value, "");
};

template <typename DataType>
struct TestOutputBusVariablesProperties : public TestBusVariablesProperties<OutputBusVariable, DataType> {
    using T = OutputBusVariable<DataType>;

    // copy/move assignment
    static_assert(!std::is_nothrow_copy_assignable<T>::value, "");
    static_assert(!std::is_nothrow_move_assignable<T>::value, "");

    // value assignment
    static_assert(std::is_assignable<T, const typename T::DataType&>::value, "");
    static_assert(std::is_assignable<T, typename T::DataType&&>::value, "");

    static_assert(is_output_bus_variable<T>::value, "");
};

template <typename DataType>
struct TestInputBusVariablesProperties : public TestBusVariablesProperties<InputBusVariable, DataType> {
    using T = InputBusVariable<DataType>;

    // Not copy/move assignable
    static_assert(!std::is_copy_assignable<T>::value, "");
    static_assert(!std::is_move_assignable<T>::value, "");

    // Not value assignable
    static_assert(!std::is_assignable<T, const typename T::DataType&>::value, "");
    static_assert(!std::is_assignable<T, typename T::DataType&&>::value, "");

    static_assert(!is_output_bus_variable<T>::value, "");
};

struct testInterfaces {
    TestOutputBusVariablesProperties<uint8_t> t1;
    TestInputBusVariablesProperties<uint8_t> t2;
};

TEST(TestBusVariables, Sync)
{
    static_assert(is_sync_bus_variable<InputBusVariable<uint8_t>>::value, "");
    static_assert(!is_async_bus_variable<InputBusVariable<uint8_t>>::value, "");

    InputBusVariable<uint16_t> inputVariable(2);
    OutputBusVariable<uint16_t> outputVariable(10);

    outputVariable = inputVariable;

    EXPECT_EQ(outputVariable, 2);
    EXPECT_EQ(outputVariable.value(), 2);
    EXPECT_EQ(outputVariable.size(), sizeof(uint16_t));
}

TEST(TestBusVariables, ASync)
{
    static_assert(!is_sync_bus_variable<AsyncInputBusVariable<uint8_t>>::value, "");
    static_assert(is_async_bus_variable<AsyncOutputBusVariable<uint8_t>>::value, "");

    AsyncInputBusVariable<uint16_t> inputVariable(2);
    AsyncOutputBusVariable<uint16_t> outputVariable(10);

    outputVariable = inputVariable;

    EXPECT_EQ(outputVariable, 2);
    EXPECT_EQ(outputVariable.value(), 2);
    EXPECT_EQ(outputVariable.size(), sizeof(uint16_t));

    {
        auto guard = inputVariable.lockWithGuard();
        EXPECT_EQ(guard->isLatched(), false);
        EXPECT_EQ(guard->hasFinished(), false);
        EXPECT_EQ(guard->isPending(), false);
    }
}

TEST(TestBusVariables, SyncAsync)
{
    AsyncInputBusVariable<uint16_t> asyncInputVariable(1);
    AsyncOutputBusVariable<uint16_t> asyncOutputVariable(2);
    InputBusVariable<uint16_t> inputVariable(3);
    OutputBusVariable<uint16_t> outputVariable(4);

    asyncOutputVariable = inputVariable;
    outputVariable = asyncInputVariable;

    EXPECT_EQ(asyncOutputVariable, 3);
    EXPECT_EQ(outputVariable, 1);
}

TEST(TestBusVariables, Move)
{
    InputBusVariable<uint16_t> inputVariable(InputBusVariable<uint16_t>{2});
    EXPECT_EQ(inputVariable.value(), 2);
}
