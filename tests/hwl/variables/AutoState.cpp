/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/variables/AutoState.hpp"
#include "broccoli/hwl/bus_types/EtherCAT.hpp"
#include "broccoli/hwl/testing/MockBusDriverControl.hpp"
#include "broccoli/hwl/type_traits.hpp"
#include "broccoli/hwl/variables/pointers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace ::testing;

template <typename BusType>
class SyncAutoStateTestFixture : public ::testing::Test {
protected:
    using T = uint8_t;
    const T value = 4;

    SyncAutoStateTestFixture()
        : inputPtr(state.monitor())
        , outputPtr(state.desired())
    {
    }

    void simulateTransferFail()
    {
        outputPtr.unsafeRef().indicatePendingTransfer();
        outputPtr.unsafeRef().indicateFailedTransfer();
    }

    void simulateTransferSuccess()
    {
        outputPtr.unsafeRef().indicatePendingTransfer();
        outputPtr.unsafeRef().indicateSuccessfulTransfer();
    }

    void simulateMonitorFollowsDesired(bool follow)
    {
        if (follow)
            inputPtr.unsafeRef().template indirect<T>() = state.desired().value();
        else
            inputPtr.unsafeRef().template indirect<T>() = state.desired().value() + 1;
    }

    AutoState<InputBusVariable<T>, AsyncOutputBusVariable<T>> state;
    BusVariablePointer inputPtr;
    AsyncBusVariablePointer outputPtr;
    hwl::testing::MockBusDriverControl<BusType> control;
};

template <typename BusType>
class AsyncAutoStateTestFixture : public ::testing::Test {
protected:
    using T = uint8_t;
    const T value = 4;

    AsyncAutoStateTestFixture()
        : inputPtr(state.monitor())
        , outputPtr(state.desired())
    {
    }

    struct OutputTransfer {
    };
    struct MonitorTransfer {
    };

    void simulateTransferFail(const OutputTransfer&)
    {
        outputPtr.unsafeRef().indicatePendingTransfer();
        outputPtr.unsafeRef().indicateFailedTransfer();
    }

    void simulateTransferFail(const MonitorTransfer&)
    {
        inputPtr.unsafeRef().indicatePendingTransfer();
        inputPtr.unsafeRef().indicateFailedTransfer();
    }

    void simulateTransferSuccess(const OutputTransfer&)
    {
        outputPtr.unsafeRef().indicatePendingTransfer();
        outputPtr.unsafeRef().indicateSuccessfulTransfer();
    }

    void simulateTransferSuccess(const MonitorTransfer&)
    {
        inputPtr.unsafeRef().indicatePendingTransfer();
        inputPtr.unsafeRef().indicateSuccessfulTransfer();
    }

    void simulateMonitorFollowsDesired(bool follow)
    {
        if (follow)
            inputPtr.unsafeRef().template indirect<T>() = state.desired().value();
        else
            inputPtr.unsafeRef().template indirect<T>() = state.desired().value() + 1;
    }

    AutoState<AsyncInputBusVariable<T>, AsyncOutputBusVariable<T>> state;
    AsyncBusVariablePointer inputPtr;
    AsyncBusVariablePointer outputPtr;
    hwl::testing::MockBusDriverControl<BusType> control;
};

using SyncAutoStateTest = SyncAutoStateTestFixture<EtherCAT>;
TEST_F(SyncAutoStateTest, ValidCycle)
{
    // should not do anything
    state.dispatch(control);
    EXPECT_FALSE(state.isLatched());

    {
        EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(outputPtr))).Times(1);
        state.desired() = value;
        EXPECT_FALSE(state.dispatch(control));
        EXPECT_FALSE(state.isLatched());
    }

    simulateTransferSuccess();
    simulateMonitorFollowsDesired(true);
    EXPECT_TRUE(state.isLatched());

    EXPECT_TRUE(state.dispatch(control));
    EXPECT_FALSE(state.retryError());

    EXPECT_EQ(state.monitor(), state.desired());
}

TEST_F(SyncAutoStateTest, FailButMonitorMatches)
{
    {
        EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(outputPtr))).Times(1);
        state.desired() = value;
        EXPECT_FALSE(state.dispatch(control));
    }

    simulateTransferFail();
    simulateMonitorFollowsDesired(true);
    EXPECT_FALSE(state.isLatched());

    {
        EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(outputPtr))).Times(1);
        EXPECT_FALSE(state.dispatch(control));
        EXPECT_FALSE(state.isLatched());
    }
}

TEST_F(SyncAutoStateTest, SuccessNoMonitorMatch)
{
    {
        EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(outputPtr))).Times(1);
        state.desired() = value;
        EXPECT_FALSE(state.dispatch(control));
    }

    simulateTransferSuccess();
    simulateMonitorFollowsDesired(false);
    EXPECT_FALSE(state.isLatched());

    EXPECT_FALSE(state.dispatch(control));
    EXPECT_FALSE(state.isLatched());
}

using AsyncAutoStateTest = AsyncAutoStateTestFixture<EtherCAT>;
TEST_F(AsyncAutoStateTest, ValidCycle)
{
    // should not do anything
    state.dispatch(control);
    EXPECT_FALSE(state.isLatched());
    EXPECT_FALSE(state.retryError());

    {
        EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(outputPtr))).Times(1);
        state.desired() = value;
        EXPECT_FALSE(state.dispatch(control));
        EXPECT_FALSE(state.isLatched());
        simulateTransferSuccess(OutputTransfer{});
    }

    {
        EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(inputPtr))).Times(1);
        EXPECT_FALSE(state.dispatch(control));
        EXPECT_FALSE(state.isLatched());
        simulateTransferSuccess(MonitorTransfer{});
    }

    simulateMonitorFollowsDesired(true);
    EXPECT_TRUE(state.isLatched());

    EXPECT_TRUE(state.dispatch(control));
    EXPECT_FALSE(state.retryError());

    EXPECT_EQ(state.monitor(), state.desired());
}

TEST_F(AsyncAutoStateTest, OutputFailButMonitorMatches)
{
    EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(outputPtr))).Times(2);

    state.desired() = value;
    EXPECT_FALSE(state.dispatch(control));
    simulateTransferFail(OutputTransfer{});

    simulateMonitorFollowsDesired(true);
    EXPECT_FALSE(state.isLatched());

    EXPECT_FALSE(state.dispatch(control));
    EXPECT_FALSE(state.isLatched());
}

TEST_F(AsyncAutoStateTest, InputFailButMonitorMatches)
{
    EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(inputPtr))).Times(2);
    EXPECT_CALL(this->control, delegateTransfer(std::hash<AsyncBusVariablePointer>{}.operator()(outputPtr))).Times(1);

    state.desired() = value;
    EXPECT_FALSE(state.dispatch(control));
    simulateTransferSuccess(OutputTransfer{});
    EXPECT_FALSE(state.dispatch(control));
    simulateTransferFail(MonitorTransfer{});

    simulateMonitorFollowsDesired(true);
    EXPECT_FALSE(state.isLatched());

    EXPECT_FALSE(state.dispatch(control));
    EXPECT_FALSE(state.isLatched());
}

TEST_F(AsyncAutoStateTest, SuccessNoMonitorMatch)
{
    EXPECT_CALL(this->control, delegateTransfer(An<size_t>())).Times(2);

    state.desired() = value;
    EXPECT_FALSE(state.dispatch(control));
    simulateTransferSuccess(OutputTransfer{});
    EXPECT_FALSE(state.dispatch(control));
    simulateTransferSuccess(MonitorTransfer{});

    simulateMonitorFollowsDesired(false);
    EXPECT_FALSE(state.isLatched());

    EXPECT_FALSE(state.dispatch(control));
    EXPECT_FALSE(state.isLatched());
}
