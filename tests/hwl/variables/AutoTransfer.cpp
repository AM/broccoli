/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/variables/AutoTransfer.hpp"
#include "broccoli/hwl/bus_types/EtherCAT.hpp"
#include "broccoli/hwl/testing/MockBusDriverControl.hpp"
#include "broccoli/hwl/variables/pointers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace ::testing;

template <typename BusType, template <typename> class VariableType>
class AutoTransferTest : public ::testing::Test {
protected:
    const uint8_t value = 4;

    AutoTransferTest()
        : ptr(state)
    {
    }

    void simulateTransferFail()
    {
        ptr.unsafeRef().indicatePendingTransfer();
        ptr.unsafeRef().indicateFailedTransfer();
    }

    void simulateTransferSuccess()
    {
        ptr.unsafeRef().indicatePendingTransfer();
        ptr.unsafeRef().indicateSuccessfulTransfer();
    }

    AutoTransfer<VariableType<uint8_t>> state{ value };
    AsyncBusVariablePointer ptr;
    hwl::testing::MockBusDriverControl<BusType> control;
};

using AutoInputTransferTest = AutoTransferTest<EtherCAT, AsyncInputBusVariable>;
TEST_F(AutoInputTransferTest, ConceptAndDefaults)
{
    EXPECT_EQ(state, value);

    state.setMaxRetries(3);
    EXPECT_FALSE(state.retryError());
    EXPECT_FALSE(state.isLatched());
}

TEST_F(AutoInputTransferTest, PullAndRetry)
{
    EXPECT_CALL(this->control, delegateTransfer(An<size_t>())).Times(1);
    state.setMaxRetries(1);
    EXPECT_FALSE(state.isLatched());
    EXPECT_FALSE(state.dispatch(control));

    state.renew();
    EXPECT_TRUE(state.unsafeContainerRef().isUncertain());
    EXPECT_FALSE(state.dispatch(control));

    simulateTransferFail();
    EXPECT_TRUE(state.retryError());
    EXPECT_FALSE(state.dispatch(control));
    EXPECT_TRUE(state.retryError());
    EXPECT_FALSE(state.isLatched());
}

TEST_F(AutoInputTransferTest, PullSuccess)
{
    EXPECT_CALL(this->control, delegateTransfer(An<size_t>())).Times(1);
    state.renew();
    state.dispatch(control);
    simulateTransferSuccess();
    EXPECT_TRUE(state.dispatch(control));

    EXPECT_FALSE(state.retryError());
    EXPECT_TRUE(state.isLatched());
    EXPECT_FALSE(state.dispatch(control));
}

using AutoOutputTransferTest = AutoTransferTest<EtherCAT, AsyncOutputBusVariable>;
TEST_F(AutoOutputTransferTest, AssignAndRetry)
{
    EXPECT_CALL(this->control, delegateTransfer(An<size_t>())).Times(1);
    state.setMaxRetries(1);
    EXPECT_FALSE(state.isLatched());
    EXPECT_FALSE(state.dispatch(control));

    state = 42;
    EXPECT_TRUE(state.unsafeContainerRef().isUncertain());
    EXPECT_FALSE(state.dispatch(control));

    simulateTransferFail();
    EXPECT_TRUE(state.retryError());
    EXPECT_FALSE(state.dispatch(control));
    EXPECT_TRUE(state.retryError());
    EXPECT_FALSE(state.isLatched());
}

TEST_F(AutoOutputTransferTest, RenewSuccess)
{
    EXPECT_CALL(this->control, delegateTransfer(An<size_t>())).Times(1);
    state.renew();
    state.dispatch(control);
    simulateTransferSuccess();
    EXPECT_TRUE(state.dispatch(control));

    EXPECT_FALSE(state.retryError());
    EXPECT_TRUE(state.isLatched());
    EXPECT_FALSE(state.dispatch(control));
}
