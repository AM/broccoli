/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/bus_types.hpp"
#include "broccoli/hwl/type_traits.hpp"
#include "broccoli/hwl/variables.hpp"
#include "broccoli/hwl/variables/pointers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace ::testing;

template <typename BusVariableType_, typename BusVariableHandleType_>
struct TypeTuple {
    using BusVariableType = BusVariableType_;
    using BusVariableHandleType = BusVariableHandleType_;
};

template <typename Type>
class TestBusVariableHandle : public Test {
protected:
    const typename Type::BusVariableType::DataType m_value = 42;
    const typename Type::BusVariableType::DataType m_otherValue = 7;

    TestBusVariableHandle()
        : m_variable(m_value)
        , m_pointer(m_variable)
    {
    }

    using V = typename Type::BusVariableType;
    using H = typename Type::BusVariableHandleType;

    V m_variable;
    H m_pointer;

    void testAsyncInterface(BusVariablePointer&) {}

    void testAsyncInterface(AsyncBusVariablePointer&)
    {
        {
            auto lockedPtr = this->m_pointer.lockWithGuard();
            lockedPtr->indicatePendingTransfer();
        }
        {
            auto guard = this->m_variable.lockWithGuard();
            EXPECT_TRUE(guard->isPending());
        }
    }

    static_assert(std::is_constructible<H, V&>::value, "");
    static_assert(std::is_copy_constructible<H>::value, "");
    static_assert(std::is_nothrow_move_constructible<H>::value, "");
    static_assert(!std::is_copy_assignable<H>::value, "");
    static_assert(!std::is_nothrow_move_assignable<H>::value, "");
};

using TestTypesBusVariables = Types<TypeTuple<OutputBusVariable<uint8_t>, BusVariablePointer>,
    TypeTuple<InputBusVariable<uint16_t>, BusVariablePointer>,
    TypeTuple<AsyncInputBusVariable<uint16_t>, BusVariablePointer>,
    TypeTuple<AsyncOutputBusVariable<uint8_t>, BusVariablePointer>,
    TypeTuple<AsyncInputBusVariable<uint16_t>, AsyncBusVariablePointer>,
    TypeTuple<AsyncOutputBusVariable<uint8_t>, AsyncBusVariablePointer>>;

TYPED_TEST_SUITE(TestBusVariableHandle, TestTypesBusVariables);

TYPED_TEST(TestBusVariableHandle, TestTypesBusVariables)
{
    using T = typename TypeParam::BusVariableType::DataType;

    EXPECT_EQ(this->m_pointer.size(), sizeof(T));
    {
        auto guard = this->m_pointer.lockWithGuard();
        EXPECT_EQ(*((T*)guard->ptr()), this->m_value);

        *((T*)guard->ptr()) = this->m_otherValue;
    }
    EXPECT_EQ(*((T*)this->m_pointer.copy().ptr()), this->m_otherValue);

    this->testAsyncInterface(this->m_pointer);
}

TEST(TestBusVariablePointers, Hashing)
{

    AsyncInputBusVariable<uint8_t> input;
    AsyncBusVariablePointer inputPtr(input);

    EXPECT_EQ(std::hash<AsyncInputBusVariable<uint8_t>>{}(input),
        std::hash<AsyncBusVariablePointer>{}(inputPtr));
}

TEST(TestBusVariablePointers, copyTo)
{
    InputBusVariable<uint16_t> input{ 0x1234 };
    BusVariablePointer ptr(input);
    char buffer[3];

#ifdef NDEBUG
    EXPECT_FALSE(ptr.copyTo(buffer, 3, 2));
    EXPECT_EQ(buffer[2], 0x34);
#endif

    EXPECT_TRUE(ptr.copyTo(buffer, 3, 1));
    EXPECT_EQ(buffer[1], 0x34);
    EXPECT_EQ(buffer[2], 0x12);
}

TEST(TestBusVariablePointers, copyToReversed)
{
    InputBusVariable<uint16_t> input{ 0x1234 };
    BusVariablePointer ptr(input, !core::PlatformHelperFactory::create()->usesLittleEndian());
    char buffer[3];

    EXPECT_TRUE(ptr.copyTo(buffer, 3, 1));
    EXPECT_EQ(buffer[1], 0x12);
    EXPECT_EQ(buffer[2], 0x34);
}

TEST(TestBusVariablePointers, copyFrom)
{
    InputBusVariable<uint16_t> input;
    BusVariablePointer ptr(input);
    char buffer[3]{ 0x12, 0x34, 0x56 };

#ifdef NDEBUG
    EXPECT_FALSE(ptr.copyFrom(buffer, 3, 2));
    EXPECT_EQ(input, 0x56);
#endif

    EXPECT_TRUE(ptr.copyFrom(buffer, 3, 1));
    EXPECT_EQ((input & 0xff00) >> 8, 0x56);
    EXPECT_EQ(input & 0xff, 0x34);
}

TEST(TestBusVariablePointers, copyFromReversed)
{
    InputBusVariable<uint16_t> input;
    BusVariablePointer ptr(input, !core::PlatformHelperFactory::create()->usesLittleEndian());
    char buffer[3]{ 0x12, 0x34, 0x56 };

    EXPECT_TRUE(ptr.copyFrom(buffer, 3, 1));
    EXPECT_EQ((input & 0xff00) >> 8, 0x34);
    EXPECT_EQ(input & 0xff, 0x56);
}
