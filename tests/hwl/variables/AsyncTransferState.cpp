/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/variables/AsyncTransferState.hpp"
#include "broccoli/hwl/variables/AsyncTransferStateHandle.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace ::testing;

class TestAsyncTransferState : public Test {
protected:
    TestAsyncTransferState()
        : m_handle(m_state)
    {
    }

    AsyncTransferState m_state;
    AsyncTransferStateHandle m_handle;

    using T = AsyncTransferState;

    static_assert(std::is_constructible<T>::value, "");
    static_assert(std::is_nothrow_copy_constructible<T>::value, "");
    static_assert(std::is_nothrow_copy_assignable<T>::value, "");
    static_assert(std::is_nothrow_move_constructible<T>::value, "");
    static_assert(std::is_nothrow_move_assignable<T>::value, "");

    using U = AsyncTransferStateHandle;
    static_assert(std::is_constructible<U, T&>::value, "");
    static_assert(std::is_nothrow_copy_constructible<U>::value, "");
    static_assert(std::is_nothrow_copy_assignable<U>::value, "");
    static_assert(std::is_nothrow_move_constructible<U>::value, "");
    static_assert(std::is_nothrow_move_assignable<U>::value, "");
};

TEST_F(TestAsyncTransferState, Defaults)
{
    EXPECT_FALSE(this->m_state.hasFailed());
    EXPECT_FALSE(this->m_state.isLatched());
    EXPECT_FALSE(this->m_state.isPending());
    EXPECT_FALSE(this->m_state.hasFinished());

    EXPECT_FALSE(this->m_handle.isTransferPending());
}

TEST_F(TestAsyncTransferState, UpdateCycle)
{
    // Assume a prior transfer completed with failure
    this->m_handle.indicateFailedTransfer();
    EXPECT_TRUE(this->m_state.hasFinished());
    EXPECT_TRUE(this->m_state.hasFailed());
    EXPECT_FALSE(this->m_state.isLatched());

    this->m_state.markUncertain();
    EXPECT_TRUE(this->m_state.isUncertain());
    EXPECT_FALSE(this->m_handle.isTransferPending());
    EXPECT_FALSE(this->m_state.isPending());
    EXPECT_TRUE(this->m_state.hasFailed());
    EXPECT_FALSE(this->m_state.isLatched());

    this->m_handle.indicatePendingTransfer();
    EXPECT_TRUE(this->m_handle.isTransferPending());
    EXPECT_TRUE(this->m_state.isPending());
    EXPECT_FALSE(this->m_state.isLatched());
    EXPECT_FALSE(this->m_state.isUncertain());

    this->m_handle.indicateSuccessfulTransfer();
    EXPECT_FALSE(this->m_handle.isTransferPending());
    EXPECT_FALSE(this->m_state.isPending());
    EXPECT_TRUE(this->m_state.isLatched());
    EXPECT_TRUE(this->m_state.hasFinished());

    // still latched
    EXPECT_TRUE(this->m_state.isLatched());
}

TEST_F(TestAsyncTransferState, RequestDuringTransfer)
{
    // Assume a pending transfer
    this->m_handle.indicatePendingTransfer();

    this->m_state.markUncertain();
    this->m_handle.indicateSuccessfulTransfer();
    EXPECT_TRUE(this->m_state.isUncertain()); // still uncertain

    this->m_handle.indicateFailedTransfer();
    EXPECT_TRUE(this->m_state.hasFinished());
    EXPECT_FALSE(this->m_state.isLatched());
}
