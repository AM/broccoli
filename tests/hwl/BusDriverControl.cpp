/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/bus_types/EtherCAT.hpp"
#include "broccoli/hwl/testing/MockBusDriverControl.hpp"
#include "broccoli/hwl/variables.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

TEST(BusInterface, hashing)
{
    AsyncInputBusVariable<uint8_t> input;
    AsyncOutputBusVariable<uint8_t> output;

    MockBusDriverControl<EtherCAT> busInterface;

    EXPECT_CALL(busInterface, delegateTransfer(std::hash<AsyncInputBusVariable<uint8_t>>{}(input)));
    EXPECT_CALL(busInterface, delegateTransfer(std::hash<AsyncOutputBusVariable<uint8_t>>{}(output)));

    busInterface.transfer(input);
    busInterface.transfer(output);
}
