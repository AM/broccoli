/*
 * This file is part of broccoli.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/hwl/bus_types/EtherCAT.hpp"
#include "broccoli/hwl/testing/MockBusDevice.hpp"
#include "broccoli/hwl/testing/MockBusDriver.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace hwl;
using namespace hwl::testing;
using namespace ::testing;

TEST(BusAbstraction, process)
{
    hwl::testing::MockBusDriver<EtherCAT> bus;
    hwl::testing::MockBusDevice<EtherCAT> device(bus);
    hwl::testing::MockBusDevice<EtherCAT> anotherDevice(bus);
    hwl::BusDriver<EtherCAT>::DeviceContainerType devices;

    devices.push_back(device);
    devices.push_back(anotherDevice);

    EXPECT_CALL(bus, devices())
        .WillRepeatedly(ReturnRef(devices));

    EXPECT_CALL(bus, processBus())
        .Times(2);

    EXPECT_CALL(device, processDevice)
        .Times(2);
    EXPECT_CALL(device, onStateChange)
        .Times(1);

    EXPECT_CALL(anotherDevice, processDevice)
        .Times(2);
    EXPECT_CALL(anotherDevice, onStateChange)
        .Times(1);

    EXPECT_CALL(bus, state())
        .WillRepeatedly(Return(EtherCAT::StateType::init()));

    bus.process();
    bus.process();
}
