# Testing

Broccoli uses Google Test to run automated tests on the software.

## How to run tests

The tests are basically just executables, which are automatically built by the Google Test Suite. You may run those directly from the binary folder or use the following cmake target:
```make tests```.

Additionally, you can also run the tests and let the build system calculate the percentage of code lines in your tested module functions, which are actually executed within your tests:
```make coverage```.

The coverage report is printed to the command line. This basically lets you know if you tested all possible logical execution branches within the functions you called within your test. If the coverage is 100% all logical execution paths are executed at least once. Note that this won't show untested code within functions you didn't call at all. A detailed report which gives you information on the untested lines is located in "index.html" in the binary project folder.

## How to write tests

- Always write the unit test right after you implemented a software module.
- Black-Box testing is sufficient for most cases. Think of all possible equivalence classes (data categories for which the same logical path is executed), then implement your test cases.
- Writing tests is an effort and you may choose to not write tests for every software module. As a guideline: You can get the most out of testing if you write unit-tests at least for all modules on the lowest hierarchy level. These modules typically have the smallest interfaces (are easy to test) and errors in their implementation are the most difficult to find in a system test.

Broccoli uses google test for unit testing. The documentation is available [here](https://github.com/google/googletest/blob/master/googletest/docs/primer.md).

### Folder Structure

The folders in src/tests should match the modules of broccoli. Every module has its own cmake configuration and may use different libraries. Create a separate test file for every unit you want to test within one module.
