/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/control/Wrench.hpp"
#include "broccoli/core/floats.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;
using namespace broccoli::core;

namespace {

Wrench setupTestWrench()
{

    Wrench result = Wrench::Zero();
    result.referencePoint()(0) = 0.1;

    result.forceVector()(0) = 50.0;
    result.forceVector()(1) = 100.0;
    result.forceVector()(2) = 150.0;

    result.torqueVector()(0) = 1.0;
    result.torqueVector()(1) = 2.0;
    result.torqueVector()(2) = 3.0;

    return result;
}

TEST(Wrench, Construction)
{
    Eigen::Matrix<double, 6, 1> zero6D = Eigen::Matrix<double, 6, 1>::Zero();
    Eigen::Vector3d zero3D = Eigen::Vector3d::Zero();

    Wrench wrench = Wrench::Zero();
    EXPECT_EQ(wrench.vector(), zero6D);
    EXPECT_EQ(wrench.referencePoint(), zero3D);

    Wrench otherWrench(Eigen::Matrix<double, 6, 1>::Ones());
    EXPECT_TRUE(otherWrench.vector().isApprox(Eigen::Matrix<double, 6, 1>::Ones()));
}

TEST(Wrench, Getters)
{
    const Wrench wrench = setupTestWrench();
    EXPECT_EQ(wrench.forceVector()(0), 50.0);
    EXPECT_EQ(wrench.forceVector()(1), 100.0);
    EXPECT_EQ(wrench.forceVector()(2), 150.0);
    EXPECT_EQ(wrench.torqueVector()(0), 1.0);
    EXPECT_EQ(wrench.torqueVector()(1), 2.0);
    EXPECT_EQ(wrench.torqueVector()(2), 3.0);
}

TEST(Wrench, Comparison)
{
    Wrench wrench = setupTestWrench();
    Wrench wrench2 = setupTestWrench();

    EXPECT_TRUE(wrench == wrench2);
    EXPECT_FALSE(wrench != wrench2);

    wrench2.vector()(2) += 1.0;
    EXPECT_FALSE(wrench == wrench2);
}

TEST(Wrench, ReferencePoint)
{
    Wrench reference = setupTestWrench();
    Wrench myWrench = setupTestWrench();

    // Test shifting the reference point
    myWrench.shiftReferenceTo(Eigen::Vector3d(0.2, 0.0, 0.0));

    EXPECT_TRUE(isEqual((Eigen::Vector3d)myWrench.forceVector(), (Eigen::Vector3d)reference.forceVector()));

    EXPECT_DOUBLE_EQ(myWrench.forceVector()(0), 50.0);
    EXPECT_DOUBLE_EQ(myWrench.forceVector()(1), 100.0);
    EXPECT_DOUBLE_EQ(myWrench.forceVector()(2), 150.0);

    EXPECT_DOUBLE_EQ(myWrench.torqueVector()(0), 1.0);
    EXPECT_DOUBLE_EQ(myWrench.torqueVector()(1), 2.0 + 0.1 * myWrench.forceVector()(2));
    EXPECT_DOUBLE_EQ(myWrench.torqueVector()(2), 3.0 - 0.1 * myWrench.forceVector()(1));

    EXPECT_DOUBLE_EQ(myWrench.referencePoint()(0), 0.2);
    EXPECT_DOUBLE_EQ(myWrench.referencePoint()(1), 0.0);
    EXPECT_DOUBLE_EQ(myWrench.referencePoint()(2), 0.0);

    // Shift it back
    myWrench.shiftReferenceBy(Eigen::Vector3d(-0.1, 0.0, 0.0));
    EXPECT_TRUE(isEqual(myWrench.vector(), reference.vector()));

    EXPECT_DOUBLE_EQ(myWrench.forceVector()(0), 50.0);
    EXPECT_DOUBLE_EQ(myWrench.forceVector()(1), 100.0);
    EXPECT_DOUBLE_EQ(myWrench.forceVector()(2), 150.0);

    EXPECT_DOUBLE_EQ(myWrench.torqueVector()(0), 1.0);
    EXPECT_DOUBLE_EQ(myWrench.torqueVector()(1), 2.0);
    EXPECT_DOUBLE_EQ(myWrench.torqueVector()(2), 3.0);
}

TEST(Wrench, AddSubtract)
{
    Wrench myWrench1 = setupTestWrench();

    Wrench myWrench2 = Wrench::Zero();
    myWrench2.referencePoint()(0) = 0.1;
    myWrench2.forceVector()(0) = 25.0;
    myWrench2.forceVector()(1) = 50.0;
    myWrench2.forceVector()(2) = 100.0;
    myWrench2.torqueVector()(0) = 2.0;
    myWrench2.torqueVector()(1) = 4.0;
    myWrench2.torqueVector()(2) = 5.0;

    Wrench result = myWrench1 + myWrench2;

    EXPECT_DOUBLE_EQ(result.forceVector()(0), 75.0);
    EXPECT_DOUBLE_EQ(result.forceVector()(1), 150.0);
    EXPECT_DOUBLE_EQ(result.forceVector()(2), 250.0);

    EXPECT_DOUBLE_EQ(result.torqueVector()(0), 3.0);
    EXPECT_DOUBLE_EQ(result.torqueVector()(1), 6.0);
    EXPECT_DOUBLE_EQ(result.torqueVector()(2), 8.0);

    Wrench resultSubtract = result - myWrench1;
    EXPECT_TRUE(resultSubtract.vector().isApprox(myWrench2.vector()));
}

TEST(Wrench, Scaling)
{
    Wrench wrench = setupTestWrench();
    Wrench mult = wrench * 0.2;
    Wrench mult2 = 0.2 * wrench;

    EXPECT_DOUBLE_EQ(mult.forceVector()(0), 10.0);
    EXPECT_DOUBLE_EQ(mult.forceVector()(1), 20.0);
    EXPECT_DOUBLE_EQ(mult.forceVector()(2), 30.0);
    EXPECT_TRUE(mult == mult2);

    mult = mult / 0.2;
    EXPECT_TRUE(isEqual(wrench.vector(), mult.vector()));
}

TEST(Wrench, AssignmentOperators)
{
    Wrench wrench = setupTestWrench();
    Wrench delta = setupTestWrench();
    double scaler = 3.0;

    wrench += delta;

    EXPECT_TRUE(isEqual(wrench.vector(), (Eigen::Matrix<double, 6, 1>)(delta.vector() * 2.0)));

    wrench -= delta;
    EXPECT_TRUE(isEqual(wrench.vector(), delta.vector()));

    wrench *= scaler;
    EXPECT_DOUBLE_EQ(wrench.vector()(0), delta.vector()(0) * scaler);
    EXPECT_DOUBLE_EQ(wrench.vector()(1), delta.vector()(1) * scaler);
    EXPECT_DOUBLE_EQ(wrench.vector()(2), delta.vector()(2) * scaler);
    EXPECT_DOUBLE_EQ(wrench.vector()(3), delta.vector()(3) * scaler);
    EXPECT_DOUBLE_EQ(wrench.vector()(4), delta.vector()(4) * scaler);
    EXPECT_DOUBLE_EQ(wrench.vector()(5), delta.vector()(5) * scaler);

    wrench /= scaler;
    EXPECT_TRUE(isEqual(wrench.vector(), delta.vector()));
}

TEST(Wrench, Transformation)
{
    Wrench reference = setupTestWrench();
    Wrench testWrench = reference;
    Eigen::Matrix3d transformationMatrix;
    transformationMatrix << 0, 1, 0,
        -1, 0, 0,
        0, 0, 1;

    testWrench.transformWith(transformationMatrix);

    EXPECT_DOUBLE_EQ(testWrench.vector()(0), reference.vector()(1));
    EXPECT_DOUBLE_EQ(testWrench.vector()(1), -reference.vector()(0));
    EXPECT_DOUBLE_EQ(testWrench.vector()(2), reference.vector()(2));
    EXPECT_DOUBLE_EQ(testWrench.vector()(3), reference.vector()(4));
    EXPECT_DOUBLE_EQ(testWrench.vector()(4), -reference.vector()(3));
    EXPECT_DOUBLE_EQ(testWrench.vector()(5), reference.vector()(5));
}

TEST(Wrench, Shifted)
{
    Eigen::Vector3d position(0.1, 0.2, 0.3);
    Wrench reference = setupTestWrench();
    Wrench shiftedTo = reference.shiftedTo(position);
    Wrench shiftedBy = reference.shiftedBy(position);

    EXPECT_TRUE(reference.shiftReferenceBy(position).vector().isApprox(shiftedBy.vector()));
    EXPECT_TRUE(reference.shiftReferenceTo(position).vector().isApprox(shiftedTo.vector()));
}
}
