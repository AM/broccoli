/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/filters/IIRFilter.hpp"
#include "broccoli/control/Signal.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

// reference step response from matlab
const std::vector<double> referenceStepResponse{ 0, 0.23542489125666211, 0.4392413060584518, 0.60946167685615549, 0.74601819210048881,
    0.85034113522443866, 0.9249644946266129, 0.97317018758162277, 0.998678030781655,
    1.0053849970508242, 0.9971543434780018, 0.97765286962226694, 0.9502328292105594,
    0.91785382050932829, 0.88303925331727717, 0.84786166214825254, 0.81395113189214985,
    0.78252135229080566, 0.75440825389258415, 0.73011674032646234, 0.70987166715859673,
    0.69366988206569258, 0.68133079888038717, 0.67254360172441519, 0.6669097450257877,
    0.66397991758562136, 0.66328506675324184, 0.664361429807625, 0.66676979536367675,
    0.67010942253665162, 0.67402718634159575, 0.67822260237108756, 0.68244942088444394,
    0.68651447892268047, 0.69027446754301514, 0.69363121776958558, 0.696526040601391,
    0.69893357967592029, 0.70085555521575293, 0.70231469892299869, 0.70334910478840773,
    0.70400715269595693, 0.70434310176672232, 0.70441339943591452, 0.70427371055079624,
    0.70397663810751587, 0.70357008305210844 };

TEST(IIRFilter, DefaultConstructor)
{
    IIRFilter<double, 1, 5> filter;
    filter.init({ 0.1, 0.2 }, { 0.6, 0.5, 0.4, 0.3, 0.2, 0.1 }, 0.001);
}
TEST(IIRFilter, CheckIIRFilter)
{
    IIRFilter<double, 2, 2> filter;
    filter.init({ 0, 0.235424891256662, -0.204585685009958 }, { 1, -1.734744774147542, 0.778800783071405 }, 0.001);
    const auto& const_filter_reference = filter;

    double allowableError = 1.0e-09;
    for (double i : referenceStepResponse) {
        filter.process(mapSignal(1.0));
        EXPECT_NEAR(filter.value(), i, allowableError);
    }

    EXPECT_DOUBLE_EQ(const_filter_reference.derivative().sampleTime(), 0.001);
}
#ifndef NDEBUG
TEST(IIRFilter, ParameterValidation)
{
    IIRFilter<double, 1, 1> filter;
    EXPECT_DEATH(filter.init({ 0, 0.1 }, { 0.0, 0.0 }, 0.1), "") << "First denominator parameter may not be zero";
}
#endif

} // namespace
