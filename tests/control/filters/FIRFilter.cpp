/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/filters/FIRFilter.hpp"
#include "broccoli/control/Signal.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(FIRFilter, DefaultConstructor)
{
    FIRFilter<double, 5> filter;
    filter.init({ 0.5, 0.4, 0.3, 0.2, 0.1 });

    EXPECT_DOUBLE_EQ(filter.value(), 0.0);

    filter.reset(1.0);
    EXPECT_DOUBLE_EQ(filter.value(), 1.0);
}

TEST(FIRFilter, FilterStepVector)
{
    Eigen::Vector2d inputVector = Eigen::Vector2d::Zero();

    FIRFilter<Eigen::Vector2d, 5> filter(inputVector);
    filter.init({ 0.5, 0.4, 0.3, 0.2, 0.1 });

    EXPECT_DOUBLE_EQ(filter.value()(0), 0.0);
    EXPECT_DOUBLE_EQ(filter.value()(1), 0.0);

    inputVector(0) = 1.0;
    inputVector(1) = 3.0;

    filter.process(mapSignal(inputVector));

    // Expected result for digital filter after one step
    EXPECT_DOUBLE_EQ(filter.value()(0), 0.5);
    EXPECT_DOUBLE_EQ(filter.value()(1), 1.5);
}

TEST(FIRFilter, MatlabRefStepResponse)
{
    // The expected filter output was calculated with matlab
    constexpr std::size_t nrOfSamples = 13;
    const double expected[nrOfSamples]{ -0.001451684596776, 0.0064288139063799991, -0.01985643277699,
        0.049513167006180005, -0.12194680162902, 0.49994179130078004, 1.1218303842305801,
        0.95037041559538016, 1.01974001537855, 0.99345476869518012, 1.0013352671983362,
        0.99988358260156018, 0.99988358260156018 };

    FIRFilter<double, 12> filter;
    filter.init({ -0.001451684596776, 0.007880498503156, -0.02628524668337, 0.06936959978317,
        -0.1714599686352, 0.6218885929298, 0.6218885929298, -0.1714599686352,
        0.06936959978317, -0.02628524668337, 0.007880498503156, -0.001451684596776 });

    EXPECT_FLOAT_EQ(filter.value(), 0.0);

    for (std::size_t i = 0; i < nrOfSamples; i++) {
        filter.process(mapSignal(1.0));
        EXPECT_FLOAT_EQ(filter.value(), expected[i]);
    }
}
} // namespace
