/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/filters/FirstOrderLowPass.hpp"
#include "gtest/gtest.h"
#include <cmath>

using namespace broccoli::control;

namespace {

class FirstOrderLowPassMatlabReference : public ::testing::Test {
protected:
    // Expected step response calculated with matlab using tustin-transformation of
    // continuous first order filter with T = 0.1, dt = 0.01
    const std::vector<double> m_referenceStepResponse{ 0.047619047619047616, 0.1383219954648526, 0.22038656732534284,
        0.29463546567531018, 0.36181304037289969, 0.42259275081357589,
        0.47758391740275913, 0.52733783003106782, 0.57235327479001374,
        0.61308153433382206, 0.64993091201631525, 0.6832708251576185,
        0.71343550847594051, 0.74072736481156531, 0.76541999673427341,
        0.78776094942624741, 0.80797419233803347, 0.82626236449631607,
        0.84280880597285734, 0.85777939588020424, 0.87132421532018478,
        0.88357905195635755, 0.89466676129384726, 0.90469849831348093,
        0.913774831807435, 0.92198675258767926, 0.92941658567456686,
        0.93613881561032231, 0.942220833171244, 0.9477236109644589,
        0.9527023146821294, 0.95720685614097412, 0.96128239365135748,
        0.96496978473218054, 0.96830599571006815, 0.97132447230910923,
        0.97405547494633682, 0.97652638209430465, 0.97876196475198984,
        0.98078463477561, 0.9826146695588851, 0.98427041531518178,
        0.98576847099945009, 0.98712385471378816, 0.98835015426485606,
        0.98945966338248881, 0.99046350496510893, 0.99137174258747951,
        0.99219348138867192, 0.99293695935165549, 0.99360962988959312,
        0.99421823656677466, 0.99476888070327241, 0.99526708254105589,
        0.99571783658476476, 0.99612566167193006, 0.99649464627460338,
        0.996828489486546 };

    const double m_timeConstant = 0.1;
    const double m_dt = 0.01;

    void compareFilterOutputToReference(FirstOrderLowPass<double>& filter)
    {
        double allowableError = 1.0e-09;

        for (double i : m_referenceStepResponse) {
            filter.process(mapSignal(1.0));
            EXPECT_NEAR(filter.value(), i, allowableError);
        }
    }
};

TEST(FirstOrderLowPass, DefaultConstructor)
{
    FirstOrderLowPass<double> filter;
    filter.init(0.01, 10.0);
    EXPECT_DOUBLE_EQ(filter.value(), 0.0);
    EXPECT_DOUBLE_EQ(filter.derivative().value(), 0.0);
}

TEST(FirstOrderLowPass, Passthru)
{
    FirstOrderLowPass<double> filter;
    filter.init(0.001, 0.0);
    EXPECT_DOUBLE_EQ(filter.process(mapSignal(2.0)).value(), 2.0);
}

#ifdef NDEBUG
TEST(FirstOrderLowPass, ParameterValidation)
{
    FirstOrderLowPass<double> filter;
    filter.init(-0.01, 10.0);
    EXPECT_DOUBLE_EQ(filter.process(mapSignal(1.0)).value(), 1.0) << "Negative sample time should activate pass-thru";

    filter.init(0.001, -1.0);
    EXPECT_DOUBLE_EQ(filter.process(mapSignal(2.0)).value(), 2.0) << "Negative cutoff frequency shout activate pass-thru";

    FirstOrderLowPass<double> filterTimeConstantLimited;
    filterTimeConstantLimited.initTimeConstant(0.001, 0.0001);

    EXPECT_DOUBLE_EQ(filterTimeConstantLimited.process(mapSignal(1.0)).value(), 0.5) << "Invalid time constant should be clamped to stable region";
}
#else
TEST(FirstOrderLowPass, ParameterValidation)
{
    FirstOrderLowPass<double> filter;

    EXPECT_DEATH(filter.init(0.001, -0.01), "");
    EXPECT_DEATH(filter.initTimeConstant(0.001, 0.0001), "");
    EXPECT_DEATH(filter.init(-0.1, 1.0), "");
}
#endif

TEST_F(FirstOrderLowPassMatlabReference, StepResponse)
{
    FirstOrderLowPass<double> filter(0);
    filter.initTimeConstant(0.01, 0.1);

    compareFilterOutputToReference(filter);
}

TEST_F(FirstOrderLowPassMatlabReference, StepResponseHzConstructor)
{
    FirstOrderLowPass<double> filter(0, m_dt, 1.0 / (2.0 * M_PI * m_timeConstant));
    compareFilterOutputToReference(filter);
}

TEST_F(FirstOrderLowPassMatlabReference, StepResponseHzInit)
{
    FirstOrderLowPass<double> filter(0);
    filter.init(m_dt, 1.0 / (2.0 * M_PI * m_timeConstant));
    compareFilterOutputToReference(filter);
}

TEST(FirstOrderLowPass, FirstDerivative)
{

    FirstOrderLowPass<double> filter(0);
    filter.initTimeConstant(0.01, 0.1);

    // Expected first derivative of step response calculated with matlab using tustin-transformation of
    // continuous first order filter with T = 0.1, dt = 0.01
    constexpr std::size_t nrOfSamples = 57;
    const double expected[nrOfSamples]{ 9.0702947845804989, 8.2064571860490236, 7.4248898349967343,
        6.7177574697589506, 6.0779710440676205, 5.4991166589183234,
        4.9753912628308692, 4.5015444758945922, 4.0728259543808321,
        3.684937768249319, 3.3339913141303246, 3.0164683318322005,
        2.7291856335624809, 2.4692631922708097, 2.2340952691974003,
        2.0213242911786056, 1.82881721582826, 1.6546441476541274,
        1.4970589907346898, 1.3544819439980538, 1.2254836636172772,
        1.1087709337489704, 1.0031737019633669, 0.90763334939540918,
        0.82119207802442418, 0.7429833086887605, 0.67222299357554416,
        0.60820175609217486, 0.55027777932148414, 0.49787037176705073,
        0.4504541458844713, 0.40755375103833646, 0.36873910808230548,
        0.33362109778876103, 0.30184765990410867, 0.27310026372275864,
        0.24709071479678268, 0.22355826576851978, 0.202267002362011,
        0.18300347832751473, 0.16557457562966782, 0.14980556842683068,
        0.13553837143380765, 0.12262995510678998, 0.11095091176327454,
        0.10038415826201241, 0.090823762237057792, 0.08217388011924065,
        0.074347796298357416, 0.067267053793762521, 0.06086066771815446,
        0.055064413649774657, 0.049820183778348515, 0.045075404370886751,
        0.04078250871653033, 0.036898460267331679, 0.033384321194263045 };

    double allowableError = 1.0e-09;

    // two steps required until derivative is valid
    filter.process(mapSignal(1.0, 0.01));

    for (std::size_t i = 0; i < nrOfSamples; i++) {
        filter.process(mapSignal(1.0, 0.01));
        EXPECT_NEAR(filter.derivative().value(), expected[i], allowableError);
    }
}

TEST(FirstOrderLowPass, ResetState)
{
    FirstOrderLowPass<double> filter(0);
    filter.init(0.01, 1);

    filter.process(mapSignal(3.0));
    filter.process(mapSignal(3.0));

    filter.reset(2.0);
    EXPECT_DOUBLE_EQ(filter.value(), 2.0);
    EXPECT_DOUBLE_EQ(filter.derivative().value(), 0.0);

    filter.reset(1.0, 0.5);
    EXPECT_DOUBLE_EQ(filter.value(), 1.0);
    EXPECT_NEAR(filter.derivative().value(), 0.5, 0.001);
}
} // namespace
