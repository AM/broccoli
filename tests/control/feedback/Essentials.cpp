/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/feedback/essentials.hpp"
#include "broccoli/control/Signal.hpp"
#include "gtest/gtest.h"
#include <Eigen/Dense>

using namespace broccoli::control;

namespace {

TEST(Essentials, PControl)
{
    Signal<double> desiredState(0.1);
    Signal<double> actualState(0.2);

    auto controlError = desiredState - actualState;
    auto controlEffortOne = PControl(2.0, desiredState, actualState);

    EXPECT_DOUBLE_EQ(controlEffortOne.value(), -0.2);
    EXPECT_DOUBLE_EQ(PControl(2.0, controlError).value(), -0.2);
}

TEST(Essentials, PControlVector)
{
    Signal<Eigen::Vector3d> desiredState(Eigen::Vector3d(0.1, 0.2, 0.3));
    Signal<Eigen::Vector3d> actualState(Eigen::Vector3d(0.4, 0.5, 0.6));

    auto controlEffortOne = PControl(2.0, desiredState, actualState);

    EXPECT_DOUBLE_EQ(controlEffortOne.value()(0), -0.6);
    EXPECT_DOUBLE_EQ(controlEffortOne.value()(1), -0.6);
    EXPECT_DOUBLE_EQ(controlEffortOne.value()(2), -0.6);

    Eigen::Vector3d vectorGain(1.0, 2.0, 3.0);
    auto controlEffortElementWise = PControlElementWise(vectorGain, desiredState, actualState);

    EXPECT_DOUBLE_EQ(controlEffortElementWise.value()(0), -0.3);
    EXPECT_DOUBLE_EQ(controlEffortElementWise.value()(1), -0.6);
    EXPECT_DOUBLE_EQ(controlEffortElementWise.value()(2), -0.9);
}
} // namespace
