/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/sources/MultiSine.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(MultiSine, Constructor)
{
    MultiSine<> m_sines(0.001);
    m_sines.process();
    EXPECT_EQ(m_sines.value(), 0.0);
}

TEST(MultiSine, SingleSine)
{
    MultiSine<> m_sines(0.01);

    m_sines.singleSine(30);
    m_sines.setAmplitude(1.0);

    m_sines.start();
    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), 1.0);

    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), cos(2.0 * M_PI * 30.0 * 0.01));

    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), cos(2.0 * M_PI * 30.0 * 0.02));
}

TEST(MultiSine, SingleSineUnexpectedly)
{
    MultiSine<> m_sines(0.01);

    m_sines.uniformSineDistribution(30, 100, 1);
    m_sines.setAmplitude(1.0);

    m_sines.start();
    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), 1.0);

    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), cos(2.0 * M_PI * 30.0 * 0.01));

    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), cos(2.0 * M_PI * 30.0 * 0.02));
}

TEST(MultiSine, MultiSine)
{
    MultiSine<> m_sines(0.01);

    m_sines.uniformSineDistribution(1, 3, 4);
    m_sines.setAmplitude(1.0);

    EXPECT_EQ(m_sines.numberOfSines(), 4);

    std::vector<double> frequencies = m_sines.frequencyList();
    EXPECT_EQ(frequencies.at(0), 1.0);
    EXPECT_FLOAT_EQ(frequencies.at(1), 1.66666666);
    EXPECT_FLOAT_EQ(frequencies.at(2), 2.33333333);
    EXPECT_EQ(frequencies.at(3), 3.0);

    // Just 2 sines
    m_sines.uniformSineDistribution(1, 3, 2);

    m_sines.start();
    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), 0.0);

    m_sines.process();
    EXPECT_DOUBLE_EQ(m_sines.value(), cos(2.0 * M_PI * 1.0 * 0.01) + cos(2.0 * M_PI * 3.0 * 0.01 - M_PI));
}

} // namespace
