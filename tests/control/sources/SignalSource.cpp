/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/sources/SignalSource.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <Eigen/Dense>

using namespace broccoli::control;
using namespace ::testing;

namespace {

template <typename T>
class MockSignalSource : public SignalSource<MockSignalSource<T>> {
public:
    explicit MockSignalSource(const bool& autoRun)
        : SignalSource<MockSignalSource<T>>(autoRun)
    {
    }
    MOCK_METHOD(MockSignalSource<T>&, computeOutput, (const double& time));

    T value() const
    {
        return 0.0;
    }

    double sampleTime() const
    {
        return 0.001;
    }
};

TEST(SignalSource, Constructor)
{
    MockSignalSource<double> source(false);
    MockSignalSource<Eigen::Vector3d> sourceVector(false);

    EXPECT_EQ(source.value(), 0.0);
}

TEST(SignalSource, StartStop)
{
    MockSignalSource<double> source(false);

    EXPECT_CALL(source, computeOutput(0.001)).Times(1).WillRepeatedly(ReturnRef(source));
    EXPECT_CALL(source, computeOutput(0.0)).Times(1).WillRepeatedly(ReturnRef(source));

    source.start();

    EXPECT_TRUE(source.isActive());

    source.process();

    source.stop();
    source.process();

    EXPECT_FALSE(source.isActive());
}

TEST(SignalSource, AutoStart)
{
    MockSignalSource<double> source(true);

    EXPECT_CALL(source, computeOutput(0.0)).Times(1).WillRepeatedly(ReturnRef(source));
    EXPECT_TRUE(source.isActive());

    source.process();
}

TEST(SignalSource, StartOnChange)
{
    MockSignalSource<double> source(false);
    EXPECT_CALL(source, computeOutput(0.0)).Times(2).WillRepeatedly(ReturnRef(source));
    EXPECT_CALL(source, computeOutput(0.001)).Times(1).WillRepeatedly(ReturnRef(source));

    source.startOnChange(false);
    source.process();
    EXPECT_FALSE(source.isActive());

    source.startOnChange(true);
    source.process();
    EXPECT_TRUE(source.isActive());

    source.startOnChange(true);
    source.process();
    EXPECT_TRUE(source.isActive());
}
} // namespace
