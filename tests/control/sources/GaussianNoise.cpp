/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/sources/GaussianNoise.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(GaussianNoise, Sample)
{
    GaussianNoise<> m_noise(0.01);

    double sampleOne = m_noise.process().value();
    EXPECT_NE(sampleOne, m_noise.process().value());
}

TEST(GaussianNoise, Deactivated)
{
    GaussianNoise<> m_noise(0.01);
    m_noise.stop();

    double sampleOne = m_noise.process().value();
    EXPECT_DOUBLE_EQ(sampleOne, m_noise.process().value());
}

} // namespace
