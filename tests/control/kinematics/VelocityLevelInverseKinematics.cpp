/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "mockups.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;
using namespace broccoli::tests;

namespace {

TEST(VelocityLevelInverseKinematics, DriftCompensation)
{
    Eigen::Vector2d actualPosition = Eigen::Vector2d::Zero();
    Eigen::Vector2d desiredPosition(2.0, 3.0);
    Eigen::Vector2d desiredVelocity = Eigen::Vector2d::Zero();
    Eigen::Vector2d driftPerStep(5e-04, 5e-04);

    MockVelocityLevelInverseKinematics<2, 3> IK;
    IK.setTarget(desiredPosition, desiredVelocity);

    auto integrateWithSimulatedDrift = [&actualPosition, &IK, driftPerStep]() {
        Eigen::Vector2d effectiveVelocity;

        for (int i = 0; i < 1000; i++) {
            effectiveVelocity = IK.effectiveDesiredVelocity(actualPosition, 0.001);
            actualPosition += effectiveVelocity * 0.001 + driftPerStep;
        }
    };

    integrateWithSimulatedDrift();
    EXPECT_NEAR(actualPosition(0), desiredPosition(0), 1e-03);
    EXPECT_NEAR(actualPosition(1), desiredPosition(1), 1e-03);
}

TEST(VelocityLevelInverseKinematics, FeedForwardOfDesiredVelocity)
{
    Eigen::Vector2d actualPosition = Eigen::Vector2d::Zero();
    Eigen::Vector2d desiredPosition(2.0, 3.0);
    Eigen::Vector2d desiredVelocity(5.0, 6.0);

    MockVelocityLevelInverseKinematics<2, 3> IK;
    IK.setDriftCompensationGain(0.0);
    IK.setTarget(desiredPosition, desiredVelocity);

    Eigen::Vector2d effectiveVelocity = IK.effectiveDesiredVelocity(actualPosition, 0.001);

    EXPECT_DOUBLE_EQ(effectiveVelocity(0), desiredVelocity(0));
    EXPECT_DOUBLE_EQ(effectiveVelocity(1), desiredVelocity(1));
}

} // namespace
