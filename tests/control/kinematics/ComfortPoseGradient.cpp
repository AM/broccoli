/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/control/kinematics/ComfortPoseGradient.hpp"
#include "mockups.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;
using namespace broccoli::tests;
using namespace ::testing;

namespace {

TEST(ComfortPoseGradient, ComfortPoseEqualScaling)
{
    ComfortPoseGradient<3> comfortPose;

    comfortPose.setPose(Eigen::Vector3d(1.0, -1.0, 2.0));
    comfortPose.setWeight(10.0);
    comfortPose.useEqualScaling();
    EXPECT_TRUE(comfortPose.jointScalingVector().isApprox(Eigen::Vector3d::Ones()));

    comfortPose.process(Eigen::Vector3d(0.0, 1.0, 0.0), Eigen::Vector3d::Zero());
    Eigen::Vector3d result = comfortPose.gradient();

    EXPECT_DOUBLE_EQ(result(0), -10.0);
    EXPECT_DOUBLE_EQ(result(1), 20.0);
    EXPECT_DOUBLE_EQ(result(2), -20.0);
}

TEST(ComfortPoseGradient, RangeBasedScaling)
{
    ComfortPoseGradient<3> comfortPose;
    Eigen::Vector3d min(-2.0, -1.0, 0.0);
    Eigen::Vector3d max(2.0, 1.0, 2.0);
    comfortPose.useRangeBasedScaling(min, max);

    EXPECT_EQ(comfortPose.jointScalingVector()(0), 1.0);
    EXPECT_EQ(comfortPose.jointScalingVector()(1), 2.0);
    EXPECT_EQ(comfortPose.jointScalingVector()(2), 2.0);
}

TEST(ComfortPoseGradient, CustomScaling)
{
    ComfortPoseGradient<3> comfortPose;

    Eigen::Vector3d customScaling(1.0, 0.2, 0.5);
    comfortPose.useCustomScaling(customScaling);
    EXPECT_TRUE(comfortPose.jointScalingVector().isApprox(customScaling));

#ifdef NDEBUG
    Eigen::Vector3d invalidCustomScaling(-1.0, 0.2, 0.5);
    comfortPose.useCustomScaling(customScaling);
    EXPECT_TRUE(comfortPose.jointScalingVector().isApprox(customScaling));
#endif
}

} // namespace
