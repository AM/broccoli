/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/control/kinematics/AutomaticSupervisoryControl.hpp"
#include "mockups.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;
using namespace broccoli::tests;
using namespace ::testing;

namespace {

Eigen::Matrix<double, 2, 3> systemJacobian()
{
    Eigen::Matrix<double, 2, 3> jacobian;
    jacobian << 0.4, -0.3, 0.0,
        0.6, 0.1, 0.3;
    return jacobian;
}

TEST(AutomaticSupervisoryControl, ResolvedMotionRateControl)
{
    MockAutomaticSupervisoryControl<2, 3> IK;
    Eigen::Matrix<double, 2, 3> jacobian = systemJacobian();
    Eigen::Vector2d desiredTaskSpaceVelocity(2.0, 4.0);
    EXPECT_CALL(IK, effectiveDesiredVelocity).WillRepeatedly(Return(desiredTaskSpaceVelocity));

    IK.process(jacobian, Eigen::Vector2d::Zero(), Eigen::Vector3d::Zero(), 0.001);
    Eigen::Vector2d directKinematicsVelocity = jacobian * IK.outputVelocity().value();

    EXPECT_TRUE(directKinematicsVelocity.isApprox(desiredTaskSpaceVelocity));
}

TEST(AutomaticSupervisoryControl, NullSpaceGradient)
{
    MockAutomaticSupervisoryControl<2, 3> IK;
    Eigen::Matrix<double, 2, 3> jacobian = systemJacobian();
    Eigen::Vector3d nullSpaceGradient(0.4, -0.5, 0.4);
    IK.setWeighingMatrix(Eigen::Vector3d(1.0, 1.0, 10.0));
    Eigen::Vector2d desiredTaskSpaceVelocity(2.0, 4.0);
    EXPECT_CALL(IK, effectiveDesiredVelocity).WillRepeatedly(Return(desiredTaskSpaceVelocity));

    IK.process(jacobian, Eigen::Vector2d::Zero(), nullSpaceGradient, 0.001);
    Eigen::Vector2d directKinematicsVelocity = jacobian * IK.outputVelocity().value();

    EXPECT_TRUE(directKinematicsVelocity.isApprox(desiredTaskSpaceVelocity));
}

TEST(AutomaticSupervisoryControl, NullSpacePassthrough)
{
    MockAutomaticSupervisoryControl<2, 3> IK;
    Eigen::Matrix<double, 2, 3> jacobian = systemJacobian();
    Eigen::Vector3d nullSpaceGradient(0.4, -0.5, 0.4);
    IK.setTaskSpaceConstraintFactor(0.0);
    Eigen::Vector2d desiredTaskSpaceVelocity(2.0, 4.0);
    EXPECT_CALL(IK, effectiveDesiredVelocity).WillRepeatedly(Return(desiredTaskSpaceVelocity));

    IK.process(jacobian, Eigen::Vector2d::Zero(), nullSpaceGradient, 0.001);

    EXPECT_TRUE(IK.outputVelocity().value().isApprox(-nullSpaceGradient));
}

TEST(AutomaticSupervisoryControl, ChainedIKsPassthrough)
{
    MockVelocityLevelInverseKinematics<2, 3> slaveIK;
    MockAutomaticSupervisoryControl<2, 3> IK;
    Signal<Eigen::Vector3d> slaveVelocity(Eigen::Vector3d(0.4, -0.5, 0.4), 0.001);
    Eigen::Vector2d desiredTaskSpaceVelocity(2.0, 4.0);

    EXPECT_CALL(slaveIK, outputVelocity()).WillRepeatedly(ReturnRef(slaveVelocity));
    EXPECT_CALL(IK, effectiveDesiredVelocity).WillRepeatedly(Return(desiredTaskSpaceVelocity));

    Eigen::Matrix<double, 2, 3> jacobian = systemJacobian();
    IK.setTaskSpaceConstraintFactor(0.0);

    IK.process(jacobian, Eigen::Vector2d::Zero(), slaveIK);

    EXPECT_TRUE(IK.outputVelocity().value().isApprox(slaveVelocity.value()));
}

} // namespace
