/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/control/kinematics/NullSpaceGradient.hpp"
#include "mockups.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;
using namespace broccoli::tests;
using namespace ::testing;

namespace {

TEST(NullSpaceGradient, Interface)
{
    MockNullSpaceGradient<2> gradient;
    EXPECT_CALL(gradient, calculateGradient).WillRepeatedly(Return(Eigen::Vector2d(1.0, 2.0)));
    gradient.process(Eigen::Vector2d::Zero(), Eigen::Vector2d::Zero());

    gradient.setWeight(2.0);
    EXPECT_DOUBLE_EQ(gradient.weight(), 2.0);

    EXPECT_DOUBLE_EQ(gradient.gradient()(0), 2.0);
    EXPECT_DOUBLE_EQ(gradient.gradient()(1), 4.0);
}
} // namespace
