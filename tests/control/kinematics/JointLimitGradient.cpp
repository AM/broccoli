/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/control/kinematics/JointLimitGradient.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;
using namespace ::testing;

namespace {

TEST(JointLimitGradient, CheckGradient)
{
    Eigen::Vector2d safetyMargins(0.1, 0.1);
    Eigen::Vector2d minimumAngles(-0.5, -0.7);
    Eigen::Vector2d maximumAngles(0.2, 0.5);

    JointLimitGradient<2> jointLimitGradient(safetyMargins, minimumAngles, maximumAngles);

    jointLimitGradient.process(Eigen::Vector2d(0.3, -0.6), Eigen::Vector2d::Zero());
    EXPECT_DOUBLE_EQ(jointLimitGradient.gradient()(0), 2.0);
    EXPECT_DOUBLE_EQ(jointLimitGradient.gradient()(1), 0.0);

    jointLimitGradient.process(Eigen::Vector2d(0.3, -0.7), Eigen::Vector2d::Zero());
    EXPECT_DOUBLE_EQ(jointLimitGradient.gradient()(0), 2.0);
    EXPECT_DOUBLE_EQ(jointLimitGradient.gradient()(1), -1.0);
}

#ifdef NDEBUG
TEST(JointLimitGradient, InvalidInput)
{
    Eigen::Vector2d safetyMargins(0.0, -0.1);
    Eigen::Vector2d minimumAngles(0.5, -0.5);
    Eigen::Vector2d maximumAngles(-0.2, 0.5);

    JointLimitGradient<2> jointLimitGradient(safetyMargins, minimumAngles, maximumAngles);

    jointLimitGradient.process(Eigen::Vector2d(0.3, -0.6), Eigen::Vector2d::Zero());
    EXPECT_DOUBLE_EQ(jointLimitGradient.gradient()(0), -201.0);
    EXPECT_DOUBLE_EQ(jointLimitGradient.gradient()(1), -2.0);
}
#endif

} // namespace
