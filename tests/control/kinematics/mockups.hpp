/* 
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#pragma once

#include "broccoli/control/kinematics/AutomaticSupervisoryControl.hpp"
#include "broccoli/control/kinematics/NullSpaceGradient.hpp"
#include "broccoli/control/kinematics/VelocityLevelInverseKinematics.hpp"
#include "gmock/gmock.h"

namespace broccoli {
namespace tests {

    //! A mock class of VelocityLevelInverseKinematics
    template <std::size_t TaskSpaceSize, std::size_t ConfigurationSpaceSize>
    class MockVelocityLevelInverseKinematics : public control::VelocityLevelInverseKinematics<TaskSpaceSize, ConfigurationSpaceSize> {
    public:
        using control::VelocityLevelInverseKinematics<TaskSpaceSize, ConfigurationSpaceSize>::effectiveDesiredVelocity;
        MOCK_METHOD((const control::Signal<typename control::VelocityLevelInverseKinematics<TaskSpaceSize, ConfigurationSpaceSize>::ConfigurationSpaceType>&), outputVelocity, (), (const override));
    };

    //! A mock class for AutomaticSupervisoryControl
    template <std::size_t TaskSpaceSize, std::size_t ConfigurationSpaceSize>
    class MockAutomaticSupervisoryControl : public control::AutomaticSupervisoryControl<TaskSpaceSize, ConfigurationSpaceSize> {
    public:
        using TaskSpaceType = typename control::AutomaticSupervisoryControl<TaskSpaceSize, ConfigurationSpaceSize>::TaskSpaceType;
        Eigen::Vector2d m_effectiveVelocity{ 2.0, 4.0 };
        MOCK_METHOD(TaskSpaceType, effectiveDesiredVelocity, (const TaskSpaceType&, const double&), (const override));
    };

    //! A mock class for NullSpaceGradient
    template <std::size_t NrOfDofs>
    class MockNullSpaceGradient : public control::NullSpaceGradient<NrOfDofs> {
    public:
        using JointSpaceType = typename control::NullSpaceGradient<NrOfDofs>::JointSpaceType;
        MOCK_METHOD(JointSpaceType, calculateGradient, (const JointSpaceType& actualJointPosition, const JointSpaceType& actualJointVelocity));
    };

} // namespace tests
} // namespace broccoli
