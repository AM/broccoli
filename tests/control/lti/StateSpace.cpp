/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/lti/Continuous2DiscreteZOH.hpp"
#include "broccoli/control/lti/StateSpaceModel.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

static ContinuousStateSpaceParameters<> referenceSystem()
{
    ContinuousStateSpaceParameters<2, 1, 1> continuousSystem;
    continuousSystem.A << -1.5, -2, 1, 0.0;
    continuousSystem.B << 0.5, 0;
    continuousSystem.C << 0.0, 1.0;
    continuousSystem.D << 5.0;

    return continuousSystem;
}

TEST(StateSpace, StateSpaceParametersConstructor)
{
    Eigen::Matrix2d A;
    A << 0.1, 0.2, 0.3, 0.4;

    Eigen::Matrix<double, 2, 3> B;
    B << 0.1, 0.2, 0.3, 0.4, 0.5, 0.6;

    Eigen::Matrix<double, 1, 2> C;
    C << 0.1, 0.2;

    Eigen::Matrix<double, 1, 3> D;
    D << 0.4, 0.5, 0.7;

    ContinuousStateSpaceParameters<2, 3, 1> p(A, B, C, D);

    EXPECT_TRUE(p.A.isApprox(A));
    EXPECT_TRUE(p.B.isApprox(B));
    EXPECT_TRUE(p.C.isApprox(C));
    EXPECT_TRUE(p.D.isApprox(D));
}
TEST(StateSpace, Continuous2DiscreteZOH)
{
    auto continuousSystem = referenceSystem();

    // Reference from Matlab using c2d()
    DiscreteStateSpaceParameters<2, 1, 1> expectedParams;
    expectedParams.A << 0.998500125437315, -0.001998500083552, 9.992500417760047e-04, 0.999999000499979;
    expectedParams.B << 4.996250208880023e-04, 2.498750052192677e-07;
    expectedParams.C = continuousSystem.C;
    expectedParams.D = continuousSystem.D;

    auto discreteSystem = continuous2DiscreteZOH(continuousSystem, 0.001);

    EXPECT_TRUE(discreteSystem.A.isApprox(expectedParams.A));
    EXPECT_TRUE(discreteSystem.B.isApprox(expectedParams.B));
    EXPECT_TRUE(discreteSystem.C.isApprox(expectedParams.C));
    EXPECT_TRUE(discreteSystem.D.isApprox(expectedParams.D));
}
TEST(StateSpace, StateSpaceModelResult)
{
    StateSpaceModel<> mdl(Eigen::Vector2d::Zero(), Eigen::Matrix<double, 1, 1>(1.0), continuous2DiscreteZOH(referenceSystem(), 0.001));
    EXPECT_TRUE(mdl.state().value().isApprox(Eigen::Vector2d::Zero()));
    EXPECT_DOUBLE_EQ(mdl.value()(0), 5.0);

    for (int i = 0; i < 100; i++) {
        mdl.process(mapSignal(Eigen::Matrix<double, 1, 1>(1.0)));
    }
    EXPECT_NEAR(mdl.value()(0), 5.002329564062457, 1e-14);
    EXPECT_DOUBLE_EQ(mdl.sampleTime(), 0.001);

    // This is x[n+1]
    EXPECT_NEAR(mdl.state().value()(0), 0.046276118229583, 1e-14);
    EXPECT_NEAR(mdl.state().value()(1), 0.002375627141508, 1e-14);
}

TEST(StateSpace, SetState)
{
    StateSpaceModel<> mdl(Eigen::Vector2d::Zero(), Eigen::Matrix<double, 1, 1>(1.0), continuous2DiscreteZOH(referenceSystem(), 0.001));

    mdl.setState(Eigen::Vector2d(1.0, 2.0));
    EXPECT_TRUE(mdl.state().value().isApprox(Eigen::Vector2d(1.0, 2.0)));
}

} // namespace
