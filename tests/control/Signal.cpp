/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/Signal.hpp"
#include "broccoli/control/SignalLimits.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(Signal, ConstructionAndBaseInterface)
{
    Signal<double> doubleSignal(0.2, 0.001);

    EXPECT_DOUBLE_EQ(mapSignal(doubleSignal).value(), 0.2);
    EXPECT_DOUBLE_EQ(doubleSignal.sampleTime(), 0.001);

    std::stringstream output;
    output << doubleSignal;

    EXPECT_EQ(output.str(), std::string("<broccoli signal dt=0.001>\n0.2\n</broccoli signal>"));
}

TEST(Signal, SignalLimits)
{
    SignalLimits<double> limit(2.0, 3.0);
    EXPECT_DOUBLE_EQ(limit.min(), 2.0);
    EXPECT_DOUBLE_EQ(limit.max(), 3.0);

    auto anotherLimit = makeSignalLimits(Eigen::Vector3d::Zero(), Eigen::Vector3d::Constant(1.0));
    EXPECT_TRUE(anotherLimit.min().isApprox(Eigen::Vector3d::Zero()));
    EXPECT_TRUE(anotherLimit.max().isApprox(Eigen::Vector3d::Ones()));
}

TEST(Signal, Evaluation)
{
    Signal<double> valueA(0.1);

    auto expression = 0.5 * valueA;

    auto evaluated = expression.evaluate();
    valueA.value() = 0.2;

    EXPECT_DOUBLE_EQ(expression.value(), 0.1);
    EXPECT_DOUBLE_EQ(evaluated.value(), 0.05);
}

TEST(Signal, Mapping)
{
    double lvalue = 0.1;
    Signal<double> aSignal(0.1);

    static_assert(std::is_same<internal::SignalNoSampleTime<double&>, decltype(mapSignal(lvalue))>::value, "Mapping of lvalue must use references");
    static_assert(std::is_same<Signal<double>&, decltype(mapSignal(aSignal))>::value, "Mapping of signal must return reference to signal");
    static_assert(std::is_same<internal::SignalNoSampleTime<double>, decltype(mapSignal(0.1))>::value, "Mapping of rvalue must store element");

    auto mappedRValue = mapSignal(0.1);
    EXPECT_DOUBLE_EQ(mappedRValue.value(), 0.1);

    auto mappedLValue = mapSignal(lvalue);
    EXPECT_DOUBLE_EQ(mappedLValue.value(), 0.1);
}

TEST(Signal, EigenMapping)
{
    Eigen::Vector3d vector(0.1, 0.2, 0.3);

    static_assert(std::is_same<internal::SignalNoSampleTime<const Eigen::Vector3d&>, decltype(mapSignal(vector))>::value, "Mapping of lvalue storage must use const reference");
    static_assert(std::is_same<internal::SignalNoSampleTime<Eigen::Vector3d>, decltype(mapSignal(Eigen::Vector3d(0.1, 0.2, 0.4)))>::value, "Mapping of rvalue storage must use move");
    static_assert(std::is_same<internal::SignalNoSampleTime<Eigen::Vector3d>, decltype(mapSignal(vector * 0.1))>::value, "Mapping of lvalue expression must use move");
    static_assert(std::is_same<internal::SignalNoSampleTime<Eigen::Vector3d>, decltype(mapSignal(Eigen::Vector3d(0.1, 0.2, 0.4) * 0.2))>::value, "Mapping of rvalue expression must use move");

    auto rvalueStorage = mapSignal(Eigen::Vector3d(0.1, 0.2, 0.3));
    EXPECT_DOUBLE_EQ(rvalueStorage.value()(0), 0.1);
    EXPECT_DOUBLE_EQ(rvalueStorage.value()(1), 0.2);
    EXPECT_DOUBLE_EQ(rvalueStorage.value()(2), 0.3);

    auto lvalueStorage = mapSignal(vector);
    EXPECT_DOUBLE_EQ(lvalueStorage.value()(0), 0.1);
    EXPECT_DOUBLE_EQ(lvalueStorage.value()(1), 0.2);
    EXPECT_DOUBLE_EQ(lvalueStorage.value()(2), 0.3);

    auto rvalueExpression = mapSignal(Eigen::Vector3d(0.1, 0.2, 0.3) * 2.0);
    EXPECT_DOUBLE_EQ(rvalueExpression.value()(0), 0.2);
    EXPECT_DOUBLE_EQ(rvalueExpression.value()(1), 0.4);
    EXPECT_DOUBLE_EQ(rvalueExpression.value()(2), 0.6);

    auto lvalueExpression = mapSignal(vector * 2.0);
    EXPECT_DOUBLE_EQ(lvalueExpression.value()(0), 0.2);
    EXPECT_DOUBLE_EQ(lvalueExpression.value()(1), 0.4);
    EXPECT_DOUBLE_EQ(lvalueExpression.value()(2), 0.6);
}
}
