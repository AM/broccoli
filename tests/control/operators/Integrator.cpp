/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/operators/Integrator.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(Integrator, Construction)
{
    Integrator<double> constructDefault;
    EXPECT_DOUBLE_EQ(constructDefault.value(), 0.0);

    Integrator<Eigen::Vector2d> constructEigenPrototype(Eigen::Vector2d::Ones());
    EXPECT_DOUBLE_EQ(constructEigenPrototype.value()(0), 1.0);
    EXPECT_DOUBLE_EQ(constructEigenPrototype.value()(1), 1.0);
}

TEST(Integrator, ProcessScalar)
{

    Integrator<double> integrator;

    integrator.process(mapSignal(2.0, 0.1));
    EXPECT_DOUBLE_EQ(integrator.value(), 0.2);

    integrator.process(mapSignal(3.0, 0.1));
    EXPECT_DOUBLE_EQ(integrator.value(), 0.5);

    integrator.value() = 5.0;
    EXPECT_DOUBLE_EQ(integrator.value(), 5.0);

    integrator.process(mapSignal(5.0, 0.1));
    EXPECT_DOUBLE_EQ(integrator.value(), 5.5);

    integrator.process(mapSignal(5.0, 0.1));
    EXPECT_DOUBLE_EQ(integrator.value(), 6.0);
}

TEST(Integrator, ProcessEigen)
{
    Eigen::Vector2d integrand(2.0, 3.0);
    Integrator<Eigen::Vector2d> integrator(Eigen::Vector2d::Zero());

    integrator.process(mapSignal(integrand, 0.1));
    EXPECT_DOUBLE_EQ(integrator.value()(0), 0.2);
    EXPECT_DOUBLE_EQ(integrator.value()(1), 0.3);

    integrator.process(mapSignal(integrand, 0.1));
    EXPECT_DOUBLE_EQ(integrator.value()(0), 0.4);
    EXPECT_DOUBLE_EQ(integrator.value()(1), 0.6);
}

TEST(Integrator, PerfectForwarding)
{
    double myStorage = 0.0;
    Integrator<double&> integrator(myStorage);
    integrator.process(mapSignal(0.2, 0.001));

    EXPECT_DOUBLE_EQ(myStorage, 0.0002);
}

} // namespace
