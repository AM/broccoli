/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/operators/SaturatedIntegrator.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(SaturatedIntegrator, Construction)
{
    SignalLimits<double> limitDouble;
    SaturatedIntegrator<double> constructDefault(limitDouble);
    EXPECT_DOUBLE_EQ(constructDefault.value(), 0.0);

    SignalLimits<Eigen::Vector2d> limitVector;
    SaturatedIntegrator<Eigen::Vector2d> constructEigenPrototype(Eigen::Vector2d::Ones(), limitVector);
    EXPECT_DOUBLE_EQ(constructEigenPrototype.value()(0), 1.0);
    EXPECT_DOUBLE_EQ(constructEigenPrototype.value()(1), 1.0);
}

TEST(SaturatedIntegrator, ProcessScalarMax)
{
    SignalLimits<double> limitDouble;
    limitDouble.min() = 0.0;
    limitDouble.max() = 0.9;

    SaturatedIntegrator<double> integrator(limitDouble);

    for (std::size_t i = 0; i < 9; i++) {
        integrator.process(mapSignal(1.0, 0.1));

        ASSERT_DOUBLE_EQ(integrator.value(), (i + 1) * 0.1);
    }
    integrator.process(mapSignal(1.0, 0.1));
    integrator.process(mapSignal(1.0, 0.1));

    ASSERT_DOUBLE_EQ(integrator.value(), 0.9);
}

TEST(SaturatedIntegrator, ProcessScalarMin)
{
    SignalLimits<double> limitDouble;
    limitDouble.min() = -0.9;
    limitDouble.max() = 0.0;

    SaturatedIntegrator<double> integrator(limitDouble);

    for (std::size_t i = 0; i < 9; i++) {
        integrator.process(mapSignal(-1.0, 0.1));

        ASSERT_DOUBLE_EQ(integrator.value(), -0.1 * (i + 1));
    }
    integrator.process(mapSignal(-1.0, 0.1));
    integrator.process(mapSignal(-1.0, 0.1));

    ASSERT_DOUBLE_EQ(integrator.value(), -0.9);
}

TEST(SaturatedIntegrator, ProcessEigen)
{
    SignalLimits<Eigen::Vector3d> limitVector;
    limitVector.min().setZero();
    limitVector.max().setConstant(0.9);

    SaturatedIntegrator<Eigen::Vector3d> vectorIntegrator(Eigen::Vector3d::Zero(), limitVector);
    Eigen::Vector3d vector = Eigen::Vector3d::Constant(1.0);

    for (std::size_t i = 0; i < 9; i++) {
        vectorIntegrator.process(mapSignal(vector, 0.1));

        for (std::size_t j = 0; j < 3; j++) {
            ASSERT_DOUBLE_EQ(vectorIntegrator.value()(j), (i + 1) * 0.1);
        }
    }
    vectorIntegrator.process(mapSignal(vector, 0.1));
    vectorIntegrator.process(mapSignal(vector, 0.1));

    for (std::size_t i = 0; i < 3; i++) {
        ASSERT_DOUBLE_EQ(vectorIntegrator.value()(i), 0.9);
    }
}

} // namespace
