/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/operators/BlendingScheme.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(BlendingScheme, ProcessScalar)
{
    EXPECT_DOUBLE_EQ(BlendingScheme(Signal<double>(0.2), Signal<double>(1.0), Signal<double>(3.0, 0.001)).value(), 2.6);
    EXPECT_DOUBLE_EQ(BlendingScheme(Signal<double>(0.2), Signal<double>(1.0), Signal<double>(-2.0, 0.001)).value(), -1.4);
}

TEST(BlendingScheme, ProcessVector)
{
    Eigen::Vector2d vectorA(0.1, 0.2);
    Eigen::Vector2d vectorB(-0.3, -0.4);
    Eigen::Vector2d offset(0.2, 0.3);
    auto blender = BlendingScheme(Signal<double>(0.2), Signal<Eigen::Vector2d>(vectorA), Signal<Eigen::Vector2d>(vectorB)) + Signal<Eigen::Vector2d>(offset);

    EXPECT_NEAR(blender.value()(0), -0.02, 1e-06);
    EXPECT_NEAR(blender.value()(1), 0.02, 1e-06);
}

TEST(BlendingSCheme, SampleTimePropagation)
{
    EXPECT_LT(BlendingScheme(mapSignal(0.2), mapSignal(0.3), mapSignal(0.5)).sampleTime(), 0.0) << "Propagated sample time should be inherited";
    EXPECT_DOUBLE_EQ(BlendingScheme(mapSignal(0.2, 0.1), mapSignal(0.3), mapSignal(0.5)).sampleTime(), 0.1) << "Propagated sample time should be from first signal";
    EXPECT_DOUBLE_EQ(BlendingScheme(mapSignal(0.2), mapSignal(0.3, 0.2), mapSignal(0.5)).sampleTime(), 0.2) << "Propagated sample time should be from second signal";
    EXPECT_DOUBLE_EQ(BlendingScheme(mapSignal(0.2), mapSignal(0.3), mapSignal(0.5, 0.3)).sampleTime(), 0.3) << "Propagated sample time should be from third signal";
}

#ifndef NDEBUG
TEST(BlendingSCheme, AssertValidation)
{
    EXPECT_DEATH(BlendingScheme(mapSignal(0.2, 0.1), mapSignal(0.3, 0.2), mapSignal(0.4)), "");
}
#endif

} // namespace
