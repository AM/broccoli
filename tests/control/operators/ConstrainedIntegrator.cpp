/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/operators/ConstrainedIntegrator.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(ConstrainedIntegrator, Construction)
{
    ConstrainedIntegrator<double> constructDefault;
    EXPECT_DOUBLE_EQ(constructDefault.value(), 0.0);

    ConstrainedIntegrator<Eigen::Vector2d> constructEigenPrototype(Eigen::Vector2d::Ones());
    EXPECT_DOUBLE_EQ(constructEigenPrototype.value()(0), 1.0);
    EXPECT_DOUBLE_EQ(constructEigenPrototype.value()(1), 1.0);
}

TEST(ConstrainedIntegrator, ProcessScalar)
{
    ConstrainedIntegrator<double> integrator;

    integrator.process(mapSignal(2.0, 0.1), mapSignal(1.0));
    EXPECT_DOUBLE_EQ(integrator.value(), 0.2);

    integrator.process(mapSignal(3.0, 0.1), mapSignal(0.0));
    EXPECT_DOUBLE_EQ(integrator.value(), 0.2);
}

TEST(ConstrainedIntegrator, ProcessEigen)
{
    Eigen::Vector2d integrand(2.0, 3.0);
    Eigen::Vector2d isUnconstrained(1.0, 0.0);
    ConstrainedIntegrator<Eigen::Vector2d> integrator(Eigen::Vector2d::Zero());

    integrator.process(mapSignal(integrand, 0.1), mapSignal(isUnconstrained));
    EXPECT_DOUBLE_EQ(integrator.value()(0), 0.2);
    EXPECT_DOUBLE_EQ(integrator.value()(1), 0.0);

    integrator.process(mapSignal(integrand, 0.1), mapSignal(isUnconstrained));
    EXPECT_DOUBLE_EQ(integrator.value()(0), 0.4);
    EXPECT_DOUBLE_EQ(integrator.value()(1), 0.0);
}

} // namespace
