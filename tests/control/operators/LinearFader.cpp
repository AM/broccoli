/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/operators/LinearFader.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {
TEST(LinearFader, EigenType)
{
    LinearFader fader;
    fader.init(0.1, 0.1);
    EXPECT_DOUBLE_EQ(fader.fadingValue(), 0.1);
    EXPECT_TRUE(fader.isIdling());
    auto result = fader.processSignal(mapSignal(Eigen::Vector2d::Ones(), 0.1));

    EXPECT_DOUBLE_EQ(result.value()(0), 0.1);
    EXPECT_DOUBLE_EQ(result.value()(1), 0.1);
}

TEST(LinearFader, FadeIn)
{
    LinearFader fader;
    fader.init(0.5, 0.3);
    fader.switchOff();
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), 0.0);

    fader.fadeIn();
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -1.0);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -2.0);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -3.0);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-4.0, 0.1)).value(), -4.0);
}

TEST(LinearFader, FadeOut)
{
    LinearFader fader;
    fader.init(0.5, 0.3);
    fader.switchOn();
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -3.0);

    fader.fadeOut();
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -2.0);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -1.0);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), 0.0);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-4.0, 0.1)).value(), 0.0);
}

TEST(LinearFader, SwitchOnOff)
{
    LinearFader fader;
    fader.init(0.5, 0.3);

    // switch off during fading in
    fader.fadeIn();
    fader.switchOff();
    fader.process(0.1);
    EXPECT_DOUBLE_EQ(fader.fadingValue(), 0.0);
    fader.process(0.1);
    EXPECT_DOUBLE_EQ(fader.fadingValue(), 0.0);

    // switch on during fading out
    fader.setFadingValue(1.0);
    fader.fadeOut();
    fader.switchOn();
    fader.process(0.1);
    EXPECT_DOUBLE_EQ(fader.fadingValue(), 1.0);
    fader.process(0.1);
    EXPECT_DOUBLE_EQ(fader.fadingValue(), 1.0);
}

TEST(LinearFader, IdleAndSet)
{
    LinearFader fader;
    fader.init(1.0 / 3.0, 0.3);
    fader.fadeIn();
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -2.0);
    fader.idle();
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -2.0);

    fader.setFadingValue(0.5);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -1.5);

    // misuse
    fader.setFadingValue(1.5);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), -3.0);
    fader.setFadingValue(-1.5);
    EXPECT_DOUBLE_EQ(fader.processSignal(mapSignal(-3.0, 0.1)).value(), 0.0);
}

#ifndef NDEBUG
TEST(LinearFader, ParameterValidation)
{
    EXPECT_DEATH(LinearFader(-1.0), "");
    EXPECT_DEATH(LinearFader(2.0), "");
}
#else
TEST(LinearFader, ParameterValidation)
{
    LinearFader fader(-1.0);
    EXPECT_DOUBLE_EQ(fader.fadingValue(), 0.0);

    LinearFader anotherFader(2.0);
    EXPECT_DOUBLE_EQ(anotherFader.fadingValue(), 1.0);
}
#endif

} // namespace
