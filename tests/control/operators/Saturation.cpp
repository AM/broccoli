/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/operators/Saturation.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(Saturation, SignalBased)
{
    Signal<Eigen::Vector3d> input({ 1.0, 2.0, -3.0 });
    Signal<Eigen::Vector3d> min({ -1.0, -2.0, -2.0 });
    Signal<Eigen::Vector3d> max({ 0.5, 1.0, 1.0 });

    auto saturation = Saturation(input, min, max);

    EXPECT_TRUE(saturation.value().isApprox(Eigen::Vector3d(0.5, 1.0, -2.0)));
}

TEST(Saturation, LimitsBased)
{
    SignalLimits<const Eigen::Vector3d> limits({ -1.0, -2.0, -2.0 }, { 0.5, 1.0, 1.0 });

    Signal<Eigen::Vector3d> input({ 1.0, 2.0, -3.0 });

    auto saturation = Saturation(input, limits);
    EXPECT_TRUE(saturation.value().isApprox(Eigen::Vector3d(0.5, 1.0, -2.0)));
}

} // namespace
