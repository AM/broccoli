/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am/
 */

#include "broccoli/control/Signal.hpp"
#include "gtest/gtest.h"

using namespace broccoli::control;

namespace {

TEST(ElementaryOperators, SumDifferenceOfSignals)
{
    Signal<double> a(0.1, 0.1);
    Signal<double> b(1.0, 0.1);

    auto result = a + b + a + b - a;
    Signal<double> evaluatedResult = result;

    EXPECT_EQ(result.sampleTime(), 0.1);
    EXPECT_EQ(evaluatedResult.sampleTime(), 0.1);

    EXPECT_DOUBLE_EQ(result.value(), 2.1);
    EXPECT_DOUBLE_EQ(evaluatedResult.value(), 2.1);

    EXPECT_EQ((Signal<double>(1.0) + Signal<double>(2.0)).value(), 3.0);

    // Test auto mapping of fundamental types
    EXPECT_DOUBLE_EQ((a + 0.3).value(), 0.4);
    EXPECT_DOUBLE_EQ((0.2 - a).value(), 0.1);
}

TEST(ElementaryOperators, ProductOfSignals)
{
    Signal<double> a(2.0, 0.1);
    Signal<double> b(1.5, 0.1);

    auto result = a * b * a;
    Signal<double> evaluatedResult = result;

    EXPECT_EQ(result.sampleTime(), 0.1);
    EXPECT_EQ(evaluatedResult.sampleTime(), 0.1);

    EXPECT_DOUBLE_EQ(result.value(), 6.0);
    EXPECT_DOUBLE_EQ(evaluatedResult.value(), 6.0);

    // Test auto mapping of fundamental types
    auto anotherResult = a * 2.0 * b;
    EXPECT_DOUBLE_EQ(anotherResult.value(), 6.0);
}

TEST(ElementaryOperators, QuotientOfSignals)
{
    Signal<double> a(3.0, 0.1);
    Signal<double> b(1.5, 0.1);

    auto result = a / b;
    Signal<double> evaluatedResult = result;

    EXPECT_EQ(result.sampleTime(), 0.1);
    EXPECT_EQ(evaluatedResult.sampleTime(), 0.1);

    EXPECT_DOUBLE_EQ(result.value(), 2.0);
    EXPECT_DOUBLE_EQ(evaluatedResult.value(), 2.0);

    // Test auto mapping of fundamental types
    auto anotherResult = 6.0 / a * b;
    EXPECT_DOUBLE_EQ(anotherResult.value(), 3.0);
}

TEST(ElementaryOperators, UnaryOperators)
{
    Signal<double> a(3.0, 0.1);

    auto result = +a;
    EXPECT_DOUBLE_EQ(result.value(), 3.0);

    auto minusResult = -a;
    EXPECT_DOUBLE_EQ(minusResult.value(), -3.0);
}

TEST(ElementaryOperators, minMax)
{
    Signal<double> a(3.0);
    Signal<double> b(-1.0);

    EXPECT_DOUBLE_EQ(a.min(b).value(), -1.0);
    EXPECT_DOUBLE_EQ(a.max(b).value(), 3.0);

    Signal<Eigen::Vector3d> va({ 1.0, 2.0, 3.0 });
    Signal<Eigen::Vector3d> vb({ -1.0, -2.0, 2.0 });

    auto min = va.min(vb);

    EXPECT_DOUBLE_EQ(min.value()(0), -1.0);
    EXPECT_DOUBLE_EQ(min.value()(1), -2.0);
    EXPECT_DOUBLE_EQ(min.value()(2), 2.0);

    auto max = va.max(vb);
    EXPECT_DOUBLE_EQ(max.value()(0), 1.0);
    EXPECT_DOUBLE_EQ(max.value()(1), 2.0);
    EXPECT_DOUBLE_EQ(max.value()(2), 3.0);

    // vector <-> scalar
    auto minScalar = mapSignal(2.0).min(va);
    EXPECT_DOUBLE_EQ(minScalar.value()(0), 1.0);
    EXPECT_DOUBLE_EQ(minScalar.value()(1), 2.0);
    EXPECT_DOUBLE_EQ(minScalar.value()(2), 2.0);

    auto maxScalar = mapSignal(-1.0).max(vb);
    EXPECT_DOUBLE_EQ(maxScalar.value()(0), -1.0);
    EXPECT_DOUBLE_EQ(maxScalar.value()(1), -1.0);
    EXPECT_DOUBLE_EQ(maxScalar.value()(2), 2.0);

    // Test auto mapping of fundamental types
    EXPECT_DOUBLE_EQ(a.min(2.0).value(), 2.0);
    EXPECT_DOUBLE_EQ(a.max(2.0).value(), 3.0);
}

TEST(ElementaryOperators, elementWiseProduct)
{
    Signal<double> a(3.0);
    Signal<double> b(-1.0);

    EXPECT_DOUBLE_EQ(a.elementWiseProduct(b).value(), -3.0);

    Eigen::Vector3d va({ 1.0, 2.0, 3.0 });
    Eigen::Vector3d vb({ -1.0, -2.0, 2.0 });

    auto result = mapSignal(va).elementWiseProduct(mapSignal(vb));

    EXPECT_DOUBLE_EQ(result.value()(0), -1.0);
    EXPECT_DOUBLE_EQ(result.value()(1), -4.0);
    EXPECT_DOUBLE_EQ(result.value()(2), 6.0);

    // Test auto mapping of fundamental types
    EXPECT_DOUBLE_EQ(a.elementWiseProduct(-2.0).value(), -6.0);
}
}
