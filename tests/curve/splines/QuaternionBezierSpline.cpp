/*
 * This file is part of broccoli
 * Copyright (C) 2022 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mec.ed.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/splines/QuaternionBezierSpline.hpp"
#include "QuaternionSplineHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of quaternion Bezier splines
TEST(QuaternionBezierSpline, BasicUsage)
{
    // Check validity
    checkQuaternionSplineIsValidFixedSize<QuaternionBezierSpline, QuaternionBezierCurve>();

    // Check operators
    checkOperatorsQuaternionSpline<QuaternionBezierSpline>();

    // Check removeEmptySegments()
    checkRemoveEmptySegmentsQuaternionSpline<QuaternionBezierSpline>();

    // Construct
    QuaternionBezierSpline spline;
    spline.m_segments.resize(2);
    spline.m_segments[0].m_controlPoints[0] = Eigen::Quaterniond(1, 0, 0, 0);
    spline.m_segments[0].m_controlPoints[1] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[0].m_controlPoints[2] = Eigen::Quaterniond(0, 1, 1, 0);
    spline.m_segments[0].m_controlPoints[3] = Eigen::Quaterniond(0, 0, 1, 1);
    spline.m_segments[1].m_controlPoints[0] = Eigen::Quaterniond(0, 0, 1, 1);
    spline.m_segments[1].m_controlPoints[1] = Eigen::Quaterniond(0, 1, 1, 0);
    spline.m_segments[1].m_controlPoints[2] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[1].m_controlPoints[3] = Eigen::Quaterniond(1, 0, 0, 0);
    for (size_t i = 0; i < spline.m_segments.size(); i++)
        for (size_t j = 0; j < spline.m_segments[i].m_controlPoints.size(); j++)
            spline.m_segments[i].m_controlPoints[j].normalize();
    spline.m_segmentProportions = std::vector<double>(2);
    spline.m_segmentProportions[0] = 0.2;
    spline.m_segmentProportions[1] = 0.8;

    // Check segment boundaries
    checkGetSegmentBoundariesQuaternionSpline<decltype(spline)>(spline);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionSpline<decltype(spline), 2>(spline);

    // Check encoding to XML
    checkEncodeToXMLQuaternionSpline<QuaternionBezierSpline, QuaternionBezierCurve>();
}

//! Check interpolation with Bezier
TEST(QuaternionBezierSpline, InterpolatePiecewise)
{
    // Initialize helpers
    QuaternionSplineResult result;

    // Create control point quaternions
    auto keyFrames = keyFramesSetBQuaternionInterpolation();

    // Setup spline
    QuaternionBezierSpline spline;
    bool returnValue = spline.interpolate(keyFrames, nullptr /* empty -> use uniform proportions */, QuaternionSplineInterpolationMethod::BEZIER_PIECEWISE, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Write plot files for visual debugging
    plotQuaternionSpline2D<decltype(spline)>(spline, 0.001, 2, "QuaternionBezierSplinePiecewise2D");
    plotQuaternionSpline3D<decltype(spline)>(spline, 0.001, "QuaternionBezierSplinePiecewise3D");
}

//! Check interpolation with Bezier
TEST(QuaternionBezierSpline, InterpolateSmooth)
{
    // Initialize helpers
    QuaternionSplineResult result;

    // Create control point quaternions
    auto keyFrames = keyFramesSetAQuaternionInterpolation();
    auto customProportions = keyFramesSetAQuaternionInterpolationCustomProportions();

    // Setup spline (uniform proportions)
    QuaternionBezierSpline splineUniform;
    const bool returnValueUniform = splineUniform.interpolate(keyFrames, nullptr /* empty -> use uniform proportions */, QuaternionSplineInterpolationMethod::BEZIER_SMOOTH, &result);
    ASSERT_EQ(returnValueUniform, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Setup spline (custom proportions)
    QuaternionBezierSpline splineCustom;
    const bool returnValueCustom = splineCustom.interpolate(keyFrames, &customProportions, QuaternionSplineInterpolationMethod::BEZIER_SMOOTH, &result);
    ASSERT_EQ(returnValueCustom, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeCustomQuaternionSpline<decltype(splineUniform)>(splineUniform, 1);
    compareAnalyticAndNumericDerivativeCustomQuaternionSpline<decltype(splineCustom)>(splineCustom, 1);

    // Write plot files for visual debugging
    plotQuaternionSpline2D<decltype(splineUniform)>(splineUniform, 0.001, 2, "QuaternionBezierSplineSmooth2DUniform");
    plotQuaternionSpline3D<decltype(splineUniform)>(splineUniform, 0.001, "QuaternionBezierSplineSmooth3DUniform");
    plotQuaternionSpline2D<decltype(splineCustom)>(splineCustom, 0.001, 2, "QuaternionBezierSplineSmooth2DCustom");
    plotQuaternionSpline3D<decltype(splineCustom)>(splineCustom, 0.001, "QuaternionBezierSplineSmooth3DCustom");
}

#endif // HAVE_EIGEN3
