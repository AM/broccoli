/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/curve/splines/PolynomialSpline.hpp"
#include "SplineHelpers.hpp"
#include "broccoli/curve/splines/PolynomialSplineSplitter.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Template function to check basic usage of polynomial splines of the given degree
template <unsigned int Degree>
static void checkBasicUsagePolynomialSpline()
{
    // Check validity
    checkIsValidSpline<PolynomialSpline<Degree>, PolynomialCurve<Degree>>();

    // Check operators
    checkOperatorsSpline<PolynomialSpline<Degree>>();

    // Check removeEmptySegments()
    checkRemoveEmptySegmentsSpline<PolynomialSpline<Degree>>();

    // Construct splines
    PolynomialSpline<Degree> splineA;
    splineA.m_segments.push_back(PolynomialCurve<Degree>());
    splineA.m_segments.push_back(PolynomialCurve<Degree>());
    for (unsigned int i = 0; i <= Degree; i++) {
        splineA.m_segments[0].m_coefficients[i] = 1.23 + i * 4.56;
        splineA.m_segments[1].m_coefficients[i] = 1.23 - i * 4.56;
    }
    splineA.m_segmentProportions = std::vector<double>(2, 0.5);
    PolynomialSpline<Degree> splineB;
    splineB.m_segments.push_back(PolynomialCurve<Degree>());
    for (unsigned int i = 0; i <= Degree; i++)
        splineA.m_segments[0].m_coefficients[i] = 1.23 + i * 4.56;
    splineB.m_segmentProportions = std::vector<double>(1, 1.0);

    // Check segment boundaries
    checkGetSegmentBoundariesSpline<decltype(splineA)>(splineA);
    checkGetSegmentBoundariesSpline<decltype(splineB)>(splineB);

    // Check high performance evaluation
    checkHighPerformanceEvaluationSpline<decltype(splineA), 2>(splineA);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeCustomSpline<PolynomialSpline<Degree>>(splineB, 1);

    // Check splitting and joining (only for selected degrees)
    if (Degree == 0 || Degree == 1 || Degree == 3 || Degree == 5)
        checkSplitAndJoinSpline<PolynomialSplineSplitter<Degree>, PolynomialSpline<Degree>, PolynomialCurve<Degree>, 3>(splineA);

    // Check encoding to XML
    checkEncodeToXMLSpline<PolynomialSpline<Degree>, PolynomialCurve<Degree>>();
}

//! Test basic usage of polynomial splines
TEST(PolynomialSpline, BasicUsage)
{
    checkBasicUsagePolynomialSpline<0>();
    checkBasicUsagePolynomialSpline<1>();
    checkBasicUsagePolynomialSpline<2>();
    checkBasicUsagePolynomialSpline<3>();
    checkBasicUsagePolynomialSpline<4>();
    checkBasicUsagePolynomialSpline<5>();
}

//! Checks getters of polynomial spline of certain degree
template <unsigned int Degree>
bool checkGettersPolynomialSpline()
{
    PolynomialSpline<Degree> spline;
    if (spline.degree() != Degree)
        return false;
    if (spline.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of polynomial spline
TEST(PolynomialSpline, Getters)
{
    ASSERT_TRUE(checkGettersPolynomialSpline<0>());
    ASSERT_TRUE(checkGettersPolynomialSpline<1>());
    ASSERT_TRUE(checkGettersPolynomialSpline<2>());
    ASSERT_TRUE(checkGettersPolynomialSpline<3>());
}

//! Check construction and interpolation of polynomial spline of degree zero
TEST(PolynomialSpline, DegreeZero)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<0> spline;

    // Step 1: test with non-uniform (custom) proportions
    // --------------------------------------------------
    // Setup parameter set
    std::vector<double> parameters;
    std::vector<double> proportions;
    parameters.resize(5);
    proportions.resize(5);

    // Function values
    parameters[0] = -2;
    parameters[1] = -1;
    parameters[2] = 0;
    parameters[3] = 1;
    parameters[4] = 2;

    // Proportions
    proportions[0] = 0.2;
    proportions[1] = 0.2;
    proportions[2] = 0.2;
    proportions[3] = 0.2;
    proportions[4] = 0.2;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CONSTANT, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size());
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Check function values
    ASSERT_TRUE(spline.evaluate(0.0000, 0) == -2);
    ASSERT_TRUE(spline.evaluate(0.1000, 0) == -2);
    ASSERT_TRUE(spline.evaluate(0.1999, 0) == -2);
    ASSERT_TRUE(spline.evaluate(0.2001, 0) == -1);
    ASSERT_TRUE(spline.evaluate(0.3999, 0) == -1);
    ASSERT_TRUE(spline.evaluate(0.4001, 0) == 0);
    ASSERT_TRUE(spline.evaluate(0.5999, 0) == 0);
    ASSERT_TRUE(spline.evaluate(0.6001, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.7999, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.8001, 0) == 2);
    ASSERT_TRUE(spline.evaluate(1.0000, 0) == 2);

    // Check derivatives
    ASSERT_TRUE(spline.evaluate(0.1, 1) == 0);
    ASSERT_TRUE(spline.evaluate(0.1, 2) == 0);
    ASSERT_TRUE(spline.evaluate(0.1, 3) == 0);
    ASSERT_TRUE(spline.evaluate(0.1, 4) == 0);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<0>, PolynomialSpline<0>, PolynomialCurve<0>, 3>(spline);

    // Step 2: test with uniform proportions
    // -------------------------------------
    // Setup parameter set
    parameters.resize(5);
    proportions.clear();

    // Function values
    parameters[0] = -2;
    parameters[1] = -1;
    parameters[2] = 0;
    parameters[3] = 1;
    parameters[4] = 2;

    // Trigger interpolation (wrong method)
#ifdef NDEBUG
    returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_LINEAR, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
    returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CUBIC_FIRST_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
    returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
    returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
#ifdef HAVE_EIGEN3
    returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
    returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
#endif // HAVE_EIGEN3
#endif // NDEBUG

    // Trigger interpolation (correct method)
    returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CONSTANT, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size());
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<0>>(spline, 0.001, 1, "PolynomialSplineDegreeZero");

    // Check function values
    ASSERT_TRUE(spline.evaluate(0.0000, 0) == -2);
    ASSERT_TRUE(spline.evaluate(0.1000, 0) == -2);
    ASSERT_TRUE(spline.evaluate(0.1999, 0) == -2);
    ASSERT_TRUE(spline.evaluate(0.2001, 0) == -1);
    ASSERT_TRUE(spline.evaluate(0.3999, 0) == -1);
    ASSERT_TRUE(spline.evaluate(0.4001, 0) == 0);
    ASSERT_TRUE(spline.evaluate(0.5999, 0) == 0);
    ASSERT_TRUE(spline.evaluate(0.6001, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.7999, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.8001, 0) == 2);
    ASSERT_TRUE(spline.evaluate(1.0000, 0) == 2);

    // Check derivatives
    ASSERT_TRUE(spline.evaluate(0.1, 1) == 0);
    ASSERT_TRUE(spline.evaluate(0.1, 2) == 0);
    ASSERT_TRUE(spline.evaluate(0.1, 3) == 0);
    ASSERT_TRUE(spline.evaluate(0.1, 4) == 0);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<0>, PolynomialSpline<0>, PolynomialCurve<0>, 3>(spline);
}

//! Check construction and interpolation of polynomial spline of degree one
TEST(PolynomialSpline, DegreeOne)
{
    // Initialize helpers
    SplineResult result;
    bool returnValue;

    // Initialize container
    PolynomialSpline<1> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(3);
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = -1;

    // Trigger interpolation (wrong method)
#ifdef NDEBUG
    returnValue = spline.interpolate(parameters, nullptr, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CUBIC_FIRST_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
    returnValue = spline.interpolate(parameters, nullptr, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
    returnValue = spline.interpolate(parameters, nullptr, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
#ifdef HAVE_EIGEN3
    returnValue = spline.interpolate(parameters, nullptr, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
    returnValue = spline.interpolate(parameters, nullptr, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
#endif // HAVE_EIGEN3
#endif // NDEBUG

    // Trigger interpolation (correct method)
    returnValue = spline.interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_LINEAR, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() - 1);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<1>>(spline, 0.001, 2, "PolynomialSplineDegreeOne");

    // Check function values
    ASSERT_TRUE(spline.evaluate(0.00, 0) == 0);
    ASSERT_TRUE(spline.evaluate(0.25, 0) == 0.5);
    ASSERT_TRUE(spline.evaluate(0.50, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.75, 0) == 0);
    ASSERT_TRUE(spline.evaluate(1.00, 0) == -1);

    // Check derivatives
    ASSERT_TRUE(spline.evaluate(0.25, 1) == 2);
    ASSERT_TRUE(spline.evaluate(0.75, 1) == -4);
    ASSERT_TRUE(spline.evaluate(0.25, 2) == 0);
    ASSERT_TRUE(spline.evaluate(0.75, 2) == 0);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<1>, PolynomialSpline<1>, PolynomialCurve<1>, 3>(spline);
}

//! Check construction and interpolation of polynomial spline of degree three (with given first order derivatives)
TEST(PolynomialSpline, DegreeThreeFirstDerivatives)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<3> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(10, 0);
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = 1;
    parameters[3] = 1;
    parameters[4] = 0;
    parameters[5] = 0;
    parameters[6] = 10;
    parameters[7] = 10;
    parameters[8] = 10;
    parameters[9] = 0;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CUBIC_FIRST_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() / 2 - 1);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<3>>(spline, 0.001, 2, "PolynomialSplineDegreeThreeFirstDerivatives");

    // Check function values
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 0) - 0) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 0) - 1) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 0) - 1) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 0) - 1) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 0) - 0) < 1e-10);

    // Check derivatives
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 1) - 0) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 1) - 10) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 1) - 10) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 1) - 10) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 1) - 0) < 1e-10);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<3>, PolynomialSpline<3>, PolynomialCurve<3>, 3>(spline);
}

//! Check construction and interpolation of polynomial spline of degree three (with given second order derivatives)
TEST(PolynomialSpline, DegreeThreeSecondDerivatives)
{
    // Initialize helpers
    SplineResult result;
    bool returnValue;

    // Initialize container
    PolynomialSpline<3> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(10, 0);
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = 1;
    parameters[3] = 1;
    parameters[4] = 0;
    parameters[5] = 0;
    parameters[6] = 10;
    parameters[7] = 10;
    parameters[8] = 10;
    parameters[9] = 0;

    // Trigger interpolation (wrong method)
#ifdef NDEBUG
    returnValue = spline.interpolate(parameters, nullptr, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
#ifdef HAVE_EIGEN3
    returnValue = spline.interpolate(parameters, nullptr, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, false);
    ASSERT_EQ(result, SplineResult::ERROR_DEGREE_TOO_LOW);
#endif // HAVE_EIGEN3
#endif // NDEBUG

    // Trigger interpolation (correct method)
    returnValue = spline.interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() / 2 - 1);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<3>>(spline, 0.001, 2, "PolynomialSplineDegreeThreeSecondDerivatives");

    // Check function values
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 0) - 0) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 0) - 1) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 0) - 1) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 0) - 1) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 0) - 0) < 1e-10);

    // Check derivatives
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 2) - 0) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 2) - 10) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 2) - 10) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 2) - 10) < 1e-10);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 2) - 0) < 1e-10);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<3>, PolynomialSpline<3>, PolynomialCurve<3>, 3>(spline);
}

//! Check construction and interpolation of polynomial spline of degree five (given first and second order derivatives)
TEST(PolynomialSpline, DegreeFiveFirstAndSecondDerivatives)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<5> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(15, 0);
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = 1;
    parameters[3] = 1;
    parameters[4] = 0;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() / 3 - 1);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<5>>(spline, 0.001, 2, "PolynomialSplineDegreeFiveFirstAndSecondDerivatives");

    // Check function values
    ASSERT_TRUE(spline.evaluate(0.00, 0) == 0);
    ASSERT_TRUE(spline.evaluate(0.125, 0) == 0.5);
    ASSERT_TRUE(spline.evaluate(0.25, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.50, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.75, 0) == 1);
    ASSERT_TRUE(spline.evaluate(0.875, 0) == 0.5);
    ASSERT_TRUE(spline.evaluate(1.00, 0) == 0);

    // Check derivatives
    ASSERT_TRUE(spline.evaluate(0.00, 1) == 0);
    ASSERT_TRUE(spline.evaluate(0.25, 1) == 0);
    ASSERT_TRUE(spline.evaluate(0.50, 1) == 0);
    ASSERT_TRUE(spline.evaluate(0.75, 1) == 0);
    ASSERT_TRUE(spline.evaluate(1.00, 1) == 0);
    ASSERT_TRUE(spline.evaluate(0.00, 2) == 0);
    ASSERT_TRUE(spline.evaluate(0.25, 2) == 0);
    ASSERT_TRUE(spline.evaluate(0.50, 2) == 0);
    ASSERT_TRUE(spline.evaluate(0.75, 2) == 0);
    ASSERT_TRUE(spline.evaluate(1.00, 2) == 0);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<5>, PolynomialSpline<5>, PolynomialCurve<5>, 3>(spline);
}

#ifdef HAVE_EIGEN3
//! Check construction and interpolation of smooth cubic spline with only one segment (special case)
TEST(PolynomialSpline, SmoothCubicSingleSegment)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<3> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(4, 0);
    parameters[0] = 1.0;
    parameters[1] = 2.0;
    parameters[2] = 0.1;
    parameters[3] = 0.2;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() - 3);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<3>>(spline, 0.001, 2, "PolynomialSplineSmoothCubicSingleSegment");

    // Check interpolation
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 0) - parameters[0]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 0) - parameters[1]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 2) - parameters[2]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 2) - parameters[3]) < 1e-6);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<3>, PolynomialSpline<3>, PolynomialCurve<3>, 3>(spline);
}

//! Check construction and interpolation of smooth cubic spline with only two segments (special case)
TEST(PolynomialSpline, SmoothCubicTwoSegments)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<3> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(5, 0);
    parameters[0] = 0.5;
    parameters[1] = 1.5;
    parameters[2] = 2.0;
    parameters[3] = 0.1;
    parameters[4] = 0.2;
    std::vector<double> proportions;
    proportions.resize(2);
    proportions[0] = 0.2;
    proportions[1] = 0.8;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() - 3);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<3>>(spline, 0.001, 3, "PolynomialSplineSmoothCubicTwoSegments");

    // Check interpolation
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 0) - parameters[0]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.2, 0) - parameters[1]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 0) - parameters[2]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 2) - parameters[3]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 2) - parameters[4]) < 1e-6);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<3>, PolynomialSpline<3>, PolynomialCurve<3>, 3>(spline);
}

//! Check construction and interpolation of smooth cubic spline
TEST(PolynomialSpline, SmoothCubic)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<3> spline;

    // Setup parameter set
    std::vector<double> proportions;
    std::vector<double> parameters;
    parameters.resize(7, 0);
    parameters[0] = 0.0;
    parameters[1] = 1.0;
    parameters[2] = 0.5;
    parameters[3] = 2.0;
    parameters[4] = 3.0;
    parameters[5] = 0.1;
    parameters[6] = 0.2;
    proportions.resize(4, 0);
    proportions[0] = 0.1;
    proportions[1] = 0.2;
    proportions[2] = 0.3;
    proportions[3] = 0.4;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_CUBIC_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() - 3);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<3>>(spline, 0.001, 2, "PolynomialSplineSmoothCubic");

    // Check interpolation
    double splinePosition = 0;
    for (size_t i = 0; i < parameters.size() - 2; i++) {
        ASSERT_TRUE(fabs(spline.evaluate(splinePosition, 0) - parameters[i]) < 1e-6);
        if (i < proportions.size())
            splinePosition += proportions[i];
    }
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 2) - parameters[5]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 2) - parameters[6]) < 1e-6);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<3>, PolynomialSpline<3>, PolynomialCurve<3>, 3>(spline);
}

//! Check construction and interpolation of smooth quintic spline with only one segment (special case)
TEST(PolynomialSpline, SmoothQuinticSingleSegment)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<5> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(6, 0);
    parameters[0] = 1.0;
    parameters[1] = 2.0;
    parameters[2] = 3.0;
    parameters[3] = 4.0;
    parameters[4] = 0.1;
    parameters[5] = 0.2;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() - 5);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<5>>(spline, 0.001, 5, "PolynomialSplineSmoothQuinticSingleSegment");

    // Check interpolation
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 0) - parameters[0]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 0) - parameters[1]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 1) - parameters[2]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 1) - parameters[3]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 2) - parameters[4]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 2) - parameters[5]) < 1e-6);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<5>, PolynomialSpline<5>, PolynomialCurve<5>, 3>(spline);
}

//! Check construction and interpolation of smooth quintic spline with only two segments (special case)
TEST(PolynomialSpline, SmoothQuinticTwoSegments)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<5> spline;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(7, 0);
    parameters[0] = 0.0;
    parameters[1] = 1.0;
    parameters[2] = 0.0;
    parameters[3] = 0.0;
    parameters[4] = 0.0;
    parameters[5] = 0.0;
    parameters[6] = 0.0;
    std::vector<double> proportions;
    proportions.resize(2);
    proportions[0] = 0.2;
    proportions[1] = 0.8;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() - 5);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<5>>(spline, 0.001, 5, "PolynomialSplineSmoothQuinticTwoSegments");

    // Check interpolation
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 0) - parameters[0]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.2, 0) - parameters[1]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 0) - parameters[2]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 1) - parameters[3]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 1) - parameters[4]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 2) - parameters[5]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 2) - parameters[6]) < 1e-6);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<5>, PolynomialSpline<5>, PolynomialCurve<5>, 3>(spline);
}

//! Check construction and interpolation of smooth quintic spline
TEST(PolynomialSpline, SmoothQuintic)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialSpline<5> spline;

    // Setup parameter set
    std::vector<double> proportions;
    std::vector<double> parameters;
    parameters.resize(9, 0);
    parameters[0] = 0.0;
    parameters[1] = 1.0;
    parameters[2] = 0.5;
    parameters[3] = 2.0;
    parameters[4] = 3.0;
    parameters[5] = 0.1;
    parameters[6] = 0.2;
    parameters[7] = 0.3;
    parameters[8] = 0.4;
    proportions.resize(4, 0);
    proportions[0] = 0.1;
    proportions[1] = 0.2;
    proportions[2] = 0.3;
    proportions[3] = 0.4;

    // Trigger interpolation
    bool returnValue = spline.interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(spline.m_segments.size() == parameters.size() - 5);
    ASSERT_TRUE(spline.m_segmentProportions.size() == spline.m_segments.size());

    // Write plot file for visual debugging
    plotSpline<PolynomialSpline<5>>(spline, 0.001, 5, "PolynomialSplineSmoothQuintic");

    // Check interpolation
    double splinePosition = 0;
    for (size_t i = 0; i < parameters.size() - 4; i++) {
        ASSERT_TRUE(fabs(spline.evaluate(splinePosition, 0) - parameters[i]) < 1e-6);
        if (i < proportions.size())
            splinePosition += proportions[i];
    }
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 1) - parameters[5]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 1) - parameters[6]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(0.0, 2) - parameters[7]) < 1e-6);
    ASSERT_TRUE(fabs(spline.evaluate(1.0, 2) - parameters[8]) < 1e-6);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);

    // Check split and join
    checkSplitAndJoinSpline<PolynomialSplineSplitter<5>, PolynomialSpline<5>, PolynomialCurve<5>, 3>(spline);
}
#endif // HAVE_EIGEN3
