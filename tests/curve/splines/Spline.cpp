/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/splines/Spline.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test string representation of the given result type for spline algorithms
TEST(Spline, SplineResultToString)
{
    uint8_t i;
    SplineResult testSplineResult;
    for (i = 0; i < static_cast<uint8_t>(SplineResult::SPLINERESULT_COUNT); i++) {
        testSplineResult = static_cast<SplineResult>(i);
        ASSERT_GT(splineResultString(testSplineResult).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(splineResultString(testSplineResult) == "UNKNOWN");
        } else {
            ASSERT_TRUE(splineResultString(testSplineResult) != "UNKNOWN");
        }
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(SplineResult::SPLINERESULT_COUNT);
    testSplineResult = static_cast<SplineResult>(i);
    ASSERT_TRUE(splineResultString(testSplineResult) == "UNKNOWN");
#endif // NDEBUG
}
