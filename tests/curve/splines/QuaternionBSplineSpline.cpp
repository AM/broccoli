/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/splines/QuaternionBSplineSpline.hpp"
#include "QuaternionSplineHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of quaternion BSpline splines
TEST(QuaternionBSplineSpline, BasicUsage)
{
    // Check validity
    checkQuaternionSplineIsValidBSpline<0>();
    checkQuaternionSplineIsValidBSpline<1>();
    checkQuaternionSplineIsValidBSpline<2>();
    checkQuaternionSplineIsValidBSpline<3>();
    checkQuaternionSplineIsValidBSpline<4>();
    checkQuaternionSplineIsValidBSpline<5>();

    // Construct
    QuaternionBSplineSpline<3, 4> spline;
    spline.m_segments.resize(2);
    spline.m_segments[0].m_basis.setClampedUniformKnots(4);
    spline.m_segments[0].m_controlPoints.resize(4);
    spline.m_segments[0].m_controlPoints[0] = Eigen::Quaterniond(1, 0, 0, 0);
    spline.m_segments[0].m_controlPoints[1] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[0].m_controlPoints[2] = Eigen::Quaterniond(0, 1, 1, 0);
    spline.m_segments[0].m_controlPoints[3] = Eigen::Quaterniond(0, 0, 1, 1);
    spline.m_segments[1].m_basis.setClampedUniformKnots(4);
    spline.m_segments[1].m_controlPoints.resize(4);
    spline.m_segments[1].m_controlPoints[0] = Eigen::Quaterniond(0, 0, 1, 1);
    spline.m_segments[1].m_controlPoints[1] = Eigen::Quaterniond(0, 1, 1, 0);
    spline.m_segments[1].m_controlPoints[2] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[1].m_controlPoints[3] = Eigen::Quaterniond(1, 0, 0, 0);
    for (size_t i = 0; i < spline.m_segments.size(); i++)
        for (size_t j = 0; j < spline.m_segments[i].m_controlPoints.size(); j++)
            spline.m_segments[i].m_controlPoints[j].normalize();
    spline.m_segmentProportions = std::vector<double>(2);
    spline.m_segmentProportions[0] = 0.2;
    spline.m_segmentProportions[1] = 0.8;

    // Check segment boundaries
    checkGetSegmentBoundariesQuaternionSpline<decltype(spline)>(spline);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionSpline<decltype(spline), 2>(spline);

    // Check encoding to XML
    checkEncodeToXMLQuaternionSpline<QuaternionBSplineSpline<0, 1>, QuaternionBSplineCurve<0, 1>>();
    checkEncodeToXMLQuaternionSpline<QuaternionBSplineSpline<1, 2>, QuaternionBSplineCurve<1, 2>>();
    checkEncodeToXMLQuaternionSpline<QuaternionBSplineSpline<2, 3>, QuaternionBSplineCurve<2, 3>>();
    checkEncodeToXMLQuaternionSpline<QuaternionBSplineSpline<3, 4>, QuaternionBSplineCurve<3, 4>>();
    checkEncodeToXMLQuaternionSpline<QuaternionBSplineSpline<4, 5>, QuaternionBSplineCurve<4, 5>>();
    checkEncodeToXMLQuaternionSpline<QuaternionBSplineSpline<5, 6>, QuaternionBSplineCurve<5, 6>>();
}

//! Checks getters of quaternion B-Spline spline of certain degree
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkGettersQuaternionBSplineSpline()
{
    QuaternionBSplineSpline<Degree, MaximumBSplineCount> spline;
    if (spline.degree() != Degree)
        return false;
    if (spline.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of quaternion B-spline spline
TEST(QuaternionBSplineSpline, Getters)
{
    ASSERT_TRUE(checkGettersQuaternionBSplineSpline<0>());
    ASSERT_TRUE(checkGettersQuaternionBSplineSpline<1>());
    ASSERT_TRUE(checkGettersQuaternionBSplineSpline<2>());
    ASSERT_TRUE(checkGettersQuaternionBSplineSpline<3>());
    ASSERT_TRUE(checkGettersQuaternionBSplineSpline<4>());
    ASSERT_TRUE(checkGettersQuaternionBSplineSpline<5>());
}

//! Check interpolation with BSpline
TEST(QuaternionBSplineSpline, InterpolateSimple)
{
    // Initialize helpers
    QuaternionSplineResult result;

    // Create control point quaternions
    auto keyFrames = keyFramesSetAQuaternionInterpolation();

    // Setup spline
    QuaternionBSplineSpline<3, 7> spline;
    bool returnValue = spline.interpolate(keyFrames, nullptr /* empty -> use uniform proportions */, QuaternionSplineInterpolationMethod::BSPLINE_SIMPLE, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeCustomQuaternionSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomQuaternionSpline<decltype(spline)>(spline, 2);

    // Write plot files for visual debugging
    plotQuaternionSpline2D<decltype(spline)>(spline, 0.001, 3, "QuaternionBSplineSplineSimple2D");
    plotQuaternionSpline3D<decltype(spline)>(spline, 0.001, "QuaternionBSplineSplineSimple3D");
}

#endif // HAVE_EIGEN3
