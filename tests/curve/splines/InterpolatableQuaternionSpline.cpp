/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/splines/InterpolatableQuaternionSpline.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Check quaternionSplineInterpolationMethodString() function
TEST(InterpolatableQuaternionSpline, QuaternionSplineInterpolationMethodString)
{
    // Pass through all elements
    for (uint64_t i = 0; i < static_cast<uint64_t>(QuaternionSplineInterpolationMethod::QUATERNIONSPLINEINTERPOLATIONMETHOD_COUNT); i++) {
        // Get string representation
        std::string str = quaternionSplineInterpolationMethodString(static_cast<QuaternionSplineInterpolationMethod>(i));

        // Check, if string is non-empty
        ASSERT_GT(str.size(), 0);

        // Check, if element is known
        ASSERT_FALSE(str == "UNKNOWN");
    }

#ifdef NDEBUG
    // Check out of bounds
    ASSERT_TRUE(quaternionSplineInterpolationMethodString(QuaternionSplineInterpolationMethod::QUATERNIONSPLINEINTERPOLATIONMETHOD_COUNT) == "UNKNOWN");
#endif // NDEBUG
}

#endif // HAVE_EIGEN3
