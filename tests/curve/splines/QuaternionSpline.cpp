/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/splines/QuaternionSpline.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Check quaternionSplineResultString() function
TEST(QuaternionSpline, QuaternionSplineResultString)
{
    // Pass through all elements
    for (uint64_t i = 0; i < static_cast<uint64_t>(QuaternionSplineResult::QUATERNIONSPLINERESULT_COUNT); i++) {
        // Get string representation
        std::string str = quaternionSplineResultString(static_cast<QuaternionSplineResult>(i));

        // Check, if string is non-empty
        ASSERT_GT(str.size(), 0);

        // Check, if element is known
        if (i > 0)
            ASSERT_FALSE(str == "UNKNOWN");
    }

#ifdef NDEBUG
    // Check out of bounds
    ASSERT_TRUE(quaternionSplineResultString(QuaternionSplineResult::QUATERNIONSPLINERESULT_COUNT) == "UNKNOWN");
#endif // NDEBUG
}

#endif // HAVE_EIGEN3
