/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

#include "../curves/CurveHelpers.hpp"
#include "broccoli/curve/splines/Spline.hpp"
#include "broccoli/curve/splines/SplineSplitter.hpp"
#include "gtest/gtest.h"
#include <fstream>
#include <iostream>
#include <string>

//! Template function to check isValid() of Spline
template <class SplineType, class CurveType>
inline void checkIsValidSpline()
{
    // Initialize helpers
    broccoli::curve::SplineResult result;

    // Initialize container
    SplineType spline;

    // Check no segments
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::SplineResult::ERROR_INVALID_SPLINE_NO_SEGMENTS);

    // Check dimension mismatch
    spline.m_segments.push_back(CurveType());
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::SplineResult::ERROR_INVALID_SPLINE_PROPORTION_COUNT);

    // Check negative proportions
    spline.m_segmentProportions.push_back(-1);
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::SplineResult::ERROR_INVALID_SPLINE_PROPORTION_NEGATIVE);

    // Check wrong sum of proportions
    spline.m_segmentProportions.clear();
    spline.m_segmentProportions.push_back(0.5);
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::SplineResult::ERROR_INVALID_SPLINE_PROPORTION_SUM);

    // Check success
    spline.m_segmentProportions.clear();
    spline.m_segmentProportions.push_back(1);
    ASSERT_EQ(spline.isValid(&result), true);
    ASSERT_EQ(result, broccoli::curve::SplineResult::SUCCESS);
}

//! Template function to test spline operators
template <class SplineType>
inline void checkOperatorsSpline()
{
    // Construct splines
    SplineType splineA;
    SplineType splineB; // same
    SplineType splineC; // different segments
    SplineType splineD; // differet segment proportions
    splineA.m_segments.resize(2);
    splineA.m_segmentProportions = std::vector<double>(2, 0.5);
    splineB = splineA;
    splineC.m_segments.resize(1);
    splineC.m_segmentProportions = std::vector<double>(1, 1.0);
    splineD.m_segments.resize(2);
    splineD.m_segmentProportions = std::vector<double>(1);
    splineD.m_segmentProportions[0] = 0.2;
    splineD.m_segmentProportions[0] = 0.8;

    // Equality operators
    ASSERT_TRUE(splineA == splineB);
    ASSERT_FALSE(splineA != splineB);
    ASSERT_FALSE(splineA == splineC);
    ASSERT_TRUE(splineA != splineC);
    ASSERT_FALSE(splineA == splineD);
    ASSERT_TRUE(splineA != splineD);
}

//! Template function to compare analytic and numeric derivative of splines (custom)
template <class SplineType>
inline void compareAnalyticAndNumericDerivativeCustomSpline(const SplineType& spline, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    compareAnalyticAndNumericDerivativeGenericCustom<SplineType>(spline, 0.1, 0.9, derivationOrder, toleratedRelativeError);
}

//! Template function to check high performance evaluation
template <class SplineType, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationSpline(const SplineType& spline)
{
    checkHighPerformanceEvaluationGeneric<SplineType, MaximumDerivation>(spline, 0, 1);
}

//! Template function for plotting splines (using gnuplot)
template <class SplineType>
inline void plotSpline(const SplineType& spline, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    plotGeneric<SplineType>(spline, 0, 1, stepSize, maximumDerivation, fileName);
}

//! Template function to check encodeToXML() of Spline
template <class SplineType, class CurveType>
inline void checkEncodeToXMLSpline()
{
    // Initialize container
    SplineType spline;
    spline.m_segments.push_back(CurveType());

    // Initialize helpers
    broccoli::io::encoding::CharacterStream stream;
    broccoli::io::encoding::CharacterStreamSize streamSize = spline.encodeToXML(stream, 1, 4);
    ASSERT_GT(streamSize, 0);
    ASSERT_EQ(streamSize, stream.size());
}

//! Template function to check getSegmentBoundaries() of splines
template <class SplineType>
inline void checkGetSegmentBoundariesSpline(const SplineType& spline)
{
    // Get boundaries
    std::vector<double> segmentBoundaries = spline.getSegmentBoundaries();
    ASSERT_EQ(segmentBoundaries.size(), spline.m_segmentProportions.size() + 1);

    // Check values
    double currentPosition = 0;
    for (size_t i = 0; i < spline.m_segmentProportions.size(); i++) {
        ASSERT_TRUE(fabs(segmentBoundaries[i] - currentPosition) < 1e-9);
        currentPosition += spline.m_segmentProportions[i];
    }
    ASSERT_TRUE(fabs(segmentBoundaries.back() - 1.0) < 1e-6);
}

//! Template function to check removeEmptySegments() function of spline
template <class SplineType>
inline void checkRemoveEmptySegmentsSpline()
{
    SplineType spline;
    ASSERT_EQ(spline.removeEmptySegments(), 0);
    spline.m_segments.resize(13);
    for (size_t i = 0; i < 13; i++)
        spline.m_segments[i].m_coefficients[0] = i;
    spline.m_segmentProportions.resize(13, 0.1);
    spline.m_segmentProportions[1] = 0;
    spline.m_segmentProportions[2] = 0;
    spline.m_segmentProportions[4] = 0;
    ASSERT_EQ(spline.removeEmptySegments(), 3);
    ASSERT_EQ(spline.m_segments.size(), 10);
    ASSERT_EQ(spline.m_segmentProportions.size(), 10);
    ASSERT_EQ(spline.m_segments[0].m_coefficients[0], 0);
    ASSERT_EQ(spline.m_segments[1].m_coefficients[0], 3);
    ASSERT_EQ(spline.m_segments[2].m_coefficients[0], 5);
    ASSERT_EQ(spline.m_segments[3].m_coefficients[0], 6);
    ASSERT_EQ(spline.m_segments[4].m_coefficients[0], 7);
    ASSERT_EQ(spline.m_segments[5].m_coefficients[0], 8);
    ASSERT_EQ(spline.m_segments[6].m_coefficients[0], 9);
    ASSERT_EQ(spline.m_segments[7].m_coefficients[0], 10);
    ASSERT_EQ(spline.m_segments[8].m_coefficients[0], 11);
    ASSERT_EQ(spline.m_segments[9].m_coefficients[0], 12);
}

//! Template function to check splitting and joining splines
template <class SplitterType, class SplineType, class CurveType, unsigned int maximumDerivative>
inline void checkSplitAndJoinSpline(const SplineType& spline, const double& tolerance = 1e-6)
{
    // Initialize helpers
    broccoli::curve::SplineResult splineResult = broccoli::curve::SplineResult::UNKNOWN;

    // Setup test cases
    std::vector<std::vector<double>> testCaseProportions;
    testCaseProportions.push_back(std::vector<double>{ 1.0 });
    testCaseProportions.push_back(std::vector<double>{ 0.5, 0.5 });
    testCaseProportions.push_back(std::vector<double>{ 1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0 });
    testCaseProportions.push_back(std::vector<double>{ 0.1, 0.2, 0.3, 0.4 });
    testCaseProportions.push_back(std::vector<double>{ 0.4, 0.3, 0.2, 0.1 });
    testCaseProportions.push_back(std::vector<double>{ 1.0 / 5.0, 3.0 / 5.0, 1.0 / 5.0 });
    testCaseProportions.push_back(std::vector<double>{ 0.0, 0.5, 0.5 });
    testCaseProportions.push_back(std::vector<double>{ 0.5, 0.0, 0.5 });
    testCaseProportions.push_back(std::vector<double>{ 0.5, 0.5, 0.0 });

    // Iterate through test cases
    for (size_t i = 0; i < testCaseProportions.size(); i++) {
        // 1. TEST SPLITTING
        // -----------------
        // Initialize helpers
        std::vector<SplineType> splittedSplines;
        std::vector<SplineType*> splittedSplinesPointers;
        std::vector<SplineType const*> splittedSplinesPointersConst;

        // Initialize intermediate splines
        splittedSplines.resize(testCaseProportions[i].size());
        for (size_t j = 0; j < splittedSplines.size(); j++) {
            splittedSplinesPointers.push_back(&(splittedSplines[j]));
            splittedSplinesPointersConst.push_back(&(splittedSplines[j]));
        }

        // Perform split
        const bool splitResult = SplitterType::split(spline, splittedSplinesPointers, &testCaseProportions[i], &splineResult);
        ASSERT_EQ(splitResult, true);
        ASSERT_EQ(splineResult, broccoli::curve::SplineResult::SUCCESS);

        // 2. TEST JOINING
        // ---------------
        // Initialize helpers
        SplineType joinedSpline; // Spline after splitting and rejoining

        // Perform join
        const bool joinResult = SplitterType::join(joinedSpline, splittedSplinesPointersConst, &testCaseProportions[i], &splineResult);
        ASSERT_EQ(joinResult, true);
        ASSERT_EQ(splineResult, broccoli::curve::SplineResult::SUCCESS);

        // 3. TEST RESULT
        // --------------
        double x = 0;
        const double dx = 1e-2;
        while (x <= 1.0) {
            for (unsigned int d = 0; d <= maximumDerivative; d++) {
                // Evaluate original spline
                const double originalDi = spline.evaluate(x, d);

                // Evaluate re-joined trajectory
                const double rejoinedDi = spline.evaluate(x, d);

                // Compare values
                ASSERT_LE(fabs(originalDi - rejoinedDi), tolerance);
            }

            // Update time
            x += dx;
        }
    }
}
