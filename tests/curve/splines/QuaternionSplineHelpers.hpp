/*
 * This file is part of broccoli
 * Copyright (C) 2022 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mec.ed.tum.de/am
 */

#pragma once // Load this file only once

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "../curves/QuaternionCurveHelpers.hpp"
#include "broccoli/curve/splines/QuaternionBSplineSpline.hpp"
#include "broccoli/curve/splines/QuaternionSpline.hpp"
#include "gtest/gtest.h"

//! Template function to check isValid() of QuaternionSpline
template <class SplineType, class CurveType>
inline void checkQuaternionSplineIsValidFixedSize()
{
    // Initialize helpers
    broccoli::curve::QuaternionSplineResult result;

    // Initialize container
    SplineType spline;

    // Check no segments
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_NO_SEGMENTS);

    // Check dimension mismatch
    spline.m_segments.push_back(CurveType());
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_PROPORTION_COUNT);

    // Check negative proportions
    spline.m_segmentProportions.push_back(-1);
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_PROPORTION_NEGATIVE);

    // Check wrong sum of proportions
    spline.m_segmentProportions.clear();
    spline.m_segmentProportions.push_back(0.5);
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_PROPORTION_SUM);

    // Check success
    spline.m_segmentProportions.clear();
    spline.m_segmentProportions.push_back(1);
    ASSERT_EQ(spline.isValid(&result), true);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::SUCCESS);
}

//! Template function to check isValid() of QuaternionBSplineSpline
template <unsigned int Degree>
inline void checkQuaternionSplineIsValidBSpline()
{
    // Initialize helpers
    broccoli::curve::QuaternionSplineResult result;
    const int p = Degree;
    const int k = p + 1;
    const int m = k + 3;

    // Initialize container
    broccoli::curve::QuaternionBSplineSpline<Degree, Degree + 1 + 3> spline;

    // Check no segments
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_NO_SEGMENTS);

    // Check invalid segment
    spline.m_segments.push_back(broccoli::curve::QuaternionBSplineCurve<Degree, Degree + 1 + 3>());
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SEGMENT);
    for (int i = 0; i < m; i++)
        spline.m_segments[0].m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    spline.m_segments[0].m_basis.setClampedUniformKnots(m);

    // Check dimension mismatch
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_PROPORTION_COUNT);

    // Check negative proportions
    spline.m_segmentProportions.push_back(-1);
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_PROPORTION_NEGATIVE);

    // Check wrong sum of proportions
    spline.m_segmentProportions.clear();
    spline.m_segmentProportions.push_back(0.5);
    ASSERT_EQ(spline.isValid(&result), false);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::ERROR_INVALID_SPLINE_PROPORTION_SUM);

    // Check success
    spline.m_segmentProportions.clear();
    spline.m_segmentProportions.push_back(1);
    ASSERT_EQ(spline.isValid(&result), true);
    ASSERT_EQ(result, broccoli::curve::QuaternionSplineResult::SUCCESS);
}

//! Template function to test quaternion spline operators
template <class SplineType>
inline void checkOperatorsQuaternionSpline()
{
    // Construct splines
    SplineType splineA;
    SplineType splineB; // same
    SplineType splineC; // different segments
    SplineType splineD; // differet segment proportions
    splineA.m_segments.resize(2);
    splineA.m_segmentProportions = std::vector<double>(2, 0.5);
    splineB = splineA;
    splineC.m_segments.resize(1);
    splineC.m_segmentProportions = std::vector<double>(1, 1.0);
    splineD.m_segments.resize(2);
    splineD.m_segmentProportions = std::vector<double>(1);
    splineD.m_segmentProportions[0] = 0.2;
    splineD.m_segmentProportions[0] = 0.8;

    // Equality operators
    ASSERT_TRUE(splineA == splineB);
    ASSERT_FALSE(splineA != splineB);
    ASSERT_FALSE(splineA == splineC);
    ASSERT_TRUE(splineA != splineC);
    ASSERT_FALSE(splineA == splineD);
    ASSERT_TRUE(splineA != splineD);
}

//! Template function to compare analytic and numeric derivative of quaternion splines (custom)
template <class SplineType>
inline void compareAnalyticAndNumericDerivativeCustomQuaternionSpline(const SplineType& spline, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    compareAnalyticAndNumericDerivativeGenericCustomQuaternion<SplineType>(spline, 0.1, 0.9, derivationOrder, toleratedRelativeError);
}

//! Template function to check high performance evaluation
template <class SplineType, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationQuaternionSpline(const SplineType& spline)
{
    checkHighPerformanceEvaluationGenericQuaternion<SplineType, MaximumDerivation>(spline, 0, 1);
}

//! Template function for plotting quaternion splines in 2D (using gnuplot)
template <class SplineType>
inline void plotQuaternionSpline2D(const SplineType& spline, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    plotGenericQuaternion2D<SplineType>(spline, 0, 1, stepSize, maximumDerivation, fileName);
}

//! Template function for plotting quaternion splines in 3D (using gnuplot)
template <class SplineType>
inline void plotQuaternionSpline3D(const SplineType& spline, const double& stepSize, const std::string& fileName)
{
    plotGenericQuaternion3D<SplineType>(spline, 0, 1, stepSize, fileName);
}

//! Template function to check encodeToXML() of QuaternionSpline
template <class SplineType, class CurveType>
inline void checkEncodeToXMLQuaternionSpline()
{
    // Initialize container
    SplineType spline;
    spline.m_segments.push_back(CurveType());

    // Initialize helpers
    broccoli::io::encoding::CharacterStream stream;
    broccoli::io::encoding::CharacterStreamSize streamSize = spline.encodeToXML(stream, 1, 4);
    ASSERT_GT(streamSize, 0);
    ASSERT_EQ(streamSize, stream.size());
}

//! Template function to check getSegmentBoundaries() of quaternion splines
template <class SplineType>
inline void checkGetSegmentBoundariesQuaternionSpline(const SplineType& spline)
{
    // Get boundaries
    std::vector<double> segmentBoundaries = spline.getSegmentBoundaries();
    ASSERT_EQ(segmentBoundaries.size(), spline.m_segmentProportions.size() + 1);

    // Check values
    double currentPosition = 0;
    for (size_t i = 0; i < spline.m_segmentProportions.size(); i++) {
        ASSERT_TRUE(fabs(segmentBoundaries[i] - currentPosition) < 1e-9);
        currentPosition += spline.m_segmentProportions[i];
    }
    ASSERT_TRUE(fabs(segmentBoundaries.back() - 1.0) < 1e-6);
}

//! Template function to check removeEmptySegments() function of quaternion spline
template <class SplineType>
inline void checkRemoveEmptySegmentsQuaternionSpline()
{
    SplineType spline;
    ASSERT_EQ(spline.removeEmptySegments(), 0);
    spline.m_segments.resize(13);
    for (size_t i = 0; i < 13; i++)
        spline.m_segments[i].m_controlPoints[0] = Eigen::Quaterniond(i, 0, 0, 0);
    spline.m_segmentProportions.resize(13, 0.1);
    spline.m_segmentProportions[1] = 0;
    spline.m_segmentProportions[2] = 0;
    spline.m_segmentProportions[4] = 0;
    ASSERT_EQ(spline.removeEmptySegments(), 3);
    ASSERT_EQ(spline.m_segments.size(), 10);
    ASSERT_EQ(spline.m_segmentProportions.size(), 10);
    ASSERT_EQ(spline.m_segments[0].m_controlPoints[0].w(), 0);
    ASSERT_EQ(spline.m_segments[1].m_controlPoints[0].w(), 3);
    ASSERT_EQ(spline.m_segments[2].m_controlPoints[0].w(), 5);
    ASSERT_EQ(spline.m_segments[3].m_controlPoints[0].w(), 6);
    ASSERT_EQ(spline.m_segments[4].m_controlPoints[0].w(), 7);
    ASSERT_EQ(spline.m_segments[5].m_controlPoints[0].w(), 8);
    ASSERT_EQ(spline.m_segments[6].m_controlPoints[0].w(), 9);
    ASSERT_EQ(spline.m_segments[7].m_controlPoints[0].w(), 10);
    ASSERT_EQ(spline.m_segments[8].m_controlPoints[0].w(), 11);
    ASSERT_EQ(spline.m_segments[9].m_controlPoints[0].w(), 12);
}

//! Define keyframe set "A" for quaternion interpolation
inline std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> keyFramesSetAQuaternionInterpolation()
{
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> keyFrames;
    keyFrames.reserve(7);
    keyFrames.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    keyFrames.push_back(Eigen::Quaterniond(0.866025, 0, 0.5, 0));
    keyFrames.push_back(Eigen::Quaterniond(0.612372, 0.353553, 0.353553, -0.612372));
    keyFrames.push_back(Eigen::Quaterniond(0, 0.5, 0, -0.866025));
    keyFrames.push_back(Eigen::Quaterniond(-0.612372, 0.353553, -0.353553, -0.612372));
    keyFrames.push_back(Eigen::Quaterniond(-0.866025, 0, -0.499999, 0));
    keyFrames.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    for (size_t i = 0; i < keyFrames.size(); i++)
        keyFrames[i].normalize();
    return keyFrames;
}

//! Define custom proportions for keyframe set "A" for quaternion interpolation
inline std::vector<double> keyFramesSetAQuaternionInterpolationCustomProportions()
{
    std::vector<double> proportions;
    proportions.reserve(6);
    proportions.push_back(123);
    proportions.push_back(234);
    proportions.push_back(345);
    proportions.push_back(543);
    proportions.push_back(432);
    proportions.push_back(321);
    double sum = 0.0;
    for (size_t i = 0; i < proportions.size(); i++)
        sum += proportions[i];
    for (size_t i = 0; i < proportions.size(); i++)
        proportions[i] /= sum;
    return proportions;
}

//! Define keyframe set "B" for quaternion interpolation
inline std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> keyFramesSetBQuaternionInterpolation()
{
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> keyFrames;
    keyFrames.reserve(4);
    keyFrames.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    keyFrames.push_back(Eigen::Quaterniond(0.933013, -0.25, 0.066987, -0.25));
    keyFrames.push_back(Eigen::Quaterniond(0.836516, -0.482963, -0.12941, 0.224144));
    keyFrames.push_back(Eigen::Quaterniond(0.707107, -0.707107, 0, 0));
    for (size_t i = 0; i < keyFrames.size(); i++)
        keyFrames[i].normalize();
    return keyFrames;
}

//! Define custom proportions for keyframe set "B" for quaternion interpolation
inline std::vector<double> keyFramesSetBQuaternionInterpolationCustomProportions()
{
    std::vector<double> proportions;
    proportions.reserve(3);
    proportions.push_back(123);
    proportions.push_back(321);
    proportions.push_back(234);
    double sum = 0.0;
    for (size_t i = 0; i < proportions.size(); i++)
        sum += proportions[i];
    for (size_t i = 0; i < proportions.size(); i++)
        proportions[i] /= sum;
    return proportions;
}

#endif // HAVE_EIGEN3
