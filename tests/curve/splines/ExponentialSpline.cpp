/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/splines/ExponentialSpline.hpp"
#include "SplineHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of exponential splines
TEST(ExponentialSpline, BasicUsage)
{
    // Check validity
    checkIsValidSpline<ExponentialSpline, ExponentialCurve>();

    // Check operators
    checkOperatorsSpline<ExponentialSpline>();

    // Check removeEmptySegments()
    checkRemoveEmptySegmentsSpline<ExponentialSpline>();

    // Construct
    ExponentialSpline spline;
    spline.m_segments.resize(1);
    spline.m_segments[0].m_coefficients[0] = 1.23;
    spline.m_segments[0].m_coefficients[1] = -2.34;
    spline.m_segments[0].m_coefficients[2] = 0.123;
    spline.m_segmentProportions = std::vector<double>(1, 1.0);

    // Check segment boundaries
    checkGetSegmentBoundariesSpline<decltype(spline)>(spline);

    // Check high performance evaluation
    checkHighPerformanceEvaluationSpline<decltype(spline), 2>(spline);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeCustomSpline<ExponentialSpline>(spline, 1);

    // Check encoding to XML
    checkEncodeToXMLSpline<ExponentialSpline, ExponentialCurve>();
}

//! Check construction and evaluation of exponential splines
TEST(ExponentialSpline, ConstructionEvaluation)
{
    // Initialize container
    ExponentialSpline spline;

    // Setup segments
    // f(x) = c[0] + c[1]*exp(c[2]*x)
    spline.m_segments.resize(1);
    spline.m_segments[0].m_coefficients[0] = 1; // Offset
    spline.m_segments[0].m_coefficients[1] = 2; // Scaling
    spline.m_segments[0].m_coefficients[2] = 3; // Streching

    // Setup proportions
    spline.m_segmentProportions.resize(1, 1.0);

    // Write plot file for visual debugging
    plotSpline<ExponentialSpline>(spline, 0.001, 1, "ExponentialSplineConstructionEvaluation");

    // Get coefficients
    double c0 = spline.m_segments[0].m_coefficients[0];
    double c1 = spline.m_segments[0].m_coefficients[1];
    double c2 = spline.m_segments[0].m_coefficients[2];

    // Check function values
    double tol = 1e-10;
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 0) - (c0 + c1 * exp(c2 * 0.00))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 0) - (c0 + c1 * exp(c2 * 0.25))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 0) - (c0 + c1 * exp(c2 * 0.50))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 0) - (c0 + c1 * exp(c2 * 0.75))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 0) - (c0 + c1 * exp(c2 * 1.00))) < tol);

    // Check derivatives
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 1) - (c1 * c2 * exp(c2 * 0.00))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 1) - (c1 * c2 * exp(c2 * 0.25))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 1) - (c1 * c2 * exp(c2 * 0.50))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 1) - (c1 * c2 * exp(c2 * 0.75))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 1) - (c1 * c2 * exp(c2 * 1.00))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 2) - (c1 * c2 * c2 * exp(c2 * 0.00))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 2) - (c1 * c2 * c2 * exp(c2 * 0.25))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 2) - (c1 * c2 * c2 * exp(c2 * 0.50))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 2) - (c1 * c2 * c2 * exp(c2 * 0.75))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 2) - (c1 * c2 * c2 * exp(c2 * 1.00))) < tol);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 3);
}
