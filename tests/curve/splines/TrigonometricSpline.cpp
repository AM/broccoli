/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/splines/TrigonometricSpline.hpp"
#include "SplineHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of trigonometric spline
TEST(TrigonometricSpline, BasicUsage)
{
    // Check validity
    checkIsValidSpline<TrigonometricSpline, TrigonometricCurve>();

    // Check operators
    checkOperatorsSpline<TrigonometricSpline>();

    // Check removeEmptySegments()
    checkRemoveEmptySegmentsSpline<TrigonometricSpline>();

    // Loop over all function types
    for (uint8_t i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        // Construct
        TrigonometricSpline spline;
        spline.m_segments.push_back(TrigonometricCurve());
        spline.m_segments[0].m_type = static_cast<TrigonometricCurve::Type>(i);
        spline.m_segments[0].m_coefficients[0] = 1.23;
        spline.m_segments[0].m_coefficients[1] = -2.34;
        spline.m_segments[0].m_coefficients[2] = 0.123;
        spline.m_segments[0].m_coefficients[3] = -0.234;
        spline.m_segmentProportions = std::vector<double>(1, 1.0);

        // Check segment boundaries
        checkGetSegmentBoundariesSpline<decltype(spline)>(spline);

        // Check high performance evaluation
        checkHighPerformanceEvaluationSpline<decltype(spline), 2>(spline);

        // Check numeric and analytic derivative
        if (i != static_cast<uint8_t>(TrigonometricCurve::Type::TANGENT))
            compareAnalyticAndNumericDerivativeCustomSpline<TrigonometricSpline>(spline, 1);
    }

    // Check encoding to XML
    checkEncodeToXMLSpline<TrigonometricSpline, TrigonometricCurve>();
}

//! Check construction and evaluation of sine spline
TEST(TrigonometricSpline, Sine)
{
    // Initialize container
    TrigonometricSpline spline;

    // Setup segments
    // f(x) = c[0] + c[1]*sin(c[2]*x + c[3])
    spline.m_segments.resize(1);
    spline.m_segments[0].m_type = TrigonometricCurve::Type::SINE;
    spline.m_segments[0].m_coefficients[0] = 0; // Offset
    spline.m_segments[0].m_coefficients[1] = 1; // Amplitude
    spline.m_segments[0].m_coefficients[2] = 2.0 * M_PI; // Streching
    spline.m_segments[0].m_coefficients[3] = 2.0 * M_PI; // Phase shift

    // Setup proportions
    spline.m_segmentProportions.resize(1, 1.0);

    // Write plot file for visual debugging
    plotSpline<TrigonometricSpline>(spline, 0.001, 2, "TrigonometricSplineSine");

    // Check function values
    double tol = 1e-10;
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 0) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 0) - 1.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 0) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 0) + 1.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 0) - 0.0) < tol);

    // Check derivatives
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 1) - 2.0 * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 1) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 1) + 2.0 * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 1) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 1) - 2.0 * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 2) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 2) + 4.0 * M_PI * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 2) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 2) - 4.0 * M_PI * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 2) - 0.0) < tol);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 3);
}

//! Check construction and evaluation of cosine spline
TEST(TrigonometricSpline, Cosine)
{
    // Initialize container
    TrigonometricSpline spline;

    // Setup segments
    // f(x) = c[0] + c[1]*cos(c[2]*x + c[3])
    spline.m_segments.resize(1);
    spline.m_segments[0].m_type = TrigonometricCurve::Type::COSINE;
    spline.m_segments[0].m_coefficients[0] = 0; // Offset
    spline.m_segments[0].m_coefficients[1] = 1; // Amplitude
    spline.m_segments[0].m_coefficients[2] = 2.0 * M_PI; // Streching
    spline.m_segments[0].m_coefficients[3] = 2.0 * M_PI; // Phase shift

    // Setup proportions
    spline.m_segmentProportions.resize(1, 1.0);

    // Write plot file for visual debugging
    plotSpline<TrigonometricSpline>(spline, 0.001, 2, "TrigonometricSplineCosine");

    // Check function values
    double tol = 1e-10;
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 0) - 1.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 0) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 0) + 1.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 0) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 0) - 1.0) < tol);

    // Check derivatives
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 1) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 1) + 2.0 * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 1) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 1) - 2.0 * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 1) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 2) + 4.0 * M_PI * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.25, 2) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 2) - 4.0 * M_PI * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.75, 2) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 2) + 4.0 * M_PI * M_PI) < tol);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 2);
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 3);
}

//! Check construction and evaluation of tangent spline
TEST(TrigonometricSpline, Tangent)
{
    // Initialize container
    TrigonometricSpline spline;

    // Setup segments
    // f(x) = c[0] + c[1]*tan(c[2]*x + c[3])
    spline.m_segments.resize(1);
    spline.m_segments[0].m_type = TrigonometricCurve::Type::TANGENT;
    spline.m_segments[0].m_coefficients[0] = 0; // Offset
    spline.m_segments[0].m_coefficients[1] = 1; // Amplitude
    spline.m_segments[0].m_coefficients[2] = M_PI / 4.0; // Streching
    spline.m_segments[0].m_coefficients[3] = 0; // Phase shift

    // Setup proportions
    spline.m_segmentProportions.resize(1, 1.0);

    // Write plot file for visual debugging
    plotSpline<TrigonometricSpline>(spline, 0.001, 1, "TrigonometricSplineTangent");

    // Check function values
    double tol = 1e-10;
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 0) - 0.0) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 0) - tan(M_PI / 4.0 * 0.5)) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 0) - tan(M_PI / 4.0 * 1.0)) < tol);

    // Check derivatives
    ASSERT_TRUE(fabs(spline.evaluate(0.00, 1) - 1.0 / 4.0 * M_PI) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(0.50, 1) - M_PI / 4.0 * (1.0 + tan(M_PI / 8.0) * tan(M_PI / 8.0))) < tol);
    ASSERT_TRUE(fabs(spline.evaluate(1.00, 1) - 1.0 / 2.0 * M_PI) < tol);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomSpline<decltype(spline)>(spline, 1);
}

//! Check construction and evaluation of trigonometric splines (combination of sine/cosine/tangent)
TEST(TrigonometricSpline, TrigonometricSplineCombination)
{
    // Initialize container
    TrigonometricSpline spline;

    // Setup segments
    // f(x) = c[0] + c[1]*func(c[2]*x + c[3])
    spline.m_segments.resize(3);
    spline.m_segments[0].m_type = TrigonometricCurve::Type::SINE;
    spline.m_segments[0].m_coefficients[0] = 1; // Offset
    spline.m_segments[0].m_coefficients[1] = 2; // Amplitude
    spline.m_segments[0].m_coefficients[2] = 2; // Streching
    spline.m_segments[0].m_coefficients[3] = M_PI_2; // Phase shift
    spline.m_segments[1].m_type = TrigonometricCurve::Type::COSINE;
    spline.m_segments[1].m_coefficients[0] = 1; // Offset
    spline.m_segments[1].m_coefficients[1] = 2; // Amplitude
    spline.m_segments[1].m_coefficients[2] = 2; // Streching
    spline.m_segments[1].m_coefficients[3] = M_PI_2; // Phase shift
    spline.m_segments[2].m_type = TrigonometricCurve::Type::TANGENT;
    spline.m_segments[2].m_coefficients[0] = 1; // Offset
    spline.m_segments[2].m_coefficients[1] = 2; // Amplitude
    spline.m_segments[2].m_coefficients[2] = 2; // Streching
    spline.m_segments[2].m_coefficients[3] = 0; // Phase shift

    // Setup proportions
    spline.m_segmentProportions.resize(3, 1.0 / 3.0);

    // Write plot file for visual debugging
    plotSpline<TrigonometricSpline>(spline, 0.001, 0, "TrigonometricSplineCombination");
}
