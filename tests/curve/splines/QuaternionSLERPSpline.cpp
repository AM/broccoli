/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/splines/QuaternionSLERPSpline.hpp"
#include "QuaternionSplineHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of quaternion SLERP splines
TEST(QuaternionSLERPSpline, BasicUsage)
{
    // Check validity
    checkQuaternionSplineIsValidFixedSize<QuaternionSLERPSpline, QuaternionSLERPCurve>();

    // Check operators
    checkOperatorsQuaternionSpline<QuaternionSLERPSpline>();

    // Check removeEmptySegments()
    checkRemoveEmptySegmentsQuaternionSpline<QuaternionSLERPSpline>();

    // Construct
    QuaternionSLERPSpline spline;
    spline.m_segments.resize(2);
    spline.m_segments[0].m_controlPoints[0] = Eigen::Quaterniond(1, 0, 0, 0);
    spline.m_segments[0].m_controlPoints[1] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[1].m_controlPoints[0] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[1].m_controlPoints[1] = Eigen::Quaterniond(1, 0, 0, 0);
    for (size_t i = 0; i < spline.m_segments.size(); i++)
        for (size_t j = 0; j < spline.m_segments[i].m_controlPoints.size(); j++)
            spline.m_segments[i].m_controlPoints[j].normalize();
    spline.m_segmentProportions = std::vector<double>(2);
    spline.m_segmentProportions[0] = 0.2;
    spline.m_segmentProportions[1] = 0.8;

    // Check segment boundaries
    checkGetSegmentBoundariesQuaternionSpline<decltype(spline)>(spline);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionSpline<decltype(spline), 2>(spline);

    // Check encoding to XML
    checkEncodeToXMLQuaternionSpline<QuaternionSLERPSpline, QuaternionSLERPCurve>();
}

//! Check interpolation with SLERP
TEST(QuaternionSLERPSpline, InterpolatePiecewise)
{
    // Initialize helpers
    QuaternionSplineResult result;

    // Create control point quaternions
    auto keyFrames = keyFramesSetAQuaternionInterpolation();

    // Setup spline
    QuaternionSLERPSpline spline;
    bool returnValue = spline.interpolate(keyFrames, nullptr /* empty -> use uniform proportions */, QuaternionSplineInterpolationMethod::SLERP_PIECEWISE, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Write plot files for visual debugging
    plotQuaternionSpline2D<decltype(spline)>(spline, 0.001, 2, "QuaternionSLERPSpline2D");
    plotQuaternionSpline3D<decltype(spline)>(spline, 0.001, "QuaternionSLERPSpline3D");
}

#endif // HAVE_EIGEN3
