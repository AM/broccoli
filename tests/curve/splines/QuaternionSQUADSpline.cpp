/*
 * This file is part of broccoli
 * Copyright (C) 2022 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mec.ed.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/splines/QuaternionSQUADSpline.hpp"
#include "QuaternionSplineHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of quaternion SQUAD splines
TEST(QuaternionSQUADSpline, BasicUsage)
{
    // Check validity
    checkQuaternionSplineIsValidFixedSize<QuaternionSQUADSpline, QuaternionSQUADCurve>();

    // Check operators
    checkOperatorsQuaternionSpline<QuaternionSQUADSpline>();

    // Check removeEmptySegments()
    checkRemoveEmptySegmentsQuaternionSpline<QuaternionSQUADSpline>();

    // Construct
    QuaternionSQUADSpline spline;
    spline.m_segments.resize(2);
    spline.m_segments[0].m_controlPoints[0] = Eigen::Quaterniond(1, 0, 0, 0);
    spline.m_segments[0].m_controlPoints[1] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[0].m_controlPoints[2] = Eigen::Quaterniond(0, 1, 1, 0);
    spline.m_segments[0].m_controlPoints[3] = Eigen::Quaterniond(0, 0, 1, 1);
    spline.m_segments[1].m_controlPoints[0] = Eigen::Quaterniond(0, 0, 1, 1);
    spline.m_segments[1].m_controlPoints[1] = Eigen::Quaterniond(0, 1, 1, 0);
    spline.m_segments[1].m_controlPoints[2] = Eigen::Quaterniond(1, 1, 0, 0);
    spline.m_segments[1].m_controlPoints[3] = Eigen::Quaterniond(1, 0, 0, 0);
    for (size_t i = 0; i < spline.m_segments.size(); i++)
        for (size_t j = 0; j < spline.m_segments[i].m_controlPoints.size(); j++)
            spline.m_segments[i].m_controlPoints[j].normalize();
    spline.m_segmentProportions = std::vector<double>(2);
    spline.m_segmentProportions[0] = 0.2;
    spline.m_segmentProportions[1] = 0.8;

    // Check segment boundaries
    checkGetSegmentBoundariesQuaternionSpline<decltype(spline)>(spline);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionSpline<decltype(spline), 2>(spline);

    // Check encoding to XML
    checkEncodeToXMLQuaternionSpline<QuaternionSQUADSpline, QuaternionSQUADCurve>();
}

//! Check interpolation with SQUAD
TEST(QuaternionSQUADSpline, InterpolatePiecewise)
{
    // Initialize helpers
    QuaternionSplineResult result;

    // Create control point quaternions
    auto keyFrames = keyFramesSetBQuaternionInterpolation();

    // Setup spline
    QuaternionSQUADSpline spline;
    bool returnValue = spline.interpolate(keyFrames, nullptr /* empty -> use uniform proportions */, QuaternionSplineInterpolationMethod::SQUAD_PIECEWISE, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Write plot files for visual debugging
    plotQuaternionSpline2D<decltype(spline)>(spline, 0.001, 2, "QuaternionSQUADSplinePiecewise2D");
    plotQuaternionSpline3D<decltype(spline)>(spline, 0.001, "QuaternionSQUADSplinePiecewise3D");
}

//! Check interpolation with SQUAD
TEST(QuaternionSQUADSpline, InterpolateSmooth)
{
    // Initialize helpers
    QuaternionSplineResult result;

    // Create control point quaternions
    auto keyFrames = keyFramesSetAQuaternionInterpolation();
    auto customProportions = keyFramesSetAQuaternionInterpolationCustomProportions();

    // Setup spline (uniform proportions)
    QuaternionSQUADSpline splineUniform;
    const bool returnValueUniform = splineUniform.interpolate(keyFrames, nullptr /* empty -> use uniform proportions */, QuaternionSplineInterpolationMethod::SQUAD_SMOOTH, &result);
    ASSERT_EQ(returnValueUniform, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Setup spline (custom proportions)
    QuaternionSQUADSpline splineCustom;
    const bool returnValueCustom = splineCustom.interpolate(keyFrames, &customProportions, QuaternionSplineInterpolationMethod::SQUAD_SMOOTH, &result);
    ASSERT_EQ(returnValueCustom, true);
    ASSERT_EQ(result, QuaternionSplineResult::SUCCESS);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeCustomQuaternionSpline<decltype(splineUniform)>(splineUniform, 1);
    compareAnalyticAndNumericDerivativeCustomQuaternionSpline<decltype(splineCustom)>(splineCustom, 1);

    // Write plot files for visual debugging
    plotQuaternionSpline2D<decltype(splineUniform)>(splineUniform, 0.001, 2, "QuaternionSQUADSplineSmooth2DUniform");
    plotQuaternionSpline3D<decltype(splineUniform)>(splineUniform, 0.001, "QuaternionSQUADSplineSmooth3DUniform");
    plotQuaternionSpline2D<decltype(splineCustom)>(splineCustom, 0.001, 2, "QuaternionSQUADSplineSmooth2DCustom");
    plotQuaternionSpline3D<decltype(splineCustom)>(splineCustom, 0.001, "QuaternionSQUADSplineSmooth3DCustom");
}

#endif // HAVE_EIGEN3
