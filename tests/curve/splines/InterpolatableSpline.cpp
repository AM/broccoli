/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/splines/InterpolatableSpline.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test string representation of the given spline interpolation method
TEST(InterpolatableSpline, SplineInterpolationMethodToString)
{
    uint8_t i;
    SplineInterpolationMethod testInterpolationMethod;
    for (i = 0; i < static_cast<uint8_t>(SplineInterpolationMethod::SPLINEINTERPOLATIONMETHOD_COUNT); i++) {
        testInterpolationMethod = static_cast<SplineInterpolationMethod>(i);
        ASSERT_GT(splineInterpolationMethodString(testInterpolationMethod).size(), 0);
        ASSERT_TRUE(splineInterpolationMethodString(testInterpolationMethod) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(SplineInterpolationMethod::SPLINEINTERPOLATIONMETHOD_COUNT);
    testInterpolationMethod = static_cast<SplineInterpolationMethod>(i);
    ASSERT_TRUE(splineInterpolationMethodString(testInterpolationMethod) == "UNKNOWN");
#endif
}
