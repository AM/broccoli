/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/curves/BSplineBasis.hpp"
#include "broccoli/core/Time.hpp"
#include "gtest/gtest.h"
#include <fstream>
#include <iostream>
#include <string>

using namespace broccoli;
using namespace curve;

//! Template function to compare analytic and numeric derivative
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
static void compareAnalyticAndNumericDerivativeBSplineBasis(const BSplineBasis<Degree, MaximumBSplineCount>& basis, const unsigned int& derivationOrder)
{
    // Initialize helpers
    const double dx = 0.0001; // Stepsize for evaluating numeric derivative
    const double xmin = basis.m_knotSequence[0] + 2.0 * dx;
    const double xmax = basis.m_knotSequence[basis.m_knotSequence.size() - 1] - 2.0 * dx;
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize + 1);
    static const double toleratedRelativeError = 1e-3;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + i * stepsize;

        // Skip site close to knots (probable discontinuities)
        bool closeToKnot = false;
        for (size_t i = 0; i < basis.m_knotSequence.size(); i++) {
            if (fabs(x - basis.m_knotSequence[i]) <= dx) {
                closeToKnot = true;
                break;
            }
        }
        if (closeToKnot == false) {
            // Compute analytic derivative
            const auto analyticDerivativeArray = basis.evaluate(x, derivationOrder).m_B;
            Eigen::Matrix<double, Eigen::Dynamic, Degree + 1> analyticDerivative;
            analyticDerivative.resize(analyticDerivativeArray.size(), Eigen::NoChange);
            for (size_t r = 0; r < analyticDerivativeArray.size(); r++)
                for (size_t c = 0; c < analyticDerivativeArray[r].size(); c++)
                    analyticDerivative(r, c) = analyticDerivativeArray[r][c];

            // Compute numeric derivative dq/dt
            auto leftSideArray = basis.evaluate(x - dx, derivationOrder - 1).m_B;
            Eigen::Matrix<double, Eigen::Dynamic, Degree + 1> leftSide;
            leftSide.resize(leftSideArray.size(), Eigen::NoChange);
            for (size_t r = 0; r < leftSideArray.size(); r++)
                for (size_t c = 0; c < leftSideArray[r].size(); c++)
                    leftSide(r, c) = leftSideArray[r][c];
            auto rightSideArray = basis.evaluate(x + dx, derivationOrder - 1).m_B;
            Eigen::Matrix<double, Eigen::Dynamic, Degree + 1> rightSide;
            rightSide.resize(rightSideArray.size(), Eigen::NoChange);
            for (size_t r = 0; r < rightSideArray.size(); r++)
                for (size_t c = 0; c < rightSideArray[r].size(); c++)
                    rightSide(r, c) = rightSideArray[r][c];
            const Eigen::Matrix<double, Eigen::Dynamic, Degree + 1> numericDerivative = (rightSide - leftSide) / (2 * dx);

            // Compute error
            const auto difference = analyticDerivative - numericDerivative;
            const double error = difference.norm();
            const double magnitude = fabs(analyticDerivative.norm());
            double relativeError = error;
            if (magnitude > 1e-9)
                relativeError = error / magnitude;

            // Check error
            if (relativeError > toleratedRelativeError) {
                std::cout << "Analytic and numeric derivative differ at position x=" << x;
                std::cout << " for derivation order d=" << derivationOrder;
                std::cout << " (absolute error: " << error;
                std::cout << ") (relative error : " << relativeError << ")\n";
                ASSERT_LT(relativeError, toleratedRelativeError);
            }
        }
    }
}

//! Template function to check high performance evaluation
template <unsigned int Degree, unsigned int MaximumDerivation, unsigned int MaximumBSplineCount = Degree + 1>
void checkHighPerformanceEvaluationBSplineBasis(const BSplineBasis<Degree, MaximumBSplineCount>& basis)
{
    // Initialize helpers
    const double xmin = basis.m_knotSequence[0];
    const double xmax = basis.m_knotSequence[basis.m_knotSequence.size() - 1] - 1e-3;
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize + 1);
    static const double toleratedRelativeError = 1e-9;
    broccoli::core::Time normalEvaluationTime = 0;
    broccoli::core::Time highPerformanceEvaluationTime = 0;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + i * stepsize;

        // Perform "normal" evaluation
        std::array<BSplineBasisSample<Degree, MaximumBSplineCount>, MaximumDerivation + 1> normalDi;
        broccoli::core::Time normalEvaluationStartTime = broccoli::core::Time::currentTime();
        for (unsigned int d = 0; d <= MaximumDerivation; d++)
            normalDi[d] = basis.evaluate(x, d);
        normalEvaluationTime += broccoli::core::Time::currentTime() - normalEvaluationStartTime;

        // Perform "high-performance" evaluation
        broccoli::core::Time highPerformanceEvaluationStartTime = broccoli::core::Time::currentTime();
        const auto highPerformanceDi = basis.template evaluateD0ToDN<MaximumDerivation>(x);
        highPerformanceEvaluationTime += broccoli::core::Time::currentTime() - highPerformanceEvaluationStartTime;

        // Check value and derivatives
        for (unsigned int d = 0; d <= MaximumDerivation; d++) {
            // Check X
            Eigen::Matrix<double, 2, Degree + 1> normalX;
            for (size_t r = 0; r < normalDi[d].m_X.size(); r++)
                for (size_t c = 0; c < normalDi[d].m_X[r].size(); c++)
                    normalX(r, c) = normalDi[d].m_X[r][c];
            Eigen::Matrix<double, 2, Degree + 1> highPerformanceX;
            for (size_t r = 0; r < highPerformanceDi[d].m_X.size(); r++)
                for (size_t c = 0; c < highPerformanceDi[d].m_X[r].size(); c++)
                    highPerformanceX(r, c) = highPerformanceDi[d].m_X[r][c];
            const double errorX = (normalX - highPerformanceX).norm();
            if (errorX > 0) {
                std::cout << "High performance evaluation (evaluateD0ToDN()) differs in m_X at position x=" << x;
                std::cout << " for derivation order d=" << d;
                std::cout << " (error : " << errorX << ")\n";
                ASSERT_EQ(errorX, 0);
            }

            // Check B
            Eigen::Matrix<double, Eigen::Dynamic, Degree + 1> normalB;
            normalB.resize(normalDi[d].m_B.size(), Eigen::NoChange);
            for (size_t r = 0; r < normalDi[d].m_B.size(); r++)
                for (size_t c = 0; c < normalDi[d].m_B[r].size(); c++)
                    normalB(r, c) = normalDi[d].m_B[r][c];
            Eigen::Matrix<double, Eigen::Dynamic, Degree + 1> highPerformanceB;
            highPerformanceB.resize(highPerformanceDi[d].m_B.size(), Eigen::NoChange);
            for (size_t r = 0; r < highPerformanceDi[d].m_B.size(); r++)
                for (size_t c = 0; c < highPerformanceDi[d].m_B[r].size(); c++)
                    highPerformanceB(r, c) = highPerformanceDi[d].m_B[r][c];
            const double errorB = (normalB - highPerformanceB).norm();
            const double magnitudeB = fabs(normalB.norm());
            double relativeErrorB = errorB;
            if (magnitudeB > 1e-9)
                relativeErrorB = errorB / magnitudeB;
            if (relativeErrorB > toleratedRelativeError) {
                std::cout << "High performance evaluation (evaluateD0ToDN()) differs in m_B at position x=" << x;
                std::cout << " for derivation order d=" << d;
                std::cout << " (absolute error: " << errorB;
                std::cout << ") (relative error : " << relativeErrorB << ")\n";
                ASSERT_LT(relativeErrorB, toleratedRelativeError);
            }
        }
    }

    // Print time measurements to console
    const double speedImprovements = normalEvaluationTime.toDouble() / highPerformanceEvaluationTime.toDouble();
    std::cout << "High performance evaluation (evaluateD0ToDN()) with N=" << MaximumDerivation << " is " << speedImprovements << " times faster than normal evaluation (" << MaximumDerivation + 1 << "x evaluate())." << std::endl;
}

//! Checks construction of B-spline of certain degree
template <unsigned int Degree>
bool checkConstructionBSplineBasis()
{
    // Default constructor
    BSplineBasis<Degree> basisA;
    if (basisA.m_knotSequence.size() != 0)
        return false;

    // Specialized constructor (clamped uniform mapping)
    BSplineBasis<Degree, Degree + 4> basisB(Degree + 4);
    if (basisB.m_knotSequence.size() == 0)
        return false;
    if (basisB.m_knotSequence.size() != 2 * (Degree + 1) + 3)
        return false;

    // Specialized constructor (zero-knots)
    BSplineBasis<Degree> basisC(Degree);
    if (Degree > 0 && basisC.m_knotSequence.size() != 2 * Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check construction of B-spline basis
TEST(BSplineBasis, Construction)
{
    ASSERT_TRUE(checkConstructionBSplineBasis<0>());
    ASSERT_TRUE(checkConstructionBSplineBasis<1>());
    ASSERT_TRUE(checkConstructionBSplineBasis<2>());
    ASSERT_TRUE(checkConstructionBSplineBasis<3>());
}

//! Checks operators
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkOperatorsBSplineBasis()
{
    // Check (in-)equality
    BSplineBasis<Degree, MaximumBSplineCount> basisA(Degree + 1);
    BSplineBasis<Degree, MaximumBSplineCount> basisB(Degree + 1); // Same
#ifdef NDEBUG
    BSplineBasis<Degree, MaximumBSplineCount> basisC(Degree + 2); // Different dimension of knot sequence
#endif // NDEBUG
    BSplineBasis<Degree, MaximumBSplineCount> basisD(Degree + 1); // Different values of knot sequence
    basisD.m_knotSequence.resize(basisD.m_knotSequence.size());
    basisD.m_knotSequence.fill(1.23);

    // Check equality
    if (!(basisA == basisB) || (basisA != basisB))
        return false;

#ifdef NDEBUG
    // Check different dimension
    if ((basisA == basisC) || !(basisA != basisC))
        return false;
#endif // NDEBUG

    // Check different values
    if ((basisA == basisD) || !(basisA != basisD))
        return false;

    // Otherwise: success!
    return true;
}

//! Check operators of B-spline basis
TEST(BSplineBasis, Operators)
{
    ASSERT_TRUE(checkOperatorsBSplineBasis<0>());
    ASSERT_TRUE(checkOperatorsBSplineBasis<1>());
    ASSERT_TRUE(checkOperatorsBSplineBasis<2>());
    ASSERT_TRUE(checkOperatorsBSplineBasis<3>());
}

//! Checks getters of B-spline of certain degree
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkGettersBSplineBasis()
{
    BSplineBasis<Degree, MaximumBSplineCount> basis;
    if (basis.degree() != Degree)
        return false;
    if (basis.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of B-spline basis
TEST(BSplineBasis, Getters)
{
    ASSERT_TRUE(checkGettersBSplineBasis<0>());
    ASSERT_TRUE(checkGettersBSplineBasis<1>());
    ASSERT_TRUE(checkGettersBSplineBasis<2>());
    ASSERT_TRUE(checkGettersBSplineBasis<3>());
}

//! Checks isValid() of B-spline of certain degree
template <unsigned int Degree>
bool checkIsValidBSplineBasis()
{
    // Valid construction
    BSplineBasis<Degree> basisA(1);
    if (basisA.isValid() == false)
        return false;

    // Invalid: decreasing knot sequence
    auto basisB = BSplineBasis<Degree, Degree + 4>(Degree + 4);
    if (basisB.isValid() == false)
        return false;
    basisB.m_knotSequence[0] = 1;
    basisB.m_knotSequence[basisB.m_knotSequence.size() - 1] = -1;
    if (basisB.isValid() == true)
        return false;

    // Invalid: uninitialized knot sequence
    BSplineBasis<Degree> basisC;
    if (basisC.isValid() == true)
        return false;

    // Otherwise: success!
    return true;
}

//! Check isValid() of B-spline basis
TEST(BSplineBasis, IsValid)
{
    ASSERT_TRUE(checkIsValidBSplineBasis<0>());
    ASSERT_TRUE(checkIsValidBSplineBasis<1>());
    ASSERT_TRUE(checkIsValidBSplineBasis<2>());
    ASSERT_TRUE(checkIsValidBSplineBasis<3>());
}

//! Checks setClampedUniformKnots() of B-spline of certain degree
template <unsigned int Degree>
bool checkSetClampedUniformKnotsBSplineBasis()
{
    // Several test cases: various counts of "interior" knots
    for (int interiorKnots = 0; interiorKnots <= 3; interiorKnots++) {
        // Setup basis
        BSplineBasis<Degree, (Degree + 1) + 3> basis;
        basis.setClampedUniformKnots((Degree + 1) + interiorKnots);
        if (basis.m_knotSequence.size() != 2 * (Degree + 1) + interiorKnots)
            return false;

        // Check first k and last k elements
        for (unsigned int i = 0; i < basis.order(); i++) {
            if (basis.m_knotSequence[i] != 0.0)
                return false;
            if (basis.m_knotSequence[basis.m_knotSequence.size() - 1 - i] != 1.0)
                return false;
        }

        // Check interior knots
        double interiorKnotStepSize = 1.0 / ((double)(interiorKnots + 1));
        for (int i = 0; i < interiorKnots; i++)
            if (basis.m_knotSequence[basis.order() + i] != basis.m_knotSequence[basis.order() + i - 1] + interiorKnotStepSize)
                return false;
    }

    // Otherwise: success!
    return true;
}

//! Check setClampedUniformKnots() of B-spline basis
TEST(BSplineBasis, SetClampedUniformKnots)
{
    ASSERT_TRUE(checkSetClampedUniformKnotsBSplineBasis<0>());
    ASSERT_TRUE(checkSetClampedUniformKnotsBSplineBasis<1>());
    ASSERT_TRUE(checkSetClampedUniformKnotsBSplineBasis<2>());
    ASSERT_TRUE(checkSetClampedUniformKnotsBSplineBasis<3>());
}

//! Checks encodeToXML() of B-spline of certain degree
template <unsigned int Degree>
bool checkEncodeToXMLBSplineBasis()
{
    BSplineBasis<Degree> basis;
    io::encoding::CharacterStream stream;
    io::encoding::CharacterStreamSize streamSize = basis.encodeToXML(stream, 1, 4);
    if (streamSize == 0)
        return false;
    if (streamSize != stream.size())
        return false;

    // Otherwise: success!
    return true;
}

//! Check encodeToXML() of B-spline basis
TEST(BSplineBasis, EncodeToXML)
{
    ASSERT_TRUE(checkEncodeToXMLBSplineBasis<0>());
    ASSERT_TRUE(checkEncodeToXMLBSplineBasis<1>());
    ASSERT_TRUE(checkEncodeToXMLBSplineBasis<2>());
    ASSERT_TRUE(checkEncodeToXMLBSplineBasis<3>());
}

//! Checks evaluate() of B-spline of certain degree
template <unsigned int Degree>
bool checkEvaluateBSplineBasis()
{
    // Several test cases: various counts of "interior" knots
    for (int interiorKnots = 0; interiorKnots <= 3; interiorKnots++) {
        // Initialize helpers
        int k = Degree + 1;
        int m = k + interiorKnots;
        int n = m + k;

        // Create test basis
        auto basis = BSplineBasis<Degree, Degree + 1 + 3>(m);

        // Evaluate within [t_0, t_{m}[ (pass through all m B-Splines)
        for (int s = 0; s <= 99; s++) {
            // Create sample
            double x = basis.m_knotSequence[0] + 0.01 * s * (basis.m_knotSequence[m] - basis.m_knotSequence[0]);
            auto sample = basis.evaluate(x, 0);

            // Check positivity and support for all B_ij (see deBoor 2001, p.91)
            for (int j = 1; j <= k; j++) {
                for (int i = 0; i < m; i++) {
                    double t_i = basis.m_knotSequence[i]; // t_i
                    double t_ipj = basis.m_knotSequence[i + j]; // t_{i+j}

                    // Positivity
                    if (!(sample.m_B[i][j - 1] >= 0))
                        return false;

                    // Support
                    if (t_i == t_ipj) {
                        // Special case of coninciding knots
                        if (!(sample.m_B[i][j - 1] == 0))
                            return false;
                    } else {
                        // B_ij vanishes outside the interval [t_i, ..., t_{i+j}[
                        if (!(t_i <= x && x < t_ipj)) {
                            if (sample.m_B[i][j - 1] != 0)
                                return false;
                        }

                        // B_ij is positive inside the interval ]t_i, ..., t_{i+j}[
                        if (t_i < x && x < t_ipj) {
                            if (sample.m_B[i][j - 1] <= 0)
                                return false;
                        }
                    }
                }
            }

            // Check partition of unity in [t_j, ... , t_{n-j-1}[
            for (int j = 1; j <= k; j++) {
                if (basis.m_knotSequence[j] <= x && x < basis.m_knotSequence[n - j - 1]) {
                    // Compute sum
                    double sum = 0;
                    for (int i = 0; i < m; i++)
                        sum += sample.m_B[i][j - 1];
                    if (fabs(sum - 1.0) > 1e-9)
                        return false;
                }
            }
        }
    }

    // Otherwise: success!
    return true;
}

//! Check evaluate() of B-spline basis
TEST(BSplineBasis, Evaluate)
{
    ASSERT_TRUE(checkEvaluateBSplineBasis<0>());
    ASSERT_TRUE(checkEvaluateBSplineBasis<1>());
    ASSERT_TRUE(checkEvaluateBSplineBasis<2>());
    ASSERT_TRUE(checkEvaluateBSplineBasis<3>());
}

//! Plots the passed B-Spline basis
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
void plotBSplineBasisBSplineBasis(const BSplineBasis<Degree, MaximumBSplineCount>& basis, const unsigned int& maximumDerivation, const int& sampleCount, const std::string& fileName)
{
    // Initialize helpers
    const int n = basis.m_knotSequence.size();
    const int k = Degree + 1;
    const int m = n - k;

    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "plot '" << fileName << ".dat' using 1:2 with lines title \"Sum of D^0 B_{i," << k << "}\"\n";
    for (unsigned int d = 0; d <= maximumDerivation; d++)
        for (int i = 0; i < m; i++)
            gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (2 + d * m + i + 1) << " with lines title \"D^" << d << "B_{" << i << "," << k << "}\"\n";
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");

    // Evaluate within [t_0, t_{n-1}]
    for (int s = 0; s <= sampleCount; s++) {
        // Create samples
        double x = basis.m_knotSequence[0] + 1.0 / sampleCount * s * (basis.m_knotSequence[n - 1] - basis.m_knotSequence[0]);
        dataFile << x;
        for (unsigned int d = 0; d <= maximumDerivation; d++) {
            auto Dd_B_ik = basis.evaluate(x, d);
            if (d == 0) {
                // Evaluate sum
                double sum = 0;
                for (int i = 0; i < m; i++)
                    sum += Dd_B_ik.m_B[i][k - 1];
                dataFile << " " << sum;
            }
            for (int i = 0; i < m; i++) {
                dataFile << " " << Dd_B_ik.m_B[i][k - 1];
            }
        }

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Check through the example given in deBoor 2001 p 91f.
TEST(BSplineBasis, CheckExampleOfDeBoor)
{
    // Create parabolic B-spline basis with 5 B-splines according to example of deBoor
    static const int p = 2;
    const int k = p + 1;
    const int m = 5;
    auto basis = BSplineBasis<p, m>(m);
    basis.m_knotSequence[0] = 0;
    basis.m_knotSequence[1] = 1;
    basis.m_knotSequence[2] = 1;
    basis.m_knotSequence[3] = 3;
    basis.m_knotSequence[4] = 4;
    basis.m_knotSequence[5] = 6;
    basis.m_knotSequence[6] = 6;
    basis.m_knotSequence[7] = 6;

    // Plot
    plotBSplineBasisBSplineBasis<p>(basis, 3, 600, "BSplineBasisExampleDeBoor");

    // Check values of B_ik at x = 0
    auto sample = basis.evaluate(0, 0);
    ASSERT_TRUE(fabs(sample.m_B[0][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[1][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[2][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[3][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[4][k - 1] - 0.0) < 1e-9);

    // Check values of B_ik at x = 1
    sample = basis.evaluate(1, 0);
    ASSERT_TRUE(fabs(sample.m_B[0][k - 1] - 1.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[1][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[2][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[3][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[4][k - 1] - 0.0) < 1e-9);

    // Check values of B_ik at x = 2
    sample = basis.evaluate(2, 0);
    ASSERT_TRUE(fabs(sample.m_B[0][k - 1] - 1.0 / 4.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[1][k - 1] - 7.0 / 12.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[2][k - 1] - 1.0 / 6.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[3][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[4][k - 1] - 0.0) < 1e-9);

    // Check values of B_ik at x = 3
    sample = basis.evaluate(3, 0);
    ASSERT_TRUE(fabs(sample.m_B[0][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[1][k - 1] - 1.0 / 3.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[2][k - 1] - 2.0 / 3.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[3][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[4][k - 1] - 0.0) < 1e-9);

    // Check values of B_ik at x = 4
    sample = basis.evaluate(4, 0);
    ASSERT_TRUE(fabs(sample.m_B[0][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[1][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[2][k - 1] - 2.0 / 3.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[3][k - 1] - 1.0 / 3.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[4][k - 1] - 0.0) < 1e-9);

    // Check values of B_ik at x = 5
    sample = basis.evaluate(5, 0);
    ASSERT_TRUE(fabs(sample.m_B[0][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[1][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[2][k - 1] - 1.0 / 6.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[3][k - 1] - 7.0 / 12.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[4][k - 1] - 1.0 / 4.0) < 1e-9);

    // Check values of B_ik at x = 6
    sample = basis.evaluate(6, 0);
    ASSERT_TRUE(fabs(sample.m_B[0][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[1][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[2][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[3][k - 1] - 0.0) < 1e-9);
    ASSERT_TRUE(fabs(sample.m_B[4][k - 1] - 0.0) < 1e-9);

    // Check analytic and numeric derivatives
    compareAnalyticAndNumericDerivativeBSplineBasis<p>(basis, 1);
    compareAnalyticAndNumericDerivativeBSplineBasis<p>(basis, 2);
    compareAnalyticAndNumericDerivativeBSplineBasis<p>(basis, 3);

    // Check high-performance evaluation
    checkHighPerformanceEvaluationBSplineBasis<p, 2>(basis);
}

#endif // HAVE_EIGEN3
