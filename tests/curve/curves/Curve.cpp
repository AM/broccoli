/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/curves/Curve.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test string representation of the given result type for curve algorithms
TEST(Curve, CurveResultToString)
{
    uint8_t i;
    for (i = 0; i < static_cast<uint8_t>(CurveResult::CURVERESULT_COUNT); i++) {
        ASSERT_GT(curveResultString(static_cast<CurveResult>(i)).size(), 0);
        if (i == 0)
            ASSERT_TRUE(curveResultString(static_cast<CurveResult>(i)) == "UNKNOWN");
        else
            ASSERT_TRUE(curveResultString(static_cast<CurveResult>(i)) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(CurveResult::CURVERESULT_COUNT);
    ASSERT_TRUE(curveResultString(static_cast<CurveResult>(i)) == "UNKNOWN");
#endif // NDEBUG
}

//! Test string representation of the given function type
TEST(Curve, FunctionTypeToString)
{
    uint8_t i;
    for (i = 0; i < static_cast<uint8_t>(Curve::FunctionType::FUNCTIONTYPE_COUNT); i++) {
        ASSERT_GT(Curve::functionTypeString(static_cast<Curve::FunctionType>(i)).size(), 0);
        ASSERT_TRUE(Curve::functionTypeString(static_cast<Curve::FunctionType>(i)) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(Curve::FunctionType::FUNCTIONTYPE_COUNT);
    ASSERT_TRUE(Curve::functionTypeString(static_cast<Curve::FunctionType>(i)) == "UNKNOWN");
#endif // NDEBUG
}
