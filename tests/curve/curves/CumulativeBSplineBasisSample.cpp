/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/curves/CumulativeBSplineBasisSample.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Checks construction of B-spline sample of certain degree
template <unsigned int Degree>
bool checkConstructionCumulativeBSplineBasisSample()
{
    // Default constructor
    CumulativeBSplineBasisSample<Degree, Degree + 1> sampleA;
    CumulativeBSplineBasisSample<Degree, Degree + 1 + 3> sampleB(3);
    if (sampleB.m_B.size() != 3)
        return false;
    for (unsigned int i = 0; i < 3; i++) {
        if (sampleA.m_f[i] != 0)
            return false;
        if (sampleB.m_f[i] != 0)
            return false;
    }

    // Specialized constructor
    typename BSplineBasisSample<Degree>::KnotSequenceType knotSequence;
    knotSequence.resize((Degree + 1) + 1);
    knotSequence.fill(0);
    for (size_t i = 1; i < knotSequence.size(); i++)
        knotSequence[i] = 1;
    BSplineBasisSample<Degree> sampleC(knotSequence, 0, 0);
    CumulativeBSplineBasisSample<Degree> sampleD;
    sampleD.evaluate(sampleC);
    if (sampleD.m_B.size() == 0)
        return false;

    // Otherwise: success!
    return true;
}

//! Check construction of cumulative B-spline basis sample
TEST(CumulativeBSplineBasisSample, Construction)
{
    ASSERT_TRUE(checkConstructionCumulativeBSplineBasisSample<0>());
    ASSERT_TRUE(checkConstructionCumulativeBSplineBasisSample<1>());
    ASSERT_TRUE(checkConstructionCumulativeBSplineBasisSample<2>());
    ASSERT_TRUE(checkConstructionCumulativeBSplineBasisSample<3>());
}

//! Checks operators
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkOperatorsCumulativeBSplineBasisSample()
{
    // Check (in-)equality
    CumulativeBSplineBasisSample<Degree, MaximumBSplineCount> sampleA(1);
    CumulativeBSplineBasisSample<Degree, MaximumBSplineCount> sampleB(1); // Same
#ifdef NDEBUG
    CumulativeBSplineBasisSample<Degree, MaximumBSplineCount> sampleC(2); // Different dimension of B
#endif // NDEBUG
    CumulativeBSplineBasisSample<Degree, MaximumBSplineCount> sampleD(1); // Different values of B
    for (size_t i = 0; i < sampleD.m_B.size(); i++)
        sampleD.m_B[i] = 1.23;

    // Check equality
    if (!(sampleA == sampleB) || (sampleA != sampleB))
        return false;

        // Check different dimension
#ifdef NDEBUG
    if ((sampleA == sampleC) || !(sampleA != sampleC))
        return false;
#endif // NDEBUG

    // Check different values
    if ((sampleA == sampleD) || !(sampleA != sampleD))
        return false;

    // Otherwise: success!
    return true;
}

//! Check operators of cumulative B-spline basis sample
TEST(CumulativeBSplineBasisSample, Operators)
{
    ASSERT_TRUE(checkOperatorsCumulativeBSplineBasisSample<0>());
    ASSERT_TRUE(checkOperatorsCumulativeBSplineBasisSample<1>());
    ASSERT_TRUE(checkOperatorsCumulativeBSplineBasisSample<2>());
    ASSERT_TRUE(checkOperatorsCumulativeBSplineBasisSample<3>());
}

//! Checks getters of cumulative B-spline basis sample of certain degree
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkGettersCumulativeBSplineBasisSample()
{
    CumulativeBSplineBasisSample<Degree, MaximumBSplineCount> sample;
    if (sample.degree() != Degree)
        return false;
    if (sample.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of cumulative B-spline basis sample
TEST(CumulativeBSplineBasisSample, Getters)
{
    ASSERT_TRUE(checkGettersCumulativeBSplineBasisSample<0>());
    ASSERT_TRUE(checkGettersCumulativeBSplineBasisSample<1>());
    ASSERT_TRUE(checkGettersCumulativeBSplineBasisSample<2>());
    ASSERT_TRUE(checkGettersCumulativeBSplineBasisSample<3>());
}

#endif // HAVE_EIGEN3
