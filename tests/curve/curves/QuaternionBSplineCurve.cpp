/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/curves/QuaternionBSplineCurve.hpp"
#include "QuaternionCurveHelpers.hpp"
#include "gtest/gtest.h"
#include <cmath>

using namespace broccoli;
using namespace curve;

//! Test of basic usage of QuaternionBSplineCurve (of order 4)
TEST(QuaternionBSplineCurve, BasicUsage)
{
    // Create curves
    QuaternionBSplineCurve<0, 3> curveA;
    ASSERT_TRUE(!curveA.isValid());
    curveA.m_basis.setClampedUniformKnots(1);
    curveA.m_controlPoints.reserve(1);
    curveA.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_TRUE(curveA.isValid());
    QuaternionBSplineCurve<0, 3> curveB;
    ASSERT_TRUE(!curveB.isValid());
    curveB.m_basis.setClampedUniformKnots(3);
    curveB.m_controlPoints.reserve(3);
    curveB.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    curveB.m_controlPoints.push_back(Eigen::Quaterniond(1, 1, 0, 0));
    curveB.m_controlPoints.back().normalize();
    curveB.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_TRUE(curveB.isValid());
    QuaternionBSplineCurve<1, 2> curveC;
    ASSERT_TRUE(!curveC.isValid());
    curveC.m_basis.setClampedUniformKnots(2);
    curveC.m_controlPoints.reserve(2);
    curveC.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    curveC.m_controlPoints.push_back(Eigen::Quaterniond(1, 1, 0, 0));
    curveC.m_controlPoints.back().normalize();
    ASSERT_TRUE(curveC.isValid());
    QuaternionBSplineCurve<2, 3> curveD;
    ASSERT_TRUE(!curveD.isValid());
    curveD.m_basis.setClampedUniformKnots(3);
    curveD.m_controlPoints.reserve(3);
    curveD.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    curveD.m_controlPoints.push_back(Eigen::Quaterniond(1, 1, 0, 0));
    curveD.m_controlPoints.back().normalize();
    curveD.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_TRUE(curveD.isValid());
    QuaternionBSplineCurve<3, 4> curveE;
    ASSERT_TRUE(!curveE.isValid());
    curveE.m_basis.setClampedUniformKnots(4);
    curveE.m_controlPoints.reserve(4);
    curveE.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    curveE.m_controlPoints.push_back(Eigen::Quaterniond(1, 1, 0, 0));
    curveE.m_controlPoints.back().normalize();
    curveE.m_controlPoints.push_back(Eigen::Quaterniond(0, 1, 1, 0));
    curveE.m_controlPoints.back().normalize();
    curveE.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_TRUE(curveE.isValid());
    QuaternionBSplineCurve<3, 4> curveF;
    ASSERT_TRUE(!curveF.isValid());
    curveF.m_basis.m_knotSequence.resize(8);
    curveF.m_basis.m_knotSequence[0] = -3;
    curveF.m_basis.m_knotSequence[1] = -2;
    curveF.m_basis.m_knotSequence[2] = -2;
    curveF.m_basis.m_knotSequence[3] = 0;
    curveF.m_basis.m_knotSequence[4] = 1;
    curveF.m_basis.m_knotSequence[5] = 3;
    curveF.m_basis.m_knotSequence[6] = 3;
    curveF.m_basis.m_knotSequence[7] = 3;
    curveF.m_controlPoints.reserve(4);
    curveF.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    curveF.m_controlPoints.push_back(Eigen::Quaterniond(1, 1, 0, 0));
    curveF.m_controlPoints.back().normalize();
    curveF.m_controlPoints.push_back(Eigen::Quaterniond(0, 1, 1, 0));
    curveF.m_controlPoints.back().normalize();
    curveF.m_controlPoints.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_TRUE(curveF.isValid());

    // Test function type
    ASSERT_TRUE(curveA.functionType() == QuaternionCurve::FunctionType::BSPLINE);

    // (In-)equality operators
#ifdef NDEBUG
    ASSERT_TRUE(curveA != curveB);
    ASSERT_TRUE(curveA != (QuaternionBSplineCurve<0, 3>()));
#endif // NDEBUG

    // Test SIMPLE interpolation
    QuaternionBSplineCurve<3, 6> curveX;
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> parameters;
    parameters.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    parameters.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    parameters.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    QuaternionCurveInterpolationMethod quaternionCurveInterpolationMethod = QuaternionCurveInterpolationMethod::BSPLINE_SIMPLE;
    QuaternionCurveResult quaternionCurveResult = QuaternionCurveResult::UNKNOWN;
    // Wrong interpolation: too few control points
#ifdef NDEBUG
    ASSERT_FALSE(curveX.interpolate(parameters, nullptr, quaternionCurveInterpolationMethod, &quaternionCurveResult));
    ASSERT_TRUE(quaternionCurveResult == QuaternionCurveResult::ERROR_INVALID_PARAMETERS);
#endif // NDEBUG
    parameters.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_TRUE(curveX.interpolate(parameters, nullptr, quaternionCurveInterpolationMethod, &quaternionCurveResult));
    ASSERT_TRUE(quaternionCurveResult == QuaternionCurveResult::SUCCESS);
    ASSERT_TRUE(curveX.isValid() == true);
    std::vector<double> proportions;
    proportions.push_back(0.5);
    proportions.push_back(0.5);
    // Wrong interpolation: too few control points
#ifdef NDEBUG
    ASSERT_FALSE(curveX.interpolate(parameters, &proportions, quaternionCurveInterpolationMethod, &quaternionCurveResult));
    ASSERT_TRUE(quaternionCurveResult == QuaternionCurveResult::ERROR_INVALID_PARAMETERS);
#endif // NDEBUG
    parameters.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    parameters.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_TRUE(curveX.interpolate(parameters, nullptr, quaternionCurveInterpolationMethod, &quaternionCurveResult));
    ASSERT_TRUE(quaternionCurveResult == QuaternionCurveResult::SUCCESS);
    ASSERT_TRUE(curveX.isValid() == true);
    // Wrong interpolation: too many control points
#ifdef NDEBUG
    parameters.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    ASSERT_FALSE(curveX.interpolate(parameters, &proportions, quaternionCurveInterpolationMethod, &quaternionCurveResult));
    ASSERT_TRUE(quaternionCurveResult == QuaternionCurveResult::ERROR_INVALID_PARAMETERS);
#endif // NDEBUG

    // Check analytic and numeric derivatives
    compareAnalyticAndNumericDerivativeQuaternionCurve<decltype(curveA)>(curveA, 1);
    compareAnalyticAndNumericDerivativeQuaternionCurve<decltype(curveA)>(curveA, 2);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomQuaternionCurve<decltype(curveA)>(curveA, 1);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionCurve<decltype(curveA), 2>(curveA);

    // Check encoding to XML
    checkEncodeToXMLQuaternionCurve<decltype(curveA)>(curveA);
}

//! Checks getters of quaternion B-Spline curve of certain degree
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkGettersQuaternionBSplineCurve()
{
    QuaternionBSplineCurve<Degree, MaximumBSplineCount> curve;
    if (curve.degree() != Degree)
        return false;
    if (curve.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of quaternion B-spline curve
TEST(QuaternionBSplineCurve, Getters)
{
    ASSERT_TRUE(checkGettersQuaternionBSplineCurve<0>());
    ASSERT_TRUE(checkGettersQuaternionBSplineCurve<1>());
    ASSERT_TRUE(checkGettersQuaternionBSplineCurve<2>());
    ASSERT_TRUE(checkGettersQuaternionBSplineCurve<3>());
    ASSERT_TRUE(checkGettersQuaternionBSplineCurve<4>());
    ASSERT_TRUE(checkGettersQuaternionBSplineCurve<5>());
}

#endif // HAVE_EIGEN3
