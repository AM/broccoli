/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/curves/ExponentialCurve.hpp"
#include "CurveHelpers.hpp"
#include "gtest/gtest.h"
#include <time.h>

using namespace broccoli;
using namespace curve;

//! Function to test exponential curve operators
bool checkOperatorsExponentialCurve(const bool& TestOperator, const bool& TestCase)
{
    ExponentialCurve TestExponentialA;
    ExponentialCurve TestExponentialB;

    uint8_t i;
    float randomNumber;
    srand(time(NULL));
    if (TestCase == true) {
        for (i = 0; i < TestExponentialA.m_coefficients.size(); i++) {
            randomNumber = rand();
            TestExponentialA.m_coefficients[i] = randomNumber;
            TestExponentialB.m_coefficients[i] = randomNumber;
        }
    } else {
        for (i = 0; i < TestExponentialA.m_coefficients.size(); i++) {
            randomNumber = rand();
            TestExponentialA.m_coefficients[i] = randomNumber;
            TestExponentialB.m_coefficients[TestExponentialA.m_coefficients.size() - 1 - i] = randomNumber;
        }
    }

    //Switch Operators *==* or *!=*
    if (TestOperator == true) {
        if (TestExponentialA == TestExponentialB)
            return true;
        else
            return false;
    } else {
        if (TestExponentialA != TestExponentialB)
            return true;
        else
            return false;
    }
}

//! Test of exponential curve operators
TEST(ExponentialCurve, Operators)
{
    //Test equally operator on true
    ASSERT_TRUE(checkOperatorsExponentialCurve(true, true));

    //Test equally operator on false
    ASSERT_FALSE(checkOperatorsExponentialCurve(true, false));

    //Test inequally operator on true
    ASSERT_FALSE(checkOperatorsExponentialCurve(false, true));

    //Test inequally operator on false
    ASSERT_TRUE(checkOperatorsExponentialCurve(false, false));
}

//! Test basic usage of exponential curves
TEST(ExponentialCurve, BasicUsage)
{
    // Construct
    ExponentialCurve exponential;
    exponential.m_coefficients[0] = 1.23;
    exponential.m_coefficients[1] = -2.34;
    exponential.m_coefficients[2] = 0.123;

    // Check validity
    ASSERT_TRUE(exponential.isValid());

    // Check function type
    ASSERT_TRUE(exponential.functionType() == Curve::FunctionType::EXPONENTIAL);

    // Check analytic and numeric derivatives
    compareAnalyticAndNumericDerivativeCurve<decltype(exponential)>(exponential, 1);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomCurve<decltype(exponential)>(exponential, 1);

    // Check high performance evaluation
    checkHighPerformanceEvaluationCurve<decltype(exponential), 2>(exponential);

    // Check encoding to XML
    checkEncodeToXMLCurve<decltype(exponential)>(exponential);
}
