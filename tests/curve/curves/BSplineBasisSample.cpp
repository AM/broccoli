/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/curves/BSplineBasisSample.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Checks construction of B-spline sample of certain degree
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkConstructionBSplineBasisSample()
{
    // Default constructor
    BSplineBasisSample<Degree, MaximumBSplineCount> sampleA;
    BSplineBasisSample<Degree, MaximumBSplineCount> sampleB(1);
    if (sampleB.m_B.size() == 0 || sampleB.m_B[0].size() == 0)
        return false;
    for (unsigned int j = 1; j < Degree + 1; j++) {
        for (int i = 0; i < 2; i++) {
            if (sampleA.m_X[i][j - 1] != -1)
                return false;
            if (sampleB.m_X[i][j - 1] != -1)
                return false;
        }
    }

    // Specialized constructor
    typename BSplineBasisSample<Degree, MaximumBSplineCount>::KnotSequenceType knotSequence;
    knotSequence.resize((Degree + 1) + 1);
    knotSequence.fill(0);
    for (size_t i = 1; i < knotSequence.size(); i++)
        knotSequence[i] = 1;
    BSplineBasisSample<Degree, MaximumBSplineCount> sampleC(knotSequence, 0, 0);
    if (sampleC.m_B.size() == 0 || sampleC.m_B[0].size() == 0)
        return false;
    for (unsigned int j = 1; j < Degree + 1; j++) {
        if (sampleC.m_X[0][j - 1] != 0)
            return false;
        if (sampleC.m_X[1][j - 1] != 0)
            return false;
    }

    // Otherwise: success!
    return true;
}

//! Check construction of B-spline basis sample
TEST(BSplineBasisSample, Construction)
{
    ASSERT_TRUE(checkConstructionBSplineBasisSample<0>());
    ASSERT_TRUE(checkConstructionBSplineBasisSample<1>());
    ASSERT_TRUE(checkConstructionBSplineBasisSample<2>());
    ASSERT_TRUE(checkConstructionBSplineBasisSample<3>());
}

//! Checks operators
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkOperatorsBSplineBasisSample()
{
    // Check (in-)equality
    BSplineBasisSample<Degree, MaximumBSplineCount> sampleA(1);
    BSplineBasisSample<Degree, MaximumBSplineCount> sampleB(1); // Same
#ifdef NDEBUG
    BSplineBasisSample<Degree, MaximumBSplineCount> sampleC(2); // Different dimension of B
#endif
    BSplineBasisSample<Degree, MaximumBSplineCount> sampleD(1); // Different values of B
    for (size_t i = 0; i < sampleD.m_B.size(); i++)
        for (size_t j = 0; j < sampleD.m_B[i].size(); j++)
            sampleD.m_B[i][j] = 1.23;

    // Check equal
    if (!(sampleA == sampleB) || (sampleA != sampleB))
        return false;

#ifdef NDEBUG
    // Check wrong dimension
    if ((sampleA == sampleC) || !(sampleA != sampleC))
        return false;
#endif

    // Check inequal
    if ((sampleA == sampleD) || !(sampleA != sampleD))
        return false;

    // Otherwise: success!
    return true;
}

//! Check operators of B-spline basis sample
TEST(BSplineBasisSample, Operators)
{
    ASSERT_TRUE(checkOperatorsBSplineBasisSample<0>());
    ASSERT_TRUE(checkOperatorsBSplineBasisSample<1>());
    ASSERT_TRUE(checkOperatorsBSplineBasisSample<2>());
    ASSERT_TRUE(checkOperatorsBSplineBasisSample<3>());
}

//! Checks getters of B-spline basis sample of certain degree
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkGettersBSplineBasisSample()
{
    BSplineBasisSample<Degree, MaximumBSplineCount> sample;
    if (sample.degree() != Degree)
        return false;
    if (sample.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of B-spline basis sample
TEST(BSplineBasisSample, Getters)
{
    ASSERT_TRUE(checkGettersBSplineBasisSample<0>());
    ASSERT_TRUE(checkGettersBSplineBasisSample<1>());
    ASSERT_TRUE(checkGettersBSplineBasisSample<2>());
    ASSERT_TRUE(checkGettersBSplineBasisSample<3>());
}

#endif // HAVE_EIGEN3
