/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/curves/PolynomialCurve.hpp"
#include "CurveHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Template function to test polynomial curve operators
template <unsigned int Degree>
bool checkOperatorsPolynomialCurve(const bool& TestOperator, const bool& TestCase)
{
    PolynomialCurve<Degree> TestpolynomA;
    PolynomialCurve<Degree> TestpolynomB;

    uint8_t i;
    float randomNumber;
    srand(time(NULL));
    if (TestCase == true) {
        for (i = 0; i <= Degree; i++) {
            randomNumber = rand();
            TestpolynomA.m_coefficients[i] = randomNumber;
            TestpolynomB.m_coefficients[i] = randomNumber;
        }
    } else {
        for (i = 0; i <= Degree; i++) {
            randomNumber = rand();
            TestpolynomA.m_coefficients[i] = randomNumber;
            TestpolynomB.m_coefficients[Degree - i] = randomNumber * 2;
        }
    }

    //Switch Operators *==* or *!=*
    if (TestOperator == true) {
        if (TestpolynomA == TestpolynomB)
            return true;
        else
            return false;
    } else {
        if (TestpolynomA != TestpolynomB)
            return true;
        else
            return false;
    }
}

//! Test of polynomial curve operators
TEST(PolynomialCurve, Operators)
{
    // Test equally operator on true
    ASSERT_TRUE(checkOperatorsPolynomialCurve<0>(true, true));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<1>(true, true));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<2>(true, true));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<3>(true, true));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<4>(true, true));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<5>(true, true));

    // Test equally operator on false
    ASSERT_FALSE(checkOperatorsPolynomialCurve<0>(true, false));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<1>(true, false));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<2>(true, false));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<3>(true, false));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<4>(true, false));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<5>(true, false));

    // Test inequally operator on true
    ASSERT_FALSE(checkOperatorsPolynomialCurve<0>(false, true));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<1>(false, true));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<2>(false, true));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<3>(false, true));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<4>(false, true));
    ASSERT_FALSE(checkOperatorsPolynomialCurve<5>(false, true));

    // Test inequally operator on false
    ASSERT_TRUE(checkOperatorsPolynomialCurve<0>(false, false));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<1>(false, false));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<2>(false, false));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<3>(false, false));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<4>(false, false));
    ASSERT_TRUE(checkOperatorsPolynomialCurve<5>(false, false));
}

//! Checks getters of polynomial curves of certain degree
template <unsigned int Degree>
bool checkGettersPolynomialCurve()
{
    PolynomialCurve<Degree> polynomial;
    if (polynomial.degree() != Degree)
        return false;
    if (polynomial.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of polynomial curves
TEST(PolynomialCurve, Getters)
{
    ASSERT_TRUE(checkGettersPolynomialCurve<0>());
    ASSERT_TRUE(checkGettersPolynomialCurve<1>());
    ASSERT_TRUE(checkGettersPolynomialCurve<2>());
    ASSERT_TRUE(checkGettersPolynomialCurve<3>());
}

//! Template function to check basic usage of polynomial curves of the given degree
template <unsigned int Degree>
static void checkBasicUsagePolynomialCurve()
{
    // Construct
    PolynomialCurve<Degree> polynomial;
    for (unsigned int d = 0; d <= Degree; d++)
        polynomial.m_coefficients[d] = 1.23 + d;

    // Check validity
    ASSERT_TRUE(polynomial.isValid());

    // Check function type
    ASSERT_TRUE(polynomial.functionType() == Curve::FunctionType::POLYNOMIAL);

    // Check analytic and numeric derivatives
    compareAnalyticAndNumericDerivativeCurve<decltype(polynomial)>(polynomial, 1);
    compareAnalyticAndNumericDerivativeCurve<decltype(polynomial)>(polynomial, 2);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomCurve<decltype(polynomial)>(polynomial, 1);
    compareAnalyticAndNumericDerivativeCustomCurve<decltype(polynomial)>(polynomial, 2);

    // Check high performance evaluation
    checkHighPerformanceEvaluationCurve<decltype(polynomial), 2>(polynomial);

    // Check encoding to XML
    checkEncodeToXMLCurve<decltype(polynomial)>(polynomial);
}

//! Test basic usage of polynomial curves
TEST(PolynomialCurve, BasicUsage)
{
    checkBasicUsagePolynomialCurve<0>();
    checkBasicUsagePolynomialCurve<1>();
    checkBasicUsagePolynomialCurve<2>();
    checkBasicUsagePolynomialCurve<3>();
    checkBasicUsagePolynomialCurve<4>();
    checkBasicUsagePolynomialCurve<5>();
}

//! Test interpolateConstant()
TEST(PolynomialCurve, InterpolateConstant)
{
    // Construct
    PolynomialCurve<0> polynomial;

    // Perform interpolation
    std::vector<double> parameters(1);
    parameters[0] = 1.23;
    CurveResult result = CurveResult::UNKNOWN;
    ASSERT_TRUE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CONSTANT, &result));
    ASSERT_TRUE(result == CurveResult::SUCCESS);

    // Check result
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 0) - parameters[0]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 0) - parameters[0]) < 1e-9);

#ifdef NDEBUG
    // Perform interpolation (wrong parameter dimension)
    parameters.resize(parameters.size() + 1);
    parameters.back() = 3.456;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CONSTANT, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_INVALID_PARAMETERS);
#endif
}

//! Test interpolateLinear()
TEST(PolynomialCurve, InterpolateLinear)
{
    // Construct
    PolynomialCurve<1> polynomial;

    // Perform interpolation
    std::vector<double> parameters(2);
    parameters[0] = 1.23;
    parameters[1] = -2.34;
    CurveResult result = CurveResult::UNKNOWN;
    ASSERT_TRUE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_LINEAR, &result));
    ASSERT_TRUE(result == CurveResult::SUCCESS);

    // Check result
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 0) - parameters[0]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 0) - parameters[1]) < 1e-9);

#ifdef NDEBUG
    // Perform interpolation (degree too low)
    PolynomialCurve<0> lowerDegreePolynomial;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(lowerDegreePolynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_LINEAR, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_DEGREE_TOO_LOW);

    // Perform interpolation (wrong parameter dimension)
    parameters.resize(parameters.size() + 1);
    parameters.back() = 3.456;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_LINEAR, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_INVALID_PARAMETERS);
#endif
}

//! Test interpolateCubicFirstDerivatives()
TEST(PolynomialCurve, InterpolateCubicFirstDerivatives)
{
    // Construct
    PolynomialCurve<3> polynomial;

    // Perform interpolation
    std::vector<double> parameters(4);
    parameters[0] = 1.23;
    parameters[1] = -2.34;
    parameters[2] = 3.45;
    parameters[3] = -4.56;
    CurveResult result = CurveResult::UNKNOWN;
    ASSERT_TRUE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CUBIC_FIRST_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::SUCCESS);

    // Check result
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 0) - parameters[0]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 0) - parameters[1]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 1) - parameters[2]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 1) - parameters[3]) < 1e-9);

#ifdef NDEBUG
    // Perform interpolation (degree too low)
    PolynomialCurve<2> lowerDegreePolynomial;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(lowerDegreePolynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CUBIC_FIRST_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_DEGREE_TOO_LOW);

    // Perform interpolation (wrong parameter dimension)
    parameters.resize(parameters.size() + 1);
    parameters.back() = 3.456;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CUBIC_FIRST_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_INVALID_PARAMETERS);
#endif
}

//! Test interpolateCubicSecondDerivatives()
TEST(PolynomialCurve, InterpolateCubicSecondDerivatives)
{
    // Construct
    PolynomialCurve<3> polynomial;

    // Perform interpolation
    std::vector<double> parameters(4);
    parameters[0] = 1.23;
    parameters[1] = -2.34;
    parameters[2] = 3.45;
    parameters[3] = -4.56;
    CurveResult result = CurveResult::UNKNOWN;
    ASSERT_TRUE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CUBIC_SECOND_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::SUCCESS);

    // Check result
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 0) - parameters[0]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 0) - parameters[1]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 2) - parameters[2]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 2) - parameters[3]) < 1e-9);

#ifdef NDEBUG
    // Perform interpolation (degree too low)
    PolynomialCurve<2> lowerDegreePolynomial;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(lowerDegreePolynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CUBIC_SECOND_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_DEGREE_TOO_LOW);

    // Perform interpolation (wrong parameter dimension)
    parameters.resize(parameters.size() + 1);
    parameters.back() = 3.456;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_CUBIC_SECOND_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_INVALID_PARAMETERS);
#endif
}

//! Test interpolateQuinticFirstAndSecondDerivatives()
TEST(PolynomialCurve, InterpolateQuinticFirstAndSecondDerivatives)
{
    // Construct
    PolynomialCurve<5> polynomial;

    // Perform interpolation
    std::vector<double> parameters(6);
    parameters[0] = 1.23;
    parameters[1] = -2.34;
    parameters[2] = 3.45;
    parameters[3] = -4.56;
    parameters[4] = 5.67;
    parameters[5] = -6.78;
    CurveResult result = CurveResult::UNKNOWN;
    ASSERT_TRUE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::SUCCESS);

    // Check result
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 0) - parameters[0]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 0) - parameters[1]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 1) - parameters[2]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 1) - parameters[3]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(0.0, 2) - parameters[4]) < 1e-9);
    ASSERT_TRUE(fabs(polynomial.evaluate(1.0, 2) - parameters[5]) < 1e-9);

#ifdef NDEBUG
    // Perform interpolation (degree too low)
    PolynomialCurve<4> lowerDegreePolynomial;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(lowerDegreePolynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_DEGREE_TOO_LOW);

    // Perform interpolation (wrong parameter dimension)
    parameters.resize(parameters.size() + 1);
    parameters.back() = 3.456;
    result = CurveResult::UNKNOWN;
    ASSERT_FALSE(polynomial.interpolate(parameters, CurveInterpolationMethod::POLYNOMIAL_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result));
    ASSERT_TRUE(result == CurveResult::ERROR_INVALID_PARAMETERS);
#endif
}
