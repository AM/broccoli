/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/curves/InterpolatableQuaternionCurve.hpp"
#include "gtest/gtest.h"

// This module requires Eigen library
#ifdef HAVE_EIGEN3

using namespace broccoli;
using namespace curve;

//! Test string representation of the given quaternion curve interpolation method
TEST(InterpolatableQuaternionCurve, QuaternionCurveInterpolationMethodToString)
{
    uint8_t i;
    for (i = 0; i < static_cast<uint8_t>(QuaternionCurveInterpolationMethod::QUATERNIONCURVEINTERPOLATIONMETHOD_COUNT); i++) {
        // Get string representation
        std::string str = quaternionCurveInterpolationMethodString(static_cast<QuaternionCurveInterpolationMethod>(i));

        // Check, if string is non-empty
        ASSERT_GT(str.size(), 0);

        // Check, if element is known
        ASSERT_FALSE(str == "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(QuaternionCurveInterpolationMethod::QUATERNIONCURVEINTERPOLATIONMETHOD_COUNT);
    ASSERT_TRUE(quaternionCurveInterpolationMethodString(static_cast<QuaternionCurveInterpolationMethod>(i)) == "UNKNOWN");
#endif // NDEBUG
}

#endif // HAVE_EIGEN3
