/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/curves/QuaternionCurve.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test string representation of the given result type for quaternion curve algorithms
TEST(QuaternionCurve, QuaternionCurveResultToString)
{
    uint8_t i;
    for (i = 0; i < static_cast<uint8_t>(QuaternionCurveResult::QUATERNIONCURVERESULT_COUNT); i++) {
        // Get string representation
        std::string str = quaternionCurveResultString(static_cast<QuaternionCurveResult>(i));

        // Check, if string is non-empty
        ASSERT_GT(str.size(), 0);

        // Check, if element is known
        if (i > 0)
            ASSERT_FALSE(str == "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(QuaternionCurveResult::QUATERNIONCURVERESULT_COUNT);
    ASSERT_TRUE(quaternionCurveResultString(static_cast<QuaternionCurveResult>(i)) == "UNKNOWN");
#endif // NDEBUG
}

//! Test string representation of the given quaternion function type
TEST(QuaternionCurve, FunctionTypeToString)
{
    uint8_t i;
    for (i = 0; i < static_cast<uint8_t>(QuaternionCurve::FunctionType::FUNCTIONTYPE_COUNT); i++) {
        ASSERT_GT(QuaternionCurve::functionTypeString(static_cast<QuaternionCurve::FunctionType>(i)).size(), 0);
        ASSERT_TRUE(QuaternionCurve::functionTypeString(static_cast<QuaternionCurve::FunctionType>(i)) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(QuaternionCurve::FunctionType::FUNCTIONTYPE_COUNT);
    ASSERT_TRUE(QuaternionCurve::functionTypeString(static_cast<QuaternionCurve::FunctionType>(i)) == "UNKNOWN");
#endif // NDEBUG
}

//! Check recalculateKeyFrameSequence() function
TEST(QuaternionCurve, recalculateKeyFrameSequence)
{
    // Initialize test sequence
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> keyFrames;
    keyFrames.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    keyFrames.push_back(Eigen::Quaterniond(-1, -1, 0, 0));
    keyFrames.push_back(Eigen::Quaterniond(1, 0, 0, 0));
    keyFrames.push_back(Eigen::Quaterniond(-1, 0, 0, 0));
    for (size_t i = 0; i < keyFrames.size(); i++)
        keyFrames[i].normalize();

    // Process
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> processedKeyFrames = QuaternionCurve::recalculateKeyFrameSequence(keyFrames);

    // Check results
    ASSERT_TRUE(keyFrames.size() == processedKeyFrames.size());
    ASSERT_TRUE(processedKeyFrames[0].coeffs() == keyFrames[0].coeffs());
    ASSERT_TRUE(processedKeyFrames[1].coeffs() == -keyFrames[1].coeffs());
    ASSERT_TRUE(processedKeyFrames[2].coeffs() == keyFrames[2].coeffs());
    ASSERT_TRUE(processedKeyFrames[3].coeffs() == -keyFrames[3].coeffs());
}

#endif // HAVE_EIGEN3
