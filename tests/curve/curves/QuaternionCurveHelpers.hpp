/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#pragma once // Load this file only once

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/core/Time.hpp"
#include "broccoli/curve/curves/QuaternionCurve.hpp"
#include "gtest/gtest.h"
#include <fstream>
#include <iostream>
#include <string>

//! Template function to compare analytic and numeric derivative of generic (quaternion) objects with corresponding interface
template <class EvaluatableObjectType>
inline void compareAnalyticAndNumericDerivativeGenericQuaternion(const EvaluatableObjectType& evaluatableObject, const unsigned int& derivationOrder)
{
    // Initialize helpers
    const double xmin = 0; // WARNING: [xmin, xmax] has to be [0, 1], since the numeric derivative evaluation is only possible for this interval!
    const double xmax = 1;
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize + 1);
    static const double toleratedRelativeError = 1e-3;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + i * stepsize;

        // Compute analytic
        const Eigen::Quaterniond analyticDerivative = evaluatableObject.evaluate(x, derivationOrder);

        // Compute numeric derivative
        const Eigen::Quaterniond numericDerivative = evaluatableObject.evaluateNumericDerivative(x, derivationOrder);

        // Compute error
        const double error = (analyticDerivative.coeffs() - numericDerivative.coeffs()).norm();
        const double magnitude = analyticDerivative.norm();
        double relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;

        // Check error
        if (relativeError > toleratedRelativeError) {
            std::cout << "Analytic and numeric derivative differ at position x=" << x;
            std::cout << " for derivation order d=" << derivationOrder;
            std::cout << " (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }
    }

    // Check invalid stepsize
#ifdef NDEBUG
    if (derivationOrder > 0) {
        Eigen::Quaterniond invalidDerivative = evaluatableObject.evaluateNumericDerivative(0.5, derivationOrder, -1.0);
        ASSERT_TRUE(invalidDerivative.norm() == 0);
    }
#endif

    // Test projection of numeric derivative
    ASSERT_TRUE((evaluatableObject.evaluateNumericDerivative(-1, derivationOrder).coeffs() - evaluatableObject.evaluateNumericDerivative(0, derivationOrder).coeffs()).norm() == 0);
    ASSERT_TRUE((evaluatableObject.evaluateNumericDerivative(2, derivationOrder).coeffs() - evaluatableObject.evaluateNumericDerivative(1, derivationOrder).coeffs()).norm() == 0);
}

//! Template function to compare analytic and numeric derivative of quaternion curves
template <class QuaternionCurveType>
inline void compareAnalyticAndNumericDerivativeQuaternionCurve(const QuaternionCurveType& curve, const unsigned int& derivationOrder)
{
    compareAnalyticAndNumericDerivativeGenericQuaternion<QuaternionCurveType>(curve, derivationOrder);
}

//! Template function to compare analytic and numeric derivative of a generic (quaternion) object with corresponding functionality
template <class EvaluatableObjectType>
inline void compareAnalyticAndNumericDerivativeGenericCustomQuaternion(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    // Initialize helpers
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize - 1);
    static const double dx = 1e-6; // "Step-size" for numeric derivation

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + stepsize + i * stepsize;

        // Compute analytic
        const Eigen::Quaterniond analyticDerivative = evaluatableObject.evaluate(x, derivationOrder);

        // Compute numeric derivative
        const Eigen::Quaterniond leftSide = evaluatableObject.evaluate(x - dx, derivationOrder - 1);
        const Eigen::Quaterniond rightSide = evaluatableObject.evaluate(x + dx, derivationOrder - 1);
        Eigen::Quaterniond numericDerivative;
        numericDerivative.coeffs() = (rightSide.coeffs() - leftSide.coeffs()) / (2.0 * dx);

        // Compute error
        const double error = (analyticDerivative.coeffs() - numericDerivative.coeffs()).norm();
        const double magnitude = analyticDerivative.norm();
        double relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;

        // Check error
        if (relativeError > toleratedRelativeError) {
            std::cout << "Analytic and numeric derivative (custom) differ at position x=" << x;
            std::cout << " for derivation order d=" << derivationOrder;
            std::cout << " (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }
    }
}

//! Template function to compare analytic and numeric derivative of quaternion curves (custom)
template <class QuaternionCurveType>
inline void compareAnalyticAndNumericDerivativeCustomQuaternionCurve(const QuaternionCurveType& curve, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    compareAnalyticAndNumericDerivativeGenericCustomQuaternion<QuaternionCurveType>(curve, 0, 1, derivationOrder, toleratedRelativeError);
}

//! Template function to check high performance evaluation of generic (quaternion) objects providing a corresponding interface
template <class EvaluatableObjectType, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationGenericQuaternion(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax)
{
    // Initialize helpers
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize + 1);
    static const double toleratedRelativeError = 1e-9;
    broccoli::core::Time normalEvaluationTime = 0;
    broccoli::core::Time highPerformanceEvaluationTime = 0;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + i * stepsize;

        // Perform "normal" evaluation
        std::array<Eigen::Quaterniond, MaximumDerivation + 1> normalDi;
        broccoli::core::Time normalEvaluationStartTime = broccoli::core::Time::currentTime();
        for (unsigned int d = 0; d <= MaximumDerivation; d++)
            normalDi[d] = evaluatableObject.evaluate(x, d);
        normalEvaluationTime += broccoli::core::Time::currentTime() - normalEvaluationStartTime;

        // Perform "high-performance" evaluation
        broccoli::core::Time highPerformanceEvaluationStartTime = broccoli::core::Time::currentTime();
        const auto highPerformanceDi = evaluatableObject.template evaluateD0ToDN<MaximumDerivation>(x);
        highPerformanceEvaluationTime += broccoli::core::Time::currentTime() - highPerformanceEvaluationStartTime;

        // Check value and derivatives
        for (unsigned int d = 0; d <= MaximumDerivation; d++) {
            const double error = (normalDi[d].coeffs() - highPerformanceDi[d].coeffs()).norm();
            const double magnitude = normalDi[d].norm();
            double relativeError = error;
            if (magnitude > 1e-9)
                relativeError = error / magnitude;
            if (relativeError > toleratedRelativeError) {
                std::cout << "High performance evaluation (evaluateD0ToDN()) differs at position x=" << x;
                std::cout << " for derivation order d=" << d;
                std::cout << " (absolute error: " << error;
                std::cout << ") (relative error : " << relativeError << ")\n";
                ASSERT_LT(relativeError, toleratedRelativeError);
            }
        }
    }

    // Print time measurements to console
    const double speedImprovements = normalEvaluationTime.toDouble() / highPerformanceEvaluationTime.toDouble();
    std::cout << "High performance evaluation (evaluateD0ToDN()) with N=" << MaximumDerivation << " is " << speedImprovements << " times faster than normal evaluation (" << MaximumDerivation + 1 << "x evaluate())." << std::endl;
}

//! Template function to check high performance evaluation of quaternion curves
template <class QuaternionCurveType, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationQuaternionCurve(const QuaternionCurveType& curve)
{
    checkHighPerformanceEvaluationGenericQuaternion<QuaternionCurveType, MaximumDerivation>(curve, 0, 1);
}

//! Checks encodeToXML() of quaternion curves
template <class QuaternionCurveType>
inline void checkEncodeToXMLQuaternionCurve(const QuaternionCurveType& curve)
{
    broccoli::io::encoding::CharacterStream stream;
    broccoli::io::encoding::CharacterStreamSize streamSize = curve.encodeToXML(stream, 1, 4);
    ASSERT_GT(streamSize, 0);
    ASSERT_EQ(streamSize, stream.size());
}

//! Template function for plotting generic (quaternion) objects with corresponding evaluate function in 2D (using gnuplot)
template <class EvaluatableObjectType>
inline void plotGenericQuaternion2D(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    for (unsigned int d = 0; d <= maximumDerivation; d++) {
        if (d == 0)
            gnuplotFile << "plot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 1) << " w l title \"D^" << d << "q_w\"\n";
        else
            gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 1) << " w l title \"D^" << d << " q_w\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 2) << " w l title \"D^" << d << " q_x\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 3) << " w l title \"D^" << d << " q_y\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * d + 4) << " w l title \"D^" << d << " q_z\"\n";
    }
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * (maximumDerivation + 1) + 1) << " w l title \"omegax\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * (maximumDerivation + 1) + 2) << " w l title \"omegay\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * (maximumDerivation + 1) + 3) << " w l title \"omegaz\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * (maximumDerivation + 1) + 4) << " w l title \"alphax\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * (maximumDerivation + 1) + 5) << " w l title \"alphay\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (1 + 4 * (maximumDerivation + 1) + 6) << " w l title \"alphaz\"\n";
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    const size_t steps = std::floor((xmax - xmin) / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = xmin + i * stepSize;
        dataFile << x << " ";

        // Evaluate value and derivatives
        std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> Di;
        size_t maximumDerivationToEvaluate = maximumDerivation;
        if (maximumDerivationToEvaluate < 2)
            maximumDerivationToEvaluate = 2; // Evaluate min. up to the second order derivative (for angular acceleration)
        Di.resize(1 + maximumDerivationToEvaluate);
        for (unsigned int d = 0; d <= maximumDerivationToEvaluate; d++)
            Di[d] = evaluatableObject.evaluate(x, d);

        // Write value and derivatives
        for (unsigned int d = 0; d <= maximumDerivation; d++) {
            dataFile << " " << Di[d].w();
            dataFile << " " << Di[d].x();
            dataFile << " " << Di[d].y();
            dataFile << " " << Di[d].z();
        }

        // Compute intermediate results
        const Eigen::Quaterniond inverseValue = Di[0].conjugate(); // q^-1(t) (for unit-quaternions the inverse is equal to the conjugate)

        // Compute angular velocity
        // ------------------------
        // Compute (half) angular velocity represented as quaternion (with zero scalar value)
        const Eigen::Quaterniond halfAngularVelocity = Di[1] * inverseValue; // dq(t)/dt * q^-1(t)

        // Convert to 3-dimensional vector
        const Eigen::Vector3d angularVelocity = 2.0 * halfAngularVelocity.vec();

        // Write angular velocity
        for (int j = 0; j < 3; j++)
            dataFile << " " << angularVelocity(j);

        // Compute angular acceleration
        // ----------------------------
        // Compute (half) angular acceleration represented as quaternion (with zero scalar value)
        Eigen::Quaterniond firstPart = Di[2] * inverseValue; // d^2q(t)/dt^2 * q^-1(t)
        Eigen::Quaterniond secondPart = halfAngularVelocity * halfAngularVelocity; // (dq(t)/dt * q^-1(t))^2
        Eigen::Quaterniond halfAngularAcceleration;
        halfAngularAcceleration.coeffs() = firstPart.coeffs() - secondPart.coeffs();

        // Convert to 3-dimensional vector and pass back
        Eigen::Vector3d angularAcceleration = 2.0 * halfAngularAcceleration.vec();

        // Write angular acceleration
        for (int j = 0; j < 3; j++)
            dataFile << " " << angularAcceleration(j);

        // Proceed to next line
        dataFile << "\n";
    }
    dataFile.close();
}

//! Template function for plotting quaternion curves in 2D (using gnuplot)
template <class CurveType>
inline void plotQuaternionCurve2D(const CurveType& curve, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    plotGenericQuaternion2D<CurveType>(curve, 0, 1, stepSize, maximumDerivation, fileName);
}

//! Template function for plotting generic (quaternion) objects with corresponding evaluate function in 3D (using gnuplot)
template <class EvaluatableObjectType>
inline void plotGenericQuaternion3D(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const double& stepSize, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "set style line 1 linecolor rgb '#0065BD' linetype 1 linewidth 1\n";
    gnuplotFile << "set style line 2 linecolor rgb '#E37222' linetype 1 linewidth 1\n";
    gnuplotFile << "set style line 3 linecolor rgb '#A2AD00' linetype 1 linewidth 1\n";
    gnuplotFile << "\n";
    gnuplotFile << "splot '" << fileName << ".dat' using 2:3:4 with lines linestyle 1 title \"x-axis\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 5:6:7 with lines linestyle 2 title \"y-axis\"\n";
    gnuplotFile << "replot '" << fileName << ".dat' using 8:9:10 with lines linestyle 3 title \"z-axis\"\n";
    gnuplotFile << "set view equal xyz\n";
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    const size_t steps = std::floor((xmax - xmin) / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = xmin + i * stepSize;
        dataFile << x << " ";

        // Evaluate value
        Eigen::Quaterniond point = evaluatableObject.evaluate(x, 0);

        // Compute x-axis
        Eigen::Quaterniond xAxis = point * Eigen::Quaterniond(0, 1, 0, 0) * point.conjugate();
        dataFile << " " << xAxis.x() << " " << xAxis.y() << " " << xAxis.z();

        // Compute y-axis
        Eigen::Quaterniond yAxis = point * Eigen::Quaterniond(0, 0, 1, 0) * point.conjugate();
        dataFile << " " << yAxis.x() << " " << yAxis.y() << " " << yAxis.z();

        // Compute z-axis
        Eigen::Quaterniond zAxis = point * Eigen::Quaterniond(0, 0, 0, 1) * point.conjugate();
        dataFile << " " << zAxis.x() << " " << zAxis.y() << " " << zAxis.z();

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Template function for plotting quaternion curves in 3D (using gnuplot)
template <class CurveType>
inline void plotQuaternionCurve3D(const CurveType& curve, const double& stepSize, const std::string& fileName)
{
    plotGenericQuaternion3D<CurveType>(curve, 0, 1, stepSize, fileName);
}

#endif // HAVE_EIGEN3
