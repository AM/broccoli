/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/curves/InterpolatableCurve.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test string representation of the given curve interpolation method
TEST(InterpolatableCurve, CurveInterpolationMethodToString)
{
    uint8_t i;
    for (i = 0; i < static_cast<uint8_t>(CurveInterpolationMethod::CURVEINTERPOLATIONMETHOD_COUNT); i++) {
        ASSERT_GT(curveInterpolationMethodString(static_cast<CurveInterpolationMethod>(i)).size(), 0);
        ASSERT_TRUE(curveInterpolationMethodString(static_cast<CurveInterpolationMethod>(i)) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(CurveInterpolationMethod::CURVEINTERPOLATIONMETHOD_COUNT);
    ASSERT_TRUE(curveInterpolationMethodString(static_cast<CurveInterpolationMethod>(i)) == "UNKNOWN");
#endif // NDEBUG
}
