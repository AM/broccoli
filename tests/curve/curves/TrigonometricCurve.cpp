/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/curves/TrigonometricCurve.hpp"
#include "CurveHelpers.hpp"
#include "gtest/gtest.h"
#include <time.h>

using namespace broccoli;
using namespace curve;

//! Function to test trigonometric curve operators
bool checkOperatorsTrigonometricCurve(const bool& TestOperator, const bool& TestCase, const bool& TestType, const TrigonometricCurve::Type& type)
{
    TrigonometricCurve TestTrigonometricA;
    TrigonometricCurve TestTrigonometricB;

    uint8_t i;
    float randomNumber;
    srand(time(NULL));

    if (TestCase == true) {
        for (i = 0; i < TestTrigonometricA.m_coefficients.size(); i++) {
            randomNumber = rand();
            TestTrigonometricA.m_coefficients[i] = randomNumber;
            TestTrigonometricB.m_coefficients[i] = randomNumber;
            TestTrigonometricA.m_type = type;
            TestTrigonometricB.m_type = type;
        }
    } else {
        if (TestType == false) {
            for (i = 0; i < TestTrigonometricA.m_coefficients.size(); i++) {
                randomNumber = rand();
                TestTrigonometricA.m_coefficients[i] = randomNumber;
                TestTrigonometricB.m_coefficients[TestTrigonometricB.m_coefficients.size() - 1 - i] = randomNumber;
            }

            TestTrigonometricA.m_type = type;
            TestTrigonometricB.m_type = type;
        } else {
            for (i = 0; i < TestTrigonometricA.m_coefficients.size(); i++) {
                randomNumber = rand();
                TestTrigonometricA.m_coefficients[i] = randomNumber;
                TestTrigonometricB.m_coefficients[i] = randomNumber;
            }

            do {
                randomNumber = rand() % static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT);

                TestTrigonometricA.m_type = type;
                TestTrigonometricB.m_type = static_cast<TrigonometricCurve::Type>(randomNumber);
            } while (type == static_cast<TrigonometricCurve::Type>(randomNumber));
        }
    }

    //Switch Operators *==* or *!=*
    if (TestOperator == true) {
        if (TestTrigonometricA == TestTrigonometricB)
            return true;
        else
            return false;
    } else {
        if (TestTrigonometricA != TestTrigonometricB)
            return true;
        else
            return false;
    }
}

//! Test string representation of the given function type
TEST(TrigonometricCurve, FunctionTypeToString)
{
    uint8_t i;
    TrigonometricCurve testFunctionType;
    for (i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        testFunctionType.m_type = static_cast<TrigonometricCurve::Type>(i);
        ASSERT_GT(testFunctionType.typeString(testFunctionType.m_type).size(), 0);
        ASSERT_TRUE(testFunctionType.typeString(testFunctionType.m_type) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT);
    testFunctionType.m_type = static_cast<TrigonometricCurve::Type>(i);
    ASSERT_TRUE(testFunctionType.typeString(testFunctionType.m_type) == "UNKNOWN");
#endif
}

//! Test of trigonometric curve operators
TEST(TrigonometricCurve, Operators)
{
    uint8_t i;

    //Test equally operator on true
    for (i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        ASSERT_TRUE(checkOperatorsTrigonometricCurve(true, true, true, static_cast<TrigonometricCurve::Type>(i)));
    }

    //Test equally operator on false *false trigonometric type*
    for (i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        ASSERT_FALSE(checkOperatorsTrigonometricCurve(true, false, true, static_cast<TrigonometricCurve::Type>(i)));
    }

    //Test equally operator on false *false trigonometric parameters*
    for (i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        ASSERT_FALSE(checkOperatorsTrigonometricCurve(true, false, false, static_cast<TrigonometricCurve::Type>(i)));
    }

    //Test inequally operator on true
    for (i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        ASSERT_FALSE(checkOperatorsTrigonometricCurve(false, true, true, static_cast<TrigonometricCurve::Type>(i)));
    }

    //Test inequally operator on false *false trigonometric type*
    for (i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        ASSERT_TRUE(checkOperatorsTrigonometricCurve(false, false, true, static_cast<TrigonometricCurve::Type>(i)));
    }

    //Test inequally operator on false *false trigonometric type*
    for (i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        ASSERT_TRUE(checkOperatorsTrigonometricCurve(false, false, false, static_cast<TrigonometricCurve::Type>(i)));
    }
}

//! Test basic usage of trigonometric curves
TEST(TrigonometricCurve, BasicUsage)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(TrigonometricCurve::Type::TYPE_COUNT); i++) {
        // Construct
        TrigonometricCurve trigonometric;
        trigonometric.m_type = static_cast<TrigonometricCurve::Type>(i);
        trigonometric.m_coefficients[0] = 1.23;
        trigonometric.m_coefficients[1] = -2.34;
        trigonometric.m_coefficients[2] = 0.123;
        trigonometric.m_coefficients[3] = -0.234;

        // Check validity
        ASSERT_TRUE(trigonometric.isValid());

        // Check function type
        ASSERT_TRUE(trigonometric.functionType() == Curve::FunctionType::TRIGONOMETRIC);

        // Check analytic and numeric derivatives
        compareAnalyticAndNumericDerivativeCurve<decltype(trigonometric)>(trigonometric, 1);

        // Check analytic and numeric derivatives (custom)
        compareAnalyticAndNumericDerivativeCustomCurve<decltype(trigonometric)>(trigonometric, 1);

        // Check high performance evaluation
        checkHighPerformanceEvaluationCurve<decltype(trigonometric), 2>(trigonometric);

        // Check encoding to XML
        checkEncodeToXMLCurve<decltype(trigonometric)>(trigonometric);
    }
}
