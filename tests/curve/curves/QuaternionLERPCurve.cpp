/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/curves/QuaternionLERPCurve.hpp"
#include "QuaternionCurveHelpers.hpp"
#include "gtest/gtest.h"
#include <cmath>

using namespace broccoli;
using namespace curve;

//! Test of basic usage of QuaternionLERPCurve
TEST(QuaternionLERPCurve, BasicUsage)
{
    // Create curves
    QuaternionLERPCurve curveA; // Default constructor
    curveA.m_controlPoints[0] = Eigen::Quaterniond(0, 1, 0, 0);
    curveA.m_controlPoints[1] = Eigen::Quaterniond(0, 0, 1, 0);
    QuaternionLERPCurve curveB(Eigen::Quaterniond(1, 0, 0, 0), Eigen::Quaterniond(sqrt(2.0) / 2.0, sqrt(2.0) / 2.0, 0, 0)); // Specialized constructor
    QuaternionLERPCurve curveC(Eigen::Quaterniond(1, 0, 0, 0), Eigen::Quaterniond(1, 0, 0, 0)); // Same control points

    // Check validity
    ASSERT_TRUE(curveA.isValid());

    // Test function type
    ASSERT_TRUE(curveA.functionType() == QuaternionCurve::FunctionType::LERP);

    // (In-)equality operators
    ASSERT_TRUE(curveA != curveB);
    ASSERT_TRUE(curveA != curveC);
    ASSERT_TRUE(curveB != curveC);

    // Test evaluation
    ASSERT_TRUE(curveA.evaluate(0.0, 0).coeffs() == curveA.m_controlPoints[0].coeffs());
    ASSERT_TRUE(curveA.evaluate(0.5, 0).coeffs() == 0.5 * (curveA.m_controlPoints[0].coeffs() + curveA.m_controlPoints[1].coeffs()));
    ASSERT_TRUE(curveA.evaluate(1.0, 0).coeffs() == curveA.m_controlPoints[1].coeffs());
    ASSERT_TRUE(curveA.evaluate(0.5, 1).coeffs() == curveA.m_controlPoints[1].coeffs() - curveA.m_controlPoints[0].coeffs());
    ASSERT_TRUE(curveA.evaluate(0.5, 2).norm() == 0);

    // Check analytic and numeric derivatives
    compareAnalyticAndNumericDerivativeQuaternionCurve<decltype(curveA)>(curveA, 1);
    compareAnalyticAndNumericDerivativeQuaternionCurve<decltype(curveA)>(curveA, 2);

    // Check analytic and numeric derivatives (custom)
    compareAnalyticAndNumericDerivativeCustomQuaternionCurve<decltype(curveA)>(curveA, 1);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionCurve<decltype(curveA), 2>(curveA);

    // Check encoding to XML
    checkEncodeToXMLQuaternionCurve<decltype(curveA)>(curveA);
}

#endif // HAVE_EIGEN3
