/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#pragma once // Load this file only once

#include "broccoli/core/Time.hpp"
#include "broccoli/curve/curves/Curve.hpp"
#include "gtest/gtest.h"
#include <fstream>
#include <iostream>
#include <string>

//! Template function to compare analytic and numeric derivative of generic objects with corresponding interface
template <class EvaluatableObjectType>
inline void compareAnalyticAndNumericDerivativeGeneric(const EvaluatableObjectType& evaluatableObject, const unsigned int& derivationOrder)
{
    // Initialize helpers
    const double xmin = 0; // WARNING: [xmin, xmax] has to be [0, 1], since the numeric derivative evaluation is only possible for this interval!
    const double xmax = 1;
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize + 1);
    static const double toleratedRelativeError = 1e-3;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + i * stepsize;

        // Compute analytic
        const double analyticDerivative = evaluatableObject.evaluate(x, derivationOrder);

        // Compute numeric derivative
        const double numericDerivative = evaluatableObject.evaluateNumericDerivative(x, derivationOrder);

        // Compute error
        const double error = fabs(analyticDerivative - numericDerivative);
        const double magnitude = fabs(analyticDerivative);
        double relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;

        // Check error
        if (relativeError > toleratedRelativeError) {
            std::cout << "Analytic and numeric derivative differ at position x=" << x;
            std::cout << " for derivation order d=" << derivationOrder;
            std::cout << " (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }
    }

    // Check invalid stepsize
#ifdef NDEBUG
    if (derivationOrder > 0) {
        double invalidDerivative = evaluatableObject.evaluateNumericDerivative(0.5, derivationOrder, -1.0);
        ASSERT_TRUE(invalidDerivative == 0);
    }
#endif

    // Test projection of numeric derivative
    ASSERT_TRUE(evaluatableObject.evaluateNumericDerivative(-1, derivationOrder) == evaluatableObject.evaluateNumericDerivative(0, derivationOrder));
    ASSERT_TRUE(evaluatableObject.evaluateNumericDerivative(2, derivationOrder) == evaluatableObject.evaluateNumericDerivative(1, derivationOrder));
}

//! Template function to compare analytic and numeric derivative of curves
template <class CurveType>
inline void compareAnalyticAndNumericDerivativeCurve(const CurveType& curve, const unsigned int& derivationOrder)
{
    compareAnalyticAndNumericDerivativeGeneric<CurveType>(curve, derivationOrder);
}

//! Template function to compare analytic and numeric derivative of a generic object with corresponding functionality
template <class EvaluatableObjectType>
inline void compareAnalyticAndNumericDerivativeGenericCustom(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    // Initialize helpers
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize - 1);
    static const double dx = 1e-6; // "Step-size" for numeric derivation

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + stepsize + i * stepsize;

        // Compute analytic
        const double analyticDerivative = evaluatableObject.evaluate(x, derivationOrder);

        // Compute numeric derivative
        const double leftSide = evaluatableObject.evaluate(x - dx, derivationOrder - 1);
        const double rightSide = evaluatableObject.evaluate(x + dx, derivationOrder - 1);
        const double numericDerivative = (rightSide - leftSide) / (2.0 * dx);

        // Compute error
        const double error = fabs(analyticDerivative - numericDerivative);
        const double magnitude = fabs(analyticDerivative);
        double relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;

        // Check error
        if (relativeError > toleratedRelativeError) {
            std::cout << "Analytic and numeric derivative (custom) differ at position x=" << x;
            std::cout << " for derivation order d=" << derivationOrder;
            std::cout << " (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }
    }
}

//! Template function to compare analytic and numeric derivative of curves (custom)
template <class CurveType>
inline void compareAnalyticAndNumericDerivativeCustomCurve(const CurveType& curve, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    compareAnalyticAndNumericDerivativeGenericCustom<CurveType>(curve, 0, 1, derivationOrder, toleratedRelativeError);
}

//! Template function to check high performance evaluation of generic objects providing a corresponding interface
template <class EvaluatableObjectType, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationGeneric(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax)
{
    // Initialize helpers
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize + 1);
    static const double toleratedRelativeError = 1e-9;
    broccoli::core::Time normalEvaluationTime = 0;
    broccoli::core::Time highPerformanceEvaluationTime = 0;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + i * stepsize;

        // Perform "normal" evaluation
        std::array<double, MaximumDerivation + 1> normalDi;
        broccoli::core::Time normalEvaluationStartTime = broccoli::core::Time::currentTime();
        for (unsigned int d = 0; d <= MaximumDerivation; d++)
            normalDi[d] = evaluatableObject.evaluate(x, d);
        normalEvaluationTime += broccoli::core::Time::currentTime() - normalEvaluationStartTime;

        // Perform "high-performance" evaluation
        broccoli::core::Time highPerformanceEvaluationStartTime = broccoli::core::Time::currentTime();
        const auto highPerformanceDi = evaluatableObject.template evaluateD0ToDN<MaximumDerivation>(x);
        highPerformanceEvaluationTime += broccoli::core::Time::currentTime() - highPerformanceEvaluationStartTime;

        // Check value and derivatives
        for (unsigned int d = 0; d <= MaximumDerivation; d++) {
            const double error = fabs(normalDi[d] - highPerformanceDi[d]);
            const double magnitude = fabs(normalDi[d]);
            double relativeError = error;
            if (magnitude > 1e-9)
                relativeError = error / magnitude;
            if (relativeError > toleratedRelativeError) {
                std::cout << "High performance evaluation (evaluateD0ToDN()) differs at position x=" << x;
                std::cout << " for derivation order d=" << d;
                std::cout << " (absolute error: " << error;
                std::cout << ") (relative error : " << relativeError << ")\n";
                ASSERT_LT(relativeError, toleratedRelativeError);
            }
        }
    }

    // Print time measurements to console
    const double speedImprovements = normalEvaluationTime.toDouble() / highPerformanceEvaluationTime.toDouble();
    std::cout << "High performance evaluation (evaluateD0ToDN()) with N=" << MaximumDerivation << " is " << speedImprovements << " times faster than normal evaluation (" << MaximumDerivation + 1 << "x evaluate())." << std::endl;
}

//! Template function to check high performance evaluation of curves
template <class CurveType, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationCurve(const CurveType& curve)
{
    checkHighPerformanceEvaluationGeneric<CurveType, MaximumDerivation>(curve, 0, 1);
}

//! Template function for plotting generic objects with corresponding evaluate function (using gnuplot)
template <class EvaluatableObjectType>
inline void plotGeneric(const EvaluatableObjectType& evaluatableObject, const double& xmin, const double& xmax, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    gnuplotFile << "plot '" << fileName << ".dat' using 1:2 w l title \"Base function\"\n";
    for (unsigned int d = 1; d <= maximumDerivation; d++)
        gnuplotFile << "replot '" << fileName << ".dat' using 1:" << (2 + d) << " w l title \"Derivative of order " << d << "\"\n";
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    const size_t steps = std::floor((xmax - xmin) / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = xmin + i * stepSize;
        dataFile << x << " ";

        // Evaluate base function
        dataFile << evaluatableObject.evaluate(x, 0);

        // Evaluate derivative
        for (unsigned int d = 1; d <= maximumDerivation; d++)
            dataFile << " " << evaluatableObject.evaluate(x, d);

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Template function for plotting curves (using gnuplot)
template <class CurveType>
inline void plotCurve(const CurveType& curve, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    plotGeneric<CurveType>(curve, 0, 1, stepSize, maximumDerivation, fileName);
}

//! Checks encodeToXML() of curves
template <class CurveType>
inline void checkEncodeToXMLCurve(const CurveType& curve)
{
    broccoli::io::encoding::CharacterStream stream;
    broccoli::io::encoding::CharacterStreamSize streamSize = curve.encodeToXML(stream, 1, 4);
    ASSERT_GT(streamSize, 0);
    ASSERT_EQ(streamSize, stream.size());
}
