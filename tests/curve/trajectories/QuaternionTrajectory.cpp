/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/trajectories/QuaternionTrajectory.hpp"
#include "QuaternionTrajectoryHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test string representation of the given result type for quaternion-trajectory algorithms
TEST(QuaternionTrajectory, QuaternionTrajectoryResultString)
{
    uint8_t i;
    QuaternionTrajectoryResult testResult;
    for (i = 0; i < static_cast<uint8_t>(QuaternionTrajectoryResult::QUATERNIONTRAJECTORYRESULT_COUNT); i++) {
        testResult = static_cast<QuaternionTrajectoryResult>(i);
        ASSERT_GT(quaternionTrajectoryResultString(testResult).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(quaternionTrajectoryResultString(testResult) == "UNKNOWN");
        } else {
            ASSERT_TRUE(quaternionTrajectoryResultString(testResult) != "UNKNOWN");
        }
    }

#ifdef NDEBUG
    // Check out of bounds
    i = static_cast<uint8_t>(QuaternionTrajectoryResult::QUATERNIONTRAJECTORYRESULT_COUNT);
    testResult = static_cast<QuaternionTrajectoryResult>(i);
    ASSERT_TRUE(quaternionTrajectoryResultString(testResult) == "UNKNOWN");
#endif // NDEBUG
}

#endif // HAVE_EIGEN3
