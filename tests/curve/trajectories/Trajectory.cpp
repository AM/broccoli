/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/trajectories/Trajectory.hpp"
#include "TrajectoryHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test string representation of the given result type for trajectory algorithms
TEST(Trajectory, TrajectoryResultToString)
{
    uint8_t i;
    TrajectoryResult testResult;
    for (i = 0; i < static_cast<uint8_t>(TrajectoryResult::TRAJECTORYRESULT_COUNT); i++) {
        testResult = static_cast<TrajectoryResult>(i);
        ASSERT_GT(trajectoryResultString(testResult).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(trajectoryResultString(testResult) == "UNKNOWN");
        } else {
            ASSERT_TRUE(trajectoryResultString(testResult) != "UNKNOWN");
        }
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(TrajectoryResult::TRAJECTORYRESULT_COUNT);
    testResult = static_cast<TrajectoryResult>(i);
    ASSERT_TRUE(trajectoryResultString(testResult) == "UNKNOWN");
#endif
}
