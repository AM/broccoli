/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "../splines/QuaternionSplineHelpers.hpp"
#include "broccoli/curve/splines/PolynomialSpline.hpp"
#include "broccoli/curve/trajectories/QuaternionBSplineTrajectory.hpp"
#include "broccoli/curve/trajectories/QuaternionTrajectory.hpp"
#include "gtest/gtest.h"

//! Helper function to compare quaternions
inline bool areQuaternionsEquivalent(const Eigen::Quaterniond& first, const Eigen::Quaterniond& second)
{
    // Orientations are equivalent if q1 = q2 OR q1 = -q2
    return (first.isApprox(second) || first.isApprox(Eigen::Quaterniond(-second.w(), -second.x(), -second.y(), -second.z())));
}

//! Template function to check isValid() of QuaternionTrajectory
template <class TrajectoryType, class ParameterCurveType, class QuaternionCurveType>
inline void checkQuaternionTrajectoryIsValidFixedSize()
{
    // Initialize helpers
    broccoli::curve::QuaternionTrajectoryResult quaternionTrajectoryResult = broccoli::curve::QuaternionTrajectoryResult::UNKNOWN;
    broccoli::curve::SplineResult parameterSplineResult = broccoli::curve::SplineResult::UNKNOWN;
    broccoli::curve::QuaternionSplineResult quaternionSplineResult = broccoli::curve::QuaternionSplineResult::UNKNOWN;

    // Check invalid parameter spline
    TrajectoryType trajectoryA;
    trajectoryA.m_quaternionSpline.m_segments.resize(1);
    trajectoryA.m_quaternionSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryA.m_duration = 1.0;
    ASSERT_EQ(trajectoryA.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), false);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::ERROR_INVALID_TRAJECTORY_PARAMETERSPLINE);
    ASSERT_NE(parameterSplineResult, broccoli::curve::SplineResult::SUCCESS);

    // Check invalid quaternion spline
    TrajectoryType trajectoryB;
    trajectoryB.m_parameterSpline.m_segments.resize(1);
    trajectoryB.m_parameterSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_duration = 1.0;
    ASSERT_EQ(trajectoryB.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), false);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::ERROR_INVALID_TRAJECTORY_QUATERNIONSPLINE);
    ASSERT_NE(quaternionSplineResult, broccoli::curve::QuaternionSplineResult::SUCCESS);

    // Check invalid duration
    TrajectoryType trajectoryC;
    trajectoryC.m_parameterSpline.m_segments.resize(1);
    trajectoryC.m_parameterSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryC.m_quaternionSpline.m_segments.resize(1);
    trajectoryC.m_quaternionSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryC.m_duration = -1.0;
    ASSERT_EQ(trajectoryC.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), false);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::ERROR_INVALID_TRAJECTORY_DURATION_NEGATIVE);

    // Check success
    TrajectoryType trajectoryD;
    trajectoryD.m_parameterSpline.m_segments.resize(1);
    trajectoryD.m_parameterSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryD.m_quaternionSpline.m_segments.resize(1);
    trajectoryD.m_quaternionSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryD.m_duration = 1.0;
    ASSERT_EQ(trajectoryD.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), true);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::SUCCESS);
    ASSERT_EQ(parameterSplineResult, broccoli::curve::SplineResult::SUCCESS);
    ASSERT_EQ(quaternionSplineResult, broccoli::curve::QuaternionSplineResult::SUCCESS);
}

//! Template function to check isValid() of QuaternionBSplineTrajectory
template <unsigned int Degree>
inline void checkQuaternionTrajectoryIsValidBSpline()
{
    // Initialize helpers
    broccoli::curve::QuaternionTrajectoryResult quaternionTrajectoryResult = broccoli::curve::QuaternionTrajectoryResult::UNKNOWN;
    broccoli::curve::SplineResult parameterSplineResult = broccoli::curve::SplineResult::UNKNOWN;
    broccoli::curve::QuaternionSplineResult quaternionSplineResult = broccoli::curve::QuaternionSplineResult::UNKNOWN;
    static constexpr int p = Degree;
    static constexpr int k = p + 1;
    static constexpr int m = k + 3;

    // Check invalid parameter spline
    broccoli::curve::QuaternionBSplineTrajectory<broccoli::curve::PolynomialSpline<5>, Degree, m> trajectoryA;
    trajectoryA.m_quaternionSpline.m_segments.resize(1);
    trajectoryA.m_quaternionSpline.m_segments[0].m_basis.setClampedUniformKnots(m);
    trajectoryA.m_quaternionSpline.m_segments[0].m_controlPoints.resize(m, Eigen::Quaterniond(1, 0, 0, 0));
    trajectoryA.m_quaternionSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryA.m_duration = 1.0;
    ASSERT_EQ(trajectoryA.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), false);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::ERROR_INVALID_TRAJECTORY_PARAMETERSPLINE);
    ASSERT_NE(parameterSplineResult, broccoli::curve::SplineResult::SUCCESS);

    // Check invalid quaternion spline
    broccoli::curve::QuaternionBSplineTrajectory<broccoli::curve::PolynomialSpline<5>, Degree, m> trajectoryB;
    trajectoryB.m_parameterSpline.m_segments.resize(1);
    trajectoryB.m_parameterSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_duration = 1.0;
    ASSERT_EQ(trajectoryB.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), false);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::ERROR_INVALID_TRAJECTORY_QUATERNIONSPLINE);
    ASSERT_NE(quaternionSplineResult, broccoli::curve::QuaternionSplineResult::SUCCESS);

    // Check invalid duration
    broccoli::curve::QuaternionBSplineTrajectory<broccoli::curve::PolynomialSpline<5>, Degree, m> trajectoryC;
    trajectoryC.m_parameterSpline.m_segments.resize(1);
    trajectoryC.m_parameterSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryC.m_quaternionSpline.m_segments.resize(1);
    trajectoryC.m_quaternionSpline.m_segments[0].m_basis.setClampedUniformKnots(m);
    trajectoryC.m_quaternionSpline.m_segments[0].m_controlPoints.resize(m, Eigen::Quaterniond(1, 0, 0, 0));
    trajectoryC.m_quaternionSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryC.m_duration = -1.0;
    ASSERT_EQ(trajectoryC.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), false);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::ERROR_INVALID_TRAJECTORY_DURATION_NEGATIVE);

    // Check success
    broccoli::curve::QuaternionBSplineTrajectory<broccoli::curve::PolynomialSpline<5>, Degree, m> trajectoryD;
    trajectoryD.m_parameterSpline.m_segments.resize(1);
    trajectoryD.m_parameterSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryD.m_quaternionSpline.m_segments.resize(1);
    trajectoryD.m_quaternionSpline.m_segments[0].m_basis.setClampedUniformKnots(m);
    trajectoryD.m_quaternionSpline.m_segments[0].m_controlPoints.resize(m, Eigen::Quaterniond(1, 0, 0, 0));
    trajectoryD.m_quaternionSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryD.m_duration = 1.0;
    trajectoryD.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult);
    ASSERT_EQ(trajectoryD.isValid(&quaternionTrajectoryResult, &parameterSplineResult, &quaternionSplineResult), true);
    ASSERT_EQ(quaternionTrajectoryResult, broccoli::curve::QuaternionTrajectoryResult::SUCCESS);
    ASSERT_EQ(parameterSplineResult, broccoli::curve::SplineResult::SUCCESS);
    ASSERT_EQ(quaternionSplineResult, broccoli::curve::QuaternionSplineResult::SUCCESS);
}

//! Template function to test quaternion trajectory operators
template <class TrajectoryType>
inline void checkOperatorsQuaternionTrajectory()
{
    // Construct splines
    TrajectoryType trajectoryA;
    TrajectoryType trajectoryB; // same
    TrajectoryType trajectoryC; // different parameter spline
    TrajectoryType trajectoryD; // different quaternion spline
    trajectoryA.m_parameterSpline.m_segments.resize(2);
    trajectoryA.m_parameterSpline.m_segmentProportions = std::vector<double>(2, 0.5);
    trajectoryA.m_quaternionSpline.m_segments.resize(4);
    trajectoryA.m_quaternionSpline.m_segmentProportions = std::vector<double>(4, 0.25);
    trajectoryB = trajectoryA;
    trajectoryC.m_parameterSpline.m_segments.resize(1);
    trajectoryC.m_parameterSpline.m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryC.m_quaternionSpline = trajectoryA.m_quaternionSpline;
    trajectoryD.m_parameterSpline = trajectoryA.m_parameterSpline;
    trajectoryD.m_quaternionSpline.m_segments.resize(2);
    trajectoryD.m_quaternionSpline.m_segmentProportions = std::vector<double>(2, 0.5);

    // Equality operators
    ASSERT_TRUE(trajectoryA == trajectoryB);
    ASSERT_FALSE(trajectoryA != trajectoryB);
    ASSERT_FALSE(trajectoryA == trajectoryC);
    ASSERT_TRUE(trajectoryA != trajectoryC);
    ASSERT_FALSE(trajectoryA == trajectoryD);
    ASSERT_TRUE(trajectoryA != trajectoryD);
}

//! Template function to check getParameterSplineSegmentBoundaries() of quaternion trajectories
template <class TrajectoryType>
inline void checkGetParameterSplineSegmentBoundariesQuaternionTrajectory(const TrajectoryType& trajectory)
{
    // Get boundaries
    std::vector<double> segmentBoundaries = trajectory.getParameterSplineSegmentBoundaries();
    ASSERT_EQ(segmentBoundaries.size(), trajectory.m_parameterSpline.m_segmentProportions.size() + 1);

    // Check values
    double currentPosition = 0;
    for (size_t i = 0; i < trajectory.m_parameterSpline.m_segmentProportions.size(); i++) {
        ASSERT_TRUE(fabs(segmentBoundaries[i] - currentPosition) < 1e-9);
        currentPosition += trajectory.m_parameterSpline.m_segmentProportions[i] * trajectory.m_duration;
    }
    ASSERT_TRUE(fabs(segmentBoundaries.back() - trajectory.m_duration) < 1e-6);
}

//! Template function to check encodeToXML() of QuaternionTrajectory
template <class TrajectoryType>
inline void checkEncodeToXMLQuaternionTrajectory()
{
    // Initialize container
    TrajectoryType trajectory;
    trajectory.m_parameterSpline.m_segments.resize(2);
    trajectory.m_parameterSpline.m_segmentProportions = std::vector<double>(2, 0.5);
    trajectory.m_quaternionSpline.m_segments.resize(2);
    trajectory.m_quaternionSpline.m_segmentProportions = std::vector<double>(2, 0.5);
    trajectory.m_duration = 1.23;

    // Initialize helpers
    broccoli::io::encoding::CharacterStream stream;
    broccoli::io::encoding::CharacterStreamSize streamSize = trajectory.encodeToXML(stream, 1, 4);
    ASSERT_GT(streamSize, 0);
    ASSERT_EQ(streamSize, stream.size());
}

//! Template function to compare analytic and numeric derivative of quaternion trajectorys (custom)
template <class TrajectoryType>
inline void compareAnalyticAndNumericDerivativeCustomQuaternionTrajectory(const TrajectoryType& trajectory, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    compareAnalyticAndNumericDerivativeGenericCustomQuaternion<TrajectoryType>(trajectory, 0.1 * trajectory.m_duration, 0.9 * trajectory.m_duration, derivationOrder, toleratedRelativeError);
}

//! Template function to check high performance evaluation
template <class TrajectoryType, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationQuaternionTrajectory(const TrajectoryType& trajectory)
{
    checkHighPerformanceEvaluationGenericQuaternion<TrajectoryType, MaximumDerivation>(trajectory, 0, trajectory.m_duration);
}

//! Template function to check high performance evaluation of angular velocity and acceleration
template <class TrajectoryType>
inline void checkHighPerformanceEvaluationQuaternionTrajectoryAngularVelocityAcceleration(const TrajectoryType& trajectory)
{
    // Initialize helpers
    const double xmin = 0;
    const double xmax = trajectory.m_duration;
    const double stepsize = 0.001 * (xmax - xmin);
    size_t steps = std::floor((xmax - xmin) / stepsize + 1);
    static const double toleratedRelativeError = 1e-9;
    broccoli::core::Time normalEvaluationTime = 0;
    broccoli::core::Time highPerformanceEvaluationTime = 0;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = xmin + i * stepsize;

        // Perform "normal" evaluation
        Eigen::Quaterniond normalD0;
        Eigen::Vector3d normalV;
        Eigen::Vector3d normalA;
        broccoli::core::Time normalEvaluationStartTime = broccoli::core::Time::currentTime();
        normalD0 = trajectory.evaluate(x, 0);
        normalV = trajectory.evaluateAngularVelocity(x);
        normalA = trajectory.evaluateAngularAcceleration(x);
        normalEvaluationTime += broccoli::core::Time::currentTime() - normalEvaluationStartTime;

        // Perform "high-performance" evaluation for value and angular velocity
        Eigen::Quaterniond highPerformanceD0V_D0;
        Eigen::Vector3d highPerformanceD0V_V;
        trajectory.evaluateD0V(x, highPerformanceD0V_D0, highPerformanceD0V_V);

        // Perform "high-performance" evaluation for value, angular velocity and acceleration
        broccoli::core::Time highPerformanceEvaluationStartTime = broccoli::core::Time::currentTime();
        Eigen::Quaterniond highPerformanceD0VA_D0;
        Eigen::Vector3d highPerformanceD0VA_V;
        Eigen::Vector3d highPerformanceD0VA_A;
        trajectory.evaluateD0VA(x, highPerformanceD0VA_D0, highPerformanceD0VA_V, highPerformanceD0VA_A);
        highPerformanceEvaluationTime += broccoli::core::Time::currentTime() - highPerformanceEvaluationStartTime;

        // Check value for evaluateD0V
        double error = (normalD0.coeffs() - highPerformanceD0V_D0.coeffs()).norm();
        double magnitude = normalD0.norm();
        double relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;
        if (relativeError > toleratedRelativeError) {
            std::cout << "High performance evaluation (evaluateD0V()) differs at position x=" << x;
            std::cout << " for base value D0 (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }

        // Check angular velocity for evaluateD0V
        error = (normalV - highPerformanceD0V_V).norm();
        magnitude = normalV.norm();
        relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;
        if (relativeError > toleratedRelativeError) {
            std::cout << "High performance evaluation (evaluateD0V()) differs at position x=" << x;
            std::cout << " for angular velocity V (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }

        // Check value for evaluateD0VA
        error = (normalD0.coeffs() - highPerformanceD0VA_D0.coeffs()).norm();
        magnitude = normalD0.norm();
        relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;
        if (relativeError > toleratedRelativeError) {
            std::cout << "High performance evaluation (evaluateD0VA()) differs at position x=" << x;
            std::cout << " for base value D0 (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }

        // Check angular velocity for evaluateD0VA
        error = (normalV - highPerformanceD0VA_V).norm();
        magnitude = normalV.norm();
        relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;
        if (relativeError > toleratedRelativeError) {
            std::cout << "High performance evaluation (evaluateD0VA()) differs at position x=" << x;
            std::cout << " for angular velocity V (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }

        // Check angular acceleration for evaluateD0VA
        error = (normalA - highPerformanceD0VA_A).norm();
        magnitude = normalA.norm();
        relativeError = error;
        if (magnitude > 1e-9)
            relativeError = error / magnitude;
        if (relativeError > toleratedRelativeError) {
            std::cout << "High performance evaluation (evaluateD0VA()) differs at position x=" << x;
            std::cout << " for angular acceleration A (absolute error: " << error;
            std::cout << ") (relative error : " << relativeError << ")\n";
            ASSERT_LT(relativeError, toleratedRelativeError);
        }
    }

    // Print time measurements to console
    const double speedImprovements = normalEvaluationTime.toDouble() / highPerformanceEvaluationTime.toDouble();
    std::cout << "High performance evaluation (evaluateD0VA()) is " << speedImprovements << " times faster than normal evaluation (evaluate() + evaluateAngularVelocity() + evaluateAngularAcceleration())." << std::endl;
}

//! Template function for plotting quaternion trajectorys in 2D (using gnuplot)
template <class TrajectoryType>
inline void plotQuaternionTrajectory2D(const TrajectoryType& trajectory, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    plotGenericQuaternion2D<TrajectoryType>(trajectory, 0, trajectory.m_duration, stepSize, maximumDerivation, fileName);
}

//! Template function for plotting quaternion trajectorys in 3D (using gnuplot)
template <class TrajectoryType>
inline void plotQuaternionTrajectory3D(const TrajectoryType& trajectory, const double& stepSize, const std::string& fileName)
{
    plotGenericQuaternion3D<TrajectoryType>(trajectory, 0, trajectory.m_duration, stepSize, fileName);
}

#endif // HAVE_EIGEN3
