/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "../splines/SplineHelpers.hpp"
#include "broccoli/curve/trajectories/Trajectory.hpp"
#include "broccoli/curve/trajectories/TrajectorySplitter.hpp"
#include "gtest/gtest.h"

//! Template function to check isValid() of trajectory
template <class TrajectoryType, class CurveType>
inline void checkIsValidTrajectory()
{
    // Initialize helpers
    broccoli::curve::TrajectoryResult trajectoryResult = broccoli::curve::TrajectoryResult::UNKNOWN;
    broccoli::curve::SplineResult splineResult = broccoli::curve::SplineResult::UNKNOWN;
    TrajectoryType trajectory;
    trajectory.m_splines[0].m_segments.push_back(CurveType());
    trajectory.m_splines[0].m_segmentProportions.push_back(1);

    // Check, no segments
    ASSERT_EQ(trajectory.isValid(&trajectoryResult, &splineResult), false);
    ASSERT_EQ(trajectoryResult, broccoli::curve::TrajectoryResult::ERROR_INVALID_TRAJECTORY_SPLINES);
    ASSERT_EQ(splineResult, broccoli::curve::SplineResult::ERROR_INVALID_SPLINE_NO_SEGMENTS);

    // Check success
    trajectory.m_splines[1].m_segments.push_back(CurveType());
    trajectory.m_splines[1].m_segmentProportions.push_back(1);
    ASSERT_EQ(trajectory.isValid(&trajectoryResult, &splineResult), true);
    ASSERT_EQ(trajectoryResult, broccoli::curve::TrajectoryResult::SUCCESS);
    ASSERT_EQ(splineResult, broccoli::curve::SplineResult::SUCCESS);

    // Check, invalid duration
    trajectory.m_duration = -1;
    ASSERT_EQ(trajectory.isValid(&trajectoryResult, &splineResult), false);
    ASSERT_EQ(trajectoryResult, broccoli::curve::TrajectoryResult::ERROR_INVALID_TRAJECTORY_DURATION_NEGATIVE);
    ASSERT_EQ(splineResult, broccoli::curve::SplineResult::UNKNOWN);
}

//! Template function to check operators of trajectory
template <class TrajectoryType, class CurveType>
inline void checkOperatorsTrajectory()
{
    // Initialize container
    TrajectoryType trajectoryA;
    trajectoryA.m_splines[0].m_segments.push_back(CurveType());
    for (size_t i = 0; i < trajectoryA.m_splines[0].m_segments[0].m_coefficients.size(); i++)
        trajectoryA.m_splines[0].m_segments[0].m_coefficients[i] = 1.23 + i * 3.456;

    // Inequality (splines)
    TrajectoryType trajectoryB = trajectoryA;
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[0] += 1;
    ASSERT_TRUE(trajectoryA != trajectoryB);

    // Inequality (duration)
    TrajectoryType trajectoryC = trajectoryA;
    trajectoryC.m_duration += 1;
    ASSERT_TRUE(trajectoryA != trajectoryC);

    // Equality
    TrajectoryType trajectoryD = trajectoryA;
    ASSERT_TRUE(trajectoryA == trajectoryD);
}

//! Template function to check getSegmentBoundaries() of trajectories
template <class TrajectoryType, unsigned int dimension>
inline void checkGetSegmentBoundariesTrajectory(const TrajectoryType& trajectory)
{
    // Get boundaries
    std::array<std::vector<double>, dimension> segmentBoundaries = trajectory.getSegmentBoundaries();

    // Pass through dimensions
    for (unsigned int d = 0; d < dimension; d++) {
        ASSERT_EQ(segmentBoundaries[d].size(), trajectory.m_splines[d].m_segmentProportions.size() + 1);

        // Check values
        double currentPosition = 0;
        for (size_t i = 0; i < trajectory.m_splines[d].m_segmentProportions.size(); i++) {
            ASSERT_TRUE(fabs(segmentBoundaries[d][i] - currentPosition) < 1e-9);
            currentPosition += trajectory.m_splines[d].m_segmentProportions[i] * trajectory.m_duration;
        }
        ASSERT_TRUE(fabs(segmentBoundaries[d].back() - trajectory.m_duration) < 1e-6);
    }
}

//! Template function to check encodeToXML() of trajectory
template <class TrajectoryType, class CurveType>
inline void checkEncodeToXMLTrajetory()
{
    // Initialize container
    TrajectoryType trajectory;
    trajectory.m_splines[0].m_segments.push_back(CurveType());

    // Initialize helpers
    broccoli::io::encoding::CharacterStream stream;
    broccoli::io::encoding::CharacterStreamSize streamSize = trajectory.encodeToXML(stream, 1, 4);
    ASSERT_GT(streamSize, 0);
    ASSERT_EQ(streamSize, stream.size());
}

//! Template function to compare analytic and numeric derivative of a trajectory
template <class TrajectoryType, unsigned int dimension>
inline void compareAnalyticAndNumericDerivativeTrajectoryCustom(const TrajectoryType& trajectory, const unsigned int& derivationOrder, const double& toleratedRelativeError = 1e-3)
{
    // Initialize helpers
    const double stepsize = 0.001 * trajectory.m_duration;
    size_t steps = std::floor(trajectory.m_duration / stepsize + 1) - 2;
    static const double dx = 1e-6; // "Step-size" for numeric derivation

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = (i + 1) * stepsize;

        // Compute analytic
        const std::array<double, dimension> analyticDerivative = trajectory.evaluate(x, derivationOrder);

        // Compute numeric derivative
        const std::array<double, dimension> leftSide = trajectory.evaluate(x - dx, derivationOrder - 1);
        const std::array<double, dimension> rightSide = trajectory.evaluate(x + dx, derivationOrder - 1);
        std::array<double, dimension> numericDerivative;
        for (unsigned int j = 0; j < dimension; j++)
            numericDerivative[j] = (rightSide[j] - leftSide[j]) / (2.0 * dx);

        // Compute error
        std::array<double, dimension> error;
        std::array<double, dimension> magnitude;
        std::array<double, dimension> relativeError;
        for (unsigned int j = 0; j < dimension; j++) {
            error[j] = fabs(analyticDerivative[j] - numericDerivative[j]);
            magnitude[j] = fabs(analyticDerivative[j]);
            relativeError[j] = error[j];
            if (magnitude[j] > 1e-9)
                relativeError[j] = error[j] / magnitude[j];

            // Check error
            if (relativeError[j] > toleratedRelativeError) {
                std::cout << "Analytic and numeric derivative (custom) differ for dimension n=" << j << " at position x=" << x;
                std::cout << " for derivation order d=" << derivationOrder;
                std::cout << " (absolute error: " << error[j];
                std::cout << ") (relative error : " << relativeError[j] << ")\n";
                ASSERT_LT(relativeError[j], toleratedRelativeError);
            }
        }
    }
}

//! Template function to check high performance evaluation of trajectory
template <class TrajectoryType, unsigned int dimension, unsigned int MaximumDerivation>
inline void checkHighPerformanceEvaluationTrajectory(const TrajectoryType& trajectory)
{
    // Initialize helpers
    const double stepsize = 0.001 * trajectory.m_duration;
    size_t steps = std::floor(trajectory.m_duration / stepsize + 1);
    static const double toleratedRelativeError = 1e-9;
    broccoli::core::Time normalEvaluationTime = 0;
    broccoli::core::Time highPerformanceEvaluationTime = 0;

    // Loop over timesteps
    for (size_t i = 0; i < steps; i++) {
        // Compute current position
        const double x = i * stepsize;

        // Perform "normal" evaluation
        std::array<std::array<double, MaximumDerivation + 1>, dimension> normalDi;
        broccoli::core::Time normalEvaluationStartTime = broccoli::core::Time::currentTime();
        for (unsigned int d = 0; d <= MaximumDerivation; d++) {
            const std::array<double, dimension> Di = trajectory.evaluate(x, d);
            for (unsigned int j = 0; j < dimension; j++)
                normalDi[j][d] = Di[j];
        }
        normalEvaluationTime += broccoli::core::Time::currentTime() - normalEvaluationStartTime;

        // Perform "high-performance" evaluation
        broccoli::core::Time highPerformanceEvaluationStartTime = broccoli::core::Time::currentTime();
        const auto highPerformanceDi = trajectory.template evaluateD0ToDN<MaximumDerivation>(x);
        highPerformanceEvaluationTime += broccoli::core::Time::currentTime() - highPerformanceEvaluationStartTime;

        // Check value and derivatives
        for (unsigned int j = 0; j < dimension; j++) {
            for (unsigned int d = 0; d <= MaximumDerivation; d++) {
                const double error = fabs(normalDi[j][d] - highPerformanceDi[j][d]);
                const double magnitude = fabs(normalDi[j][d]);
                double relativeError = error;
                if (magnitude > 1e-9)
                    relativeError = error / magnitude;
                if (relativeError > toleratedRelativeError) {
                    std::cout << "High performance evaluation (evaluateD0ToDN()) differs for dimension n=" << j << " at position x=" << x;
                    std::cout << " for derivation order d=" << d;
                    std::cout << " (absolute error: " << error;
                    std::cout << ") (relative error : " << relativeError << ")\n";
                    ASSERT_LT(relativeError, toleratedRelativeError);
                }
            }
        }
    }

    // Print time measurements to console
    const double speedImprovements = normalEvaluationTime.toDouble() / highPerformanceEvaluationTime.toDouble();
    std::cout << "High performance evaluation (evaluateD0ToDN()) with N=" << MaximumDerivation << " is " << speedImprovements << " times faster than normal evaluation (" << MaximumDerivation + 1 << "x evaluate())." << std::endl;
}

//! Template function for plotting trajectories (using gnuplot)
template <class TrajectoryType, unsigned int dimension>
inline void plotTrajectory(const TrajectoryType& trajectory, const double& stepSize, const unsigned int& maximumDerivation, const std::string& fileName)
{
    // Writing gnuplot file
    std::ofstream gnuplotFile;
    gnuplotFile.open(fileName + ".gp");
    for (unsigned int d = 0; d <= maximumDerivation; d++) {
        for (unsigned int j = 0; j < dimension; j++) {
            if (j > 0 || d > 0)
                gnuplotFile << "re";
            gnuplotFile << "plot '" << fileName << ".dat' using 1:" << (2 + j + d * dimension) << " w l title \"d^" << d << " y[" << j << "] / dx^" << d << "\"\n";
        }
    }
    gnuplotFile.close();

    // Writing data file
    std::ofstream dataFile;
    dataFile.open(fileName + ".dat");
    size_t steps = std::floor(trajectory.m_duration / stepSize + 1);
    for (size_t i = 0; i < steps; i++) {
        // Compute position
        const double x = i * stepSize;
        dataFile << x << " ";

        // Evaluate value and derivatives
        for (unsigned int d = 0; d <= maximumDerivation; d++) {
            std::array<double, dimension> Di = trajectory.evaluate(x, d);
            for (unsigned int j = 0; j < dimension; j++)
                dataFile << " " << Di[j];
        }

        // Switch to next data set
        dataFile << "\n";
    }
    dataFile.close();
}

//! Template function to check splitting and joining trajectories
template <class SplitterType, class TrajectoryType, class SplineType, class CurveType, unsigned int dimension, unsigned int maximumDerivative>
inline void checkSplitAndJoinTrajectory(const TrajectoryType& trajectory, const double& tolerance = 1e-6)
{
    // Initialize helpers
    broccoli::curve::TrajectoryResult trajectoryResult = broccoli::curve::TrajectoryResult::UNKNOWN;
    broccoli::curve::SplineResult splineResult = broccoli::curve::SplineResult::UNKNOWN;

    // Setup test cases
    std::vector<std::vector<double>> testCaseProportions;
    testCaseProportions.push_back(std::vector<double>{ 1.0 });
    testCaseProportions.push_back(std::vector<double>{ 0.5, 0.5 });
    testCaseProportions.push_back(std::vector<double>{ 1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0 });
    testCaseProportions.push_back(std::vector<double>{ 0.1, 0.2, 0.3, 0.4 });
    testCaseProportions.push_back(std::vector<double>{ 0.4, 0.3, 0.2, 0.1 });
    testCaseProportions.push_back(std::vector<double>{ 1.0 / 5.0, 3.0 / 5.0, 1.0 / 5.0 });
    testCaseProportions.push_back(std::vector<double>{ 0.0, 0.5, 0.5 });
    testCaseProportions.push_back(std::vector<double>{ 0.5, 0.0, 0.5 });
    testCaseProportions.push_back(std::vector<double>{ 0.5, 0.5, 0.0 });

    // Iterate through test cases
    for (size_t i = 0; i < testCaseProportions.size(); i++) {
        // 1. TEST SPLITTING
        // -----------------
        // Initialize helpers
        std::vector<TrajectoryType> splittedTrajectories;
        std::vector<TrajectoryType*> splittedTrajectoryPointers;
        std::vector<TrajectoryType const*> splittedTrajectoryPointersConst;

        // Initialize intermediate trajectories
        splittedTrajectories.resize(testCaseProportions[i].size());
        for (size_t j = 0; j < splittedTrajectories.size(); j++) {
            splittedTrajectories[j].m_duration = trajectory.m_duration * testCaseProportions[i][j];
            splittedTrajectoryPointers.push_back(&(splittedTrajectories[j]));
            splittedTrajectoryPointersConst.push_back(&(splittedTrajectories[j]));
        }

        // Perform split
        const bool splitResult = SplitterType::split(trajectory, splittedTrajectoryPointers, false, &trajectoryResult, &splineResult);
        ASSERT_EQ(splitResult, true);
        ASSERT_EQ(trajectoryResult, broccoli::curve::TrajectoryResult::SUCCESS);
        ASSERT_EQ(splineResult, broccoli::curve::SplineResult::SUCCESS);

        // 2. TEST JOINING
        // ---------------
        // Initialize helpers
        TrajectoryType joinedTrajectory; // Trajectory after splitting and rejoining
        joinedTrajectory.m_duration = trajectory.m_duration;

        // Perform join
        const bool joinResult = SplitterType::join(joinedTrajectory, splittedTrajectoryPointersConst, false, &trajectoryResult, &splineResult);
        ASSERT_EQ(joinResult, true);
        ASSERT_EQ(trajectoryResult, broccoli::curve::TrajectoryResult::SUCCESS);
        ASSERT_EQ(splineResult, broccoli::curve::SplineResult::SUCCESS);

        // 3. TEST RESULT
        // --------------
        double t = 0;
        const double dt = trajectory.m_duration * 1e-2;
        while (t <= trajectory.m_duration) {
            for (unsigned int d = 0; d <= maximumDerivative; d++) {
                // Evaluate original trajectory
                const std::array<double, dimension> originalDi = trajectory.evaluate(t, d);

                // Evaluate re-joined trajectory
                const std::array<double, dimension> rejoinedDi = joinedTrajectory.evaluate(t, d);

                // Compare values
                for (unsigned int j = 0; j < dimension; j++)
                    ASSERT_LE(fabs(originalDi[j] - rejoinedDi[j]), tolerance);
            }

            // Update time
            t += dt;
        }
    }
}
