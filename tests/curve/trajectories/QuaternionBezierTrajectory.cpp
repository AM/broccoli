/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/trajectories/QuaternionBezierTrajectory.hpp"
#include "QuaternionTrajectoryHelpers.hpp"
#include "broccoli/curve/splines/PolynomialSpline.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of quaternion Bezier trajectories
TEST(QuaternionBezierTrajectory, BasicUsage)
{
    // Check validity
    checkQuaternionTrajectoryIsValidFixedSize<QuaternionBezierTrajectory<PolynomialSpline<5>>, PolynomialCurve<5>, QuaternionBezierCurve>();

    // Check operators
    checkOperatorsQuaternionTrajectory<QuaternionBezierTrajectory<PolynomialSpline<5>>>();

    // Construct
    QuaternionBezierTrajectory<PolynomialSpline<5>> trajectory;
    trajectory.m_duration = 1.23;
    ASSERT_TRUE(trajectory.m_parameterSpline.interpolate(std::vector<double>{ 0, 1, 0, 0, 0, 0 }, nullptr, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES));
    ASSERT_TRUE(trajectory.m_quaternionSpline.interpolate(keyFramesSetAQuaternionInterpolation(), nullptr, QuaternionSplineInterpolationMethod::BEZIER_SMOOTH));

    // Check segment boundaries
    checkGetParameterSplineSegmentBoundariesQuaternionTrajectory<decltype(trajectory)>(trajectory);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeCustomQuaternionTrajectory<decltype(trajectory)>(trajectory, 1);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionTrajectory<decltype(trajectory), 2>(trajectory);
    checkHighPerformanceEvaluationQuaternionTrajectoryAngularVelocityAcceleration<decltype(trajectory)>(trajectory);

    // Check encoding to XML
    checkEncodeToXMLQuaternionTrajectory<QuaternionBezierTrajectory<PolynomialSpline<5>>>();
}

#endif // HAVE_EIGEN3
