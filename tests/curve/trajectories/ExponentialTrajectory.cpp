/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/trajectories/ExponentialTrajectory.hpp"
#include "TrajectoryHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of exponential trajectories
TEST(ExponentialTrajectory, BasicUsage)
{
    // Check validity
    checkIsValidTrajectory<ExponentialTrajectory<2>, ExponentialCurve>();

    // Check operators
    checkOperatorsTrajectory<ExponentialTrajectory<1>, ExponentialCurve>();

    // Check getters
    ASSERT_EQ(ExponentialTrajectory<12>::dimension(), 12);

    // Construct trajectories
    ExponentialTrajectory<2> trajectoryA;
    trajectoryA.m_splines[0].m_segments.resize(2);
    trajectoryA.m_splines[0].m_segments[0].m_coefficients[0] = 0.12;
    trajectoryA.m_splines[0].m_segments[0].m_coefficients[1] = -0.23;
    trajectoryA.m_splines[0].m_segments[0].m_coefficients[2] = 0.45;
    trajectoryA.m_splines[0].m_segments[1].m_coefficients[0] = 0.78;
    trajectoryA.m_splines[0].m_segments[1].m_coefficients[1] = -0.89;
    trajectoryA.m_splines[0].m_segments[1].m_coefficients[2] = 0.43;
    trajectoryA.m_splines[0].m_segmentProportions = std::vector<double>(2, 0.5);
    trajectoryA.m_splines[1].m_segments.resize(1);
    trajectoryA.m_splines[1].m_segments[0].m_coefficients[0] = 1.23;
    trajectoryA.m_splines[1].m_segments[0].m_coefficients[1] = -2.34;
    trajectoryA.m_splines[1].m_segments[0].m_coefficients[2] = 0.123;
    trajectoryA.m_splines[1].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryA.m_duration = 2.0;
    ExponentialTrajectory<2> trajectoryB;
    trajectoryB.m_splines[0].m_segments.resize(1);
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[0] = 0.12;
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[1] = -0.23;
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[2] = 0.45;
    trajectoryB.m_splines[0].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_splines[1].m_segments.resize(1);
    trajectoryB.m_splines[1].m_segments[0].m_coefficients[0] = 1.23;
    trajectoryB.m_splines[1].m_segments[0].m_coefficients[1] = -2.34;
    trajectoryB.m_splines[1].m_segments[0].m_coefficients[2] = 0.123;
    trajectoryB.m_splines[1].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_duration = 1.23;

    // Check high performance evaluation
    checkHighPerformanceEvaluationTrajectory<ExponentialTrajectory<2>, 2, 2>(trajectoryA);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeTrajectoryCustom<ExponentialTrajectory<2>, 2>(trajectoryB, 1);

    // Check encoding to XML
    checkEncodeToXMLTrajetory<ExponentialTrajectory<1>, ExponentialCurve>();
}
