/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/curve/trajectories/QuaternionBSplineTrajectory.hpp"
#include "QuaternionTrajectoryHelpers.hpp"
#include "broccoli/curve/splines/PolynomialSpline.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of quaternion BSpline trajectories
TEST(QuaternionBSplineTrajectory, BasicUsage)
{
    // Check validity
    checkQuaternionTrajectoryIsValidBSpline<0>();
    checkQuaternionTrajectoryIsValidBSpline<1>();
    checkQuaternionTrajectoryIsValidBSpline<2>();
    checkQuaternionTrajectoryIsValidBSpline<3>();
    checkQuaternionTrajectoryIsValidBSpline<4>();
    checkQuaternionTrajectoryIsValidBSpline<5>();

    // Construct
    QuaternionBSplineTrajectory<PolynomialSpline<5>, 3, 7> trajectory;
    trajectory.m_duration = 1.23;
    ASSERT_TRUE(trajectory.m_parameterSpline.interpolate(std::vector<double>{ 0, 1, 0, 0, 0, 0 }, nullptr, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES));
    ASSERT_TRUE(trajectory.m_quaternionSpline.interpolate(keyFramesSetAQuaternionInterpolation(), nullptr, QuaternionSplineInterpolationMethod::BSPLINE_SIMPLE));

    // Check segment boundaries
    checkGetParameterSplineSegmentBoundariesQuaternionTrajectory<decltype(trajectory)>(trajectory);

    // Check high performance evaluation
    checkHighPerformanceEvaluationQuaternionTrajectory<decltype(trajectory), 2>(trajectory);
    checkHighPerformanceEvaluationQuaternionTrajectoryAngularVelocityAcceleration<decltype(trajectory)>(trajectory);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeCustomQuaternionTrajectory<decltype(trajectory)>(trajectory, 1);
    compareAnalyticAndNumericDerivativeCustomQuaternionTrajectory<decltype(trajectory)>(trajectory, 2);

    // Check encoding to XML
    checkEncodeToXMLQuaternionTrajectory<QuaternionBSplineTrajectory<PolynomialSpline<5>, 0, 1>>();
    checkEncodeToXMLQuaternionTrajectory<QuaternionBSplineTrajectory<PolynomialSpline<5>, 1, 2>>();
    checkEncodeToXMLQuaternionTrajectory<QuaternionBSplineTrajectory<PolynomialSpline<5>, 2, 3>>();
    checkEncodeToXMLQuaternionTrajectory<QuaternionBSplineTrajectory<PolynomialSpline<5>, 3, 4>>();
    checkEncodeToXMLQuaternionTrajectory<QuaternionBSplineTrajectory<PolynomialSpline<5>, 4, 5>>();
    checkEncodeToXMLQuaternionTrajectory<QuaternionBSplineTrajectory<PolynomialSpline<5>, 5, 6>>();
}

//! Checks getters of quaternion B-Spline trajectory of certain degree
template <unsigned int Degree, unsigned int MaximumBSplineCount = Degree + 1>
bool checkGettersQuaternionBSplineTrajectory()
{
    QuaternionBSplineTrajectory<PolynomialSpline<5>, Degree, MaximumBSplineCount> trajectory;
    if (trajectory.degree() != Degree)
        return false;
    if (trajectory.order() != Degree + 1)
        return false;

    // Otherwise: success!
    return true;
}

//! Check getters of quaternion B-spline trajectory
TEST(QuaternionBSplineTrajectory, Getters)
{
    ASSERT_TRUE(checkGettersQuaternionBSplineTrajectory<0>());
    ASSERT_TRUE(checkGettersQuaternionBSplineTrajectory<1>());
    ASSERT_TRUE(checkGettersQuaternionBSplineTrajectory<2>());
    ASSERT_TRUE(checkGettersQuaternionBSplineTrajectory<3>());
    ASSERT_TRUE(checkGettersQuaternionBSplineTrajectory<4>());
    ASSERT_TRUE(checkGettersQuaternionBSplineTrajectory<5>());
}

#endif // HAVE_EIGEN3
