/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/curve/trajectories/PolynomialTrajectory.hpp"
#include "TrajectoryHelpers.hpp"
#include "broccoli/curve/trajectories/PolynomialTrajectorySplitter.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Template function to check basic usage of polynomial trajectories of the given degree
template <unsigned int Degree>
static void checkBasicUsagePolynomialTrajectory()
{
    // Check validity
    checkIsValidTrajectory<PolynomialTrajectory<Degree, 2>, PolynomialCurve<Degree>>();

    // Check operators
    checkOperatorsTrajectory<PolynomialTrajectory<Degree, 1>, PolynomialCurve<Degree>>();

    // Check getters
    const unsigned int trajectoryDimension = PolynomialTrajectory<Degree, 12>::dimension();
    ASSERT_EQ(trajectoryDimension, 12);
    const unsigned int trajectoryDegree = PolynomialTrajectory<Degree, 12>::degree();
    ASSERT_EQ(trajectoryDegree, Degree);
    const unsigned int trajectorOrder = PolynomialTrajectory<Degree, 12>::order();
    ASSERT_EQ(trajectorOrder, Degree + 1);

    // Construct trajectories
    PolynomialTrajectory<Degree, 2> trajectoryA;
    trajectoryA.m_splines[0].m_segments.push_back(PolynomialCurve<Degree>());
    trajectoryA.m_splines[0].m_segments.push_back(PolynomialCurve<Degree>());
    trajectoryA.m_splines[1].m_segments.push_back(PolynomialCurve<Degree>());
    for (unsigned int i = 0; i <= Degree; i++) {
        trajectoryA.m_splines[0].m_segments[0].m_coefficients[i] = 1.23 + i * 4.56;
        trajectoryA.m_splines[0].m_segments[1].m_coefficients[i] = 1.23 - i * 4.56;
        trajectoryA.m_splines[1].m_segments[0].m_coefficients[i] = 0.12 + i * 0.45;
    }
    trajectoryA.m_splines[0].m_segmentProportions = std::vector<double>(2, 0.5);
    trajectoryA.m_splines[1].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryA.m_duration = 2.0;
    PolynomialTrajectory<Degree, 2> trajectoryB;
    trajectoryB.m_splines[0].m_segments.push_back(PolynomialCurve<Degree>());
    trajectoryB.m_splines[1].m_segments.push_back(PolynomialCurve<Degree>());
    for (unsigned int i = 0; i <= Degree; i++) {
        trajectoryB.m_splines[0].m_segments[0].m_coefficients[i] = 1.23 + i * 4.56;
        trajectoryB.m_splines[1].m_segments[0].m_coefficients[i] = 0.12 + i * 0.45;
    }
    trajectoryB.m_splines[0].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_splines[1].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_duration = 1.23;

    // Check segment boundaries
    checkGetSegmentBoundariesTrajectory<decltype(trajectoryA), 2>(trajectoryA);
    checkGetSegmentBoundariesTrajectory<decltype(trajectoryB), 2>(trajectoryB);

    // Check high performance evaluation
    checkHighPerformanceEvaluationTrajectory<PolynomialTrajectory<Degree, 2>, 2, 2>(trajectoryA);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeTrajectoryCustom<PolynomialTrajectory<Degree, 2>, 2>(trajectoryB, 1);

    // Check splitting and joining (only for selected degrees)
    if (Degree == 0 || Degree == 1 || Degree == 3 || Degree == 5) {
        checkSplitAndJoinTrajectory<PolynomialTrajectorySplitter<Degree, 2>, PolynomialTrajectory<Degree, 2>, PolynomialSpline<Degree>, PolynomialCurve<Degree>, 2, 3>(trajectoryA);
        checkSplitAndJoinTrajectory<PolynomialTrajectorySplitter<Degree, 2>, PolynomialTrajectory<Degree, 2>, PolynomialSpline<Degree>, PolynomialCurve<Degree>, 2, 3>(trajectoryB);
    }

    // Check encoding to XML
    checkEncodeToXMLTrajetory<PolynomialTrajectory<Degree, 1>, PolynomialCurve<Degree>>();
}

//! Test basic usage of polynomial trajectories
TEST(PolynomialTrajectory, BasicUsage)
{
    checkBasicUsagePolynomialTrajectory<0>();
    checkBasicUsagePolynomialTrajectory<1>();
    checkBasicUsagePolynomialTrajectory<2>();
    checkBasicUsagePolynomialTrajectory<3>();
    checkBasicUsagePolynomialTrajectory<4>();
    checkBasicUsagePolynomialTrajectory<5>();
}

//! Check construction and evaluation of polynomial trajectory of degree one
TEST(PolynomialTrajectory, DegreeOne)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialTrajectory<1, 1> trajectory;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(3);
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = -1;
    trajectory.m_duration = 10;

    // Trigger interpolation
    bool returnValue = trajectory.m_splines[0].interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_LINEAR, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check function values
    ASSERT_TRUE(trajectory.evaluate(0.0, 0)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(2.5, 0)[0] == 0.5);
    ASSERT_TRUE(trajectory.evaluate(5.0, 0)[0] == 1);
    ASSERT_TRUE(trajectory.evaluate(7.5, 0)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(10.0, 0)[0] == -1);

    // Check derivatives
    ASSERT_TRUE(trajectory.evaluate(2.5, 1)[0] == 0.2);
    ASSERT_TRUE(trajectory.evaluate(7.5, 1)[0] == -0.4);
    ASSERT_TRUE(trajectory.evaluate(2.5, 2)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(7.5, 2)[0] == 0);

    // Write plot file for visual debugging
    plotTrajectory<PolynomialTrajectory<1, 1>, 1>(trajectory, 0.001, 2, "PolynomialTrajectoryDegreeOne");

    // Check high performance evaluation
    checkHighPerformanceEvaluationTrajectory<PolynomialTrajectory<1, 1>, 1, 2>(trajectory);

    // Check splitting and joining
    checkSplitAndJoinTrajectory<PolynomialTrajectorySplitter<1, 1>, PolynomialTrajectory<1, 1>, PolynomialSpline<1>, PolynomialCurve<1>, 1, 3>(trajectory);
}

//! Check construction and evaluation of polynomial trajectory of degree five
TEST(PolynomialTrajectory, DegreeFive)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialTrajectory<5, 1> trajectory;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(9);
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = 3;
    trajectory.m_duration = 10;

    // Trigger interpolation
    bool returnValue = trajectory.m_splines[0].interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check function values
    ASSERT_TRUE(trajectory.evaluate(0.0, 0)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(2.5, 0)[0] == 0.5);
    ASSERT_TRUE(trajectory.evaluate(5.0, 0)[0] == 1);
    ASSERT_TRUE(trajectory.evaluate(7.5, 0)[0] == 2);
    ASSERT_TRUE(trajectory.evaluate(10.0, 0)[0] == 3);

    // Check derivatives
    ASSERT_TRUE(trajectory.evaluate(0, 1)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(5, 1)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(10, 1)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(0, 2)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(5, 2)[0] == 0);
    ASSERT_TRUE(trajectory.evaluate(10, 2)[0] == 0);

    // Write plot file for visual debugging
    plotTrajectory<PolynomialTrajectory<5, 1>, 1>(trajectory, 0.001, 2, "PolynomialTrajectoryDegreeFive");

    // Check analytic/numeric derivative
    compareAnalyticAndNumericDerivativeTrajectoryCustom<PolynomialTrajectory<5, 1>, 1>(trajectory, 1);

    // Check high performance evaluation
    checkHighPerformanceEvaluationTrajectory<PolynomialTrajectory<5, 1>, 1, 2>(trajectory);

    // Check splitting and joining
    checkSplitAndJoinTrajectory<PolynomialTrajectorySplitter<5, 1>, PolynomialTrajectory<5, 1>, PolynomialSpline<5>, PolynomialCurve<5>, 1, 3>(trajectory);
}

#ifdef HAVE_EIGEN3
//! Check construction and evaluation of polynomial trajectory of degree five (smooth)
TEST(PolynomialTrajectory, DegreeFiveSmooth)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialTrajectory<5, 1> trajectory;

    // Setup parameter set
    std::vector<double> proportions;
    std::vector<double> parameters;
    parameters.resize(9, 0);
    parameters[0] = 0.0;
    parameters[1] = 1.0;
    parameters[2] = 0.5;
    parameters[3] = 2.0;
    parameters[4] = 3.0;
    parameters[5] = 0.1;
    parameters[6] = 0.2;
    parameters[7] = 0.3;
    parameters[8] = 0.4;
    proportions.resize(4, 0);
    proportions[0] = 0.1;
    proportions[1] = 0.2;
    proportions[2] = 0.3;
    proportions[3] = 0.4;
    trajectory.m_duration = 10;

    // Trigger interpolation
    bool returnValue = trajectory.m_splines[0].interpolate(parameters, &proportions, SplineInterpolationMethod::POLYNOMIAL_SMOOTH_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Check segment count
    ASSERT_TRUE(trajectory.m_splines[0].m_segments.size() == parameters.size() - 5);
    ASSERT_TRUE(trajectory.m_splines[0].m_segmentProportions.size() == trajectory.m_splines[0].m_segments.size());

    // Check interpolation
    double splinePosition = 0;
    for (size_t i = 0; i < parameters.size() - 4; i++) {
        ASSERT_TRUE(fabs(trajectory.evaluate(splinePosition * trajectory.m_duration, 0)[0] - parameters[i]) < 1e-6);
        if (i < proportions.size())
            splinePosition += proportions[i];
    }
    ASSERT_TRUE(fabs(trajectory.evaluate(0.0 * trajectory.m_duration, 1)[0] - parameters[5] / trajectory.m_duration) < 1e-6);
    ASSERT_TRUE(fabs(trajectory.evaluate(1.0 * trajectory.m_duration, 1)[0] - parameters[6] / trajectory.m_duration) < 1e-6);
    ASSERT_TRUE(fabs(trajectory.evaluate(0.0 * trajectory.m_duration, 2)[0] - parameters[7] / trajectory.m_duration / trajectory.m_duration) < 1e-6);
    ASSERT_TRUE(fabs(trajectory.evaluate(1.0 * trajectory.m_duration, 2)[0] - parameters[8] / trajectory.m_duration / trajectory.m_duration) < 1e-6);

    // Write plot file for visual debugging
    plotTrajectory<PolynomialTrajectory<5, 1>, 1>(trajectory, 0.001, 2, "PolynomialTrajectoryDegreeFiveSmooth");

    // Check analytic/numeric derivative
    compareAnalyticAndNumericDerivativeTrajectoryCustom<PolynomialTrajectory<5, 1>, 1>(trajectory, 1);

    // Check high performance evaluation
    checkHighPerformanceEvaluationTrajectory<PolynomialTrajectory<5, 1>, 1, 2>(trajectory);

    // Check splitting and joining
    checkSplitAndJoinTrajectory<PolynomialTrajectorySplitter<5, 1>, PolynomialTrajectory<5, 1>, PolynomialSpline<5>, PolynomialCurve<5>, 1, 3>(trajectory);
}
#endif // HAVE_EIGEN3

//! Check construction and evaluation of multi-dimensional polynomial trajectory of degree five
TEST(PolynomialTrajectory, DegreeFiveMultiDimensional)
{
    // Initialize helpers
    SplineResult result;

    // Initialize container
    PolynomialTrajectory<5, 3> trajectory;
    trajectory.m_duration = 10;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(9);

    // Trigger interpolation
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = 3;
    parameters[3] = 0.1;
    parameters[4] = 0.2;
    parameters[5] = 0.3;
    parameters[6] = -0.1;
    parameters[7] = -0.2;
    parameters[8] = -0.3;
    bool returnValue = trajectory.m_splines[0].interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);
    parameters[0] = 2;
    parameters[1] = 3;
    parameters[2] = -1;
    returnValue = trajectory.m_splines[1].interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);
    parameters[0] = 0.1;
    parameters[1] = 0.2;
    parameters[2] = -1.234;
    returnValue = trajectory.m_splines[2].interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &result);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(result, SplineResult::SUCCESS);

    // Write plot file for visual debugging
    plotTrajectory<PolynomialTrajectory<5, 3>, 3>(trajectory, 0.001, 2, "PolynomialTrajectoryDegreeFiveMultiDimensional");

    // Check analytic/numeric derivative
    compareAnalyticAndNumericDerivativeTrajectoryCustom<PolynomialTrajectory<5, 3>, 3>(trajectory, 1);

    // Check high performance evaluation
    checkHighPerformanceEvaluationTrajectory<PolynomialTrajectory<5, 3>, 3, 2>(trajectory);

    // Check splitting and joining
    checkSplitAndJoinTrajectory<PolynomialTrajectorySplitter<5, 3>, PolynomialTrajectory<5, 3>, PolynomialSpline<5>, PolynomialCurve<5>, 3, 3>(trajectory);
}

//! Check splitting and joining polynomial trajectores of degree five
TEST(PolynomialTrajectory, DegreeFiveSplitJoint)
{
    // Initialize helpers
    TrajectoryResult trajectoryResult;
    SplineResult splineResult;

    // Initialize container
    PolynomialTrajectory<5, 1> sourceTrajectory;

    // Setup parameter set
    std::vector<double> parameters;
    parameters.resize(6);
    parameters[0] = 0;
    parameters[1] = 1;
    parameters[2] = 0;
    parameters[3] = 0;
    parameters[4] = 0;
    parameters[5] = 0;
    sourceTrajectory.m_duration = 1;

    // Trigger interpolation
    bool returnValue = sourceTrajectory.m_splines[0].interpolate(parameters, nullptr /* empty -> use uniform proportions */, SplineInterpolationMethod::POLYNOMIAL_PIECEWISE_QUINTIC_FIRST_AND_SECOND_DERIVATIVES, &splineResult);
    ASSERT_EQ(returnValue, true);
    ASSERT_EQ(splineResult, SplineResult::SUCCESS);

    // Check function values
    ASSERT_TRUE(sourceTrajectory.evaluate(0.0, 0)[0] == 0.0);
    ASSERT_TRUE(sourceTrajectory.evaluate(0.5, 0)[0] == 0.5);
    ASSERT_TRUE(sourceTrajectory.evaluate(1.0, 0)[0] == 1.0);

    // Check derivatives
    ASSERT_TRUE(sourceTrajectory.evaluate(0, 1)[0] == 0);
    ASSERT_TRUE(sourceTrajectory.evaluate(1, 1)[0] == 0);
    ASSERT_TRUE(sourceTrajectory.evaluate(0, 2)[0] == 0);
    ASSERT_TRUE(sourceTrajectory.evaluate(1, 2)[0] == 0);

    // Write plot file for visual debugging
    plotTrajectory<PolynomialTrajectory<5, 1>, 1>(sourceTrajectory, 0.001, 5, "PolynomialTrajectoryDegreeFiveSplitJoin_source");

    // Create intermediate trajectories
    std::vector<PolynomialTrajectory<5, 1>> splittedTrajectories;
    std::vector<PolynomialTrajectory<5, 1>*> splittedTrajectoriesPointer;
    std::vector<PolynomialTrajectory<5, 1> const*> splittedTrajectoriesPointerConst;
    splittedTrajectories.resize(2);
    splittedTrajectoriesPointer.resize(2);
    splittedTrajectoriesPointerConst.resize(2);
    splittedTrajectories[0].m_duration = 0.5;
    splittedTrajectories[1].m_duration = 0.5;
    splittedTrajectoriesPointer[0] = &splittedTrajectories[0];
    splittedTrajectoriesPointer[1] = &splittedTrajectories[1];
    splittedTrajectoriesPointerConst[0] = &splittedTrajectories[0];
    splittedTrajectoriesPointerConst[1] = &splittedTrajectories[1];

    // Perform split
    const bool splitReturnValue = TrajectorySplitter<PolynomialTrajectory<5, 1>, PolynomialSpline<5>, PolynomialCurve<5>>::split(sourceTrajectory, splittedTrajectoriesPointer, false, &trajectoryResult, &splineResult);
    ASSERT_TRUE(splitReturnValue == true);
    ASSERT_TRUE(trajectoryResult == TrajectoryResult::SUCCESS);
    ASSERT_TRUE(splineResult == SplineResult::SUCCESS);

    // Write plot file for visual debugging
    plotTrajectory<PolynomialTrajectory<5, 1>, 1>(splittedTrajectories[0], 0.001, 5, "PolynomialTrajectoryDegreeFiveSplitJoin_splitted[0]");
    plotTrajectory<PolynomialTrajectory<5, 1>, 1>(splittedTrajectories[1], 0.001, 5, "PolynomialTrajectoryDegreeFiveSplitJoin_splitted[1]");

    // Check function values
    ASSERT_EQ(sourceTrajectory.evaluate(0.0, 0)[0], splittedTrajectories[0].evaluate(0.0, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.25, 0)[0], splittedTrajectories[0].evaluate(0.25, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 0)[0], splittedTrajectories[0].evaluate(0.5, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 0)[0], splittedTrajectories[1].evaluate(0.0, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.75, 0)[0], splittedTrajectories[1].evaluate(0.25, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(1.0, 0)[0], splittedTrajectories[1].evaluate(0.5, 0)[0]);

    // Check first order derivatives
    ASSERT_EQ(sourceTrajectory.evaluate(0.0, 1)[0], splittedTrajectories[0].evaluate(0.0, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.25, 1)[0], splittedTrajectories[0].evaluate(0.25, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 1)[0], splittedTrajectories[0].evaluate(0.5, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 1)[0], splittedTrajectories[1].evaluate(0.0, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.75, 1)[0], splittedTrajectories[1].evaluate(0.25, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(1.0, 1)[0], splittedTrajectories[1].evaluate(0.5, 1)[0]);

    // Check second order derivatives
    ASSERT_EQ(sourceTrajectory.evaluate(0.0, 2)[0], splittedTrajectories[0].evaluate(0.0, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.25, 2)[0], splittedTrajectories[0].evaluate(0.25, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 2)[0], splittedTrajectories[0].evaluate(0.5, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 2)[0], splittedTrajectories[1].evaluate(0.0, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.75, 2)[0], splittedTrajectories[1].evaluate(0.25, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(1.0, 2)[0], splittedTrajectories[1].evaluate(0.5, 2)[0]);

    // Create joined trajectory
    PolynomialTrajectory<5, 1> joinedTrajectory;
    joinedTrajectory.m_duration = sourceTrajectory.m_duration;

    // Perform join
    const bool joinReturnValue = PolynomialTrajectorySplitter<5, 1>::join(joinedTrajectory, splittedTrajectoriesPointerConst, false, &trajectoryResult, &splineResult);
    ASSERT_TRUE(joinReturnValue == true);
    ASSERT_TRUE(trajectoryResult == TrajectoryResult::SUCCESS);
    ASSERT_TRUE(splineResult == SplineResult::SUCCESS);

    // Write plot file for visual debugging
    plotTrajectory<PolynomialTrajectory<5, 1>, 1>(joinedTrajectory, 0.001, 5, "PolynomialTrajectoryDegreeFiveSplitJoin_joined");

    // Check function values
    ASSERT_EQ(sourceTrajectory.evaluate(0.0, 0)[0], joinedTrajectory.evaluate(0.0, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.25, 0)[0], joinedTrajectory.evaluate(0.25, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 0)[0], joinedTrajectory.evaluate(0.5, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.75, 0)[0], joinedTrajectory.evaluate(0.75, 0)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(1.0, 0)[0], joinedTrajectory.evaluate(1.0, 0)[0]);

    // Check first order derivatives
    ASSERT_EQ(sourceTrajectory.evaluate(0.0, 1)[0], joinedTrajectory.evaluate(0.0, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.25, 1)[0], joinedTrajectory.evaluate(0.25, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 1)[0], joinedTrajectory.evaluate(0.5, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.75, 1)[0], joinedTrajectory.evaluate(0.75, 1)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(1.0, 1)[0], joinedTrajectory.evaluate(1.0, 1)[0]);

    // Check second order derivatives
    ASSERT_EQ(sourceTrajectory.evaluate(0.0, 2)[0], joinedTrajectory.evaluate(0.0, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.25, 2)[0], joinedTrajectory.evaluate(0.25, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.5, 2)[0], joinedTrajectory.evaluate(0.5, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(0.75, 2)[0], joinedTrajectory.evaluate(0.75, 2)[0]);
    ASSERT_EQ(sourceTrajectory.evaluate(1.0, 2)[0], joinedTrajectory.evaluate(1.0, 2)[0]);
}
