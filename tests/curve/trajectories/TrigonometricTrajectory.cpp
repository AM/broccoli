/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/curve/trajectories/TrigonometricTrajectory.hpp"
#include "TrajectoryHelpers.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace curve;

//! Test basic usage of trigonometric trajectory
TEST(TrigonometricTrajectory, BasicUsage)
{
    // Check validity
    checkIsValidTrajectory<TrigonometricTrajectory<2>, TrigonometricCurve>();

    // Check operators
    checkOperatorsTrajectory<TrigonometricTrajectory<1>, TrigonometricCurve>();

    // Check getters
    ASSERT_EQ(TrigonometricTrajectory<12>::dimension(), 12);

    // Construct trajectories
    TrigonometricTrajectory<2> trajectoryA;
    trajectoryA.m_splines[0].m_segments.push_back(TrigonometricCurve());
    trajectoryA.m_splines[0].m_segments[0].m_type = TrigonometricCurve::Type::COSINE;
    trajectoryA.m_splines[0].m_segments[0].m_coefficients[0] = 1.23;
    trajectoryA.m_splines[0].m_segments[0].m_coefficients[1] = -2.34;
    trajectoryA.m_splines[0].m_segments[0].m_coefficients[2] = 0.123;
    trajectoryA.m_splines[0].m_segments[0].m_coefficients[3] = -0.234;
    trajectoryA.m_splines[0].m_segments.push_back(TrigonometricCurve());
    trajectoryA.m_splines[0].m_segments[1].m_type = TrigonometricCurve::Type::SINE;
    trajectoryA.m_splines[0].m_segments[1].m_coefficients[0] = 1.23;
    trajectoryA.m_splines[0].m_segments[1].m_coefficients[1] = -2.34;
    trajectoryA.m_splines[0].m_segments[1].m_coefficients[2] = 0.123;
    trajectoryA.m_splines[0].m_segments[1].m_coefficients[3] = -0.234;
    trajectoryA.m_splines[0].m_segmentProportions = std::vector<double>(2, 0.5);
    trajectoryA.m_splines[1].m_segments.push_back(TrigonometricCurve());
    trajectoryA.m_splines[1].m_segments[0].m_type = TrigonometricCurve::Type::SINE;
    trajectoryA.m_splines[1].m_segments[0].m_coefficients[0] = 0.12;
    trajectoryA.m_splines[1].m_segments[0].m_coefficients[1] = -0.23;
    trajectoryA.m_splines[1].m_segments[0].m_coefficients[2] = 0.45;
    trajectoryA.m_splines[1].m_segments[0].m_coefficients[3] = -0.89;
    trajectoryA.m_splines[1].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryA.m_duration = 2.0;
    TrigonometricTrajectory<2> trajectoryB;
    trajectoryB.m_splines[0].m_segments.push_back(TrigonometricCurve());
    trajectoryB.m_splines[0].m_segments[0].m_type = TrigonometricCurve::Type::COSINE;
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[0] = 1.23;
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[1] = -2.34;
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[2] = 0.123;
    trajectoryB.m_splines[0].m_segments[0].m_coefficients[3] = -0.234;
    trajectoryB.m_splines[0].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_splines[1].m_segments.push_back(TrigonometricCurve());
    trajectoryB.m_splines[1].m_segments[0].m_type = TrigonometricCurve::Type::SINE;
    trajectoryB.m_splines[1].m_segments[0].m_coefficients[0] = 0.12;
    trajectoryB.m_splines[1].m_segments[0].m_coefficients[1] = -0.23;
    trajectoryB.m_splines[1].m_segments[0].m_coefficients[2] = 0.45;
    trajectoryB.m_splines[1].m_segments[0].m_coefficients[3] = -0.89;
    trajectoryB.m_splines[1].m_segmentProportions = std::vector<double>(1, 1.0);
    trajectoryB.m_duration = 1.23;

    // Check high performance evaluation
    checkHighPerformanceEvaluationTrajectory<TrigonometricTrajectory<2>, 2, 2>(trajectoryA);

    // Check numeric and analytic derivative
    compareAnalyticAndNumericDerivativeTrajectoryCustom<TrigonometricTrajectory<2>, 2>(trajectoryB, 1);

    // Check encoding to XML
    checkEncodeToXMLTrajetory<TrigonometricTrajectory<1>, TrigonometricCurve>();
}
