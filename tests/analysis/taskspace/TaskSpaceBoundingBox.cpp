/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceBoundingBox.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Creates a dummy object for testing
static inline TaskSpaceBoundingBox createTestBoundingBox()
{
    TaskSpaceBoundingBox box;
    box.addSample(Eigen::Vector3d(1, 2, 3));
    box.addSample(Eigen::Vector3d(4, 5, 6));
    return box;
}

//! Test of copy constructor
TEST(TaskSpaceBoundingBox, CopyConstructor)
{
    const TaskSpaceBoundingBox boxA = createTestBoundingBox();
    const TaskSpaceBoundingBox boxB(boxA);
    ASSERT_TRUE(boxB == boxA);
}

//! Test of copy assignement operator
TEST(TaskSpaceBoundingBox, CopyAssignmentOperator)
{
    const TaskSpaceBoundingBox boxA = createTestBoundingBox();
    TaskSpaceBoundingBox boxB;
    ASSERT_TRUE(boxB != boxA);
    boxB = boxA;
    ASSERT_TRUE(boxB == boxA);
}

//! Test of addSample() and clear()
TEST(TaskSpaceBoundingBox, AddSampleAndClear)
{
    // Initialize helpers
    TaskSpaceBoundingBox box;
    Eigen::Vector3d zero = Eigen::Vector3d::Zero();
    Eigen::Vector3d sampleA(1, 2, 3);
    Eigen::Vector3d sampleB(4, 5, 6);

    // Test adding samples
    ASSERT_EQ(box.samples(), 0);
    box.addSample(sampleA);
    ASSERT_EQ(box.samples(), 1);
    ASSERT_TRUE(box.minimum().isApprox(sampleA));
    ASSERT_TRUE(box.maximum().isApprox(sampleA));
    box.addSample(sampleB);
    ASSERT_EQ(box.samples(), 2);
    ASSERT_TRUE(box.minimum().isApprox(sampleA));
    ASSERT_TRUE(box.maximum().isApprox(sampleB));

    // Test clear
    box.clear();
    ASSERT_EQ(box.samples(), 0);
    ASSERT_TRUE(box.minimum().isApprox(zero));
    ASSERT_TRUE(box.maximum().isApprox(zero));
}

//! Test of createMesh()
TEST(TaskSpaceBoundingBox, createMesh)
{
    // Initialize helpers
    TaskSpaceBoundingBox box = createTestBoundingBox();

    // Test create mesh (without colors)
    auto meshWithoutColors = box.createMesh(false);
    ASSERT_GT(meshWithoutColors.m_indexBuffer.cols(), 0);
    ASSERT_EQ(meshWithoutColors.m_colorBuffer.cols(), 0);

    // Test create mesh (with colors)
    auto meshWithColors = box.createMesh(true);
    ASSERT_GT(meshWithColors.m_indexBuffer.cols(), 0);
    ASSERT_GT(meshWithColors.m_colorBuffer.cols(), 0);
}

//! Test of serialization
TEST(TaskSpaceBoundingBox, Serialization)
{
    // Setup input
    TaskSpaceBoundingBox input = createTestBoundingBox();

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
