/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceEvaluatorResult.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Test converstion to string representation of results
TEST(TaskSpaceEvaluatorResult, toString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(TaskSpaceEvaluatorResult::Type::TYPE_COUNT); i++) {
        ASSERT_GT(TaskSpaceEvaluatorResult::toString(static_cast<TaskSpaceEvaluatorResult::Type>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(TaskSpaceEvaluatorResult::toString(static_cast<TaskSpaceEvaluatorResult::Type>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(TaskSpaceEvaluatorResult::toString(static_cast<TaskSpaceEvaluatorResult::Type>(i)) != "UNKNOWN");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(TaskSpaceEvaluatorResult::toString(TaskSpaceEvaluatorResult::Type::TYPE_COUNT) == "UNKNOWN");
#endif
}

#endif // HAVE_EIGEN3
