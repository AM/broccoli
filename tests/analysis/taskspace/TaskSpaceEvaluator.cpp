/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceEvaluator.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "broccoli/geometry/DHParameters.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

#define WRITE_CONSOLE_OUTPUT // Enables writing to console
//#define WRITE_FILE_OUTPUT // Enables writing output files

//! Helper function to create a kinematic chain
static inline KinematicChain createKinematicChain(const double& maximumTranslationStepSize)
{
    // Initialize return value
    KinematicChain chain;

    // Setup segment list
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(5);

    // Create root segment
    KinematicChainSegment rootSegment("root");

    // Create first rotational segment
    KinematicChainSegment firstRotationSegment("first-rotation", "root");
    firstRotationSegment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    firstRotationSegment.minimumPosition() = -M_PI_2;
    firstRotationSegment.maximumPosition() = M_PI_2;
    firstRotationSegment.samples() = 2;

    // Create second rotational segment
    KinematicChainSegment secondRotationSegment("second-rotation", "first-rotation");
    secondRotationSegment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    secondRotationSegment.initialTransform() = geometry::DHParameters(0.0, -M_PI_2, 0, M_PI).toTransform();
    secondRotationSegment.minimumPosition() = -M_PI_2;
    secondRotationSegment.maximumPosition() = M_PI_2;
    secondRotationSegment.samples() = 2;

    // Create translational segment
    KinematicChainSegment translationSegment("translation", "second-rotation");
    translationSegment.dofType() = KinematicChainSegment::DOFType::TRANSLATION;
    translationSegment.initialTransform() = geometry::DHParameters(0.0, -M_PI_2, 0, 0).toTransform();
    translationSegment.minimumPosition() = 1.0;
    translationSegment.maximumPosition() = 2.0;
    translationSegment.samples() = 2;

    // Create tcp segment
    KinematicChainSegment tcpSegment("tcp", "translation");

    // Add segments to chain
    segmentList.push_back(rootSegment);
    segmentList.push_back(firstRotationSegment);
    segmentList.push_back(secondRotationSegment);
    segmentList.push_back(translationSegment);
    segmentList.push_back(tcpSegment);
    chain.set(segmentList);

    // Use adaptive sampling
    chain.autoComputeSamples(maximumTranslationStepSize);

    // Pass back chain
    return chain;
}

//! Test of a simple evaluation problem
TEST(TaskSpaceEvaluator, SimpleEvaluationProblem)
{
    // Setup input
    TaskSpaceEvaluatorInput<3> input;
#ifdef WRITE_CONSOLE_OUTPUT
    input.m_consoleOutput = true;
#else
    input.m_consoleOutput = false;
#endif
    input.m_kinematicChain = createKinematicChain(input.m_minimumCellDimensions.minCoeff() / 2.0);
    input.setupTaskSpaceSelectionMatrix(true, true, true, false, false, false);
    //input.m_maximumThreadCount = 1;
#ifdef WRITE_FILE_OUTPUT
    input.m_writeOutputFiles = true;
#else
    input.m_writeOutputFiles = false;
#endif
    input.m_outputFolder = "TaskSpaceEvaluator_Test";
    //input.m_writeOutputFilesPreSampler = true;
    //input.m_writeOutputFilesMainSampler = true;
    input.setupOutputMeshesAll();

    // Setup evaluator
    TaskSpaceEvaluator<3> evaluator(input);

    // Trigger pre-processing
    ASSERT_TRUE(evaluator.preProcess());

    // Trigger processing
    ASSERT_TRUE(evaluator.process());

    // Only "plot" for positive y-values (relative to octree center
    for (size_t i = 0; i < evaluator.input().m_outputFilesOctreeMeshList.size(); i++)
        evaluator.input().m_outputFilesOctreeMeshList[i].m_positionMinimum(0) = evaluator.output().taskspace().center()(0);

    // Trigger post-processing
    ASSERT_TRUE(evaluator.postProcess());

    // Test serialization and deserialization
    testSerializableData(evaluator, evaluator.computeBinaryStreamSize());

    // Test saving and loading
#ifdef WRITE_FILE_OUTPUT
    TaskSpaceEvaluatorResult::Type result = TaskSpaceEvaluatorResult::Type::UNKNOWN;
    const std::string filePath = "TaskSpaceEvaluator_Test.bin";
    ASSERT_TRUE(evaluator.saveToFile(filePath, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);
    TaskSpaceEvaluator<3> loadedEvaluator;
    ASSERT_TRUE(loadedEvaluator.loadFromFile(filePath, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);
    ASSERT_TRUE(loadedEvaluator == evaluator);
#endif
}

#endif // HAVE_EIGEN3
