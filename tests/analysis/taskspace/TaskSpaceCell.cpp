/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceCell.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;
using namespace io;

//! Mockup-class to give access to protected members
class TaskSpaceSampleMockup : public TaskSpaceSample {
public:
    double& conditionIndex() { return m_conditionIndex; }
    double& manipulabilityMeasure() { return m_manipulabilityMeasure; }
    double& jointRangeAvailability() { return m_jointRangeAvailability; }
};

//! Creates a dummy cell for testing
static inline TaskSpaceCell createTestCell()
{
    TaskSpaceCell cell;
    cell.m_totalSamples = 123;
    cell.m_conditionIndexMinimum = 1.23;
    cell.m_conditionIndexMean = 2.34;
    cell.m_conditionIndexMaximum = 3.45;
    cell.m_manipulabilityMeasureMinimum = -1.23;
    cell.m_manipulabilityMeasureMean = -2.34;
    cell.m_manipulabilityMeasureMaximum = -3.45;
    cell.m_jointRangeAvailabilityMinimum = 4.56;
    cell.m_jointRangeAvailabilityMean = 6.78;
    cell.m_jointRangeAvailabilityMaximum = 7.89;
    return cell;
}

//! Test of reset()
TEST(TaskSpaceCell, reset)
{
    TaskSpaceCell defaultCell;
    TaskSpaceCell filledCell = createTestCell();
    ASSERT_TRUE(filledCell != defaultCell);
    filledCell.reset();
    ASSERT_TRUE(filledCell == defaultCell);
}

//! Test of addSample() (and metric)
TEST(TaskSpaceCell, addSample)
{
    // Initialize samples
    TaskSpaceSampleMockup sampleA;
    sampleA.conditionIndex() = 1.23;
    sampleA.manipulabilityMeasure() = 2.34;
    sampleA.jointRangeAvailability() = 3.45;
    TaskSpaceSampleMockup sampleB;
    sampleB.conditionIndex() = 4.56;
    sampleB.manipulabilityMeasure() = 5.67;
    sampleB.jointRangeAvailability() = 6.78;

    // Create test cell
    TaskSpaceCell cell;
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::REACHABILITY), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MINIMUM), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MEAN), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MAXIMUM), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MINIMUM), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MEAN), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MAXIMUM), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MINIMUM), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MEAN), 0);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MAXIMUM), 0);

    // Add sample A
    cell.addSample(sampleA);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::REACHABILITY), 1);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MINIMUM), sampleA.conditionIndex());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MEAN), sampleA.conditionIndex());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MAXIMUM), sampleA.conditionIndex());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MINIMUM), sampleA.manipulabilityMeasure());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MEAN), sampleA.manipulabilityMeasure());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MAXIMUM), sampleA.manipulabilityMeasure());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MINIMUM), sampleA.jointRangeAvailability());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MEAN), sampleA.jointRangeAvailability());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MAXIMUM), sampleA.jointRangeAvailability());

    // Add sample B
    cell.addSample(sampleB);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::REACHABILITY), 2);
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MINIMUM), sampleA.conditionIndex());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MEAN), 0.5 * (sampleA.conditionIndex() + sampleB.conditionIndex()));
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::CONDITION_INDEX_MAXIMUM), sampleB.conditionIndex());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MINIMUM), sampleA.manipulabilityMeasure());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MEAN), 0.5 * (sampleA.manipulabilityMeasure() + sampleB.manipulabilityMeasure()));
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::MANIPULABILITY_MEASURE_MAXIMUM), sampleB.manipulabilityMeasure());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MINIMUM), sampleA.jointRangeAvailability());
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MEAN), 0.5 * (sampleA.jointRangeAvailability() + sampleB.jointRangeAvailability()));
    ASSERT_FLOAT_EQ(cell.metric(TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MAXIMUM), sampleB.jointRangeAvailability());

    // Test invalid metric
#ifdef NDEBUG
    ASSERT_EQ(cell.metric(TaskSpaceMetric::Type::TYPE_COUNT), 0);
#endif
}

//! Test of serialization
TEST(TaskSpaceCell, Serialization)
{
    const TaskSpaceCell input = createTestCell();
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
