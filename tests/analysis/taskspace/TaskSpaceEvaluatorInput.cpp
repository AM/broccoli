/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceEvaluatorInput.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Creates a dummy object for testing
template <unsigned int L>
static inline TaskSpaceEvaluatorInput<L> createTestInput()
{
    TaskSpaceEvaluatorInput<L> input;
    input.m_consoleOutput = !input.m_consoleOutput;
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(2);
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    input.m_kinematicChain.set(segmentList);
    input.m_taskSpaceSelectionMatrix.resize(6, Eigen::NoChange);
    input.m_taskSpaceSelectionMatrix << 1, 2, 3, 4, 5, 6,
        7, 8, 9, 10, 11, 12,
        13, 14, 15, 16, 17, 18,
        19, 20, 21, 22, 23, 24,
        25, 26, 27, 28, 29, 30,
        31, 32, 33, 34, 35, 36;
    input.m_maximumThreadCount++;
    input.m_minimumCellDimensions += Eigen::Vector3d(0.1, 0.1, 0.1);
    input.m_extraCellCount++;
    input.m_writeOutputFiles = !input.m_writeOutputFiles;
    input.m_outputFolder += "abc";
    input.m_plyFileFormat = io::PLYFormat::Type::BINARY_BIG_ENDIAN;
    input.m_writeOutputFilesEvaluator = !input.m_writeOutputFilesEvaluator;
    input.m_outputFilesEvaluatorPrefix += "asdf";
    input.m_writeOutputFilesPreSampler = !input.m_writeOutputFilesPreSampler;
    input.m_writeOutputFilesMainSampler = !input.m_writeOutputFilesMainSampler;
    input.m_writeOutputFilesBoundingBoxPreSampling = !input.m_writeOutputFilesBoundingBoxPreSampling;
    input.m_outputFilesBoundingBoxPreSamplingName += "def";
    input.m_outputFilesBoundingBoxPreSamplingWithColors = !input.m_outputFilesBoundingBoxPreSamplingWithColors;
    input.m_writeOutputFilesBoundingBoxMainSampling = !input.m_writeOutputFilesBoundingBoxMainSampling;
    input.m_outputFilesBoundingBoxMainSamplingName += "def";
    input.m_outputFilesBoundingBoxMainSamplingWithColors = !input.m_outputFilesBoundingBoxMainSamplingWithColors;
    input.m_writeOutputFilesOctree = !input.m_writeOutputFilesOctree;
    input.m_outputFilesOctreePrefix += "xyz";
    input.m_outputFilesOctreeMeshList.resize(2);
    return input;
}

//! Test of serialization
TEST(TaskSpaceEvaluatorInput, Serialization)
{
    // Setup input
    TaskSpaceEvaluatorInput<1> input = createTestInput<1>();

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

//! Test of setupTaskSpaceSelectionMatrix()
TEST(TaskSpaceEvaluatorInput, SetupTaskSpaceSelectionMatrix)
{
    // Initialize helpers
    TaskSpaceEvaluatorInput<1> input;

    // Test count of rows
    input.setupTaskSpaceSelectionMatrix(true, false, true, false, true, false);
    ASSERT_EQ(input.m_taskSpaceSelectionMatrix.rows(), 3);

    // Test mapping
    input.setupTaskSpaceSelectionMatrix(true, true, true, true, true, true);
    for (Eigen::Index r = 0; r < 6; r++) {
        for (Eigen::Index c = 0; c < 6; c++) {
            if (r == c)
                ASSERT_EQ(input.m_taskSpaceSelectionMatrix(r, c), 1);
            else
                ASSERT_EQ(input.m_taskSpaceSelectionMatrix(r, c), 0);
        }
    }
}

//! Test of setupOutputMeshes()
TEST(TaskSpaceEvaluatorInput, SetupOutputMeshes)
{
    // Initialize helpers
    TaskSpaceEvaluatorInput<1> input;
    input.setupOutputMeshesDefault();
    ASSERT_GT(input.m_outputFilesOctreeMeshList.size(), 0);
    input.m_outputFilesOctreeMeshList.clear();
    input.setupOutputMeshesAll();
    ASSERT_GT(input.m_outputFilesOctreeMeshList.size(), 0);
}

#endif // HAVE_EIGEN3
