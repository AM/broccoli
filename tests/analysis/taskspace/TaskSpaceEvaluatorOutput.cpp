/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceEvaluatorOutput.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Test of serialization
TEST(TaskSpaceEvaluatorOutput, Serialization)
{
    // Setup input
    TaskSpaceEvaluatorOutput<2> input;
    input.boundingBoxPreSampling().addSample(Eigen::Vector3d(1, 2, 3));
    input.boundingBoxPreSampling().addSample(Eigen::Vector3d(2, 3, 4));
    input.boundingBoxMainSampling().addSample(Eigen::Vector3d(-1, -2, -3));
    input.boundingBoxMainSampling().addSample(Eigen::Vector3d(-2, -3, -4));

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
