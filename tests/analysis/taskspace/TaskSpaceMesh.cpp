/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceMesh.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Test converstion to string representation of type
TEST(TaskSpaceMesh, toString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(TaskSpaceMesh::Type::TYPE_COUNT); i++) {
        ASSERT_GT(TaskSpaceMesh::toString(static_cast<TaskSpaceMesh::Type>(i)).size(), 0);
        ASSERT_TRUE(TaskSpaceMesh::toString(static_cast<TaskSpaceMesh::Type>(i)) != "UNKNOWN");
    }
#ifdef NDEBUG
    ASSERT_TRUE(TaskSpaceMesh::toString(TaskSpaceMesh::Type::TYPE_COUNT) == "UNKNOWN");
#endif
}

#endif // HAVE_EIGEN3
