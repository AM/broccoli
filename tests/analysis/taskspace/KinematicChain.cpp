/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/KinematicChain.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Test of operators
TEST(KinematicChain, Operators)
{
    // Initialize helpers
    KinematicChain chainA;
    KinematicChain chainB;
    KinematicChain::SegmentList list;
    list.reserve(2);
    list.push_back(KinematicChainSegment("a"));
    list.push_back(KinematicChainSegment("b", "a"));
    chainB.set(list);

    // Other segment list
    ASSERT_TRUE(chainA != chainB);
    ASSERT_FALSE(chainA == chainB);

    // Same
    KinematicChain chainC = chainB;
    ASSERT_TRUE(chainC == chainB);
    ASSERT_FALSE(chainC != chainB);
}

//! Test of method set()
TEST(KinematicChain, set)
{
    // Initialize helpers
    TaskSpaceEvaluatorResult::Type result = TaskSpaceEvaluatorResult::Type::UNKNOWN;
    KinematicChain chain;
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(2);

#ifdef NDEBUG
    // Test empty list
    segmentList.push_back(KinematicChainSegment("a"));
    ASSERT_FALSE(chain.set(segmentList, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_INVALID_SEGMENT_COUNT);

    // Test invalid segment
    segmentList.push_back(KinematicChainSegment("b", "a"));
    segmentList.back().dofType() = KinematicChainSegment::DOFType::DOFTYPE_COUNT;
    ASSERT_FALSE(chain.set(segmentList, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_DOFTYPE);
    segmentList.back().dofType() = KinematicChainSegment::DOFType::ROTATION;

    // Test two segments with the same name
    segmentList[1] = KinematicChainSegment("a");
    ASSERT_FALSE(chain.set(segmentList, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_DUPLICATE_SEGMENT_NAME);

    // Test root has parent
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a", "x"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_FALSE(chain.set(segmentList, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_ROOT_HAS_PARENT);

    // Test parent child mismatch
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "c"));
    ASSERT_FALSE(chain.set(segmentList, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_PARENT_CHILD_MISMATCH);
#endif

    // Success
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_TRUE(chain.set(segmentList, &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);
}

//! Test of method clear()
TEST(KinematicChain, clear)
{
    // Initialize helpers
    KinematicChain chain;
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(2);
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    chain.set(segmentList);
    ASSERT_GT(chain.segments().size(), 0);
    chain.clear();
    ASSERT_EQ(chain.segments().size(), 0);
}

//! Test of method assemble()
TEST(KinematicChain, assemble)
{
    // Initialize helpers
    TaskSpaceEvaluatorResult::Type result = TaskSpaceEvaluatorResult::Type::UNKNOWN;
    KinematicChain chain;
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(2);

#ifdef NDEBUG
    // Test invalid segment count
    segmentList.push_back(KinematicChainSegment("a"));
    ASSERT_FALSE(chain.assemble(segmentList, "a", "b", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_INVALID_SEGMENT_COUNT);

    // Test invalid tcp segment name
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_FALSE(chain.assemble(segmentList, "a", "", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_INVALID_TCP_NAME);

    // Test invalid root segment name
    ASSERT_FALSE(chain.assemble(segmentList, "a", "a", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_INVALID_ROOT_NAME);

    // Test invalid segment
    segmentList[1].dofType() = KinematicChainSegment::DOFType::DOFTYPE_COUNT;
    ASSERT_FALSE(chain.assemble(segmentList, "a", "b", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_DOFTYPE);
    segmentList[1].dofType() = KinematicChainSegment::DOFType::ROTATION;

    // Test duplicate segment name
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("a", "c"));
    ASSERT_FALSE(chain.assemble(segmentList, "a", "b", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_DUPLICATE_SEGMENT_NAME);
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));

    // Test root segment not found
    ASSERT_FALSE(chain.assemble(segmentList, "c", "b", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_ROOT_NOT_FOUND);

    // Test tcp segment not found
    ASSERT_FALSE(chain.assemble(segmentList, "a", "c", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_TCP_NOT_FOUND);

    // Test parent not found
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "c"));
    ASSERT_FALSE(chain.assemble(segmentList, "a", "b", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_CHAIN_PARENT_NOT_FOUND);
#endif

    // Test success (with explicit root)
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_TRUE(chain.assemble(segmentList, "a", "b", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);

    // Test success (with implicit root)
    segmentList.clear();
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_TRUE(chain.assemble(segmentList, "", "b", &result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);
}

//! Test of method toString()
TEST(KinematicChain, toString)
{
    // Initialize helpers
    KinematicChain chain;
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(2);
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_TRUE(chain.set(segmentList));

    // Encoding
    ASSERT_GT(chain.toString().size(), 0);
}

//! Test of setters and getters
TEST(KinematicChain, SettersAndGetters)
{
    // Initialize helpers
    KinematicChain chain;
    ASSERT_EQ(chain.totalSamples(), 0);
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(2);
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_TRUE(chain.set(segmentList));

    // Setters (non-const)
    chain.segment(0).dofType() = KinematicChainSegment::DOFType::ROTATION;
    chain.segment(0).samples() = 2;
    chain.segment(1).dofType() = KinematicChainSegment::DOFType::TRANSLATION;
    chain.segment(1).samples() = 3;

    // Getters (const)
    const KinematicChain constChain = chain;
    ASSERT_EQ(constChain.segments().size(), 2);
    ASSERT_EQ(constChain.segment(0).dofType(), KinematicChainSegment::DOFType::ROTATION);
    ASSERT_EQ(constChain.totalSamples(), 6);
    ASSERT_EQ(constChain.dofCount(), 2);
}

//! Test of serialization
TEST(KinematicChain, Serialization)
{
    // Setup input
    KinematicChain input;
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(2);
    segmentList.push_back(KinematicChainSegment("a"));
    segmentList.push_back(KinematicChainSegment("b", "a"));
    ASSERT_TRUE(input.set(segmentList));

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
