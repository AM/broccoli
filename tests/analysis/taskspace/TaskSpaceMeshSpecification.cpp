/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceMeshSpecification.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Creates a dummy object for testing
static inline TaskSpaceMeshSpecification createTestSpecification()
{
    TaskSpaceMeshSpecification specification;
    specification.m_name = "abc";
    specification.m_level++;
    specification.m_metricType = TaskSpaceMetric::Type::JOINT_RANGE_AVAILABILITY_MEAN;
    specification.m_meshType = TaskSpaceMesh::Type::VOXEL_BOX_SMOOTH;
    specification.m_positionMinimum = Eigen::Vector3d(0.1, 0.2, 0.3);
    specification.m_positionMaximum = Eigen::Vector3d(0.2, 0.3, 0.4);
    specification.m_metricMinimum = 1.123;
    specification.m_metricMaximum = 2.34;
    specification.m_voxelMeshScaling = Eigen::Vector3d(0.1, 0.2, 0.3);
    specification.m_icoSphereSubdivisions++;
    specification.m_metricVolumeThreshold += 2.0;
    specification.m_colorGradient.resize(3);
    specification.m_colorGradient[0] << 1, 2, 3, 4;
    specification.m_colorGradient[1] << 5, 6, 7, 8;
    specification.m_colorGradient[2] << 9, 10, 11, 12;
    specification.m_metricFirstColor += 12.0;
    specification.m_metricLastColor += 22.0;
    return specification;
}

//! Test of operators
TEST(TaskSpaceMeshSpecification, Operators)
{
    TaskSpaceMeshSpecification specA = createTestSpecification();
    TaskSpaceMeshSpecification specB = specA;
    ASSERT_TRUE(specB == specA);
    ASSERT_FALSE(specB != specA);
    specB.m_colorGradient[0](0, 0) += 1;
    ASSERT_TRUE(specB != specA);
    ASSERT_FALSE(specB == specA);
}

//! Test of serialization
TEST(TaskSpaceMeshSpecification, Serialization)
{
    // Setup input
    TaskSpaceMeshSpecification input = createTestSpecification();

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
