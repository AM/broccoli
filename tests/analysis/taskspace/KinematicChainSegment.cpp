/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/KinematicChainSegment.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Test converstion of DOF type to string representation
TEST(KinematicChainSegment, DOFTypeToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(KinematicChainSegment::DOFType::DOFTYPE_COUNT); i++) {
        ASSERT_GT(KinematicChainSegment::toString(static_cast<KinematicChainSegment::DOFType>(i)).size(), 0);
        ASSERT_TRUE(KinematicChainSegment::toString(static_cast<KinematicChainSegment::DOFType>(i)) != "UNKNOWN");
    }
#ifdef NDEBUG
    ASSERT_TRUE(KinematicChainSegment::toString(KinematicChainSegment::DOFType::DOFTYPE_COUNT) == "UNKNOWN");
#endif
}

//! Test of operators
TEST(KinematicChainSegment, Operators)
{
    // Initialize helpers
    KinematicChainSegment segmentA("a");
    KinematicChainSegment segmentB("b");

    // Other name
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);

    // Other parent
    segmentA = KinematicChainSegment("a", "b");
    segmentB = KinematicChainSegment("a", "c");
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);
    segmentA = KinematicChainSegment("a", "b");
    segmentB = KinematicChainSegment("a", "b");

    // Other DOF type
    segmentB = segmentA;
    segmentB.dofType() = KinematicChainSegment::DOFType::ROTATION;
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);

    // Other initial transform
    segmentB = segmentA;
    segmentB.initialTransform().translate(Eigen::Vector3d(1, 2, 3));
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);
    segmentB = segmentA;
    segmentB.initialTransform().rotate(Eigen::AngleAxisd(1.0, Eigen::Vector3d(1, 0, 0)));
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);

    // Other minimum/maximum position
    segmentB = segmentA;
    segmentB.minimumPosition() += 1.0;
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);
    segmentB = segmentA;
    segmentB.maximumPosition() += 1.0;
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);

    // Other samples
    segmentB = segmentA;
    segmentB.samples() += 10;
    ASSERT_TRUE(segmentA != segmentB);
    ASSERT_FALSE(segmentA == segmentB);

    // Same
    segmentB = segmentA;
    ASSERT_TRUE(segmentA == segmentB);
    ASSERT_FALSE(segmentA != segmentB);
}

//! Test of method isValid()
TEST(KinematicChainSegment, isValid)
{
    // Initialize helpers
    TaskSpaceEvaluatorResult::Type result = TaskSpaceEvaluatorResult::Type::UNKNOWN;
    KinematicChainSegment segment("");

    // Invalid name
    segment = KinematicChainSegment("");
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_NAME);

    // Invalid parent
    segment = KinematicChainSegment("a", "a");
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_PARENT);

    // Invalid DOF type
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::DOFTYPE_COUNT;
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_DOFTYPE);

    // FIXED: invalid limits
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::FIXED;
    segment.minimumPosition() = 0;
    segment.maximumPosition() = 1.0;
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_LIMITS);

    // FIXED: invalid limits
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::FIXED;
    segment.samples() = 10;
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_SAMPLES);

    // ROTATION - TRANSLATION: invalid samples
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    segment.samples() = 0;
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_SAMPLES);

    // ROTATION - TRANSLATION: invalid limits
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    segment.samples() = 1;
    segment.minimumPosition() = 1;
    segment.maximumPosition() = 2;
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_LIMITS);
    segment.samples() = 2;
    segment.minimumPosition() = 2;
    segment.maximumPosition() = 1;
    ASSERT_FALSE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::ERROR_SEGMENT_INVALID_LIMITS);

    // FIXED: valid
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::FIXED;
    segment.samples() = 1;
    ASSERT_TRUE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);

    // ROTATION - TRANSLATION: valid
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    segment.samples() = 1;
    segment.minimumPosition() = 1;
    segment.maximumPosition() = 1;
    ASSERT_TRUE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);
    segment.samples() = 10;
    segment.minimumPosition() = 1;
    segment.maximumPosition() = 10;
    ASSERT_TRUE(segment.isValid(&result));
    ASSERT_EQ(result, TaskSpaceEvaluatorResult::Type::SUCCESS);
}

//! Test of method toString()
TEST(KinematicChainSegment, toString)
{
    // Initialize helpers
    KinematicChainSegment segment("a");

    // Without parent
    segment = KinematicChainSegment("a");
    ASSERT_GT(segment.toString().size(), 0);

    // With parent
    segment = KinematicChainSegment("a", "b");
    ASSERT_GT(segment.toString().size(), 0);

    // Rotational
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    ASSERT_GT(segment.toString().size(), 0);

    // Translational
    segment = KinematicChainSegment("a", "b");
    segment.dofType() = KinematicChainSegment::DOFType::TRANSLATION;
    ASSERT_GT(segment.toString().size(), 0);
}

//! Test of setters and getters
TEST(KinematicChainSegment, SettersAndGetters)
{
    // Initialize helpers
    KinematicChainSegment segment("a", "b");

    // Setters (non-const)
    segment.unlinkParent();
    segment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    Eigen::Isometry3d testTransform = Eigen::Isometry3d::Identity();
    testTransform.rotate(Eigen::AngleAxisd(0.1, Eigen::Vector3d(1, 0, 0)));
    testTransform.translate(Eigen::Vector3d(1, 2, -3));
    segment.initialTransform() = testTransform;
    segment.minimumPosition() = 0;
    segment.maximumPosition() = 1;
    segment.samples() = 11;

    // Getters (const)
    const KinematicChainSegment constSegment = segment;
    ASSERT_TRUE(constSegment.name() == "a");
    ASSERT_TRUE(constSegment.parent() == "");
    ASSERT_TRUE(constSegment.dofType() == KinematicChainSegment::DOFType::ROTATION);
    ASSERT_TRUE(constSegment.initialTransform().linear().isApprox(testTransform.linear()));
    ASSERT_TRUE(constSegment.initialTransform().translation().isApprox(testTransform.translation()));
    ASSERT_TRUE(constSegment.minimumPosition() == 0);
    ASSERT_TRUE(constSegment.maximumPosition() == 1);
    ASSERT_TRUE(constSegment.samples() == 11);
    ASSERT_TRUE(constSegment.stepSize() == 0.1);
}

//! Test of serialization
TEST(KinematicChainSegment, Serialization)
{
    // Setup input
    KinematicChainSegment input("abc", "def");
    input.dofType() = KinematicChainSegment::DOFType::ROTATION;
    input.initialTransform().rotate(Eigen::AngleAxisd(1.23, Eigen::Vector3d(1.1, 1.2, 1.3)));
    input.initialTransform().translate(Eigen::Vector3d(0.5, -0.1, 2.3));
    input.minimumPosition() = -1.23;
    input.maximumPosition() = 4.56;
    input.samples() = 987;

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
