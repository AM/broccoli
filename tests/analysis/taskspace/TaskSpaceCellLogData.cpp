/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceCellLogData.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;
using namespace io;

//! Test of encodeColumnDataToLogStream()
TEST(TaskSpaceCellLogData, encodeColumnDataToLogStream)
{
    // Initialize helpers
    TaskSpaceCell cell;
    TaskSpaceCellLogData dataA;
    TaskSpaceCellLogData dataB(1, std::array<size_t, 3>{ 1, 2, 3 }, Eigen::Vector3d(0.1, 0.2, 0.3), cell);
    LogFileData::LogStream logStream;
    ColumnBasedLogStream headerStream(logStream, true);
    ColumnBasedLogStream dataStream(logStream, false);

    // Encode header
    logStream.clear();
    dataA.encodeColumnDataToLogStream(headerStream);
    dataB.encodeColumnDataToLogStream(headerStream);

    // Encode data
    logStream.clear();
    dataA.encodeColumnDataToLogStream(dataStream);
    dataB.encodeColumnDataToLogStream(dataStream);
}

#endif // HAVE_EIGEN3
