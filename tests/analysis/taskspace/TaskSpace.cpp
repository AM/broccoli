/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpace.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "broccoli/analysis/taskspace/TaskSpaceEvaluatorInput.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Mockup-class to give access to protected members
class TaskSpaceSampleMockup : public TaskSpaceSample {
public:
    auto& position() { return m_position; }
    auto& conditionIndex() { return m_conditionIndex; }
    auto& manipulabilityMeasure() { return m_manipulabilityMeasure; }
    auto& jointRangeAvailability() { return m_jointRangeAvailability; }
};

//! Mockup-class to give access to protected members
template <unsigned int L>
class TaskSpaceMockup : public TaskSpace<L> {
public:
    using TaskSpace<L>::m_octree;
    using TaskSpace<L>::m_droppedSamples;
    auto& octree() { return m_octree; }
    auto& droppedSamples() { return m_droppedSamples; }
};

//! Creates a dummy object for testing
static inline TaskSpaceMockup<2> createTestTaskSpace()
{
    TaskSpaceMockup<2> taskSpace;
    taskSpace.octree().resize(2, 2, 2);
    for (size_t x = 0; x < 4; x++) {
        for (size_t y = 0; y < 4; y++) {
            for (size_t z = 0; z < 4; z++) {
                TaskSpaceCell& currentCell = taskSpace.octree()(1, x, y, z);

                // Compute sample
                TaskSpaceSampleMockup currentSample;
                currentSample.conditionIndex() = x;
                currentSample.manipulabilityMeasure() = y;
                currentSample.jointRangeAvailability() = z;

                // Add sample to cell
                currentCell.addSample(currentSample);
            }
        }
    }
    taskSpace.octree().m_center += Eigen::Vector3d(-0.1, 0.2, -0.3);
    taskSpace.octree().m_dimensions += Eigen::Vector3d(0.3, 0.4, 0.5);
    taskSpace.droppedSamples()++;
    taskSpace.update();
    return taskSpace;
}

//! Test of setup()
TEST(TaskSpace, Setup)
{
    // Initialize helpers
    TaskSpace<2> taskSpace;

    // Test setup
    ASSERT_TRUE(taskSpace.setup(Eigen::Vector3d(0, 0, 0), Eigen::Vector3d(1, 1, 1), Eigen::Vector3d(0.1, 0.1, 0.1), 0));
    ASSERT_TRUE(taskSpace.size(0) == (std::array<size_t, 3>{ { 5, 5, 5 } }));
    ASSERT_TRUE(taskSpace.size(1) == (std::array<size_t, 3>{ { 10, 10, 10 } }));
    ASSERT_TRUE(taskSpace.center().isApprox(Eigen::Vector3d(0.5, 0.5, 0.5)));
    ASSERT_TRUE(taskSpace.dimensions().isApprox(Eigen::Vector3d(1, 1, 1)));
}

//! Test of reset()
TEST(TaskSpace, Reset)
{
    // Initialize helpers
    TaskSpace<2> taskSpace = createTestTaskSpace();

    // Test reset
    taskSpace.reset();
    ASSERT_EQ(taskSpace.droppedSamples(), 0);
    for (size_t l = 0; l < 2; l++)
        for (size_t i = 0; i < taskSpace.cellCount(l); i++)
            ASSERT_EQ(taskSpace.cell(l, i).m_totalSamples, 0);
}

//! Test of addSamples() and update()
TEST(TaskSpace, AddSamplesAndUpdate)
{
    // Initialize helpers
    TaskSpace<2> taskSpace = createTestTaskSpace();
    taskSpace.reset();

    // Create sample list
    TaskSpace<2>::SampleList samples;
    samples.reserve(2);
    TaskSpaceSampleMockup firstSample, secondSample;
    firstSample.position() = taskSpace.center();
    firstSample.conditionIndex() = 1;
    firstSample.manipulabilityMeasure() = 3;
    firstSample.jointRangeAvailability() = 5;
    secondSample.position() = taskSpace.center();
    secondSample.conditionIndex() = 2;
    secondSample.manipulabilityMeasure() = 4;
    secondSample.jointRangeAvailability() = 6;
    samples.push_back(firstSample);
    samples.push_back(secondSample);

    // Test adding samples
    taskSpace.addSamples(samples);
    ASSERT_EQ(taskSpace.metaCell().m_totalSamples, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_conditionIndexMinimum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_conditionIndexMean, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_conditionIndexMaximum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_manipulabilityMeasureMinimum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_manipulabilityMeasureMean, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_manipulabilityMeasureMaximum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_jointRangeAvailabilityMinimum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_jointRangeAvailabilityMean, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_jointRangeAvailabilityMaximum, 0);

    // Test update
    taskSpace.update();
    ASSERT_EQ(taskSpace.metaCell().m_totalSamples, 2);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_conditionIndexMinimum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_conditionIndexMean, 1.5);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_conditionIndexMaximum, 2);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_manipulabilityMeasureMinimum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_manipulabilityMeasureMean, 3.5);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_manipulabilityMeasureMaximum, 4);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_jointRangeAvailabilityMinimum, 0);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_jointRangeAvailabilityMean, 5.5);
    ASSERT_FLOAT_EQ(taskSpace.metaCell().m_jointRangeAvailabilityMaximum, 6);
}

//! Test of createMesh()
TEST(TaskSpace, createMesh)
{
    // Setup taskspace
    TaskSpaceMockup<2> taskSpace;
    taskSpace.octree().resize(2, 2, 2);
    for (size_t x = 0; x < 2; x++) {
        for (size_t y = 0; y < 2; y++) {
            for (size_t z = 0; z < 2; z++) {
                TaskSpaceCell& currentCell = taskSpace.octree()(1, x, y, z);

                // Compute sample
                TaskSpaceSampleMockup currentSample;
                currentSample.conditionIndex() = x;
                currentSample.manipulabilityMeasure() = y;
                currentSample.jointRangeAvailability() = z;

                // Add sample to cell
                currentCell.addSample(currentSample);
            }
        }
    }
    taskSpace.octree().m_center += Eigen::Vector3d(-0.1, 0.2, -0.3);
    taskSpace.octree().m_dimensions += Eigen::Vector3d(0.3, 0.4, 0.5);
    taskSpace.update();

    // Setup mesh specifications
    TaskSpaceEvaluatorInput<2> evaluatorInput;
    evaluatorInput.setupOutputMeshesAll();

    // Create meshes of all different types
    for (size_t i = 0; i < evaluatorInput.m_outputFilesOctreeMeshList.size(); i++) {
        auto mesh = taskSpace.createMesh(evaluatorInput.m_outputFilesOctreeMeshList[i]);
        (void)mesh;
    }
}

//! Test of serialization
TEST(TaskSpace, Serialization)
{
    // Setup input
    TaskSpace<2> input = createTestTaskSpace();

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
