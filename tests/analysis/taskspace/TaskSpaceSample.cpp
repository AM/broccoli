/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceSample.hpp"
#include "broccoli/analysis/taskspace/TaskSpaceSampleEvaluator.hpp"
#include "broccoli/geometry/DHParameters.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;
using namespace io;

//! Test of evaluate()
TEST(TaskSpaceSample, evaluate)
{
    // Initialize helpers
    const double l1 = 0.345;
    const double l2 = 0.123;
    const double translationDOF = 0.5;
    const double rotationDOF = M_PI_4;

    // Setup kinematic chain
    KinematicChain chain;
    KinematicChain::SegmentList segmentList;
    segmentList.reserve(4);
    KinematicChainSegment rootSegment("root");
    KinematicChainSegment translationSegment("translation", "root");
    translationSegment.dofType() = KinematicChainSegment::DOFType::TRANSLATION;
    translationSegment.initialTransform() = geometry::DHParameters(0.0, -M_PI_2, l1, 0.0).toTransform();
    translationSegment.minimumPosition() = 0.0;
    translationSegment.maximumPosition() = 1.0;
    translationSegment.samples() = 2;
    KinematicChainSegment rotationSegment("rotation", "translation");
    rotationSegment.dofType() = KinematicChainSegment::DOFType::ROTATION;
    rotationSegment.initialTransform() = geometry::DHParameters(0.0, -M_PI_2, 0, 0).toTransform();
    rotationSegment.minimumPosition() = 0.0;
    rotationSegment.maximumPosition() = M_PI_2;
    rotationSegment.samples() = 2;
    KinematicChainSegment tcpSegment("tcp", "rotation");
    tcpSegment.initialTransform().translation() = Eigen::Vector3d(l2, 0, 0);
    segmentList.push_back(rootSegment);
    segmentList.push_back(translationSegment);
    segmentList.push_back(rotationSegment);
    segmentList.push_back(tcpSegment);
    chain.set(segmentList);

    // Setup task space definition
    Eigen::Matrix<double, 2, 6> taskSpaceSelection;
    taskSpaceSelection << 1, 0, 0, 0, 0, 0, // translation x
        0, 0, 0, 0, 0, 1; // rotation z

    // Trigger evaluation
    TaskSpaceSample sample;
    Eigen::VectorXd configuration = Eigen::VectorXd::Zero(4);
    configuration(1) = translationDOF;
    configuration(2) = rotationDOF;
    TaskSpaceSampleEvaluator sampleEvaluator(chain, taskSpaceSelection);
    sampleEvaluator.evaluate(123, configuration, sample);

    // Compute via DH
    const Eigen::Isometry3d D_root_translation = geometry::DHParameters(0.0, -M_PI_2, l1 + translationDOF, 0.0).toTransform();
    const Eigen::Isometry3d D_translation_rotation = geometry::DHParameters(0.0, -M_PI_2, 0, rotationDOF).toTransform();
    const Eigen::Isometry3d D_rotation_tcp = geometry::DHParameters(l2, 0, 0, 0).toTransform();
    const Eigen::Isometry3d D_root_tcp = D_root_translation * D_translation_rotation * D_rotation_tcp;

    // Check sample index
    ASSERT_EQ(sample.sampleIndex(), 123);

    // Check configuration
    ASSERT_TRUE(sample.configuration().isApprox(configuration));

    // Check position
    const Eigen::Vector3d expectedPosition = D_root_tcp.translation();
    ASSERT_TRUE(sample.position().isApprox(expectedPosition));

    // Check orientation
    const Eigen::Quaterniond expectedOrientation(D_root_tcp.linear());
    ASSERT_LT(fabs(2.0 * acos(sample.orientation().dot(expectedOrientation))), 1e-4);

    // Check jacobian of translation
    Eigen::Matrix<double, 3, 2> expectedJacobianTranslation;
    expectedJacobianTranslation << 0, -l2 * sin(rotationDOF), //
        1, -l2 * cos(rotationDOF), //
        0, 0;
    ASSERT_EQ(sample.jacobianTranslation().cols(), expectedJacobianTranslation.cols());
    ASSERT_TRUE(sample.jacobianTranslation().isApprox(expectedJacobianTranslation));

    // Check jacobian of rotation
    Eigen::Matrix<double, 3, 2> expectedJacobianRotation;
    expectedJacobianRotation << 0, 0, //
        0, 0, //
        0, -1;
    ASSERT_EQ(sample.jacobianRotation().cols(), expectedJacobianRotation.cols());
    ASSERT_TRUE(sample.jacobianRotation().isApprox(expectedJacobianRotation));

    // Check task-space jacobian
    Eigen::Matrix<double, 2, 2> expectedJacobianTaskSpace;
    expectedJacobianTaskSpace << 0, -l2 * sin(rotationDOF), //
        0, -1;
    ASSERT_EQ(sample.jacobianTaskSpace().cols(), expectedJacobianTaskSpace.cols());
    ASSERT_TRUE(sample.jacobianTaskSpace().isApprox(expectedJacobianTaskSpace));

    // Check condition index
    const Eigen::JacobiSVD<Eigen::MatrixXd> svdJ(expectedJacobianTaskSpace, Eigen::ComputeFullU | Eigen::ComputeFullV);
    const double expectedConditionIndex = svdJ.singularValues().minCoeff() / svdJ.singularValues().maxCoeff();
    ASSERT_TRUE(fabs(sample.conditionIndex() - expectedConditionIndex) < 1e-9);

    // Check manipulability measure
    const Eigen::MatrixXd JJtransposed = expectedJacobianTaskSpace * expectedJacobianTaskSpace.transpose();
    const double det_JJtransposed = JJtransposed.determinant();
    const double expectedManipulabilityMeasure = sqrt(det_JJtransposed);
    ASSERT_TRUE(fabs(sample.manipulabilityMeasure() - expectedManipulabilityMeasure) < 1e-9);

    // Check joint range availability
    double expectedJointRangeAvailability = 1.0;
    expectedJointRangeAvailability *= std::min(translationSegment.maximumPosition() - translationDOF, translationDOF - translationSegment.minimumPosition()) / (0.5 * (translationSegment.maximumPosition() - translationSegment.minimumPosition()));
    expectedJointRangeAvailability *= std::min(rotationSegment.maximumPosition() - rotationDOF, rotationDOF - rotationSegment.minimumPosition()) / (0.5 * (rotationSegment.maximumPosition() - rotationSegment.minimumPosition()));
    expectedJointRangeAvailability = sqrt(expectedJointRangeAvailability);
    ASSERT_TRUE(fabs(sample.jointRangeAvailability() - expectedJointRangeAvailability) < 1e-9);
}

//! Test of encodeColumnDataToLogStream()
TEST(TaskSpaceSample, encodeColumnDataToLogStream)
{
    // Initialize helpers
    TaskSpaceSample sample;
    LogFileData::LogStream logStream;
    ColumnBasedLogStream headerStream(logStream, true);
    ColumnBasedLogStream dataStream(logStream, false);

    // Encode header
    logStream.clear();
    sample.encodeColumnDataToLogStream(headerStream);

    // Encode data
    logStream.clear();
    sample.encodeColumnDataToLogStream(dataStream);
}

#endif // HAVE_EIGEN3
