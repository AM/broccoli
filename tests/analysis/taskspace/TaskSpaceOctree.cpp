/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

// This module requires Eigen library
#ifdef HAVE_EIGEN3

#include "broccoli/analysis/taskspace/TaskSpaceOctree.hpp"
#include "../../io/serialization/SerializableDataTester.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace analysis;

//! Mockup-class to give access to protected members
class TaskSpaceSampleMockup : public TaskSpaceSample {
public:
    double& conditionIndex() { return m_conditionIndex; }
    double& manipulabilityMeasure() { return m_manipulabilityMeasure; }
    double& jointRangeAvailability() { return m_jointRangeAvailability; }
};

//! Creates a dummy object for testing
static inline TaskSpaceOctree<2> createTestOctree()
{
    TaskSpaceOctree<2> octree;
    octree.resize(2, 2, 2);
    for (size_t x = 0; x < 4; x++) {
        for (size_t y = 0; y < 4; y++) {
            for (size_t z = 0; z < 4; z++) {
                TaskSpaceCell& currentCell = octree(1, x, y, z);

                // Compute sample
                TaskSpaceSampleMockup currentSample;
                currentSample.conditionIndex() = x;
                currentSample.manipulabilityMeasure() = y;
                currentSample.jointRangeAvailability() = z;

                // Add sample to cell
                currentCell.addSample(currentSample);
            }
        }
    }
    octree.m_center += Eigen::Vector3d(-0.1, 0.2, -0.3);
    octree.m_dimensions += Eigen::Vector3d(0.3, 0.4, 0.5);

    return octree;
}

//! Test of propagate bottom-up and update of meta cell
TEST(TaskSpaceOctree, PropagateBottomUpAndUpdateMetaCell)
{
    // Setup input
    TaskSpaceOctree<2> input = createTestOctree();

    // Test propagate up
    input.propagateBottomUp();
    for (size_t x = 0; x < 2; x++) {
        for (size_t y = 0; y < 2; y++) {
            for (size_t z = 0; z < 2; z++) {
                TaskSpaceCell& currentCell = input(0, x, y, z);

                // Compute linked lower level cells
                std::array<TaskSpaceCell*, 8> lowerLevelCells;
                lowerLevelCells[0] = &input(1, 2 * x, 2 * y, 2 * z);
                lowerLevelCells[1] = &input(1, 2 * x, 2 * y, 2 * z + 1);
                lowerLevelCells[2] = &input(1, 2 * x, 2 * y + 1, 2 * z);
                lowerLevelCells[3] = &input(1, 2 * x, 2 * y + 1, 2 * z + 1);
                lowerLevelCells[4] = &input(1, 2 * x + 1, 2 * y, 2 * z);
                lowerLevelCells[5] = &input(1, 2 * x + 1, 2 * y, 2 * z + 1);
                lowerLevelCells[6] = &input(1, 2 * x + 1, 2 * y + 1, 2 * z);
                lowerLevelCells[7] = &input(1, 2 * x + 1, 2 * y + 1, 2 * z + 1);

                const uint64_t expectedTotalSamples = 8;
                const double expectedConditionIndexMinimum = 2 * x;
                const double expectedConditionIndexMean = (4 * (2 * x) + 4 * (2 * x + 1)) / 8.0;
                const double expectedConditionIndexMaximum = 2 * x + 1;
                const double expectedManipulabilityMeasureMinimum = 2 * y;
                const double expectedManipulabilityMeasureMean = (4 * (2 * y) + 4 * (2 * y + 1)) / 8.0;
                const double expectedManipulabilityMeasureMaximum = 2 * y + 1;
                const double expectedJointRangeAvailabilityMinimum = 2 * z;
                const double expectedJointRangeAvailabilityMean = (4 * (2 * z) + 4 * (2 * z + 1)) / 8.0;
                const double expectedJointRangeAvailabilityMaximum = 2 * z + 1;

                // Check values
                ASSERT_EQ(currentCell.m_totalSamples, expectedTotalSamples);
                ASSERT_FLOAT_EQ(currentCell.m_conditionIndexMinimum, expectedConditionIndexMinimum);
                ASSERT_FLOAT_EQ(currentCell.m_conditionIndexMean, expectedConditionIndexMean);
                ASSERT_FLOAT_EQ(currentCell.m_conditionIndexMaximum, expectedConditionIndexMaximum);
                ASSERT_FLOAT_EQ(currentCell.m_manipulabilityMeasureMinimum, expectedManipulabilityMeasureMinimum);
                ASSERT_FLOAT_EQ(currentCell.m_manipulabilityMeasureMean, expectedManipulabilityMeasureMean);
                ASSERT_FLOAT_EQ(currentCell.m_manipulabilityMeasureMaximum, expectedManipulabilityMeasureMaximum);
                ASSERT_FLOAT_EQ(currentCell.m_jointRangeAvailabilityMinimum, expectedJointRangeAvailabilityMinimum);
                ASSERT_FLOAT_EQ(currentCell.m_jointRangeAvailabilityMean, expectedJointRangeAvailabilityMean);
                ASSERT_FLOAT_EQ(currentCell.m_jointRangeAvailabilityMaximum, expectedJointRangeAvailabilityMaximum);
            }
        }
    }

    // Test update of metacell
    input.updateMetaCell();
    ASSERT_EQ(input.m_metaCell.m_totalSamples, 4 * 4 * 4);
    if (input.m_metaCell.m_totalSamples > 0) {
        ASSERT_FLOAT_EQ(input.m_metaCell.m_conditionIndexMinimum, 0);
        ASSERT_FLOAT_EQ(input.m_metaCell.m_conditionIndexMean, 4 * 4 * (0.0 + 1.0 + 2.0 + 3.0) / ((double)input.m_metaCell.m_totalSamples));
        ASSERT_FLOAT_EQ(input.m_metaCell.m_conditionIndexMaximum, 3);
        ASSERT_FLOAT_EQ(input.m_metaCell.m_manipulabilityMeasureMinimum, 0);
        ASSERT_FLOAT_EQ(input.m_metaCell.m_manipulabilityMeasureMean, 4 * 4 * (0.0 + 1.0 + 2.0 + 3.0) / ((double)input.m_metaCell.m_totalSamples));
        ASSERT_FLOAT_EQ(input.m_metaCell.m_manipulabilityMeasureMaximum, 3);
        ASSERT_FLOAT_EQ(input.m_metaCell.m_jointRangeAvailabilityMinimum, 0);
        ASSERT_FLOAT_EQ(input.m_metaCell.m_jointRangeAvailabilityMean, 4 * 4 * (0.0 + 1.0 + 2.0 + 3.0) / ((double)input.m_metaCell.m_totalSamples));
        ASSERT_FLOAT_EQ(input.m_metaCell.m_jointRangeAvailabilityMaximum, 3);
    }
}

//! Test of serialization
TEST(TaskSpaceOctree, Serialization)
{
    // Setup input
    TaskSpaceOctree<2> input = createTestOctree();

    // Test serialization
    testSerializableData(input, input.computeBinaryStreamSize());
}

#endif // HAVE_EIGEN3
