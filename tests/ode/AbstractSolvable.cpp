/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "SpringDamperMassSystem.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <broccoli/ode/integration/AbstractSolvable.hpp>
#include <broccoli/ode/integration/AbstractSolvableEntity.hpp>
#include <broccoli/ode/integration/AbstractTimeDiscreteSolvableEntity.hpp>
#include <broccoli/ode/integration/SequentiallySolvableSystem.hpp>
#include <broccoli/ode/integration/solvers/AbstractFixedStepSystemSolver.hpp>
#include <broccoli/ode/integration/solvers/AbstractSystemSolver.hpp>
#include <broccoli/ode/integration/solvers/ForwardEulerSystemSolver.hpp>

using namespace broccoli;
using namespace ode;
using namespace ::testing;
using ::testing::AllOf;
using ::testing::Args;
using ::testing::Lt;
using ::testing::Return;

//! Mock for AbstractSolvableListener
class MockAbstractSolvableListener : public AbstractSolvableListener {
public:
    MOCK_METHOD1(init, void(const double& time));
    MOCK_METHOD0(finally, void());
    MOCK_METHOD2(before, void(const double& t0, const double& t1));
    MOCK_METHOD2(after, void(const double& t0, const double& t1));
};

//! Mock for AbstractSolvable
class MockAbstractSolvable : public AbstractSolvable {
public:
    MOCK_METHOD1(init, void(const double& time));
    MOCK_METHOD0(finally, void());
    MOCK_METHOD2(before, void(const double& t0, const double& t1));
    MOCK_METHOD2(after, void(const double& t0, const double& t1));
    MOCK_CONST_METHOD0(sampleTime, core::IntegerNanoSeconds<uint64_t>());
    MOCK_METHOD3(integrateWith, void(AbstractSystemSolver* solver, const double& t0, const double& t1));

    void concreteInit(const double& time)
    {
        AbstractSolvable::init(time);
    }

    void concreteFinally()
    {
        AbstractSolvable::finally();
    }

    void concreteBefore(const double& t0, const double& t1)
    {
        AbstractSolvable::before(t0, t1);
    }

    void concreteAfter(const double& t0, const double& t1)
    {
        AbstractSolvable::after(t0, t1);
    }
};

//! Mock for AbstractSolvableEntity
class MockAbstractSolvableEntity : public AbstractSolvableEntity {
public:
    MOCK_METHOD2(before, void(const double& t0, const double& t1));
    MOCK_METHOD2(after, void(const double& t0, const double& t1));
    MOCK_METHOD0(state, Eigen::VectorXd&());
    MOCK_CONST_METHOD0(stateGradient, const Eigen::VectorXd&());
    MOCK_METHOD0(evaluateRHS, void());
    MOCK_CONST_METHOD0(stateVectorSize, std::size_t());
    MOCK_CONST_METHOD0(sampleTime, core::IntegerNanoSeconds<uint64_t>());
};

//! Mock for AbstractTimeDiscreteSolvableEntity
class MockAbstractTimeDiscreteSolvableEntity : public AbstractTimeDiscreteSolvableEntity {
public:
    MOCK_METHOD2(before, void(const double& t0, const double& t1));
    MOCK_METHOD2(after, void(const double& t0, const double& t1));
    MOCK_CONST_METHOD0(sampleTime, core::IntegerNanoSeconds<uint64_t>());
};

//! Mock for AbstractFixedStepSystemSolver
class MockAbstractFixedStepSystemSolver : public AbstractFixedStepSystemSolver {
public:
    MOCK_METHOD2(init, void(AbstractSolvable* solvable, const double& continuousStepTime));
    MOCK_METHOD2(atomicStep, void(AbstractSolvableEntity* entity, const double& dt));
};

//! Mock for AbstractSystemSolver
class MockAbstractSystemSolver : public AbstractSystemSolver {
public:
    MOCK_METHOD2(init, void(AbstractSolvable* solvable, const double& continuousStepTime));
    MOCK_METHOD3(integrateEntity, void(AbstractSolvableEntity* entity, const double& t0, const double& t1));
    MOCK_CONST_METHOD0(timeStep, double());
};

//! Mock for SequentiallySolvableSystem
class MockSequentiallySolvableSystem : public SequentiallySolvableSystem {
public:
    MOCK_METHOD2(before, void(const double& t0, const double& t1));
    MOCK_METHOD2(after, void(const double& t0, const double& t1));
    MOCK_CONST_METHOD0(sampleTime, core::IntegerNanoSeconds<uint64_t>());
};

//! Test the interface of AbstractSolvable
TEST(AbstractSolvableListener, EventInterface)
{
    MockAbstractSolvableListener listener;
    MockAbstractSolvable solvable;

    // Use the concrete implementations for init(), finally(), before() and after()
    // in this test
    ON_CALL(solvable, before(_, _))
        .WillByDefault(Invoke(&solvable, &MockAbstractSolvable::concreteBefore));

    ON_CALL(solvable, after(_, _))
        .WillByDefault(Invoke(&solvable, &MockAbstractSolvable::concreteAfter));

    ON_CALL(solvable, init(0.0))
        .WillByDefault(Invoke(&solvable, &MockAbstractSolvable::concreteInit));

    ON_CALL(solvable, finally())
        .WillByDefault(Invoke(&solvable, &MockAbstractSolvable::concreteFinally));

    EXPECT_CALL(listener, init(0.0))
        .Times(1);

    EXPECT_CALL(solvable, init(0.0))
        .Times(1);

    EXPECT_CALL(listener, finally())
        .Times(1);

    EXPECT_CALL(solvable, finally())
        .Times(1);

    EXPECT_CALL(listener, before(1.0, 2.0))
        .Times(1);

    EXPECT_CALL(listener, after(1.0, 2.0))
        .Times(1);

    EXPECT_CALL(solvable, before(1.0, 2.0))
        .Times(1);

    EXPECT_CALL(solvable, after(1.0, 2.0))
        .Times(1);

    solvable.addListener(&listener);

    solvable.init(0.0);
    solvable.before(1.0, 2.0);
    solvable.after(1.0, 2.0);
    solvable.finally();
}

//! Test AbstractSystemSolver's behavior
TEST(AbstractSystemSolver, solve)
{
    MockAbstractSolvable solvable;
    MockAbstractSystemSolver solver;

    ON_CALL(solvable, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.1)));

    EXPECT_CALL(solvable, init(DoubleEq(0.45)))
        .Times(1);

    EXPECT_CALL(solvable, finally())
        .Times(1);

    // Checks continuous step time is right
    EXPECT_CALL(solver, init(&solvable, DoubleEq(0.1)))
        .Times(1);

    EXPECT_CALL(solvable, sampleTime())
        .Times(AtLeast(1));

    EXPECT_CALL(solvable, integrateWith(&solver, _, _)).Times(0);
    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0), DoubleEq(0.1))).Times(1);
    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.1), DoubleEq(0.2))).Times(1);
    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.2), DoubleEq(0.3))).Times(1);
    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.3), DoubleEq(0.4))).Times(1);
    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.4), DoubleEq(0.45))).Times(1);
    solver.solve(&solvable, 0.45);
}

//! Test AbstractSystemSolver's behavior
TEST(AbstractSystemSolver, solveForLargeNumberOfSteps)
{
    NiceMock<MockAbstractSolvable> solvable;
    NiceMock<MockAbstractSystemSolver> solver;

    ON_CALL(solvable, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.001)));

    EXPECT_CALL(solvable, integrateWith(&solver, _, _)).Times(9999);
    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.999), DoubleEq(1.0))).Times(1);

    solver.solve(&solvable, 10.0);
}

//! Test AbstractSystemSolver's behavior
TEST(AbstractSystemSolver, solveOneStep)
{
    NiceMock<MockAbstractSolvable> solvable;
    NiceMock<MockAbstractSystemSolver> solver;

    ON_CALL(solvable, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(20.0)));

    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.0), DoubleEq(10.0))).Times(1);

    solver.solve(&solvable, 10.0);
}

//! Test AbstractFixedStepSystemSolver's behavior
TEST(AbstractFixedStepSystemSolver, integrateEntity)
{
    NiceMock<MockAbstractSolvableEntity> entity;
    NiceMock<MockAbstractFixedStepSystemSolver> solver;

    double dt = 0.001;
    double endTime = 10.0005;

    // Make the system continuous
    ON_CALL(entity, stateVectorSize())
        .WillByDefault(Return(1));

    ON_CALL(entity, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.001)));

    EXPECT_DOUBLE_EQ(solver.timeStep(), 0.0);
    solver.setTimeStep(dt);
    EXPECT_DOUBLE_EQ(solver.timeStep(), dt);

    {
        InSequence dummy;
        EXPECT_CALL(solver, atomicStep(&entity, DoubleNear(dt, 1e-15))).Times(10000);
        EXPECT_CALL(solver, atomicStep(&entity, DoubleNear(0.0005, 1e-15))).Times(1);
    }

    solver.solve(&entity, endTime);
    EXPECT_DOUBLE_EQ(entity.currentTime(), endTime);
}

//! Test AbstractFixedStepSystemSolver's behavior
TEST(AbstractFixedStepSystemSolver, integrateEntityOneBlock)
{
    NiceMock<MockAbstractSolvableEntity> entity;
    NiceMock<MockAbstractFixedStepSystemSolver> solver;

    double dt = 0.001;
    double endTime = 9.9995;

    // Make the system continuous
    ON_CALL(entity, stateVectorSize())
        .WillByDefault(Return(1));

    ON_CALL(entity, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.0)));

    EXPECT_DOUBLE_EQ(solver.timeStep(), 0.0);
    solver.setTimeStep(dt);
    EXPECT_DOUBLE_EQ(solver.timeStep(), dt);

    {
        InSequence dummy;
        EXPECT_CALL(solver, atomicStep(&entity, DoubleNear(dt, 1e-15))).Times(9999);
        EXPECT_CALL(solver, atomicStep(&entity, DoubleNear(0.0005, 1e-15))).Times(1);
    }

    solver.solve(&entity, endTime);
    EXPECT_DOUBLE_EQ(entity.currentTime(), endTime);
}

//! Test AbstractSolvableEntity
TEST(AbstractSolvableEntity, integrateWith)
{
    NiceMock<MockAbstractSolvableEntity> entity;
    MockAbstractSystemSolver solver;

    ON_CALL(entity, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.1)));

    ON_CALL(entity, stateVectorSize())
        .WillByDefault(Return(1));

    EXPECT_CALL(entity, sampleTime()).Times(AtLeast(1));

    {
        InSequence dummy;
        EXPECT_CALL(entity, before(DoubleEq(0.0), DoubleEq(0.1))).Times(1);
        EXPECT_CALL(solver, integrateEntity(&entity, DoubleEq(0.0), DoubleEq(0.1))).Times(1);
        EXPECT_CALL(entity, after(DoubleEq(0.0), DoubleEq(0.1))).Times(1);
    }

    {
        InSequence dummy;
        EXPECT_CALL(entity, before(DoubleEq(0.11), DoubleEq(0.21))).Times(0);
        EXPECT_CALL(solver, integrateEntity(&entity, DoubleEq(0.11), DoubleEq(0.21))).Times(1);
        EXPECT_CALL(entity, after(DoubleEq(0.11), DoubleEq(0.21))).Times(0);
    }

    // initialize entity
    entity.init(0.0);

    // multiple of the sample time
    entity.integrateWith(&solver, 0.0, 0.1);

    // not a multiple of the sample time
    entity.integrateWith(&solver, 0.11, 0.21);
}

//! Test AbstractTimeDiscreteSolvableEntity
TEST(AbstractTimeDiscreteSolvableEntity, integrateWith)
{
    MockAbstractTimeDiscreteSolvableEntity entity;
    MockAbstractSystemSolver solver;

    ON_CALL(entity, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.1)));

    EXPECT_CALL(entity, sampleTime()).Times(AtLeast(1));
    {
        InSequence dummy;
        EXPECT_CALL(entity, before(DoubleEq(0.0), DoubleEq(0.1))).Times(1);
        EXPECT_CALL(entity, after(DoubleEq(0.0), DoubleEq(0.1))).Times(1);
    }

    {
        InSequence dummy;
        EXPECT_CALL(entity, before(DoubleEq(0.11), DoubleEq(0.21))).Times(0);
        EXPECT_CALL(entity, after(DoubleEq(0.11), DoubleEq(0.21))).Times(0);
    }

    // initialize entity
    entity.init(0.0);

    // multiple of the sample time
    entity.integrateWith(&solver, 0.0, 0.1);

    // not a multiple of the sample time
    entity.integrateWith(&solver, 0.11, 0.21);

    EXPECT_THROW(entity.state(), std::runtime_error);
    EXPECT_THROW(entity.stateGradient(), std::runtime_error);
    EXPECT_THROW(entity.evaluateRHS(), std::runtime_error);
    EXPECT_EQ(entity.stateVectorSize(), 0);
}

//! Test SequentiallySolvableSystem
TEST(SequentiallySolvableSystem, Events)
{
    SequentiallySolvableSystem system;
    NiceMock<MockAbstractSolvable> solvable;
    NiceMock<MockAbstractSystemSolver> solver;

    EXPECT_CALL(solvable, init(0.0)).Times(1);
    EXPECT_CALL(solvable, finally()).Times(1);
    EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.1), DoubleEq(0.2))).Times(1);
    EXPECT_CALL(solvable, before(_, _)).Times(0);
    EXPECT_CALL(solvable, after(_, _)).Times(0);

    system.addChild(&solvable);

    system.init(0.0);
    system.integrateWith(&solver, 0.1, 0.2);
    system.finally();
}

//! Test SequentiallySolvableSystem
TEST(SequentiallySolvableSystem, sampleTime)
{
    SequentiallySolvableSystem system;
    MockAbstractSolvable solvable1;
    MockAbstractSolvable solvable2;
    MockAbstractSolvable solvable3;

    ON_CALL(solvable1, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.15)));

    ON_CALL(solvable2, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.1)));

    ON_CALL(solvable3, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.0)));

    EXPECT_CALL(solvable1, sampleTime()).Times(AtLeast(1));
    EXPECT_CALL(solvable2, sampleTime()).Times(AtLeast(1));
    EXPECT_CALL(solvable3, sampleTime()).Times(AtLeast(1));

    EXPECT_DOUBLE_EQ(solvable1.sampleTime().integer(), 15e07);
    EXPECT_DOUBLE_EQ(solvable2.sampleTime().integer(), 10e07);
    EXPECT_DOUBLE_EQ(solvable3.sampleTime().integer(), 0);

    EXPECT_DOUBLE_EQ(system.propagatedSampleTime().integer(), 0);

    system.addChild(&solvable1);
    EXPECT_DOUBLE_EQ(system.propagatedSampleTime().integer(), 15e07);

    system.addChild(&solvable2);
    EXPECT_DOUBLE_EQ(system.propagatedSampleTime().integer(), 5e07);

    system.addChild(&solvable3);
    EXPECT_DOUBLE_EQ(system.propagatedSampleTime().integer(), 5e07);

    system.removeChildren();
    EXPECT_DOUBLE_EQ(system.propagatedSampleTime().integer(), 0);
}

//! Test SequentiallySolvableSystem
TEST(SequentiallySolvableSystem, discreteInternals)
{
    NiceMock<MockSequentiallySolvableSystem> system;
    NiceMock<MockAbstractSolvable> solvable;
    NiceMock<MockAbstractSystemSolver> solver;

    ON_CALL(system, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.13)));

    ON_CALL(solvable, sampleTime())
        .WillByDefault(Return(core::IntegerNanoSeconds<uint64_t>(0.15)));

    {
        InSequence dummy;

        // 0.13 -> 0.26
        EXPECT_CALL(system, before(DoubleEq(0.13), DoubleEq(0.26))).Times(1);
        EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.13), DoubleEq(0.26))).Times(1);
        EXPECT_CALL(system, after(DoubleEq(0.13), DoubleEq(0.26))).Times(1);

        // 0.15 -> 0.3
        EXPECT_CALL(solvable, integrateWith(&solver, DoubleEq(0.15), DoubleEq(0.3))).Times(1);
    }

    system.addChild(&solvable);
    EXPECT_DOUBLE_EQ(system.propagatedSampleTime().integer(), 10000000);

    system.integrateWith(&solver, 0.13, 0.26);
    system.integrateWith(&solver, 0.15, 0.3);
}

//! Test an atomic step
TEST(ForwardEulerSystemSolver, atomicStep)
{
    NiceMock<MockAbstractSolvableEntity> entity;
    ForwardEulerSystemSolver solver;

    Eigen::VectorXd stateVector(2);
    Eigen::VectorXd gradientVector(2);
    stateVector.fill(0.0);
    gradientVector.fill(1.0);

    ON_CALL(entity, stateVectorSize())
        .WillByDefault(Return(1));

    ON_CALL(entity, state())
        .WillByDefault(ReturnRef(stateVector));

    ON_CALL(entity, stateGradient())
        .WillByDefault(ReturnRef(gradientVector));

    EXPECT_CALL(entity, evaluateRHS()).Times(1);

    solver.atomicStep(&entity, 0.1);

    EXPECT_EQ(stateVector(0), 0.1);
}

//! Test the Forward euler explicit solver with a spring-mass-damper system
TEST(ForwardEulerSystemSolver, springMassDamper)
{
    SpringDamperMassSystem system(4.0, 0.2, 0.1, 0.2, 0.5);
    ForwardEulerSystemSolver solver;
    solver.setTimeStep(0.00001);

    // Numerical Integration
    solver.solve(&system, 10.0);

    // Compare with analytical solution
    EXPECT_NEAR(system.state()[0], system.analyticalSolution(10.0), 0.00001);
    EXPECT_NEAR(system.state()[1], system.analyticalSolution(10.0, 1), 0.00001);
}
