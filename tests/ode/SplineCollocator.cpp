/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "SpringDamperMassSystem.hpp"
#include "broccoli/ode/collocation/CubicSplineCollocator.hpp"
#include "broccoli/ode/collocation/QuinticSplineCollocator.hpp"
#include "gtest/gtest.h"
#include <fstream>
#include <iostream>
#include <string>
#include <type_traits>

using namespace broccoli;
using namespace ode;

//! Helper function to enable/disable first order boundary conditions
template <class CollocatorType>
void setFirstOrderBoundaryConditions(CollocatorType& collocator, const bool& enable)
{
    // General case -> do nothing
    (void)collocator; // unused
    (void)enable; // unused
}

//! Helper function to enable/disable first order boundary conditions (specialization for \ref CubicSplineCollocator)
template <>
void setFirstOrderBoundaryConditions<CubicSplineCollocator>(CubicSplineCollocator& collocator, const bool& enable)
{
    // Set corresponding collocation method of collocator
    if (enable == true)
        collocator.m_method = CubicSplineCollocator::CollocationMethod::COLLOCATION_WITH_FIRST_DERIVATIVES;
    else
        collocator.m_method = CubicSplineCollocator::CollocationMethod::COLLOCATION_WITHOUT_FIRST_DERIVATIVES;
}

//! Helper function to compare approximation obtained through collocation with the analytic solution
template <class CollocatorType>
void compareToAnalyticSolution(const double& mass, const double& damping, const double& stiffness, const double& x0, const double& dotx0, const double& duration, const int& collocationSiteCount, const bool& forceFirstOrderBC, const bool& writePlot, const double& timeStepSize, const std::string& fileName = "")
{
    // Setup analytical system
    // -----------------------
    // Create analytical test system
    SpringDamperMassSystem analyticSystem(mass, damping, stiffness, x0, dotx0);

    // Compute boundary values from analytic system
    const double ddotx0 = analyticSystem.analyticalSolution(0, 2);
    const double xn = analyticSystem.analyticalSolution(duration, 0);
    const double dotxn = analyticSystem.analyticalSolution(duration, 1);
    const double ddotxn = analyticSystem.analyticalSolution(duration, 2);

    // Setup collocator
    // ----------------
    // Initialize collocation manager
    CollocatorType collocator;
    setFirstOrderBoundaryConditions<CollocatorType>(collocator, forceFirstOrderBC);
    SplineCollocatorBase::Result collocatorResult;
    ASSERT_EQ(collocator.substep_checkInput(&collocatorResult), false);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::ERROR_INPUT_NOT_INITIALIZED);

    // Setup knot sites
    int knotCount = collocationSiteCount + 2; // Count of spline knots
    collocator.m_knotSites = Eigen::VectorXd::Zero(knotCount);
    double hi = duration / ((double)knotCount - 1.0); // Segment duration for uniform distribution
    double knotShift = 0.1 * hi; // Shift knots to be not equally distributed
    collocator.m_knotSites(0) = 0;
    for (int i = 1; i < knotCount - 1; i++) {
        collocator.m_knotSites(i) = i * hi + knotShift;
        knotShift = -knotShift;
    }
    collocator.m_knotSites(knotCount - 1) = duration;

    // Setup ode coefficients and right hand side
    collocator.m_ODEAlpha = Eigen::VectorXd::Zero(collocationSiteCount);
    collocator.m_ODEBeta = Eigen::VectorXd::Zero(collocationSiteCount);
    collocator.m_ODEGamma = Eigen::VectorXd::Zero(collocationSiteCount);
    collocator.m_ODETau = Eigen::VectorXd::Zero(collocationSiteCount);
    for (int i = 0; i < (int)collocationSiteCount; i++) {
        collocator.m_ODEAlpha(i) = mass;
        collocator.m_ODEBeta(i) = damping;
        collocator.m_ODEGamma(i) = stiffness;
    }

    // Setup boundary conditions
    collocator.m_boundaryConditions = Eigen::VectorXd::Zero(6);
    collocator.m_boundaryConditions(0) = x0;
    collocator.m_boundaryConditions(1) = dotx0;
    collocator.m_boundaryConditions(2) = ddotx0;
    collocator.m_boundaryConditions(3) = xn;
    collocator.m_boundaryConditions(4) = dotxn;
    collocator.m_boundaryConditions(5) = ddotxn;

    // Run collocator
    // --------------
    // Run single steps
    ASSERT_EQ(collocator.substep_checkInput(&collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);
    ASSERT_EQ(collocator.substep_addVirtualControlPoints(&collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);
    ASSERT_EQ(collocator.substep_setupBlockTridiagonalSystem(&collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);
    ASSERT_EQ(collocator.substep_solveBlockTridiagonalSystem(&collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);
    ASSERT_EQ(collocator.substep_computeSplineGradients(&collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);
    ASSERT_EQ(collocator.substep_assembleCollocationLSE(&collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);
    ASSERT_EQ(collocator.substep_solveCollocationLSE(true, &collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);
    ASSERT_EQ(collocator.substep_convertToTrajectory(&collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);

    // Re-run complete process
    ASSERT_EQ(collocator.process(true, &collocatorResult), true);
    ASSERT_EQ(collocatorResult, SplineCollocatorBase::Result::SUCCESS);

    // Evaluate systems
    // ----------------
    // Setup time stepping
    size_t steps = ::floor(duration / timeStepSize) + 1; // Count of time steps to evaluate
    Eigen::VectorXd time = Eigen::VectorXd::Zero(steps); // Time t
    for (size_t i = 0; i < steps; i++)
        time(i) = i * timeStepSize;

    // Evaluate test system
    Eigen::VectorXd x_analytic = Eigen::VectorXd::Zero(steps); // x(t) of test system
    Eigen::VectorXd dot_x_analytic = Eigen::VectorXd::Zero(steps); // dotx(t) of test system
    Eigen::VectorXd ddot_x_analytic = Eigen::VectorXd::Zero(steps); // ddotx(t) of test system
    for (size_t i = 0; i < steps; i++) {
        x_analytic(i) = analyticSystem.analyticalSolution(time(i), 0);
        dot_x_analytic(i) = analyticSystem.analyticalSolution(time(i), 1);
        ddot_x_analytic(i) = analyticSystem.analyticalSolution(time(i), 2);
    }

    // Evaluate approximation
    Eigen::VectorXd x_spline = Eigen::VectorXd::Zero(steps); // x(t) of spline
    Eigen::VectorXd dot_x_spline = Eigen::VectorXd::Zero(steps); // dotx(t) of spline
    Eigen::VectorXd ddot_x_spline = Eigen::VectorXd::Zero(steps); // ddotx(t) of spline
    Eigen::VectorXd dddot_x_spline = Eigen::VectorXd::Zero(steps); // dddotx(t) of spline
    Eigen::VectorXd ddddot_x_spline = Eigen::VectorXd::Zero(steps); // dddotx(t) of spline
    for (size_t i = 0; i < steps; i++) {
        x_spline(i) = collocator.m_trajectory.evaluate(time(i), 0)[0];
        dot_x_spline(i) = collocator.m_trajectory.evaluate(time(i), 1)[0];
        ddot_x_spline(i) = collocator.m_trajectory.evaluate(time(i), 2)[0];
        dddot_x_spline(i) = collocator.m_trajectory.evaluate(time(i), 3)[0];
        ddddot_x_spline(i) = collocator.m_trajectory.evaluate(time(i), 4)[0];
    }
    double x_spline_max = x_spline.lpNorm<Eigen::Infinity>();
    double dot_x_spline_max = dot_x_spline.lpNorm<Eigen::Infinity>();
    double ddot_x_spline_max = ddot_x_spline.lpNorm<Eigen::Infinity>();
    double dddot_x_spline_max = dddot_x_spline.lpNorm<Eigen::Infinity>();
    double ddddot_x_spline_max = ddddot_x_spline.lpNorm<Eigen::Infinity>();

    // Plot systems
    // ------------
    if (writePlot == true) {

        // Writing gnuplot file
        std::ofstream gnuplotFile;
        gnuplotFile.open(fileName + ".gp");
        gnuplotFile << "set style line 1 linecolor rgb '#0065BD' linetype 1 linewidth 1.5\n";
        gnuplotFile << "set style line 2 linecolor rgb '#E37222' linetype 1 linewidth 1.5\n";
        gnuplotFile << "set style line 3 linecolor rgb '#A2AD00' linetype 1 linewidth 1.5\n";
        gnuplotFile << "set style line 4 linecolor rgb '#0065BD' linetype 2 linewidth 1\n";
        gnuplotFile << "set style line 5 linecolor rgb '#E37222' linetype 2 linewidth 1\n";
        gnuplotFile << "set style line 6 linecolor rgb '#A2AD00' linetype 2 linewidth 1\n";
        gnuplotFile << "set style line 7 linecolor rgb '#5BC5F2' linetype 3 linewidth 1\n";
        gnuplotFile << "set style line 8 linecolor rgb '#E3828F' linetype 3 linewidth 1\n";

        gnuplotFile << "\n";
        gnuplotFile << "plot '" << fileName << ".dat' using 1:2 with lines linestyle 1 title \"x(t) (analytic)\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:3 with lines linestyle 2 title \"dotx(t) (analytic)\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:4 with lines linestyle 3 title \"ddotx(t) (analytic)\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:5 with lines linestyle 4 title \"x(t) (spline)\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:6 with lines linestyle 5 title \"dotx(t) (spline)\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:7 with lines linestyle 6 title \"ddotx(t) (spline)\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:8 with lines linestyle 7 title \"dddotx(t) (spline)\"\n";
        gnuplotFile << "replot '" << fileName << ".dat' using 1:9 with lines linestyle 8 title \"ddddotx(t) (spline)\"\n";
        gnuplotFile.close();

        // Writing data file
        std::ofstream dataFile;
        dataFile.open(fileName + ".dat");
        for (Eigen::Index i = 0; i < time.rows(); i++) {
            dataFile << (double)time(i);
            dataFile << " " << (double)x_analytic(i);
            dataFile << " " << (double)dot_x_analytic(i);
            dataFile << " " << (double)ddot_x_analytic(i);
            dataFile << " " << (double)x_spline(i);
            dataFile << " " << (double)dot_x_spline(i);
            dataFile << " " << (double)ddot_x_spline(i);
            dataFile << " " << (double)dddot_x_spline(i);
            dataFile << " " << (double)ddddot_x_spline(i);
            dataFile << "\n";
        }
        dataFile.close();
    }

    // Check approximation
    // -------------------
    // Check boundary conditions
    ASSERT_TRUE(fabs(collocator.m_trajectory.evaluate(0, 0)[0] - x0) < 1e-9);
    if (forceFirstOrderBC == true)
        ASSERT_TRUE(fabs(collocator.m_trajectory.evaluate(0, 1)[0] - dotx0) < 1e-9);
    ASSERT_TRUE(fabs(collocator.m_trajectory.evaluate(0, 2)[0] - ddotx0) < 1e-9);
    ASSERT_TRUE(fabs(collocator.m_trajectory.evaluate(duration, 0)[0] - xn) < 1e-9);
    if (forceFirstOrderBC == true)
        ASSERT_TRUE(fabs(collocator.m_trajectory.evaluate(duration, 1)[0] - dotxn) < 1e-9);
    ASSERT_TRUE(fabs(collocator.m_trajectory.evaluate(duration, 2)[0] - ddotxn) < 1e-9);

    // Check collocation
    for (int i = 0; i < collocationSiteCount; i++) {
        const double t = collocator.m_knotSites(i + 1);
        const double yi = collocator.m_trajectory.evaluate(t, 0)[0];
        const double dotyi = collocator.m_trajectory.evaluate(t, 1)[0];
        const double ddotyi = collocator.m_trajectory.evaluate(t, 2)[0];
        ASSERT_TRUE(fabs(mass * ddotyi + damping * dotyi + stiffness * yi) < 1e-9);
    }

    // Check continuity at interior knots (check relative error between left and right limit)
    for (Eigen::Index i = 0; i < collocator.m_knotSitesRemapped.rows() - 2; i++) {
        const double t = collocator.m_knotSitesRemapped(i + 1);
        const double t_left = t - 1e-9;
        const double t_right = t + 1e-9;
        ASSERT_TRUE(fabs((collocator.m_trajectory.evaluate(t_left, 0)[0] - collocator.m_trajectory.evaluate(t_right, 0)[0]) / x_spline_max) < 1e-3 /* <- relative error < 0.1% */);
        ASSERT_TRUE(fabs((collocator.m_trajectory.evaluate(t_left, 1)[0] - collocator.m_trajectory.evaluate(t_right, 1)[0]) / dot_x_spline_max) < 1e-3 /* <- relative error < 0.1% */);
        ASSERT_TRUE(fabs((collocator.m_trajectory.evaluate(t_left, 2)[0] - collocator.m_trajectory.evaluate(t_right, 2)[0]) / ddot_x_spline_max) < 1e-3 /* <- relative error < 0.1% */);
        if (std::is_same<CollocatorType, QuinticSplineCollocator>::value == true) {
            // C^4 continuity is only achieved for quintic spline collocation
            ASSERT_TRUE(fabs((collocator.m_trajectory.evaluate(t_left, 3)[0] - collocator.m_trajectory.evaluate(t_right, 3)[0]) / dddot_x_spline_max) < 1e-3 /* <- relative error < 0.1% */);
            ASSERT_TRUE(fabs((collocator.m_trajectory.evaluate(t_left, 4)[0] - collocator.m_trajectory.evaluate(t_right, 4)[0]) / ddddot_x_spline_max) < 1e-3 /* <- relative error < 0.1% */);
        }
    }
}

// Spline Collocator Base
// --------------------------
//! Test string representation of the given result
TEST(SplineCollocatorBase, ResultToString)
{
    uint8_t i;

    for (i = 0; i < static_cast<uint8_t>(SplineCollocatorBase::Result::RESULT_COUNT); i++) {
        ASSERT_GT(SplineCollocatorBase::resultString(static_cast<SplineCollocatorBase::Result>(i)).size(), 0);
        ASSERT_TRUE(SplineCollocatorBase::resultString(static_cast<SplineCollocatorBase::Result>(i)) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(SplineCollocatorBase::Result::RESULT_COUNT);
    ASSERT_TRUE(SplineCollocatorBase::resultString(static_cast<SplineCollocatorBase::Result>(i)) == "UNKNOWN");
#endif
}

// Quintic spline collocation
// --------------------------
//! Test **quintic** spline collocation through comparison with an analytical example (underdamped)
TEST(QuinticSplineCollocator, CompareAnalyticUnderdamped)
{
    compareToAnalyticSolution<QuinticSplineCollocator>(1.0 /* <- mass */, 1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "QuinticSplineCollocator_underdamped");
}

//! Test **quintic** spline collocation through comparison with an analytical example (overdamped)
TEST(QuinticSplineCollocator, CompareAnalyticOverdamped)
{
    compareToAnalyticSolution<QuinticSplineCollocator>(1.0 /* <- mass */, 10.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "QuinticSplineCollocator_overdamped");
}

//! Test **quintic** spline collocation through comparison with an analytical example (critically damped)
TEST(QuinticSplineCollocator, CompareAnalyticCriticallyDamped)
{
    compareToAnalyticSolution<QuinticSplineCollocator>(1.0 /* <- mass */, 10.0 /* <- damping */, 25.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "QuinticSplineCollocator_criticallydamped");
}

//! Test **quintic** spline collocation through comparison with an analytical example (minimal count of collocation points)
TEST(QuinticSplineCollocator, CompareAnalyticMinimal)
{
    compareToAnalyticSolution<QuinticSplineCollocator>(1.0 /* <- mass */, 1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 1 /* <- # coll. sites */, true, true, 0.001, "QuinticSplineCollocator_minimum_collocation_points");
}

//! Test **quintic** spline collocation through comparison with an analytical example (instable system)
TEST(QuinticSplineCollocator, CompareAnalyticInstable)
{
    compareToAnalyticSolution<QuinticSplineCollocator>(1.0 /* <- mass */, -1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "QuinticSplineCollocator_instable");
}

//! Test **quintic** spline collocation through comparison with an analytical example (underdamped, long time horizon)
TEST(QuinticSplineCollocator, CompareAnalyticUnderdampedLong)
{
    compareToAnalyticSolution<QuinticSplineCollocator>(1.0 /* <- mass */, 0.1 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 50.0 /* <- duration */, 100 /* <- # coll. sites */, true, true, 0.01, "QuinticSplineCollocator_underdamped_long");
}

// Cubic spline collocation
// ------------------------
//! Test string representation of the given collocation method
TEST(CubicSplineCollocator, MethodToString)
{
    uint8_t i = 0;
    CubicSplineCollocator testMethod;

    for (i = 0; i < static_cast<uint8_t>(CubicSplineCollocator::CollocationMethod::COLLOCATIONMETHOD_COUNT); i++) {
        testMethod.m_method = static_cast<CubicSplineCollocator::CollocationMethod>(i);
        ASSERT_GT(testMethod.collocationMethodString().size(), 0);
        ASSERT_TRUE(testMethod.collocationMethodString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(CubicSplineCollocator::CollocationMethod::COLLOCATIONMETHOD_COUNT);
    testMethod.m_method = static_cast<CubicSplineCollocator::CollocationMethod>(i);
    ASSERT_TRUE(testMethod.collocationMethodString() == "UNKNOWN");
#endif
}

//! Test **cubic** spline collocation through comparison with an analytical example (underdamped)
TEST(CubicSplineCollocator, CompareAnalyticUnderdamped)
{
    // Do NOT force first order boundary conditions
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, false, true, 0.001, "CubicSplineCollocator_underdamped_withoutFOBC");

    // Force first order boundary conditions:
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "CubicSplineCollocator_underdamped_withFOBC");
}

//! Test **cubic** spline collocation through comparison with an analytical example (overdamped)
TEST(CubicSplineCollocator, CompareAnalyticOverdamped)
{
    // Do NOT force first order boundary conditions
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 10.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, false, true, 0.001, "CubicSplineCollocator_overdamped_withoutFOBC");

    // Force first order boundary conditions:
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 10.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "CubicSplineCollocator_overdamped_withFOBC");
}

//! Test **cubic** spline collocation through comparison with an analytical example (critically damped)
TEST(CubicSplineCollocator, CompareAnalyticCriticallyDamped)
{
    // Do NOT force first order boundary conditions
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 10.0 /* <- damping */, 25.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, false, true, 0.001, "CubicSplineCollocator_criticallydamped_withoutFOBC");

    // Force first order boundary conditions:
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 10.0 /* <- damping */, 25.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "CubicSplineCollocator_criticallydamped_withFOBC");
}

//! Test **cubic** spline collocation through comparison with an analytical example (minimal count of collocation points)
TEST(CubicSplineCollocator, CompareAnalyticMinimal)
{
    // Do NOT force first order boundary conditions
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 1 /* <- # coll. sites */, false, true, 0.001, "CubicSplineCollocator_minimum_collocation_points_withoutFOBC");

    // Force first order boundary conditions:
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 1 /* <- # coll. sites */, true, true, 0.001, "CubicSplineCollocator_minimum_collocation_points_withFOBC");
}

//! Test **cubic** spline collocation through comparison with an analytical example (instable system)
TEST(CubicSplineCollocator, CompareAnalyticInstable)
{
    // Do NOT force first order boundary conditions
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, -1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, false, true, 0.001, "CubicSplineCollocator_instable_withoutFOBC");

    // Force first order boundary conditions:
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, -1.0 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 5.0 /* <- duration */, 10 /* <- # coll. sites */, true, true, 0.001, "CubicSplineCollocator_instable_withFOBC");
}

//! Test **cubic** spline collocation through comparison with an analytical example (long time horizon)
TEST(CubicSplineCollocator, CompareAnalyticUnderdampedLong)
{
    // Do NOT force first order boundary conditions
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 0.1 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 50.0 /* <- duration */, 100 /* <- # coll. sites */, false, true, 0.01, "CubicSplineCollocator_underdamped_long_withoutFOBC");

    // Force first order boundary conditions:
    compareToAnalyticSolution<CubicSplineCollocator>(1.0 /* <- mass */, 0.1 /* <- damping */, 10.0 /* <- stiffness */, 1.0 /* <- x0 */, 0.0 /* <- dotx0 */, 50.0 /* <- duration */, 100 /* <- # coll. sites */, true, true, 0.01, "CubicSplineCollocator_underdamped_long_withFOBC");
}
