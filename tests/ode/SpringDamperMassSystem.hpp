/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#pragma once // Load this file only once

#include "broccoli/ode/integration/AbstractSolvableEntity.hpp"
#include <Eigen/Dense>
#include <math.h>

namespace broccoli {
namespace ode {
    //! Spring-damper-mass system (without excitation)
    /*!
     * May be solved by time-integration + provides a method with the analytical solution.
     * \f[ m\,\ddot{x}(t) + d\,\dot{x}(t) + c\,x(t) = 0 \f]
     */
    class SpringDamperMassSystem : public AbstractSolvableEntity {
    public:
        //! Constructor
        /*!
         * \param [in] mass Intitializes \ref m_mass - \copybrief m_mass
         * \param [in] damping Intitializes \ref m_damping - \copybrief m_damping
         * \param [in] stiffness Intitializes \ref m_stiffness - \copybrief m_stiffness
         * \param [in] x0 Intitializes \ref m_x0 - \copybrief m_x0
         * \param [in] dotx0 Intitializes \ref m_dotx0 - \copybrief m_dotx0
         */
        SpringDamperMassSystem(const double& mass, const double& damping, const double& stiffness, const double& x0, const double& dotx0)
            : m_mass(mass)
            , m_damping(damping)
            , m_stiffness(stiffness)
            , m_x0(x0)
            , m_dotx0(dotx0)
        {
            m_A << 0, 1,
                -stiffness / mass, -damping / mass;

            m_state(0) = x0;
            m_state(1) = dotx0;
        }

        std::size_t stateVectorSize() const override
        {
            return 2;
        }

        core::IntegerNanoSeconds<uint64_t> sampleTime() const override
        {
            return 0.0; // fully continuous system
        }

        void evaluateRHS() override
        {
            // Calc gradient and store it
            m_gradient = m_A * m_state;
        }

        Eigen::VectorXd& state() override
        {
            return m_state;
        }

        const Eigen::VectorXd& stateGradient() const override
        {
            return m_gradient;
        }

        //! Compute the analytical solution \f$ x(t) \f$ (or its derivative up to the second order)
        /*!
         * \param [in] time Time [s] to evaluate system at
         * \param [in] derivationOrder Order of derivative (implemented: up to second order)
         * \return Value of \f$ x(t) \f$ (or its derivative)
         */
        double analyticalSolution(const double& time, const unsigned int& derivationOrder = 0) const
        {
            // Check characteristic polynomial
            const double rootArgument = m_damping * m_damping / (4.0 * m_mass * m_mass) - m_stiffness / m_mass;
            const double alpha = -m_damping / (2.0 * m_mass);

            // Check system type
            if (rootArgument < 0) {
                // ...two complex conjugate eigenvalues (underdamped system)
                const double beta = sqrt(-rootArgument);
                const double& C1 = m_x0;
                const double C2 = 1.0 / beta * (m_dotx0 - m_x0 * alpha);
                const double exp_alpha_t = exp(alpha * time);

                // Check derivative order
                if (derivationOrder == 0) {
                    // x(t)
                    return exp_alpha_t * (C1 * cos(beta * time) + C2 * sin(beta * time));
                } else {
                    const double C3 = C1 * alpha + C2 * beta;
                    const double C4 = C2 * alpha - C1 * beta;
                    if (derivationOrder == 1) {
                        // dotx(t)
                        return exp_alpha_t * (C3 * cos(beta * time) + C4 * sin(beta * time));
                    } else {
                        const double C5 = C3 * alpha + C4 * beta;
                        const double C6 = C4 * alpha - C3 * beta;
                        if (derivationOrder == 2) {
                            // ddotx(t)
                            return exp_alpha_t * (C5 * cos(beta * time) + C6 * sin(beta * time));
                        } else {
                            // NOT IMPLEMENTED!
                            return 0;
                        }
                    }
                }
            } else if (rootArgument > 0) {
                // ...two distinct real valued eigenvalues (overdamped system)
                const double beta = sqrt(rootArgument);
                const double lambda_1 = alpha + beta;
                const double lambda_2 = alpha - beta;
                const double C2 = (m_dotx0 - m_x0 * lambda_1) / (lambda_2 - lambda_1);
                const double C1 = m_x0 - C2;
                const double exp_lambda_1_t = exp(lambda_1 * time);
                const double exp_lambda_2_t = exp(lambda_2 * time);

                // Check derivative order
                if (derivationOrder == 0) {
                    // x(t)
                    return C1 * exp_lambda_1_t + C2 * exp_lambda_2_t;
                } else {
                    if (derivationOrder == 1) {
                        // dotx(t)
                        return C1 * lambda_1 * exp_lambda_1_t + C2 * lambda_2 * exp_lambda_2_t;
                    } else {
                        if (derivationOrder == 2) {
                            // ddotx(t)
                            return C1 * lambda_1 * lambda_1 * exp_lambda_1_t + C2 * lambda_2 * lambda_2 * exp_lambda_2_t;
                        } else {
                            // NOT IMPLEMENTED!
                            return 0;
                        }
                    }
                }
            } else {
                // ...two coinciding real valued eigenvalues (critically damped system)
                const double& lambda = alpha;
                const double& C1 = m_x0;
                const double C2 = m_dotx0 - m_x0 * lambda;
                const double exp_lambda_t = exp(lambda * time);

                // Check derivative order
                if (derivationOrder == 0) {
                    // x(t)
                    return exp_lambda_t * (C1 + C2 * time);
                } else {
                    if (derivationOrder == 1) {
                        // dotx(t)
                        return exp_lambda_t * (C1 * lambda + C2 * (1 + lambda * time));
                    } else {
                        if (derivationOrder == 2) {
                            // ddotx(t)
                            return exp_lambda_t * (C1 * lambda * lambda + C2 * lambda * (2 + lambda * time));
                        } else {
                            // NOT IMPLEMENTED!
                            return 0;
                        }
                    }
                }
            }
        }

    protected:
        // Mechanical Parameters
        double m_mass; //!< Mass \f$ m \f$ [kg]
        double m_damping; //!< Damping coefficient \f$ d \f$ [Ns/m]
        double m_stiffness; //!< Stiffness coefficient \f$ c \f$ [N/m]

        // Initial values
        double m_x0; //!< \f$ x(t=0) = x_0 \f$
        double m_dotx0; //!< \f$ \dot{x}(t=0) = \dot{x}_0 \f$

        //! System state gradient \f$ \dot x \f$
        Eigen::VectorXd m_gradient{ 2 };

        //! dynamics matrix A, where \f$ \dot x = A x \f$
        Eigen::Matrix2d m_A;

        //! System state \f$ x \f$
        Eigen::VectorXd m_state{ 2 };

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace ode
} // namespace broccoli
