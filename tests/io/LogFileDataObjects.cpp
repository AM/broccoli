/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/logging/ColumnBasedLogFileData.hpp"
#include "broccoli/io/logging/Logger.hpp"
#include "broccoli/io/logging/TemporalLogFileData.hpp"
#include "broccoli/io/logging/XMLBasedLogFileData.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Convert "LogStream" to a std::string
auto toString = [](const LogFileData::LogStream& stream) {
    std::string line = "";
    for (auto item : stream) {
        line += item;
    }
    return line;
};

//! Tests getters of ColumnBasedLogStream
TEST(ColumnBasedLogStream, Getters)
{
    LogFileData::LogStream baseStream;
    ColumnBasedLogStream streamForHeader(baseStream, true);
    EXPECT_EQ(streamForHeader.generateColumnNames(), true);
    ColumnBasedLogStream streamForData(baseStream, false);
    EXPECT_EQ(streamForData.generateColumnNames(), false);
}

//! Mock class for testing ColumnBasedLogFileData
class MockColumnBasedLogFileData : public ColumnBasedLogFileData {
public:
    MockColumnBasedLogFileData()
        : quaternion(1, 2, 3, 4)
    {
        matrixArray[0] << 1, 2,
            4, 5;

        matrixArray[1] << 3, 5,
            9, 8;

        matrix << 5, 4, 3, 2;
    }

    void encodeColumnHeaderToLogStream(LogStream& stream) const override
    {
        ColumnBasedLogFileData::encodeColumnHeaderToLogStream(stream);
    }

protected:
    void encodeColumnDataToLogStream(ColumnBasedLogStream& stream) const override
    {
        stream.addData(scalar, "time");
        stream.addData(integer, "integer");
        stream.addData(unsignedInteger, "unsignedInteger");
        stream.addData(boolean, "bool");
        stream.addData(scalarArray, "scalarArray");
        stream.addData(vectorArray, "vectorArray");
        stream.addData(matrixArray, "matrixArray");
        stream.setColumnNamePrefix("my.");
        stream.addData(matrix, "matrix");
        stream.appendColumnNamePrefix("crazy.");
        stream.addData(quaternion, "quaternion");
        stream.addData(integer, "");
    }

    std::array<double, 3> scalarArray{ 1.0, 2.0, 3.0 };
    std::array<Eigen::Vector2d, 2> vectorArray{ { { 1.0, 2.0 }, { 3.0, 4.0 } } };
    std::array<Eigen::Matrix2d, 2> matrixArray;
    Eigen::Matrix2d matrix;
    Eigen::Quaterniond quaternion;

    double scalar = 1.0;
    int integer = -2;
    uint64_t unsignedInteger = 0xFFFFFFFFFFFFFFFF;
    bool boolean = false;
};

//! Tests header column generation of ColumnBasedLogFileData
TEST(ColumnBasedLogFileData, columnGeneration)
{
    MockColumnBasedLogFileData data;
    ColumnBasedLogFileData::LogStream stream;

    data.encodeColumnHeaderToLogStream(stream);

    EXPECT_EQ(toString(stream), std::string("# time:1 integer:2 unsignedInteger:3 bool:4 scalarArray[0]:5 scalarArray[1]:6 scalarArray[2]:7 vectorArray[0](0):8 vectorArray[0](1):9 vectorArray[1](0):10 vectorArray[1](1):11 matrixArray[0](0,0):12 matrixArray[0](0,1):13 matrixArray[0](1,0):14 matrixArray[0](1,1):15 matrixArray[1](0,0):16 matrixArray[1](0,1):17 matrixArray[1](1,0):18 matrixArray[1](1,1):19 my.matrix(0,0):20 my.matrix(0,1):21 my.matrix(1,0):22 my.matrix(1,1):23 my.crazy.quaternion.w():24 my.crazy.quaternion.x():25 my.crazy.quaternion.y():26 my.crazy.quaternion.z():27\n"));
}

//! Tests serialization of complex types in ColumnBasedLogFileData
TEST(ColumnBasedLogFileData, complexTypeSerialization)
{
    MockColumnBasedLogFileData data;
    ColumnBasedLogFileData::LogStream stream;
    data.encodeDataToLogStream(stream);

    EXPECT_EQ(toString(stream), std::string("+1.0000000000000000e+00 -2 18446744073709551615 +0 +1.0000000000000000e+00 +2.0000000000000000e+00 +3.0000000000000000e+00 +1.0000000000000000e+00 +2.0000000000000000e+00 +3.0000000000000000e+00 +4.0000000000000000e+00 +1.0000000000000000e+00 +2.0000000000000000e+00 +4.0000000000000000e+00 +5.0000000000000000e+00 +3.0000000000000000e+00 +5.0000000000000000e+00 +9.0000000000000000e+00 +8.0000000000000000e+00 +5.0000000000000000e+00 +4.0000000000000000e+00 +3.0000000000000000e+00 +2.0000000000000000e+00 +1.0000000000000000e+00 +2.0000000000000000e+00 +3.0000000000000000e+00 +4.0000000000000000e+00 -2\n"));
}

//! Mock class for testing TemporalLogFileData
class MockTemporalLogFileData : public TemporalLogFileData {
public:
    void encodeColumnHeaderToLogStream(LogStream& stream) const override
    {
        ColumnBasedLogFileData::encodeColumnHeaderToLogStream(stream);
    }

protected:
    void encodeTimeDataToLogStream(ColumnBasedLogStream& stream) const override
    {
        stream.addData(1.0, "value");
    }
};

TEST(TemporalLogFileData, SerializationAndHeader)
{
    MockTemporalLogFileData logFileData;
    TemporalLogFileData::LogStream stream;

    logFileData.encodeColumnHeaderToLogStream(stream);

    EXPECT_EQ(toString(stream), "# time:1 value:2\n");

    stream.clear();
    logFileData.setLogTime(2.0);
    logFileData.encodeDataToLogStream(stream);

    EXPECT_DOUBLE_EQ(logFileData.logTime().toDouble(), 2.0);
    EXPECT_EQ(toString(stream), "+2.000000000 +1.0000000000000000e+00\n");

    logFileData.setLogTime(core::Time(0, 1));
    stream.clear();
    logFileData.encodeDataToLogStream(stream);

    EXPECT_EQ(toString(stream), "+0.000000001 +1.0000000000000000e+00\n");
}

//! Mock class for testing XMLBasedLogFileData
class MockXMLBasedLogFileData : public XMLBasedLogFileData {
public:
    void encodeDataToLogStream(LogStream& stream) const
    {
        encoding::encode(stream, "<Test></Test>\n");
    }
};

TEST(XMLBasedLogFileData, HeaderAndFooter)
{
    // Initialize helpers
    MockXMLBasedLogFileData logFileData;
    LogFileData::LogStream stream;
    std::unordered_map<std::string, std::string> fileAttributes;

    // Test header
    // -----------
    // With default settings
    fileAttributes.clear();
    stream.clear();
    logFileData.encodeHeaderToLogStream(stream, fileAttributes);
    EXPECT_EQ(toString(stream), "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Data>\n");

    // With custom settings
    fileAttributes.clear();
    fileAttributes["XMLVersion"] = "2.34";
    fileAttributes["XMLEncoding"] = "ascii";
    fileAttributes["XMLRootElementTag"] = "Root";
    fileAttributes["OtherAttributeKey"] = "OtherAttributeValue";
    stream.clear();
    logFileData.encodeHeaderToLogStream(stream, fileAttributes);
    EXPECT_EQ(toString(stream), "<?xml version=\"2.34\" encoding=\"ascii\"?>\n<Root OtherAttributeKey=\"OtherAttributeValue\">\n");

    // Test data
    // ---------
    fileAttributes.clear();
    stream.clear();
    logFileData.encodeDataToLogStream(stream);
    EXPECT_EQ(toString(stream), "<Test></Test>\n");

    // Test footer
    // -----------
    // With default settings
    fileAttributes.clear();
    stream.clear();
    logFileData.encodeFooterToLogStream(stream, fileAttributes);
    EXPECT_EQ(toString(stream), "</Data>\n");

    // With custom settings
    fileAttributes.clear();
    fileAttributes["XMLRootElementTag"] = "Root";
    stream.clear();
    logFileData.encodeFooterToLogStream(stream, fileAttributes);
    EXPECT_EQ(toString(stream), "</Root>\n");
}
