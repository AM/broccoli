/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/network/NetworkSocket.hpp"
#include "broccoli/io/serialization/SerializableDataString.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;
using namespace serialization;

//! Test string representation of types
TEST(NetworkSocketSignal, TypesToString)
{
    uint8_t i = 0;
    NetworkSocketSignal testSignal;

    for (i = 0; i < static_cast<uint8_t>(NetworkSocketSignal::Type::TYPE_COUNT); i++) {
        testSignal.m_type = static_cast<NetworkSocketSignal::Type>(i);
        ASSERT_GT(testSignal.typeString().size(), 0);
        ASSERT_TRUE(testSignal.typeString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(NetworkSocketSignal::Type::TYPE_COUNT);
    testSignal.m_type = static_cast<NetworkSocketSignal::Type>(i);
    ASSERT_TRUE(testSignal.typeString() == "UNKNOWN");
#endif
}

//! Test string representation of messages
TEST(NetworkSocketSignal, MessagesToString)
{
    uint8_t i = 0;
    NetworkSocketSignal testMessage;

    for (i = 0; i < static_cast<uint8_t>(NetworkSocketSignal::Message::MESSAGE_COUNT); i++) {
        testMessage.m_message = static_cast<NetworkSocketSignal::Message>(i);
        ASSERT_GT(testMessage.messageString().size(), 0);
        ASSERT_TRUE(testMessage.messageString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(NetworkSocketSignal::Message::MESSAGE_COUNT);
    testMessage.m_message = static_cast<NetworkSocketSignal::Message>(i);
    ASSERT_TRUE(testMessage.messageString() == "UNKNOWN");
#endif
}

//! Test string representation of the given socket type
TEST(NetworkSocket, SocketTypeToString)
{
    uint8_t i;
    NetworkSocketType testSocketType;

    for (i = 0; i < static_cast<uint8_t>(NetworkSocketType::NETWORKSOCKETTYPE_COUNT); i++) {
        testSocketType = static_cast<NetworkSocketType>(i);
        ASSERT_GT(networkSocketTypeString(testSocketType).size(), 0);
        ASSERT_TRUE(networkSocketTypeString(testSocketType) != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(NetworkSocketType::NETWORKSOCKETTYPE_COUNT);
    testSocketType = static_cast<NetworkSocketType>(i);
    ASSERT_TRUE(networkSocketTypeString(testSocketType) == "UNKNOWN");
#endif
}

//! Test string representation of types
TEST(NetworkSocket, TypesToString)
{
    // Just check, if string is non-empty
    NetworkSocketSignal testSignal;
    ASSERT_GT(networkSocketTypeString(NetworkSocketType::TCP_SERVER).size(), 0);
    ASSERT_GT(testSignal.typeString().size(), 0);
    ASSERT_GT(testSignal.messageString().size(), 0);
}

//! Test construction
TEST(NetworkSocket, Construction)
{
    NetworkSocket<SerializableDataString, SerializableDataString> TCPServerSocket(TCPServerOptions("TCPServerSocket", 5005, true, 0, 0, false, 10, 10));
    NetworkSocket<SerializableDataString, SerializableDataString> TCPClientSocket(TCPClientOptions("TCPClientSocket", "localhost", 5005, true, 0, 0, false, 10, 10));
    NetworkSocket<SerializableDataString, SerializableDataString> UDPSocket(UDPSocketOptions("UDPSocket", "localhost", 5006, 5005, true, 0, 0, false, 10, 10));
    (void)TCPServerSocket;
    (void)TCPClientSocket;
    (void)UDPSocket;
}
