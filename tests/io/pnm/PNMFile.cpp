﻿/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/pnm/PNMFile.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Generic template for checking equality between two objects
template <typename A, typename B>
static inline void checkEquality(const A& a, const B& b)
{
    ASSERT_TRUE(a == b);
    ASSERT_TRUE(b == a);
    ASSERT_FALSE(a != b);
    ASSERT_FALSE(b != a);
}

//! Generic template for checking inequality between two objects
template <typename A, typename B>
static inline void checkInequality(const A& a, const B& b)
{
    ASSERT_FALSE(a == b);
    ASSERT_FALSE(b == a);
    ASSERT_TRUE(a != b);
    ASSERT_TRUE(b != a);
}

//! Basic construction and operators
TEST(PNMFile, ConstructionAndOperators)
{
    // Construction
    // ------------
    // Unknown type
    const PNMFile empty;
    ASSERT_TRUE(empty.type() == PNMFile::Type::UNKNOWN);

    // PBM
    const PNMFile pbm(PNMFile::Type::PBM);
    ASSERT_TRUE(pbm.type() == PNMFile::Type::PBM);
    ASSERT_TRUE((pbm.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFile pbmFilled(PNMFile::Type::PBM, 12, 34);
    pbmFilled.dataPBM().fill(PNMFileData::PBMSample::WHITE);
    ASSERT_TRUE(pbmFilled.type() == PNMFile::Type::PBM);
    ASSERT_TRUE((pbmFilled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PGM8
    const PNMFile pgm8(PNMFile::Type::PGM_8BIT);
    ASSERT_TRUE(pgm8.type() == PNMFile::Type::PGM_8BIT);
    ASSERT_TRUE((pgm8.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFile pgm8Filled(PNMFile::Type::PGM_8BIT, 12, 34);
    pgm8Filled.dataPGM8().fill(123);
    ASSERT_TRUE(pgm8Filled.type() == PNMFile::Type::PGM_8BIT);
    ASSERT_TRUE((pgm8Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PGM16
    const PNMFile pgm16(PNMFile::Type::PGM_16BIT);
    ASSERT_TRUE(pgm16.type() == PNMFile::Type::PGM_16BIT);
    ASSERT_TRUE((pgm16.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFile pgm16Filled(PNMFile::Type::PGM_16BIT, 12, 34);
    pgm16Filled.dataPGM16().fill(1234);
    ASSERT_TRUE(pgm16Filled.type() == PNMFile::Type::PGM_16BIT);
    ASSERT_TRUE((pgm16Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PPM8
    const PNMFile ppm8(PNMFile::Type::PPM_8BIT);
    ASSERT_TRUE(ppm8.type() == PNMFile::Type::PPM_8BIT);
    ASSERT_TRUE((ppm8.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFile ppm8Filled(PNMFile::Type::PPM_8BIT, 12, 34);
    ppm8Filled.dataPPM8().fill({ 0, 127, 255 });
    ASSERT_TRUE(ppm8Filled.type() == PNMFile::Type::PPM_8BIT);
    ASSERT_TRUE((ppm8Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PGM16
    const PNMFile ppm16(PNMFile::Type::PPM_16BIT);
    ASSERT_TRUE(ppm16.type() == PNMFile::Type::PPM_16BIT);
    ASSERT_TRUE((ppm16.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFile ppm16Filled(PNMFile::Type::PPM_16BIT, 12, 34);
    ppm16Filled.dataPPM16().fill({ 0, 1000, 2000 });
    ASSERT_TRUE(ppm16Filled.type() == PNMFile::Type::PPM_16BIT);
    ASSERT_TRUE((ppm16Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // Operators
    // ---------
    // Compare empty against...
    checkEquality(empty, PNMFile());
    checkInequality(empty, pbm);
    checkInequality(empty, pbmFilled);
    checkInequality(empty, pgm8);
    checkInequality(empty, pgm8Filled);
    checkInequality(empty, pgm16);
    checkInequality(empty, pgm16Filled);
    checkInequality(empty, ppm8);
    checkInequality(empty, ppm8Filled);
    checkInequality(empty, ppm16);
    checkInequality(empty, ppm16Filled);

    // Compare PBM against...
    PNMFile newPbmFilled = PNMFile(PNMFile::Type::PBM, 12, 34);
    newPbmFilled.dataPBM().fill(PNMFileData::PBMSample::WHITE);
    checkEquality(pbm, PNMFile(PNMFile::Type::PBM));
    checkInequality(pbm, pbmFilled);
    checkInequality(pbm, pgm8);
    checkInequality(pbm, pgm8Filled);
    checkInequality(pbm, pgm16);
    checkInequality(pbm, pgm16Filled);
    checkInequality(pbm, ppm8);
    checkInequality(pbm, ppm8Filled);
    checkInequality(pbm, ppm16);
    checkInequality(pbm, ppm16Filled);
    checkEquality(pbmFilled, newPbmFilled);
    newPbmFilled.dataPBM().fill(PNMFileData::PBMSample::BLACK);
    checkInequality(pbmFilled, newPbmFilled);
    checkInequality(pbmFilled, pgm8);
    checkInequality(pbmFilled, pgm8Filled);
    checkInequality(pbmFilled, pgm16);
    checkInequality(pbmFilled, pgm16Filled);
    checkInequality(pbmFilled, ppm8);
    checkInequality(pbmFilled, ppm8Filled);
    checkInequality(pbmFilled, ppm16);
    checkInequality(pbmFilled, ppm16Filled);

    // Compare PGM8 against...
    PNMFile newPgm8Filled = PNMFile(PNMFile::Type::PGM_8BIT, 12, 34);
    newPgm8Filled.dataPGM8().fill(123);
    checkEquality(pgm8, PNMFile(PNMFile::Type::PGM_8BIT));
    checkInequality(pgm8, pgm8Filled);
    checkInequality(pgm8, pgm16);
    checkInequality(pgm8, pgm16Filled);
    checkInequality(pgm8, ppm8);
    checkInequality(pgm8, ppm8Filled);
    checkInequality(pgm8, ppm16);
    checkInequality(pgm8, ppm16Filled);
    checkEquality(pgm8Filled, newPgm8Filled);
    newPgm8Filled.dataPGM8().fill(23);
    checkInequality(pgm8Filled, newPgm8Filled);
    checkInequality(pgm8Filled, pgm16);
    checkInequality(pgm8Filled, pgm16Filled);
    checkInequality(pgm8Filled, ppm8);
    checkInequality(pgm8Filled, ppm8Filled);
    checkInequality(pgm8Filled, ppm16);
    checkInequality(pgm8Filled, ppm16Filled);

    // Compare PGM16 against...
    PNMFile newPgm16Filled = PNMFile(PNMFile::Type::PGM_16BIT, 12, 34);
    newPgm16Filled.dataPGM16().fill(1234);
    checkEquality(pgm16, PNMFile(PNMFile::Type::PGM_16BIT));
    checkInequality(pgm16, pgm16Filled);
    checkInequality(pgm16, ppm8);
    checkInequality(pgm16, ppm8Filled);
    checkInequality(pgm16, ppm16);
    checkInequality(pgm16, ppm16Filled);
    checkEquality(pgm16Filled, newPgm16Filled);
    newPgm16Filled.dataPGM16().fill(234);
    checkInequality(pgm16Filled, newPgm16Filled);
    checkInequality(pgm16Filled, ppm8);
    checkInequality(pgm16Filled, ppm8Filled);
    checkInequality(pgm16Filled, ppm16);
    checkInequality(pgm16Filled, ppm16Filled);

    // Compare PPM8 against...
    PNMFile newPpm8Filled = PNMFile(PNMFile::Type::PPM_8BIT, 12, 34);
    newPpm8Filled.dataPPM8().fill({ 0, 127, 255 });
    checkEquality(ppm8, PNMFile(PNMFile::Type::PPM_8BIT));
    checkInequality(ppm8, ppm8Filled);
    checkInequality(ppm8, ppm16);
    checkInequality(ppm8, ppm16Filled);
    checkEquality(ppm8Filled, newPpm8Filled);
    newPpm8Filled.dataPPM8().fill({ 0, 127, 0 });
    checkInequality(ppm8Filled, newPpm8Filled);
    checkInequality(ppm8Filled, ppm16);
    checkInequality(ppm8Filled, ppm16Filled);

    // Compare PPM16 against...
    PNMFile newPpm16Filled = PNMFile(PNMFile::Type::PPM_16BIT, 12, 34);
    newPpm16Filled.dataPPM16().fill({ 0, 1000, 2000 });
    checkEquality(ppm16Filled, newPpm16Filled);
    checkInequality(ppm16, ppm16Filled);
    newPpm16Filled.dataPPM16().fill({ 0, 1234, 2000 });
    checkInequality(ppm16Filled, newPpm16Filled);
}

//! Mockup-class for access to protected functions
class MockPNMFile : public PNMFile {
public:
    MockPNMFile(const PNMFile& original)
        : PNMFile(original)
    {
    }

    //! Encodes the file with a certain header variant
    /*! \return `true` if the file is valid, `false` otherwise */
    bool encodeVariant(encoding::CharacterStream& stream, const PNMFileEncoding::Type& encoding, const size_t& variant) const
    {
        bool valid = true;
        stream.reserve(stream.size() + streamSizeUpperBound(encoding) + 1000);

        // Encode magic number
        if (encoding == PNMFileEncoding::Type::ASCII) {
            if (m_type == PNMFileData::Type::PBM)
                encoding::encode(stream, "P1\n");
            else if (m_type == PNMFileData::Type::PGM_8BIT || m_type == PNMFileData::Type::PGM_16BIT)
                encoding::encode(stream, "P2\n");
            else
                encoding::encode(stream, "P3\n");
        } else {
            if (m_type == PNMFileData::Type::PBM)
                encoding::encode(stream, "P4\n");
            else if (m_type == PNMFileData::Type::PGM_8BIT || m_type == PNMFileData::Type::PGM_16BIT)
                encoding::encode(stream, "P5\n");
            else
                encoding::encode(stream, "P6\n");
        }
        if (variant == 1) {
            // Variant 1: first character is not "P"
            stream[0] = (char)'X';
            valid = false;
        }
        if (variant == 2) {
            // Variant 2: invalid magic number
            stream[1] = (char)'7';
            valid = false;
        }
        if (variant == 3) {
            // Variant 3: comment after magic number
            encoding::encode(stream, "# 12345 6789 #\n");
        }
        if (variant == 9) {
            // Variant 9: too short for full header
            return false;
        }
        if (variant == 10) {
            // Variant 10: no width detected (but header is long enough)
            encoding::encode(stream, "#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
            return false;
        }

        // Encode width
        encoding::encode(stream, width());
        if (variant == 4) {
            // Variant 4: comments after width
            encoding::encode(stream, "# 12345 6789 #\n   #\t  \n");
        }
        encoding::encode(stream, (char)' ');

        // Encode height
        if (variant == 11) {
            // Variant 11: no columns detected (but header is long enough)
            encoding::encode(stream, "#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
            return false;
        }
        encoding::encode(stream, height());
        if (variant == 5) {
            // Variant 5: comment after height
            encoding::encode(stream, "# 12345 6789 #\n");
        }
        encoding::encode(stream, (char)'\n');

        // Encode max. sample value
        if (m_type != PNMFileData::Type::PBM) {
            if (variant == 12) {
                // Variant 12: no bit depth detected (but header is long enough)
                encoding::encode(stream, "#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
                return false;
            }
            if (m_type == PNMFileData::Type::PGM_8BIT || m_type == PNMFileData::Type::PPM_8BIT)
                encoding::encode(stream, "255");
            else if (m_type == PNMFileData::Type::PGM_16BIT || m_type == PNMFileData::Type::PPM_16BIT)
                encoding::encode(stream, "65535");
            if (variant == 6) {
                // Variant 6: comment after depth
                encoding::encode(stream, "# 12345 6789 #\n");
            }
            encoding::encode(stream, '\n');
        }

        // Encode buffer
        encodeBuffer(stream, encoding);
        if (variant == 7) {
            // Variant 7: buffer longer than expected
            encoding::encode(stream, " 12345 6789 \n");
        }
        if (variant == 8 && width() * height() > 10) {
            // Variant 8: buffer shorter than expected
            stream.resize(stream.size() - 6);
            valid = false;
        }
        return valid;
    }
};

//! Checks encoding and decoding for the given PNM file
static inline void checkPNMEncodingAndDecoding(const PNMFile& file)
{
    encoding::CharacterStream streamAscii, streamBinary, streamInvalid;
    if (file.type() == PNMFile::Type::UNKNOWN) {
        // "Regular" encoding and decoding
        // -------------------------------
        // Unknown type
#ifdef NDEBUG
        ASSERT_FALSE(file.encode(streamInvalid, PNMFileEncoding::Type::ASCII));
        ASSERT_FALSE(file.encode(streamInvalid, PNMFileEncoding::Type::BINARY));
#endif
    } else {
        // Ascii encoding
        ASSERT_TRUE(file.encode(streamAscii, PNMFileEncoding::Type::ASCII));
        ASSERT_GT(streamAscii.size(), 0);
        ASSERT_LE(streamAscii.size(), file.streamSizeUpperBound(PNMFileEncoding::Type::ASCII));
        PNMFile decodedFileAscii;
        ASSERT_TRUE(decodedFileAscii.decode(streamAscii));
        ASSERT_TRUE(decodedFileAscii == file);

        // Binary encoding
        ASSERT_TRUE(file.encode(streamBinary, PNMFileEncoding::Type::BINARY));
        ASSERT_GT(streamBinary.size(), 0);
        ASSERT_EQ(streamBinary.size(), file.streamSizeUpperBound(PNMFileEncoding::Type::BINARY));
        PNMFile decodedFileBinary;
        ASSERT_TRUE(decodedFileBinary.decode(streamBinary));
        ASSERT_TRUE(decodedFileBinary == file);

        // Invalid encoding type
#ifdef NDEBUG
        ASSERT_FALSE(file.encode(streamInvalid, PNMFileEncoding::Type::UNKNOWN));
#endif

        // Decoding robustness
        // -------------------
        MockPNMFile mockFile = file;
        for (size_t v = 0; v <= 12; v++) {
            // Ascii encoding
            streamAscii.clear();
            const bool asciiValid = mockFile.encodeVariant(streamAscii, PNMFileEncoding::Type::ASCII, v);
            ASSERT_GT(streamAscii.size(), 0);
            PNMFile decodedMockFileAscii;
            if (decodedMockFileAscii.decode(streamAscii) != asciiValid)
                std::cout << decodedMockFileAscii.decode(streamAscii);
            ASSERT_EQ(decodedMockFileAscii.decode(streamAscii), asciiValid);
            if (asciiValid == true)
                ASSERT_TRUE(decodedMockFileAscii == mockFile);

            // Binary encoding
            streamBinary.clear();
            const bool binaryValid = mockFile.encodeVariant(streamBinary, PNMFileEncoding::Type::BINARY, v);
            ASSERT_GT(streamBinary.size(), 0);
            PNMFile decodedMockFileBinary;
            ASSERT_EQ(decodedMockFileBinary.decode(streamBinary), binaryValid);
            if (binaryValid == true)
                ASSERT_TRUE(decodedMockFileBinary == mockFile);
        }
    }
}

//! Fills the given PNM file with a dummy PPM16 raster
static inline void fillDummyImagePPM16(PNMFile& file, const size_t& rows, const size_t& columns)
{
    file.setType(PNMFile::Type::PPM_16BIT);
    file.dataPPM16().resize(rows, columns);
    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < columns; j++) {
            PNMFile::PPM16Cell& cell = file.dataPPM16()(i, j);
            cell[0] = (i * 65535ULL) / rows;
            cell[1] = (j * 65535ULL) / columns;
            cell[2] = (cell[0] + cell[1]) / 2;
        }
    }
}

//! Test encoding and decoding
TEST(PNMFile, EncodingAndDecoding)
{
    // Test for various resolutions
    for (size_t rows = 0; rows < 10; rows++) {
        for (size_t columns = 0; columns < 100; columns++) {
            // Create source image
            PNMFile source;
            fillDummyImagePPM16(source, rows, columns);

            // Test for each image type
            for (uint8_t t = 0; t < static_cast<uint8_t>(PNMFile::Type::TYPE_COUNT); t++) {
                const PNMFile::Type type = static_cast<PNMFile::Type>(t);
                PNMFile converted = source;
                converted.convertTo(type);
                checkPNMEncodingAndDecoding(converted);
            }
        }
    }
}

//! Test writing and reading files
TEST(PNMFile, WriteAndReadFile)
{
    // Create test file
    PNMFile source;
    fillDummyImagePPM16(source, 32 /* <-- height */, 64 /* <-- width */);

    // Test for each image type
    for (uint8_t t = 0; t < static_cast<uint8_t>(PNMFile::Type::TYPE_COUNT); t++) {
        const PNMFile::Type type = static_cast<PNMFile::Type>(t);
        PNMFile converted = source;
        converted.convertTo(type);
        PNMFile readBack;
        const bool expectedResult = (type != PNMFile::Type::UNKNOWN);
        const std::string fileNameAscii = "TestPNMFile_" + converted.typeString() + "_ascii" + PNMFile::fileExtension(type);
        const std::string fileNameBinary = "TestPNMFile_" + converted.typeString() + "_binary" + PNMFile::fileExtension(type);
        if (type != PNMFile::Type::UNKNOWN) {
            ASSERT_EQ(converted.writeFile(fileNameAscii, PNMFileEncoding::Type::ASCII, false), expectedResult);
            readBack.clear();
            ASSERT_TRUE(readBack.readFile(fileNameAscii));
            ASSERT_TRUE(readBack == converted);
            ASSERT_EQ(converted.writeFile(fileNameAscii + ".gz", PNMFileEncoding::Type::ASCII), expectedResult);
            readBack.clear();
            ASSERT_TRUE(readBack.readFile(fileNameAscii + ".gz"));
            ASSERT_TRUE(readBack == converted);
            ASSERT_EQ(converted.writeFile(fileNameBinary, PNMFileEncoding::Type::BINARY, false), expectedResult);
            readBack.clear();
            ASSERT_TRUE(readBack.readFile(fileNameBinary));
            ASSERT_TRUE(readBack == converted);
            ASSERT_EQ(converted.writeFile(fileNameBinary + ".gz", PNMFileEncoding::Type::BINARY), expectedResult);
            readBack.clear();
            ASSERT_TRUE(readBack.readFile(fileNameBinary + ".gz"));
            ASSERT_TRUE(readBack == converted);
        } else {
#ifdef NDEBUG
            ASSERT_EQ(converted.writeFile(fileNameAscii, PNMFileEncoding::Type::ASCII, false), expectedResult);
            ASSERT_EQ(converted.writeFile(fileNameAscii + ".gz", PNMFileEncoding::Type::ASCII), expectedResult);
            ASSERT_EQ(converted.writeFile(fileNameBinary, PNMFileEncoding::Type::BINARY, false), expectedResult);
            ASSERT_EQ(converted.writeFile(fileNameBinary + ".gz", PNMFileEncoding::Type::BINARY), expectedResult);
#endif
        }
    }
}
