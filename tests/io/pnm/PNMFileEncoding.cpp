/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/pnm/PNMFileEncoding.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test converstion TO string representation of types
TEST(PNMFileEncoding, ToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PNMFileEncoding::Type::TYPE_COUNT); i++) {
        ASSERT_GT(PNMFileEncoding::toString(static_cast<PNMFileEncoding::Type>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(PNMFileEncoding::toString(static_cast<PNMFileEncoding::Type>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(PNMFileEncoding::toString(static_cast<PNMFileEncoding::Type>(i)) != "UNKNOWN");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(PNMFileEncoding::toString(PNMFileEncoding::Type::TYPE_COUNT) == "UNKNOWN");
#endif
}
