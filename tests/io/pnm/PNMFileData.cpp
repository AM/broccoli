/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/pnm/PNMFileData.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test conversion TO string representation of types
TEST(PNMFileData, TypeToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PNMFileData::Type::TYPE_COUNT); i++) {
        ASSERT_GT(PNMFileData::toString(static_cast<PNMFileData::Type>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(PNMFileData::toString(static_cast<PNMFileData::Type>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(PNMFileData::toString(static_cast<PNMFileData::Type>(i)) != "UNKNOWN");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(PNMFileData::toString(PNMFileData::Type::TYPE_COUNT) == "UNKNOWN");
#endif
}

//! Test conversion of type to corresponding file extension
TEST(PNMFileData, FileExtension)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PNMFileData::Type::TYPE_COUNT); i++)
        ASSERT_GT(PNMFileData::fileExtension(static_cast<PNMFileData::Type>(i)).size(), 0);
}

//! Generic template for checking equality between two objects
template <typename A, typename B>
static inline void checkEquality(const A& a, const B& b)
{
    ASSERT_TRUE(a == b);
    ASSERT_TRUE(b == a);
    ASSERT_FALSE(a != b);
    ASSERT_FALSE(b != a);
}

//! Generic template for checking inequality between two objects
template <typename A, typename B>
static inline void checkInequality(const A& a, const B& b)
{
    ASSERT_FALSE(a == b);
    ASSERT_FALSE(b == a);
    ASSERT_TRUE(a != b);
    ASSERT_TRUE(b != a);
}

//! Basic construction and operators
TEST(PNMFileData, ConstructionAndOperators)
{
    // Construction
    // ------------
    // Unknown type
    const PNMFileData empty;
    ASSERT_TRUE(empty.type() == PNMFileData::Type::UNKNOWN);

    // PBM
    const PNMFileData pbm(PNMFileData::Type::PBM);
    ASSERT_TRUE(pbm.type() == PNMFileData::Type::PBM);
    ASSERT_TRUE((pbm.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFileData pbmFilled(PNMFileData::Type::PBM, 12, 34);
    pbmFilled.dataPBM().fill(PNMFileData::PBMSample::WHITE);
    ASSERT_TRUE(pbmFilled.type() == PNMFileData::Type::PBM);
    ASSERT_TRUE((pbmFilled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PGM8
    const PNMFileData pgm8(PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE(pgm8.type() == PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE((pgm8.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFileData pgm8Filled(PNMFileData::Type::PGM_8BIT, 12, 34);
    pgm8Filled.dataPGM8().fill(123);
    ASSERT_TRUE(pgm8Filled.type() == PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE((pgm8Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PGM16
    const PNMFileData pgm16(PNMFileData::Type::PGM_16BIT);
    ASSERT_TRUE(pgm16.type() == PNMFileData::Type::PGM_16BIT);
    ASSERT_TRUE((pgm16.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFileData pgm16Filled(PNMFileData::Type::PGM_16BIT, 12, 34);
    pgm16Filled.dataPGM16().fill(1234);
    ASSERT_TRUE(pgm16Filled.type() == PNMFileData::Type::PGM_16BIT);
    ASSERT_TRUE((pgm16Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PPM8
    const PNMFileData ppm8(PNMFileData::Type::PPM_8BIT);
    ASSERT_TRUE(ppm8.type() == PNMFileData::Type::PPM_8BIT);
    ASSERT_TRUE((ppm8.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFileData ppm8Filled(PNMFileData::Type::PPM_8BIT, 12, 34);
    ppm8Filled.dataPPM8().fill({ 0, 127, 255 });
    ASSERT_TRUE(ppm8Filled.type() == PNMFileData::Type::PPM_8BIT);
    ASSERT_TRUE((ppm8Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // PGM16
    const PNMFileData ppm16(PNMFileData::Type::PPM_16BIT);
    ASSERT_TRUE(ppm16.type() == PNMFileData::Type::PPM_16BIT);
    ASSERT_TRUE((ppm16.size() == std::array<size_t, 2>{ 0, 0 }));
    PNMFileData ppm16Filled(PNMFileData::Type::PPM_16BIT, 12, 34);
    ppm16Filled.dataPPM16().fill({ 0, 1000, 2000 });
    ASSERT_TRUE(ppm16Filled.type() == PNMFileData::Type::PPM_16BIT);
    ASSERT_TRUE((ppm16Filled.size() == std::array<size_t, 2>{ 12, 34 }));

    // Operators
    // ---------
    // Compare empty against...
    checkEquality(empty, PNMFileData());
    checkInequality(empty, pbm);
    checkInequality(empty, pbmFilled);
    checkInequality(empty, pgm8);
    checkInequality(empty, pgm8Filled);
    checkInequality(empty, pgm16);
    checkInequality(empty, pgm16Filled);
    checkInequality(empty, ppm8);
    checkInequality(empty, ppm8Filled);
    checkInequality(empty, ppm16);
    checkInequality(empty, ppm16Filled);

    // Compare PBM against...
    PNMFileData newPbmFilled = PNMFileData(PNMFileData::Type::PBM, 12, 34);
    newPbmFilled.dataPBM().fill(PNMFileData::PBMSample::WHITE);
    checkEquality(pbm, PNMFileData(PNMFileData::Type::PBM));
    checkInequality(pbm, pbmFilled);
    checkInequality(pbm, pgm8);
    checkInequality(pbm, pgm8Filled);
    checkInequality(pbm, pgm16);
    checkInequality(pbm, pgm16Filled);
    checkInequality(pbm, ppm8);
    checkInequality(pbm, ppm8Filled);
    checkInequality(pbm, ppm16);
    checkInequality(pbm, ppm16Filled);
    checkEquality(pbmFilled, newPbmFilled);
    newPbmFilled.dataPBM().fill(PNMFileData::PBMSample::BLACK);
    checkInequality(pbmFilled, newPbmFilled);
    checkInequality(pbmFilled, pgm8);
    checkInequality(pbmFilled, pgm8Filled);
    checkInequality(pbmFilled, pgm16);
    checkInequality(pbmFilled, pgm16Filled);
    checkInequality(pbmFilled, ppm8);
    checkInequality(pbmFilled, ppm8Filled);
    checkInequality(pbmFilled, ppm16);
    checkInequality(pbmFilled, ppm16Filled);

    // Compare PGM8 against...
    PNMFileData newPgm8Filled = PNMFileData(PNMFileData::Type::PGM_8BIT, 12, 34);
    newPgm8Filled.dataPGM8().fill(123);
    checkEquality(pgm8, PNMFileData(PNMFileData::Type::PGM_8BIT));
    checkInequality(pgm8, pgm8Filled);
    checkInequality(pgm8, pgm16);
    checkInequality(pgm8, pgm16Filled);
    checkInequality(pgm8, ppm8);
    checkInequality(pgm8, ppm8Filled);
    checkInequality(pgm8, ppm16);
    checkInequality(pgm8, ppm16Filled);
    checkEquality(pgm8Filled, newPgm8Filled);
    newPgm8Filled.dataPGM8().fill(23);
    checkInequality(pgm8Filled, newPgm8Filled);
    checkInequality(pgm8Filled, pgm16);
    checkInequality(pgm8Filled, pgm16Filled);
    checkInequality(pgm8Filled, ppm8);
    checkInequality(pgm8Filled, ppm8Filled);
    checkInequality(pgm8Filled, ppm16);
    checkInequality(pgm8Filled, ppm16Filled);

    // Compare PGM16 against...
    PNMFileData newPgm16Filled = PNMFileData(PNMFileData::Type::PGM_16BIT, 12, 34);
    newPgm16Filled.dataPGM16().fill(1234);
    checkEquality(pgm16, PNMFileData(PNMFileData::Type::PGM_16BIT));
    checkInequality(pgm16, pgm16Filled);
    checkInequality(pgm16, ppm8);
    checkInequality(pgm16, ppm8Filled);
    checkInequality(pgm16, ppm16);
    checkInequality(pgm16, ppm16Filled);
    checkEquality(pgm16Filled, newPgm16Filled);
    newPgm16Filled.dataPGM16().fill(234);
    checkInequality(pgm16Filled, newPgm16Filled);
    checkInequality(pgm16Filled, ppm8);
    checkInequality(pgm16Filled, ppm8Filled);
    checkInequality(pgm16Filled, ppm16);
    checkInequality(pgm16Filled, ppm16Filled);

    // Compare PPM8 against...
    PNMFileData newPpm8Filled = PNMFileData(PNMFileData::Type::PPM_8BIT, 12, 34);
    newPpm8Filled.dataPPM8().fill({ 0, 127, 255 });
    checkEquality(ppm8, PNMFileData(PNMFileData::Type::PPM_8BIT));
    checkInequality(ppm8, ppm8Filled);
    checkInequality(ppm8, ppm16);
    checkInequality(ppm8, ppm16Filled);
    checkEquality(ppm8Filled, newPpm8Filled);
    newPpm8Filled.dataPPM8().fill({ 0, 127, 0 });
    checkInequality(ppm8Filled, newPpm8Filled);
    checkInequality(ppm8Filled, ppm16);
    checkInequality(ppm8Filled, ppm16Filled);

    // Compare PPM16 against...
    PNMFileData newPpm16Filled = PNMFileData(PNMFileData::Type::PPM_16BIT, 12, 34);
    newPpm16Filled.dataPPM16().fill({ 0, 1000, 2000 });
    checkEquality(ppm16Filled, newPpm16Filled);
    checkInequality(ppm16, ppm16Filled);
    newPpm16Filled.dataPPM16().fill({ 0, 1234, 2000 });
    checkInequality(ppm16Filled, newPpm16Filled);
}

//! Setters and Getters
TEST(PNMFileData, SettersAndGetters)
{
    // Const getter
    const PNMFileData constPBM(PNMFileData::Type::PBM);
    ASSERT_TRUE(constPBM.type() == PNMFileData::Type::PBM);
    ASSERT_TRUE(constPBM.typeString().size() > 0);
    ASSERT_TRUE(constPBM.dataPBM().elementCount() == 0);
    ASSERT_TRUE(constPBM.size() == constPBM.dataPBM().size());
    const PNMFileData constPGM8(PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE(constPGM8.type() == PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE(constPGM8.typeString().size() > 0);
    ASSERT_TRUE(constPGM8.dataPGM8().elementCount() == 0);
    ASSERT_TRUE(constPGM8.size() == constPGM8.dataPGM8().size());
    const PNMFileData constPGM16(PNMFileData::Type::PGM_16BIT);
    ASSERT_TRUE(constPGM16.type() == PNMFileData::Type::PGM_16BIT);
    ASSERT_TRUE(constPGM16.typeString().size() > 0);
    ASSERT_TRUE(constPGM16.dataPGM16().elementCount() == 0);
    ASSERT_TRUE(constPGM16.size() == constPGM16.dataPGM16().size());
    const PNMFileData constPPM8(PNMFileData::Type::PPM_8BIT);
    ASSERT_TRUE(constPPM8.type() == PNMFileData::Type::PPM_8BIT);
    ASSERT_TRUE(constPPM8.typeString().size() > 0);
    ASSERT_TRUE(constPPM8.dataPPM8().elementCount() == 0);
    ASSERT_TRUE(constPPM8.size() == constPPM8.dataPPM8().size());
    const PNMFileData constPPM16(PNMFileData::Type::PPM_16BIT);
    ASSERT_TRUE(constPPM16.type() == PNMFileData::Type::PPM_16BIT);
    ASSERT_TRUE(constPPM16.typeString().size() > 0);
    ASSERT_TRUE(constPPM16.dataPPM16().elementCount() == 0);
    ASSERT_TRUE(constPPM16.size() == constPPM16.dataPPM16().size());
#ifdef NDEBUG
    const PNMFileData constUnknown(PNMFileData::Type::UNKNOWN);
    ASSERT_TRUE((constUnknown.size() == std::array<size_t, 2>{ 0, 0 }));
#endif

    // Getter and setter
    PNMFileData fileData(PNMFileData::Type::PBM, 12, 34);
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PBM);
    ASSERT_TRUE(fileData.dataPBM().elementCount() == 12 * 34);
    fileData.setType(PNMFileData::Type::PBM);
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PBM);
    ASSERT_TRUE(fileData.dataPBM().elementCount() == 12 * 34);
    fileData.setType(PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE(fileData.dataPGM8().elementCount() == 0);
    fileData.dataPGM8().resize(12, 34);
    ASSERT_TRUE(fileData.dataPGM8().elementCount() == 12 * 34);
    fileData.clearData();
    ASSERT_TRUE(fileData.dataPGM8().elementCount() == 0);
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PGM_8BIT);
    fileData.dataPGM8().resize(12, 34);
    ASSERT_TRUE(fileData.dataPGM8().elementCount() == 12 * 34);
    fileData.clear();
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::UNKNOWN);
    const PNMFileData filledPBM(PNMFileData::Type::PBM, 12, 34);
    fileData.setData(filledPBM.dataPBM());
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PBM);
    ASSERT_TRUE(fileData.dataPBM().elementCount() == 12 * 34);
    const PNMFileData filledPGM8(PNMFileData::Type::PGM_8BIT, 12, 34);
    fileData.setData(filledPGM8.dataPGM8());
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PGM_8BIT);
    ASSERT_TRUE(fileData.dataPGM8().elementCount() == 12 * 34);
    const PNMFileData filledPGM16(PNMFileData::Type::PGM_16BIT, 12, 34);
    fileData.setData(filledPGM16.dataPGM16());
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PGM_16BIT);
    ASSERT_TRUE(fileData.dataPGM16().elementCount() == 12 * 34);
    const PNMFileData filledPPM8(PNMFileData::Type::PPM_8BIT, 12, 34);
    fileData.setData(filledPPM8.dataPPM8());
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PPM_8BIT);
    ASSERT_TRUE(fileData.dataPPM8().elementCount() == 12 * 34);
    const PNMFileData filledPPM16(PNMFileData::Type::PPM_16BIT, 12, 34);
    fileData.setData(filledPPM16.dataPPM16());
    ASSERT_TRUE(fileData.type() == PNMFileData::Type::PPM_16BIT);
    ASSERT_TRUE(fileData.dataPPM16().elementCount() == 12 * 34);
}

//! Test conversion between data types
TEST(PNMFileData, ConversionPBMToOther)
{
    // Initialize helpers
    PNMFileData convertedData, expectedData;

    // Setup source
    PNMFileData source(PNMFileData::Type::PBM, 2, 3);
    source.dataPBM()(0, 0) = PNMFileData::PBMSample::BLACK; // Red
    source.dataPBM()(0, 1) = PNMFileData::PBMSample::BLACK; // Green
    source.dataPBM()(0, 2) = PNMFileData::PBMSample::BLACK; // Blue
    source.dataPBM()(1, 0) = PNMFileData::PBMSample::WHITE; // Cyan
    source.dataPBM()(1, 1) = PNMFileData::PBMSample::WHITE; // Magenta
    source.dataPBM()(1, 2) = PNMFileData::PBMSample::WHITE; // Yellow

    // Test converstion to PBM
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PBM);
    expectedData = source;
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PGM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_8BIT, 2, 3);
    expectedData.dataPGM8()(0, 0) = 0; // Red
    expectedData.dataPGM8()(0, 1) = 0; // Green
    expectedData.dataPGM8()(0, 2) = 0; // Blue
    expectedData.dataPGM8()(1, 0) = 255; // Cyan
    expectedData.dataPGM8()(1, 1) = 255; // Magenta
    expectedData.dataPGM8()(1, 2) = 255; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PGM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_16BIT, 2, 3);
    expectedData.dataPGM16()(0, 0) = 0; // Red
    expectedData.dataPGM16()(0, 1) = 0; // Green
    expectedData.dataPGM16()(0, 2) = 0; // Blue
    expectedData.dataPGM16()(1, 0) = 65535; // Cyan
    expectedData.dataPGM16()(1, 1) = 65535; // Magenta
    expectedData.dataPGM16()(1, 2) = 65535; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_8BIT, 2, 3);
    expectedData.dataPPM8()(0, 0) = { 0, 0, 0 }; // Red
    expectedData.dataPPM8()(0, 1) = { 0, 0, 0 }; // Green
    expectedData.dataPPM8()(0, 2) = { 0, 0, 0 }; // Blue
    expectedData.dataPPM8()(1, 0) = { 255, 255, 255 }; // Cyan
    expectedData.dataPPM8()(1, 1) = { 255, 255, 255 }; // Magenta
    expectedData.dataPPM8()(1, 2) = { 255, 255, 255 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_16BIT, 2, 3);
    expectedData.dataPPM16()(0, 0) = { 0, 0, 0 }; // Red
    expectedData.dataPPM16()(0, 1) = { 0, 0, 0 }; // Green
    expectedData.dataPPM16()(0, 2) = { 0, 0, 0 }; // Blue
    expectedData.dataPPM16()(1, 0) = { 65535, 65535, 65535 }; // Cyan
    expectedData.dataPPM16()(1, 1) = { 65535, 65535, 65535 }; // Magenta
    expectedData.dataPPM16()(1, 2) = { 65535, 65535, 65535 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);
}

//! Test conversion between data types
TEST(PNMFileData, ConversionPGM8ToOther)
{
    // Initialize helpers
    PNMFileData convertedData, expectedData;

    // Setup source
    PNMFileData source(PNMFileData::Type::PGM_8BIT, 2, 3);
    source.dataPGM8()(0, 0) = 85; // Red
    source.dataPGM8()(0, 1) = 85; // Green
    source.dataPGM8()(0, 2) = 85; // Blue
    source.dataPGM8()(1, 0) = 170; // Cyan
    source.dataPGM8()(1, 1) = 170; // Magenta
    source.dataPGM8()(1, 2) = 170; // Yellow

    // Test conversion to PBM
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PBM);
    expectedData = PNMFileData(PNMFileData::Type::PBM, 2, 3);
    expectedData.dataPBM()(0, 0) = PNMFileData::PBMSample::BLACK; // Red
    expectedData.dataPBM()(0, 1) = PNMFileData::PBMSample::BLACK; // Green
    expectedData.dataPBM()(0, 2) = PNMFileData::PBMSample::BLACK; // Blue
    expectedData.dataPBM()(1, 0) = PNMFileData::PBMSample::WHITE; // Cyan
    expectedData.dataPBM()(1, 1) = PNMFileData::PBMSample::WHITE; // Magenta
    expectedData.dataPBM()(1, 2) = PNMFileData::PBMSample::WHITE; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test converstion to PGM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_8BIT);
    expectedData = source;
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PGM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_16BIT, 2, 3);
    expectedData.dataPGM16()(0, 0) = 21845; // Red
    expectedData.dataPGM16()(0, 1) = 21845; // Green
    expectedData.dataPGM16()(0, 2) = 21845; // Blue
    expectedData.dataPGM16()(1, 0) = 43690; // Cyan
    expectedData.dataPGM16()(1, 1) = 43690; // Magenta
    expectedData.dataPGM16()(1, 2) = 43690; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_8BIT, 2, 3);
    expectedData.dataPPM8()(0, 0) = { 85, 85, 85 }; // Red
    expectedData.dataPPM8()(0, 1) = { 85, 85, 85 }; // Green
    expectedData.dataPPM8()(0, 2) = { 85, 85, 85 }; // Blue
    expectedData.dataPPM8()(1, 0) = { 170, 170, 170 }; // Cyan
    expectedData.dataPPM8()(1, 1) = { 170, 170, 170 }; // Magenta
    expectedData.dataPPM8()(1, 2) = { 170, 170, 170 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_16BIT, 2, 3);
    expectedData.dataPPM16()(0, 0) = { 21845, 21845, 21845 }; // Red
    expectedData.dataPPM16()(0, 1) = { 21845, 21845, 21845 }; // Green
    expectedData.dataPPM16()(0, 2) = { 21845, 21845, 21845 }; // Blue
    expectedData.dataPPM16()(1, 0) = { 43690, 43690, 43690 }; // Cyan
    expectedData.dataPPM16()(1, 1) = { 43690, 43690, 43690 }; // Magenta
    expectedData.dataPPM16()(1, 2) = { 43690, 43690, 43690 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);
}

//! Test conversion between data types
TEST(PNMFileData, ConversionPGM16ToOther)
{
    // Initialize helpers
    PNMFileData convertedData, expectedData;

    // Setup source
    PNMFileData source(PNMFileData::Type::PGM_16BIT, 2, 3);
    source.dataPGM16()(0, 0) = 21845; // Red
    source.dataPGM16()(0, 1) = 21845; // Green
    source.dataPGM16()(0, 2) = 21845; // Blue
    source.dataPGM16()(1, 0) = 43690; // Cyan
    source.dataPGM16()(1, 1) = 43690; // Magenta
    source.dataPGM16()(1, 2) = 43690; // Yellow

    // Test conversion to PBM
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PBM);
    expectedData = PNMFileData(PNMFileData::Type::PBM, 2, 3);
    expectedData.dataPBM()(0, 0) = PNMFileData::PBMSample::BLACK; // Red
    expectedData.dataPBM()(0, 1) = PNMFileData::PBMSample::BLACK; // Green
    expectedData.dataPBM()(0, 2) = PNMFileData::PBMSample::BLACK; // Blue
    expectedData.dataPBM()(1, 0) = PNMFileData::PBMSample::WHITE; // Cyan
    expectedData.dataPBM()(1, 1) = PNMFileData::PBMSample::WHITE; // Magenta
    expectedData.dataPBM()(1, 2) = PNMFileData::PBMSample::WHITE; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test converstion to PGM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_8BIT, 2, 3);
    expectedData.dataPGM8()(0, 0) = 85; // Red
    expectedData.dataPGM8()(0, 1) = 85; // Green
    expectedData.dataPGM8()(0, 2) = 85; // Blue
    expectedData.dataPGM8()(1, 0) = 170; // Cyan
    expectedData.dataPGM8()(1, 1) = 170; // Magenta
    expectedData.dataPGM8()(1, 2) = 170; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PGM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_16BIT);
    expectedData = source;
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_8BIT, 2, 3);
    expectedData.dataPPM8()(0, 0) = { 85, 85, 85 }; // Red
    expectedData.dataPPM8()(0, 1) = { 85, 85, 85 }; // Green
    expectedData.dataPPM8()(0, 2) = { 85, 85, 85 }; // Blue
    expectedData.dataPPM8()(1, 0) = { 170, 170, 170 }; // Cyan
    expectedData.dataPPM8()(1, 1) = { 170, 170, 170 }; // Magenta
    expectedData.dataPPM8()(1, 2) = { 170, 170, 170 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_16BIT, 2, 3);
    expectedData.dataPPM16()(0, 0) = { 21845, 21845, 21845 }; // Red
    expectedData.dataPPM16()(0, 1) = { 21845, 21845, 21845 }; // Green
    expectedData.dataPPM16()(0, 2) = { 21845, 21845, 21845 }; // Blue
    expectedData.dataPPM16()(1, 0) = { 43690, 43690, 43690 }; // Cyan
    expectedData.dataPPM16()(1, 1) = { 43690, 43690, 43690 }; // Magenta
    expectedData.dataPPM16()(1, 2) = { 43690, 43690, 43690 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);
}

//! Test conversion between data types
TEST(PNMFileData, ConversionPPM8ToOther)
{
    // Initialize helpers
    PNMFileData convertedData, expectedData;

    // Setup source
    PNMFileData source(PNMFileData::Type::PPM_8BIT, 2, 3);
    source.dataPPM8()(0, 0) = { 255, 0, 0 }; // Red
    source.dataPPM8()(0, 1) = { 0, 255, 0 }; // Green
    source.dataPPM8()(0, 2) = { 0, 0, 255 }; // Blue
    source.dataPPM8()(1, 0) = { 0, 255, 255 }; // Cyan
    source.dataPPM8()(1, 1) = { 255, 0, 255 }; // Magenta
    source.dataPPM8()(1, 2) = { 255, 255, 0 }; // Yellow

    // Test conversion to PBM
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PBM);
    expectedData = PNMFileData(PNMFileData::Type::PBM, 2, 3);
    expectedData.dataPBM()(0, 0) = PNMFileData::PBMSample::BLACK; // Red
    expectedData.dataPBM()(0, 1) = PNMFileData::PBMSample::BLACK; // Green
    expectedData.dataPBM()(0, 2) = PNMFileData::PBMSample::BLACK; // Blue
    expectedData.dataPBM()(1, 0) = PNMFileData::PBMSample::WHITE; // Cyan
    expectedData.dataPBM()(1, 1) = PNMFileData::PBMSample::WHITE; // Magenta
    expectedData.dataPBM()(1, 2) = PNMFileData::PBMSample::WHITE; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test converstion to PGM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_8BIT, 2, 3);
    expectedData.dataPGM8()(0, 0) = 85; // Red
    expectedData.dataPGM8()(0, 1) = 85; // Green
    expectedData.dataPGM8()(0, 2) = 85; // Blue
    expectedData.dataPGM8()(1, 0) = 170; // Cyan
    expectedData.dataPGM8()(1, 1) = 170; // Magenta
    expectedData.dataPGM8()(1, 2) = 170; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PGM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_16BIT, 2, 3);
    expectedData.dataPGM16()(0, 0) = 21845; // Red
    expectedData.dataPGM16()(0, 1) = 21845; // Green
    expectedData.dataPGM16()(0, 2) = 21845; // Blue
    expectedData.dataPGM16()(1, 0) = 43690; // Cyan
    expectedData.dataPGM16()(1, 1) = 43690; // Magenta
    expectedData.dataPGM16()(1, 2) = 43690; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_8BIT);
    expectedData = source;
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_16BIT, 2, 3);
    expectedData.dataPPM16()(0, 0) = { 65535, 0, 0 }; // Red
    expectedData.dataPPM16()(0, 1) = { 0, 65535, 0 }; // Green
    expectedData.dataPPM16()(0, 2) = { 0, 0, 65535 }; // Blue
    expectedData.dataPPM16()(1, 0) = { 0, 65535, 65535 }; // Cyan
    expectedData.dataPPM16()(1, 1) = { 65535, 0, 65535 }; // Magenta
    expectedData.dataPPM16()(1, 2) = { 65535, 65535, 0 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);
}

//! Test conversion between data types
TEST(PNMFileData, ConversionPPM16ToOther)
{
    // Initialize helpers
    PNMFileData convertedData, expectedData;

    // Setup source
    PNMFileData source(PNMFileData::Type::PPM_16BIT, 2, 3);
    source.dataPPM16()(0, 0) = { 65535, 0, 0 }; // Red
    source.dataPPM16()(0, 1) = { 0, 65535, 0 }; // Green
    source.dataPPM16()(0, 2) = { 0, 0, 65535 }; // Blue
    source.dataPPM16()(1, 0) = { 0, 65535, 65535 }; // Cyan
    source.dataPPM16()(1, 1) = { 65535, 0, 65535 }; // Magenta
    source.dataPPM16()(1, 2) = { 65535, 65535, 0 }; // Yellow

    // Test conversion to PBM
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PBM);
    expectedData = PNMFileData(PNMFileData::Type::PBM, 2, 3);
    expectedData.dataPBM()(0, 0) = PNMFileData::PBMSample::BLACK; // Red
    expectedData.dataPBM()(0, 1) = PNMFileData::PBMSample::BLACK; // Green
    expectedData.dataPBM()(0, 2) = PNMFileData::PBMSample::BLACK; // Blue
    expectedData.dataPBM()(1, 0) = PNMFileData::PBMSample::WHITE; // Cyan
    expectedData.dataPBM()(1, 1) = PNMFileData::PBMSample::WHITE; // Magenta
    expectedData.dataPBM()(1, 2) = PNMFileData::PBMSample::WHITE; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test converstion to PGM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_8BIT, 2, 3);
    expectedData.dataPGM8()(0, 0) = 85; // Red
    expectedData.dataPGM8()(0, 1) = 85; // Green
    expectedData.dataPGM8()(0, 2) = 85; // Blue
    expectedData.dataPGM8()(1, 0) = 170; // Cyan
    expectedData.dataPGM8()(1, 1) = 170; // Magenta
    expectedData.dataPGM8()(1, 2) = 170; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PGM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PGM_16BIT);
    expectedData = PNMFileData(PNMFileData::Type::PGM_16BIT, 2, 3);
    expectedData.dataPGM16()(0, 0) = 21845; // Red
    expectedData.dataPGM16()(0, 1) = 21845; // Green
    expectedData.dataPGM16()(0, 2) = 21845; // Blue
    expectedData.dataPGM16()(1, 0) = 43690; // Cyan
    expectedData.dataPGM16()(1, 1) = 43690; // Magenta
    expectedData.dataPGM16()(1, 2) = 43690; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM8
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_8BIT);
    expectedData = PNMFileData(PNMFileData::Type::PPM_8BIT, 2, 3);
    expectedData.dataPPM8()(0, 0) = { 255, 0, 0 }; // Red
    expectedData.dataPPM8()(0, 1) = { 0, 255, 0 }; // Green
    expectedData.dataPPM8()(0, 2) = { 0, 0, 255 }; // Blue
    expectedData.dataPPM8()(1, 0) = { 0, 255, 255 }; // Cyan
    expectedData.dataPPM8()(1, 1) = { 255, 0, 255 }; // Magenta
    expectedData.dataPPM8()(1, 2) = { 255, 255, 0 }; // Yellow
    ASSERT_TRUE(convertedData == expectedData);

    // Test conversion to PPM16
    convertedData = source;
    convertedData.convertTo(PNMFileData::Type::PPM_16BIT);
    expectedData = source;
    ASSERT_TRUE(convertedData == expectedData);
}
