/*
 * This file is part of broccoli.
 * Copyright (C) 2019 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/joystick/Joystick.hpp"
#include "gtest/gtest.h"
#include <SDL.h>

using namespace broccoli;
using namespace io;

#ifdef HAVE_SDL2

//! Test string representation of the given power level
TEST(JoystickInformation, TypesToString)
{
    int8_t i;
    JoystickInformation testPowerLevel;

    for (i = -1; i <= 5; i++) {
        testPowerLevel.m_powerLevel = static_cast<SDL_JoystickPowerLevel>(i);
        ASSERT_GT(testPowerLevel.powerLevelString().size(), 0);
        ASSERT_TRUE(testPowerLevel.powerLevelString() != "UNKNOWN");
    }

#ifdef NDEBUG
    testPowerLevel.m_powerLevel = static_cast<SDL_JoystickPowerLevel>(6);
    ASSERT_GT(testPowerLevel.powerLevelString().size(), 0);
    ASSERT_TRUE(testPowerLevel.powerLevelString() == "UNKNOWN");
#endif
}

//! Test string representation of the given event type
TEST(JoystickEvent, EventTypesToString)
{
    uint16_t i;
    JoystickEvent testEventType;

    i = 0x100;
    testEventType.m_type = static_cast<SDL_EventType>(i);
    ASSERT_TRUE(testEventType.typeString() == "SDL_QUIT");

    for (i = 0x600; i <= 0x606; i++) {
        testEventType.m_type = static_cast<SDL_EventType>(i);
        ASSERT_GT(testEventType.typeString().size(), 0);
        ASSERT_TRUE(testEventType.typeString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = 0x607;
    testEventType.m_type = static_cast<SDL_EventType>(i);
    ASSERT_TRUE(testEventType.typeString() == "UNKNOWN");
#endif
}

//! Test string specification of action type to perform
TEST(JoystickAction, ActionsToString)
{
    uint8_t i;
    JoystickAction testAction;

    for (i = 0; i < static_cast<uint8_t>(JoystickAction::Type::TYPE_COUNT); i++) {
        testAction.m_type = static_cast<JoystickAction::Type>(i);
        ASSERT_GT(testAction.typeString().size(), 0);
        ASSERT_TRUE(testAction.typeString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(JoystickSignal::Type::TYPE_COUNT);
    testAction.m_type = static_cast<JoystickAction::Type>(i);
    ASSERT_TRUE(testAction.typeString() == "UNKNOWN");
#endif
}

//! Test string representation of the given hat value
TEST(JoystickState, HatValueToString)
{
    ASSERT_TRUE(JoystickState::hatValueString(0x00) == "SDL_HAT_CENTERED");
    ASSERT_TRUE(JoystickState::hatValueString(0x01) == "SDL_HAT_UP");
    ASSERT_TRUE(JoystickState::hatValueString(0x02) == "SDL_HAT_RIGHT");
    ASSERT_TRUE(JoystickState::hatValueString(0x04) == "SDL_HAT_DOWN");
    ASSERT_TRUE(JoystickState::hatValueString(0x08) == "SDL_HAT_LEFT");
    ASSERT_TRUE(JoystickState::hatValueString(0x01 | 0x02) == "SDL_HAT_RIGHTUP");
    ASSERT_TRUE(JoystickState::hatValueString(0x02 | 0x04) == "SDL_HAT_RIGHTDOWN");
    ASSERT_TRUE(JoystickState::hatValueString(0x01 | 0x08) == "SDL_HAT_LEFTUP");
    ASSERT_TRUE(JoystickState::hatValueString(0x04 | 0x08) == "SDL_HAT_LEFTDOWN");
}

//! Test string representation of types
TEST(JoystickSignal, TypesToString)
{
    uint8_t i = 0;
    JoystickSignal testSignal;

    for (i = 0; i < static_cast<uint8_t>(JoystickSignal::Type::TYPE_COUNT); i++) {
        testSignal.m_type = static_cast<JoystickSignal::Type>(i);
        ASSERT_GT(testSignal.typeString().size(), 0);
        ASSERT_TRUE(testSignal.typeString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(JoystickSignal::Type::TYPE_COUNT);
    testSignal.m_type = static_cast<JoystickSignal::Type>(i);
    ASSERT_TRUE(testSignal.typeString() == "UNKNOWN");
#endif
}

//! Test string representation of messages
TEST(JoystickSignal, MessagesToString)
{
    uint8_t i = 0;
    JoystickSignal testMessage;

    for (i = 0; i < static_cast<uint8_t>(JoystickSignal::Message::MESSAGE_COUNT); i++) {
        testMessage.m_message = static_cast<JoystickSignal::Message>(i);
        ASSERT_GT(testMessage.messageString().size(), 0);
        ASSERT_TRUE(testMessage.messageString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(JoystickSignal::Message::MESSAGE_COUNT);
    testMessage.m_message = static_cast<JoystickSignal::Message>(i);
    ASSERT_TRUE(testMessage.messageString() == "UNKNOWN");
#endif
}

//! Test string representation of types
TEST(Joystick, TypesToString)
{
    // Just check, if string is non-empty
    JoystickAction testAction;
    JoystickEvent testEvent;
    JoystickInformation testInformation;
    JoystickSignal testSignal;
    ASSERT_GT(testAction.typeString().size(), 0);
    ASSERT_GT(testEvent.typeString().size(), 0);
    ASSERT_GT(testInformation.powerLevelString().size(), 0);
    ASSERT_GT(testSignal.typeString().size(), 0);
    ASSERT_GT(testSignal.messageString().size(), 0);
    ASSERT_GT(JoystickState::hatValueString(SDL_HAT_LEFTUP).size(), 0);
}

//! Test setters and getters
TEST(Joystick, SettersAndGetters)
{
    // Create instance
    Joystick joystick(0, "TestDevice");

    // Setters and getters
    std::string desiredDeviceName = "A";
    joystick.setDesiredDeviceName(desiredDeviceName);
    ASSERT_EQ(desiredDeviceName, joystick.desiredDeviceName());
    unsigned int motionThreshold = 123;
    joystick.setMotionThreshold(motionThreshold);
    ASSERT_EQ(motionThreshold, joystick.motionThreshold());

    // Getters only
    bool isSDLInitialized = joystick.isSDLInitialized();
    ASSERT_EQ(isSDLInitialized, false);
    bool isConnected = joystick.isConnected();
    ASSERT_EQ(isConnected, false);
    std::vector<JoystickInformation> availableJoysticks = joystick.availableJoysticks();
    (void)availableJoysticks;
    JoystickInformation activeJoystick = joystick.activeJoystick();
    (void)activeJoystick;
    JoystickState initialState = joystick.initialState();
    (void)initialState;
    JoystickState currentState = joystick.currentState();
    (void)currentState;
}

//! Check basic creation and handling of joystick instance
TEST(Joystick, BasicHandling)
{
    // Create joystick instace
    Joystick joystick(0, "TestDevice");

    // Copy constructor
    Joystick joystickCopy(joystick);
    ASSERT_TRUE(joystickCopy.desiredDeviceName() == joystick.desiredDeviceName());

    // Assignment operator
    joystickCopy.setDesiredDeviceName("abc");
    joystickCopy = joystick;
    ASSERT_TRUE(joystickCopy.desiredDeviceName() == joystick.desiredDeviceName());

    // Initialize joystick module
    joystick.initialize();

    // Check, if SDL was initialized successfully
    ASSERT_EQ(joystick.isSDLInitialized(), true);

    // Wait until background joystick of joystick was executed at least once
    while (joystick.executionCalls() <= 1) {
        // Wait...
    }

    // Get list of available devices
    std::vector<JoystickInformation> availableJoysticks = joystick.availableJoysticks();

    // Output device list
    std::cout << "available joysticks:" << std::endl;
    for (size_t i = 0; i < availableJoysticks.size(); i++) {
        std::cout << "#" << i << ": Name: " << availableJoysticks[i].m_name << " | GUID: " << availableJoysticks[i].m_GUIDstring << std::endl;
    }
    if (availableJoysticks.size() == 0)
        std::cout << "none!" << std::endl;

    // Deinitialize joystick module
    joystick.deInitialize();
}

#else
#warning("Skipped Joystick test as SDL2 was not found!")
#endif
