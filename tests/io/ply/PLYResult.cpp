/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/ply/PLYResult.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test converstion TO string representation of results
TEST(PLYResult, ToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PLYResult::PLYRESULT_COUNT); i++) {
        ASSERT_GT(PLYResultToString(static_cast<PLYResult>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(PLYResultToString(static_cast<PLYResult>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(PLYResultToString(static_cast<PLYResult>(i)) != "UNKNOWN");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(PLYResultToString(PLYResult::PLYRESULT_COUNT) == "UNKNOWN");
#endif
}
