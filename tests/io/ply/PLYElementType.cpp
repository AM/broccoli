/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/ply/PLYElementType.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test operators of PLYPropertyValue
TEST(PLYElementType, Operators)
{
    PLYElementType typeA, typeB;

    // Test equality
    ASSERT_TRUE(typeA == typeB);
    ASSERT_FALSE(typeA != typeB);

    // Test inequality (name)
    typeB = typeA;
    typeB.m_name = "a";
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);

    // Test inequality (elementCount)
    typeB = typeA;
    typeB.m_elementCount = 3;
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);

    // Test inequality (propertyTypes)
    typeB = typeA;
    typeB.m_propertyTypes.resize(1);
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);

    // Test inequality (data)
    typeB = typeA;
    typeB.m_data.resize(1);
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);
}

//! Test de- and encoding of header
TEST(PLYElementType, EncodingAndDecodingHeader)
{
    // Initialize helpers
    PLYResult result = PLYResult::UNKNOWN;
    encoding::CharacterStream stream;
    PLYElementType inputType, outputType;
    inputType.m_name = "abc";
    inputType.m_elementCount = 2;
    inputType.m_propertyTypes.resize(2);
    inputType.m_propertyTypes[0].m_name = "ScalarProperty";
    inputType.m_propertyTypes[1].m_name = "ListProperty";
    inputType.m_propertyTypes[1].m_isListProperty = true;

    // Encoding and decoding
    inputType.encodeHeader(stream);
    std::string inputString = std::string(stream.begin(), stream.end());
    ASSERT_TRUE(outputType.decodeHeader(core::stringSplit(inputString, '\n', true), &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);

    // Test error checking
#ifdef NDEBUG
    inputString = "elemen vertex 5";
    ASSERT_FALSE(outputType.decodeHeader(core::stringSplit(inputString, '\n', true), &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_ELEMENTTYPE);
    inputString = "element vertex";
    ASSERT_FALSE(outputType.decodeHeader(core::stringSplit(inputString, '\n', true), &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_ELEMENTTYPE);
    inputString = "element vertex abc";
    ASSERT_FALSE(outputType.decodeHeader(core::stringSplit(inputString, '\n', true), &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_ELEMENTTYPE);
    inputString = "element vertex 5 abc";
    ASSERT_FALSE(outputType.decodeHeader(core::stringSplit(inputString, '\n', true), &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_ELEMENTTYPE);
    inputString = "element vertex 5\nproperty float";
    ASSERT_FALSE(outputType.decodeHeader(core::stringSplit(inputString, '\n', true), &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
#endif
}

//! Test of bufferMatchesSpecification()
TEST(PLYElementType, BufferMatchesSpecification)
{
    // Initialize helpers
    PLYResult result;

    // Setup element
    PLYElementType elementType;
    elementType.m_elementCount = 1;

    // Test invalid element count
#ifdef NDEBUG
    ASSERT_FALSE(elementType.bufferMatchesSpecification(&result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_ELEMENTCOUNT);
#endif

    // Test invalid property count
    elementType.m_data.resize(1);
    elementType.m_propertyTypes.resize(1);
#ifdef NDEBUG
    ASSERT_FALSE(elementType.bufferMatchesSpecification(&result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_PROPERTYCOUNT);
#endif

    // Test invalid scalar type of value
    elementType.m_data[0].resize(1);
    elementType.m_propertyTypes[0].m_valueType = PLYScalar::Type::UNKNOWN;
#ifdef NDEBUG
    ASSERT_FALSE(elementType.bufferMatchesSpecification(&result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_SCALARTYPE);
#endif

    // Test invalid buffer size (too small)
    elementType.m_data[0][0].setScalar((CHAR_t)123);
    elementType.m_propertyTypes[0].m_valueType = PLYScalar::Type::SHORT;
#ifdef NDEBUG
    ASSERT_FALSE(elementType.bufferMatchesSpecification(&result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_PROPERTYVALUE);
#endif

    // Test invalid buffer size (too big)
    elementType.m_data[0][0].setScalar((SHORT_t)123);
    elementType.m_propertyTypes[0].m_valueType = PLYScalar::Type::CHAR;
#ifdef NDEBUG
    ASSERT_FALSE(elementType.bufferMatchesSpecification(&result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_PROPERTYVALUE);
#endif

    // Test success
    elementType.m_data[0][0].setScalar((CHAR_t)123);
    elementType.m_propertyTypes[0].m_valueType = PLYScalar::Type::CHAR;
    ASSERT_TRUE(elementType.bufferMatchesSpecification(&result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
}

//! Helper method for testing encoding and decoding buffer for different counter and value types
template <typename ValueType>
void checkEncodingAndDecodingBufferTyped(const PLYScalar::Type& counterType, const PLYScalar::Type& valueType)
{
    // Initialize helpers
    PLYResult result = PLYResult::UNKNOWN;
    encoding::CharacterStream stream;
    encoding::CharacterStreamSize inputStreamSize, outputStreamSize;
    PLYElementType inputType, outputType, brokenType;
    inputType.m_name = "abc";
    inputType.m_elementCount = 2;
    inputType.m_propertyTypes.resize(2);
    inputType.m_propertyTypes[0].m_name = "ScalarProperty";
    inputType.m_propertyTypes[0].m_valueType = valueType;
    inputType.m_propertyTypes[1].m_name = "ListProperty";
    inputType.m_propertyTypes[1].m_isListProperty = true;
    inputType.m_propertyTypes[1].m_counterType = counterType;
    inputType.m_propertyTypes[1].m_valueType = valueType;
    outputType = inputType; // Copy "header"
    inputType.m_data.resize(2);
    for (int i = 0; i < 2; i++) {
        inputType.m_data[i].resize(2);
        inputType.m_data[i][0].setScalar((ValueType)123);
        inputType.m_data[i][1].setList(std::vector<ValueType>{ 12, 34 });
    }

    // Encoding and decoding in ASCII format
    stream.clear();
    inputStreamSize = inputType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result);
    ASSERT_TRUE(inputStreamSize > 0);
    ASSERT_EQ(inputStreamSize, stream.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    outputStreamSize = outputType.decodeBuffer(stream, 0, PLYFormat::Type::ASCII, &result);
    ASSERT_TRUE(outputStreamSize > 0);
    ASSERT_EQ(outputStreamSize, stream.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputType == inputType);

    // Encoding and decoding in BINARY (little endian) format
    stream.clear();
    inputStreamSize = inputType.encodeBuffer(stream, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result);
    ASSERT_TRUE(inputStreamSize > 0);
    ASSERT_EQ(inputStreamSize, stream.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    outputStreamSize = outputType.decodeBuffer(stream, 0, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result);
    ASSERT_TRUE(outputStreamSize > 0);
    ASSERT_EQ(outputStreamSize, stream.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputType == inputType);

    // Encoding and decoding in BINARY (big endian) format
    stream.clear();
    inputStreamSize = inputType.encodeBuffer(stream, PLYFormat::Type::BINARY_BIG_ENDIAN, &result);
    ASSERT_TRUE(inputStreamSize > 0);
    ASSERT_EQ(inputStreamSize, stream.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    outputStreamSize = outputType.decodeBuffer(stream, 0, PLYFormat::Type::BINARY_BIG_ENDIAN, &result);
    ASSERT_TRUE(outputStreamSize > 0);
    ASSERT_EQ(outputStreamSize, stream.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputType == inputType);

    // Test error checking
#ifdef NDEBUG
    // Encoding - wrong element size
    brokenType = inputType;
    brokenType.m_elementCount = 0;
    ASSERT_EQ(brokenType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_ELEMENTCOUNT);

    // Encoding - wrong property count
    brokenType = inputType;
    brokenType.m_propertyTypes.clear();
    ASSERT_EQ(brokenType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_PROPERTYCOUNT);

    // Encoding - invalid value type
    brokenType = inputType;
    brokenType.m_propertyTypes[0].m_valueType = PLYScalar::Type::UNKNOWN;
    ASSERT_EQ(brokenType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_SCALARTYPE);

    // Encoding - invalid counter type
    brokenType = inputType;
    brokenType.m_propertyTypes[1].m_counterType = PLYScalar::Type::UNKNOWN;
    ASSERT_EQ(brokenType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_SCALARTYPE);

    // Decoding - broken stream
    stream.clear();
    inputType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result);
    stream[0] = 'a';
    stream[1] = ' ';
    brokenType = inputType;
    ASSERT_EQ(brokenType.decodeBuffer(stream, 0, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_SCALAR);

    // Decoding - invalid counter type
    stream.clear();
    inputType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result);
    brokenType = inputType;
    brokenType.m_propertyTypes[0].m_counterType = PLYScalar::Type::UNKNOWN;
    ASSERT_EQ(brokenType.decodeBuffer(stream, 0, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_SCALARTYPE);

    // Decoding - invalid value type
    stream.clear();
    inputType.encodeBuffer(stream, PLYFormat::Type::ASCII, &result);
    brokenType = inputType;
    brokenType.m_propertyTypes[0].m_valueType = PLYScalar::Type::UNKNOWN;
    ASSERT_EQ(brokenType.decodeBuffer(stream, 0, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_SCALARTYPE);
#endif
}

//! Test de- and encoding of buffer
TEST(PLYElementType, EncodingAndDecodingBuffer)
{
    // Repeat for all counter types
    for (uint8_t i = 0; i < static_cast<uint8_t>(PLYScalar::Type::TYPE_COUNT); i++) {
        PLYScalar::Type counterType = static_cast<PLYScalar::Type>(i);
        // Repeat for all value types
        if (counterType != PLYScalar::Type::UNKNOWN) {
            checkEncodingAndDecodingBufferTyped<CHAR_t>(counterType, PLYScalar::Type::CHAR);
            checkEncodingAndDecodingBufferTyped<UCHAR_t>(counterType, PLYScalar::Type::UCHAR);
            checkEncodingAndDecodingBufferTyped<SHORT_t>(counterType, PLYScalar::Type::SHORT);
            checkEncodingAndDecodingBufferTyped<USHORT_t>(counterType, PLYScalar::Type::USHORT);
            checkEncodingAndDecodingBufferTyped<INT_t>(counterType, PLYScalar::Type::INT);
            checkEncodingAndDecodingBufferTyped<UINT_t>(counterType, PLYScalar::Type::UINT);
            checkEncodingAndDecodingBufferTyped<FLOAT_t>(counterType, PLYScalar::Type::FLOAT);
            checkEncodingAndDecodingBufferTyped<DOUBLE_t>(counterType, PLYScalar::Type::DOUBLE);
        }
    }
}

//! Test getPropertyTypeIndexByName()
TEST(PLYElementType, GetPropertyTypeIndexByName)
{
    // Setup element and property types
    PLYElementType elementType;
    elementType.m_propertyTypes.resize(2);
    elementType.m_propertyTypes[0].m_name = "abc";
    elementType.m_propertyTypes[1].m_name = "def";

    // Test return value
    ASSERT_EQ(elementType.getPropertyTypeIndexByName("abc"), 0);
    ASSERT_EQ(elementType.getPropertyTypeIndexByName("def"), 1);
    ASSERT_EQ(elementType.getPropertyTypeIndexByName("abcd"), -1);
    ASSERT_EQ(elementType.getPropertyTypeIndexByName("bcd"), -1);
    ASSERT_EQ(elementType.getPropertyTypeIndexByName("x"), -1);
}
