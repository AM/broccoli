﻿/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/ply/PLYFile.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test operators of PLYPropertyValue
TEST(PLYFile, Operators)
{
    PLYFile fileA, fileB;

    // Test equality
    ASSERT_TRUE(fileA == fileB);
    ASSERT_FALSE(fileA != fileB);

    // Test inequality (element type count)
    fileB = fileA;
    fileB.m_elementTypes.resize(1);
    ASSERT_FALSE(fileA == fileB);
    ASSERT_TRUE(fileA != fileB);
}

// Creates a dummy ply data container (with all possible properties)
static inline PLYFile createPLYFile()
{
    // Setup new file
    PLYFile file;
    file.m_elementTypes.resize(2);

    // Define element type with scalar properties
    // ------------------------------------------
    PLYElementType& scalarElementType = file.m_elementTypes[0];
    scalarElementType.m_name = "ElementWithScalarProperties";
    scalarElementType.m_elementCount = 1;
    scalarElementType.m_propertyTypes.resize(static_cast<uint8_t>(PLYScalar::Type::TYPE_COUNT) - 1);
    for (uint8_t i = 0; i < scalarElementType.m_propertyTypes.size(); i++) {
        PLYScalar::Type type = static_cast<PLYScalar::Type>(i + 1);
        scalarElementType.m_propertyTypes[i].m_name = "ScalarProperty_" + PLYScalar::typeToString(type);
        scalarElementType.m_propertyTypes[i].m_isListProperty = false;
        scalarElementType.m_propertyTypes[i].m_valueType = type;
    }

    // Define element type with list properties
    // ----------------------------------------
    PLYElementType& listElementType = file.m_elementTypes[1];
    listElementType.m_name = "ElementWithListProperties";
    listElementType.m_elementCount = 2;
    listElementType.m_propertyTypes.resize(static_cast<uint8_t>(PLYScalar::Type::TYPE_COUNT) - 1);
    for (uint8_t i = 0; i < listElementType.m_propertyTypes.size(); i++) {
        PLYScalar::Type type = static_cast<PLYScalar::Type>(i + 1);
        listElementType.m_propertyTypes[i].m_name = "ListProperty_" + PLYScalar::typeToString(type);
        listElementType.m_propertyTypes[i].m_isListProperty = true;
        listElementType.m_propertyTypes[i].m_counterType = PLYScalar::Type::UCHAR;
        listElementType.m_propertyTypes[i].m_valueType = type;
    }

    // Fill data buffer
    // ----------------
    // Pass through all element types
    for (size_t elementTypeIndex = 0; elementTypeIndex < file.m_elementTypes.size(); elementTypeIndex++) {
        PLYElementType& elementType = file.m_elementTypes[elementTypeIndex];
        elementType.m_data.resize(elementType.m_elementCount);
        // Pass through all elements of this type
        for (size_t elementIndex = 0; elementIndex < elementType.m_elementCount; elementIndex++) {
            elementType.m_data[elementIndex].resize(elementType.m_propertyTypes.size());
            // Pass through all properties of this element
            for (size_t propertyIndex = 0; propertyIndex < elementType.m_propertyTypes.size(); propertyIndex++) {
                PLYPropertyType& propertyType = elementType.m_propertyTypes[propertyIndex];
                if (propertyType.m_isListProperty == false) {
                    // Single scalar property
                    switch (propertyType.m_valueType) {
                    case PLYScalar::Type::CHAR: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((CHAR_t)-127);
                        break;
                    }
                    case PLYScalar::Type::UCHAR: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((UCHAR_t)255);
                        break;
                    }
                    case PLYScalar::Type::SHORT: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((SHORT_t)-32767);
                        break;
                    }
                    case PLYScalar::Type::USHORT: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((USHORT_t)65535);
                        break;
                    }
                    case PLYScalar::Type::INT: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((INT_t)-2147483647);
                        break;
                    }
                    case PLYScalar::Type::UINT: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((UINT_t)4294967295);
                        break;
                    }
                    case PLYScalar::Type::FLOAT: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((FLOAT_t)-1.23);
                        break;
                    }
                    case PLYScalar::Type::DOUBLE: {
                        elementType.m_data[elementIndex][propertyIndex].setScalar((DOUBLE_t)2.34);
                        break;
                    }
                    default: {
                        break;
                    }
                    }
                } else {
                    // List of scalars property
                    switch (propertyType.m_valueType) {
                    case PLYScalar::Type::CHAR: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<CHAR_t>(2, -127));
                        break;
                    }
                    case PLYScalar::Type::UCHAR: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<UCHAR_t>(2, 255));
                        break;
                    }
                    case PLYScalar::Type::SHORT: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<SHORT_t>(2, -32767));
                        break;
                    }
                    case PLYScalar::Type::USHORT: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<USHORT_t>(2, 65535));
                        break;
                    }
                    case PLYScalar::Type::INT: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<INT_t>(2, -2147483647));
                        break;
                    }
                    case PLYScalar::Type::UINT: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<UINT_t>(2, 4294967295));
                        break;
                    }
                    case PLYScalar::Type::FLOAT: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<FLOAT_t>(2, -1.23));
                        break;
                    }
                    case PLYScalar::Type::DOUBLE: {
                        elementType.m_data[elementIndex][propertyIndex].setList(std::vector<DOUBLE_t>(2, 2.34));
                        break;
                    }
                    default: {
                        break;
                    }
                    }
                }
            }
        }
    }

    // Pass back created file
    return file;
}

//! Test encoding and decoding of PLY data structures
TEST(PLYFile, EncodeAndDecode)
{
    // Initialize helpers
    PLYResult result = PLYResult::UNKNOWN;

    // Test encoding
    // -------------
    PLYFile fileToEncode = createPLYFile();
    encoding::CharacterStream encodedStreamAscii;
    encoding::CharacterStream encodedStreamBinaryLittleEndian;
    encoding::CharacterStream encodedStreamBinaryBigEndian;
    encoding::CharacterStream brokenStream;
    ASSERT_TRUE(fileToEncode.encode(encodedStreamAscii, PLYFormat::Type::ASCII, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(fileToEncode.encode(encodedStreamBinaryLittleEndian, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(fileToEncode.encode(encodedStreamBinaryBigEndian, PLYFormat::Type::BINARY_BIG_ENDIAN, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);

    // Test decoding
    // -------------
    PLYFile decodedFileAscii;
    PLYFile decodedFileBinaryLittleEndian;
    PLYFile decodedFileBinaryBigEndian;
    PLYFile brokenDecodedFile;
    ASSERT_TRUE(decodedFileAscii.decode(encodedStreamAscii, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(decodedFileBinaryLittleEndian.decode(encodedStreamBinaryLittleEndian, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(decodedFileBinaryBigEndian.decode(encodedStreamBinaryBigEndian, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);

    // Compare results
    // ---------------
    ASSERT_TRUE(fileToEncode == decodedFileAscii);
    ASSERT_TRUE(fileToEncode == decodedFileBinaryLittleEndian);
    ASSERT_TRUE(fileToEncode == decodedFileBinaryBigEndian);

    // Test error detection
#ifdef NDEBUG
    // Invalid header (empty stream)
    brokenStream.clear();
    ASSERT_FALSE(brokenDecodedFile.decode(brokenStream, &result));
    ASSERT_TRUE(result != PLYResult::SUCCESS);

    // Invalid header (no "end_header\n" found)
    brokenStream.clear();
    encoding::encode(brokenStream, "ply\nformat ascii 1.0\n");
    ASSERT_FALSE(brokenDecodedFile.decode(brokenStream, &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_END);

    // Invalid header (no "ply" found)
    brokenStream.clear();
    encoding::encode(brokenStream, "pl\nformat ascii 1.0\nend_header\n");
    ASSERT_FALSE(brokenDecodedFile.decode(brokenStream, &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PLY);

    // Invalid header (no "format" found)
    brokenStream.clear();
    encoding::encode(brokenStream, "ply\nforma ascii 1.0\nend_header\n");
    ASSERT_FALSE(brokenDecodedFile.decode(brokenStream, &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_FORMAT);

    // Invalid header (invalid element)
    brokenStream.clear();
    encoding::encode(brokenStream, "ply\nformat ascii 1.0\nelement vertex\nend_header\n");
    ASSERT_FALSE(brokenDecodedFile.decode(brokenStream, &result));
    ASSERT_TRUE(result != PLYResult::SUCCESS);

    // Invalid buffer
    brokenStream.clear();
    encoding::encode(brokenStream, "ply\nformat ascii 1.0\nelement vertex 1\nproperty float x\nend_header\n");
    ASSERT_FALSE(brokenDecodedFile.decode(brokenStream, &result));
    ASSERT_TRUE(result != PLYResult::SUCCESS);
#endif
}

TEST(PLYFile, WriteAndRead)
{
    // Initialize helpers
    PLYResult result = PLYResult::UNKNOWN;

    // Test writing
    // ------------
    PLYFile fileToWrite = createPLYFile();
    ASSERT_TRUE(fileToWrite.writeFile("PLYTestAscii.ply", PLYFormat::Type::ASCII, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(fileToWrite.writeFile("PLYTestAscii.ply.gz", PLYFormat::Type::ASCII, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(fileToWrite.writeFile("PLYTestBinaryLittleEndian.ply", PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(fileToWrite.writeFile("PLYTestBinaryLittleEndian.ply.gz", PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(fileToWrite.writeFile("PLYTestBinaryBigEndian.ply", PLYFormat::Type::BINARY_BIG_ENDIAN, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(fileToWrite.writeFile("PLYTestBinaryBigEndian.ply.gz", PLYFormat::Type::BINARY_BIG_ENDIAN, &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);

    // Test reading
    // ------------
    PLYFile readFileAscii, readFileAsciiCompressed;
    PLYFile readFileBinaryLittleEndian, readFileBinaryLittleEndianCompressed;
    PLYFile readFileBinaryBigEndian, readFileBinaryBigEndianCompressed;
    ASSERT_TRUE(readFileAscii.readFile("PLYTestAscii.ply", &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(readFileAsciiCompressed.readFile("PLYTestAscii.ply.gz", &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(readFileBinaryLittleEndian.readFile("PLYTestBinaryLittleEndian.ply", &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(readFileBinaryLittleEndianCompressed.readFile("PLYTestBinaryLittleEndian.ply.gz", &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(readFileBinaryBigEndian.readFile("PLYTestBinaryBigEndian.ply", &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(readFileBinaryBigEndianCompressed.readFile("PLYTestBinaryBigEndian.ply", &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);

    // Compare results
    // ---------------
    ASSERT_TRUE(readFileAscii == fileToWrite);
    ASSERT_TRUE(readFileAsciiCompressed == fileToWrite);
    ASSERT_TRUE(readFileBinaryLittleEndian == fileToWrite);
    ASSERT_TRUE(readFileBinaryLittleEndianCompressed == fileToWrite);
    ASSERT_TRUE(readFileBinaryBigEndian == fileToWrite);
    ASSERT_TRUE(readFileBinaryBigEndianCompressed == fileToWrite);

    // Test error detection
#ifdef NDEBUG
    // File without buffer
    PLYFile fileWithoutBuffer = fileToWrite;
    for (size_t i = 0; i < fileWithoutBuffer.m_elementTypes.size(); i++)
        fileWithoutBuffer.m_elementTypes[i].m_data.clear();
    ASSERT_FALSE(fileWithoutBuffer.writeFile("PLYTestAsciiERROR.ply", PLYFormat::Type::ASCII, &result));
    ASSERT_TRUE(result != PLYResult::SUCCESS);

    // Non-existent file
    PLYFile nonexistentInputFile;
    ASSERT_FALSE(nonexistentInputFile.readFile("PLYTestAsciiNONExistent.ply", &result));
    ASSERT_EQ(result, PLYResult::ERROR_FILE_NOEXIST);
#endif
}

//! Helper class to make protected and private methods available for unit tests
class PLYFileTester : public PLYFile {
public:
    PLYFileTester(const PLYFile& file)
        : PLYFile(file)
    {
    }

    encoding::CharacterStreamSize streamSizeUpperBound(const PLYFormat::Type& format) const { return PLYFile::streamSizeUpperBound(format); }
};

//! Test of streamSizeUpperBound()
TEST(PLYFile, StreamSizeUpperBound)
{
    // Initialize helpers
    PLYFileTester testFile(createPLYFile());
    encoding::CharacterStream encodedStreamAscii;
    encoding::CharacterStream encodedStreamBinaryLittleEndian;
    encoding::CharacterStream encodedStreamBinaryBigEndian;

    // Encode file
    testFile.encode(encodedStreamAscii, PLYFormat::Type::ASCII);
    testFile.encode(encodedStreamBinaryLittleEndian, PLYFormat::Type::BINARY_LITTLE_ENDIAN);
    testFile.encode(encodedStreamBinaryBigEndian, PLYFormat::Type::BINARY_BIG_ENDIAN);

    // Check, if estimate is a true upper bound
    const auto estimatedStreamSizeAscii = testFile.streamSizeUpperBound(PLYFormat::Type::ASCII);
    const auto estimatedStreamSizeBinaryLittleEndian = testFile.streamSizeUpperBound(PLYFormat::Type::BINARY_LITTLE_ENDIAN);
    const auto estimatedStreamSizeBinaryBigEndian = testFile.streamSizeUpperBound(PLYFormat::Type::BINARY_BIG_ENDIAN);
    ASSERT_GE(estimatedStreamSizeAscii, encodedStreamAscii.size());
    ASSERT_EQ(estimatedStreamSizeBinaryLittleEndian, encodedStreamBinaryLittleEndian.size());
    ASSERT_EQ(estimatedStreamSizeBinaryBigEndian, encodedStreamBinaryBigEndian.size());
}

//! Test getElementTypeIndexByName()
TEST(PLYFile, GetElementTypeIndexByName)
{
    // Setup element and property types
    PLYFile file;
    file.m_elementTypes.resize(2);
    file.m_elementTypes[0].m_name = "abc";
    file.m_elementTypes[1].m_name = "def";

    // Test return value
    ASSERT_EQ(file.getElementTypeIndexByName("abc"), 0);
    ASSERT_EQ(file.getElementTypeIndexByName("def"), 1);
    ASSERT_EQ(file.getElementTypeIndexByName("abcd"), -1);
    ASSERT_EQ(file.getElementTypeIndexByName("bcd"), -1);
    ASSERT_EQ(file.getElementTypeIndexByName("x"), -1);
}
