/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/ply/PLYPropertyType.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test operators of PLYPropertyValue
TEST(PLYPropertyType, Operators)
{
    PLYPropertyType typeA, typeB;

    // Test equality
    ASSERT_TRUE(typeA == typeB);
    ASSERT_FALSE(typeA != typeB);

    // Test inequality (name)
    typeB = typeA;
    typeB.m_name = "a";
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);

    // Test inequality (isListProperty)
    typeB = typeA;
    typeB.m_isListProperty = !typeB.m_isListProperty;
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);

    // Test inequality (counterType)
    typeB = typeA;
    typeB.m_counterType = PLYScalar::Type::DOUBLE;
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);

    // Test inequality (valueType)
    typeB = typeA;
    typeB.m_valueType = PLYScalar::Type::DOUBLE;
    ASSERT_FALSE(typeA == typeB);
    ASSERT_TRUE(typeA != typeB);
}

//! Test de- and encoding of header
TEST(PLYPropertyType, EncodingAndDecodingHeader)
{
    // Initialize helpers
    PLYResult result = PLYResult::UNKNOWN;
    encoding::CharacterStream stream;
    PLYPropertyType inputType, outputType;
    inputType.m_name = "abc";
    inputType.m_counterType = PLYScalar::Type::UCHAR;
    inputType.m_valueType = PLYScalar::Type::INT;

    // Test encoding and decoding of scalar property headers
    inputType.m_isListProperty = false;
    stream.clear();
    inputType.encodeHeader(stream);
    outputType = PLYPropertyType();
    ASSERT_TRUE(outputType.decodeHeader(std::string(stream.begin(), stream.end()), &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputType == inputType);

    // Test encoding and decoding of list property headers
    inputType.m_isListProperty = true;
    stream.clear();
    inputType.encodeHeader(stream);
    outputType = PLYPropertyType();
    ASSERT_TRUE(outputType.decodeHeader(std::string(stream.begin(), stream.end()), &result));
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputType == inputType);

    // Test error checking
#ifdef NDEBUG
    ASSERT_FALSE(outputType.decodeHeader("", &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
    ASSERT_FALSE(outputType.decodeHeader("property float", &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
    ASSERT_FALSE(outputType.decodeHeader("propert float x", &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
    ASSERT_FALSE(outputType.decodeHeader("property abc x", &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
    ASSERT_FALSE(outputType.decodeHeader("property list uchar float", &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
    ASSERT_FALSE(outputType.decodeHeader("property list abc float x", &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
    ASSERT_FALSE(outputType.decodeHeader("property list uchar abc x", &result));
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_HEADER_PROPERTYTYPE);
#endif
}
