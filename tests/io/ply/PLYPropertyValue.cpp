/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/ply/PLYPropertyValue.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test operators of PLYPropertyValue
TEST(PLYPropertyValue, Operators)
{
    // Initialize helpers
    PLYPropertyValue valueA, valueB;
    valueA.setScalar((uint8_t)1);
    valueB.setScalar((uint8_t)2);

    // Inequality
    ASSERT_TRUE(valueA != valueB);
    ASSERT_FALSE(valueA == valueB);

    // Equality
    PLYPropertyValue valueC = valueA;
    ASSERT_TRUE(valueA == valueC);
    ASSERT_FALSE(valueA != valueC);
}

//! Helper for testing setters and getters for different property types
template <typename ValueType>
void testSettersAndGetters(const ValueType& setValue)
{
    // Initialize helpers
    PLYPropertyValue value;
    ValueType getValue = 0;

    // Test setter and getter (scalar)
    value.setScalar(setValue);
    ASSERT_TRUE(value.scalar(getValue));
    ASSERT_EQ(setValue, getValue);

    // Test setter and getter (list)
    for (size_t i = 0; i < 3; i++) {
        // Set list
        std::vector<ValueType> setList(i, setValue);
        value.setList(setList);
        std::vector<ValueType> list(i + 1, 0);
        ASSERT_TRUE(value.list(list));
        ASSERT_TRUE(setList == list);
    }
}

//! Test setters and getters of PLYPropertyValue
TEST(PLYPropertyValue, SettersAndGetters)
{
    // Initialize helpers
    PLYPropertyValue value;

    // Set single CHAR
    testSettersAndGetters((CHAR_t)-127);

    // Set single UCHAR
    testSettersAndGetters((UCHAR_t)255);

    // Set single SHORT
    testSettersAndGetters((SHORT_t)-32767);

    // Set single USHORT
    testSettersAndGetters((USHORT_t)65535);

    // Set single INT
    testSettersAndGetters((INT_t)-2147483647);

    // Set single UINT
    testSettersAndGetters((UINT_t)4294967295);

    // Set single FLOAT
    testSettersAndGetters((FLOAT_t)-1.23);

    // Set single DOUBLE
    testSettersAndGetters((DOUBLE_t)2.34);

    // Test invalid type
#ifdef NDEBUG
    value.setScalar((UCHAR_t)0);
    USHORT_t getInvalidScalar = 0;
    ASSERT_FALSE(value.scalar(getInvalidScalar));
#endif

    // Run for different list lengths
    for (size_t i = 0; i < 3; i++) {
        // Test invalid type
#ifdef NDEBUG
        if (i > 0) {
            value.setList(std::vector<UCHAR_t>(3, 0));
            std::vector<USHORT_t> getInvalidList;
            ASSERT_FALSE(value.list(getInvalidList));
        }
#endif
    }
}

//! Test getters (with automatic conversion) of PLYPropertyValue
TEST(PLYPropertyValue, GettersConverted)
{
    // Initialize helpers
    PLYPropertyValue value;
    double outputValue = 0;
    double expectedOutputValue = 1;
    std::vector<double> outputValues{ 0, 0 };
    std::vector<double> expectedOutputValues{ 1, 2 };

    // Test (CHAR)
    value.setScalar((CHAR_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::CHAR, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<CHAR_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::CHAR, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test (UCHAR)
    value.setScalar((UCHAR_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::UCHAR, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<UCHAR_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::UCHAR, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test (SHORT)
    value.setScalar((SHORT_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::SHORT, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<SHORT_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::SHORT, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test (USHORT)
    value.setScalar((USHORT_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::USHORT, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<USHORT_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::USHORT, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test (INT)
    value.setScalar((INT_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::INT, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<INT_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::INT, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test (UINT)
    value.setScalar((UINT_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::UINT, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<UINT_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::UINT, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test (FLOAT)
    value.setScalar((FLOAT_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::FLOAT, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<FLOAT_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::FLOAT, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test (DOUBLE)
    value.setScalar((DOUBLE_t)1);
    ASSERT_TRUE(value.scalarConverted(PLYScalar::Type::DOUBLE, outputValue));
    ASSERT_EQ(outputValue, expectedOutputValue);
    value.setList(std::vector<DOUBLE_t>{ 1, 2 });
    ASSERT_TRUE(value.listConverted(PLYScalar::Type::DOUBLE, outputValues));
    ASSERT_EQ(outputValues, expectedOutputValues);

    // Test invalid type
#ifdef NDEBUG
    ASSERT_FALSE(value.scalarConverted(PLYScalar::Type::UNKNOWN, outputValue));
    ASSERT_FALSE(value.listConverted(PLYScalar::Type::UNKNOWN, outputValues));
#endif
}

//! Helper function to test encoding and decoding of scalar properties
template <typename ValueType>
void testEncodeAndDecodeScalar(const ValueType& setValue, const std::vector<uint8_t>& expectedStreamAscii, const std::vector<uint8_t>& expectedStreamBinaryLittleEndian, const std::vector<uint8_t>& expectedStreamBinaryBigEndian)
{
    // Initialize helpers
    PLYResult result = PLYResult::UNKNOWN;
    encoding::CharacterStream stream, emptyStream;
    encoding::CharacterStream invalidStream{ ' ', ' ' };
    encoding::CharacterStream brokenStream{ 'x', ' ' };
    PLYPropertyValue inputValue, outputValue, emptyValue;
    ValueType getValue = 0;

    // Set input value
    inputValue.setScalar(setValue);
    stream.clear();

    // Test ASCII encoding/decoding
    stream.clear();
    ASSERT_EQ(inputValue.encodeScalar<ValueType>(stream, PLYFormat::Type::ASCII, &result), expectedStreamAscii.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_EQ(stream, expectedStreamAscii);
    encoding::encode(stream, ' ');
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(stream, 0, PLYFormat::Type::ASCII, &result), expectedStreamAscii.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputValue.scalar(getValue));
    ASSERT_EQ(getValue, setValue);

    // Test binary little endian encoding/decoding
    stream.clear();
    ASSERT_EQ(inputValue.encodeScalar<ValueType>(stream, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result), expectedStreamBinaryLittleEndian.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_EQ(stream, expectedStreamBinaryLittleEndian);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(stream, 0, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result), expectedStreamBinaryLittleEndian.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputValue.scalar(getValue));
    ASSERT_EQ(getValue, setValue);

    // Test binary big endian encoding/decoding
    stream.clear();
    ASSERT_EQ(inputValue.encodeScalar<ValueType>(stream, PLYFormat::Type::BINARY_BIG_ENDIAN, &result), expectedStreamBinaryBigEndian.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_EQ(stream, expectedStreamBinaryBigEndian);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(stream, 0, PLYFormat::Type::BINARY_BIG_ENDIAN, &result), expectedStreamBinaryBigEndian.size());
    ASSERT_EQ(result, PLYResult::SUCCESS);
    ASSERT_TRUE(outputValue.scalar(getValue));
    ASSERT_EQ(getValue, setValue);

    // Test error detection
#ifdef NDEBUG
    ASSERT_EQ(inputValue.encodeScalar<ValueType>(stream, PLYFormat::Type::UNKNOWN, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_FORMAT);
    ASSERT_EQ(emptyValue.encodeScalar<ValueType>(stream, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_PROPERTYVALUE);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(stream, 0, PLYFormat::Type::UNKNOWN, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_FORMAT);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(emptyStream, 0, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_SCALAR);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(emptyStream, 0, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_SCALAR);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(emptyStream, 0, PLYFormat::Type::BINARY_BIG_ENDIAN, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_SCALAR);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(brokenStream, 0, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_SCALAR);
    ASSERT_EQ(outputValue.decodeScalar<ValueType>(invalidStream, 0, PLYFormat::Type::ASCII, &result), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_SCALAR);
#endif
}

//! Helper for testing encoding and decoding of list properties (one counter type)
template <typename CounterType, typename ValueType>
void testEncodeAndDecodeListTyped()
{
    // Initialize helpers
    PLYResult result = PLYResult::UNKNOWN;
    encoding::CharacterStream stream, emptyStream;
    PLYPropertyValue inputValue, outputValue, brokenValue;
    brokenValue.setScalar((uint8_t)0);
    encoding::CharacterStreamSize inputStreamSize, outputStreamSize;

    // Run for different list lengths
    for (size_t i = 0; i <= 3; i++) {
        // Initialize list
        std::vector<ValueType> setList(i, 123);
        std::vector<ValueType> list(i + 1, 0);
        inputValue.setList(setList);

        // Test ASCII encoding/decoding
        stream.clear();
        inputStreamSize = inputValue.encodeList<CounterType, ValueType>(stream, PLYFormat::Type::ASCII, &result);
        ASSERT_EQ(inputStreamSize, stream.size());
        ASSERT_EQ(result, PLYResult::SUCCESS);
        outputStreamSize = outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::ASCII, &result);
        ASSERT_EQ(outputStreamSize, stream.size());
        ASSERT_EQ(result, PLYResult::SUCCESS);
        ASSERT_TRUE(outputValue.list(list));
        ASSERT_EQ(list, setList);

        // Test binary little endian encoding/decoding
        stream.clear();
        inputStreamSize = inputValue.encodeList<CounterType, ValueType>(stream, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result);
        ASSERT_EQ(inputStreamSize, stream.size());
        ASSERT_EQ(result, PLYResult::SUCCESS);
        outputStreamSize = outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result);
        ASSERT_EQ(outputStreamSize, stream.size());
        ASSERT_EQ(result, PLYResult::SUCCESS);
        ASSERT_TRUE(outputValue.list(list));
        ASSERT_EQ(list, setList);

        // Test binary big endian encoding/decoding
        stream.clear();
        inputStreamSize = inputValue.encodeList<CounterType, ValueType>(stream, PLYFormat::Type::BINARY_BIG_ENDIAN, &result);
        ASSERT_EQ(inputStreamSize, stream.size());
        ASSERT_EQ(result, PLYResult::SUCCESS);
        outputStreamSize = outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::BINARY_BIG_ENDIAN, &result);
        ASSERT_EQ(outputStreamSize, stream.size());
        ASSERT_EQ(result, PLYResult::SUCCESS);
        ASSERT_TRUE(outputValue.list(list));
        ASSERT_EQ(list, setList);
    }

#ifdef NDEBUG
    // Test error detection in encoding
    stream.clear();
    if (sizeof(ValueType) > 1) {
        ASSERT_EQ((brokenValue.encodeList<CounterType, ValueType>(stream, PLYFormat::Type::ASCII, &result)), 0);
        ASSERT_EQ(result, PLYResult::ERROR_INVALID_BUFFER_PROPERTYVALUE);
    }
    stream.clear();
    ASSERT_EQ((inputValue.encodeList<CounterType, ValueType>(stream, PLYFormat::Type::UNKNOWN, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_FORMAT);

    // Test error detection in decoding
    stream.clear();
    ASSERT_EQ((outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::ASCII, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    encoding::encode(stream, "3    ");
    ASSERT_EQ((outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::ASCII, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    encoding::encode(stream, "a 1 2 3");
    ASSERT_EQ((outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::ASCII, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    encoding::encode(stream, "3 a 2 3");
    ASSERT_EQ((outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::ASCII, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    encoding::encode(stream, "-1 1 2 3");
    ASSERT_EQ((outputValue.decodeList<int8_t, ValueType>(stream, 0, PLYFormat::Type::ASCII, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    encoding::encode(stream, "  ");
    ASSERT_EQ((outputValue.decodeList<int8_t, ValueType>(stream, 0, PLYFormat::Type::ASCII, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    ASSERT_EQ((outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    serialization::serialize(stream, serialization::Endianness::LITTLE, (CounterType)1);
    ASSERT_EQ((outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_DECODE_LIST);
    stream.clear();
    serialization::serialize(stream, serialization::Endianness::LITTLE, (CounterType)0);
    ASSERT_EQ((outputValue.decodeList<CounterType, ValueType>(stream, 0, PLYFormat::Type::UNKNOWN, &result)), 0);
    ASSERT_EQ(result, PLYResult::ERROR_INVALID_FORMAT);
#endif
}

//! Helper for testing encoding and decoding of list properties (different counter types)
template <typename ValueType>
void testEncodeAndDecodeList()
{
    testEncodeAndDecodeListTyped<UCHAR_t, ValueType>();
    testEncodeAndDecodeListTyped<USHORT_t, ValueType>();
    testEncodeAndDecodeListTyped<UINT_t, ValueType>();
}

//! Test encoding and decoding of property values
TEST(PLYPropertyValue, EncodeAndDecode)
{
    // Test CHAR
    testEncodeAndDecodeScalar((CHAR_t)-123, (std::vector<uint8_t>{ '-', '1', '2', '3' }), (std::vector<uint8_t>{ 0x80 | 0x7B }), (std::vector<uint8_t>{ 0x80 | 0x7B }));
    testEncodeAndDecodeList<CHAR_t>();

    // Test UCHAR
    testEncodeAndDecodeScalar((UCHAR_t)123, (std::vector<uint8_t>{ '1', '2', '3' }), (std::vector<uint8_t>{ 0x7B }), (std::vector<uint8_t>{ 0x7B }));
    testEncodeAndDecodeList<UCHAR_t>();

    // Test SHORT
    testEncodeAndDecodeScalar((SHORT_t)-1234, (std::vector<uint8_t>{ '-', '1', '2', '3', '4' }), (std::vector<uint8_t>{ 0xD2, 0x80 | 0x04 }), (std::vector<uint8_t>{ 0x80 | 0x04, 0xD2 }));
    testEncodeAndDecodeList<SHORT_t>();

    // Test USHORT
    testEncodeAndDecodeScalar((USHORT_t)1234, (std::vector<uint8_t>{ '1', '2', '3', '4' }), (std::vector<uint8_t>{ 0xD2, 0x04 }), (std::vector<uint8_t>{ 0x04, 0xD2 }));
    testEncodeAndDecodeList<USHORT_t>();

    // Test INT
    testEncodeAndDecodeScalar((INT_t)-1000000, (std::vector<uint8_t>{ '-', '1', '0', '0', '0', '0', '0', '0' }), (std::vector<uint8_t>{ 0x40, 0x42, 0x0F, 0x80 | 0x00 }), (std::vector<uint8_t>{ 0x80 | 0x00, 0x0F, 0x42, 0x40 }));
    testEncodeAndDecodeList<INT_t>();

    // Test UINT
    testEncodeAndDecodeScalar((UINT_t)1000000, (std::vector<uint8_t>{ '1', '0', '0', '0', '0', '0', '0' }), (std::vector<uint8_t>{ 0x40, 0x42, 0x0F, 0x00 }), (std::vector<uint8_t>{ 0x00, 0x0F, 0x42, 0x40 }));
    testEncodeAndDecodeList<UINT_t>();

    // Test FLOAT 0xBF9D70A4
    testEncodeAndDecodeScalar((FLOAT_t)-1.23, (std::vector<uint8_t>{ '-', '1', '.', '2', '3', '0', '0', '0', '0', '0', 'e', '+', '0', '0' }), (std::vector<uint8_t>{ 0xA4, 0x70, 0x9D, 0xBF }), (std::vector<uint8_t>{ 0xBF, 0x9D, 0x70, 0xA4 }));
    testEncodeAndDecodeList<FLOAT_t>();

    // Test DOUBLE 0x4002B851EB851EB8
    testEncodeAndDecodeScalar((DOUBLE_t)2.34, (std::vector<uint8_t>{ '+', '2', '.', '3', '3', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', 'e', '+', '0', '0' }), (std::vector<uint8_t>{ 0xB8, 0x1E, 0x85, 0xEB, 0x51, 0xB8, 0x02, 0x40 }), (std::vector<uint8_t>{ 0x40, 0x02, 0xB8, 0x51, 0xEB, 0x85, 0x1E, 0xB8 }));
    testEncodeAndDecodeList<DOUBLE_t>();
}
