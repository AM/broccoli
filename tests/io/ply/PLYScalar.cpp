/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/ply/PLYScalar.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test converstion TO string representation of scalar types
TEST(PLYScalar, TypeToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PLYScalar::Type::TYPE_COUNT); i++) {
        ASSERT_GT(PLYScalar::typeToString(static_cast<PLYScalar::Type>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(PLYScalar::typeToString(static_cast<PLYScalar::Type>(i)) == "unknown");
        } else {
            ASSERT_TRUE(PLYScalar::typeToString(static_cast<PLYScalar::Type>(i)) != "unknown");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(PLYScalar::typeToString(PLYScalar::Type::TYPE_COUNT) == "unknown");
#endif
}

//! Test converstion FROM string representation of scalar types
TEST(PLYScalar, TypeFromString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PLYScalar::Type::TYPE_COUNT); i++) {
        const std::string stringRepresentation = PLYScalar::typeToString(static_cast<PLYScalar::Type>(i));
        const PLYScalar::Type typeRepresentation = PLYScalar::typeFromString(stringRepresentation);
        ASSERT_TRUE(typeRepresentation == static_cast<PLYScalar::Type>(i));
    }
#ifdef NDEBUG
    ASSERT_TRUE(PLYScalar::typeFromString("") == PLYScalar::Type::UNKNOWN);
#endif
}

//! Test evaluation of byte count for various scalar types
TEST(PLYScalar, ByteCount)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PLYScalar::Type::TYPE_COUNT); i++) {
        if (static_cast<PLYScalar::Type>(i) != PLYScalar::Type::UNKNOWN) {
            ASSERT_TRUE(PLYScalar::byteCount(static_cast<PLYScalar::Type>(i)) > 0);
        }
#ifdef NDEBUG
        else {
            ASSERT_TRUE(PLYScalar::byteCount(static_cast<PLYScalar::Type>(i)) == 0);
        }
#endif
    }
#ifdef NDEBUG
    ASSERT_TRUE(PLYScalar::byteCount(PLYScalar::Type::TYPE_COUNT) == 0);
#endif
}

//! Test type definitions among their byte count
TEST(PLYScalar, ByteCountTypes)
{
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::CHAR), sizeof(CHAR_t));
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::UCHAR), sizeof(UCHAR_t));
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::SHORT), sizeof(SHORT_t));
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::USHORT), sizeof(USHORT_t));
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::INT), sizeof(INT_t));
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::UINT), sizeof(UINT_t));
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::FLOAT), sizeof(FLOAT_t));
    ASSERT_EQ(PLYScalar::byteCount(PLYScalar::Type::DOUBLE), sizeof(DOUBLE_t));
}
