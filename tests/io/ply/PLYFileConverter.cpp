﻿/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/ply/PLYFileConverter.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;
using namespace geometry;

//! Helper function to create a dummy CG mesh for testing conversion to/from PLY data structure
CGMesh createTestCGMesh(const size_t& trianglecount, const bool& withPerVertexNormals, const bool& withPerVertexColors, const bool& withMaterialIndices)
{
    // Empty mesh
    if (trianglecount == 0)
        return CGMesh();

    // Initialize helpers
    const size_t vertexCount = trianglecount + 1;

    // Setup mesh
    CGMesh mesh;
    mesh.m_vertexBuffer.resize(Eigen::NoChange, vertexCount);
    if (withPerVertexNormals == true)
        mesh.m_normalBuffer.resize(Eigen::NoChange, vertexCount);
    if (withPerVertexColors == true)
        mesh.m_colorBuffer.resize(Eigen::NoChange, vertexCount);
    if (withMaterialIndices == true)
        mesh.m_indexBuffer.resize(4, trianglecount);
    else
        mesh.m_indexBuffer.resize(3, trianglecount);

    // Create buffers
    const double radius = 0.5;
    for (size_t i = 0; i < vertexCount; i++) {
        const double angle = ((int)i - 1) * 2.0 * M_PI / trianglecount;

        // Compute vertex position
        mesh.m_vertexBuffer.col(i).setZero();
        if (i > 0) {
            mesh.m_vertexBuffer(0, i) = radius * cos(angle);
            mesh.m_vertexBuffer(1, i) = radius * sin(angle);
        }

        // Compute normals
        if (withPerVertexNormals == true) {
            mesh.m_normalBuffer(0, i) = 0;
            mesh.m_normalBuffer(1, i) = 0;
            mesh.m_normalBuffer(2, i) = 1;
        }

        // Compute color
        if (withPerVertexColors == true) {
            double red = 0.0;
            double green = 0.0;
            double blue = 0.0;
            double alpha = 1.0;
            if (i > 0) {
                double distanceRed = fabs(angle - 0);
                while (distanceRed > M_PI)
                    distanceRed = fabs(2.0 * M_PI - distanceRed);
                red = 1.0 - distanceRed / M_PI;
                double distanceGreen = fabs(angle - 2.0 / 3.0 * M_PI);
                while (distanceGreen > M_PI)
                    distanceGreen = fabs(2.0 * M_PI - distanceGreen);
                green = 1.0 - distanceGreen / M_PI;
                double distanceBlue = fabs(angle - 4.0 / 3.0 * M_PI);
                while (distanceBlue > M_PI)
                    distanceBlue = fabs(2.0 * M_PI - distanceBlue);
                blue = 1.0 - distanceBlue / M_PI;
            }
            mesh.m_colorBuffer(0, i) = (int)(red * 255.0);
            mesh.m_colorBuffer(1, i) = (int)(green * 255.0);
            mesh.m_colorBuffer(2, i) = (int)(blue * 255.0);
            mesh.m_colorBuffer(3, i) = (int)(alpha * 255.0);
        }
    }

    // Compute index buffer
    for (size_t i = 0; i < trianglecount; i++) {
        mesh.m_indexBuffer(0, i) = 0;
        mesh.m_indexBuffer(1, i) = i + 1;
        mesh.m_indexBuffer(2, i) = i + 2;
        if (i + 1 == trianglecount)
            mesh.m_indexBuffer(2, i) = 1;
        if (withMaterialIndices == true)
            mesh.m_indexBuffer(3, i) = 0;
    }

    // Compute material buffer
    if (withMaterialIndices == true) {
        mesh.m_materialBuffer.resize(2);

        // Set first material
        mesh.m_materialBuffer[0].m_name = "";
        mesh.m_materialBuffer[0].m_ambientColor = { 1, 2, 3 };
        mesh.m_materialBuffer[0].m_ambientColorFromVertexColor = false;
        mesh.m_materialBuffer[0].m_ambientIntensity = 0.123;
        mesh.m_materialBuffer[0].m_diffuseColor = { 4, 5, 6 };
        mesh.m_materialBuffer[0].m_diffuseColorFromVertexColor = false;
        mesh.m_materialBuffer[0].m_diffuseIntensity = 0.456;
        mesh.m_materialBuffer[0].m_specularColor = { 7, 8, 9 };
        mesh.m_materialBuffer[0].m_specularColorFromVertexColor = false;
        mesh.m_materialBuffer[0].m_specularIntensity = 0.789;
        mesh.m_materialBuffer[0].m_shininess = 0.147;
        mesh.m_materialBuffer[0].m_alpha = 32;
        mesh.m_materialBuffer[0].m_alphaFromVertexColor = false;
        mesh.m_materialBuffer[0].m_shadeless = false;

        // Set second material
        mesh.m_materialBuffer[1].m_name = "";
        mesh.m_materialBuffer[1].m_ambientColor = { 11, 12, 13 };
        mesh.m_materialBuffer[1].m_ambientColorFromVertexColor = true;
        mesh.m_materialBuffer[1].m_ambientIntensity = 0.321;
        mesh.m_materialBuffer[1].m_diffuseColor = { 14, 15, 16 };
        mesh.m_materialBuffer[1].m_diffuseColorFromVertexColor = true;
        mesh.m_materialBuffer[1].m_diffuseIntensity = 0.654;
        mesh.m_materialBuffer[1].m_specularColor = { 17, 18, 19 };
        mesh.m_materialBuffer[1].m_specularColorFromVertexColor = true;
        mesh.m_materialBuffer[1].m_specularIntensity = 0.987;
        mesh.m_materialBuffer[1].m_shininess = 0.741;
        mesh.m_materialBuffer[1].m_alpha = 64;
        mesh.m_materialBuffer[1].m_alphaFromVertexColor = true;
        mesh.m_materialBuffer[1].m_shadeless = true;
    }

    // Pass back generated mesh
    return mesh;
}

//! Helper function to test conversion to/from CGMesh with different options
void checkConvertToFromCGMesh(const size_t& trianglecount, const bool& withPerVertexNormals, const bool& withPerVertexColors, const bool& withMaterialIndices)
{
    // Initialize helpers
    PLYResult plyResult = PLYResult::UNKNOWN;
    CGMeshResult meshResult = CGMeshResult::UNKNOWN;
    CGMesh inputMesh = createTestCGMesh(trianglecount, withPerVertexNormals, withPerVertexColors, withMaterialIndices);
    CGMesh outputMesh;
    PLYFile file;
    PLYFile brokenFile;

    // Test convert from
    ASSERT_TRUE(PLYFileConverter::convert(inputMesh, file, &plyResult, &meshResult));
    ASSERT_EQ(plyResult, PLYResult::SUCCESS);
    ASSERT_EQ(meshResult, CGMeshResult::SUCCESS);

    // Test convert to
    ASSERT_TRUE(PLYFileConverter::convert(file, outputMesh, &plyResult));
    ASSERT_EQ(plyResult, PLYResult::SUCCESS);

    // Compare vertex buffer
    ASSERT_TRUE(outputMesh.m_vertexBuffer.rows() == inputMesh.m_vertexBuffer.rows());
    ASSERT_TRUE(outputMesh.m_vertexBuffer.cols() == inputMesh.m_vertexBuffer.cols());
    ASSERT_LE((outputMesh.m_vertexBuffer - inputMesh.m_vertexBuffer).norm(), 1e-6); // PLY format has only float precision

    // Compate normal buffer
    ASSERT_TRUE(outputMesh.m_normalBuffer.rows() == inputMesh.m_normalBuffer.rows());
    ASSERT_TRUE(outputMesh.m_normalBuffer.cols() == inputMesh.m_normalBuffer.cols());
    ASSERT_LE((outputMesh.m_normalBuffer - inputMesh.m_normalBuffer).norm(), 1e-6); // PLY format has only float precision

    // Compare color buffer
    ASSERT_TRUE(outputMesh.m_colorBuffer.rows() == inputMesh.m_colorBuffer.rows());
    ASSERT_TRUE(outputMesh.m_colorBuffer.cols() == inputMesh.m_colorBuffer.cols());
    ASSERT_TRUE(outputMesh.m_colorBuffer.isApprox(inputMesh.m_colorBuffer)); // Colors have to be exact

    // Compare material buffer
    ASSERT_TRUE(outputMesh.m_materialBuffer == inputMesh.m_materialBuffer); // Materials have to be exact

    // Compare index buffer
    ASSERT_TRUE(outputMesh.m_indexBuffer.rows() == inputMesh.m_indexBuffer.rows());
    ASSERT_TRUE(outputMesh.m_indexBuffer.cols() == inputMesh.m_indexBuffer.cols());
    ASSERT_TRUE(outputMesh.m_indexBuffer.isApprox(inputMesh.m_indexBuffer)); // Index buffer has to be exact

    // Test error detection
#ifdef NDEBUG
    if (trianglecount > 0) {
        // Invalid mesh
        inputMesh.m_vertexBuffer.resize(Eigen::NoChange, 0);
        ASSERT_FALSE(PLYFileConverter::convert(inputMesh, brokenFile, &plyResult, &meshResult));
        ASSERT_EQ(plyResult, PLYResult::ERROR_MESH_INVALID);
        ASSERT_NE(meshResult, CGMeshResult::SUCCESS);

        // Invalid buffer
        for (size_t i = 0; i < file.m_elementTypes.size(); i++) {
            brokenFile = file;
            brokenFile.m_elementTypes[i].m_data.clear();
            ASSERT_FALSE(PLYFileConverter::convert(brokenFile, outputMesh, &plyResult));
            ASSERT_EQ(plyResult, PLYResult::ERROR_INVALID_BUFFER_ELEMENTCOUNT);
        }

        // Test missing "vertex" properties
        brokenFile = file;
        int64_t idx_vertex_x = brokenFile.m_elementTypes[0].getPropertyTypeIndexByName("x");
        ASSERT_GE(idx_vertex_x, 0);
        brokenFile.m_elementTypes[0].m_propertyTypes[idx_vertex_x].m_name = "DELETED";
        ASSERT_FALSE(PLYFileConverter::convert(brokenFile, outputMesh, &plyResult));
        ASSERT_EQ(plyResult, PLYResult::ERROR_INVALID_VERTEX_ELEMENT_TYPE);

        // Test missing "face" properties
        brokenFile = file;
        int64_t idx_face_vertex_indices = brokenFile.m_elementTypes[1].getPropertyTypeIndexByName("vertex_indices");
        ASSERT_GE(idx_face_vertex_indices, 0);
        brokenFile.m_elementTypes[1].m_propertyTypes[idx_face_vertex_indices].m_name = "DELETED";
        ASSERT_FALSE(PLYFileConverter::convert(brokenFile, outputMesh, &plyResult));
        ASSERT_EQ(plyResult, PLYResult::ERROR_INVALID_FACE_ELEMENT_TYPE);
    }
#endif
}

//! Test conversion to/from CGMesh
TEST(PLYFileConverter, ConvertToFromCGMesh)
{
    // DEBUGGING
    PLYFile debugFile;
    PLYFileConverter::convert(createTestCGMesh(12, true, true, true), debugFile);
    debugFile.writeFile("debugPLYFileASCII.ply", PLYFormat::Type::ASCII);
    debugFile.writeFile("debugPLYFileLittleEndian.ply", PLYFormat::Type::BINARY_LITTLE_ENDIAN);
    debugFile.writeFile("debugPLYFileBinaryEndian.ply", PLYFormat::Type::BINARY_BIG_ENDIAN);

    // Check empty mesh
    checkConvertToFromCGMesh(0, false, false, false);

    // Loop for different triangle counts
    for (size_t i = 0; i < 3; i++) {
        // All possible permutations of the options
        checkConvertToFromCGMesh(3 + i, false, false, false);
        checkConvertToFromCGMesh(3 + i, false, false, true);
        checkConvertToFromCGMesh(3 + i, false, true, false);
        checkConvertToFromCGMesh(3 + i, false, true, true);
        checkConvertToFromCGMesh(3 + i, true, false, false);
        checkConvertToFromCGMesh(3 + i, true, false, true);
        checkConvertToFromCGMesh(3 + i, true, true, false);
        checkConvertToFromCGMesh(3 + i, true, true, true);
    }
}
