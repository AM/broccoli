/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/ply/PLYFormat.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test converstion TO string representation of formats
TEST(PLYFormat, ToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PLYFormat::Type::TYPE_COUNT); i++) {
        ASSERT_GT(PLYFormat::toString(static_cast<PLYFormat::Type>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(PLYFormat::toString(static_cast<PLYFormat::Type>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(PLYFormat::toString(static_cast<PLYFormat::Type>(i)) != "UNKNOWN");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(PLYFormat::toString(PLYFormat::Type::TYPE_COUNT) == "UNKNOWN");
#endif
}

//! Test converstion FROM string representation of formats
TEST(PLYFormat, FromString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(PLYFormat::Type::TYPE_COUNT); i++) {
        const std::string stringRepresentation = PLYFormat::toString(static_cast<PLYFormat::Type>(i));
        const PLYFormat::Type typeRepresentation = PLYFormat::fromString(stringRepresentation);
        ASSERT_TRUE(typeRepresentation == static_cast<PLYFormat::Type>(i));
    }
#ifdef NDEBUG
    ASSERT_TRUE(PLYFormat::fromString("") == PLYFormat::Type::UNKNOWN);
#endif
}
