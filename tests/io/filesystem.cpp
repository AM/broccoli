/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/filesystem.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Check handling of files
TEST(filesystem, FileHandling)
{
    // Initialize helpers
    std::string testFile = "test_filesystem_FileHandling.txt";
    std::string testFileCopy = "test_filesystem_FileHandlingCopy.txt";

    // Check, if file exists (could be true or false)
    if (filesystem::fileExists(testFile) == true) {
        // Remove file
        ASSERT_EQ(filesystem::removeFile(testFile), true);
    }

    // Check, if file exists (has to be gone now)
    ASSERT_EQ(filesystem::fileExists(testFile), false);

    // Create file
    std::string testString = "Test of file handling...\n";
    std::ofstream newTestFile;
    newTestFile.open(testFile);
    newTestFile << testString;
    newTestFile.close();

    // Check, if file exists
    ASSERT_EQ(filesystem::fileExists(testFile), true);

    // Test detection of file size
    ASSERT_EQ(filesystem::fileSize(testFile), testString.size());

    // Check copying file
    ASSERT_EQ(filesystem::copyFile(testFile, testFileCopy), true);
    ASSERT_EQ(filesystem::fileExists(testFileCopy), true);

    // Remove files
    ASSERT_EQ(filesystem::removeFile(testFile), true);
    ASSERT_EQ(filesystem::removeFile(testFileCopy), true);

    // Check, if files exist (have to be gone now)
    ASSERT_EQ(filesystem::fileExists(testFile), false);
    ASSERT_EQ(filesystem::fileExists(testFileCopy), false);
}

//! Check handling of empty directories
TEST(filesystem, DirectoryHandlingEmpty)
{
    // Initialize helpers
    std::string testDirectory = "test_filesystem_DirectoryHandlingEmpty";

    // Check, if directory exists (could be true or false)
    if (filesystem::directoryExists(testDirectory) == true) {
        // Remove directory
        ASSERT_EQ(filesystem::removeDirectory(testDirectory), true);
    }

    // Check, if directory exists (has to be gone now)
    ASSERT_EQ(filesystem::directoryExists(testDirectory), false);

    // Create directory
    ASSERT_EQ(filesystem::createDirectory(testDirectory), true);

    // Check, if directory exists
    ASSERT_EQ(filesystem::directoryExists(testDirectory), true);

    // Remove directory
    ASSERT_EQ(filesystem::removeDirectory(testDirectory), true);

    // Check, if directory exists (has to be gone now)
    ASSERT_EQ(filesystem::directoryExists(testDirectory), false);
}

//! Check parsing of file paths
TEST(filesystem, FilePathParsing)
{
    // Check parsing of file names
    ASSERT_EQ(filesystem::parseFileName(""), "");
    ASSERT_EQ(filesystem::parseFileName("/"), "");
    ASSERT_EQ(filesystem::parseFileName("a/"), "");
    ASSERT_EQ(filesystem::parseFileName("/b"), "b");
    ASSERT_EQ(filesystem::parseFileName("a/b"), "b");
    ASSERT_EQ(filesystem::parseFileName("/a/b"), "b");

    // Check parsing of directory paths
    ASSERT_EQ(filesystem::parseDirectory(""), "");
    ASSERT_EQ(filesystem::parseDirectory("/"), "");
    ASSERT_EQ(filesystem::parseDirectory("a/"), "a");
    ASSERT_EQ(filesystem::parseDirectory("/b"), "");
    ASSERT_EQ(filesystem::parseDirectory("a/b"), "a");
    ASSERT_EQ(filesystem::parseDirectory("/a/b"), "/a");
}

//! Check parsing of file extensions
TEST(filesystem, FileExtensionParsing)
{
    ASSERT_EQ(filesystem::parseFileExtension(""), "");
    ASSERT_EQ(filesystem::parseFileExtension("."), "");
    ASSERT_EQ(filesystem::parseFileExtension("a"), "");
    ASSERT_EQ(filesystem::parseFileExtension("a."), "");
    ASSERT_EQ(filesystem::parseFileExtension(".b"), "b");
    ASSERT_EQ(filesystem::parseFileExtension("a.b"), "b");
}

//! Check writing and reading files
TEST(filesystem, FileWritingAndReading)
{
    // Initialize helpers
    const std::string testFilePathUncompressed = "test_filesystem_FileWritingAndReading_uncompressed.txt";
    const std::string testFilePathCompressedZlib = "test_filesystem_FileWritingAndReading_compressed.txt.zlib";
    const std::string testFilePathCompressedGzip = "test_filesystem_FileWritingAndReading_compressed.txt.gz";
    const std::string testMessage = "Hello World!";

    // Create buffers
    filesystem::DataStream writeStream(testMessage.begin(), testMessage.end());
    filesystem::DataStream readStreamUncompressed, readStreamCompressedZlib, readStreamCompressedGzip;

    // Write files
    ASSERT_TRUE(filesystem::writeFile(testFilePathUncompressed, writeStream));
    ASSERT_TRUE(filesystem::writeFile(testFilePathCompressedZlib, writeStream));
    ASSERT_TRUE(filesystem::writeFile(testFilePathCompressedGzip, writeStream));

    // Read files
    ASSERT_TRUE(filesystem::readFile(testFilePathUncompressed, readStreamUncompressed));
    ASSERT_TRUE(filesystem::readFile(testFilePathCompressedZlib, readStreamCompressedZlib));
    ASSERT_TRUE(filesystem::readFile(testFilePathCompressedGzip, readStreamCompressedGzip));

    // Compare content
    ASSERT_TRUE(readStreamUncompressed == writeStream);
    ASSERT_TRUE(readStreamCompressedZlib == writeStream);
    ASSERT_TRUE(readStreamCompressedGzip == writeStream);
}
