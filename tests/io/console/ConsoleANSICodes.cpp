/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/console/ConsoleANSICodes.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

TEST(ConsoleANSICodes, Codes)
{
    // Test foreground colors
    std::cout << ConsoleANSICodes::ForeGround::black() << "ForeGround::black()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::red() << "ForeGround::red()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::green() << "ForeGround::green()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::yellow() << "ForeGround::yellow()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::blue() << "ForeGround::blue()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::magenta() << "ForeGround::magenta()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::cyan() << "ForeGround::cyan()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::white() << "ForeGround::white()"
              << ConsoleANSICodes::reset() << std::endl;

    // Test foreground colors (bright)
    std::cout << ConsoleANSICodes::ForeGround::brightBlack() << "ForeGround::brightBlack()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::brightRed() << "ForeGround::brightRed()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::brightGreen() << "ForeGround::brightGreen()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::brightYellow() << "ForeGround::brightYellow()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::brightBlue() << "ForeGround::brightBlue()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::brightMagenta() << "ForeGround::brightMagenta()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::brightCyan() << "ForeGround::brightCyan()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::ForeGround::brightWhite() << "ForeGround::brightWhite()"
              << ConsoleANSICodes::reset() << std::endl;

    // Test background colors
    std::cout << ConsoleANSICodes::BackGround::black() << "BackGround::black()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::red() << "BackGround::red()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::green() << "BackGround::green()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::yellow() << "BackGround::yellow()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::blue() << "BackGround::blue()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::magenta() << "BackGround::magenta()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::cyan() << "BackGround::cyan()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::white() << "BackGround::white()"
              << ConsoleANSICodes::reset() << std::endl;

    // Test background colors (bright)
    std::cout << ConsoleANSICodes::BackGround::brightBlack() << "BackGround::brightBlack()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::brightRed() << "BackGround::brightRed()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::brightGreen() << "BackGround::brightGreen()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::brightYellow() << "BackGround::brightYellow()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::brightBlue() << "BackGround::brightBlue()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::brightMagenta() << "BackGround::brightMagenta()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::brightCyan() << "BackGround::brightCyan()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::BackGround::brightWhite() << "BackGround::brightWhite()"
              << ConsoleANSICodes::reset() << std::endl;

    // Test decorations
    std::cout << ConsoleANSICodes::bold() << "bold()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::underline() << "underline()"
              << ConsoleANSICodes::reset() << std::endl;
    std::cout << ConsoleANSICodes::reversed() << "reversed()"
              << ConsoleANSICodes::reset() << std::endl;
}
