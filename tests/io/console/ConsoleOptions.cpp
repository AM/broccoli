/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/console/ConsoleOptions.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

static constexpr size_t customInterfaceIndex = ConsoleOptions::defaultInterfaceCount(); //!< Index of custom interface

//! Test configuration
TEST(ConsoleOptions, BasicUsage)
{
    // Constructor
    ConsoleOptions defaultOptions;
    ASSERT_EQ(defaultOptions.interfaceCount(), ConsoleOptions::defaultInterfaceCount());
    ConsoleOptions specialOptions("threadName", 123, 10, false, "logFileName");
    ASSERT_EQ(specialOptions.interfaceCount(), ConsoleOptions::defaultInterfaceCount());
    specialOptions.addInterface(ConsoleInterfaceOptions());
    ASSERT_EQ(specialOptions.interfaceCount(), ConsoleOptions::defaultInterfaceCount() + 1);

    // Setter for interface options
    specialOptions.plainOptions().m_prefixConsole = "plain";
    specialOptions.debugOptions().m_prefixConsole = "debug";
    specialOptions.infoOptions().m_prefixConsole = "info";
    specialOptions.warningOptions().m_prefixConsole = "warning";
    specialOptions.errorOptions().m_prefixConsole = "error";
    specialOptions.criticalOptions().m_prefixConsole = "critical";
    specialOptions.interfaceOptions(customInterfaceIndex).m_prefixConsole = "custom";

    // Getter for interface options
    const ConsoleOptions optionCopy = specialOptions;
    ASSERT_TRUE(optionCopy.plainOptions().m_prefixConsole == "plain");
    ASSERT_TRUE(optionCopy.debugOptions().m_prefixConsole == "debug");
    ASSERT_TRUE(optionCopy.infoOptions().m_prefixConsole == "info");
    ASSERT_TRUE(optionCopy.warningOptions().m_prefixConsole == "warning");
    ASSERT_TRUE(optionCopy.errorOptions().m_prefixConsole == "error");
    ASSERT_TRUE(optionCopy.criticalOptions().m_prefixConsole == "critical");
    ASSERT_TRUE(optionCopy.interfaceOptions(customInterfaceIndex).m_prefixConsole == "custom");
}
