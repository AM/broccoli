/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/console/Console.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

static constexpr unsigned int bufferSize = 6; //!< Size of the buffer
static constexpr size_t customInterfaceIndex = ConsoleOptions::defaultInterfaceCount(); //!< Index of custom interface

//! Global interface for "custom" messages
inline const Console::Interface& customInterface()
{
    static Console::Interface interface(static_cast<size_t>(customInterfaceIndex));
    return interface;
}

//! Test configuration
TEST(Console, Configuration)
{
    // Default configuration
    Console::flush();

    // First configuration
    Console::configure(ConsoleOptions("Console", 0, bufferSize, false, "ConsoleTest"));

    // Re-configuration
    ConsoleOptions options = Console::options();
    options.m_writeLogFile = true;
    ASSERT_EQ(options.addInterface(ConsoleInterfaceOptions(true, "<ConsolePrefix>", "<ConsoleSuffix>", true, "<PrefixLogFile>", "<SuffixLogFile>")), customInterfaceIndex);
    Console::configure(options);
}

//! Test writing through print
TEST(Console, Print)
{
    // Initialize helpers
    const core::Time timeout(10, 0);

    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::plain().print("Console::plain().print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::debug().print("Console::debug().print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::info().print("Console::info().print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::warning().print("Console::warning().print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::error().print("Console::error().print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::critical().print("Console::critical().print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    customInterface().print("customInterface().print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::Interface invalidInterface(123);
    invalidInterface.print("invalidInterface.print()\n");
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    ASSERT_EQ(Console::lostMessageCount(), 0);
}

//! Test writing through printf
TEST(Console, Printf)
{
    // Initialize helpers
    const core::Time timeout(10, 0);

    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::plain().printf("Console::plain().printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::debug().printf("Console::debug().printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::info().printf("Console::info().printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::warning().printf("Console::warning().printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::error().printf("Console::error().printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::critical().printf("Console::critical().printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    customInterface().printf("customInterface().printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::Interface invalidInterface(123);
    invalidInterface.printf("invalidInterface.printf(%.2f)\n", 1.23456789);
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    ASSERT_EQ(Console::lostMessageCount(), 0);
}

//! Test writing through operator<<
TEST(Console, StreamOperator)
{
    // Initialize helpers
    const core::Time timeout(10, 0);

    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::plain() << "Console::plain()"
                     << " << ";
    Console::plain() << 1.23456789
                     << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::debug() << "Console::debug()"
                     << " << ";
    Console::debug() << 1.23456789
                     << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::info() << "Console::info()"
                    << " << ";
    Console::info() << 1.23456789
                    << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::warning() << "Console::warning()"
                       << " << ";
    Console::warning() << 1.23456789
                       << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::error() << "Console::error()"
                     << " << ";
    Console::error() << 1.23456789
                     << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::critical() << "Console::critical()"
                        << " << ";
    Console::critical() << 1.23456789
                        << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    customInterface() << "customInterface()"
                      << " << ";
    customInterface() << 1.23456789
                      << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    Console::Interface invalidInterface(123);
    invalidInterface << "invalidInterface()"
                     << " << ";
    invalidInterface << 1.23456789
                     << "\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    ASSERT_EQ(Console::lostMessageCount(), 0);
}

//! Test writing with mixed operators and levels
TEST(Console, MixelLevelsAndOperators)
{
    // Initialize helpers
    const core::Time timeout(10, 0);

    Console::debug().print("Console::debug().print()");
    Console::info().printf("Console::info().print(%.2f)", 1.23456789);
    Console::warning() << "";
    Console::warning() << "Console::warning() << \"\"";
    Console::error() << "\nConsole::error() << \\n\n";
    Console::flush(timeout);
    ASSERT_EQ(Console::bufferedMessageCount(), 0);

    ASSERT_EQ(Console::lostMessageCount(), 0);
}

//! Test writing an incomplete line and clearing it again
TEST(Console, ClearLine)
{
    // Initialize helpers
    const core::Time timeout(10, 0);

    // Write temporary text
    Console::plain().print("incomplete");

    // Flush
    Console::flush(timeout);

    // Reset text
    Console::plain().print("\r          \r");
    Console::flush(timeout);

    ASSERT_EQ(Console::lostMessageCount(), 0);
}
