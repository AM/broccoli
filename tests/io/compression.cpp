/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/compression.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Helper method to fill a data stream with dummy data
static inline void fillDummyDataStream(compression::DataStream& dataStream, const size_t& size)
{
    dataStream.resize(size);
    for (size_t i = 0; i < size; i++)
        dataStream[i] = i % 8;
}

//! Check basic usage for compression and decompression
TEST(compression, BasicUsage)
{
    // Check no-compression (non-empty)
    compression::DataStream noCompressionInput, noCompressionOutput;
    fillDummyDataStream(noCompressionInput, 123);
    noCompressionOutput.clear();
    ASSERT_TRUE(compression::compress(noCompressionInput, noCompressionOutput, compression::CompressionOptions::noCompression()));
    ASSERT_TRUE(noCompressionOutput == noCompressionInput);
    noCompressionOutput.clear();
    ASSERT_TRUE(compression::decompress(noCompressionInput, noCompressionOutput, compression::CompressionOptions::noCompression()));
    ASSERT_TRUE(noCompressionOutput == noCompressionInput);

    // Check no-compression (empty)
    noCompressionInput.clear();
    noCompressionOutput.clear();
    ASSERT_TRUE(compression::compress(noCompressionInput, noCompressionOutput, compression::CompressionOptions::noCompression()));
    ASSERT_TRUE(noCompressionOutput == noCompressionInput);
    noCompressionOutput.clear();
    ASSERT_TRUE(compression::decompress(noCompressionInput, noCompressionOutput, compression::CompressionOptions::noCompression()));
    ASSERT_TRUE(noCompressionOutput == noCompressionInput);

    // Setup loops
    const size_t minimumStreamLength = 0;
    const size_t maximumStreamLength = 1024;
    int minimumCompressionLevel = -1;
    int maximumCompressionLevel = 9;
#ifdef NDEBUG
    minimumCompressionLevel--;
    maximumCompressionLevel++;
#endif

    // Pass through all methods
    for (uint8_t i = 0; i < static_cast<uint8_t>(compression::Method::METHOD_COUNT); i++) {
        // Test for different compression levels
        for (int j = minimumCompressionLevel; j <= maximumCompressionLevel; j++) {
            // Setup options
            const compression::Method method = static_cast<compression::Method>(i);
            compression::CompressionOptions compressionOptions(true, method, j);
            compression::DecompressionOptions decompressionOptions(compressionOptions);
            ASSERT_TRUE(compressionOptions.compressed() == true);
            ASSERT_TRUE(compressionOptions.method() == method);
            if (j < -1)
                ASSERT_TRUE(compressionOptions.level() == -1);
            else if (j > 9)
                ASSERT_TRUE(compressionOptions.level() == 9);
            else
                ASSERT_TRUE(compressionOptions.level() == j);
            if (method == compression::Method::RAW) {
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::noCompression());
                ASSERT_TRUE(compressionOptions == compression::CompressionOptions::raw(j));
                if (j > -1 && j < 9) {
                    ASSERT_TRUE(compressionOptions != compression::CompressionOptions::raw(j - 1));
                    ASSERT_TRUE(compressionOptions != compression::CompressionOptions::raw(j + 1));
                }
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::zlib(j));
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::gzip(j));
                ASSERT_TRUE(decompressionOptions != compression::DecompressionOptions(compression::CompressionOptions::noCompression()));
                ASSERT_TRUE(decompressionOptions == compression::DecompressionOptions(compression::CompressionOptions::raw(j)));
                ASSERT_TRUE(decompressionOptions != compression::DecompressionOptions(compression::CompressionOptions::zlib(j)));
                ASSERT_TRUE(decompressionOptions != compression::DecompressionOptions(compression::CompressionOptions::gzip(j)));
            } else if (method == compression::Method::ZLIB) {
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::noCompression());
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::raw(j));
                ASSERT_TRUE(compressionOptions == compression::CompressionOptions::zlib(j));
                if (j > -1 && j < 9) {
                    ASSERT_TRUE(compressionOptions != compression::CompressionOptions::zlib(j - 1));
                    ASSERT_TRUE(compressionOptions != compression::CompressionOptions::zlib(j + 1));
                }
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::gzip(j));
                ASSERT_TRUE(decompressionOptions != compression::DecompressionOptions(compression::CompressionOptions::noCompression()));
                ASSERT_TRUE(decompressionOptions != compression::DecompressionOptions(compression::CompressionOptions::raw(j)));
                ASSERT_TRUE(decompressionOptions == compression::DecompressionOptions(compression::CompressionOptions::zlib(j)));
                ASSERT_TRUE(decompressionOptions == compression::DecompressionOptions(compression::CompressionOptions::gzip(j)));
            } else {
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::noCompression());
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::raw(j));
                ASSERT_TRUE(compressionOptions != compression::CompressionOptions::zlib(j));
                ASSERT_TRUE(compressionOptions == compression::CompressionOptions::gzip(j));
                if (j > -1 && j < 9) {
                    ASSERT_TRUE(compressionOptions != compression::CompressionOptions::gzip(j - 1));
                    ASSERT_TRUE(compressionOptions != compression::CompressionOptions::gzip(j + 1));
                }
                ASSERT_TRUE(decompressionOptions != compression::DecompressionOptions(compression::CompressionOptions::noCompression()));
                ASSERT_TRUE(decompressionOptions != compression::DecompressionOptions(compression::CompressionOptions::raw(j)));
                ASSERT_TRUE(decompressionOptions == compression::DecompressionOptions(compression::CompressionOptions::zlib(j)));
                ASSERT_TRUE(decompressionOptions == compression::DecompressionOptions(compression::CompressionOptions::gzip(j)));
            }

            // Test for different stream lengths
            for (size_t k = minimumStreamLength; k <= maximumStreamLength; k++) {
                // Setup streams
                compression::DataStream compressInput, compressOutput, decompressInput, decompressOutput;
                compression::DataStream originalData;
                fillDummyDataStream(originalData, k);

                // Check compression and decompression (empty stream)
                compressInput.clear();
                compressOutput.clear();
                ASSERT_TRUE(compression::compress(compressInput, compressOutput, compressionOptions));
                decompressInput = compressOutput;
                decompressOutput.clear();
                ASSERT_TRUE(compression::decompress(decompressInput, decompressOutput, compressionOptions));
                ASSERT_TRUE(decompressOutput == compressInput);

                // Check compression and decompression (not pre-allocated)
                compressInput = originalData;
                compressOutput.clear();
                ASSERT_TRUE(compression::compress(compressInput, compressOutput, compressionOptions));
                decompressInput = compressOutput;
                decompressOutput.clear();
                ASSERT_TRUE(compression::decompress(decompressInput, decompressOutput, compressionOptions));
                ASSERT_TRUE(decompressOutput == compressInput);

                // Get buffer sizes
                const size_t uncompressedSize = compressInput.size();
                const size_t compressedSize = compressOutput.size();

                // Check compression and decompression (pre-allocated - sufficient space)
                compressInput = originalData;
                compressOutput.clear();
                compressOutput.resize(2 * compressedSize, 0);
                ASSERT_TRUE(compression::compress(compressInput, compressOutput, compressionOptions));
                decompressInput = compressOutput;
                decompressOutput.clear();
                decompressOutput.resize(2 * uncompressedSize, 0);
                ASSERT_TRUE(compression::decompress(decompressInput, decompressOutput, compressionOptions));
                ASSERT_TRUE(decompressOutput == compressInput);

                // Check compression and decompression (pre-allocated - insufficient space)
                compressInput = originalData;
                compressOutput.clear();
                compressOutput.resize(1, 0);
                ASSERT_TRUE(compression::compress(compressInput, compressOutput, compressionOptions));
                decompressInput = compressOutput;
                decompressOutput.clear();
                decompressOutput.resize(1, 0);
                ASSERT_TRUE(compression::decompress(decompressInput, decompressOutput, compressionOptions));
                ASSERT_TRUE(decompressOutput == compressInput);

                // Check invalid cases
                if (k > 0) {
                    // Check decompression (broken input stream - removed byte)
                    decompressInput = compressOutput;
                    decompressInput.resize(decompressInput.size() - 1);
                    decompressOutput.clear();
                    ASSERT_FALSE(compression::decompress(decompressInput, decompressOutput, compressionOptions));

                    // Check decompression (broken input stream - invalid data)
                    decompressInput.clear();
                    decompressInput.resize(compressOutput.size(), 0);
                    decompressOutput.clear();
                    ASSERT_FALSE(compression::decompress(decompressInput, decompressOutput, compressionOptions));

                    // Check decompression (too large input stream - added byte)
                    decompressInput = compressOutput;
                    decompressInput.resize(decompressInput.size() + 1, 0);
                    decompressOutput.clear();
                    ASSERT_TRUE(compression::decompress(decompressInput, decompressOutput, compressionOptions));
                }
            }
        }
    }
}
