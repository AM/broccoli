/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/encoding.hpp"
#include "gtest/gtest.h"
#include <limits>

using namespace broccoli;
using namespace io;

//! Check encoding and decoding of basic datatypes
TEST(encoding, BasicDataTypes)
{
    // Initialize helpers
    encoding::CharacterStream stream;
    std::vector<encoding::CharacterStreamSize> partSizes; // Sizes of added parts
    char testChar = 'A';
    uint8_t testUint8_t = std::numeric_limits<uint8_t>::max() - 1;
    int8_t testInt8_t = std::numeric_limits<int8_t>::min() + 1;
    uint16_t testUint16_t = std::numeric_limits<uint16_t>::max() - 1;
    int16_t testInt16_t = std::numeric_limits<int16_t>::min() + 1;
    uint32_t testUint32_t = std::numeric_limits<uint32_t>::max() - 1;
    int32_t testInt32_t = std::numeric_limits<int32_t>::min() + 1;
    uint64_t testUint64_t = std::numeric_limits<uint64_t>::max() - 1;
    int64_t testInt64_t = std::numeric_limits<int64_t>::min() + 1;
    float testFloat = std::numeric_limits<float>::max() / 2.0;
    double testDouble = std::numeric_limits<double>::max() / 2.0;
    std::string testString = "test";

    // Test encoding
    // -------------
    // Create stream of datatypes
    partSizes.push_back(encoding::encode(stream, testChar)); // Test character
    partSizes.push_back(encoding::encode(stream, testUint8_t)); // Test uint8_t
    partSizes.push_back(encoding::encode(stream, testUint16_t)); // Test uint16_t
    partSizes.push_back(encoding::encode(stream, testUint32_t)); // Test uint32_t
    partSizes.push_back(encoding::encode(stream, testUint64_t)); // Test uint64_t
    partSizes.push_back(encoding::encode(stream, testInt8_t)); // Test int8_t
    partSizes.push_back(encoding::encode(stream, testInt16_t)); // Test int16_t
    partSizes.push_back(encoding::encode(stream, testInt32_t)); // Test int32_t
    partSizes.push_back(encoding::encode(stream, testInt64_t)); // Test int64_t
    partSizes.push_back(encoding::encode(stream, testFloat)); // Test float
    partSizes.push_back(encoding::encode(stream, testDouble)); // Test double
    partSizes.push_back(encoding::encode(stream, testString)); // Test string

    // Check single sizes
    ASSERT_EQ(partSizes[0], 1); // char
    ASSERT_EQ(partSizes[1], 3); // uint8_t
    ASSERT_EQ(partSizes[2], 5); // uint16_t
    ASSERT_EQ(partSizes[3], 10); // uint32_t
    ASSERT_EQ(partSizes[4], 20); // uint64_t
    ASSERT_EQ(partSizes[5], 4); // int8_t
    ASSERT_EQ(partSizes[6], 6); // int16_t
    ASSERT_EQ(partSizes[7], 11); // int32_t
    ASSERT_EQ(partSizes[8], 20); // int64_t
    ASSERT_EQ(partSizes[9], 14); // float
    ASSERT_EQ(partSizes[10], 24); // double
    ASSERT_EQ(partSizes[11], 4); // string

    // Check total size
    encoding::CharacterStreamSize totalSize = 0;
    for (size_t i = 0; i < partSizes.size(); i++)
        totalSize += partSizes[i];
    ASSERT_EQ(totalSize, stream.size());

    // Test decoding
    // -------------
    encoding::CharacterStreamSize currentIndex = 0;

    // Decode char
    char decodedChar = '0';
    currentIndex = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, decodedChar), partSizes[0]);
    ASSERT_EQ(decodedChar, testChar);
    currentIndex += partSizes[0];

    // Decode uint8_t
    uint8_t decodedUint8_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[1], decodedUint8_t), partSizes[1]);
    ASSERT_EQ(decodedUint8_t, testUint8_t);
    currentIndex += partSizes[1];

    // Decode uint16_t
    uint16_t decodedUint16_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[2], decodedUint16_t), partSizes[2]);
    ASSERT_EQ(decodedUint16_t, testUint16_t);
    currentIndex += partSizes[2];

    // Decode uint32_t
    uint32_t decodedUint32_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[3], decodedUint32_t), partSizes[3]);
    ASSERT_EQ(decodedUint32_t, testUint32_t);
    currentIndex += partSizes[3];

    // Decode uint64_t
    uint64_t decodedUint64_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[4], decodedUint64_t), partSizes[4]);
    ASSERT_EQ(decodedUint64_t, testUint64_t);
    currentIndex += partSizes[4];

    // Decode int8_t
    int8_t decodedInt8_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[5], decodedInt8_t), partSizes[5]);
    ASSERT_EQ(decodedInt8_t, testInt8_t);
    currentIndex += partSizes[5];

    // Decode int16_t
    int16_t decodedInt16_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[6], decodedInt16_t), partSizes[6]);
    ASSERT_EQ(decodedInt16_t, testInt16_t);
    currentIndex += partSizes[6];

    // Decode int32_t
    int32_t decodedInt32_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[7], decodedInt32_t), partSizes[7]);
    ASSERT_EQ(decodedInt32_t, testInt32_t);
    currentIndex += partSizes[7];

    // Decode int64_t
    int64_t decodedInt64_t = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[8], decodedInt64_t), partSizes[8]);
    ASSERT_EQ(decodedInt64_t, testInt64_t);
    currentIndex += partSizes[8];

    // Decode float
    float decodedFloat = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[9], decodedFloat), partSizes[9]);
    ASSERT_FLOAT_EQ(decodedFloat, testFloat);
    currentIndex += partSizes[9];

    // Decode double
    double decodedDouble = 0;
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[10], decodedDouble), partSizes[10]);
    ASSERT_DOUBLE_EQ(decodedDouble, testDouble);
    currentIndex += partSizes[10];

    // Decode string
    std::string decodedString = "";
    ASSERT_EQ(encoding::decode(stream, currentIndex, partSizes[11], decodedString), partSizes[11]);
    ASSERT_EQ(decodedString, testString);
    currentIndex += partSizes[11];

    // Test index length
    ASSERT_EQ(currentIndex, stream.size());

    // Test invalid index
#ifdef NDEBUG
    ASSERT_EQ(encoding::decode(stream, stream.size(), decodedChar), 0);
#endif

    // Test empty stream
#ifdef NDEBUG
    const encoding::CharacterStream emptyStream;
    ASSERT_EQ(encoding::decode(emptyStream, 0, decodedChar), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedUint8_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedUint8_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedUint16_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedUint16_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedUint32_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedUint32_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedUint64_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedUint64_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedInt8_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedInt8_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedInt16_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedInt16_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedInt32_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedInt32_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedInt64_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedInt64_t), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedFloat), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedFloat), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedDouble), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedDouble), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 0, decodedString), 0);
    ASSERT_EQ(encoding::decode(emptyStream, 0, 1, decodedString), 0);
#endif

    // Test invalid characters
#ifdef NDEBUG
    encoding::CharacterStream invalidStream;
    encoding::encode(invalidStream, "abcdefghijklmnopqrstuvwxyz");
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedUint8_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedUint16_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedUint32_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedUint64_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedInt8_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedInt16_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedInt32_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedInt64_t), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedFloat), 0);
    ASSERT_EQ(encoding::decode(invalidStream, 0, 1, decodedDouble), 0);
#endif

    // Test overflow
#ifdef NDEBUG
    encoding::CharacterStream overflowTestUint64_t;
    encoding::encode(overflowTestUint64_t, (uint64_t)std::numeric_limits<uint64_t>::max());
    ASSERT_EQ(encoding::decode(overflowTestUint64_t, 0, overflowTestUint64_t.size(), decodedUint8_t), 0);
    ASSERT_EQ(encoding::decode(overflowTestUint64_t, 0, overflowTestUint64_t.size(), decodedUint16_t), 0);
    ASSERT_EQ(encoding::decode(overflowTestUint64_t, 0, overflowTestUint64_t.size(), decodedUint32_t), 0);
    encoding::CharacterStream overflowTestInt64_t;
    encoding::encode(overflowTestInt64_t, (int64_t)std::numeric_limits<int64_t>::max());
    ASSERT_EQ(encoding::decode(overflowTestInt64_t, 0, overflowTestInt64_t.size(), decodedInt8_t), 0);
    ASSERT_EQ(encoding::decode(overflowTestInt64_t, 0, overflowTestInt64_t.size(), decodedInt16_t), 0);
    ASSERT_EQ(encoding::decode(overflowTestInt64_t, 0, overflowTestInt64_t.size(), decodedInt32_t), 0);
    encoding::CharacterStream overflowTestDouble;
    encoding::encode(overflowTestDouble, (double)std::numeric_limits<double>::max());
    ASSERT_EQ(encoding::decode(overflowTestDouble, 0, overflowTestDouble.size(), decodedFloat), 0);
#endif
}

//! Check encoding and decoding for special format specifiers
TEST(encoding, SpecialFormatSpecifier)
{
    // Initialize helpers
    encoding::CharacterStream stream;

    // Test scientific notation without leading zeros
    std::array<double, 15> values{ 0.0e0, -1.23411e1, -1.23411e-1, -1.23411e10, -1.23411e-10, -1.23411e12, -1.23411e-12, -1.23411e100, -1.23411e-100, -1.23411e103, -1.23411e-103, -1.23411e120, -1.23411e-120, -1.23411e123, -1.23411e-123 };
    std::array<std::string, 15> formats{ "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e", "^%.3e" };
    std::array<std::string, 15> expectedStrings{ "0.000e+0", "-1.234e+1", "-1.234e-1", "-1.234e+10", "-1.234e-10", "-1.234e+12", "-1.234e-12", "-1.234e+100", "-1.234e-100", "-1.234e+103", "-1.234e-103", "-1.234e+120", "-1.234e-120", "-1.234e+123", "-1.234e-123" };
    for (size_t i = 0; i < values.size(); i++) {
        stream.clear();
        ASSERT_EQ(encoding::encode(stream, values[i], formats[i]), expectedStrings[i].size());
        const std::string actualString(stream.begin(), stream.end());
        ASSERT_EQ(actualString, expectedStrings[i]);
    }

    // Test invalid input
#ifdef NDEBUG
    stream.clear();
    ASSERT_EQ(encoding::encode(stream, "abc"), 3);
    ASSERT_EQ(encoding::encode(stream, (double)1.23, ""), 0);
    ASSERT_EQ(stream.size(), 3);
    ASSERT_EQ(encoding::encode(stream, (double)1.23, "%"), 0);
    ASSERT_EQ(stream.size(), 3);
    ASSERT_EQ(encoding::encode(stream, (double)1.23, "%.16e abcdefghijklmnopqrstuvwxyz"), 0);
    ASSERT_EQ(stream.size(), 3);
    ASSERT_EQ(encoding::encode(stream, (double)1.23, "a%"), 0);
    ASSERT_EQ(stream.size(), 3);
    ASSERT_EQ(encoding::encode(stream, (double)1.23, "^%"), 0);
    ASSERT_EQ(stream.size(), 3);
    ASSERT_EQ(encoding::encode(stream, (double)1.23, "^%.16e abcdefghijklmnopqrstuvwxyz"), 0);
    ASSERT_EQ(stream.size(), 3);
#endif
}

#ifdef HAVE_EIGEN3
//! Check encoding and decoding of matrices
TEST(encoding, Matrix)
{
    // Initialize helpers
    encoding::CharacterStream stream;
    Eigen::Matrix<double, 2, 3> originalMatrixFixedSize;
    originalMatrixFixedSize << 1.23, 2.34, 3.45, -5.43, -4.32, -3.21;
    Eigen::Matrix<double, 2, 3> decodedMatrixFixedSize;
    decodedMatrixFixedSize << 1.23, 2.34, 3.45, -5.43, -4.32, -3.21;
    const encoding::CharacterStreamSize expectedLengthMatrixFixedSize = 5 * 2 * 3 + 2 * 2 + 1;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> originalMatrixDynamicSize;
    originalMatrixDynamicSize.resize(2, 3);
    originalMatrixDynamicSize << 1.23, 2.34, 3.45, -5.43, -4.32, -3.21;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> decodedMatrixDynamicSize;
    const encoding::CharacterStreamSize expectedLengthMatrixDynamicSize = 5 * 2 * 3 + 2 * 2 + 1;

    // Test encoding
    ASSERT_EQ((encoding::encode(stream, originalMatrixFixedSize, "%+.2f")), expectedLengthMatrixFixedSize);
    ASSERT_EQ((encoding::encode(stream, originalMatrixDynamicSize, "%+.2f")), expectedLengthMatrixDynamicSize);

    // Test decoding
    encoding::CharacterStreamSize currentIndex = 0;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthMatrixFixedSize, decodedMatrixFixedSize)), expectedLengthMatrixFixedSize);
    ASSERT_TRUE(decodedMatrixFixedSize == originalMatrixFixedSize);
    currentIndex += expectedLengthMatrixFixedSize;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthMatrixDynamicSize, decodedMatrixDynamicSize)), expectedLengthMatrixDynamicSize);
    ASSERT_TRUE(decodedMatrixDynamicSize == originalMatrixDynamicSize);

    // Test encoding (default format)
    const encoding::CharacterStreamSize expectedLengthDefaultFormat = 23 * 2 * 3 + 2 * 2 + 1;
    ASSERT_EQ((encoding::encode(stream, originalMatrixFixedSize)), expectedLengthDefaultFormat);
    ASSERT_EQ((encoding::encode(stream, originalMatrixDynamicSize)), expectedLengthDefaultFormat);

    // Test empty stream
#ifdef NDEBUG
    const encoding::CharacterStream emptyStream;
    ASSERT_EQ((encoding::decode(emptyStream, 0, 0, decodedMatrixFixedSize)), 0);
    ASSERT_EQ((encoding::decode(emptyStream, 0, 1, decodedMatrixFixedSize)), 0);
#endif

    // Test invalid matrix (non-rectangular)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2, 3; 4, 5");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedMatrixDynamicSize)), 0);
#endif

    // Test invalid matrix (wrong dimensions)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2; 3, 4");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedMatrixFixedSize)), 0);
#endif

    // Test invalid matrix (invalid coefficient)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2; 3, abc");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedMatrixDynamicSize)), 0);
#endif
}

//! Check encoding and decoding of quaternions
TEST(encoding, Quaternion)
{
    // Initialize helpers
    encoding::CharacterStream stream;
    Eigen::Quaterniond originalQuaternion(1.23, 2.34, -3.45, 4.56);
    Eigen::Quaterniond decodedQuaternion(0, 0, 0, 0);
    const encoding::CharacterStreamSize expectedLengthQuaternion = 4 * 5 + 3;

    // Test encoding
    ASSERT_EQ((encoding::encode(stream, originalQuaternion, "%+.2f")), expectedLengthQuaternion);

    // Test decoding
    encoding::CharacterStreamSize currentIndex = 0;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthQuaternion, decodedQuaternion)), expectedLengthQuaternion);
    ASSERT_TRUE(decodedQuaternion.coeffs() == originalQuaternion.coeffs());

    // Test encoding (default format)
    const encoding::CharacterStreamSize expectedLengthDefaultFormat = 4 * 23 + 3;
    ASSERT_EQ((encoding::encode(stream, originalQuaternion)), expectedLengthDefaultFormat);

    // Test empty stream
#ifdef NDEBUG
    const encoding::CharacterStream emptyStream;
    ASSERT_EQ((encoding::decode(emptyStream, 0, 0, decodedQuaternion)), 0);
    ASSERT_EQ((encoding::decode(emptyStream, 0, 1, decodedQuaternion)), 0);
#endif

    // Test invalid quaternion (invalid delimiter count)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2, 3");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedQuaternion)), 0);
#endif

    // Test invalid quaternion (invalid coefficient)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2, 3, abc");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedQuaternion)), 0);
#endif
}
#endif // HAVE_EIGEN3

//! Check encoding and decoding of arrays
TEST(encoding, Array)
{
    // Initialize helpers
    encoding::CharacterStream stream;
    std::array<uint64_t, 3> originalArrayUint64t{ 1, 2, 3 };
    std::array<uint64_t, 3> decodedArrayUint64t{ 0, 0, 0 };
    const encoding::CharacterStreamSize expectedLengthArrayUint64t = 3 * 1 + 2;
    std::array<int64_t, 3> originalArrayInt64t{ 1, -2, 3 };
    std::array<int64_t, 3> decodedArrayInt64t{ 0, 0, 0 };
    const encoding::CharacterStreamSize expectedLengthArrayInt64t = 3 * 2 + 2;
    std::array<double, 3> originalArrayDouble{ 1.2345, 2.34, -3.45 };
    std::array<double, 3> decodedArrayDouble{ 0.0, 0.0, 0.0 };
    const encoding::CharacterStreamSize expectedLengthArrayDouble = 3 * 7 + 2;

    // Test encoding
    ASSERT_EQ((encoding::encode(stream, originalArrayUint64t, "%llu")), expectedLengthArrayUint64t);
    ASSERT_EQ((encoding::encode(stream, originalArrayInt64t, "%+lld")), expectedLengthArrayInt64t);
    ASSERT_EQ((encoding::encode(stream, originalArrayDouble, "%+.4f")), expectedLengthArrayDouble);

    // Test decoding
    encoding::CharacterStreamSize currentIndex = 0;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthArrayUint64t, decodedArrayUint64t)), expectedLengthArrayUint64t);
    ASSERT_TRUE(decodedArrayUint64t == originalArrayUint64t);
    currentIndex += expectedLengthArrayUint64t;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthArrayInt64t, decodedArrayInt64t)), expectedLengthArrayInt64t);
    ASSERT_TRUE(decodedArrayInt64t == originalArrayInt64t);
    currentIndex += expectedLengthArrayInt64t;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthArrayDouble, decodedArrayDouble)), expectedLengthArrayDouble);
    ASSERT_TRUE(decodedArrayDouble == originalArrayDouble);

    // Test encoding (default format)
    const encoding::CharacterStreamSize expectedLengthDefaultFormat = 3 * 23 + 2;
    ASSERT_EQ((encoding::encode(stream, originalArrayDouble)), expectedLengthDefaultFormat);

    // Test empty stream
#ifdef NDEBUG
    const encoding::CharacterStream emptyStream;
    ASSERT_EQ((encoding::decode(emptyStream, 0, 0, decodedArrayDouble)), 0);
    ASSERT_EQ((encoding::decode(emptyStream, 0, 1, decodedArrayDouble)), 0);
#endif

    // Test invalid array (invalid delimiter count)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedArrayDouble)), 0);
#endif

    // Test invalid array (invalid coefficient)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2, abc");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedArrayDouble)), 0);
#endif
}

//! Check encoding and decoding of vectors
TEST(encoding, Vector)
{
    // Initialize helpers
    encoding::CharacterStream stream;
    std::vector<uint64_t> originalVectorUint64t{ 1, 2, 3 };
    std::vector<uint64_t> decodedVectorUint64t;
    const encoding::CharacterStreamSize expectedLengthVectorUint64t = 3 * 1 + 2;
    std::vector<int64_t> originalVectorInt64t{ 1, -2, 3 };
    std::vector<int64_t> decodedVectorInt64t;
    const encoding::CharacterStreamSize expectedLengthVectorInt64t = 3 * 2 + 2;
    std::vector<double> originalVectorDouble{ 1.2345, 2.34, -3.45 };
    std::vector<double> decodedVectorDouble;
    const encoding::CharacterStreamSize expectedLengthVectorDouble = 3 * 7 + 2;

    // Test encoding
    ASSERT_EQ((encoding::encode(stream, originalVectorUint64t, "%llu")), expectedLengthVectorUint64t);
    ASSERT_EQ((encoding::encode(stream, originalVectorInt64t, "%+lld")), expectedLengthVectorInt64t);
    ASSERT_EQ((encoding::encode(stream, originalVectorDouble, "%+.4f")), expectedLengthVectorDouble);

    // Test decoding
    encoding::CharacterStreamSize currentIndex = 0;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthVectorUint64t, decodedVectorUint64t)), expectedLengthVectorUint64t);
    ASSERT_TRUE(decodedVectorUint64t == originalVectorUint64t);
    currentIndex += expectedLengthVectorUint64t;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthVectorInt64t, decodedVectorInt64t)), expectedLengthVectorInt64t);
    ASSERT_TRUE(decodedVectorInt64t == originalVectorInt64t);
    currentIndex += expectedLengthVectorInt64t;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthVectorDouble, decodedVectorDouble)), expectedLengthVectorDouble);
    ASSERT_TRUE(decodedVectorDouble == originalVectorDouble);

    // Test encoding (default format)
    const encoding::CharacterStreamSize expectedLengthDefaultFormat = 3 * 23 + 2;
    ASSERT_EQ((encoding::encode(stream, originalVectorDouble)), expectedLengthDefaultFormat);

    // Test empty stream
#ifdef NDEBUG
    const encoding::CharacterStream emptyStream;
    ASSERT_EQ((encoding::decode(emptyStream, 0, 0, decodedVectorDouble)), 0);
    ASSERT_EQ((encoding::decode(emptyStream, 0, 1, decodedVectorDouble)), 0);
#endif

    // Test invalid vector (invalid coefficient)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2, abc");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedVectorDouble)), 0);
#endif
}

//! Check encoding and decoding of SmartVectors
TEST(encoding, SmartVector)
{
    // Initialize helpers
    encoding::CharacterStream stream;
    memory::SmartVector<double, 2> originalVectorDouble{ 1.2345, 2.34, -3.45 };
    memory::SmartVector<double, 2> decodedVectorDouble;
    const encoding::CharacterStreamSize expectedLengthVectorDouble = 3 * 7 + 2;

    // Test encoding
    ASSERT_EQ((encoding::encode(stream, originalVectorDouble, "%+.4f")), expectedLengthVectorDouble);

    // Test decoding
    encoding::CharacterStreamSize currentIndex = 0;
    ASSERT_EQ((encoding::decode(stream, currentIndex, expectedLengthVectorDouble, decodedVectorDouble)), expectedLengthVectorDouble);
    ASSERT_TRUE(decodedVectorDouble == originalVectorDouble);

    // Test encoding (default format)
    const encoding::CharacterStreamSize expectedLengthDefaultFormat = 3 * 23 + 2;
    ASSERT_EQ((encoding::encode(stream, originalVectorDouble)), expectedLengthDefaultFormat);

    // Test empty stream
#ifdef NDEBUG
    const encoding::CharacterStream emptyStream;
    ASSERT_EQ((encoding::decode(emptyStream, 0, 0, decodedVectorDouble)), 0);
    ASSERT_EQ((encoding::decode(emptyStream, 0, 1, decodedVectorDouble)), 0);
#endif

    // Test invalid vector (invalid coefficient)
#ifdef NDEBUG
    stream.clear();
    encoding::encode(stream, "1, 2, abc");
    ASSERT_EQ((encoding::decode(stream, 0, stream.size(), decodedVectorDouble)), 0);
#endif
}

//! Check encoding and decoding with the std::string interface
TEST(encoding, StdStringInterface)
{
    const double valueToEncode = -1.2345;
    const std::string encodedString = encoding::encodeToString(valueToEncode);
    double decodedValue = 0;
    ASSERT_TRUE(encoding::decodeFromString(decodedValue, encodedString));
    ASSERT_FLOAT_EQ(decodedValue, valueToEncode);
}

//! Check encoding and decoding of base64 strings
TEST(encoding, Base64)
{
    // Initialize helpers
    const size_t maxBinaryStreamLength = 1024;
    encoding::CharacterStream sourceBinaryStream, inputBinaryStream, outputBinaryStream;
    sourceBinaryStream.resize(maxBinaryStreamLength);
    for (size_t i = 0; i < sourceBinaryStream.size(); i++)
        sourceBinaryStream[i] = i % 256;

    // Test for different binary stream lengths
    for (size_t i = 0; i < maxBinaryStreamLength; i++) {
        // Test with and without padding
        for (size_t j = 0; j < 2; j++) {
            const bool withPadding = (j == 0) ? true : false;
            inputBinaryStream.clear();
            inputBinaryStream = encoding::CharacterStream(sourceBinaryStream.begin(), sourceBinaryStream.begin() + i);
            const std::string textStream = encoding::encodeToBase64(inputBinaryStream, withPadding);
            outputBinaryStream.clear();
            ASSERT_TRUE(encoding::decodeFromBase64(textStream, outputBinaryStream));
            ASSERT_TRUE(inputBinaryStream == outputBinaryStream);
        }
    }

    // "Manual" tests
    inputBinaryStream.resize(256);
    for (size_t i = 0; i < inputBinaryStream.size(); i++)
        inputBinaryStream[i] = i;
    const std::string textStreamWithPadding = encoding::encodeToBase64(inputBinaryStream, true);
    const std::string expectedStringWithPadding = "AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fn+AgYKDhIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wsbKztLW2t7i5uru8vb6/wMHCw8TFxsfIycrLzM3Oz9DR0tPU1dbX2Nna29zd3t/g4eLj5OXm5+jp6uvs7e7v8PHy8/T19vf4+fr7/P3+/w==";
    ASSERT_EQ(textStreamWithPadding, expectedStringWithPadding);
    const std::string textStreamWithoutPadding = encoding::encodeToBase64(inputBinaryStream, false);
    const std::string expectedStringWithoutPadding = "AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWltcXV5fYGFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6e3x9fn+AgYKDhIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wsbKztLW2t7i5uru8vb6/wMHCw8TFxsfIycrLzM3Oz9DR0tPU1dbX2Nna29zd3t/g4eLj5OXm5+jp6uvs7e7v8PHy8/T19vf4+fr7/P3+/w";
    ASSERT_EQ(textStreamWithoutPadding, expectedStringWithoutPadding);

    // Test decoding of invalid string
#ifdef NDEBUG
    // Setup input
    inputBinaryStream.resize(10);
    for (size_t i = 0; i < inputBinaryStream.size(); i++)
        inputBinaryStream[i] = i;
    const std::string testStreamValidWithPadding = encoding::encodeToBase64(inputBinaryStream, true); // "AAECAwQFBgcICQ=="
    const std::string testStreamValidWithoutPadding = encoding::encodeToBase64(inputBinaryStream, false); // "AAECAwQFBgcICQ"

    // Test invalid character count
    outputBinaryStream.clear();
    std::string textStreamInvalid = testStreamValidWithPadding;
    textStreamInvalid.resize(textStreamInvalid.size() - 3);
    ASSERT_FALSE(encoding::decodeFromBase64(textStreamInvalid, outputBinaryStream));
    outputBinaryStream.clear();
    textStreamInvalid = testStreamValidWithoutPadding;
    textStreamInvalid.resize(textStreamInvalid.size() - 1);
    ASSERT_FALSE(encoding::decodeFromBase64(textStreamInvalid, outputBinaryStream));

    // Test invalid character
    for (int i = 0; i < 4; i++) {
        outputBinaryStream.clear();
        textStreamInvalid = testStreamValidWithPadding;
        textStreamInvalid[i] = (char)'?';
        ASSERT_FALSE(encoding::decodeFromBase64(textStreamInvalid, outputBinaryStream));
    }
#endif
}
