/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/logging/Logger.hpp"
#include "broccoli/io/logging/LogFileDataArray.hpp"
#include "broccoli/io/logging/LogFileDataString.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Test string representation of types
TEST(Logger, TypesToString)
{
    // Just check, if string is non-empty
    LogFileSignal testSignal;
    LoggerAction testAction;
    ASSERT_GT(testSignal.typeString().size(), 0);
    ASSERT_GT(testSignal.messageString().size(), 0);
    ASSERT_GT(testAction.typeString().size(), 0);
}

//! Test setters and getters
TEST(Logger, SettersAndGetters)
{
    // Logger
    // ------
    // Create instance
    Logger logger("logger", false, 0, "test_logger");

    // Setters and getters
    std::string firstLevelDirectory = "A";
    logger.setFirstLevelDirectory(firstLevelDirectory);
    ASSERT_EQ(firstLevelDirectory, logger.firstLevelDirectory());

    // Getters only
    std::vector<LogFileBase*> trackedFiles = logger.trackedFiles();
    (void)trackedFiles;

    // LogFile
    // -------
    // Create instance
    LogFile<LogFileDataString> logfile("logfile", "", "logfile", ".log", false, false, true, 3);

    // Setters and getters
    std::string desiredTargetDirectory = "A";
    logfile.setDesiredTargetDirectory(desiredTargetDirectory);
    ASSERT_EQ(desiredTargetDirectory, logfile.desiredTargetDirectory());
    std::string desiredFileName = "A";
    logfile.setDesiredFileName(desiredFileName);
    ASSERT_EQ(desiredFileName, logfile.desiredFileName());
    std::string desiredFileExtension = "A";
    logfile.setDesiredFileExtension(desiredFileExtension);
    ASSERT_EQ(desiredFileExtension, logfile.desiredFileExtension());
    bool desiredAddTimeStampToName = true;
    logfile.setDesiredAddTimeStampToName(desiredAddTimeStampToName);
    ASSERT_EQ(desiredAddTimeStampToName, logfile.desiredAddTimeStampToName());
    bool desiredCompressed = true;
    logfile.setDesiredCompressed(desiredCompressed);
    ASSERT_EQ(desiredCompressed, logfile.desiredCompressed());
    bool active = true;
    logfile.setActive(active);
    ASSERT_EQ(active, logfile.active());
    bool enableSkipping = !logfile.enableSkipping();
    logfile.setEnableSkipping(enableSkipping);
    ASSERT_EQ(enableSkipping, logfile.enableSkipping());
    uint64_t countOfObjectsToSkip = logfile.countOfObjectsToSkip() + 1;
    logfile.setCountOfObjectsToSkip(countOfObjectsToSkip);
    ASSERT_EQ(countOfObjectsToSkip, logfile.countOfObjectsToSkip());
    bool enableFlush = !logfile.enableFlush();
    logfile.setEnableFlush(enableFlush);
    ASSERT_EQ(enableFlush, logfile.enableFlush());
    uint64_t flushPacketSize = logfile.flushPacketSize() + 1;
    logfile.setFlushPacketSize(flushPacketSize);
    ASSERT_EQ(flushPacketSize, logfile.flushPacketSize());

    // Getters only
    std::string identifier = logfile.identifier();
    ASSERT_EQ(identifier, "logfile");
    std::string targetDirectory = logfile.targetDirectory();
    ASSERT_EQ(targetDirectory, "");
    std::string fileName = logfile.fileName();
    ASSERT_EQ(fileName, "logfile");
    std::string fileExtension = logfile.fileExtension();
    ASSERT_EQ(fileExtension, ".log");
    bool addTimeStampToName = logfile.addTimeStampToName();
    ASSERT_EQ(addTimeStampToName, false);
    bool compressed = logfile.compressed();
    ASSERT_EQ(compressed, false);
    uint64_t totalReceivedObjectCount = logfile.totalReceivedObjectCount();
    ASSERT_EQ(totalReceivedObjectCount, 0);
    uint64_t totalWrittenObjectCount = logfile.totalWrittenObjectCount();
    ASSERT_EQ(totalWrittenObjectCount, 0);
    uint64_t countOfObjectsInBuffer = logfile.countOfObjectsInBuffer();
    ASSERT_EQ(countOfObjectsInBuffer, 0);
}

//! Check logging of string data
TEST(Logger, StringData)
{
    // Create log files
    LogFile<LogFileDataString> logfileX("logfileX", "", "logfileX", ".log", false, false, true, 3);
    LogFile<LogFileDataString> logfileY("logfileY", "", "logfileY", ".log", true, true, true, 3);

    // Create log data
    LogFileDataString dataA("[DataA]");
    LogFileDataString dataB("[DataB]");
    LogFileDataString dataC("[DataC]");

    // Create logger
    Logger logger("logger", false, 0, "test_logger");

    // Copy constructor
    Logger loggerCopy(logger);
    ASSERT_TRUE(loggerCopy.firstLevelDirectory() == logger.firstLevelDirectory());

    // Assignment operator
    loggerCopy.setFirstLevelDirectory("abc");
    loggerCopy = logger;
    ASSERT_TRUE(loggerCopy.firstLevelDirectory() == logger.firstLevelDirectory());

    // Add log files to logger
    ASSERT_EQ(logger.addFile(&logfileX), true);
    ASSERT_EQ(logger.addFile(&logfileY), true);
    ASSERT_EQ(logger.addFile(&logfileX), false); // Adding same file again

    // Get file pointers
    ASSERT_EQ(logger.getFile(logfileX.identifier()), &logfileX);
    ASSERT_EQ(logger.getFile(logfileY.identifier()), &logfileY);
    ASSERT_EQ(logger.getFile("unknown"), nullptr);

    // Remove file
    ASSERT_EQ(logger.removeFile(logfileX.identifier()), true);
    ASSERT_EQ(logger.removeFile(logfileX.identifier()), false);

    // Re-add file
    ASSERT_EQ(logger.addFile(&logfileX), true);

    // Initialize logger
    ASSERT_EQ(logger.initialize(), true);

    // Trigger start of log
    LoggerAction startAction(LoggerAction::Type::LOG_START, "StringData " + core::Time::currentTime().encodeToDateTimeString(true));
    logger.m_actionBuffer.push(startAction);

    // Trigger execution of logger
    logger.triggerManualExecution();

    // Add data to the logfiles
    ASSERT_EQ(logfileX.addNewDataToBuffer(dataA), true);
    ASSERT_EQ(logfileX.addNewDataToBuffer(dataB), true);
    ASSERT_EQ(logfileX.addNewDataToBuffer(dataC), true);
    ASSERT_EQ(logfileY.addNewDataToBuffer(dataA), true);
    ASSERT_EQ(logfileY.addNewDataToBuffer(dataB), true);
    ASSERT_EQ(logfileY.addNewDataToBuffer(dataC), true);

    // Trigger execution of logger
    logger.triggerManualExecution();

    // Trigger stop of logger
    LoggerAction stopAction(LoggerAction::Type::LOG_STOP);
    logger.m_actionBuffer.push(stopAction);

    // Add data to the logfiles
    ASSERT_EQ(logfileX.addNewDataToBuffer(dataA), true);
    ASSERT_EQ(logfileY.addNewDataToBuffer(dataB), true);

    // Trigger log dump
    LoggerAction dumpAction(LoggerAction::Type::LOG_DUMP, "StringData DUMP " + core::Time::currentTime().encodeToDateTimeString(true));
    logger.m_actionBuffer.push(dumpAction);

    // Trigger execution of logger
    logger.triggerManualExecution();

    // Test proper shutdown
    ASSERT_EQ(logger.deInitialize(), true);
}

//! Check logging of array data
TEST(Logger, ArrayData)
{
    // Create log files
    LogFile<LogFileDataArray<double, 3>> logfileX("logfileX", "", "logfileX", ".log", false, false, true, 3);

    // Create log data
    LogFileDataArray<double, 3> dataA;

    // Fill up with data
    dataA.m_data.fill(1.23456);

    // Check values
    ASSERT_EQ(dataA.m_data[0], 1.23456);
    ASSERT_EQ(dataA.m_data[1], 1.23456);
    ASSERT_EQ(dataA.m_data[2], 1.23456);

    // Create logger
    Logger logger("logger", false, 0, "test_logger");

    // Add log files to logger
    ASSERT_EQ(logger.addFile(&logfileX), true);

    // Initialize logger
    ASSERT_EQ(logger.initialize(), true);

    // Trigger start of log
    LoggerAction startAction(LoggerAction::Type::LOG_START, "ArrayData " + core::Time::currentTime().encodeToDateTimeString(true));
    logger.m_actionBuffer.push(startAction);

    // Trigger execution of logger
    logger.triggerManualExecution();

    // Add data to the logfiles
    ASSERT_EQ(logfileX.addNewDataToBuffer(dataA), true);
    ASSERT_EQ(logfileX.addNewDataToBuffer(dataA), true);
    ASSERT_EQ(logfileX.addNewDataToBuffer(dataA), true);

    // Trigger execution of logger
    logger.triggerManualExecution();

    // Trigger stop of logger
    LoggerAction stopAction(LoggerAction::Type::LOG_STOP);
    logger.m_actionBuffer.push(stopAction);

    // Test proper shutdown
    ASSERT_EQ(logger.deInitialize(), true);
}

//! Test string specification of action types
TEST(LoggerAction, ActionsToString)
{
    uint8_t i = 0;
    LoggerAction testAction;

    for (i = 0; i < static_cast<uint8_t>(LoggerAction::Type::TYPE_COUNT); i++) {
        testAction.m_type = static_cast<LoggerAction::Type>(i);
        ASSERT_GT(testAction.typeString().size(), 0);
        ASSERT_TRUE(testAction.typeString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(LoggerAction::Type::TYPE_COUNT);
    testAction.m_type = static_cast<LoggerAction::Type>(i);
    ASSERT_TRUE(testAction.typeString() == "UNKNOWN");
#endif
}

//! Test string representation of types
TEST(LogFileSignal, TypesToString)
{
    uint8_t i = 0;
    LogFileSignal testSignal;

    for (i = 0; i < static_cast<uint8_t>(LogFileSignal::Type::TYPE_COUNT); i++) {
        testSignal.m_type = static_cast<LogFileSignal::Type>(i);
        ASSERT_GT(testSignal.typeString().size(), 0);
        ASSERT_TRUE(testSignal.typeString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(LogFileSignal::Type::TYPE_COUNT);
    testSignal.m_type = static_cast<LogFileSignal::Type>(i);
    ASSERT_TRUE(testSignal.typeString() == "UNKNOWN");
#endif
}

//! Test string representation of the given message
TEST(LogFileSignal, MessagesToString)
{
    uint8_t i = 0;
    LogFileSignal testMessage;

    for (i = 0; i < static_cast<uint8_t>(LogFileSignal::Message::MESSAGE_COUNT); i++) {
        testMessage.m_message = static_cast<LogFileSignal::Message>(i);
        ASSERT_GT(testMessage.messageString().size(), 0);
        ASSERT_TRUE(testMessage.messageString() != "UNKNOWN");
    }

#ifdef NDEBUG
    i = static_cast<uint8_t>(LogFileSignal::Message::MESSAGE_COUNT);
    testMessage.m_message = static_cast<LogFileSignal::Message>(i);
    ASSERT_TRUE(testMessage.messageString() == "UNKNOWN");
#endif
}
