/*
 * This file is part of broccoli
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

#include "broccoli/io/serialization/serialization.hpp"
#include "gtest/gtest.h"

//! Template function to test serialization of SerializableData objects
/*!
 * \tparam T The type of the object (derived from SerializableData)
 *
 * \param [in] input The input data instance to be serialized (filled with non-default data)
 * \param [in] expectedStreamLength Expected length of the serialized stream (use 0 if not known)
 * \param [in] prototype A prototype instance to initialize the output object
 */
template <class T>
inline void testSerializableData(const T& input, const broccoli::io::serialization::BinaryStreamSize& expectedStreamLength, const T& prototype = T())
{
    using namespace broccoli::io::serialization;

    // Test little- and big-endian versions
    for (int e = 0; e < 2; e++) {
        Endianness endianness = Endianness::LITTLE;
        if (e == 1)
            endianness = Endianness::BIG;

        // Initialize helpers
        BinaryStream stream;
        T output = prototype;

        // Check inequality
        ASSERT_TRUE(output != input);

        // Serialize
        const auto inputStreamSize = serialize(stream, endianness, input);
        ASSERT_EQ(inputStreamSize, stream.size());
        if (expectedStreamLength > 0)
            ASSERT_EQ(stream.size(), expectedStreamLength);

        // Deserialize
        const auto outputStreamSize = deSerialize(stream, 0, endianness, output);
        ASSERT_EQ(outputStreamSize, stream.size());

        // Check equality
        ASSERT_TRUE(output == input);
    }
}
