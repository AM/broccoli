/*
 * This file is part of broccoli
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#include "broccoli/io/serialization/serialization.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;

//! Check concatenation of binary streams
TEST(serialization, StreamConcatenation)
{
    // Initialize helpers
    serialization::BinaryStream stream({ 'a', 'b', 'c' });
    serialization::BinaryStream streamToAppend({ 'd', 'e', 'f' });

    // Concatenate
    serialization::BinaryStreamSize addedElements = serialization::append(stream, streamToAppend);

    // Check value
    ASSERT_TRUE(stream == serialization::BinaryStream({ 'a', 'b', 'c', 'd', 'e', 'f' }));

    // Check stream sizes
    ASSERT_TRUE(addedElements == streamToAppend.size());
}

//! Template for serialization and deserialization of various datatypes
template <class T>
void TestBasicDatatype(const T& value)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        T outputValue = T();

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, value);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == value);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());
        ASSERT_TRUE(outputStreamSize == sizeof(T));

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
#endif
    }
}

//! Check serialization and deserialization of basic datatypes
TEST(serialization, BasicDatatypes)
{
    // Booleans
    TestBasicDatatype<bool>(false);
    TestBasicDatatype<bool>(true);

    // Unsigned integers
    TestBasicDatatype<uint8_t>(0);
    TestBasicDatatype<uint8_t>(120);
    TestBasicDatatype<uint8_t>(255);
    TestBasicDatatype<uint16_t>(0);
    TestBasicDatatype<uint16_t>(30000);
    TestBasicDatatype<uint16_t>(65535);
    TestBasicDatatype<uint32_t>(0UL);
    TestBasicDatatype<uint32_t>(1111111111UL);
    TestBasicDatatype<uint32_t>(4294967295UL);
    TestBasicDatatype<uint64_t>(0ULL);
    TestBasicDatatype<uint64_t>(123456789123457869ULL);
    TestBasicDatatype<uint64_t>(18446744073709551614ULL);

    // Signed integers
    TestBasicDatatype<int8_t>(-127);
    TestBasicDatatype<int8_t>(-60);
    TestBasicDatatype<int8_t>(0);
    TestBasicDatatype<int8_t>(60);
    TestBasicDatatype<int8_t>(127);
    TestBasicDatatype<int16_t>(-32767);
    TestBasicDatatype<int16_t>(-3000);
    TestBasicDatatype<int16_t>(0);
    TestBasicDatatype<int16_t>(3000);
    TestBasicDatatype<int16_t>(32767);
    TestBasicDatatype<int32_t>(-2147483647L);
    TestBasicDatatype<int32_t>(-1111111L);
    TestBasicDatatype<int32_t>(0L);
    TestBasicDatatype<int32_t>(1111111L);
    TestBasicDatatype<int32_t>(2147483647L);
    TestBasicDatatype<int64_t>(-9223372036854775807LL);
    TestBasicDatatype<int64_t>(-123456789457869LL);
    TestBasicDatatype<int64_t>(0LL);
    TestBasicDatatype<int64_t>(123456789457869LL);
    TestBasicDatatype<int64_t>(9223372036854775807LL);

    // Floating point numbers
    TestBasicDatatype<float>(-12.345);
    TestBasicDatatype<float>(0);
    TestBasicDatatype<float>(12.345);
    TestBasicDatatype<double>(-12.3456789123);
    TestBasicDatatype<double>(0);
    TestBasicDatatype<double>(12.3456789123);
}

//! Check serialization and deserialization with different endianness
TEST(serialization, Endianness)
{
    // Initialize helpers
    serialization::BinaryStream streamBigEndian, streamLittleEndian;

    // Test bool
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (bool)true);
    ASSERT_EQ(streamBigEndian.size(), 1);
    ASSERT_EQ(streamBigEndian[0], 0x01);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (bool)false);
    ASSERT_EQ(streamLittleEndian.size(), 1);
    ASSERT_EQ(streamLittleEndian[0], 0x00);

    // Test uint8_t
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (uint8_t)0x0F);
    ASSERT_EQ(streamBigEndian.size(), 1);
    ASSERT_EQ(streamBigEndian[0], 0x0F);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (uint8_t)0x0F);
    ASSERT_EQ(streamLittleEndian.size(), 1);
    ASSERT_EQ(streamLittleEndian[0], 0x0F);

    // Test uint16_t
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (uint16_t)0x0102);
    ASSERT_EQ(streamBigEndian.size(), 2);
    ASSERT_EQ(streamBigEndian[0], 0x01);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (uint16_t)0x0102);
    ASSERT_EQ(streamLittleEndian.size(), 2);
    ASSERT_EQ(streamLittleEndian[0], 0x02);
    ASSERT_EQ(streamLittleEndian[1], 0x01);

    // Test uint32_t
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (uint32_t)0x01020304);
    ASSERT_EQ(streamBigEndian.size(), 4);
    ASSERT_EQ(streamBigEndian[0], 0x01);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    ASSERT_EQ(streamBigEndian[2], 0x03);
    ASSERT_EQ(streamBigEndian[3], 0x04);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (uint32_t)0x01020304);
    ASSERT_EQ(streamLittleEndian.size(), 4);
    ASSERT_EQ(streamLittleEndian[0], 0x04);
    ASSERT_EQ(streamLittleEndian[1], 0x03);
    ASSERT_EQ(streamLittleEndian[2], 0x02);
    ASSERT_EQ(streamLittleEndian[3], 0x01);

    // Test uint64_t
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (uint64_t)0x0102030405060708);
    ASSERT_EQ(streamBigEndian.size(), 8);
    ASSERT_EQ(streamBigEndian[0], 0x01);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    ASSERT_EQ(streamBigEndian[2], 0x03);
    ASSERT_EQ(streamBigEndian[3], 0x04);
    ASSERT_EQ(streamBigEndian[4], 0x05);
    ASSERT_EQ(streamBigEndian[5], 0x06);
    ASSERT_EQ(streamBigEndian[6], 0x07);
    ASSERT_EQ(streamBigEndian[7], 0x08);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (uint64_t)0x0102030405060708);
    ASSERT_EQ(streamLittleEndian.size(), 8);
    ASSERT_EQ(streamLittleEndian[0], 0x08);
    ASSERT_EQ(streamLittleEndian[1], 0x07);
    ASSERT_EQ(streamLittleEndian[2], 0x06);
    ASSERT_EQ(streamLittleEndian[3], 0x05);
    ASSERT_EQ(streamLittleEndian[4], 0x04);
    ASSERT_EQ(streamLittleEndian[5], 0x03);
    ASSERT_EQ(streamLittleEndian[6], 0x02);
    ASSERT_EQ(streamLittleEndian[7], 0x01);

    // Test int8_t (positive)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int8_t)0x0F);
    ASSERT_EQ(streamBigEndian.size(), 1);
    ASSERT_EQ(streamBigEndian[0], 0x0F);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int8_t)0x0F);
    ASSERT_EQ(streamLittleEndian.size(), 1);
    ASSERT_EQ(streamLittleEndian[0], 0x0F);

    // Test int8_t (negative)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int8_t)-0x0F);
    ASSERT_EQ(streamBigEndian.size(), 1);
    ASSERT_EQ(streamBigEndian[0], 0x8F);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int8_t)-0x0F);
    ASSERT_EQ(streamLittleEndian.size(), 1);
    ASSERT_EQ(streamLittleEndian[0], 0x8F);

    // Test int16_t (positive)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int16_t)0x0102);
    ASSERT_EQ(streamBigEndian.size(), 2);
    ASSERT_EQ(streamBigEndian[0], 0x01);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int16_t)0x0102);
    ASSERT_EQ(streamLittleEndian.size(), 2);
    ASSERT_EQ(streamLittleEndian[0], 0x02);
    ASSERT_EQ(streamLittleEndian[1], 0x01);

    // Test int16_t (negative)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int16_t)-0x0102);
    ASSERT_EQ(streamBigEndian.size(), 2);
    ASSERT_EQ(streamBigEndian[0], 0x81);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int16_t)-0x0102);
    ASSERT_EQ(streamLittleEndian.size(), 2);
    ASSERT_EQ(streamLittleEndian[0], 0x02);
    ASSERT_EQ(streamLittleEndian[1], 0x81);

    // Test int32_t (positive)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int32_t)0x01020304);
    ASSERT_EQ(streamBigEndian.size(), 4);
    ASSERT_EQ(streamBigEndian[0], 0x01);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    ASSERT_EQ(streamBigEndian[2], 0x03);
    ASSERT_EQ(streamBigEndian[3], 0x04);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int32_t)0x01020304);
    ASSERT_EQ(streamLittleEndian.size(), 4);
    ASSERT_EQ(streamLittleEndian[0], 0x04);
    ASSERT_EQ(streamLittleEndian[1], 0x03);
    ASSERT_EQ(streamLittleEndian[2], 0x02);
    ASSERT_EQ(streamLittleEndian[3], 0x01);

    // Test int32_t (negative)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int32_t)-0x01020304);
    ASSERT_EQ(streamBigEndian.size(), 4);
    ASSERT_EQ(streamBigEndian[0], 0x81);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    ASSERT_EQ(streamBigEndian[2], 0x03);
    ASSERT_EQ(streamBigEndian[3], 0x04);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int32_t)-0x01020304);
    ASSERT_EQ(streamLittleEndian.size(), 4);
    ASSERT_EQ(streamLittleEndian[0], 0x04);
    ASSERT_EQ(streamLittleEndian[1], 0x03);
    ASSERT_EQ(streamLittleEndian[2], 0x02);
    ASSERT_EQ(streamLittleEndian[3], 0x81);

    // Test int64_t (positive)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int64_t)0x0102030405060708);
    ASSERT_EQ(streamBigEndian.size(), 8);
    ASSERT_EQ(streamBigEndian[0], 0x01);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    ASSERT_EQ(streamBigEndian[2], 0x03);
    ASSERT_EQ(streamBigEndian[3], 0x04);
    ASSERT_EQ(streamBigEndian[4], 0x05);
    ASSERT_EQ(streamBigEndian[5], 0x06);
    ASSERT_EQ(streamBigEndian[6], 0x07);
    ASSERT_EQ(streamBigEndian[7], 0x08);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int64_t)0x0102030405060708);
    ASSERT_EQ(streamLittleEndian.size(), 8);
    ASSERT_EQ(streamLittleEndian[0], 0x08);
    ASSERT_EQ(streamLittleEndian[1], 0x07);
    ASSERT_EQ(streamLittleEndian[2], 0x06);
    ASSERT_EQ(streamLittleEndian[3], 0x05);
    ASSERT_EQ(streamLittleEndian[4], 0x04);
    ASSERT_EQ(streamLittleEndian[5], 0x03);
    ASSERT_EQ(streamLittleEndian[6], 0x02);
    ASSERT_EQ(streamLittleEndian[7], 0x01);

    // Test int64_t (negative)
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (int64_t)-0x0102030405060708);
    ASSERT_EQ(streamBigEndian.size(), 8);
    ASSERT_EQ(streamBigEndian[0], 0x81);
    ASSERT_EQ(streamBigEndian[1], 0x02);
    ASSERT_EQ(streamBigEndian[2], 0x03);
    ASSERT_EQ(streamBigEndian[3], 0x04);
    ASSERT_EQ(streamBigEndian[4], 0x05);
    ASSERT_EQ(streamBigEndian[5], 0x06);
    ASSERT_EQ(streamBigEndian[6], 0x07);
    ASSERT_EQ(streamBigEndian[7], 0x08);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (int64_t)-0x0102030405060708);
    ASSERT_EQ(streamLittleEndian.size(), 8);
    ASSERT_EQ(streamLittleEndian[0], 0x08);
    ASSERT_EQ(streamLittleEndian[1], 0x07);
    ASSERT_EQ(streamLittleEndian[2], 0x06);
    ASSERT_EQ(streamLittleEndian[3], 0x05);
    ASSERT_EQ(streamLittleEndian[4], 0x04);
    ASSERT_EQ(streamLittleEndian[5], 0x03);
    ASSERT_EQ(streamLittleEndian[6], 0x02);
    ASSERT_EQ(streamLittleEndian[7], 0x81);

    // Test float
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (float)1.234567);
    ASSERT_EQ(streamBigEndian.size(), 4);
    ASSERT_EQ(streamBigEndian[0], 0x3F);
    ASSERT_EQ(streamBigEndian[1], 0x9E);
    ASSERT_EQ(streamBigEndian[2], 0x06);
    ASSERT_EQ(streamBigEndian[3], 0x4B);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (float)1.234567);
    ASSERT_EQ(streamLittleEndian.size(), 4);
    ASSERT_EQ(streamLittleEndian[0], 0x4B);
    ASSERT_EQ(streamLittleEndian[1], 0x06);
    ASSERT_EQ(streamLittleEndian[2], 0x9E);
    ASSERT_EQ(streamLittleEndian[3], 0x3F);

    // Test double
    streamBigEndian.clear();
    streamLittleEndian.clear();
    serialization::serialize(streamBigEndian, serialization::Endianness::BIG, (double)1.23456789);
    ASSERT_EQ(streamBigEndian.size(), 8);
    ASSERT_EQ(streamBigEndian[0], 0x3F);
    ASSERT_EQ(streamBigEndian[1], 0xF3);
    ASSERT_EQ(streamBigEndian[2], 0xC0);
    ASSERT_EQ(streamBigEndian[3], 0xCA);
    ASSERT_EQ(streamBigEndian[4], 0x42);
    ASSERT_EQ(streamBigEndian[5], 0x83);
    ASSERT_EQ(streamBigEndian[6], 0xDE);
    ASSERT_EQ(streamBigEndian[7], 0x1B);
    serialization::serialize(streamLittleEndian, serialization::Endianness::LITTLE, (double)1.23456789);
    ASSERT_EQ(streamLittleEndian.size(), 8);
    ASSERT_EQ(streamLittleEndian[0], 0x1B);
    ASSERT_EQ(streamLittleEndian[1], 0xDE);
    ASSERT_EQ(streamLittleEndian[2], 0x83);
    ASSERT_EQ(streamLittleEndian[3], 0x42);
    ASSERT_EQ(streamLittleEndian[4], 0xCA);
    ASSERT_EQ(streamLittleEndian[5], 0xC0);
    ASSERT_EQ(streamLittleEndian[6], 0xF3);
    ASSERT_EQ(streamLittleEndian[7], 0x3F);
}

//! Check serialization and deserialization of times
TEST(serialization, Time)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        core::Time inputValue(1, -23456789);
        core::Time outputValue(0, 0);

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
#endif
    }
}

//! Check serialization and deserialization of strings
TEST(serialization, String)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        std::string inputValue = "asdf";
        std::string outputValue = "";

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream invalidStream;
        serialization::serialize(invalidStream, endianness, (serialization::BinaryStreamSize)1); // Only add header, but skip payload
        ASSERT_EQ(serialization::deSerialize(invalidStream, 0, endianness, outputValue), 0);
#endif
    }
}

//! Check serialization and deserialization of arrays
TEST(serialization, Array)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        std::array<double, 3> inputValue({ { 0.123, -0.123, 12.34 } });
        std::array<double, 3> outputValue({ { 0, 0, 0 } });

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (serialization::BinaryStreamSize)1);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputValue), 0);
#endif
    }
}

//! Check serialization and deserialization of vectors
TEST(serialization, Vector)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        std::vector<double> inputValue{ 0.123, -0.123, 12.34 };
        std::vector<double> outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (serialization::BinaryStreamSize)1);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputValue), 0);
#endif
    }
}

//! Check serialization and deserialization of SmartVectors
TEST(serialization, SmartVector)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        memory::SmartVector<double, 2> inputValue{ 0.123, -0.123, 12.34 };
        memory::SmartVector<double, 2> outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (serialization::BinaryStreamSize)1);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputValue), 0);
#endif
    }
}
//! Check serialization and deserialization of MultiVectors
TEST(serialization, MultiVector)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        memory::MultiVector<double, 2> inputValue(5, 6);
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 6; j++)
                inputValue(i, j) = i + 0.1 * j;
        memory::MultiVector<double, 2> outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (serialization::BinaryStreamSize)1);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputValue), 0);
#endif
    }
}

//! Check serialization and deserialization of MultiLevelGrids
TEST(serialization, MultiLevelGrid)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        memory::MultiLevelGrid<2, 3, double> inputValue(5, 6);
        for (int l = 0; l < 3; l++)
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 6; j++)
                    inputValue(l, i, j) = -0.123 * l + i + 0.1 * j;
        memory::MultiLevelGrid<2, 3, double> outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (serialization::BinaryStreamSize)1);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputValue), 0);
#endif
    }
}

#ifdef HAVE_EIGEN3
//! Check serialization and deserialization of matrices
TEST(serialization, Matrix)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSizeFixed, outputStreamSizeFixed;
        serialization::BinaryStreamSize inputStreamSizeDynamic, outputStreamSizeDynamic;
        Eigen::Matrix<double, 2, 3> inputValueFixed;
        inputValueFixed << 1.23, -2.34, 3.45, 0.12, -9.87, 5.64;
        Eigen::Matrix<double, 2, 3> outputValueFixed;
        Eigen::MatrixXd inputValueDynamic;
        inputValueDynamic.resize(2, 3);
        inputValueDynamic << 1.23, 2.34, 3.45, -5.43, -4.32, -3.21;
        Eigen::MatrixXd outputValueDynamic;

        // Serialize
        inputStreamSizeFixed = serialization::serialize(stream, endianness, inputValueFixed);
        inputStreamSizeDynamic = serialization::serialize(stream, endianness, inputValueDynamic);

        // Deserialize
        outputStreamSizeFixed = serialization::deSerialize(stream, 0, endianness, outputValueFixed);
        outputStreamSizeDynamic = serialization::deSerialize(stream, inputStreamSizeFixed, endianness, outputValueDynamic);

        // Check value
        ASSERT_TRUE(outputValueFixed == inputValueFixed);
        ASSERT_TRUE(outputValueDynamic == inputValueDynamic);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSizeFixed == inputStreamSizeFixed);
        ASSERT_TRUE(outputStreamSizeDynamic == inputStreamSizeDynamic);
        ASSERT_TRUE(outputStreamSizeFixed + outputStreamSizeDynamic == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValueFixed), 0);
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, inputValueDynamic), 0);
        serialization::BinaryStream brokenStreamOnlyRows;
        serialization::serialize(brokenStreamOnlyRows, endianness, (serialization::BinaryStreamSize)2);
        ASSERT_EQ(serialization::deSerialize(brokenStreamOnlyRows, 0, endianness, outputValueFixed), 0);
        ASSERT_EQ(serialization::deSerialize(brokenStreamOnlyRows, 0, endianness, inputValueDynamic), 0);
        serialization::BinaryStream brokenStreamNoCoefficients;
        serialization::serialize(brokenStreamNoCoefficients, endianness, (serialization::BinaryStreamSize)2);
        serialization::serialize(brokenStreamNoCoefficients, endianness, (serialization::BinaryStreamSize)2);
        ASSERT_EQ(serialization::deSerialize(brokenStreamNoCoefficients, 0, endianness, outputValueFixed), 0);
        ASSERT_EQ(serialization::deSerialize(brokenStreamNoCoefficients, 0, endianness, inputValueDynamic), 0);
#endif
    }
}

//! Helper function to check serialization and deserialization of array-like containers with matrices as elements
template <typename ArrayLikeType>
void checkMatrixArrayLike(ArrayLikeType& arrayLike, const size_t& elementCount)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Fill container
        for (size_t i = 0; i < elementCount; i++) {
            arrayLike[i].resize(2, i + 1);
            for (int j = 0; j < arrayLike[i].rows(); j++)
                for (int k = 0; k < arrayLike[i].cols(); k++)
                    arrayLike[i](j, k) = 1.23 * i - 2.34 * j + 4.56 * k;
        }

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        ArrayLikeType& inputValue = arrayLike;
        ArrayLikeType outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check sizes
        ASSERT_TRUE(outputValue.size() == inputValue.size());

        // Check values
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());
    }
}

//! Check serialization and deserialization of an array of matrices
TEST(serialization, MatrixArray)
{
    std::array<Eigen::MatrixXd, 0> arrayA;
    checkMatrixArrayLike(arrayA, 0);
    std::array<Eigen::MatrixXd, 1> arrayB;
    checkMatrixArrayLike(arrayB, 1);
    std::array<Eigen::MatrixXd, 2> arrayC;
    checkMatrixArrayLike(arrayC, 2);
}

//! Check serialization and deserialization of a vector of matrices
TEST(serialization, MatrixVector)
{
    std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> vectorA;
    vectorA.resize(0);
    checkMatrixArrayLike(vectorA, 0);
    std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> vectorB;
    vectorB.resize(1);
    checkMatrixArrayLike(vectorB, 1);
    std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd>> vectorC;
    vectorC.resize(2);
    checkMatrixArrayLike(vectorC, 2);
}

//! Check serialization and deserialization of a SmartVector of matrices
TEST(serialization, MatrixSmartVector)
{
    memory::SmartVector<Eigen::MatrixXd, 1, Eigen::aligned_allocator<Eigen::MatrixXd>> vectorA;
    vectorA.resize(0);
    checkMatrixArrayLike(vectorA, 0);
    memory::SmartVector<Eigen::MatrixXd, 1, Eigen::aligned_allocator<Eigen::MatrixXd>> vectorB;
    vectorB.resize(1);
    checkMatrixArrayLike(vectorB, 1);
    memory::SmartVector<Eigen::MatrixXd, 1, Eigen::aligned_allocator<Eigen::MatrixXd>> vectorC;
    vectorC.resize(2);
    checkMatrixArrayLike(vectorC, 2);
}

//! Check serialization and deserialization of quaternions
TEST(serialization, Quaternion)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        Eigen::Quaterniond inputValue(1.23, -2.34, 3.45, 0.12);
        Eigen::Quaterniond outputValue(0.0, 0.0, 0.0, 0.0);

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue.coeffs() == inputValue.coeffs());

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (double)1.23);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputValue), 0);
#endif
    }
}

//! Helper function to check serialization and deserialization of array-like containers with quaternions as elements
template <typename ArrayLikeType>
void checkQuaternionArrayLike(ArrayLikeType& arrayLike, const size_t& elementCount)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Fill container
        for (size_t i = 0; i < elementCount; i++)
            arrayLike[i] = Eigen::Quaterniond(1.23 * elementCount, -2.34 + elementCount, 3.45 * elementCount, -5.64 - elementCount);

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        ArrayLikeType& inputValue = arrayLike;
        ArrayLikeType outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check sizes
        ASSERT_TRUE(outputValue.size() == inputValue.size());

        // Check elements
        for (size_t i = 0; i < elementCount; i++)
            ASSERT_TRUE(outputValue[i].coeffs() == inputValue[i].coeffs());

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());
    }
}

//! Check serialization and deserialization of an array of quaternions
TEST(serialization, QuaternionArray)
{
    std::array<Eigen::Quaterniond, 0> arrayA;
    checkQuaternionArrayLike(arrayA, 0);
    std::array<Eigen::Quaterniond, 1> arrayB;
    checkQuaternionArrayLike(arrayB, 1);
    std::array<Eigen::Quaterniond, 2> arrayC;
    checkQuaternionArrayLike(arrayC, 2);
}

//! Check serialization and deserialization of a vector of quaternions
TEST(serialization, QuaternionVector)
{
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> vectorA;
    vectorA.resize(0);
    checkQuaternionArrayLike(vectorA, 0);
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> vectorB;
    vectorB.resize(1);
    checkQuaternionArrayLike(vectorB, 1);
    std::vector<Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond>> vectorC;
    vectorC.resize(2);
    checkQuaternionArrayLike(vectorC, 2);
}

//! Check serialization and deserialization of a SmartVector of quaternions
TEST(serialization, QuaternionSmartVector)
{
    memory::SmartVector<Eigen::Quaterniond, 1, Eigen::aligned_allocator<Eigen::Quaterniond>> vectorA;
    vectorA.resize(0);
    checkQuaternionArrayLike(vectorA, 0);
    memory::SmartVector<Eigen::Quaterniond, 1, Eigen::aligned_allocator<Eigen::Quaterniond>> vectorB;
    vectorB.resize(1);
    checkQuaternionArrayLike(vectorB, 1);
    memory::SmartVector<Eigen::Quaterniond, 1, Eigen::aligned_allocator<Eigen::Quaterniond>> vectorC;
    vectorC.resize(2);
    checkQuaternionArrayLike(vectorC, 2);
}
#endif // HAVE_EIGEN3

//! Check serialization and deserialization of tuples of values
TEST(serialization, Tuples)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Serialize tuple of values
        int a = 2;
        double b = 0.1;
        std::string c = "test";
        serialization::BinaryStream stream;
        ASSERT_TRUE(serialization::serialize(stream, endianness, a, b, c) == stream.size());

        // Deserialize tuple of values
        int x = 0;
        double y = 0;
        std::string z = "";
        ASSERT_TRUE(serialization::deSerialize(stream, 0, endianness, x, y, z) == stream.size());

        // Check result
        ASSERT_TRUE(a == x);
        ASSERT_TRUE(b == y);
        ASSERT_TRUE(c == z);
    }
}

//! Mockup-class to test de-serialization of serializable data objects (valid implementation)
class MockSerializableDataValid : public serialization::SerializableData {
public:
    // Members
    uint8_t m_value = 0;

    // Comparison operator
    inline bool operator==(const MockSerializableDataValid& reference) const { return m_value == reference.m_value; }

    // Serialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize serializePayload(serialization::BinaryStream& stream, const serialization::Endianness& endianness) const
    {
        return serialization::serialize(stream, endianness, m_value);
    }

    // Deserialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize deSerializePayload(const serialization::BinaryStream& stream, const serialization::BinaryStreamSize& index, const serialization::BinaryStreamSize& payloadSize, const serialization::Endianness& endianness)
    {
        (void)payloadSize; // Not needed
        return serialization::deSerialize(stream, index, endianness, m_value);
    }
};

//! Mockup-class to test de-serialization of serializable data objects (invalid implementation)
class MockSerializableDataInvalid : public serialization::SerializableData {
public:
    // Members
    uint8_t m_value = 0;

    // Comparison operator
    inline bool operator==(const MockSerializableDataInvalid& reference) const { return m_value == reference.m_value; }

    // Serialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize serializePayload(serialization::BinaryStream& stream, const serialization::Endianness& endianness) const
    {
        return serialization::serialize(stream, endianness, m_value) + 1;
    }

    // Deserialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize deSerializePayload(const serialization::BinaryStream& stream, const serialization::BinaryStreamSize& index, const serialization::BinaryStreamSize& payloadSize, const serialization::Endianness& endianness)
    {
        (void)payloadSize; // Not needed
        return serialization::deSerialize(stream, index, endianness, m_value) + 1;
    }
};

//! Check serialization and deserialization of serializable data objects
TEST(serialization, SerializableData)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        MockSerializableDataValid inputValue;
        inputValue.m_value = 123;
        MockSerializableDataValid outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputValue), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (serialization::BinaryStreamSize)1);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputValue), 0);
#endif

        // Check error (invalid user implementation)
#ifdef NDEBUG
        MockSerializableDataInvalid invalidInputValue, invalidOutputValue;
        serialization::BinaryStream invalidStream;
        ASSERT_EQ(serialization::serialize(invalidStream, endianness, invalidInputValue), 0);
        ASSERT_EQ(serialization::deSerialize(stream, 0, endianness, invalidOutputValue), 0);
#endif
    }
}

//! Mockup-class to test de-serialization of serializable (uncompressed) data objects (valid implementation)
class MockSerializableDataUncompressedValid : public serialization::SerializableData {
public:
    MockSerializableDataUncompressedValid()
    {
    }

    virtual ~MockSerializableDataUncompressedValid()
    {
    }

    // Members
    std::vector<uint8_t> m_data;

    // Comparison operator
    inline bool operator==(const MockSerializableDataUncompressedValid& reference) const { return m_data == reference.m_data; }

    // Serialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize serializePayload(serialization::BinaryStream& stream, const serialization::Endianness& endianness) const
    {
        return serialization::serialize(stream, endianness, m_data);
    }

    // Deserialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize deSerializePayload(const serialization::BinaryStream& stream, const serialization::BinaryStreamSize& index, const serialization::BinaryStreamSize& payloadSize, const serialization::Endianness& endianness)
    {
        (void)payloadSize; // Not needed
        return serialization::deSerialize(stream, index, endianness, m_data);
    }
};

//! Mockup-class to test de-serialization of serializable (compressed) data objects (valid implementation)
class MockSerializableDataCompressedValid : public MockSerializableDataUncompressedValid {
public:
    // Enable compression
    virtual compression::CompressionOptions payloadCompressionOptions() const { return compression::CompressionOptions::raw(); }
};

//! Mockup-class to test de-serialization of serializable (uncompressed) data objects (invalid implementation)
class MockSerializableDataUncompressedInvalid : public serialization::SerializableData {
public:
    MockSerializableDataUncompressedInvalid()
    {
    }

    virtual ~MockSerializableDataUncompressedInvalid()
    {
    }

    // Members
    std::vector<uint8_t> m_data;

    // Comparison operator
    inline bool operator==(const MockSerializableDataUncompressedInvalid& reference) const { return m_data == reference.m_data; }

    // Serialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize serializePayload(serialization::BinaryStream& stream, const serialization::Endianness& endianness) const
    {
        return serialization::serialize(stream, endianness, m_data) + 1;
    }

    // Deserialization of payload (see base class for details)
    virtual serialization::BinaryStreamSize deSerializePayload(const serialization::BinaryStream& stream, const serialization::BinaryStreamSize& index, const serialization::BinaryStreamSize& payloadSize, const serialization::Endianness& endianness)
    {
        (void)payloadSize; // Not needed
        return serialization::deSerialize(stream, index, endianness, m_data) + 1;
    }
};

//! Mockup-class to test de-serialization of serializable (compressed) data objects (invalid implementation)
class MockSerializableDataCompressedInvalid : public MockSerializableDataUncompressedInvalid {
public:
    // Enable compression
    virtual compression::CompressionOptions payloadCompressionOptions() const { return compression::CompressionOptions::raw(); }
};

#ifdef HAVE_ZLIB // is zlib library available?
//! Check serialization and deserialization of serializable data objects (compressed)
TEST(serialization, SerializableDataCompressed)
{
    // Initialize helpers
    const size_t preData = 123; // Count of bytes "before" the object in the stream

    // Setup dummy data with an easy-to compress pattern
    std::vector<uint8_t> dummyPattern;
    dummyPattern.resize(1024);
    for (size_t i = 0; i < dummyPattern.size(); i++)
        dummyPattern[i] = 1 + i % 3;

    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream streamWithoutCompression, streamWithCompression;
        streamWithoutCompression.resize(preData, 0);
        streamWithCompression.resize(preData, 0);
        serialization::BinaryStreamSize inputStreamSizeWithoutCompression, inputStreamSizeWithCompression, outputStreamSizeWithoutCompression, outputStreamSizeWithCompression;
        MockSerializableDataUncompressedValid inputObjectWithoutCompression, outputObjectWithoutCompression;
        MockSerializableDataCompressedValid inputObjectWithCompression, outputObjectWithCompression;
        inputObjectWithoutCompression.m_data = dummyPattern;
        inputObjectWithCompression.m_data = dummyPattern;

        // Serialize
        inputStreamSizeWithoutCompression = serialization::serialize(streamWithoutCompression, endianness, inputObjectWithoutCompression);
        inputStreamSizeWithCompression = serialization::serialize(streamWithCompression, endianness, inputObjectWithCompression);

        // Deserialize
        outputStreamSizeWithoutCompression = serialization::deSerialize(streamWithoutCompression, preData, endianness, outputObjectWithoutCompression);
        outputStreamSizeWithCompression = serialization::deSerialize(streamWithCompression, preData, endianness, outputObjectWithCompression);

        // Check data
        ASSERT_TRUE(outputObjectWithoutCompression == inputObjectWithoutCompression);
        ASSERT_TRUE(outputObjectWithCompression == inputObjectWithCompression);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSizeWithoutCompression == inputStreamSizeWithoutCompression);
        ASSERT_TRUE(outputStreamSizeWithoutCompression == streamWithoutCompression.size() - preData);
        ASSERT_TRUE(outputStreamSizeWithCompression == inputStreamSizeWithCompression);
        ASSERT_TRUE(outputStreamSizeWithCompression == streamWithCompression.size() - preData);
        ASSERT_LT(inputStreamSizeWithCompression, inputStreamSizeWithoutCompression); // Compressed data should be smaller
        const double compressionRatio = ((double)inputStreamSizeWithCompression) / ((double)inputStreamSizeWithoutCompression);
        std::cout << "compression-ratio: " << compressionRatio << " for endianness: " << serialization::endiannessToString(endianness) << std::endl;

        // Check error (invalid stream)
#ifdef NDEBUG
        const serialization::BinaryStream emptyStream;
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputObjectWithoutCompression), 0);
        ASSERT_EQ(serialization::deSerialize(emptyStream, 0, endianness, outputObjectWithCompression), 0);
        serialization::BinaryStream brokenStream;
        serialization::serialize(brokenStream, endianness, (serialization::BinaryStreamSize)1);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputObjectWithoutCompression), 0);
        ASSERT_EQ(serialization::deSerialize(brokenStream, 0, endianness, outputObjectWithCompression), 0);
#endif
        // Check error (invalid user implementation)
#ifdef NDEBUG
        MockSerializableDataUncompressedInvalid invalidInputObjectWithoutCompression, invalidOutputObjectWithoutCompression;
        MockSerializableDataCompressedInvalid invalidInputObjectWithCompression, invalidOutputObjectWithCompression;
        invalidInputObjectWithoutCompression.m_data = dummyPattern;
        invalidInputObjectWithCompression.m_data = dummyPattern;
        serialization::BinaryStream invalidStreamWithoutCompression, invalidStreamWithCompression;
        ASSERT_EQ(serialization::serialize(invalidStreamWithoutCompression, endianness, invalidInputObjectWithoutCompression), 0);
        ASSERT_EQ(serialization::serialize(invalidStreamWithCompression, endianness, invalidInputObjectWithCompression), 0);
        ASSERT_EQ(serialization::deSerialize(streamWithoutCompression, preData, endianness, invalidOutputObjectWithoutCompression), 0);
        ASSERT_EQ(serialization::deSerialize(streamWithCompression, preData, endianness, invalidOutputObjectWithCompression), 0);
#endif
    }
}
#endif
