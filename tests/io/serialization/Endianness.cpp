/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/serialization/Endianness.hpp"
#include "gtest/gtest.h"

using namespace broccoli;
using namespace io;
using namespace serialization;

//! Test converstion TO string representation of endianness
TEST(Endianness, ToString)
{
    for (uint8_t i = 0; i < static_cast<uint8_t>(Endianness::ENDIANNESS_COUNT); i++) {
        ASSERT_GT(endiannessToString(static_cast<Endianness>(i)).size(), 0);
        if (i == 0) {
            ASSERT_TRUE(endiannessToString(static_cast<Endianness>(i)) == "UNKNOWN");
        } else {
            ASSERT_TRUE(endiannessToString(static_cast<Endianness>(i)) != "UNKNOWN");
        }
    }
#ifdef NDEBUG
    ASSERT_TRUE(endiannessToString(Endianness::ENDIANNESS_COUNT) == "UNKNOWN");
#endif
}
