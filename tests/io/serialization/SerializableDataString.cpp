/*
 * This file is part of broccoli.
 * Copyright (C) 2020 Chair of Applied Mechanics, Technical University of Munich
 * https://www.amm.mw.tum.de/
 */

#include "broccoli/io/serialization/SerializableDataString.hpp"
#include "gtest/gtest.h"
#include <array>

using namespace broccoli;
using namespace io;

//! Check serialization and deserialization of SerializableDataString
TEST(SerializableDataString, BasicUsage)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        SerializableDataString inputValue;
        inputValue.m_string = "asdf";
        SerializableDataString outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());
    }
}

//! Check serialization and deserialization of array of SerializableDataString
TEST(SerializableDataString, Array)
{
    // Test little- and big-endian versions
    for (int e = 0; e <= 1; e++) {
        serialization::Endianness endianness = serialization::Endianness::LITTLE;
        if (e == 1)
            endianness = serialization::Endianness::BIG;

        // Initialize helpers
        serialization::BinaryStream stream;
        serialization::BinaryStreamSize inputStreamSize, outputStreamSize;
        std::array<SerializableDataString, 2> inputValue;
        inputValue[0].m_string = "asdf";
        inputValue[1].m_string = "jklö";
        std::array<SerializableDataString, 2> outputValue;

        // Serialize
        inputStreamSize = serialization::serialize(stream, endianness, inputValue);

        // Deserialize
        outputStreamSize = serialization::deSerialize(stream, 0, endianness, outputValue);

        // Check value
        ASSERT_TRUE(outputValue == inputValue);

        // Check stream sizes
        ASSERT_TRUE(outputStreamSize == inputStreamSize);
        ASSERT_TRUE(outputStreamSize == stream.size());
    }
}
