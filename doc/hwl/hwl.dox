namespace broccoli::hwl {
 /*!
  * \defgroup broccoli_hwl broccoli Hardware Layer Module
  * \brief The **hwl** module defines a framework for real-time communication with distributed devices
  *
  * The hardware-layer module provides a framework for real-time communication between a robot's control computer
  * and distributed devices, e.g., sensors and actuators. The framework enables to decouple the logic for operating
  * a certain device (BusDevice) from the used real-time bus technology. This makes it easier to change both the communication
  * technology and the driver software (BusDriver) for a certain bus. Furthermore, the implementation of device logic across different
  * communication technology becomes easier.
  * The decoupling of the device logic from the communication-part is achieved via the concept of "Bus Variables". These special fundamental
  * data type variables can transparently be linked to a certain object or message available via a real-time communication bus.
  * Besides the framework definitions, the hwl module additionally provides logic implementations for common devices. In the following,
  * the objects involved in the functioning of the framework are introduced shortly.
  *
  * ## Bus Variables
  * According to the standards of most real-time communication technologies, the framework differentiates
  * between two variants of bus variables: synchronous variables, which fulfill hard real-time requirements, and asynchronous variables,
  * which typically have lower priority. Moreover, every bus variable is either an output or input variable, where inputs are sensor values
  * and outputs are target values for the hardware. Example:
  * \code
  * #include <broccoli/hwl/variables.hpp>
  * using broccoli::hwl;
  * InputBusVariable<uint8_t> synchronousInput;
  * OutputBusVariable<uint16_t> synchronousOutput;
  * AsyncInputBusVariable<uint8_t> asynchronousInput;
  * \endcode
  * Bus variables are mainly designed to hold fundamental types that directly link to objects in a distributed communication system.
  * Still, it is possible to use aggregate types and packed structs as data type:
  * \code
  * AsyncInputBusVariable<CANopen::EmergencyMessage> m_emergencyMessage; // CANopen Emergency message
  * InputBusVariable<std::array<uint8_t, 10>> m_arrayOfBytes; // Can link to a 10 byte object
  * \endcode
  *
  * All operations on bus variables are thread-safe (unless noted differently). The internal data area is protected
  * via a broccoli::memory::ThreadSafeData instance. All variables are implicitly convertible to their data type. Output bus variables
  * are additionally assignable from these types.
  *
  * ### Asynchronous Communication States
  *
  * Asynchronous bus variables store the data value itself and some flags to represent the current state of the variable, i.e.,
  * is there a transfer in progress, has it failed or not, see AsyncTransferState.
  * The data may be accessed by locking the container of the bus variable explicitly. The returned data guard acts like
  * a pointer to the bus variable container:
  * \code
  * AsyncInputBusVariable<uint8_t> asynchronousInput;
  * {
  *     auto guard = asynchronousInput.lockConstWithGuard(); // Read-only lock
  *     bool transferFinishedAndValid = guard->isLatched();
  *     bool transferPending = guard->isPending();
  * }
  }
  * \endcode
  *
  * ### Bus Variable Pointers
  * To be able to link bus variables to bus communication objects, pointers to the variables are used. These special objects
  * can be created from any bus variable and share the memory-region lock, i.e. the access to a bus variable via a pointer to it
  * is also thread-safe. The framework differentiates between \ref BusVariablePointer for synchronous objects and AsyncBusVariablePointer,
  * which adds an additional interface to access the transfer state via the pointer as well.
  * Example:
  * \code
  * InputBusVariable<uint8_t> synchronousInput;
  * BusVariablePointer pointerToSynchronous(synchronousInput);
  * {
  *     // guard with write access
  *     auto guard = pointerToSynchronous.lockWithGuard();
  *     // guard->ptr() stores a raw pointer to the variable's data area.
  * }
  * Bus variable pointers are independent from the original data type or data direction (input/output). It is in the responsibility
  * of the BusDriver implementation to ensure pointers to bus variables are only used in a valid way.
  * \endcode
  *
  * ## BusDevices
  * A bus device implements logic for the abstraction of a certain logical device connected to a real-time communication bus. This can
  * be a certain sensor or actuator type, for example. All bus devices must derive from BusDevice and use dynamic polymorphism to
  * allow a BusDriver keep track of all its devices. The public interface of a bus device is intended to directly be called from user code.
  * The constructor of a BusDevice always takes a reference to the BusDriver it is attached to:
  * \code
  * EL1012 device(bus); // bus is a reference to BusDriver or BusDriverControl used to control the bus from inside the device implementation.
  * auto&& device = bus.makeDevice<EL1012>(); // shortcut for the line above
  * \endcode
  *
  * A bus device must declare a template method to link its bus variables to certain bus objects:
  * \code
  * template <typename Derived>
  * void linkVariables(BusVariableRegistryBase<Derived>& registry)
  * {
  *     registry.registerVariable(m_controlWord, typename EtherCAT::ObjectIdentifierType{ "ControlWord" });
  *     registry.registerVariable(m_asyncOutput, typename EtherCAT::AsyncObjectIdentifierType{ 0x6000, 0x1 });
  * }
  * \endcode
  * The variables are registered to a bus variable registry, which is implemented by a certain BusDriver.
  * The object identifier type depends on the used bus technology. For EtherCAT, this is the name of the variable of
  * a slave (without slave name, which is added by the registry) or an index/subindex pair for mailbox communication.
  *
  * Every BusDevice implements a BusDevice::process() method to update its state based on input variables and calculate new
  * values for output variables. This cyclic dispatch of a device's logic is automatically from the BusDriver.
  * For an example implementation refer to \ref EL200X.
  *
  * ## BusDrivers
  * A BusDriver provides interfaces for the communication via a certain bus technology. For master/slave systems this is
  * the master-stack implementation with additional logic to attach bus variables to objects.
  *
  * The BusDriver interface exposes a public method process(), which must be called cyclically from the user code. If and
  * how these calls must be synchronized to other events depends on the specific BusDriver implementation.
  * The call triggers the execution of code of the BusDriver itself (BusDriver::processBus()) and all devices attached to it (BusDevice::process()).
  * To add a device to a BusDriver, it must declare an addDevice() method (or similar), which takes a specific implementation
  * of a BusDevice as argument. All devices provide a template method linkVariables(), which takes a so-called bus variable registry as argument.
  * In this way, a BusDevice tells its BusDriver which of its bus variables should be linked to which objects/messages on the bus.
  * \warning When bus devices are copied or **moved** the linking to bus variables becomes invalid!
  * This improves cache coherency in trade for comfort. Thus, addDevice() must be called only when the device is in its final storage location.
  *
  * \code
  * SomeBusDriver master;
  * auto&& device = master.makeDevice<EL1012>([optional arguments to EL1012 constructor..]);
  * master.addDevice(device, [implementation-dependent identifier for device on bus]);
  * \endcode
  *
  * Example Driver:
  * \code
  * class MyBusDriver : public broccoli::hwl::BusDriver<broccoli::hwl::EtherCAT> {
  * public:
  *     ...
  *     template<typename DeviceType>
  *     void addDevice(const DeviceType& device, [implementation-dependent identifier for device on bus]) {
  *         device.linkVariables(m_registry);
  *         ...
  *     }
  * private:
  *     MyBusDriverRegistry m_registry;
  * };
  * \endcode
  *
  * ### Bus Variable Registries
  * A bus variable registry has the purpose of storing data for all bus variables that are a attached to objects of a certain BusDriver. A registry
  * must derive statically from BusVariableRegistryBase and make sure it provides registerVariable() specializations for all bus variables it supports.
  * After all devices registered their bus variables, the BusDriver can use this data to lookup target bus variables once new data arrives for
  * a certain object. Analogously, if the value of a bus variable changes, the BusDriver may access its data via the registry to send new data via its communication bus.
  * Bus variable registries do not store a reference to the actual bus variable itself but use a \ref BusVariablePointer instead.
  *
  * ### BusDriver Control Interface
  *
  * The BusDriverControlInterface is a parent class of BusDriver and defines an interface which is passed to the BusDevices on construction. Via this interface,
  * devices may request a change in the bus state, get descriptive data from it, or trigger asynchronous (mailbox) communication for a bus variable.
  *
  *
  * ## Technology Bridges and Specialization
  * Both BusDevice and BusDriver are templated for a certain BusType, i.e. bus technology. This makes it possible to
  * have specialization of a device for different technologies. Furthermore, a class may both derive from BusDevice and BusDriver
  * to form a bridge between two communication technologies. As a result, device implementations can be used even if the
  * data is channelled via another bus technology. For example, a CAN device implementation may either be attached directly to a BusDriver<CAN>
  * implementation or a bridging device for CAN over EtherCAT, see \ref ESDCANEtherCATGateway.
  *
  * \{
  * \defgroup broccoli_hwl_bus_types Bus Types
  * \brief Defines bus description types for different real-time communication technologies.
  *
  * \defgroup broccoli_hwl_components Components
  * \brief Specific BusDevice and BusDriver component implementations for common devices.
  *
  * \defgroup broccoli_hwl_variables Bus Variables
  * \brief Interfaces and implementations related to the bus variable concept.
  *
  * \}
  *
  * \namespace broccoli::hwl
  * \brief Namespace of \ref broccoli_hwl
  */
}