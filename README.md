# Beautiful Robot C++ Code Library (Broccoli)

Broccoli is a generic C++ header-only library for common algorithms used in robotics. It puts special emphasis on **real-time capability**, **speed**, and **portability** to real-time operating systems.

The main motivation behind the development of broccoli is code reuse between robotic projects at the Chair of Applied Mechanics, TUM. Some parts of broccoli originate from the source code of the [Lola Humanoid Robot](https://www.mec.ed.tum.de/en/am/research/current-projects/robotics/humanoid-robot-lola/) project. Broccoli is not a complete robotics ecosystem (such as ROS); instead it is meant to provide a set of independently usable modules for rapid prototyping in robotic research projects.

Maintained Release: **Broccoli Sparco 3.x.x 'sparco'**

Officially supported operating systems: **Linux (RT), QNX Neutrino RTOS 7.0, macOS**

The Broccoli library will most likely work with any POSIX compatible OS.

## Feature Overview

The Broccoli library is organised in modules:
- **analysis:**
  * taskspace analysis
- **control:**
  * feedback control
  * filters & LTI models
  * inverse kinematics
  * signal sources
  * signal processing based on expression templates
- **core:**
  * high precision timing
  * extensions to std and Eigen math libs
  * floating point comparison helpers
- **curve:**
  * interpolation and evaluation of curves, splines and trajectories
  * regular (polynomial, exponential, trigonometric) and quaternion (LERP, NLERP, SLERP, SQUAD, Bezier, BSpline) curves
  * multi-dimensional trajectories
- **geometry:**
  * polygons
  * meshes
  * rotations
  * swept-sphere volumes
- **hwl:**
  * framework for real-time communication with distributed devices
  * implementations for common bus technologies and devices
- **io:**
  * generic file i/o
  * import/export for PLY (.ply) and Portable Anymap (.pbm, .pgm, .ppm)
  * console output
  * real-time logging
  * serialization
  * compression
  * network communication
  * hmi interfaces
- **memory:**
  * efficient and thread-safe memory management and buffers
  * n-dimensional vectors and dense multi-level grids (binary tree, quadtree, octree, ...)
  * smart vectors (hybrid static and dynamic allocation)
- **ode:**
  * algorithms for solving differential equations
  * time integration
  * spline collocation (cubic and quintic)
- **parallel:**
  * efficient multi-threading and parallel-processing
  * real-time synchronization

## Documentation

[Broccoli API Documentation](https://am.pages.gitlab.lrz.de/broccoli/) (latest release)

## Requirements

* A C++14-compatible compiler

**Optional Dependencies:**

Some broccoli components make use of

* **Eigen** for linear algebra
* **SDL2** for device i/o
* **zlib** for log file compression

**Development Resources:**

The following resources are recommended for developers:

- **doxygen** for API documentation generation
- **llvm-cov/llvm-profdata** for coverage reports
- **clang-tidy** to analyze/fix the code

## Build Options

The following options can be passed to CMake:

| Name | Description | Default Value |
|---|---|---|
| OPTION_BUILD_EXAMPLES | Build example applications | OFF |
| OPTION_BUILD_DOC | Enables documentation generation with doxygen | OFF |
| OPTION_BUILD_TESTS | Enables unit testing | OFF |
| OPTION_ENABLE_COVERAGE | Enables coverage detection for unit tests | ON |
| OPTION_CLANG_TIDY_FIX | Clang-tidy will always check the code if it is available on your system. If you enable this option, the code will be fixed automatically. | OFF |

**Targets:**

- **tests**: Builds the integrated unit tests if testing is enabled
- **coverage**: Creates coverage information on the unit tests. Coverage is output to console and a html report in the build directory.
- **doc**: (Re-)triggers generation of doxygen documentation, if OPTION_BUILD_DOCS is ON
- **update-googletest**: Updates the downloaded version of Google Test
- **update-eigen**: Updates the downloaded version of the Eigen library

## Using Broccoli in CMake Projects

Broccoli exports the target ```eat::broccoli```, which can be imported using ```find_package()```. By adding
the target via ```target_link_libraries``` to your user target, all include directories are set automatically:

```cmake
cmake_minimum_required (VERSION 3.8)
project (myproject)

find_package (broccoli 3.0.0 REQUIRED)

add_executable (example example.cpp)
target_link_libraries (example eat::broccoli)
```

You may also let broccoli pull in the dependencies to the external libraries via cmake components:
```cmake
find_package (broccoli 3.0.0 REQUIRED COMPONENTS eigen sdl zlib)
```
This way broccoli automatically searches for these libraries and adds include directories/linker settings to
the broccoli target. By adding eat::broccoli to your targets, these will automatically be configured for the external libraries.

**Overview on broccoli components:**

| Name | Description | Compiler Definition |
|---|---|---|
| eigen | Search for Eigen3 library and add include directories to target (header-only) | HAVE_EIGEN3
| sdl | Search for SDL2 library and add include directories / link libraries to target | HAVE_SDL2 |
| zlib | Search for zlib library and add include directories / link libraries to target | HAVE_ZLIB |

If you want to add external dependencies by yourself, be sure to add the corresponding compiler definitions to
activate corresponding code in broccoli.

### Broccoli as an External Project

Instead of installing broccoli to the system directories you may use CMake's ExternalProjects feature. This involves creating a file ```broccoli.CMakeLists.txt.in```, which is then used to pull and install broccoli during CMake configuration time.

Add the file ```broccoli.CMakeLists.txt.in``` to your ```cmake``` directory. You may specify any branch or release tag name to get a certain version of broccoli:
```cmake
cmake_minimum_required(VERSION 2.8.2)

project(broccoli-download NONE)
include(ExternalProject)

ExternalProject_Add(
    broccoli
    GIT_REPOSITORY      https://gitlab.lrz.de/AM/broccoli.git
    GIT_TAG             origin/sparco
    PREFIX              "${CMAKE_BINARY_DIR}/broccoli"
    INSTALL_DIR         "${CMAKE_BINARY_DIR}/broccoli"
    CMAKE_ARGS          "-DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>"
    TEST_COMMAND        ""
    UPDATE_DISCONNECTED 1
    STEP_TARGETS        update
)
```

Then add some code to your ```CMakeLists.txt``` file to pull and install broccoli as external project:
```cmake
if (NOT broccoli_DIR)
    # configure and install broccoli (if not already done by super-project)
    configure_file(${CMAKE_SOURCE_DIR}/cmake/broccoli.CMakeLists.txt.in ${CMAKE_BINARY_DIR}/broccoli/download/CMakeLists.txt)
    execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/broccoli/download)
    if(result)
       message(FATAL_ERROR "CMake step for broccoli failed: ${result}")
    endif()
    execute_process(COMMAND ${CMAKE_COMMAND} --build .
        RESULT_VARIABLE result
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/broccoli/download)
    if(result)
        message(FATAL_ERROR "Build step for broccoli failed: ${result}")
    endif()

    add_custom_target(update-broccoli
        COMMAND ${CMAKE_COMMAND} --build . --target broccoli-update
        COMMAND ${CMAKE_COMMAND} --build . --target all
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/broccoli/download)

    set(broccoli_DIR ${CMAKE_BINARY_DIR}/broccoli)
endif()
```

This will install broccoli to your build directory on first cmake run. Furthermore, an additional target ```update-broccoli``` can be used to update the broccoli sources from the git repository.
To use broccoli in your CMake project just use find_package() and the imported target as usual.

## Citation
For citation in scientific papers, please use the following BibLaTeX entry:
```
@ELECTRONIC{broccoli,
  author = {Seiwald, Philipp and Sygulla, Felix},
  year = {2022},
  title = {{broccoli: Beautiful Robot C++ Code Library}},
  url = {https://gitlab.lrz.de/AM/broccoli},
  doi = {10.14459/2022mp1686390}
}
```

## Acknowledgements
This work was supported by the German Research Foundation (DFG) (grant number [407378162](https://gepris.dfg.de/gepris/projekt/407378162)).
